//
//  UITextField+PlaceHolder.h
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (PlaceHolder)
- (void)setPlaceHolder:(NSString *)holderText Font:(UIFont *)font color:(UIColor *)color;
- (void)textLeftOffset:(CGFloat)offset;
@end

NS_ASSUME_NONNULL_END
