//
//  UILabel+Style.h
//  Seller
//
//  Created by 林海 on 2018/12/21.
//  Copyright © 2018年 林海. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Style)
- (void)changeStrings:(NSArray *)strArray toSize:(NSInteger)fontSize;
- (void)changeStrings:(NSArray *)strArray toColor:(UIColor *)color;
- (void)changStrings:(NSArray *)strArray toSize:(NSInteger)fontSize toColor:(UIColor *)color;
- (void)addColorHeaderTager:(NSString *)tager backgroundColor:(UIColor *)backgroundColor;
- (void)addDeleteLineWithColor:(UIColor *)color;
-(void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing;
@end

NS_ASSUME_NONNULL_END
