//
//  UILabel+Style.m
//  Seller
//
//  Created by 林海 on 2018/12/21.
//  Copyright © 2018年 林海. All rights reserved.
//

#import "UILabel+Style.h"

@implementation UILabel (Style)
- (void)changeStrings:(NSArray *)strArray toSize:(NSInteger)fontSize{
    NSString *string = self.text;
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    for (NSString *str in strArray){
        NSRange range = [string rangeOfString:str];
        [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
    }
    [self setAttributedText:attributeStr];
}

- (void)changeStrings:(NSArray *)strArray toColor:(UIColor *)color{
    NSString *string = self.text;
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    for (NSString *str in strArray){
        NSRange range = [string rangeOfString:str];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
    [self setAttributedText:attributeStr];
}

- (void)changStrings:(NSArray *)strArray toSize:(NSInteger)fontSize toColor:(UIColor *)color{
    NSString *string = self.text;
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    for (NSString *str in strArray){
        NSRange range = [string rangeOfString:str];
        [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
    [self setAttributedText:attributeStr];
}

- (void)addColorHeaderTager:(NSString *)tager backgroundColor:(UIColor *)backgroundColor
{
    NSString *string = [NSString stringWithFormat:@"%@ %@",tager,self.text];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];;
    NSRange range = [string rangeOfString:tager];
    [attributeStr addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:range];
    [self setAttributedText:attributeStr];
}
- (void)addDeleteLineWithColor:(UIColor *)color{

    NSUInteger length = [self.text length];
    //从这里开始就是设置富文本的属性
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:self.text];
    
    //下面开始是设置线条的风格：
    //第一个参数addAttribute:是设置要中线（删除线）还是下划线。
    //NSStrikethroughStyleAttributeName：这种是从文本中间穿过，也就是删除线。
    //NSUnderlineStyleAttributeName：这种是下划线。
    
    //第二个参数value：是设置线条的风格：虚线，实现，点线......
    //第二参数需要同时设置Pattern和style才能让线条显示。
    
    //第三个参数range:是设置线条的长度，切记，不能超过字符串的长度，否则会报错。
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)  range:NSMakeRange(0, length)];
    
    //下列是设置线条的颜色
    //第一个参数就是选择设置中线的颜色还是下划线的颜色，如果上面选择的是中线，这里就要选择中线，否则颜色设置不上去。
    //第二个参数很简单，就是颜色而已。
    //第三个参数：同上。
    [attri addAttribute:NSStrikethroughColorAttributeName value:color range:NSMakeRange(0, length)];
    
    [self setAttributedText:attri];
}

-(void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing {
    if (!text || lineSpacing < 0.01) {
        self.text = text;
        return;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpacing];        //设置行间距
    [paragraphStyle setLineBreakMode:self.lineBreakMode];
    [paragraphStyle setAlignment:self.textAlignment];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    self.attributedText = attributedString;
}

@end
