//
//  NSString+Process.m
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "NSString+Process.h"

@implementation NSString (Process)
-  (BOOL) isBlankString{
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
         return YES;
     }
     if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
         return YES;
     }
     return NO;
 }

- (NSString *)cutOutWhiteSpace{
    NSString *str=[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return str;
}

@end
