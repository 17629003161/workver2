//
//  NSString+Json.h
//  Order
//
//  Created by 林海 on 2018/12/28.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Json)
+ (NSDictionary *)jsonStringToDictionary:(NSString *)jsonString;
- (NSString *)trimWhitespace;
@end

NS_ASSUME_NONNULL_END
