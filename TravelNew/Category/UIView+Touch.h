//
//  UIView+Touch.h
//  Order
//
//  Created by Mac on 2018/7/24.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Touch)
- (void)addTapGestureTarget:(id)target action:(SEL)tapAction;
- (void)removeKeyboard;
@end
