//
//  UIImage+ToBase64.m
//  TravelNew
//
//  Created by mac on 2020/11/20.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UIImage+ToBase64.h"

@implementation UIImage (ToBase64)
- (NSString*)encodeToBase64String{
    return [UIImagePNGRepresentation(self) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage*)decodeBase64ToImage:(NSString*)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
