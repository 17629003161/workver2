//
//  UIImageView+Scale.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UIImage+Scale.h"

@implementation UIImage (Scale)
+ (UIImage *)resizableImageWithName:(NSString *)imageName

{
// 加载原有图片
UIImage *norImage = [UIImage imageNamed:imageName];
// 获取原有图片的宽高的一半
CGFloat w = norImage.size.width * 0.5;
CGFloat h = norImage.size.height * 0.5;
// 生成可以拉伸指定位置的图片
UIImage *newImage = [norImage resizableImageWithCapInsets:UIEdgeInsetsMake(15, 10, h, w) resizingMode:UIImageResizingModeStretch];
return newImage;
}

@end
