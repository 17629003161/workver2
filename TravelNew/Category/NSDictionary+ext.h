//
//  NSDictionary+ext.h
//  TravelNew
//
//  Created by mac on 2020/10/31.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (ext)
- (BOOL)containsKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
