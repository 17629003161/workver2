//
//  UITabBar+badge.h
//  TravelNew
//
//  Created by mac on 2021/1/25.
//  Copyright © 2021 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITabBar (badge)
- (void)showBadgeOnItemIndex:(int)index;   //显示小红点
- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点
@end

NS_ASSUME_NONNULL_END
