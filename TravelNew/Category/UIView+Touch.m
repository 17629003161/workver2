//
//  UIView+Touch.m
//  Order
//
//  Created by Mac on 2018/7/24.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "UIView+Touch.h"

@implementation UIView (Touch)
- (void)addTapGestureTarget:(id)target action:(SEL)tapAction{
    [self setUserInteractionEnabled:YES];
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:target action:tapAction]];
}

- (void)removeKeyboard{
    for (UIView *obj in self.subviews) {
        if([obj isKindOfClass:[UITextView class]] || [obj isKindOfClass:[UITextField class]] ){
            [obj resignFirstResponder];
        }
    }
}
@end
