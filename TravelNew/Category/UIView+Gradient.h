//
//  UIView+Gradient.h
//  Order
//
//  Created by mac on 2020/11/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Gradient)
- (void)gradientFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor bounds:(CGRect)bounds;   //渐变色
- (void)gradientDefault:(CGRect)bounds;
- (void)gradientDisable:(CGRect)bounds;
@end

NS_ASSUME_NONNULL_END
