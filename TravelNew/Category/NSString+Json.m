//
//  NSString+Json.m
//  Order
//
//  Created by 林海 on 2018/12/28.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "NSString+Json.h"

@implementation NSString (Json)
+ (NSDictionary *)jsonStringToDictionary:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers error:&err];
    
    if(err) {
        DLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

- (NSString *)trimWhitespace{
    //去空格和回车
    //如果仅仅是去前后空格，用whitespaceCharacterSet
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *tmpstr = [self stringByTrimmingCharactersInSet:set];
    return tmpstr;
}

@end
