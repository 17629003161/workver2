//
//  UIImage+Color.h
//  CMall
//
//  Created by hanJianXin on 15/11/24.
//  Copyright © 2015年 hanJianXin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)
+ (UIImage *)imageWithColor:(UIColor *)color;

- (UIImage *)imageWithColor:(UIColor *)color;

@end
