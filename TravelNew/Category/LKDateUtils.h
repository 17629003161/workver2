//
//  LKDateUtils.h
//  TravelNew
//
//  Created by mac on 2020/12/1.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKDateUtils : NSObject
@property(nonatomic,assign) NSTimeInterval timeInterval;  //单位为秒
@property(nonatomic,strong) NSDate *date;
@property(nonatomic,strong) NSString *dateTimeString;       //yyyy-MM-dd HH:mm:ss
@property(nonatomic,strong) NSString *dateString;       //yyyy-MM-dd HH:mm:ss
@property(nonatomic,strong) NSString *showString;       //显示今天，明天，后天等描述性文字


+ (LKDateUtils *)today;
- (LKDateUtils *)nextDay;
- (NSString *)dateString;
- (NSString *)timeString;
- (NSString *)dateTimeString;

@property NSInteger year;
@property NSInteger month;
@property NSInteger day;
@property NSInteger hour;
@property NSInteger minute;
@property NSInteger second;
@property NSString *weekday;

@end

NS_ASSUME_NONNULL_END
