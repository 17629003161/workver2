//
//  NSDictionary+ext.m
//  TravelNew
//
//  Created by mac on 2020/10/31.
//  Copyright © 2020 lester. All rights reserved.
//

#import "NSDictionary+ext.h"

@implementation NSDictionary (ext)
- (BOOL)containsKey:(NSString *)key{
    return [[self allKeys] containsObject:key];
}
@end
