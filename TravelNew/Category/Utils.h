//
//  Utils.h
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject
+ (UILabel *)createLabelWithTitle:(NSString *)title
                       titleColor:(UIColor *)color
                         fontSize:(CGFloat)size;

+ (UILabel *)createBoldLabelWithTitle:(NSString *)title
                           titleColor:(UIColor *)color
                             fontSize:(CGFloat)size;

+ (NSInteger)randomNumber:(NSInteger)maxNumber;

+ (UIImage *)getImageFromView:(UIView *)view;

+ (NSString *)numberToStr:(NSNumber *)number;

+ (NSNumber *)stringToNumber:(NSString *)str;

+ (void)callNumber:(NSString *)phoneNumber;     //拨打电话

+ (NSString *)sexToString:(NSNumber *)sex;
+ (NSNumber *)sexStringToSex:(NSString *)str;
+ (void)sendVerifyCodeToPhoneNumber:(NSString *)phone;   //发送验证码
+ (NSString *)maskPhoneNumber:(NSString *)phoneNumber;   //屏蔽部分电话号码

//计算两点地理位置之间的距离
+ (double)calcDistanceFromLocation:(CLLocation *)location toLocation:(CLLocation *)toLocation;

- (UIImage *)ct_imageFromImage:(UIImage *)image inRect:(CGRect)rect;  //iOS拍照获取指定区域图片

+ (UIImage *)compressImageSize:(UIImage *)image toByte:(NSUInteger)maxLength;

+ (void)showMsg:(NSString *)msg imgName:(NSString *)imgName;

+ (UIView *)createNoDataView;

+ (NSString *)formatMoney:(double)number;

+ (CGFloat)getStatusBarHight;

+ (UIImage *)resizableImageWithName:(NSString *)imageName;

@end

NS_ASSUME_NONNULL_END
