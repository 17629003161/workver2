//
//  Utils.m
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "Utils.h"

@implementation Utils
+ (UILabel *)createLabelWithTitle:(NSString *)title
                       titleColor:(UIColor *)color
                         fontSize:(CGFloat)size{
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:size];
    label.textColor = color;
    label.text = title;
    return label;
}

+ (UILabel *)createBoldLabelWithTitle:(NSString *)title
                       titleColor:(UIColor *)color
                         fontSize:(CGFloat)size{
    UILabel *label = [UILabel new];
    label.font = [UIFont boldSystemFontOfSize:size];
    label.textColor = color;
    label.text = title;
    return label;
}

+ (NSInteger)randomNumber:(NSInteger)maxNumber{
    return arc4random() % maxNumber;
}

+ (UIImage *)getImageFromView:(UIView *)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSString *)numberToStr:(NSNumber *)number
{
    NSString *num_str = [NSNumberFormatter localizedStringFromNumber:number numberStyle:NSNumberFormatterNoStyle];
    return num_str;
}

+ (NSNumber *)stringToNumber:(NSString *)str
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    NSNumber *num = [f numberFromString:str];
    return num;
}

+ (void)callNumber:(NSString *)phoneNumber
{
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

+ (NSString *)sexToString:(NSNumber *)sex
{
    NSArray *array = @[@"女",@"男",@"未知"];
    NSInteger i = [sex integerValue];
    if(i>2) i = 2;
    return array[i];
}

+ (NSNumber *)sexStringToSex:(NSString *)str
{
    NSInteger i = 2;
    if([str isEqualToString:@"女"]){
        i = 0;
    }else if([str isEqualToString:@"男"]){
        i = 1;
    }else{
        i = 2;
    }
    return @(i);
}

+ (void)sendVerifyCodeToPhoneNumber:(NSString *)phone{
    Api *api = [Api apiPostUrl:@"user/getValidate"
                          para:@{@"phone":phone}
                   description:@"user-获取短信验证码"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:@"验证码发送成功"];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}
+ (NSString *)maskPhoneNumber:(NSString *)phoneNumber
{
    return [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
}


+ (double)calcDistanceFromLocation:(CLLocation *)location toLocation:(CLLocation *)toLocation{
    CLLocationDistance meters=[toLocation distanceFromLocation:location];
    return meters;
}


- (UIImage *)ct_imageFromImage:(UIImage *)image inRect:(CGRect)rect{
    //把像 素rect 转化为 点rect（如无转化则按原图像素取部分图片）
    CGFloat scale = [UIScreen mainScreen].scale;
    CGFloat x= rect.origin.x*scale,
    y=rect.origin.y*scale,
    w=rect.size.width*scale,
    h=rect.size.height*scale;
    CGRect dianRect = CGRectMake(x, y, w, h);
    //截取部分图片并生成新图片
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, dianRect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    return newImage;
}



/*!
 *  @brief 使图片压缩后刚好小于指定大小
 *
 *  @param image 当前要压缩的图 maxLength 压缩后的大小
 *
 *  @return 图片对象
 */
//图片质量压缩到某一范围内，如果后面用到多，可以抽成分类或者工具类,这里压缩递减比二分的运行时间长，二分可以限制下限。
+ (UIImage *)compressImageSize:(UIImage *)image toByte:(NSUInteger)maxLength{
    //首先判断原图大小是否在要求内，如果满足要求则不进行压缩，over
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    //原图大小超过范围，先进行“压处理”，这里 压缩比 采用二分法进行处理，6次二分后的最小压缩比是0.015625，已经够小了
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    //判断“压处理”的结果是否符合要求，符合要求就over
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    //缩处理，直接用大小的比例作为缩处理的比例进行处理，因为有取整处理，所以一般是需要两次处理
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        //获取处理后的尺寸
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio)));
        //通过图片上下文进行处理图片
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //获取处理后图片的大小
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}



+ (void)showMsg:(NSString *)msg imgName:(NSString *)imgName
{
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    // 显示模式,改成customView,即显示自定义图片(mode设置,必须写在customView赋值之前)
    hud.mode = MBProgressHUDModeCustomView;
    // 设置要显示 的自定义的图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    // 显示的文字,比如:加载失败...加载中...
    hud.label.text = msg;
    // 标志:必须为YES,才可以隐藏,  隐藏的时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.0];
}

+ (UIView *)createNoDataView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(@"no_data");
    UILabel *label = [Utils createLabelWithTitle:@"当前暂无数据"
                                      titleColor:HexColor(0x999999)
                                        fontSize:16];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .topSpaceToView(view, 60)
    .centerXEqualToView(view)
    .widthIs(190)
    .heightIs(134);
    
    label.sd_layout
    .topSpaceToView(imgv, 10)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

// 自定义正数格式(金额的格式转化) 94,862.57 前缀可在所需地方随意添加
+ (NSString *)formatMoney:(double)number{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.positiveFormat = @",###.##"; // 正数格式
    // 注意传入参数的数据长度，可用double
    NSString *money = [formatter stringFromNumber:@(number)];
//    money = [NSString stringWithFormat:@"￥%@", money];
    
    return money;
}

+ (CGFloat)getStatusBarHight {
   float statusBarHeight = 0;
   if (@available(iOS 13.0, *)) {
       UIStatusBarManager *statusBarManager = [UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager;
       statusBarHeight = statusBarManager.statusBarFrame.size.height;
   }
   else {
       statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
   }
   return statusBarHeight;
}

+ (UIImage *)resizableImageWithName:(NSString *)imageName
{
// 加载原有图片
UIImage *norImage = [UIImage imageNamed:imageName];
// 获取原有图片的宽高的一半
//CGFloat w = norImage.size.width * 0.5;
//CGFloat h = norImage.size.height * 0.5;
// 生成可以拉伸指定位置的图片
UIImage *newImage = [norImage resizableImageWithCapInsets:UIEdgeInsetsMake(15, 10, 15, 15) resizingMode:UIImageResizingModeStretch];
return newImage;
}


@end
