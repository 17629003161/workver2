//
//  LKDateUtils.m
//  TravelNew
//
//  Created by mac on 2020/12/1.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKDateUtils.h"

@implementation LKDateUtils

- (void)setTimeInterval:(NSTimeInterval)timeInterval{
    _timeInterval = timeInterval;
    _date=[NSDate dateWithTimeIntervalSince1970:timeInterval];
    [self getDataCompent];
    
}
-(void)setDate:(NSDate *)date{
    _date = date;
    _timeInterval = [date timeIntervalSince1970];
    [self getDataCompent];
}

-(void)setDateTimeString:(NSString *)dateTimeString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; // 年-月-日 时:分:秒
    NSDate * date = [formatter dateFromString:dateTimeString];
    self.date = date;
}

-(void)setDateString:(NSString *)dateString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"]; // 年-月-日
    NSDate * date = [formatter dateFromString:dateString];
    self.date = date;
}



+ (LKDateUtils *)today{
    LKDateUtils *date = [LKDateUtils new];
    date.date = [NSDate date];
    return date;
}

- (LKDateUtils *)nextDay{
    LKDateUtils *date =  [LKDateUtils new];
    date.timeInterval  = _timeInterval + 24*60*60;
    return date;
}


- (NSString *)dateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //获取当前时间日期展示字符串 如：2019-05-23-13:58:59
    NSString *str = [formatter stringFromDate:_date];
    return str;
}

- (NSString *)timeString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    //获取当前时间日期展示字符串 如：2019-05-23-13:58:59
    NSString *str = [formatter stringFromDate:_date];
    return str;
}

- (NSString *)dateTimeString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //获取当前时间日期展示字符串 如：2019-05-23-13:58:59
    NSString *str = [formatter stringFromDate:_date];
    return str;
}

- (void)getDataCompent{
    //下面是单独获取每项的值
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear |NSCalendarUnitMonth|NSCalendarUnitDay |NSCalendarUnitWeekday |NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:_date];
    
    self.year = comps.year;
    self.month = comps.month;
    self.day = comps.day;
    self.hour = comps.hour;
    self.minute = comps.minute;
    self.second = comps.second;
    self.weekday = [self getWeekString:comps.weekday];
}

- (NSString *)getWeekString:(NSInteger)weakday{
    NSArray *weakAry = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    NSInteger weak = weakday-1;
    return weakAry[weak];
}

@end
