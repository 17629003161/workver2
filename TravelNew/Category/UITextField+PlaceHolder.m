//
//  UITextField+PlaceHolder.m
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UITextField+PlaceHolder.h"

@implementation UITextField (PlaceHolder)

- (void)setPlaceHolder:(NSString *)holderText Font:(UIFont *)font color:(UIColor *)color{
    NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
    [placeholder addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, holderText.length)];
    [placeholder addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, holderText.length)];
    self.attributedPlaceholder = placeholder;
}

- (void)textLeftOffset:(CGFloat)offset{
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, offset, 1)];
    self.leftView=emptyView;
    self.leftViewMode=UITextFieldViewModeAlways;
}

@end
