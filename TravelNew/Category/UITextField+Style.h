//
//  UITextField+Style.h
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Style)
- (void)changeStrings:(NSArray *)strArray toSize:(NSInteger)fontSize;
- (void)changeStrings:(NSArray *)strArray toColor:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
