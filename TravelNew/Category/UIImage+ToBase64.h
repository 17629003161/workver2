//
//  UIImage+ToBase64.h
//  TravelNew
//
//  Created by mac on 2020/11/20.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (ToBase64)
- (NSString*)encodeToBase64String;
+ (UIImage*)decodeBase64ToImage:(NSString*)strEncodeData;
@end

NS_ASSUME_NONNULL_END
