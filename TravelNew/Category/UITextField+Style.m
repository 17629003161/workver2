//
//  UITextField+Style.m
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "UITextField+Style.h"

@implementation UITextField (Style)
- (void)changeStrings:(NSArray *)strArray toSize:(NSInteger)fontSize{
    NSString *string = self.text;
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    for (NSString *str in strArray){
        NSRange range = [string rangeOfString:str];
        [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:range];
    }
    [self setAttributedText:attributeStr];
}

- (void)changeStrings:(NSArray *)strArray toColor:(UIColor *)color{
    NSString *string = self.text;
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    for (NSString *str in strArray){
        NSRange range = [string rangeOfString:str];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
    [self setAttributedText:attributeStr];
}
@end
