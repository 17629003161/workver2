//
//  UIImageView+Scale.h
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Scale)
+ (UIImage *)resizableImageWithName:(NSString *)imageName;
@end

NS_ASSUME_NONNULL_END
