//
//  UIView+Angle.h
//  TravelNew
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Angle)
- (void)makeTopAngleSize:(CGSize)size bounds:(CGRect)bounds;
- (void)makeBottomAngleSize:(CGSize)size bounds:(CGRect)bounds;
- (void)makeRectCorner:(UIRectCorner)rectcorner cornerSize:(CGSize)cornerSize bounds:(CGRect)bounds;
@end

NS_ASSUME_NONNULL_END
