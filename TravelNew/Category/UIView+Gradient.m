//
//  UIView+Gradient.m
//  Order
//
//  Created by mac on 2020/11/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UIView+Gradient.h"

@implementation UIView (Gradient)
- (void)gradientFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor bounds:(CGRect)bounds{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
      gradientLayer.colors = @[(__bridge id)fromColor.CGColor, (__bridge id)toColor.CGColor];
      gradientLayer.locations = @[@(0),@(1.0f)];
      gradientLayer.startPoint = CGPointMake(0, 0);
      gradientLayer.endPoint = CGPointMake(1.0, 0);
      gradientLayer.frame = bounds;
      [self.layer addSublayer:gradientLayer];
}

- (void)gradientDefault:(CGRect)bounds{
    UIColor *fromColor = HexColor(0x4D83FD);
    UIColor *toColor = HexColor(0x4D9BFD);
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
      gradientLayer.colors = @[(__bridge id)fromColor.CGColor, (__bridge id)toColor.CGColor];
      gradientLayer.locations = @[@(0),@(1.0f)];
      gradientLayer.startPoint = CGPointMake(0, 0);
      gradientLayer.endPoint = CGPointMake(1.0, 0);
      gradientLayer.frame = bounds;
      [self.layer addSublayer:gradientLayer];
}

- (void)gradientDisable:(CGRect)bounds
{
    UIColor *fromColor = HexColor(0xCCCCCC);
    UIColor *toColor = HexColor(0xCCCCCC);
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
      gradientLayer.colors = @[(__bridge id)fromColor.CGColor, (__bridge id)toColor.CGColor];
      gradientLayer.locations = @[@(0),@(1.0f)];
      gradientLayer.startPoint = CGPointMake(0, 0);
      gradientLayer.endPoint = CGPointMake(1.0, 0);
      gradientLayer.frame = bounds;
    [self.layer addSublayer:gradientLayer];
}

@end
