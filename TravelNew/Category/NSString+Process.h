//
//  NSString+Process.h
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Process)
-  (BOOL) isBlankString;
- (NSString *)cutOutWhiteSpace;
@end

NS_ASSUME_NONNULL_END
