//
//  AppDelegate.m
//  TravelNew
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
#import "WXApiObject.h"

#import "YTKNetworkConfig.h"
#import "YTKNetworkAgent.h"
#import<AMapFoundationKit/AMapFoundationKit.h>
#import <AlipaySDK/AlipaySDK.h>

#import "LKHomeCtrl.h"
#import "LKDemandCtrl.h"
#import "LKMessageCtrl.h"
#import "LKMineCtrl.h"
#import "LKMessageCtrl.h"
#import "EMConversationsViewController.h"
#import "GuideView.h"
#import "WXLoginShare.h"
#import "WechatManager.h"
#import "AppointmentHallCtrl.h"
#import "AppointmentHallTABCtrl.h"
#import "MessageModel.h"
#import <Bugly/Bugly.h>
#import <AuthenticationServices/AuthenticationServices.h>
#import "TradingHallCtrl.h"
#import "TradingHallDriverCtrl.h"

@interface AppDelegate ()<QCloudSignatureProvider,QCloudCredentailFenceQueueDelegate,WXApiDelegate,BuglyDelegate>
@property(nonatomic,strong) NSString *orderNumber;
// 一个脚手架实例
@property (nonatomic) QCloudCredentailFenceQueue* credentialFenceQueue;
@property (nonatomic, unsafe_unretained) UIBackgroundTaskIdentifier taskId;
@property (nonatomic,strong) UITabBarController *tabBarController;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions API_AVAILABLE(ios(13.0)){
    // Override point for customization after application launch.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acterChanged:) name:Notification_ActerChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessageReceive:) name:Notification_NewMessageReceive object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessageRead:) name:Notification_NewMessageRead object:nil];
    
    //服务端返回格式问题，后端返回的结果不是"application/json"，afn 的 jsonResponseSerializer 是不认的。这里做临时处理
    YTKNetworkAgent *agent = [YTKNetworkAgent sharedAgent];
    [agent setValue:[NSSet setWithObjects:@"application/json", @"text/plain", @"text/javascript", @"text/json",@"text/html", nil]
         forKeyPath:@"jsonResponseSerializer.acceptableContentTypes"];
#pragma mark -- 启用腾讯buggly
    [Bugly startWithAppId:@"8fd7066c5f"];
    
    [AMapServices sharedServices].apiKey =@"e37d849e854884d2421018e6a880181a";
    
    ///地图需要v4.5.0及以上版本才必须要打开此选项（v4.5.0以下版本，需要手动配置info.plist）
    [AMapServices sharedServices].enableHTTPS = YES;
    
    //设置网络请求 baseUrl
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    config.baseUrl = ApiBaseUrl;
    //config.baseUrl = @"";
    config.cdnUrl = @"https://fen.bi";
    
    //环信初始化
    // appkey替换成自己在环信管理后台注册应用中的appkey
    EMOptions *options = [EMOptions optionsWithAppkey:@"1125200724048027#lvkauxing"];
    // apnsCertName是证书名称，可以先传nil，等后期配置apns推送时在传入证书名称
    options.apnsCertName = nil;
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    // 微信支付注册
    // WeiXinPayKey：APP在微信开发者网站上申请的Key。WeiXinLinks：微信开发者Universal Link（这个有点麻烦，后面会详细说明，先把集成过程讲完）。
    BOOL isSuccess = [WXApi registerApp:@"wx51db81742889d0a0" universalLink:@"https://sxlkcx.cn/travel/"];
    if (isSuccess) {
        DLog(@"微信支付API注册成功");
    } else {
        DLog(@"微信支付API注册失败");
    }
    
    QCloudServiceConfiguration* configuration = [QCloudServiceConfiguration new];
    QCloudCOSXMLEndPoint* endpoint = [[QCloudCOSXMLEndPoint alloc] init];
    // 服务地域简称，例如广州地区是 ap-guangzhou
    endpoint.regionName = @"ap-nanjing";
    // 使用 HTTPS
    endpoint.useHTTPS = true;
    configuration.endpoint = endpoint;
    // 密钥提供者为自己
    configuration.signatureProvider = self;
    // 初始化 COS 服务示例
    [QCloudCOSXMLService registerDefaultCOSXMLWithConfiguration:configuration];
    [QCloudCOSTransferMangerService registerDefaultCOSTransferMangerWithConfiguration:
        configuration];

    // 初始化临时密钥脚手架
    self.credentialFenceQueue = [QCloudCredentailFenceQueue new];
    self.credentialFenceQueue.delegate = self;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    UITabBarController *tb = nil;
    //self.window.rootViewController = tb;
#pragma mark -- 苹果账号登录
    [[[ASAuthorizationAppleIDProvider alloc] init] getCredentialStateForUserID:[self getUserId] completion:^(ASAuthorizationAppleIDProviderCredentialState credentialState, NSError * _Nullable error) {
        // 查询当前账号是否在钥匙串中有记录了，不需要用户重复登录
        switch (credentialState) {
            case ASAuthorizationAppleIDProviderCredentialRevoked:
                NSLog(@"ASAuthorizationAppleIDProviderCredentialRevoked");
                break;
            case ASAuthorizationAppleIDProviderCredentialAuthorized:
                NSLog(@"ASAuthorizationAppleIDProviderCredentialAuthorized");
                break;
            case ASAuthorizationAppleIDProviderCredentialNotFound:
                NSLog(@"ASAuthorizationAppleIDProviderCredentialNotFound");
                break;
            case ASAuthorizationAppleIDProviderCredentialTransferred:
                NSLog(@"ASAuthorizationAppleIDProviderCredentialTransferred");
                break;
            default:
                break;
        }
    }];

    UserInfo *user = [UserInfo shareInstance];
    [user read];             //从文件读取用户信息
    DLog(@"my user info = %@",user);
    
    if([user isCurrentRecomander] && [user isRecomander] && user.isLogin){
        tb = [self setupTabbar2];
        self.tabBarController = tb;
        self.window.rootViewController = tb;
    }else{
        tb = [self setupTabbar];
        self.tabBarController = tb;
        self.window.rootViewController = tb;
    }
    
    if(user.isLogin){
        //登录环信
        [[EMClient sharedClient] loginWithUsername:user.easemobId password:user.password completion:^(NSString *aUsername, EMError *aError) {
            if (!aError) {
                DLog(@"环信登录成功");
            } else {
                DLog(@"登录失败的原因---%@", aError.errorDescription);
            }
        }];
    }
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        //第一次启动
        GuideView *vc = [[GuideView alloc]init];
        [vc showGuideViewWithImageArray:@[@"user_guide1",@"user_guide2",@"user_guide3"] WindowRootController:tb];
        self.window.rootViewController = vc;
    }else{
        //不是第一次启动了
    }
    [self.window makeKeyAndVisible];
    return YES;
}
#pragma mark -- 是获取保存在钥匙串中的苹果账户id userId
- (NSString *)getUserId {
    // 单用户登录，直接写死账号，只需要查到 userId 即可
    NSDictionary *query = @{
        (NSString *)kSecClass: (NSString *)kSecClassGenericPassword,
        (NSString *)kSecMatchLimit: (NSString *)kSecMatchLimitOne, // 只返回一个记录
        (NSString *)kSecAttrAccount: @"appAccount", // 账号写死
        (NSString *)kSecReturnData: @(YES), // 需要返回 Item 的 Data，数据保存在这里
        // 返回对象的类型不同
        // 值为 YES 时，attributes 和 data 放在同一个 CFDictionaryRef 中返回
        // 值为 NO 时，单独返回 data，放在 CFDataRef 中返回
        (NSString *)kSecReturnAttributes: @(YES),
    };
    CFTypeRef result;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&result);
    if (res == errSecSuccess) {
        NSDictionary *resDic = (__bridge_transfer NSDictionary *)result;
        NSData * data = resDic[(NSString *)kSecValueData];
        NSString *user = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        return user;
    } else {
        return nil;
    }
}



- (void)acterChanged:(NSNotification *)notification
{
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander] && [user isRecomander] && user.isLogin){
        UITabBarController *tabctrl = [self setupTabbar];
        tabctrl.selectedIndex = 4;
        self.tabBarController = tabctrl;
        self.window.rootViewController = tabctrl;
    }else{
        UITabBarController *tabctrl = [self setupTabbar2];
        tabctrl.selectedIndex = 4;
        self.tabBarController = tabctrl;
        self.window.rootViewController = tabctrl;
    }
}

- (void)newMessageReceive:(NSNotification *)notification
{
    [self.tabBarController.tabBar showBadgeOnItemIndex:2];
}

- (void)newMessageRead:(NSNotification *)notification
{
    [self.tabBarController.tabBar hideBadgeOnItemIndex:2];
}

- (UITabBarController *)setupTabbar{
    //a.初始化一个tabBar控制器
    UITabBarController *tabctrl = [[UITabBarController alloc] init];
    tabctrl.view.backgroundColor = [UIColor whiteColor];
    //b.创建子控制器
    LKHomeCtrl *c1=[LKHomeCtrl new];
    TradingHallCtrl *c2=[TradingHallCtrl new];
    EMConversationsViewController *c3 = [EMConversationsViewController new];
    CommunityCtrl *c4 = [CommunityCtrl new];
    LKMineCtrl *c5=[LKMineCtrl new];
    
    UINavigationController *nav1 = [[UINavigationController alloc] initWithRootViewController:c1];
    UINavigationController *nav2 = [[UINavigationController alloc] initWithRootViewController:c2];
    UINavigationController *nav3 = [[UINavigationController alloc] initWithRootViewController:c3];
    UINavigationController *nav4 = [[UINavigationController alloc] initWithRootViewController:c4];
    UINavigationController *nav5 = [[UINavigationController alloc] initWithRootViewController:c5];

    tabctrl.viewControllers=@[nav1,nav2,nav3,nav4,nav5];
    NSArray *titleArray = @[@"首页",@"需求",@"消息",@"社区",@"我的"];
    NSArray *imageArray = @[
                            @[@"home_icon", @"home_icon_sel"],
                            @[@"travel_icon", @"travel_icon_sel"],
                            @[@"message_unselected",@"message_selected"],
                            @[@"step_icon",@"step_icon_sel"],
                            @[@"mine_icon", @"mine_icon_sel"]
                            ];
    
    UITabBar *tabBar = tabctrl.tabBar;
    tabBar.backgroundImage = [UIImage imageWithColor:[UIColor whiteColor]];
    
    for (int i=0; i<tabBar.items.count; i++) {                      //设置tabbar项目
        UITabBarItem *tabBarItem = [tabBar.items objectAtIndex:i];
        [tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateSelected];
        [tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateSelected];
        
        NSArray *imgArray = [imageArray objectAtIndex:i];
        UIImage * selectImage = [UIImage imageNamed:[imgArray objectAtIndex:1]];
        UIImage * unSelectImage = [UIImage imageNamed:[imgArray objectAtIndex:0]];
        //适配在IOS8下，选中状态会变成默认蓝色的问题
        NSString *version = [UIDevice currentDevice].systemVersion;
        if (version.doubleValue >7.0) {
        selectImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        tabBarItem = [tabBarItem initWithTitle:[titleArray objectAtIndex:i] image:unSelectImage selectedImage:selectImage];
    }
    return tabctrl;
}

- (UITabBarController *)setupTabbar2{
    //a.初始化一个tabBar控制器
    UITabBarController *tabctrl = [[UITabBarController alloc] init];
    tabctrl.view.backgroundColor = [UIColor whiteColor];
    //b.创建子控制器
    LKHomeCtrl *c1=[LKHomeCtrl new];
    TradingHallDriverCtrl *c2=[TradingHallDriverCtrl new];
    EMConversationsViewController *c3 = [EMConversationsViewController new];
    CommunityCtrl *c4 = [CommunityCtrl new];
    LKMineCtrl *c5=[LKMineCtrl new];
    
    UINavigationController *nav1 = [[UINavigationController alloc] initWithRootViewController:c1];
    UINavigationController *nav2 = [[UINavigationController alloc] initWithRootViewController:c2];
    UINavigationController *nav3 = [[UINavigationController alloc] initWithRootViewController:c3];
    UINavigationController *nav4 = [[UINavigationController alloc] initWithRootViewController:c4];
    UINavigationController *nav5 = [[UINavigationController alloc] initWithRootViewController:c5];

    tabctrl.viewControllers=@[nav1,nav2,nav3,nav4,nav5];
    NSArray *titleArray = @[@"首页",@"接单",@"消息",@"社区",@"我的"];
    NSArray *imageArray = @[
                            @[@"home_icon", @"home_icon_sel"],
                            @[@"travel_icon", @"travel_icon_sel"],
                            @[@"message_unselected",@"message_selected"],
                            @[@"step_icon",@"step_icon_sel"],
                            @[@"mine_icon", @"mine_icon_sel"]
                            ];
    
    UITabBar *tabBar = tabctrl.tabBar;
    tabBar.backgroundImage = [UIImage imageWithColor:[UIColor whiteColor]];
    
    for (int i=0; i<tabBar.items.count; i++) {                      //设置tabbar项目
        UITabBarItem *tabBarItem = [tabBar.items objectAtIndex:i];
        [tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateSelected];
        [tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateSelected];
        
        NSArray *imgArray = [imageArray objectAtIndex:i];
        UIImage * selectImage = [UIImage imageNamed:[imgArray objectAtIndex:1]];
        UIImage * unSelectImage = [UIImage imageNamed:[imgArray objectAtIndex:0]];
        //适配在IOS8下，选中状态会变成默认蓝色的问题
        NSString *version = [UIDevice currentDevice].systemVersion;
        if (version.doubleValue >7.0) {
        selectImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        tabBarItem = [tabBarItem initWithTitle:[titleArray objectAtIndex:i] image:unSelectImage selectedImage:selectImage];
    }
    return tabctrl;
}




- (void) signatureWithFields:(QCloudSignatureFields*)fileds
                     request:(QCloudBizHTTPRequest*)request
                  urlRequest:(NSMutableURLRequest*)urlRequst
                   compelete:(QCloudHTTPAuthentationContinueBlock)continueBlock
{

    QCloudCredential* credential = [QCloudCredential new];
    credential.secretID = @"AKIDJOK63mSl4C7vXmKSRDizXprTBNQH3JQ2"; // 永久密钥 SecretId
    credential.secretKey = @"a6NTYOzKPEnzxLVUDj15vrhMow40hBVb"; // 永久密钥 SecretKey

    // 使用永久密钥计算签名
    QCloudAuthentationV5Creator* creator = [[QCloudAuthentationV5Creator alloc]
        initWithCredential:credential];
    QCloudSignature* signature = [creator signatureForData:urlRequst];
    continueBlock(signature, nil);
}

- (void)applicationDidEnterBackground:(UIApplication * )application
{
    NSLog(@"---进入后台----");
    [[EMClient sharedClient] applicationDidEnterBackground:application];
    
    self.taskId = [application beginBackgroundTaskWithExpirationHandler:^(void) {
        //当申请的后台时间用完的时候调用这个block
        //此时我们需要结束后台任务，
        [self endTask];
    }];
    // 模拟一个长时间的任务 Task
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(longTimeTask:) userInfo:nil repeats:YES];
}
 
- (void)applicationDidBecomeActive:(UIApplication * )application
{
    NSLog(@"---活跃状态----");
    [self endTask];
}
 
#pragma mark - 停止timer
-(void)endTask
{
    if (_timer != nil||
        _timer.isValid )
    {
        [_timer invalidate];
        _timer = nil;
        //结束后台任务
        [[UIApplication sharedApplication] endBackgroundTask:_taskId];
        _taskId = UIBackgroundTaskInvalid;
        NSLog(@"停止计时");
    }
}
 
- (void)longTimeTask:(NSTimer *)timer
{
    NSTimeInterval time = [[UIApplication sharedApplication] backgroundTimeRemaining];
    NSLog(@"系统留给的我们的时间 = %.02f Seconds", time);
}



// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    [WXApi handleOpenURL:url delegate:self];
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler{
    return [WXApi handleOpenUniversalLink:userActivity delegate:(id)[WXLoginShare shareInstance]];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            DLog(@"result = %@",resultDic);
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            DLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            DLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }else if([url.host isEqualToString:@"pay"]){
        //处理微信的支付结果
        return [WechatManager handleOpenUrl:url];
    }else if ([url.host isEqualToString:@"oauth"]){ //微信登录
        return [WXLoginShare handleOpenUrl:url];
    }
    return YES;
}
#pragma mark - 支付宝支付、授权回调
// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            DLog(@"result = %@",resultDic);
            NSString *status = [resultDic objectForKey:@"resultStatus"];
            NSString *msg = [resultDic objectForKey:@"memo"];
            if([status isEqualToString:@"9000"]){
                //发送支付成功通知
                [[NSNotificationCenter defaultCenter] postNotificationName:Notification_PayIsFinished object:nil];
            }else if([status isEqualToString:@"6001"]){
                [MBProgressHUD showMessage:msg];
            }
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            DLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            DLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }else if ([url.host isEqualToString:@"pay"]) {
        // 处理微信的支付结果
        return [WechatManager handleOpenUrl:url];
    }else if ([url.host isEqualToString:@"oauth"]){ //微信登录
        return [WXLoginShare handleOpenUrl:url];
    }
    return YES;
}


-(void)onReq:(BaseReq *)req
 {
     DLog(@"发送微信授权请求");
}
// 从微信分享过后点击返回应用的时候调用
- (void)onResp:(BaseResp *)resp
{
    DLog(@"从微信分享过后点击返回应用的时候调用");
}

#pragma mark -支付完成后在后台查询后台支付结果，
- (void)checkOrderPay:(NSString *)orderNumber ofType:(NSInteger)type{
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"user/notify"
                          para:@{@"indentnum":orderNumber,
                                 @"user_id":user.userId,
                                 @"type":[NSNumber numberWithInteger:type]
                                 }
                description:@"无效api"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *json = request.responseString;
        DLog(@"json = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"status"] integerValue] == 1){
//            [MBProgressHUD showMessage:[dict objectForKey:@"message"]];
//             UINavigationController *nav = self.tabbarController.selectedViewController;
//            [nav popToRootViewControllerAnimated:YES];
//            self.tabbarController.selectedIndex = 2;            //切换到订单页面
        }else{
            [MBProgressHUD showError: [dict objectForKey:@"message"]];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application{
    //退出环信登录
    [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
        if (!aError) {
            DLog(@"退出登录成功");
        } else {
            DLog(@"退出登录失败的原因---%@", aError.errorDescription);
        }
    }];
    if(self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}



@end
