//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "MessageModel.h"


@interface UserInfo : NSObject<NSCoding>

@property (nonatomic,copy) NSString *userLabel;
@property (nonatomic,copy) NSString *referralCode;
@property (nonatomic,strong) NSNumber *creatTime;
@property (nonatomic,copy) NSString *aliUserid;
@property (nonatomic,strong) NSNumber *sex;
@property (nonatomic,strong) NSNumber *birthTime;
@property (nonatomic,assign) BOOL isRealname;
@property (nonatomic,copy) NSString *job;
@property (nonatomic,copy) NSString *sign;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,strong) NSNumber *userStyle;
@property (nonatomic,strong) NSNumber *currentUserStyle;
@property (nonatomic,copy) NSString *referralUserId;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,copy) NSString *idNumber;
@property (nonatomic,copy) NSString *unionid;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *password;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *bgImage;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,copy) NSString *city;

@property (nonatomic,strong) NSString *selectCityId;        //选择的城市id
@property (nonatomic,strong) NSString *selectCity;          //选择的城市名

@property (nonatomic,strong) NSString *token;      //用户token
@property (nonatomic,assign) BOOL isLogin;         //用户是否已登录
@property (nonatomic,strong) MessageModel *model;


-(id)initWithJson:(NSDictionary *)json;

+(instancetype) shareInstance;          //获取单例
- (void)save;                           //保存到文件
- (void)read;                           //从文件读取到单例
- (void)setLogin:(BOOL)isLogin;         //设置登录状态并保存到文件中
- (BOOL)isRecomander;                   //判断用户是否通过了推荐官认证
- (BOOL)isCurrentRecomander;            //判断用户当前是否切换到推荐官模式。
- (void)switchCurrentStyle;             //用户模式和推荐官模式切换

@end
