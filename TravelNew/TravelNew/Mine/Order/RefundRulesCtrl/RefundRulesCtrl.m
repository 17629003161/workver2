//
//  RefundRulesCtrl.m
//  TravelNew
//
//  Created by mac on 2021/1/8.
//  Copyright © 2021 lester. All rights reserved.
//

#import "RefundRulesCtrl.h"

@interface RefundRulesCtrl ()

@property(nonatomic,strong) UILabel *contentLabel;

@end

@implementation RefundRulesCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setTitle:@"退款规则" titleColor:nil];
    [self layout];
    self.contentLabel.text = @"\"旅咖出行\"提醒您：在使用订单退订前，请您务必仔细阅读并透彻理解本声明。您在取消订单时将被视为对本声明全部内容的认可。\n\n\n\
    一、预订后，已超出24小时订单退订？\n\
    1.使用日期前7天之前，无责取消；\n\
    2.使用日期前7（含）-1（含）天，扣除30%；\n\
    3.使用日期前1天以后，扣除60%；\n\n\n\
    二、24小时内预订的订单退订？\n\
    1.付款30分钟内，无责取消；\n\
    2.付款30分钟（含）-1（含）小时内，扣除10%；\n\
    3.付款1小时-3小时（含）内，扣除30%；\n\
    4.付款3小时-5小时（含）内，扣除60%；\n\
    5.付款5小时后，扣除80%；\n\n\
    针对所有订单退订，离行程开始前1小时，扣除100%。";
    [self.contentLabel changeStrings:@[@"一、预订后，已超出24小时订单退订？",@"二、24小时内预订的订单退订？"] toColor:HexColor(0x222222)];
    [self.contentLabel changeStrings:@[@"针对所有订单退订，离行程开始前1小时，扣除100%。"] toColor:HexColor(0xFF4141)];
    [self.contentLabel updateLayout];
}

- (void)layout{
    
    UILabel *label = [Utils createLabelWithTitle:@"" titleColor:HexColor(333333) fontSize:15];
    label.numberOfLines = 0;
    self.contentLabel = label;
    [self.view addSubview:label];
    label.text = @"";
    label.sd_layout
    .topSpaceToView(self.view, 13)
    .leftSpaceToView(self.view, 18)
    .rightSpaceToView(self.view, 18)
    .autoHeightRatio(0);
    
}


@end
