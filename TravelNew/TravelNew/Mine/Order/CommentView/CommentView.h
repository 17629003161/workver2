//
//  CommentView.h
//  TravelNew
//
//  Created by mac on 2020/10/15.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentView : UIView
@property (nonatomic, strong) OrderModel *model;
@property (nonatomic, assign) CGFloat score;
@property(nonatomic,copy) void(^commentFinishBlock)(void);
- (void)show;
- (void)hide;
@end

NS_ASSUME_NONNULL_END
