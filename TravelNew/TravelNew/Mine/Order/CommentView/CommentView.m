//
//  CommentView.m
//  TravelNew
//
//  Created by mac on 2020/10/15.
//  Copyright © 2020 lester. All rights reserved.
//

#import "CommentView.h"
#import "EMTextView.h"


@interface CommentView()<UITextViewDelegate>

@property (nonatomic,strong) EMTextView *textView;
@property (nonatomic,strong) NSArray *titleArray;
@property (nonatomic,strong) NSArray *titleArray2;
@property (nonatomic,strong) NSMutableArray *starArray;
@property (nonatomic,strong) NSMutableArray *selectArray;
@property (nonatomic,strong) UIView *maskView;
@end

@implementation CommentView

// 步骤 1：重写initWithFrame:方法，创建子控件并 - 添加
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self layout];
    }
    return self;
}

- (void)setScore:(CGFloat)score
{
    if(score<0){
        score = 0;
    }else if(score>5){
        score = 5;
    }
    _score = score;
    for(int i=0;i<_starArray.count;i++){
        UIImageView *imgv = _starArray[i];
        if(imgv.tag <= score){
            imgv.image = [UIImage imageNamed:@"Shape_star_orange"];
        }else{
            imgv.image = [UIImage imageNamed:@"Shape_star_gray"];
        }
    }
    for (int i=0; i<self.selectArray.count; i++) {
        UIButton *btn = self.selectArray[i];
        [btn setSelected:NO];
        btn.layer.borderColor = HexColor(0xbfbfbf).CGColor;
        if(self.score<3)
           [btn setTitle:self.titleArray[i] forState:UIControlStateNormal];
        else
            [btn setTitle:self.titleArray2[i] forState:UIControlStateNormal];
    }
}

- (NSArray *)titleArray{
    if(!_titleArray){
        _titleArray = @[@"不专业，回复速度慢",@"体验差，服务态度差",@"乱收费，行程不合理",@"不负责，接送不准时"];
    }
    return _titleArray;
}

- (NSArray *)titleArray2{
    if(!_titleArray2){
        _titleArray2 = @[@"服务热情，讲解到位",@"车辆干净，准时接送",@"价格公道，无购物",@"回复及时，体验赞"];
    }
    return _titleArray2;
}

- (UIView *)maskView{
    if(!_maskView){
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.6;
    }
    return _maskView;
}

- (void)selectIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:!btn.isSelected];
    if(btn.isSelected){
        btn.layer.borderColor = HexColor(0xFFAD4A).CGColor;
    }else{
        btn.layer.borderColor = HexColor(0xbfbfbf).CGColor;
    }
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [_textView resignFirstResponder];
}

- (void)closeIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)layout{
    [self addTapGestureTarget:self action:@selector(viewIsTyped:)];
    self.frame = CGRectMake((kScreenWidth-335)/2.0, (kScreenHeight-440)/2.0, 335, 440);
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 5;
    self.layer.shadowColor = [UIColor colorWithRed:237/255.0 green:241/255.0 blue:247/255.0 alpha:0.4].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 6;
    
    UILabel *tipL = [Utils createBoldLabelWithTitle:@"请您对此次出行评价" titleColor:HexColor(0x303030) fontSize:20];
    UIView *starView = [self createStarView];
    
    UIImageView *closeImgv = ImageViewWithImage(@"icon_clear");
    [closeImgv addTapGestureTarget:self action:@selector(closeIsClicked:)];
    
    _selectArray = [NSMutableArray new];
    for(int i=0;i<4;i++){
        UIButton *btn = [self createButton];
        [btn setTitle:self.titleArray[i] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(selectIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_selectArray addObject:btn];
    }
    UILabel *select0L = [self.selectArray objectAtIndex:0];
    UILabel *select1L = [self.selectArray objectAtIndex:1];
    UILabel *select2L = [self.selectArray objectAtIndex:2];
    UILabel *select3L = [self.selectArray objectAtIndex:3];
    
    
    EMTextView *textView = [[EMTextView alloc] init];
    textView.backgroundColor = HexColor(0xF9F9F9);
    textView.placeholder = @"请输入...";
    textView.font = [UIFont systemFontOfSize:14];
    textView.returnKeyType = UIReturnKeySend;
    self.textView = textView;
    
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认提交" font:LKSystemFont(12) cornerRadius:20];
    [btn addTapGestureTarget:self action:@selector(btnSubmitIsClicked:)];
    [self sd_addSubviews:@[tipL,closeImgv,
                           starView,
                           select0L,select1L,
                           select2L,select3L,
                           textView,
                           btn]];
    tipL.sd_layout
    .topSpaceToView(self, 25)
    .leftSpaceToView(self, 20)
    .autoHeightRatio(0);
    [tipL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    closeImgv.sd_layout
    .topSpaceToView(self, 20)
    .rightSpaceToView(self, 20)
    .widthIs(16)
    .heightIs(16);
    
    starView.sd_layout
    .topSpaceToView(tipL, 30)
    .centerXEqualToView(self)
    .widthIs(227)
    .heightIs(30);
    
    select0L.sd_layout
    .leftEqualToView(tipL)
    .topSpaceToView(starView, 30)
    .widthIs(144)
    .heightIs(32);
    
    select1L.sd_layout
    .centerYEqualToView(select0L)
    .leftSpaceToView(select0L, 18)
    .widthIs(144)
    .heightIs(32);
    
    select2L.sd_layout
    .leftEqualToView(tipL)
    .topSpaceToView(select0L, 12)
    .widthIs(144)
    .heightIs(32);
    
    select3L.sd_layout
    .centerYEqualToView(select2L)
    .leftSpaceToView(select2L, 18)
    .widthIs(144)
    .heightIs(32);
    
    textView.sd_layout
    .topSpaceToView(select3L, 31)
    .leftSpaceToView(self, 16)
    .rightSpaceToView(self, 16)
    .heightIs(82);
    
    btn.sd_layout
    .topSpaceToView(textView, 38)
    .centerXEqualToView(self)
    .widthIs(200)
    .heightIs(40);
}

- (void)btnSubmitIsClicked:(id)sender
{
    NSMutableString *tags = [NSMutableString new];
    for(UIButton *btn in self.selectArray){
        if(btn.isSelected){
            [tags appendFormat:@"_%@",btn.titleLabel.text];
        }
    }
    NSString *content = self.textView.text;
    if(content.length==0){
        [MBProgressHUD showMessage:@"请输入评论"];
        return;
    }
    Api *api = [Api apiPostUrl:@"work/requirementComment/add"
                          para:@{@"ltRequirementInfoId":self.model.ltRequirementInfoId,
                                             @"saywhat":content,
                                               @"score":@(self.score),
                                                 @"teg":tags}
                   description:@"work-添加评价"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            if(self.commentFinishBlock){
                self.commentFinishBlock();
            }
            [self hide];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];

}

- (void)starIsClicked:(UITapGestureRecognizer *)reg
{
    UIView *view = reg.view;
    NSInteger tag = view.tag;
    [self setScore:tag];
}

- (UIButton *)createButton{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(13);
    [btn setTitleColor:HexColor(0x666666) forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0xFFAD4A) forState:UIControlStateSelected];
    [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xFFF9F2)] forState:UIControlStateSelected];
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = HexColor(0xbfbfbf).CGColor;
//    btn.layer.borderColor = HexColor(0xFFAD4A).CGColor;
    return btn;
}


- (UIView *)createStarView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    _starArray = [NSMutableArray new];
    for(int i=0;i<5;i++){
        UIImageView *star = ImageViewWithImage(@"Shape_star_gray");
        star.tag = i+1;
        //UIImageView *star = ImageViewWithImage(@"Shape_star_orange");
        [star addTapGestureTarget:self action:@selector(starIsClicked:)];
        [_starArray addObject:star];
    }
    [view sd_addSubviews:_starArray];
    UIImageView *star0 = _starArray[0];
    UIImageView *star1 = _starArray[1];
    UIImageView *star2 = _starArray[2];
    UIImageView *star3 = _starArray[3];
    UIImageView *star4 = _starArray[4];

    star0.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .widthIs(30)
    .heightIs(30);
    
    star1.sd_layout
    .topEqualToView(star0)
    .leftSpaceToView(star0, 19)
    .widthIs(30)
    .heightIs(30);
    
    star2.sd_layout
    .topEqualToView(star0)
    .leftSpaceToView(star1, 19)
    .widthIs(30)
    .heightIs(30);
    
    star3.sd_layout
    .topEqualToView(star0)
    .leftSpaceToView(star2, 19)
    .widthIs(30)
    .heightIs(30);
    
    star4.sd_layout
    .topEqualToView(star0)
    .leftSpaceToView(star3, 19)
    .widthIs(30)
    .heightIs(30);
    [view setupAutoHeightWithBottomView:star0 bottomMargin:0];
    return view;
}


// 步骤 2：重写layoutSubviews，子控件设置frame
- (void)layoutSubviews {
    [super layoutSubviews];
}
// 步骤 4： 子控件赋值
- (void)setModel:(OrderModel *)model {
    
    _model = model;
   // self.lable.text = model.name;
}

- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self.maskView];
    [keyWindow addSubview:self];
}

- (void)hide{
    [self removeFromSuperview];
    [self.maskView removeFromSuperview];
}

@end
