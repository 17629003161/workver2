//
//  ContactOrderCell.m
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ContactOrderCell.h"

@interface ContactOrderCell()
@property(nonatomic,strong) UILabel *orderNumberLabel;
@property(nonatomic,strong) UILabel *orderDateLabel;
@property(nonatomic,strong) UILabel *orderPriceLabel;
@property(nonatomic,strong) UILabel *recomanderPhoneLabel;
@property(nonatomic,strong) UILabel *recomanderWXNumberLabel;

@property(nonatomic,strong) UILabel *tipLabel;

@end


@implementation ContactOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(OrderModel *)model
{
    UserInfo *user = [UserInfo shareInstance];
    _model = model;
    self.orderNumberLabel.text = model.orderNo;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.creationTime doubleValue]/1000.0;
    self.orderDateLabel.text = date.dateString;
    self.orderPriceLabel.text = [NSString stringWithFormat:@"%.2lf元",[model.money doubleValue]/100.0];
    if(model.phone.length>10){
        NSString *phone = [model.phone  stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        self.recomanderPhoneLabel.text = [NSString stringWithFormat:@"推荐官手机号:%@",phone];
    }
    self.recomanderWXNumberLabel.text = [NSString stringWithFormat:@"推荐官微信号:%@",model.wxId];
    if([self.model.subState integerValue]==OrderSubStateUserIsCommented){
        self.rightButton.hidden = YES;
        self.tipLabel.text = @"订单已评价";
    }else{
        if([user isCurrentRecomander]){
            self.rightButton.hidden = YES;
        }else{
            self.rightButton.hidden = NO;
            self.rightButton.titleLabel.text = @"评价";
        }
        self.tipLabel.text = @"订单未评价";
    }
    [self.orderNumberLabel updateLayout];
    [self.orderPriceLabel updateLayout];
    [self.orderPriceLabel updateLayout];
    [self.recomanderPhoneLabel updateLayout];
    [self.recomanderWXNumberLabel updateLayout];
}

- (void)buttonIsClicked:(UITapGestureRecognizer *)reg
{
    LKGadentButton *btn = (LKGadentButton *)reg.view;
    if(self.functionBlock){
        self.functionBlock(self,self.model,btn.titleLabel.text);
    }
}

- (void)configViews{
    self.backgroundColor = HexColor(0xf9f9f9);
    UIView *view = [UIView new];
    view.layer.borderColor = HexColor(0xD8D7DF).CGColor;
    view.layer.borderWidth = 0.5;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 4.0;
    
    UILabel *labelL2 = [Utils createLabelWithTitle:@"订单编号"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR2 = [Utils createLabelWithTitle:@"100000000001"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.orderNumberLabel = labelR2;
    UILabel *labelL3 = [Utils createLabelWithTitle:@"订单日期"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR3 = [Utils createLabelWithTitle:@"2020年7月5日"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.orderDateLabel = labelR3;
    
    UILabel *labelL4 = [Utils createLabelWithTitle:@"订单金额"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR4 = [Utils createLabelWithTitle:@"5.00元"
                                        titleColor:HexColor(0xFF9739)
                                          fontSize:14];
    self.orderPriceLabel = labelR4;
    
    UILabel *phoneLabel = [Utils createBoldLabelWithTitle:@"推荐官手机号：131***33934"
                                         titleColor:HexColor(0x444444)
                                           fontSize:14];
    self.recomanderPhoneLabel = phoneLabel;
    UILabel *wxLabel = [Utils createBoldLabelWithTitle:@"推荐官微信号：XS9230"
                                            titleColor:HexColor(0x444444)
                                              fontSize:14];
    self.recomanderWXNumberLabel = wxLabel;
    UIView *bgview = [UIView new];
    bgview.backgroundColor = HexColor(0xf8f9fb);
    [bgview sd_addSubviews:@[phoneLabel,
                             wxLabel]];
    
    UILabel *tipLabel = [Utils createLabelWithTitle:@"订单已完成" titleColor:HexColor(0x999999) fontSize:14];
    self.tipLabel = tipLabel;
    LKGadentButton *btn2 = [LKGadentButton blueButtonWhithTitle:@"评价" font:LKSystemFont(16)
                                    cornerRadius:4];
    self.rightButton = btn2;
    [btn2 addTapGestureTarget:self action:@selector(buttonIsClicked:)];
    [self.contentView addSubview:view];
    [view sd_addSubviews:@[
                             labelL2,labelR2,
                             labelL3,labelR3,
                             labelL4,labelR4,
                             bgview,
                             tipLabel,btn2]];
    labelL2.sd_layout
    .topSpaceToView(view, 21)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR2.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelR2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL3.sd_layout
    .topSpaceToView(labelL2, 12)
    .leftEqualToView(labelL2)
    .autoHeightRatio(0);
    [labelL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR3.sd_layout
    .centerYEqualToView(labelL3)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelR3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL4.sd_layout
    .topSpaceToView(labelL3, 12)
    .leftEqualToView(labelL2)
    .autoHeightRatio(0);
    [labelL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR4.sd_layout
    .centerYEqualToView(labelL4)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelR4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneLabel.sd_layout
    .topSpaceToView(bgview, 12)
    .leftSpaceToView(bgview, 10)
    .autoHeightRatio(0);
    [phoneLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    wxLabel.sd_layout
    .topSpaceToView(phoneLabel, 2)
    .leftEqualToView(phoneLabel)
    .autoHeightRatio(0);
    [wxLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgview.sd_layout
    .topSpaceToView(labelL4, 11)
    .leftSpaceToView(view, 16)
    .rightSpaceToView(view, 16);
    [bgview setupAutoHeightWithBottomView:wxLabel bottomMargin:12];
    
    tipLabel.sd_layout
    .topSpaceToView(bgview, 20)
    .leftEqualToView(bgview)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn2.sd_layout
    .centerYEqualToView(tipLabel)
    .rightSpaceToView(view, 16)
    .widthIs(144)
    .heightIs(36);
    
    [self.contentView addSubview:view];
    view.sd_layout
    .topEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15);
    [view setupAutoHeightWithBottomView:btn2 bottomMargin:16];
    [self setupAutoHeightWithBottomView:view bottomMargin:0];

}
@end
