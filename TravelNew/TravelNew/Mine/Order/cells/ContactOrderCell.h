//
//  ContactOrderCell.h
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "OrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactOrderCell : BaseCustomTableViewCell
@property(nonatomic,strong) OrderModel *model;
@property(nonatomic,strong) LKGadentButton *rightButton;
@property(nonatomic,copy) void(^functionBlock)(ContactOrderCell *cell, OrderModel *model, NSString *btnTitle);
@end

NS_ASSUME_NONNULL_END
