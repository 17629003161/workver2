//
//  OrderCell.h
//  TravelNew
//
//  Created by mac on 2020/10/14.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "OrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderCell : BaseCustomTableViewCell
@property(nonatomic,strong) OrderModel *model;
@property(nonatomic,copy) void(^functionBlock)(OrderCell *cell, OrderModel *model, NSString *btnTitle);
@property(nonatomic,copy) void(^navigationBlock)(OrderModel *);
@end

NS_ASSUME_NONNULL_END
