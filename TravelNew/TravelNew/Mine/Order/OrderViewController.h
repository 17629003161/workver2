//
//  OrderViewController.h
//  TravelNew
//
//  Created by mac on 2020/10/14.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "OrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderViewController : LKBaseTableViewController
@property(nonatomic,assign) OrderState orderSate;
@end

NS_ASSUME_NONNULL_END
