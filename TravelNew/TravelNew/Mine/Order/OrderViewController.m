//
//  OrderViewController.m
//  TravelNew
//
//  Created by mac on 2020/10/14.
//  Copyright © 2020 lester. All rights reserved.
//

#import "OrderViewController.h"
#import "OrderCell.h"
#import "ContactOrderCell.h"
#import "OrderDetailCtrl.h"
#import "CommentView.h"
#import "EMChatViewController.h"
#import "LKMessageCtrl.h"
#import "EMConversationsViewController.h"
#import "LKRefundDialog.h"
#import "RefundRulesCtrl.h"
#import "ContactOrderDetailCtrl.h"
#import "DownwindTripDetailCtrl.h"
#import "DownwindTripDetailDriverCtrl.h"

@interface OrderViewController ()
@property(nonatomic,strong) NSMutableArray *menuArray;
@property(nonatomic,strong) NSArray *menuStatesArray;
@property(nonatomic,assign) NSInteger menuIndex;
@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的订单" titleColor:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    self.menuStatesArray = @[@(OrderStateWaitTrip),
                     @(OrderStateTriping),
                     @(OrderStateTripFinished)];
    [self viewLayout];
    [self checkBlankView];
    [self setMenuIndex:[self orderStateToMenuIndex:self.orderSate]];
}

- (NSInteger)orderStateToMenuIndex:(OrderState)orderState
{
    for(int i=0; i<self.menuStatesArray.count;i++)
    {
        OrderState state = [self.menuStatesArray[i] integerValue];
        if(state == orderState)
            return i;
    }
    return 0;
}


- (void)viewLayout{
    [self.tableView registerClass:[OrderCell class] forCellReuseIdentifier:@"OrderCell"];
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    [self checkBlankView];

    UIView *menubar = [self createMenuBar];
    [self.view addSubview:menubar];
    menubar.frame = CGRectMake(0, SAFEVIEWTOPMARGIN, kScreenWidth, 46);
    self.tableView.sd_layout
    .topSpaceToView(menubar, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomSpaceToView(self.view, SAFEVIEWBOTTOMMARGIN);
}



- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/requirement/orderList"
                          para:@{
                              @"pageIndex":@(self.pageIndex),
                               @"pageSize":@(self.pageSize),
                              @"state": @(self.orderSate),
                          }
                   description:@"work-查询订单列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(OrderModel.class);
    return name;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 213;
//    OrderModel *model = [self.dataArray objectAtIndex:indexPath.row];
//    return [tableView cellHeightForIndexPath:indexPath
//                                           model:model
//                                         keyPath:@"model"
//                                       cellClass:[OrderCell class]
//                                contentViewWidth:kScreenWidth];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //OrderModel *model = [self.dataArray objectAtIndex:indexPath.row];
    LKWeakSelf
    OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell"];
//    cell.model = model;
//    cell.functionBlock = ^(OrderCell * _Nonnull cell, OrderModel * _Nonnull model, NSString * _Nonnull btnTitle){
//        [weakSelf doCellAction:cell model:model buttonTitle:btnTitle];
//    };
//    cell.navigationBlock = ^(OrderModel * _Nonnull model) {
//        LKWeakSelf
//        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
//        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
//        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
//        [weakSelf doNavigationWithEndLocation:endLocation];
//    };
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // OrderModel *model = [self.dataArray objectAtIndex:indexPath.row];
   // NSInteger orderState = [model.state integerValue];
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander]){
        static int k = 0;
        DownwindTripDetailDriverCtrl *vc = [DownwindTripDetailDriverCtrl new];
        vc.tripStep = k;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        k++;
        if(k>TripStepWaitCustomBesure){
            k=0;
        }
    }else{
        static int i=0;
        DownwindTripDetailCtrl *vc = [DownwindTripDetailCtrl new];
        vc.tripStep = i;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        i++;
        if(i>TripStepPay2){
            i=0;
        }
    }
    
}

- (double)calcDistance:(OrderModel *)model{
    LKLocationManager *manger = [LKLocationManager sharedManager];
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[model.latitude doubleValue] longitude:[model.longitude doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:manger.coordinate.latitude longitude:manger.coordinate.longitude];
    double dist = [Utils calcDistanceFromLocation:location1 toLocation:location2];
    return dist;
}

- (void)doContactCellAction:(ContactOrderCell *)cell  model:(OrderModel *)model buttonTitle:(NSString *)btnTitle{
    UserInfo *user = [UserInfo shareInstance];
    if(![user isCurrentRecomander]){
        if([btnTitle isEqualToString:@"评价"]){
            //"评价";
            NSInteger subState = [model.subState integerValue];
            if(subState==OrderSubStateIsNotCommentedNotRemind || subState == OrderSubStateUserIsNotCommented){
                CommentView *dlg = [CommentView new];
                dlg.score = 3;
                dlg.model = model;
                dlg.commentFinishBlock = ^{
                    model.subState = @(OrderSubStateUserIsCommented);
                    cell.model = model;
                };
                [dlg show];
            }
        }
    }
}

- (void)doCellAction:(OrderCell *)cell  model:(OrderModel *)model buttonTitle:(NSString *)btnTitle{

}


//订单行为
- (void)orderAction:(OrderCell *)cell model:(OrderModel *)model buttonTitle:(NSString *)btnTitle{
}
//
////获取退款金额
- (void)requestRefundMoney:(OrderCell *)cell model:(OrderModel *)model{

}

- (void)showRefundDialog:(OrderCell *)cell model:(OrderModel *)model refund:(double)refund offMoney:(double)offMoney
{
    //显示退款对话框
    LKRefundDialog *dlg = [LKRefundDialog new];
    dlg.refund = refund;
    dlg.offMoney = offMoney;
    dlg.frame = CGRectMake(0, 0, 270, 450);
    dlg.okBlock = ^(NSString * _Nonnull resion) {
        //订单退款
        [self cancleOrder:cell model:model reasion:resion];
    };
    dlg.refundRulesBlock = ^{
        RefundRulesCtrl *vc = [RefundRulesCtrl new];
        [self.navigationController pushViewController:vc animated:YES];
    };
    [dlg show];
}

- (void)cancleOrder:(OrderCell *)cell model:(OrderModel *)model reasion:(NSString *)reasion
{

}


// 设置一排固定间距自动宽度子view
- (UIView *)createMenuBar
{
    _menuArray = [NSMutableArray new];
    UIView *containerView = [UIView new];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.borderColor = HexColor(0xe6e6e6).CGColor;
    containerView.layer.borderWidth =0.5;
    CGFloat gap = 30;
    CGFloat with = (kScreenWidth-60)/3.0;
    NSArray *titleAry = @[@"待出行",@"进行中",@"已完成"];
    for (int i = 0; i < titleAry.count; i++) {
        UILabel *label = [Utils createLabelWithTitle:titleAry[i]
                                           titleColor:HexColor(0x5e5e71)
                                             fontSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = i;
        [label addTapGestureTarget:self action:@selector(menuIsClicked:)];
        [_menuArray addObject:label];
        [containerView addSubview:label];
        label.sd_layout
        .centerYEqualToView(containerView)
        .leftSpaceToView(containerView, gap+i*with)
        .widthIs(with)
        .heightRatioToView(containerView, 0.8);
    }
    NSInteger index = [self orderStateToMenuIndex:self.orderSate];
    UILabel *label0 = _menuArray[index];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0x1E69FF);
    [containerView addSubview:line];
    line.sd_layout
    .centerXEqualToView(label0)
    .bottomEqualToView(containerView)
    .widthIs(56)
    .heightIs(2);
    [_menuArray addObject:line];
    return containerView;
}

- (void)menuIsClicked:(UITapGestureRecognizer *)reg{
    UILabel *label = (UILabel *)reg.view;
    DLog(@"selected = %ld",label.tag);
    [self setMenuIndex:label.tag];
    return;
}

- (void)setMenuIndex:(NSInteger)menuIndex
{
    _menuIndex = menuIndex;
    UILabel *selectLabel = nil;
    UIView *line = [_menuArray lastObject];
    for(int i=0;i<_menuArray.count-1;i++){
        UILabel *temp = _menuArray[i];
        if(i == menuIndex){
            selectLabel = temp;
            temp.textColor = HexColor(0x36364D);
            [UIView animateWithDuration:0.1 animations:^{
                    line.centerX = temp.centerX;
            }];
        }else{
            temp.textColor = HexColor(0x5e5e71);
        }
    }
    DLog(@"selected = %ld",selectLabel.tag);
    self.orderSate = [self.menuStatesArray[selectLabel.tag] integerValue];
    [self loadFirstPage];
}

@end
