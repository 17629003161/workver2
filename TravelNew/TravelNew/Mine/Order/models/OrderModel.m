//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "OrderModel.h"

@implementation OrderModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.money  = [json objectForKey:@"money"];
		self.referrerUserEasemobId  = [json objectForKey:@"referrerUserEasemobId"];
		self.nickname  = [json objectForKey:@"nickname"];
		self.serviceCount  = [json objectForKey:@"serviceCount"];
		self.referrerUserNickname  = [json objectForKey:@"referrerUserNickname"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.endTime  = [json objectForKey:@"endTime"];
		self.creationTime  = [json objectForKey:@"creationTime"];
		self.ltRequirementInfoId  = [json objectForKey:@"ltRequirementInfoId"];
		self.type  = [json objectForKey:@"type"];
		self.state  = [json objectForKey:@"state"];
		self.number  = [json objectForKey:@"number"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.orderNo  = [json objectForKey:@"orderNo"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.phone  = [json objectForKey:@"phone"];
		self.travelTime  = [json objectForKey:@"travelTime"];
		self.referrerUserId  = [json objectForKey:@"referrerUserId"];
		self.referrerUserHeaderImage  = [json objectForKey:@"referrerUserHeaderImage"];
		self.wxId  = [json objectForKey:@"wxId"];
		self.subState  = [json objectForKey:@"subState"];
		self.requirementDescription  = [json objectForKey:@"requirementDescription"];
		self.easemobId  = [json objectForKey:@"easemobId"];
		self.address  = [json objectForKey:@"address"];
		self.userId  = [json objectForKey:@"userId"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.money forKey:@"zx_money"];
	[aCoder encodeObject:self.referrerUserEasemobId forKey:@"zx_referrerUserEasemobId"];
	[aCoder encodeObject:self.nickname forKey:@"zx_nickname"];
	[aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
	[aCoder encodeObject:self.referrerUserNickname forKey:@"zx_referrerUserNickname"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.endTime forKey:@"zx_endTime"];
	[aCoder encodeObject:self.creationTime forKey:@"zx_creationTime"];
	[aCoder encodeObject:self.ltRequirementInfoId forKey:@"zx_ltRequirementInfoId"];
	[aCoder encodeObject:self.type forKey:@"zx_type"];
	[aCoder encodeObject:self.state forKey:@"zx_state"];
	[aCoder encodeObject:self.number forKey:@"zx_number"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.orderNo forKey:@"zx_orderNo"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.phone forKey:@"zx_phone"];
	[aCoder encodeObject:self.travelTime forKey:@"zx_travelTime"];
	[aCoder encodeObject:self.referrerUserId forKey:@"zx_referrerUserId"];
	[aCoder encodeObject:self.referrerUserHeaderImage forKey:@"zx_referrerUserHeaderImage"];
	[aCoder encodeObject:self.wxId forKey:@"zx_wxId"];
	[aCoder encodeObject:self.subState forKey:@"zx_subState"];
	[aCoder encodeObject:self.requirementDescription forKey:@"zx_requirementDescription"];
	[aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.money = [aDecoder decodeObjectForKey:@"zx_money"];
		self.referrerUserEasemobId = [aDecoder decodeObjectForKey:@"zx_referrerUserEasemobId"];
		self.nickname = [aDecoder decodeObjectForKey:@"zx_nickname"];
		self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
		self.referrerUserNickname = [aDecoder decodeObjectForKey:@"zx_referrerUserNickname"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.endTime = [aDecoder decodeObjectForKey:@"zx_endTime"];
		self.creationTime = [aDecoder decodeObjectForKey:@"zx_creationTime"];
		self.ltRequirementInfoId = [aDecoder decodeObjectForKey:@"zx_ltRequirementInfoId"];
		self.type = [aDecoder decodeObjectForKey:@"zx_type"];
		self.state = [aDecoder decodeObjectForKey:@"zx_state"];
		self.number = [aDecoder decodeObjectForKey:@"zx_number"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.orderNo = [aDecoder decodeObjectForKey:@"zx_orderNo"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.phone = [aDecoder decodeObjectForKey:@"zx_phone"];
		self.travelTime = [aDecoder decodeObjectForKey:@"zx_travelTime"];
		self.referrerUserId = [aDecoder decodeObjectForKey:@"zx_referrerUserId"];
		self.referrerUserHeaderImage = [aDecoder decodeObjectForKey:@"zx_referrerUserHeaderImage"];
		self.wxId = [aDecoder decodeObjectForKey:@"zx_wxId"];
		self.subState = [aDecoder decodeObjectForKey:@"zx_subState"];
		self.requirementDescription = [aDecoder decodeObjectForKey:@"zx_requirementDescription"];
		self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"money : %@\n",self.money];
	result = [result stringByAppendingFormat:@"referrerUserEasemobId : %@\n",self.referrerUserEasemobId];
	result = [result stringByAppendingFormat:@"nickname : %@\n",self.nickname];
	result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
	result = [result stringByAppendingFormat:@"referrerUserNickname : %@\n",self.referrerUserNickname];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"endTime : %@\n",self.endTime];
	result = [result stringByAppendingFormat:@"creationTime : %@\n",self.creationTime];
	result = [result stringByAppendingFormat:@"ltRequirementInfoId : %@\n",self.ltRequirementInfoId];
	result = [result stringByAppendingFormat:@"type : %@\n",self.type];
	result = [result stringByAppendingFormat:@"state : %@\n",self.state];
	result = [result stringByAppendingFormat:@"number : %@\n",self.number];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"orderNo : %@\n",self.orderNo];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"phone : %@\n",self.phone];
	result = [result stringByAppendingFormat:@"travelTime : %@\n",self.travelTime];
	result = [result stringByAppendingFormat:@"referrerUserId : %@\n",self.referrerUserId];
	result = [result stringByAppendingFormat:@"referrerUserHeaderImage : %@\n",self.referrerUserHeaderImage];
	result = [result stringByAppendingFormat:@"wxId : %@\n",self.wxId];
	result = [result stringByAppendingFormat:@"subState : %@\n",self.subState];
	result = [result stringByAppendingFormat:@"requirementDescription : %@\n",self.requirementDescription];
	result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	
    return result;
}

- (LKChatModel *)toChatModel{
    LKChatModel *model = [[LKChatModel alloc] init];
    model.ltRequirementInfoId = self.ltRequirementInfoId;
    model.type = [NSNumber numberWithInt:1];
    model.state = self.state;
    model.cityid = self.cityid;
    model.creationTime = self.creationTime;
    model.travelTime = self.travelTime;
    model.endTime = self.endTime;
    model.latitude = self.latitude;
    model.longitude = self.longitude;
    model.address = self.address;
    model.number = self.number;
    model.requirementDescription = self.requirementDescription;
    model.easemobId = self.easemobId;
    model.userId = self.userId;
    model.offerPriceId = @(0);
    model.offerPrice = @(0);
    model.isPayed = @(NO);
    return model;
}


@end
