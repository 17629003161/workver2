//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LKChatModel.h"

@interface OrderModel : NSObject<NSCoding>

@property (nonatomic,copy) NSString *money;
@property (nonatomic,copy) NSString *referrerUserEasemobId;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *serviceCount;
@property (nonatomic,copy) NSString *referrerUserNickname;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *endTime;
@property (nonatomic,strong) NSNumber *creationTime;
@property (nonatomic,strong) NSNumber *ltRequirementInfoId;
@property (nonatomic,strong) NSNumber *type;
@property (nonatomic,strong) NSNumber *state;
@property (nonatomic,strong) NSNumber *number;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,copy) NSString *orderNo;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,strong) NSNumber *travelTime;
@property (nonatomic,copy) NSNumber *referrerUserId;
@property (nonatomic,copy) NSString *referrerUserHeaderImage;
@property (nonatomic,copy) NSString *wxId;
@property (nonatomic,copy) NSNumber *subState;
@property (nonatomic,copy) NSString *requirementDescription;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,strong) NSNumber *userId;
 
-(id)initWithJson:(NSDictionary *)json;

- (LKChatModel *)toChatModel;
@end
