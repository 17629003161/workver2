//
//  OrderDetailCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/14.
//  Copyright © 2020 lester. All rights reserved.
//

#import "OrderDetailCtrl.h"
#import "OrderDetailEntity.h"
#import "MainRecommenderCtrl.h"

@interface OrderDetailCtrl ()

@property(nonatomic,strong) UILabel *statusLabel;
@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) UIImageView *bgImgv;
@property(nonatomic,strong) UIView *contentView;
@property(nonatomic,strong) UILabel *orderPayLabel;

@property(nonatomic,strong) UILabel *orderNumberLabel;
@property(nonatomic,strong) UILabel *orderDateLabel;
@property(nonatomic,strong) UILabel *addrLabel;
@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UILabel *numberLabel;
@property(nonatomic,strong) UILabel *passengersNumberLabel;
@property(nonatomic,strong) UILabel *recomandLabel;
@property(nonatomic,strong) UILabel *servicelabel;

@property(nonatomic,strong) UIImageView *recomandImageView;
@property(nonatomic,strong) UILabel *recomandNameLabel;

@property(nonatomic,strong) OrderDetailEntity *model;

@end

@implementation OrderDetailCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self layout];
//    self.contentView.hidden = NO;
    [self requestData];
}


- (void)requestData{
    Api *api = [Api apiPostUrl:@"work/requirement/checkOrderDetails"
                          para:@{@"ltRequirementInfoId":self.ltRequirementInfoId}
                   description:@"获取订单详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            OrderDetailEntity *model = [[OrderDetailEntity alloc] initWithJson:data];
            self.model = model;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

-(void)setModel:(OrderDetailEntity *)model
{
//    _model = model;
//    OrderState orderState = [model.state integerValue];
//    switch (orderState) {
//        case OrderStateIsInCommunication:{
//            self.bgImgv.image = [UIImage imageNamed:@"rect_bg_round"];
//            self.statusLabel.text = @"沟通中";
//            self.tipLabel.text = @"正在沟通中...";
//            self.contentView.hidden = NO;
//        }
//            break;
//        case OrderStateIsWaitForService:{
//            self.bgImgv.image = [UIImage imageNamed:@"rect_bg_round"];
//            self.statusLabel.text = @"待服务";
//            self.tipLabel.text = [NSString stringWithFormat:@"距离此次出行还有%.1lf天，快去准备吧",[self calcStartDay:self.model]];
//            self.contentView.hidden = NO;
//        }
//            break;
//        case OrderStateIsServing:{
//            self.bgImgv.image = [UIImage imageNamed:@"rect_bg_round2"];
//            self.statusLabel.text = @"服务中";
//            self.tipLabel.text = @"订单进行中，有任何问题都可以联系客服哦";
//            self.contentView.hidden = NO;
//        }
//            break;
//        case OrderStateIsFinished:{
//            self.bgImgv.image = [UIImage imageNamed:@"rect_bg_round2"];
//            self.statusLabel.text = @"已完成";
//            self.tipLabel.text = @"订单已经完成了，快来评价推荐官吧";
//            self.contentView.hidden = NO;
//
//        }
//            break;
//        default:
//            break;
//    }
//    if([model.state integerValue]==OrderStateIsInCommunication){
//        self.orderPayLabel.text = @"***元";
//    }else{
//        self.orderPayLabel.text = [NSString stringWithFormat:@"%.2lf元",[model.money doubleValue]/100.0];
//    }
//    self.orderNumberLabel.text = model.orderNo;
//    
//    LKDateUtils *date = [LKDateUtils new];
//    date.timeInterval = [model.creationTime doubleValue]/1000.0;
//    self.orderDateLabel.text = date.dateString;
//    self.addrLabel.text = model.address;
//    self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",[self formatDate:model.travelTime],[self formatDate:model.endTime]];
//    self.passengersNumberLabel.text = [NSString stringWithFormat:@"%ld",[model.number integerValue]];
//    self.recomandLabel.text = model.requirementDescription;
//    self.servicelabel.text = model.remark;
//    
//    self.recomandNameLabel.text = model.referrerUserNickname;
//    [self.recomandImageView sd_setImageWithURL:[NSURL URLWithString:model.referrerUserHeaderImage]
//                              placeholderImage:[UIImage imageNamed:@"person_header"]];
//
//    [self.orderPayLabel updateLayout];
//    [self.orderNumberLabel updateLayout];
//    [self.addrLabel updateLayout];
//    [self.timeLabel updateLayout];
//    [self.passengersNumberLabel updateLayout];
//    [self.recomandLabel updateLayout];
//    [self.servicelabel updateLayout];
//    [self.recomandNameLabel updateLayout];
}

- (NSString *)formatDate:(NSNumber *)timeInterval
{
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [timeInterval doubleValue]/1000.0;
    return [NSString stringWithFormat:@"%ld月%ld日%@%02ld:%02ld",date.month,date.day,date.weekday,date.hour,date.minute];
}

- (CGFloat)calcStartDay:(OrderDetailEntity *)model{
    NSDate *today = [NSDate date];
    NSTimeInterval todayinterval = [today timeIntervalSince1970];
    NSTimeInterval startinterval = [model.travelTime longLongValue]/1000;
    CGFloat day = (startinterval - todayinterval)/(24*60*60*1.0);
    if(day<0)
        day = 0;
    return day;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)goBack:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)layout{
    UIImageView *bgimgv = ImageViewWithImage(@"rect_bg_round");
    self.bgImgv = bgimgv;
    [bgimgv setUserInteractionEnabled:YES];
    UIImageView *backimgv = ImageViewWithImage(@"return_white");
    [backimgv addTapGestureTarget:self action:@selector(goBack:)];
    UIImageView *mainimgv = ImageViewWithImage(@"bag_main");
    UILabel *label1 = [Utils createLabelWithTitle:@"沟通中" titleColor:[UIColor whiteColor] fontSize:22];
    self.statusLabel = label1;
    UILabel *label2 = [Utils createLabelWithTitle:@"距离此次出行还有0天，快去准备吧" titleColor:HexColor(0xffffff) fontSize:14];
    self.tipLabel = label2;
    UIImageView *shareimgv = ImageViewWithImage(@"share_icon2");
    UILabel *label3 = [Utils createLabelWithTitle:@"分享" titleColor:HexColor(0xffffff) fontSize:11];
    [bgimgv sd_addSubviews:@[backimgv,
                             mainimgv,label1,label2,
                             shareimgv,
                             label3]];
    [self.view addSubview:bgimgv];
    [self.view addSubview:self.scrollView];
    UIView *orderView = [self createOrderView];
    UIView *noticeView = [self createNoticeView];
    UIView *contentView = [self createContentView];
    self.contentView = contentView;
    
    [self.scrollView sd_addSubviews:@[orderView,
                                      noticeView,
                                      contentView]];

    orderView.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    noticeView.sd_layout
    .topSpaceToView(orderView, 16)
    .leftEqualToView(orderView)
    .rightEqualToView(orderView);
    
    contentView.sd_layout
    .topSpaceToView(noticeView, 16)
    .leftEqualToView(noticeView)
    .rightEqualToView(noticeView);
    
    
    bgimgv.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(222);
    
    backimgv.sd_layout
    .topSpaceToView(bgimgv, 32)
    .leftSpaceToView(bgimgv, 18)
    .widthIs(30)
    .heightIs(30);
    
    mainimgv.sd_layout
    .topSpaceToView(backimgv, 15)
    .leftSpaceToView(bgimgv, 13)
    .widthIs(44)
    .heightIs(65);
    
    label1.sd_layout
    .topSpaceToView(bgimgv, 71+10)
    .leftSpaceToView(mainimgv, 0)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 3)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    shareimgv.sd_layout
    .topSpaceToView(bgimgv, 27+10)
    .rightSpaceToView(bgimgv, 20)
    .widthIs(18)
    .heightIs(14);
    
    label3.sd_layout
    .topSpaceToView(shareimgv, 3)
    .rightSpaceToView(bgimgv, 16)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.scrollView.sd_layout
    .topSpaceToView(self.view,141+10)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .bottomEqualToView(self.view);
    
    self.scrollView.contentSize = CGSizeMake(kScreenWidth-30, kScreenHeight*1.5);
}

- (UIView *)createOrderView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    
    UILabel *label1 = [self createContentLabel:@"订单金额"];
    UILabel *label2 = [Utils createLabelWithTitle:@"0.00元" titleColor:HexColor(0xFF9739) fontSize:18];
    self.orderPayLabel = label2;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE4E4E4);
    
    UILabel *pL1 = [self creatPromptLabel:@"订单编号"];
    UILabel *pL2 = [self creatPromptLabel:@"订单时间"];
    UILabel *pL3 = [self creatPromptLabel:@"始发地"];
    UILabel *pL4 = [self creatPromptLabel:@"出行时间"];
    UILabel *pL5 = [self creatPromptLabel:@"出行人数"];
    
    UILabel *cL1 = [self createContentLabel:@"100000000001"];
    self.orderNumberLabel = cL1;
    UILabel *cL2 = [self createContentLabel:@"2020-08-12"];
    self.orderDateLabel = cL2;
    UILabel *cL3 = [self createContentLabel:@"马一直在飞"];
    self.addrLabel = cL3;
    UILabel *cL4 = [self createContentLabel:@"7月3日周五9:00-7月5日周日15:00"];
    self.timeLabel = cL4;
    UILabel *cL5 = [self createContentLabel:@"4"];
    self.passengersNumberLabel = cL5;
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = HexColor(0xF8F9FB);
    UILabel *contentL = [Utils createBoldLabelWithTitle:@"" titleColor:HexColor(0x444444) fontSize:14];
    self.recomandLabel = contentL;
    contentL.numberOfLines = 0;
    [bgView addSubview:contentL];
    [view sd_addSubviews:@[label1,label2,
                                   line,
                                   pL1,cL1,
                                   pL2,cL2,
                                   pL3,cL3,
                                   pL4,cL4,
                                   pL5,cL5,
                                   bgView]];
    label1.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .leftSpaceToView(label1, 20)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .topSpaceToView(label1, 15)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(0.5);
    
    pL1.sd_layout
    .topSpaceToView(line, 20)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [pL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL1.sd_layout
    .centerYEqualToView(pL1)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    [cL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pL2.sd_layout
    .topSpaceToView(pL1, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL2.sd_layout
    .centerYEqualToView(pL2)
    .rightEqualToView(cL1)
    .autoHeightRatio(0);
    [cL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pL3.sd_layout
    .topSpaceToView(pL2, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL3.sd_layout
    .centerYEqualToView(pL3)
    .rightEqualToView(cL1)
    .leftSpaceToView(pL3, 8)
    .autoHeightRatio(18);
    
    pL4.sd_layout
    .topSpaceToView(pL3, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL4.sd_layout
    .centerYEqualToView(pL4)
    .rightEqualToView(cL1)
    .autoHeightRatio(0);
    [cL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pL5.sd_layout
    .topSpaceToView(pL4, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL5.sd_layout
    .centerYEqualToView(pL5)
    .rightEqualToView(cL1)
    .autoHeightRatio(0);
    [cL5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(bgView, 12)
    .leftSpaceToView(bgView, 10)
    .rightSpaceToView(bgView, 10)
    .autoHeightRatio(0);
    
    bgView.sd_layout
    .topSpaceToView(pL5,20)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20);
    [bgView setupAutoHeightWithBottomView:contentL bottomMargin:12];
    
    [view setupAutoHeightWithBottomView:bgView bottomMargin:20];
    return view;
}

- (void)recomandIsClicked:(UITapGestureRecognizer *)reg
{
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = self.model.referrerUserId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)createContentView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    
    UILabel *tipL = [Utils createLabelWithTitle:@"服务内容" titleColor:HexColor(0x333333) fontSize:15];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = HexColor(0xF8F9FB);
    UILabel *contentL = [Utils createBoldLabelWithTitle:@"西安自由行，用车一天，兵马俑，华清池" titleColor:HexColor(0x333333) fontSize:14];
    self.servicelabel = contentL;
    contentL.numberOfLines = 0;
    [bgView addSubview:contentL];

    UILabel *tipL2 = [Utils createLabelWithTitle:@"推荐官信息" titleColor:HexColor(0x333333) fontSize:15];
    UIView *bgView2 = [UIView new];
    bgView2.backgroundColor = HexColor(0xF8F9FB);
    [bgView2 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    UIImageView *imgv = ImageViewWithImage(@"person_header");
    self.recomandImageView = imgv;
    imgv.layer.cornerRadius = 16;
    imgv.layer.masksToBounds = YES;
    UILabel *nameLabel = [Utils createLabelWithTitle:@"SyrenaCC" titleColor:HexColor(0x333333) fontSize:15];
    self.recomandNameLabel = nameLabel;
    UIImageView *arrow = ImageViewWithImage(@"black_right_arrow");
    [bgView2 sd_addSubviews:@[imgv,nameLabel,arrow]];
    
    [view sd_addSubviews:@[tipL,
                           bgView,
                           tipL2,
                           bgView2]];
    
    tipL.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(bgView, 12)
    .leftSpaceToView(bgView, 10)
    .rightSpaceToView(bgView, 10)
    .autoHeightRatio(0);
    
    bgView.sd_layout
    .topSpaceToView(tipL,9)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20);
    [bgView setupAutoHeightWithBottomView:contentL bottomMargin:32];
    
    tipL2.sd_layout
    .leftEqualToView(tipL)
    .topSpaceToView(bgView, 14)
    .autoHeightRatio(0);
    [tipL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(bgView2)
    .leftSpaceToView(bgView2, 20)
    .widthIs(32)
    .heightIs(32);
    
    nameLabel.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 16)
    .autoHeightRatio(0);
    [nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(nameLabel)
    .rightSpaceToView(bgView2, 20)
    .widthIs(20)
    .heightIs(20);
    
    bgView2.sd_layout
    .topSpaceToView(tipL2, 9)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(64);

    [view setupAutoHeightWithBottomView:bgView2 bottomMargin:29];
    return view;
   
}

- (UIView *)createNoticeView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    UILabel *tipLabel = [Utils createLabelWithTitle:@"出行须知" titleColor:HexColor(0x333333) fontSize:15];
    UIView *view1 = [self creatdotView:@"请您在接受服务前，认真核对推荐官，确保和订单一致，安全出行"];
    UIView *view2 = [self creatdotView:@"使用“订单分享”，方便亲友随时查看您的旅行安排和推荐官信息"];
    UIView *view3 = [self creatdotView:@"服务过程中请勿提前结束订单，不要私下交易。如需要修改订单，请通过APP完成。"];
    [view sd_addSubviews:@[tipLabel,
                           view1,
                           view2,
                           view3]];
    tipLabel.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    view1.sd_layout
    .topSpaceToView(tipLabel, 13)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 16);
    
    view2.sd_layout
    .topSpaceToView(view1, 8)
    .leftEqualToView(view1)
    .rightEqualToView(view1);
    
    view3.sd_layout
    .topSpaceToView(view2, 8)
    .leftEqualToView(view1)
    .rightEqualToView(view1);
    [view setupAutoHeightWithBottomView:view3 bottomMargin:20];
    
    return view;
}

- (UILabel *)creatPromptLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x818181) fontSize:14];
}

-(UILabel *)createContentLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:14];
}

- (UIView *)creatdotView:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *dot = ImageViewWithImage(@"vdot_orange");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:13];
    label.numberOfLines = 0;
    [view sd_addSubviews:@[dot,label]];
    dot.sd_layout
    .topSpaceToView(view, 7)
    .leftSpaceToView(view, 0)
    .widthIs(7)
    .heightIs(7);
    label.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(dot, 8)
    .rightSpaceToView(view, 0)
    .autoHeightRatio(0);
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}


@end
