//
//  OrderDetailCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/14.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "OrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailCtrl : LKBaseViewController
@property (nonatomic,strong) NSNumber *ltRequirementInfoId;
@end

NS_ASSUME_NONNULL_END
