//
//  ContactOrderDetailCtrl.m
//  TravelNew
//
//  Created by mac on 2021/1/13.
//  Copyright © 2021 lester. All rights reserved.
//

#import "ContactOrderDetailCtrl.h"

#import "OrderDetailEntity.h"
#import "MainRecommenderCtrl.h"

@interface ContactOrderDetailCtrl ()

@property(nonatomic,strong) UILabel *statusLabel;
@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) UIImageView *bgImgv;
@property(nonatomic,strong) UIView *contentView;
@property(nonatomic,strong) UILabel *orderPayLabel;

@property(nonatomic,strong) UILabel *orderNumberLabel;
@property(nonatomic,strong) UILabel *orderDateLabel;
@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UILabel *numberLabel;
@property(nonatomic,strong) UILabel *recomandLabel;
@property(nonatomic,strong) UILabel *servicelabel;

@property(nonatomic,strong) UIImageView *recomandImageView;
@property(nonatomic,strong) UILabel *recomandNameLabel;

@property(nonatomic,strong) UILabel *phoneLabel;
@property(nonatomic,strong) UILabel *wxLabel;

@property(nonatomic,strong) OrderDetailEntity *model;

@end

@implementation ContactOrderDetailCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self layout];
    [self requestData];
}


- (void)requestData{
    Api *api = [Api apiPostUrl:@"work/requirement/checkOrderDetails"
                          para:@{@"ltRequirementInfoId":self.ltRequirementInfoId}
                   description:@"获取订单详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            OrderDetailEntity *model = [[OrderDetailEntity alloc] initWithJson:data];
            self.model = model;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

-(void)setModel:(OrderDetailEntity *)model
{
//    _model = model;
//    OrderState orderState = [model.state integerValue];
//    if(orderState==OrderStateIsFinished){
//        self.bgImgv.image = [UIImage imageNamed:@"rect_bg_round2"];
//        self.statusLabel.text = @"已完成";
//        self.tipLabel.text = @"订单已经完成了，快来评价推荐官吧";
//        self.contentView.hidden = NO;
//    }
//    self.orderPayLabel.text = [NSString stringWithFormat:@"%.2lf元",[model.money doubleValue]/100.0];
//    self.orderNumberLabel.text = model.orderNo;
//    
//    LKDateUtils *date = [LKDateUtils new];
//    date.timeInterval = [model.creationTime doubleValue]/1000.0;
//    self.orderDateLabel.text = date.dateString;
//    self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",[self formatDate:model.travelTime],[self formatDate:model.endTime]];
//    self.recomandLabel.text = model.requirementDescription;
//    
//    self.recomandNameLabel.text = model.referrerUserNickname;
//    [self.recomandImageView sd_setImageWithURL:[NSURL URLWithString:model.referrerUserHeaderImage]
//                              placeholderImage:[UIImage imageNamed:@"person_header"]];
//    if([LKInputChecker checkTelNumber:self.model.phone]){
//        self.phoneLabel.text = self.model.phone;
//    }else{
//        self.phoneLabel.text = @"暂无";
//    }
//    self.wxLabel.text = self.model.wxId;
//
//    [self.orderPayLabel updateLayout];
//    [self.orderNumberLabel updateLayout];
//    [self.timeLabel updateLayout];
//    [self.recomandLabel updateLayout];
//    [self.servicelabel updateLayout];
//    [self.recomandNameLabel updateLayout];
//    [self.phoneLabel updateLayout];
//    [self.wxLabel updateLayout];
}

- (NSString *)formatDate:(NSNumber *)timeInterval
{
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [timeInterval doubleValue]/1000.0;
    return [NSString stringWithFormat:@"%ld月%ld日%@%02ld:%02ld",date.month,date.day,date.weekday,date.hour,date.minute];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)goBack:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)layout{
    UIImageView *bgimgv = ImageViewWithImage(@"rect_bg_round");
    self.bgImgv = bgimgv;
    [bgimgv setUserInteractionEnabled:YES];
    UIImageView *backimgv = ImageViewWithImage(@"return_white");
    [backimgv addTapGestureTarget:self action:@selector(goBack:)];
    UIImageView *mainimgv = ImageViewWithImage(@"bag_main");
    UILabel *label1 = [Utils createLabelWithTitle:@"已完成" titleColor:[UIColor whiteColor] fontSize:22];
    self.statusLabel = label1;
    UILabel *label2 = [Utils createLabelWithTitle:@"订单已经完成了，快来评价推荐官吧" titleColor:HexColor(0xffffff) fontSize:14];
    self.tipLabel = label2;
    UIImageView *shareimgv = ImageViewWithImage(@"share_icon2");
    UILabel *label3 = [Utils createLabelWithTitle:@"分享" titleColor:HexColor(0xffffff) fontSize:11];
    [bgimgv sd_addSubviews:@[backimgv,
                             mainimgv,label1,label2,
                             shareimgv,
                             label3]];
    [self.view addSubview:bgimgv];
    [self.view addSubview:self.scrollView];
    UIView *orderView = [self createOrderView];
    UIView *noticeView = [self createNoticeView];
    UIView *contentView = [self createContentView];
    self.contentView = contentView;
    
    [self.scrollView sd_addSubviews:@[orderView,
                                      contentView,
                                      noticeView]];

    orderView.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    contentView.sd_layout
    .topSpaceToView(orderView, 16)
    .leftEqualToView(orderView)
    .rightEqualToView(orderView);
    
    noticeView.sd_layout
    .topSpaceToView(contentView, 16)
    .leftEqualToView(contentView)
    .rightEqualToView(contentView);
    
    
    bgimgv.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(222);
    
    backimgv.sd_layout
    .topSpaceToView(bgimgv, 32)
    .leftSpaceToView(bgimgv, 18)
    .widthIs(30)
    .heightIs(30);
    
    mainimgv.sd_layout
    .topSpaceToView(backimgv, 15)
    .leftSpaceToView(bgimgv, 13)
    .widthIs(44)
    .heightIs(65);
    
    label1.sd_layout
    .topSpaceToView(bgimgv, 71+10)
    .leftSpaceToView(mainimgv, 0)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 3)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    shareimgv.sd_layout
    .topSpaceToView(bgimgv, 27+10)
    .rightSpaceToView(bgimgv, 20)
    .widthIs(18)
    .heightIs(14);
    
    label3.sd_layout
    .topSpaceToView(shareimgv, 3)
    .rightSpaceToView(bgimgv, 16)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.scrollView.sd_layout
    .topSpaceToView(self.view,141+10)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .bottomEqualToView(self.view);
    
    self.scrollView.contentSize = CGSizeMake(kScreenWidth-30, kScreenHeight*1.5);
}

- (UIView *)createOrderView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    
    UILabel *label1 = [self createContentLabel:@"订单金额"];
    UILabel *label2 = [Utils createLabelWithTitle:@"0.00元" titleColor:HexColor(0xFF9739) fontSize:18];
    self.orderPayLabel = label2;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE4E4E4);
    
    UILabel *pL1 = [self creatPromptLabel:@"订单编号"];
    UILabel *pL2 = [self creatPromptLabel:@"订单时间"];
    UILabel *pL3 = [self creatPromptLabel:@"服务类型"];
    
    UILabel *cL1 = [self createContentLabel:@"100000000001"];
    self.orderNumberLabel = cL1;
    UILabel *cL2 = [self createContentLabel:@"2020-08-12"];
    self.orderDateLabel = cL2;
    UILabel *cL3 = [self createContentLabel:@"获取联系方式"];
    [view sd_addSubviews:@[label1,label2,
                                   line,
                                   pL1,cL1,
                                   pL2,cL2,
                                   pL3,cL3,
                                   ]];
    label1.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .leftSpaceToView(label1, 20)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .topSpaceToView(label1, 15)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(0.5);
    
    pL1.sd_layout
    .topSpaceToView(line, 20)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [pL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL1.sd_layout
    .centerYEqualToView(pL1)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    [cL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pL2.sd_layout
    .topSpaceToView(pL1, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL2.sd_layout
    .centerYEqualToView(pL2)
    .rightEqualToView(cL1)
    .autoHeightRatio(0);
    [cL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pL3.sd_layout
    .topSpaceToView(pL2, 12)
    .leftEqualToView(pL1)
    .autoHeightRatio(0);
    [pL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cL3.sd_layout
    .centerYEqualToView(pL3)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(18);
    [cL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:pL3 bottomMargin:21];
    return view;
}

- (void)recomandIsClicked:(UITapGestureRecognizer *)reg
{
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = self.model.referrerUserId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)phoneIsClicked:(UITapGestureRecognizer *)reg
{
    if([LKInputChecker checkTelNumber:self.model.phone]){
        [Utils callNumber:self.model.phone];
    }
}

- (void)copyClick {
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = self.wxLabel.text;
    if (pab == nil) {
        [MBProgressHUD showError:@"复制失败"];
    } else {
        [MBProgressHUD showSuccess:@"已复制"];
    }
}


- (UIView *)createContentView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;

    UILabel *tipL = [Utils createLabelWithTitle:@"推荐官信息" titleColor:HexColor(0x333333) fontSize:15];
    UIView *bgView = [UIView new];
    bgView.backgroundColor = HexColor(0xF8F9FB);
    [bgView addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    UIImageView *imgv = ImageViewWithImage(@"person_header");
    self.recomandImageView = imgv;
    imgv.layer.cornerRadius = 16;
    imgv.layer.masksToBounds = YES;
    UILabel *nameLabel = [Utils createLabelWithTitle:@"SyrenaCC" titleColor:HexColor(0x333333) fontSize:15];
    self.recomandNameLabel = nameLabel;
    UIImageView *arrow = ImageViewWithImage(@"black_right_arrow");
    [bgView sd_addSubviews:@[imgv,nameLabel,arrow]];
    
    UILabel *tipL2 = [Utils createLabelWithTitle:@"推荐官信息" titleColor:HexColor(0x333333) fontSize:15];
    UIView *bgView2 = [UIView new];
    bgView2.backgroundColor = HexColor(0xF8F9FB);
    UILabel *phoneLabel = [Utils createLabelWithTitle:@"131****3934" titleColor:HexColor(0x333333) fontSize:15];
    self.phoneLabel = phoneLabel;
    UIImageView *phoneImagv = ImageViewWithImage(@"phone_iocn_blue");
    [phoneImagv addTapGestureTarget:self action:@selector(phoneIsClicked:)];
    [bgView2 sd_addSubviews:@[phoneLabel,phoneImagv]];
    
    UILabel *tipL3 = [Utils createLabelWithTitle:@"推荐官微信号" titleColor:HexColor(0x333333) fontSize:15];
    UIView *bgView3 = [UIView new];
    bgView3.backgroundColor = HexColor(0xF8F9FB);
    UILabel *wxNumberLabel = [Utils createLabelWithTitle:@"XS9230" titleColor:HexColor(0x333333) fontSize:15];
    self.wxLabel = wxNumberLabel;
    UILabel *copyLabel = [Utils createLabelWithTitle:@"复制" titleColor:HexColor(0x6287FE) fontSize:14];
    [copyLabel addTapGestureTarget:self action:@selector(copyClick)];
    [bgView3 sd_addSubviews:@[wxNumberLabel,copyLabel]];
    [view sd_addSubviews:@[
                           tipL,
                           bgView,tipL2,bgView2,
                           tipL3,bgView3]];
    tipL.sd_layout
    .topSpaceToView(view, 23)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(bgView)
    .leftSpaceToView(bgView, 20)
    .widthIs(32)
    .heightIs(32);
    
    nameLabel.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 16)
    .autoHeightRatio(0);
    [nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(nameLabel)
    .rightSpaceToView(bgView, 20)
    .widthIs(20)
    .heightIs(20);
    
    bgView.sd_layout
    .topSpaceToView(tipL, 9)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(64);
    
    tipL2.sd_layout
    .topSpaceToView(bgView, 14)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneLabel.sd_layout
    .centerYEqualToView(bgView2)
    .leftSpaceToView(bgView2, 10)
    .autoHeightRatio(0);
    [phoneLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneImagv.sd_layout
    .centerYEqualToView(phoneLabel)
    .rightSpaceToView(bgView2, 17)
    .widthIs(13)
    .heightIs(16);
    
    bgView2.sd_layout
    .topSpaceToView(tipL2, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(50);
    
    
    tipL3.sd_layout
    .topSpaceToView(bgView2, 14)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    wxNumberLabel.sd_layout
    .centerYEqualToView(bgView3)
    .leftSpaceToView(bgView3, 10)
    .autoHeightRatio(0);
    [wxNumberLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    copyLabel.sd_layout
    .centerYEqualToView(wxNumberLabel)
    .rightSpaceToView(bgView3, 15)
    .autoHeightRatio(0);
    [copyLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgView3.sd_layout
    .topSpaceToView(tipL3, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(50);

    [view setupAutoHeightWithBottomView:bgView3 bottomMargin:29];
    return view;
   
}

- (UIView *)createNoticeView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    UILabel *tipLabel = [Utils createLabelWithTitle:@"出行须知" titleColor:HexColor(0x333333) fontSize:15];
    UIView *view1 = [self creatdotView:@"请勿将推荐官的联系方式泄露给他人"];
    UIView *view2 = [self creatdotView:@"如您不满意推荐官的服务，请联系在线客服"];
    [view sd_addSubviews:@[tipLabel,
                           view1,
                           view2
                           ]];
    tipLabel.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    view1.sd_layout
    .topSpaceToView(tipLabel, 13)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 16);
    
    view2.sd_layout
    .topSpaceToView(view1, 8)
    .leftEqualToView(view1)
    .rightEqualToView(view1);
    
    [view setupAutoHeightWithBottomView:view2 bottomMargin:20];
    return view;
}

- (UILabel *)creatPromptLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x818181) fontSize:14];
}

-(UILabel *)createContentLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:14];
}

- (UIView *)creatdotView:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *dot = ImageViewWithImage(@"vdot_orange");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:13];
    label.numberOfLines = 0;
    [view sd_addSubviews:@[dot,label]];
    dot.sd_layout
    .topSpaceToView(view, 7)
    .leftSpaceToView(view, 0)
    .widthIs(7)
    .heightIs(7);
    label.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(dot, 8)
    .rightSpaceToView(view, 0)
    .autoHeightRatio(0);
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}


@end
