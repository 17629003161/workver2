//
//  ServiceSetupMainCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ServiceSetupMainCtrl.h"
#import "SetupCell.h"
#import "ContactSetupCtrl.h"
#import "CarSetupListCtrl.h"
#import "RoomSetupListCtrl.h"

@interface ServiceSetupMainCtrl ()

@end

@implementation ServiceSetupMainCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"服务设置" titleColor:nil];
    [self disabelPageRefresh];
    self.tableView.backgroundColor = HexColor(0xF9F9F9);
    [self.tableView registerClass:[SetupCell class] forCellReuseIdentifier:@"SetupCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.dataArray addObjectsFromArray:@[@"马上联系",@"当地用车",@"当地住宿"]];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetupCell"];
    cell.leftLabel.text = self.dataArray[indexPath.row];
    cell.rightLabel.text = @"";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            ContactSetupCtrl *vc = [ContactSetupCtrl new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:{
            CarSetupListCtrl *vc = [CarSetupListCtrl new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{
            RoomSetupListCtrl *vc = [RoomSetupListCtrl new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
}

@end
