//
//  RoomSetupListCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RoomSetupListCtrl.h"
#import "CarAuthListCell.h"
#import "HotelInfoCell.h"
#import "RoomPriceSetupCtrl.h"
#import "AddRoomCtrl.h"
#import "YTKBatchRequest.h"
#import "RoomSeupListEntity.h"
#import "LKShowPictureView.h"

@interface RoomSetupListCtrl ()
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UISwitch *btnSwitch;
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,assign) BOOL isServiceOn;
@end

@implementation RoomSetupListCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"当地住宿" titleColor:nil];
    // Do any additional setup after loading the view.
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    self.tableView.tableHeaderView = [self makeHeaderView];
    [self.tableView registerClass:[HotelInfoCell class] forCellReuseIdentifier:@"HotelInfoCell"];
    self.tableView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self sendBatchRequest];
}

- (void)loadFirstPage
{
    [super loadFirstPage];
    [self sendBatchRequest];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addFloatButton];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.btn removeFromSuperview];
}

- (void)sendBatchRequest{
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"work/house/whetherTopen" para:@{} description:@"查询是否开通"];
    Api *api2 = [Api apiPostUrl:@"work/house/list"
                           para:@{}
                    description:@"住宿信息列表"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        DLog(@"api1 jsonstr  = %@",request1.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSNumber *state = [json objectForKey:@"data"];
                weakSelf.isServiceOn = [state boolValue];
            }
        }

        YTKRequest *request2 = batchRequest.requestArray[1];
        DLog(@"api2 jsonstr  = %@",request2.responseString);
        json = [NSString jsonStringToDictionary:request2.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSArray *data = [json objectForKey:@"data"];
                [self.dataArray removeAllObjects];
                for(NSDictionary *dict in data){
                    RoomSeupListEntity *model = [[RoomSeupListEntity alloc] initWithJson:dict];
                    [self.dataArray addObject:model];
                }
                [weakSelf.tableView reloadData];
            }
        }
        if(self.dataArray.count>0 && self.isServiceOn){
            weakSelf.btnSwitch.on = YES;
        }else{
            weakSelf.btnSwitch.on = NO;
        }

    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            DLog(@"api1 error:%@ responsestr = %@",request1.error,request1.responseString);
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            DLog(@"api2 error:%@ responsestr = %@",request2.error,request2.responseString);
        }
    }];
}

- (void)switchIsClicked:(UISwitch *)sw
{
    if(self.dataArray.count==0){
        [self addRoomIsClicked:nil];
        return;
    }
    Api *api = [Api apiPostUrl:@"work/house/changeWhetherTopen" para:@{@"var":@(sw.on)} description:@"更改状态"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (UIView *)makeHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 70)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    topview.backgroundColor = HexColor(0xf9f9f9);
    self.descLabel = [Utils createLabelWithTitle:@"提供旅行住宿服务" titleColor:HexColor(0x999999) fontSize:14];
    self.btnSwitch = [[UISwitch alloc] init];
    [self.btnSwitch addTarget:self action:@selector(switchIsClicked:) forControlEvents:UIControlEventValueChanged];

    [view sd_addSubviews:@[topview,self.descLabel,self.btnSwitch]];
    
    self.descLabel.sd_layout
    .topSpaceToView(topview, 22)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [self.descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.btnSwitch.sd_layout
    .centerYEqualToView(self.descLabel)
    .rightSpaceToView(view, 16)
    .widthIs(42)
    .heightIs(24);
    return view;
}

- (void)addRoomIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    AddRoomCtrl *vc = [AddRoomCtrl new];
    vc.refreshBlock = ^{
        [weakSelf sendBatchRequest];
    };
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)addFloatButton{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    if(!self.btn){
        LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"添加房屋" font:LKSystemFont(17) cornerRadius:22];
        self.btn = btn;
        [btn addTapGestureTarget:self action:@selector(addRoomIsClicked:)];
        [keyWindow addSubview:btn];
        btn.sd_layout
        .leftSpaceToView(keyWindow, 20)
        .rightSpaceToView(keyWindow, 20)
        .bottomSpaceToView(keyWindow, 33)
        .heightIs(44);
    }else{
        [keyWindow addSubview:self.btn];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelInfoCell"];
    RoomSeupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    cell.showPictureBlock = ^(NSArray * _Nonnull picAry, UIView *picView) {
        CGRect startFrame = [picView convertRect:picView.bounds toView:self.view];
        LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        view.pictureArray = picAry;
        [view show:startFrame];

    };
    cell.navigationBlock = ^(RoomSeupListEntity * _Nonnull model) {
        LKWeakSelf
        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
        [weakSelf doNavigationWithEndLocation:endLocation];
    };
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    RoomSeupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    RoomPriceSetupCtrl *vc = [RoomPriceSetupCtrl new];
    vc.model = model;
    vc.finishBlock = ^(RoomSeupListEntity * _Nonnull model) {
        for(RoomSeupListEntity *item in self.dataArray){
            if([item.id integerValue] == [model.id integerValue]){
                item.price = model.price;
                break;
            }
        }
        [weakSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

@end
