//
//  AddRoomCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AddRoomCtrl.h"
#import "LKAddressSelectCtrl.h"
#define C_PhoteSize  ((kScreenWidth-80-24)/3.0)
@interface AddRoomCtrl ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UITextField *addressField;
@property(nonatomic,strong) UITextField *nameField;
@property(nonatomic,strong) UITextField *roomNumberField;
@property(nonatomic,strong) UITextField *capacityField;
@property(nonatomic,strong) UITextField *priceField;
@property(nonatomic,assign) CLLocationCoordinate2D location;
@property(nonatomic,strong) NSString *cityId;

@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property(nonatomic,strong) NSMutableArray *imageviewArray;
@property (nonatomic,strong) NSMutableArray *imagePathArray;
@property (nonatomic,assign) NSInteger curImageIndex;

@end

@implementation AddRoomCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"添加房屋" titleColor:nil];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.imagePathArray = [NSMutableArray new];
    self.curImageIndex = -1;
    
    [self layout];
    [self makeJsonstr];
}

- (NSString *)makeJsonstr{
    NSMutableArray *ary = [[NSMutableArray alloc] init];
    for(int i=0;i<self.imagePathArray.count;i++){
        NSString *url = self.imagePathArray[i];
        NSDictionary *dict = @{@"imageurl":url,@"sequence":@(i)};
        [ary addObject:dict];
    }
    NSData *data=[NSJSONSerialization dataWithJSONObject:ary options:NSJSONWritingPrettyPrinted error:nil];;
    NSString *jsonStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    DLog(@"jsonStr==%@",jsonStr);
    return jsonStr;
}



- (void)addRoomIsClicked:(id)sender
{
    if(self.cityId==nil){
        [MBProgressHUD showMessage:@"请选择房屋位置"];
        return;
    }
    if(self.imagePathArray.count==0){
        [MBProgressHUD showMessage:@"请选择图片"];
        return;
    }
    NSInteger roomNumber = [self.roomNumberField.text integerValue];
    NSInteger capacity = [self.capacityField.text integerValue];
    NSInteger price = [self.priceField.text integerValue];
    
    NSMutableArray *array = [NSMutableArray new];
    for (int i=0; i<self.imagePathArray.count; i++) {
        NSString *urlstr = self.imagePathArray[i];
        [array addObject:@{@"imageurl":urlstr,
                           @"sequence":@(i)}];
    }
    NSString *hostjsonstr = [self makeJsonstr];

    Api *api = [Api apiPostUrl:@"work/house/add"
                          para:@{
                              @"address":self.addressField.text,
                          @"bedroomsnum":@(roomNumber),
                                   @"id":[NSNull null],
                              @"latitude":@(self.location.latitude),
                             @"longitude":@(self.location.longitude),
                              @"ltHouseImages":hostjsonstr,
                               @"number":@(capacity),
                                @"price":@(price),
                              @"title":self.nameField.text
                                }
                   description:@"新增修改住宿信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
         NSString *jsonstr = request.responseString;
        DLog(@"jsonstr  = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            [self.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

//上传图片
- (void)cosUploadImgae:(UIImage *)image{
    NSString *remoteDir = @"lvkauxing/img/";
    NSString* tempPath = QCloudTempFilePathWithExtension(@"png");
    QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
    // 存储桶名称，格式为 BucketName-APPID
    put.bucket = @"nlcx-1301827761";
    // 对象键，是对象在 COS 上的完整路径，如果带目录的话，格式为 "dir1/object1"
    put.object = [remoteDir stringByAppendingPathComponent:tempPath];
    //需要上传的对象内容。可以传入NSData*或者NSURL*类型的变量
    NSData *data = UIImagePNGRepresentation(image);
    put.body =  data;
    //监听上传进度
    [put setSendProcessBlock:^(int64_t bytesSent,
                                int64_t totalBytesSent,
                                int64_t totalBytesExpectedToSend) {
        //      bytesSent                   新增字节数
        //      totalBytesSent              本次上传的总字节数
        //      totalBytesExpectedToSend    本地上传的目标字节数
    }];

    LKWeakSelf
    //监听上传结果
    [put setFinishBlock:^(id outputObject, NSError *error) {
        //可以从 outputObject 中获取 response 中 etag 或者自定义头部等信息
        QCloudUploadObjectResult *result = (QCloudUploadObjectResult *)outputObject;
       // NSDictionary * result = (NSDictionary *)outputObject;
        DLog(@"file location = %@",result.location);
        [weakSelf.imagePathArray addObject:result.location];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showPictures];
        });
    }];

    [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
}

- (void)typeView:(UITapGestureRecognizer *)reg
{
    [self.addressField resignFirstResponder];
    [self.roomNumberField resignFirstResponder];
    [self.capacityField resignFirstResponder];
    [self.priceField resignFirstResponder];
}

- (void)layout{
    
    self.scrollView.frame = self.view.bounds;
    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.scrollView];
    [self.scrollView addTapGestureTarget:self action:@selector(typeView:)];
    
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    barView.backgroundColor = HexColor(0xf9f9f9);
    UIView *view1 = [self createInputView:@"房屋位置"];
    self.addressField = [view1 viewWithTag:1000];
    self.addressField.enabled = YES;
    self.addressField.placeholder = @"请输入地址";
    [self.addressField addTapGestureTarget:self action:@selector(selectAddress:)];
    UIView *view2 = [self createInputView:@"房屋标题"];
    self.nameField = [view2 viewWithTag:1000];
    self.nameField.placeholder = @"请输入房屋标题";
    UIView *view3 = [self createInputView:@"卧室数量"];
    self.roomNumberField = [view3 viewWithTag:1000];
    self.roomNumberField.keyboardType = UIKeyboardTypeNumberPad;
    self.roomNumberField.placeholder = @"请输入卧室数量";
    UIView *view4 = [self createInputView:@"可住人数"];
    self.capacityField = [view4 viewWithTag:1000];
    self.capacityField.keyboardType = UIKeyboardTypeNumberPad;
    self.capacityField.placeholder = @"请输入可住人数";
    UIView *view5 = [self createInputView:@"出租价格(元/天)"];
    self.priceField = [view5 viewWithTag:1000];
    self.priceField.keyboardType = UIKeyboardTypeNumberPad;
    self.priceField.placeholder = @"请输入出租价格";
    [self.scrollView sd_addSubviews:@[barView,
                                      view1,
                                      view2,
                                      view3,
                                      view4,
                                      view5]];
    view1.sd_layout
    .topSpaceToView(barView, 20)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    view2.sd_layout
    .topSpaceToView(view1, 8)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    view3.sd_layout
    .topSpaceToView(view2, 18)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);

    view4.sd_layout
    .topSpaceToView(view3, 18)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    view5.sd_layout
    .topSpaceToView(view4, 18)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    
    UILabel *tipLabel = [Utils createBoldLabelWithTitle:@"房屋照片" titleColor:HexColor(0x333333) fontSize:16];
    UIView *contentView = [self createContentView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"提交"
                                           font:LKSystemFont(18)
                                   cornerRadius:25];
    [btn addTapGestureTarget:self action:@selector(addRoomIsClicked:)];
    [self.scrollView sd_addSubviews:@[tipLabel,contentView,
                                      btn]];
    tipLabel.sd_layout
    .topSpaceToView(view5, 18)
    .leftSpaceToView(self.scrollView, 20)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    
    contentView.sd_layout
    .topSpaceToView(tipLabel, 10)
    .leftSpaceToView(self.scrollView, 20)
    .rightSpaceToView(self.scrollView, 20);
    
    btn.sd_layout
    .topSpaceToView(contentView, 25)
    .leftSpaceToView(self.scrollView, 40)
    .rightSpaceToView(self.scrollView, 40)
    .heightIs(46);
    
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight+300);
}


- (void)selectAddress:(UITapGestureRecognizer *)reg{
    LKAddressSelectCtrl *vc = [[LKAddressSelectCtrl alloc] init];
   // vc.address = self.addressField.text;
    LKWeakSelf
    vc.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, NSString * _Nonnull cityName, CLLocationCoordinate2D location) {
        weakSelf.addressField.text = name;
        weakSelf.cityId = cityid;
        weakSelf.location = location;
    };
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (UIView *)createInputView:(NSString *)title
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *titleL = [Utils createBoldLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:16];
    UITextField *field = [UITextField new];
    [field textLeftOffset:16];
    field.clearButtonMode = UITextFieldViewModeAlways;
    field.backgroundColor = HexColor(0xf9f9f9);
    field.tag = 1000;
    [view sd_addSubviews:@[titleL,field]];
    
    titleL.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    
    field.sd_layout
    .topSpaceToView(titleL, 10)
    .leftEqualToView(titleL)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    [view setupAutoHeightWithBottomView:field bottomMargin:0];
    return view;
}




- (UIView *)createContentView{

    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:139/255.0 green:149/255.0 blue:169/255.0 alpha:0.2].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    
    [view sd_addSubviews:self.imageviewArray];
    
    UIImageView *pview0 = self.imageviewArray[0];
    pview0.hidden = NO;
    pview0.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview1 = self.imageviewArray[1];
    pview1.sd_layout
    .topEqualToView(pview0)
    .leftSpaceToView(pview0, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview2 = self.imageviewArray[2];
    pview2.sd_layout
    .topEqualToView(pview0)
    .leftSpaceToView(pview1, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview3 = self.imageviewArray[3];
    pview3.sd_layout
    .topSpaceToView(pview0, 8)
    .leftEqualToView(pview0)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview4 = self.imageviewArray[4];
    pview4.sd_layout
    .topEqualToView(pview3)
    .leftSpaceToView(pview3, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview5 = self.imageviewArray[5];
    pview5.sd_layout
    .topEqualToView(pview4)
    .leftSpaceToView(pview4, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview6 = self.imageviewArray[6];
    pview6.sd_layout
    .topSpaceToView(pview3, 8)
    .leftEqualToView(pview3)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview7 = self.imageviewArray[7];
    pview7.sd_layout
    .topEqualToView(pview6)
    .leftSpaceToView(pview6, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview8 = self.imageviewArray[8];
    pview8.sd_layout
    .topEqualToView(pview7)
    .leftSpaceToView(pview7, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    [view setupAutoHeightWithBottomView:pview0 bottomMargin:30];
    return view;
}

- (void)takePhotoIsClicked:(UITapGestureRecognizer *)reg
{
    UIImageView *imgv = (UIImageView *)reg.view;
    self.curImageIndex = imgv.tag;
    [self chooseImage];
    return;
}

- (void)deletePhotoIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    self.curImageIndex = btn.tag;
    if(self.curImageIndex<self.imagePathArray.count){
        [self.imagePathArray removeObjectAtIndex:self.curImageIndex];
        [self showPictures];
    }
}

- (void)showPictures{
    for(int i=0; i<self.imageviewArray.count; i++){
        UIImageView *imgv = self.imageviewArray[i];
        imgv.image = [UIImage imageNamed:@"share_bg_7"];
        NSMutableArray *newges = [NSMutableArray arrayWithArray:imgv.gestureRecognizers];
        for (int j =0; j<[newges count]; j++) {
            [imgv removeGestureRecognizer:[newges objectAtIndex:j]];
        }
        imgv.hidden = YES;
    }
    for (int i=0; i<self.imagePathArray.count; i++) {
        UIImageView *imgv = self.imageviewArray[i];
        [imgv sd_setImageWithURL:[NSURL URLWithString:self.imagePathArray[i]]];
        imgv.hidden = NO;
    };
    if(self.imagePathArray.count<9){
        UIImageView *lastview = self.imageviewArray[self.imagePathArray.count];
        [lastview addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
        lastview.hidden = NO;
        UIView *contentView = [lastview superview];
        [contentView setupAutoHeightWithBottomView:lastview bottomMargin:30];
        [contentView updateLayout];
    }
}


-(void)chooseImage {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//获取选择的图片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *image1 = [Utils compressImageSize:image toByte:10*1024];
    [self cosUploadImgae:image1];
}


//从相机或者相册界面弹出
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableArray *)imageviewArray
{
    if(!_imageviewArray){
        _imageviewArray = [NSMutableArray new];
        for(int i=0;i<9;i++){
            UIImageView *picView = [self makePhotoViewTag:i];
            picView.userInteractionEnabled = YES;
            [picView addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
            picView.hidden = YES;
            picView.tag = i;
            [_imageviewArray addObject:picView];
        }
    }
    return _imageviewArray;
}



- (UIImageView *)makePhotoViewTag:(NSInteger)tag{
    UIImageView *imgv = ImageViewWithImage(@"share_bg_7");
    imgv.tag = tag;
    imgv.layer.cornerRadius = 3;
    imgv.layer.masksToBounds = YES;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = tag;
    [btn setBackgroundColor:HexColor(0x64A8FD)];
    [btn addTarget:self action:@selector(deletePhotoIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"X" forState:UIControlStateNormal];
    [imgv addSubview:btn];
    btn.sd_layout
    .topEqualToView(imgv)
    .rightEqualToView(imgv)
    .widthIs(18)
    .heightIs(18);
    return imgv;
}

@end

