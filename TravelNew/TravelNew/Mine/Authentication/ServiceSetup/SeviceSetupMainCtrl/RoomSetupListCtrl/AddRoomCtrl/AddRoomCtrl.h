//
//  AddRoomCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddRoomCtrl : LKBaseViewController
@property(nonatomic,copy) void (^refreshBlock)(void);
@end

NS_ASSUME_NONNULL_END
