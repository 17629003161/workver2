//
//  AddRoomCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "RoomSeupListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomPriceSetupCtrl : LKBaseViewController
@property(nonatomic,copy) void (^finishBlock)(RoomSeupListEntity *model);
@property (nonatomic,strong) RoomSeupListEntity *model;
@end

NS_ASSUME_NONNULL_END
