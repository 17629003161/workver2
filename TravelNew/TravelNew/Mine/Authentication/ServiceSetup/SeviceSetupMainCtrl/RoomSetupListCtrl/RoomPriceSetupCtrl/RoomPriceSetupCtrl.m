//
//  AddRoomCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RoomPriceSetupCtrl.h"

@interface RoomPriceSetupCtrl ()
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UITextField *priceField;

@end

@implementation RoomPriceSetupCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"添加住宿" titleColor:nil];
    self.view.backgroundColor = HexColor(0xf9f9f9);
    [self addRightButton];
    [self.view addTapGestureTarget:self action:@selector(viewIsTyped:)];
    UIView *view = [self configViews];
    [self.view addSubview:view];
    view.sd_layout
    .topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(120);
    
    [self loadData];
}


- (void)loadData{
    if(self.model){
        self.nameLabel.text = self.model.title;
        self.priceField.text = [self.model.price stringValue];
        [self.nameLabel updateLayout];
    }
}

- (void)btnSaveIsClicked:(id)sender
{
    CGFloat price = [self.priceField.text floatValue];
    Api *api = [Api apiPostUrl:@"work/house/add"
                          para:@{@"id":self.model.id,
                                 @"price":@(price)}
                   description:@"新增修改住宿信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSString *jsonstr = request.responseString;
            DLog(@"%@",jsonstr);
            NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
            if([[json objectForKey:@"code"] integerValue]==200){
                self.model.price = @(price);
                if(self.finishBlock){
                    self.finishBlock(self.model);
                }
               [MBProgressHUD showMessage:json[@"msg"]];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)addRightButton{
    UIButton* btn= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    UIBarButtonItem* barItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    [btn addTarget:self action:@selector(btnSaveIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = barItem;
}

- (UIView *)configViews{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"市中心/钟楼地铁三室居" titleColor:HexColor(0x666666) fontSize:14];
    self.nameLabel = label;
    UILabel *label2 = [Utils createLabelWithTitle:@"房租每天" titleColor:HexColor(0x666666) fontSize:14];
    UILabel *label3 = [Utils createLabelWithTitle:@"元" titleColor:HexColor(0xFF9739) fontSize:14];
    UITextField *textField = [self createTextField];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    self.priceField = textField;
    [view sd_addSubviews:@[label,
                                       label2,textField,label3]];
    label.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 19)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    textField.sd_layout
    .leftSpaceToView(label2, 5)
    .centerYEqualToView(label2)
    .widthIs(70)
    .heightIs(32);
    
    label3.sd_layout
    .centerYEqualToView(textField)
    .leftSpaceToView(textField, 5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self.priceField resignFirstResponder];
}


- (UITextField *)createTextField
{
    UITextField *field = [UITextField new];
    field.clearButtonMode = UITextFieldViewModeAlways;
    field.layer.borderWidth = 0.5;
    field.layer.borderColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0].CGColor;
    field.layer.cornerRadius = 6.0;
    [field textLeftOffset:14];
    return field;
}

@end
