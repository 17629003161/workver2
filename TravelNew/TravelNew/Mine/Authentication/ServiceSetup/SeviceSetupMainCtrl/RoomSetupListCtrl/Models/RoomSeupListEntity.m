//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RoomSeupListEntity.h"

@implementation RoomSeupListEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"ltHouseImageList":@"LtHouseImageListEntity",};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
		self.address  = [json objectForKey:@"address"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.id  = [json objectForKey:@"id"];
		self.price  = [json objectForKey:@"price"];
		self.title  = [json objectForKey:@"title"];
		self.bedroomsnum  = [json objectForKey:@"bedroomsnum"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.ltHouseImages  = [json objectForKey:@"ltHouseImages"];
		self.number  = [json objectForKey:@"number"];
		self.ltHouseImageList = [NSMutableArray array];
	for(NSDictionary *item in [json objectForKey:@"ltHouseImageList"])
		{
[self.ltHouseImageList addObject:[[LtHouseImageListEntity alloc] initWithJson:item]];
	}

    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.id forKey:@"zx_id"];
	[aCoder encodeObject:self.price forKey:@"zx_price"];
	[aCoder encodeObject:self.title forKey:@"zx_title"];
	[aCoder encodeObject:self.bedroomsnum forKey:@"zx_bedroomsnum"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.ltHouseImages forKey:@"zx_ltHouseImages"];
	[aCoder encodeObject:self.number forKey:@"zx_number"];
	[aCoder encodeObject:self.ltHouseImageList forKey:@"zx_ltHouseImageList"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.id = [aDecoder decodeObjectForKey:@"zx_id"];
		self.price = [aDecoder decodeObjectForKey:@"zx_price"];
		self.title = [aDecoder decodeObjectForKey:@"zx_title"];
		self.bedroomsnum = [aDecoder decodeObjectForKey:@"zx_bedroomsnum"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.ltHouseImages = [aDecoder decodeObjectForKey:@"zx_ltHouseImages"];
		self.number = [aDecoder decodeObjectForKey:@"zx_number"];
		self.ltHouseImageList = [aDecoder decodeObjectForKey:@"zx_ltHouseImageList"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"id : %@\n",self.id];
	result = [result stringByAppendingFormat:@"price : %@\n",self.price];
	result = [result stringByAppendingFormat:@"title : %@\n",self.title];
	result = [result stringByAppendingFormat:@"bedroomsnum : %@\n",self.bedroomsnum];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"ltHouseImages : %@\n",self.ltHouseImages];
	result = [result stringByAppendingFormat:@"number : %@\n",self.number];
	result = [result stringByAppendingFormat:@"ltHouseImageList : %@\n",self.ltHouseImageList];
	
    return result;
}

@end
