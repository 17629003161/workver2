//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "LtHouseImageListEntity.h"

@implementation LtHouseImageListEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.sequence  = [json objectForKey:@"sequence"];
		self.id  = [json objectForKey:@"id"];
		self.houseId  = [json objectForKey:@"houseId"];
		self.imageurl  = [json objectForKey:@"imageurl"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.sequence forKey:@"zx_sequence"];
	[aCoder encodeObject:self.id forKey:@"zx_id"];
	[aCoder encodeObject:self.houseId forKey:@"zx_houseId"];
	[aCoder encodeObject:self.imageurl forKey:@"zx_imageurl"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.sequence = [aDecoder decodeObjectForKey:@"zx_sequence"];
		self.id = [aDecoder decodeObjectForKey:@"zx_id"];
		self.houseId = [aDecoder decodeObjectForKey:@"zx_houseId"];
		self.imageurl = [aDecoder decodeObjectForKey:@"zx_imageurl"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"sequence : %@\n",self.sequence];
	result = [result stringByAppendingFormat:@"id : %@\n",self.id];
	result = [result stringByAppendingFormat:@"houseId : %@\n",self.houseId];
	result = [result stringByAppendingFormat:@"imageurl : %@\n",self.imageurl];
	
    return result;
}

@end
