//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONModel.h"


@protocol LtHouseImageListEntity
@end

@interface LtHouseImageListEntity : JSONModel<NSCoding>

@property (nonatomic,strong) NSNumber *sequence;
@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,strong) NSNumber *houseId;
@property (nonatomic,copy) NSString *imageurl;
 


-(id)initWithJson:(NSDictionary *)json;

@end
