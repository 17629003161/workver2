//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LtHouseImageListEntity.h"

@interface RoomSeupListEntity : JSONModel<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,strong) NSNumber *price;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,strong) NSNumber *bedroomsnum;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,copy) NSString *ltHouseImages;
@property (nonatomic,strong) NSNumber *number;
@property (nonatomic,strong) NSMutableArray<LtHouseImageListEntity> *ltHouseImageList;
 


-(id)initWithJson:(NSDictionary *)json;


@end
