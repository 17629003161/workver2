//
//  CarPriceSetupCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "CarPriceSetupCtrl.h"

@interface CarPriceSetupCtrl ()
@property(nonatomic,strong) UITextField *distField1;
@property(nonatomic,strong) UITextField *priceField1;
@property(nonatomic,strong) UITextField *distField2;
@property(nonatomic,strong) UITextField *priceField2;
@property(nonatomic,strong) UILabel *carTypeLable;

@end

@implementation CarPriceSetupCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"当地用车" titleColor:nil];
    self.view.backgroundColor = HexColor(0xf9f9f9);
    [self.view addTapGestureTarget:self action:@selector(viewIsTyped:)];
    [self addRightButton];
    UIView *view = [self configViews];
    [self.view addSubview:view];
    view.sd_layout
    .topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(160);
    DLog(@"%@",self.model);
    [self loadData];
}

- (void)loadData{
    self.carTypeLable.text = self.model.carModel;
    self.distField1.text = [self.model.minimumMileage stringValue];
    self.priceField1.text = [self.model.minimumPrice stringValue];
    self.distField2.text = [self.model.maximumMileage stringValue];
    self.priceField2.text = [self.model.maximumPrice stringValue];
}


- (void)btnSaveIsClicked:(id)sender
{
    CGFloat ds1 = [self.distField1.text floatValue];
    CGFloat ds2 = [self.distField2.text floatValue];
    CGFloat price1 = [self.priceField1.text floatValue];
    CGFloat price2 = [self.priceField2.text floatValue];
    
    Api *api = [Api apiPostUrl:@"work/price/add"
                          para:@{@"maximumMileage":@(ds2),
                                 @"minimumMileage":@(ds1),
                                 @"maximumPrice":@(price2),
                                 @"minimumPrice":@(price1),
                                 @"carId":self.model.carId}
                   description:@"新增保存车辆报价"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            self.model.maximumMileage = @(ds2);
            self.model.minimumMileage = @(ds1);
            self.model.maximumPrice = @(price2);
            self.model.minimumPrice = @(price1);
            if(self.finishBlock){
                self.finishBlock(self.model);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)addRightButton{
    UIButton* btn= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    UIBarButtonItem* barItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    [btn addTarget:self action:@selector(btnSaveIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = barItem;
}


-(UIView *)configViews
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [self createLabel:@"丰田 凯美瑞"];
    self.carTypeLable = label;
    UILabel *label1 = [self createLabel:@"4小时起限"];
    UILabel *label2 = [self createLabel:@"公里"];
    UILabel *label3 = [self createLabel:@"元"];
    label3.textColor = HexColor(0xFF9739);
    UILabel *label4 = [self createLabel:@"整天12小时限"];
    UILabel *label5 = [self createLabel:@"公里"];
    UILabel *label6 = [self createLabel:@"元"];
    label6.textColor = HexColor(0xFF9739);
    UITextField *field1 = [self createTextField];
    self.distField1 = field1;
    self.distField1.keyboardType = UIKeyboardTypeNumberPad;
    UITextField *field2 = [self createTextField];
    self.priceField1 = field2;
    self.priceField1.keyboardType = UIKeyboardTypeNumberPad;
    UITextField *field3 = [self createTextField];
    self.distField2 = field3;
    self.distField2.keyboardType = UIKeyboardTypeNumberPad;
    UITextField *field4 = [self createTextField];
    self.priceField2 = field4;
    self.priceField2.keyboardType = UIKeyboardTypeNumberPad;
    [view sd_addSubviews:@[label,
                                          label1,field1,label2,field2,label3,
                                       label4,field3,label5,field4,label6]];
    label.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label1.sd_layout
    .topSpaceToView(label, 19)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
     
    field1.sd_layout
    .centerYEqualToView(label1)
    .leftSpaceToView(label1, 5)
    .widthIs(70)
    .heightIs(32);
    
    label2.sd_layout
    .centerYEqualToView(field1)
    .leftSpaceToView(field1, 5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field2.sd_layout
    .centerYEqualToView(label2)
    .leftSpaceToView(label2, 5)
    .widthIs(70)
    .heightIs(32);
    
    label3.sd_layout
    .centerYEqualToView(field2)
    .leftSpaceToView(field2, 5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label4.sd_layout
    .topSpaceToView(label1, 28)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
     
    field3.sd_layout
    .centerYEqualToView(label4)
    .leftSpaceToView(label4, 5)
    .widthIs(70)
    .heightIs(32);
    
    label5.sd_layout
    .centerYEqualToView(field3)
    .leftSpaceToView(field3, 5)
    .autoHeightRatio(0);
    [label5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field4.sd_layout
    .centerYEqualToView(label5)
    .leftSpaceToView(label5, 5)
    .widthIs(70)
    .heightIs(32);
    
    label6.sd_layout
    .centerYEqualToView(field4)
    .leftSpaceToView(field4, 5)
    .autoHeightRatio(0);
    [label6 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self.distField1 resignFirstResponder];
    [self.priceField1 resignFirstResponder];
    [self.distField2 resignFirstResponder];
    [self.priceField2 resignFirstResponder];
}

- (UILabel *)createLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:14];;
}

- (UITextField *)createTextField
{
    UITextField *field = [UITextField new];
    field.layer.borderWidth = 0.5;
    field.layer.borderColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0].CGColor;
    field.layer.cornerRadius = 6.0;
    [field textLeftOffset:14];
    return field;
}

@end
