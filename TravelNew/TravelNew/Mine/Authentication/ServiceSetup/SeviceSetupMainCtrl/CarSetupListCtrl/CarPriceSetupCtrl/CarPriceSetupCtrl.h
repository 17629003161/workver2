//
//  CarPriceSetupCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "CarSetupListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarPriceSetupCtrl : LKBaseViewController
@property(nonatomic,strong) void (^finishBlock)(CarSetupListEntity *);
@property(nonatomic,strong) CarSetupListEntity *model;
@end

NS_ASSUME_NONNULL_END
