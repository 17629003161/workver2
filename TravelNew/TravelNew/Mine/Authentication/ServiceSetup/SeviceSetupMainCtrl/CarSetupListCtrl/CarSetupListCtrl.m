//
//  CarSetupListCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "CarSetupListCtrl.h"
#import "CarAuthListCell.h"
#import "XingShiZhengCertificationCtrl.h"
#import "CarPriceSetupCtrl.h"
#import "YTKBatchRequest.h"

@interface CarSetupListCtrl ()
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UISwitch *btnSwitch;
@property(nonatomic,strong) LKGadentButton *btn;
@end

@implementation CarSetupListCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"当地用车" titleColor:nil];
    // Do any additional setup after loading the view.
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    self.tableView.tableHeaderView = [self makeHeaderView];
    [self.tableView registerClass:[CarAuthListCell class] forCellReuseIdentifier:@"CarAuthListCell"];
    [self sendBatchRequest];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addFloatButton];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.btn removeFromSuperview];
}

- (void)loadFirstPage{
    [super loadFirstPage];
    [self sendBatchRequest];
}


- (void)sendBatchRequest{
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"work/price/whetherTopen" para:@{} description:@"查询是否开通"];;
    Api *api2 = [Api apiPostUrl:@"work/price/list"
                                          para:@{}
                                   description:@"查询车辆报价列表"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        DLog(@"api1 jsonstr  = %@",request1.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSNumber *state = [json objectForKey:@"data"];
                BOOL isOn = [state boolValue];
                weakSelf.btnSwitch.on = isOn;
                if(isOn){
                    weakSelf.btnSwitch.hidden = NO;
                }else{
                    weakSelf.btnSwitch.hidden = YES;
                }
            }
        }

        YTKRequest *request2 = batchRequest.requestArray[1];
        DLog(@"api2 jsonstr  = %@",request2.responseString);
        json = [NSString jsonStringToDictionary:request2.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSArray *data = [json objectForKey:@"data"];
                [self.dataArray removeAllObjects];
                for(NSDictionary *dict in data){
                    CarSetupListEntity *model = [[CarSetupListEntity alloc] initWithJson:dict];
                    [self.dataArray addObject:model];
                }
                [weakSelf.tableView reloadData];
            }
        }

    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            DLog(@"api1 error:%@ responsestr = %@",request1.error,request1.responseString);
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            DLog(@"api2 error:%@ responsestr = %@",request2.error,request2.responseString);
        }
    }];
}

- (void)switchIsClicked:(UISwitch *)sw
{
    Api *api = [Api apiPostUrl:@"work/price/changeWhetherTopen" para:@{@"var":@(sw.on)} description:@"更改状态"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
    if(sw.on){
        self.btn.hidden = NO;
    }else{
        self.btn.hidden = YES;
    }
}



-(void)addCarAuth:(UITapGestureRecognizer *)reg
{
    XingShiZhengCertificationCtrl *vc = [XingShiZhengCertificationCtrl new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)makeHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 70)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    topview.backgroundColor = HexColor(0xf9f9f9);
    self.descLabel = [Utils createLabelWithTitle:@"提供旅行用车服务" titleColor:HexColor(0x999999) fontSize:14];
    self.btnSwitch = [[UISwitch alloc] init];
    [self.btnSwitch addTarget:self action:@selector(switchIsClicked:) forControlEvents:UIControlEventValueChanged];

    [view sd_addSubviews:@[topview,self.descLabel,self.btnSwitch]];
    
    self.descLabel.sd_layout
    .topSpaceToView(topview, 22)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [self.descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.btnSwitch.sd_layout
    .centerYEqualToView(self.descLabel)
    .rightSpaceToView(view, 16)
    .widthIs(42)
    .heightIs(24);
    return view;
}

- (void)addFloatButton{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    if(!self.btn){
        LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"添加车辆" font:LKSystemFont(17) cornerRadius:22];
        self.btn = btn;
        [btn addTapGestureTarget:self action:@selector(addCarAuth:)];
        [keyWindow addSubview:btn];
        btn.sd_layout
        .leftSpaceToView(keyWindow, 20)
        .rightSpaceToView(keyWindow, 20)
        .bottomSpaceToView(keyWindow, 33)
        .heightIs(44);
    }else{
        [keyWindow addSubview:self.btn];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarAuthListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarAuthListCell"];
    CarSetupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.carSetupListModel = model;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 152;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarSetupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    CarPriceSetupCtrl *vc = [CarPriceSetupCtrl new];
    vc.model = model;
    vc.finishBlock = ^(CarSetupListEntity * _Nonnull model) {
        for(CarSetupListEntity *entity in self.dataArray){
            if([entity.carId integerValue]==[model.carId integerValue]){
                entity.minimumPrice = model.minimumPrice;
                entity.maximumPrice = model.maximumPrice;
                entity.minimumMileage = model.minimumMileage;
                entity.maximumMileage = model.maximumMileage;
                break;
            }
        }
    };
    [self.navigationController pushViewController:vc animated:YES];
}

@end
