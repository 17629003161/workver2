//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface CarSetupListEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *maximumPrice;
@property (nonatomic,copy) NSString *plate_no;
@property (nonatomic,strong) NSNumber *carAge;
@property (nonatomic,strong) NSNumber *minimumPrice;
@property (nonatomic,strong) NSNumber *maximumMileage;
@property (nonatomic,strong) NSNumber *carId;
@property (nonatomic,strong) NSNumber *minimumMileage;
@property (nonatomic,copy) NSString *carModel;
 


-(id)initWithJson:(NSDictionary *)json;

@end
