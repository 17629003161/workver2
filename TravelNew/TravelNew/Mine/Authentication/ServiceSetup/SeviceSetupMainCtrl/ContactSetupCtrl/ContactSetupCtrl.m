//
//  ContactSetupCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ContactSetupCtrl.h"
#import "YTKBatchRequest.h"
#import "ContactSetupEntity.h"

@interface ContactSetupCtrl ()
@property(nonatomic,strong) UITextField *phoneField;
@property(nonatomic,strong) UITextField *wxField;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UISwitch *btnSwitch;
@property(nonatomic,strong) ContactSetupEntity *contactModel;
@end

@implementation ContactSetupCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"马上联系" titleColor:nil];
    [self addRightButton];
    self.view.backgroundColor = HexColor(0xf9f9f9);
    UIView *view = [self configViews];
    [self.view addSubview:view];
    view.sd_layout
    .topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(169);
    
    [self sendBatchRequest];
    
}

-(void)setContactModel:(ContactSetupEntity *)contactModel
{
    _contactModel = contactModel;
    self.phoneField.text = contactModel.phone;
    self.wxField.text = contactModel.wxId;
}

-(void)transformView:(NSNotification *)aNSNotification
{

}


- (void)sendBatchRequest{
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"work/information/whetherTopen" para:@{} description:@"查询是否开通"];;
    Api *api2 = [Api apiPostUrl:@"work/information/ltContactInformation" para:@{} description:@"查询联系方式"];;
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        DLog(@"查询是否开通 jsonstr  = %@",request1.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSNumber *state = [json objectForKey:@"data"];
                weakSelf.btnSwitch.on = [state boolValue];
            }
        }

        YTKRequest *request2 = batchRequest.requestArray[1];
        DLog(@"查询联系方式 jsonstr  = %@",request2.responseString);
        json = [NSString jsonStringToDictionary:request2.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([json containsKey:@"data"]){
                NSDictionary *data = [json objectForKey:@"data"];
                weakSelf.contactModel = [[ContactSetupEntity alloc] initWithJson:data];
            }
        }

    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            DLog(@"api1 error:%@ responsestr = %@",request1.error,request1.responseString);
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            DLog(@"api2 error:%@ responsestr = %@",request2.error,request2.responseString);
        }
    }];
}


- (void)btnSaveIsClicked:(id)sender
{
    NSString *phone = self.phoneField.text;
    if(![LKInputChecker checkTelNumber:phone]){
        [MBProgressHUD showMessage:@"手机号格式不正确"];
        return;
    }
    NSString *wxNumber = self.wxField.text;
    Api *api = [Api apiPostUrl:@"work/information/add"
                          para:@{@"wxId":wxNumber,
                                 @"phone":phone}
                   description:@"新增修改联系方式"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            [self.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)addRightButton{
    UIButton* btn= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    UIBarButtonItem* barItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    [btn addTarget:self action:@selector(btnSaveIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = barItem;
}

- (void)switchIsClicked:(UISwitch *)sw
{
    Api *api = [Api apiPostUrl:@"work/house/changeWhetherTopen" para:@{@"var":@(sw.on)} description:@"更改状态"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (UIView *)configViews{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    self.descLabel = [Utils createLabelWithTitle:@"提供线路规划，风土人情介绍等" titleColor:HexColor(0x999999) fontSize:14];
    self.btnSwitch = [[UISwitch alloc] init];
    [self.btnSwitch addTarget:self action:@selector(switchIsClicked:) forControlEvents:UIControlEventValueChanged];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xDCDCDC);
    UILabel *tipLabel1 = [self createTiplabel:@"手机号"];
    UILabel *tipLabel2 = [self createTiplabel:@"微信号"];
    UITextField *phoneField = [self createTextField];
    phoneField.keyboardType = UIKeyboardTypeNumberPad;
    phoneField.placeholder = @"请输入手机号";
    self.phoneField = phoneField;
    UITextField *wxField = [self createTextField];
    wxField.placeholder = @"请输入微信号";
    self.wxField = wxField;
    [view sd_addSubviews:@[self.descLabel,self.btnSwitch,
                                line,
                           tipLabel1,phoneField,
                           tipLabel2,wxField]];
    
    self.descLabel.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [self.descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.btnSwitch.sd_layout
    .centerYEqualToView(self.descLabel)
    .rightSpaceToView(view, 16)
    .widthIs(42)
    .heightIs(24);
    
    line.sd_layout
    .topSpaceToView(self.descLabel, 19)
    .rightEqualToView(view)
    .leftEqualToView(self.descLabel)
    .heightIs(0.5);

    tipLabel1.sd_layout
    .topSpaceToView(line, 20)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [tipLabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneField.sd_layout
    .centerYEqualToView(tipLabel1)
    .leftSpaceToView(tipLabel1, 14)
    .widthIs(160)
    .heightIs(32);
    
    tipLabel2.sd_layout
    .topSpaceToView(tipLabel1, 28)
    .leftEqualToView(tipLabel1)
    .autoHeightRatio(0);
    [tipLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    wxField.sd_layout
    .centerYEqualToView(tipLabel2)
    .leftSpaceToView(tipLabel2, 14)
    .widthIs(160)
    .heightIs(32);
    [view addTapGestureTarget:self action:@selector(viewIsTyped:)];
    return view;
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self.phoneField resignFirstResponder];
    [self.wxField resignFirstResponder];
}

- (UILabel *)createTiplabel:(NSString *)title
{
    return  [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:14];
}

- (UITextField *)createTextField
{
    UITextField *field = [UITextField new];
    field.clearButtonMode = UITextFieldViewModeAlways;
    field.layer.borderWidth = 0.5;
    field.layer.borderColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0].CGColor;
    field.layer.cornerRadius = 6.0;
    [field textLeftOffset:14];
    return field;
}

@end
