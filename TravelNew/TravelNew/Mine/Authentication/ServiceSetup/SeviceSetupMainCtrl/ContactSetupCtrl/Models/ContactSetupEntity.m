//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "ContactSetupEntity.h"

@implementation ContactSetupEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.phone  = [json objectForKey:@"phone"];
		self.userId  = [json objectForKey:@"userId"];
		self.wxId  = [json objectForKey:@"wxId"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.phone forKey:@"zx_phone"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.wxId forKey:@"zx_wxId"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.phone = [aDecoder decodeObjectForKey:@"zx_phone"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.wxId = [aDecoder decodeObjectForKey:@"zx_wxId"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"phone : %@\n",self.phone];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"wxId : %@\n",self.wxId];
	
    return result;
}

@end
