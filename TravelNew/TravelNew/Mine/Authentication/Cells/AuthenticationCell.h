//
//  AuthenticationCell.h
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthenticationCell : BaseCustomTableViewCell
@property(nonatomic,strong) UILabel *numberLabel;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descriptionLabel;
@property(nonatomic,strong) UILabel *resultLabel;
@property(nonatomic,assign) BOOL isOk;
@end

NS_ASSUME_NONNULL_END
