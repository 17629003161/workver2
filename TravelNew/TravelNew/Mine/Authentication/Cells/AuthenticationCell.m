//
//  AuthenticationCell.m
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AuthenticationCell.h"


@implementation AuthenticationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIsOk:(BOOL)isOk
{
    _isOk = isOk;
    if(!isOk){
        self.numberLabel.alpha = 0.3;
        self.nameLabel.alpha = 0.3;
        self.descriptionLabel.alpha = 0.3;
        self.resultLabel.alpha = 0.3;
    }else{
        self.numberLabel.alpha = 1.0;
        self.nameLabel.alpha = 1.0;
        self.descriptionLabel.alpha = 1.0;
        self.resultLabel.alpha = 1.0;
    }
}


- (void)configViews{
    self.numberLabel = [Utils createBoldLabelWithTitle:@"1" titleColor:HexColor(0x333333) fontSize:40];
    self.nameLabel = [Utils createBoldLabelWithTitle:@"实名认证" titleColor:HexColor(0x333333) fontSize:16];
    self.descriptionLabel = [Utils createLabelWithTitle:@"输入真实姓名跟身份证号码" titleColor:HexColor(0x999999) fontSize:12];
    self.resultLabel = [Utils createBoldLabelWithTitle:@"去完成" titleColor:HexColor(0x333333) fontSize:16];
    [self.contentView sd_addSubviews:@[_numberLabel,_nameLabel,_descriptionLabel,_resultLabel]];
    _numberLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_numberLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _nameLabel.sd_layout
    .topSpaceToView(self.contentView, 18)
    .leftSpaceToView(_numberLabel, 14)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _descriptionLabel.sd_layout
    .leftEqualToView(_nameLabel)
    .topSpaceToView(_nameLabel, 3)
    .autoHeightRatio(0);
    [_descriptionLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _resultLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    [_resultLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}


@end
