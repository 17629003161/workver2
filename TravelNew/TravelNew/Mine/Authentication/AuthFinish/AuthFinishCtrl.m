//
//  AuthFinishCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/20.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AuthFinishCtrl.h"

@interface AuthFinishCtrl ()

@end

@implementation AuthFinishCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [self layout];
}

- (void)btnIsClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)layout{
    UIImageView *imgv = ImageViewWithImage(@"finish_rightmark");
    UILabel *tipLabel = [Utils createBoldLabelWithTitle:@"认证提交"
                                             titleColor:HexColor(0x323232)
                                               fontSize:20];
    UILabel *tipLabel2 = [Utils createLabelWithTitle:@"系统审核中，预计1个工作日内会以短信形式通知您审核结果"
                                          titleColor:HexColor(666666)
                                            fontSize:15];
    tipLabel2.textAlignment = NSTextAlignmentCenter;
    tipLabel2.numberOfLines = 0;
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"完成"
                                           font:LKSystemFont(17)
                                   cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [self.view sd_addSubviews:@[imgv,
                                tipLabel,
                                tipLabel2,
                                btn]];
    imgv.sd_layout
    .topSpaceToView(self.view, 56)
    .centerXEqualToView(self.view)
    .widthIs(80)
    .heightIs(80);
    
    tipLabel.sd_layout
    .topSpaceToView(imgv, 30)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    tipLabel2.sd_layout
    .topSpaceToView(tipLabel, 9)
    .leftSpaceToView(self.view, 50)
    .rightSpaceToView(self.view, 50)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(tipLabel2, 74)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 20)
    .heightIs(44);
}

@end
