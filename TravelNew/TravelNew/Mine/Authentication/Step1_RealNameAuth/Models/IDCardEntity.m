//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "IDCardEntity.h"

@implementation IDCardEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.name  = [json objectForKey:@"name"];
		self.idNumber  = [json objectForKey:@"idNumber"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"zx_name"];
	[aCoder encodeObject:self.idNumber forKey:@"zx_idNumber"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.name = [aDecoder decodeObjectForKey:@"zx_name"];
		self.idNumber = [aDecoder decodeObjectForKey:@"zx_idNumber"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"name : %@\n",self.name];
	result = [result stringByAppendingFormat:@"idNumber : %@\n",self.idNumber];
	
    return result;
}

@end
