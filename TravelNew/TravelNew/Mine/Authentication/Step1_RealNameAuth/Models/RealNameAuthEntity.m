//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RealNameAuthEntity.h"

@implementation RealNameAuthEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.name  = [json objectForKey:@"name"];
		self.idCard  = [json objectForKey:@"idCard"];
		self.userId  = [json objectForKey:@"userId"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"zx_name"];
	[aCoder encodeObject:self.idCard forKey:@"zx_idCard"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.name = [aDecoder decodeObjectForKey:@"zx_name"];
		self.idCard = [aDecoder decodeObjectForKey:@"zx_idCard"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"name : %@\n",self.name];
	result = [result stringByAppendingFormat:@"idCard : %@\n",self.idCard];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	
    return result;
}

@end
