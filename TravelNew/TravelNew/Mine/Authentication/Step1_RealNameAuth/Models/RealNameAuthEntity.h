//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface RealNameAuthEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *idCard;
@property (nonatomic,copy) NSString *userId;
 


-(id)initWithJson:(NSDictionary *)json;

@end
