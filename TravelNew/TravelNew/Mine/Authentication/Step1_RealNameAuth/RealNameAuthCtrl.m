//
//  RealNameAuthCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

//实名认证

#import "RealNameAuthCtrl.h"
#import "TakeIDPictureCtrl.h"
#import "liveness/Liveness.h"
#import "MD5Encrypt.h"
#import "UIImage+ToBase64.h"
#import "IDCardEntity.h"
#import "LKShowPictureView.h"


@interface RealNameAuthCtrl ()<UITextFieldDelegate>
@property(nonatomic,strong) UITextField *fieldIDCard;
@property(nonatomic,strong) UITextField *fieldName;
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) IDCardEntity *model;

@property(nonatomic,strong) UIImageView *faceImageView;
@property(nonatomic,strong) UIImageView *backImageView;
@property(nonatomic,strong) UIImage *faceImage;
@property(nonatomic,strong) UIImage *backImage;
@property(nonatomic,strong) UIView *bottomView;


@end

@implementation RealNameAuthCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"实名认证" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF5F5F7);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [self.view addTapGestureTarget:self action:@selector(viewIsClicked:)];
    [self layout];
}

-(void)viewIsClicked:(UITapGestureRecognizer *)reg
{
    [self.fieldIDCard resignFirstResponder];
    [self.fieldName resignFirstResponder];
}

-(void)setModel:(IDCardEntity *)model
{
    _model = model;
    self.bottomView.hidden = NO;
    self.fieldIDCard.text = model.idNumber;
    self.fieldName.text = model.name;
}

#pragma mark - 下一步
- (void)nextStepIsClicked:(id)sender{
    [self realNameAuth];
    
    //活体检测
   // [self detect];
}

- (void)realNameAuth{
    if(self.faceImage==nil){
        [MBProgressHUD showMessage:@"请拍摄身份证正面照片"];
        return;
    }
    if(self.backImage==nil){
        [MBProgressHUD showMessage:@"请拍摄身份证反面照片"];
        return;
    }
    NSString *facebase64 = [self.faceImage encodeToBase64String];
    NSString *backbase64 = [self.backImage encodeToBase64String];
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/idCardAuthentication_block1A"
                          para:@{@"frontOfIDCard":facebase64,
                                 @"backOfIDCard":backbase64
                                }
                   description:@"推荐官身份证识别"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr= %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            NSDictionary *dict = [json objectForKey:@"data"];
            IDCardEntity *entity = [[IDCardEntity alloc] initWithJson:dict];
            self.model = entity;
            [self faceDetect];
        }
            [MBProgressHUD showMessage:json[@"msg"]];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

-(void)setFaceImage:(UIImage *)faceImage
{
    _faceImage = faceImage;
    self.faceImageView.image = faceImage;
}

-(void)setBackImage:(UIImage *)backImage
{
    _backImage = backImage;
    self.backImageView.image = backImage;
}


#pragma mark - 拍照按钮按下动作
- (void)takePhotoFaceIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    TakeIDPictureCtrl *vc = [TakeIDPictureCtrl new];
    vc.didSelectImageBlock = ^(UIImage * _Nonnull image) {
        self.faceImage = image;
        weakSelf.faceImageView.image = image;
        UIImageView *sampleImageView = [weakSelf.faceImageView viewWithTag:2000];
        sampleImageView.hidden = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)takePhotoBackIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    TakeIDPictureCtrl *vc = [TakeIDPictureCtrl new];
    vc.didSelectImageBlock = ^(UIImage * _Nonnull image) {
        self.backImage = image;
        weakSelf.backImageView.image = image;
        UIImageView *sampleImageView = [weakSelf.backImageView viewWithTag:2000];
        sampleImageView.hidden = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)layout{
    UIImageView *faceimgv = [self createPhotoView:@"id_card_face" tipTitle:@"点击拍摄身份证正面"];
    UIImageView *carema1 = [faceimgv viewWithTag:1000];
    [carema1 addTapGestureTarget:self action:@selector(takePhotoFaceIsClicked:)];
    self.faceImageView = faceimgv;
    
    UIImageView *backimgv = [self createPhotoView:@"id_card_back" tipTitle:@"点击拍摄身份证反面面"];
    UIImageView *carema2 = [backimgv viewWithTag:1000];
    [carema2 addTapGestureTarget:self action:@selector(takePhotoBackIsClicked:)];
    self.backImageView = backimgv;
    UIView  *bottomView = [self createBottomView];
    self.bottomView = bottomView;
    //self.bottomView.hidden = YES;

    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"下一步"
                                           font:LKSystemFont(17)
                                   cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(nextStepIsClicked:)];
    self.btn = btn;
    [self.view sd_addSubviews:@[faceimgv,backimgv,bottomView,btn]];
    
    faceimgv.sd_layout
    .topSpaceToView(self.view, 20)
    .centerXEqualToView(self.view)
    .widthIs(260)
    .heightIs(120);
    
    backimgv.sd_layout
    .topSpaceToView(faceimgv,20)
    .centerXEqualToView(self.view)
    .widthIs(260)
    .heightIs(120);
    
    bottomView.sd_layout
    .topSpaceToView(backimgv, 18)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(100);
    
    btn.sd_layout
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .bottomSpaceToView(self.view,33+SAFEVIEWBOTTOMMARGIN)
    .heightIs(44);
}


- (UIView *)createBottomView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
    view.backgroundColor = [UIColor whiteColor];

    UILabel *tipLabel1 = [self createLabel:@"姓名"];
    UITextField *nameField = [self createTextField:@"请输入真实姓名"];
    nameField.userInteractionEnabled = NO;
    self.fieldName = nameField;
    
    UILabel *tipLabel2 = [self createLabel:@"证件号码"];
    UITextField *fieldIDCard = [self createTextField:@"请输入身份证号码"];
    fieldIDCard.userInteractionEnabled = NO;
    self.fieldIDCard = fieldIDCard;

    UIView *line1 = [self createLine];
    [view sd_addSubviews:@[tipLabel1,nameField,
                           line1,
                           tipLabel2,fieldIDCard
                           ]];
    tipLabel1.sd_layout
    .topSpaceToView(view, 14)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [tipLabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    nameField.sd_layout
    .leftSpaceToView(view, 94)
    .rightSpaceToView(view, 20)
    .centerYEqualToView(tipLabel1)
    .heightIs(30);
    
    line1.sd_layout
    .topSpaceToView(tipLabel1, 14)
    .leftEqualToView(tipLabel1)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    tipLabel2.sd_layout
    .topSpaceToView(line1, 14)
    .leftEqualToView(tipLabel1)
    .autoHeightRatio(0);
    [tipLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    fieldIDCard.sd_layout
    .leftEqualToView(nameField)
    .rightSpaceToView(view, 20)
    .centerYEqualToView(tipLabel2)
    .heightIs(30);
    [view setupAutoHeightWithBottomView:fieldIDCard bottomMargin:14];
    return view;
}

- (void)photoIsClicked:(UITapGestureRecognizer *)reg
{
    UIImageView *imgv = (UIImageView *)reg.view;
    NSArray *picAry = @[imgv.image];
    LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    view.pictureArray = picAry;
    [view show:CGRectMake(0, 0, kScreenWidth, kScreenWidth)];
}

- (UIImageView *)createPhotoView:(NSString *)sampleImgvName tipTitle:(NSString *)tip{
    UIImageView *bgimgv = [UIImageView new];
    bgimgv.layer.cornerRadius = 4;
    bgimgv.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.57].CGColor;
    bgimgv.layer.shadowOffset = CGSizeMake(0,2);
    bgimgv.layer.shadowOpacity = 1;
    bgimgv.userInteractionEnabled = YES;
    bgimgv.image = [UIImage imageWithColor:HexColor(0xAECAF8)];
   // [bgimgv addTapGestureTarget:self action:@selector(photoIsClicked:)];
    UIImageView *sampleImgv = ImageViewWithImage(sampleImgvName);
    sampleImgv.tag = 2000;
    UIImageView *caremaimgv = ImageViewWithImage(@"takephoto");
    caremaimgv.tag = 1000;
    UILabel *tipLabel = [Utils createLabelWithTitle:tip titleColor:HexColor(0xffffff) fontSize:14];
    
    [bgimgv sd_addSubviews:@[sampleImgv,caremaimgv,tipLabel]];
    
    sampleImgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .leftSpaceToView(bgimgv, 50)
    .rightSpaceToView(bgimgv, 50)
    .topSpaceToView(bgimgv, 10)
    .bottomSpaceToView(bgimgv, 10);
    
    caremaimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .widthIs(30)
    .heightIs(30);
    
    tipLabel.sd_layout
    .topSpaceToView(sampleImgv, 10)
    .centerXEqualToView(bgimgv)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return bgimgv;
}



- (UILabel *)createLabel:(NSString *)str
{
    UILabel *label = [Utils createLabelWithTitle:str titleColor:HexColor(0x333333) fontSize:16];
    return label;
}

- (UITextField *)createTextField:(NSString *)placeHolder
{
    UITextField *field = [UITextField new];
    field.font = LKSystemFont(16);
    field.textColor = HexColor(0x333333);
    return field;
}

- (UIView *)createLine
{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xefefef);
    return view;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}


//----------------------------活体检测 检测 -----------------------
#pragma mark - 活体检测

- (void)faceDetect{
    NSDictionary *param = @{@"actions":@"1279",@"actionsNum":@"3"};
    [[Liveness shareInstance] startProcess:self withParam:param withDelegate:self];
}

//获取检测结果
- (void)onLiveDetectCompletion:(NSDictionary *)result{
    // 错误码, 0 检测成功
    NSString *code = [result objectForKey:@"code"];
    // 错误信息
    NSString *msg = [result objectForKey:@"msg"];
    // 检测通过,code="0"时返回图片保存路径，完整图像
    NSString *passImgPath = [result objectForKey:@"passImgPath"];
    // 检测通过,code="0"时返脸部 base64，剪裁后的脸部图像
    NSString *passFace = [result objectForKey:@"passFace"];
    if(passFace){
        [self upLoadFicePicture:passFace];
    }
}

- (void)upLoadFicePicture:(NSString *)passFace
{
    LKWeakSelf
    LKLocationManager *manager = [LKLocationManager sharedManager];
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/recommenderSFaceRecognition_block1A"
                          para:@{@"lat":@(manager.coordinate.latitude),
                                 @"lon":@(manager.coordinate.longitude),
                                 @"cityid":user.cityid,
                                 @"imageBase64":passFace}
                   description:@"推荐官人脸识别_block1A"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            BOOL isok = [[json objectForKey:@"data"] boolValue];
            [MBProgressHUD showMessage:json[@"msg"]];
            if(isok){
                if(self.FinishBlock)
                    self.FinishBlock();
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
    
}




@end
