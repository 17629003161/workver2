//
//  DriverLicenseCertificationCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

//驾驶证认证

#import "DriverLicenseCertificationCtrl.h"
#import "MD5Encrypt.h"
#import "DetectApi.h"
#import "UIImage+ToBase64.h"
#import "TakeIDPictureCtrl.h"
#import "DriverLicenseEntity.h"
#import "DriverInfoEntity.h"
#import "LKShowPictureView.h"
#import "SetupSelectDateDialog.h"
#import "LKSingleSelectDlg.h"
#import "LKIDSampleDialg.h"

@interface DriverLicenseCertificationCtrl ()<UITextFieldDelegate>

@property(nonatomic,strong) UITextField *dateField;
@property(nonatomic,strong) UITextField *fileNumberField;
@property(nonatomic,strong) UITextField *typeField;

@property(nonatomic,strong) UIImageView *idImageView;
@property(nonatomic,strong) LKGadentButton *btnNextStep;

@property(nonatomic,strong) DriverLicenseEntity *model;
@property(nonatomic,strong) DriverInfoEntity *driverInfoModel;
@property(nonatomic,strong) UIImage *image;

@end

@implementation DriverLicenseCertificationCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF5F5F7);
    [self setTitle:@"驾驶证信息" titleColor:nil];
    [self.view addSubview:self.scrollView];
    self.scrollView.frame = self.view.bounds;
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight+200);
    [self.scrollView addTapGestureTarget:self action:@selector(viewIsTaped:)];
    [self layout];
    [self getDriverInfo];
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    [self.fileNumberField resignFirstResponder];
}



-(void)transformView:(NSNotification *)aNSNotification
{

}

- (void)setDriverInfoModel:(DriverInfoEntity *)driverInfoModel
{
    _driverInfoModel = driverInfoModel;
    self.btnNextStep.isEnabled = YES;
}

- (void)getDriverInfo{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getDriverInfo" para:@{} description:@"获取推荐官驾驶证信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            DriverInfoEntity *infoModel = [[DriverInfoEntity alloc] initWithJson:data];
            if(infoModel.name.length>0){
                self.driverInfoModel = infoModel;
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}
#pragma mark - 下一步
- (void)nextStepIsClicked:(id)sender
{
    NSString *datestr = self.dateField.text;
    if(datestr.length==0){
        [MBProgressHUD showMessage:@"请选择日期"];
        return;
    }
    LKDateUtils *data = [[LKDateUtils alloc] init];
    data.dateString = datestr;
    NSTimeInterval timeStamp = data.timeInterval/1000;
    NSString *imagebase64 = [self.image encodeToBase64String];
    NSString *fileNumber = self.fileNumberField.text;
    if(self.image==nil){
        [MBProgressHUD showMessage:@"请拍摄驾驶证图片"];
        return;
    }
    if(fileNumber.length != 12){
        [MBProgressHUD showMessage:@"文件编号长度不对"];
        return;
    }
    NSString *type = self.typeField.text;
    if(type.length==0){
        [MBProgressHUD showMessage:@"请选择车型"];
        return;
    }
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/driverDataValidation_block1A"
                          para:@{@"firstTime":@((long)timeStamp),
                                 @"model":type,
                                 @"fileNumble":fileNumber,
                                 @"imgBase64":imagebase64
                          }
                   description:@"驾驶证数据确认block1A"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString  *jsonstr = request.responseString;
        NSLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            if([[json objectForKey:@"data"] boolValue]==YES){
                if(self.FinishBlock)
                    self.FinishBlock();
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        NSString *msg = [json objectForKey:@"msg"];
        [MBProgressHUD showMessage:msg];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@,%@",request.error,request.responseString);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}


- (void)layout{
    UIImageView *idImgv = [self createPhotoView:@"jiashizhengzhengmian" tipTitle:@"点击扫描驾驶证"];
    UIImageView *carema1 = [idImgv viewWithTag:1000];
    [carema1 addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
    self.idImageView = idImgv;
    
    
    UIView *bottomView = [self createBottomView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"下一步" font:LKSystemFont(17) cornerRadius:22];
    btn.layer.cornerRadius = 22;
    btn.layer.masksToBounds = YES;
    [btn addTapGestureTarget:self action:@selector(nextStepIsClicked:)];
   // btn.isEnabled = NO;
    self.btnNextStep = btn;
    [bottomView addTapGestureTarget:self action:@selector(viewIsTaped:)];
    [self.scrollView addSubview:self.idImageView];
    [self.scrollView addSubview:bottomView];
    [self.scrollView sd_addSubviews:@[self.idImageView,bottomView,btn]];
    self.idImageView.sd_layout
    .topSpaceToView(self.scrollView, 20)
    .centerXEqualToView(self.scrollView)
    .widthIs(260)
    .heightIs(120);
    
    bottomView.sd_layout
    .topSpaceToView(self.idImageView, 20)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    btn.sd_layout
    .topSpaceToView(bottomView, 40)
    .leftSpaceToView(self.scrollView, 28)
    .rightSpaceToView(self.scrollView, 28)
    .heightIs(44);
    

}

- (UITextField *)createInputField{
    UITextField *textField =  [[UITextField alloc] init];
    textField.font = [UIFont boldSystemFontOfSize:16];
    [textField textLeftOffset:16];
    textField.textColor = HexColor(0x666666);
    textField.backgroundColor = HexColor(0xF8F9FB);
    textField.layer.cornerRadius = 4;
    UIImageView *arrow = [UIImageView new];
    arrow.tag = 1000;
    arrow.image = [UIImage imageNamed:@"arrow_left_icon"];
    [textField sd_addSubviews:@[arrow]];
    arrow.sd_layout
    .centerYEqualToView(textField)
    .rightSpaceToView(textField, 10)
    .widthIs(20)
    .heightIs(20);
    return textField;
}

- (void)selectDateIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    [self.fileNumberField resignFirstResponder];
    SetupSelectDateDialog *dlg = [SetupSelectDateDialog new];
    dlg.selectDateBlock = ^(NSString * _Nonnull dateStr) {
        weakSelf.dateField.text = dateStr;
    };
    [dlg show];
}

-(void)selectTypeIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    [self.fileNumberField resignFirstResponder];
    LKSingleSelectDlg *dlg = [LKSingleSelectDlg new];
    [dlg setDataArray:@[@"A1",@"A2",@"A3",@"B1",@"B2",@"C1",@"C2",@"C3"]];
    dlg.selectBlock = ^(NSString * _Nonnull string) {
        weakSelf.typeField.text = string;
    };
    [dlg show];
}

- (void)questionIsClicked:(UITapGestureRecognizer *)reg
{
    LKIDSampleDialg *dlg = [[LKIDSampleDialg alloc] initWithFrame:CGRectMake(0, 0, 270, 321)];
    [dlg show];
}

- (UIView *)createBottomView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"初次领证日期"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    UITextField *field1 = [self createInputField];
    [field1 setUserInteractionEnabled:NO];
    field1.placeholder = @"请选择";
    [field1 addTapGestureTarget:self action:@selector(selectDateIsClicked:)];
    self.dateField = field1;
    
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"请填写档案编号"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    UIImageView *imgv2 = ImageViewWithImage(@"wenhao");
    [imgv2 addTapGestureTarget:self action:@selector(questionIsClicked:)];
    
    UITextField *field2 = [self createInputField];
    UIImageView *arrow = [field2 viewWithTag:1000];
    arrow.hidden = YES;
    field2.placeholder = @"请填写档案编号";
    field2.keyboardType = UIKeyboardTypeNumberPad;
    field2.delegate = self;
    self.fileNumberField = field2;
    
    UILabel *label3 = [Utils createBoldLabelWithTitle:@"准驾车型"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    
    UITextField *field3 = [self createInputField];
    [field3 setUserInteractionEnabled:NO];
    field3.placeholder = @"请选择";
    [field3 addTapGestureTarget:self action:@selector(selectTypeIsClicked:)];
    self.typeField = field3;
    
    [view sd_addSubviews:@[label1,field1,
                           label2,imgv2,field2,
                           label3,field3]];
    label1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .autoWidthRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field1.sd_layout
    .topSpaceToView(label1, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    label2.sd_layout
    .topSpaceToView(field1, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv2.sd_layout
    .centerYEqualToView(label2)
    .leftSpaceToView(label2, 8)
    .widthIs(18)
    .heightIs(18);
    
    
    field2.sd_layout
    .topSpaceToView(label2, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    label3.sd_layout
    .topSpaceToView(field2, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field3.sd_layout
    .topSpaceToView(label3, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    [view setupAutoHeightWithBottomView:field3 bottomMargin:30];
    return view;

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.fileNumberField){
        NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if(str.length>12){
            return NO;
        }else{
            return YES;
        }
    }
    return YES;
}

#pragma mark - 拍照按钮按下动作
- (void)takePhotoIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    TakeIDPictureCtrl *vc = [TakeIDPictureCtrl new];
    vc.didSelectImageBlock = ^(UIImage * _Nonnull image) {
        self.image = image;
        weakSelf.idImageView.image = image;
        UIImageView *sampImageview = [weakSelf.idImageView viewWithTag:2000];
        sampImageview.hidden = YES;
        
   //     [weakSelf driverLicenseDetect:image];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)photoIsClicked:(UITapGestureRecognizer *)reg
{
        UIImageView *imgv = (UIImageView *)reg.view;
  //      CGRect startFrame = [tapeView convertRect:tapeView.bounds toView:self.view];
        LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        view.pictureArray = @[imgv.image];
        [view show:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}

- (UIImageView *)createPhotoView:(NSString *)sampleImgvName tipTitle:(NSString *)tip{
    UIImageView *bgimgv = [UIImageView new];
    bgimgv.layer.cornerRadius = 4;
    bgimgv.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.57].CGColor;
    bgimgv.layer.shadowOffset = CGSizeMake(0,2);
    bgimgv.layer.shadowOpacity = 1;
    bgimgv.userInteractionEnabled = YES;
    bgimgv.image = [UIImage imageWithColor:HexColor(0xAECAF8)];
  //  [bgimgv addTapGestureTarget:self action:@selector(photoIsClicked:)];
    UIImageView *sampleImgv = ImageViewWithImage(sampleImgvName);
    sampleImgv.tag = 2000;
    UIImageView *caremaimgv = ImageViewWithImage(@"takephoto");
    caremaimgv.tag = 1000;
    UILabel *tipLabel = [Utils createLabelWithTitle:tip titleColor:HexColor(0xffffff) fontSize:14];
    
    [bgimgv sd_addSubviews:@[sampleImgv,caremaimgv,tipLabel]];
    
    sampleImgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .leftSpaceToView(bgimgv, 50)
    .rightSpaceToView(bgimgv, 50)
    .topSpaceToView(bgimgv, 10)
    .bottomSpaceToView(bgimgv, 10);
    
    caremaimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .widthIs(30)
    .heightIs(30);
    
    tipLabel.sd_layout
    .topSpaceToView(sampleImgv, 10)
    .centerXEqualToView(bgimgv)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return bgimgv;
}

- (UITextField *)createTextField{
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(0, 120, kScreenWidth-140, 50)];
    field.textColor = HexColor(0x333333);
    field.font = LKSystemFont(16);
    field.textAlignment = NSTextAlignmentRight;
    field.clearButtonMode = UITextFieldViewModeWhileEditing;
    return field;
}

- (UIView *)createInputBgView:(NSString *)title{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0xB3B3B3) fontSize:16];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xB3B3B3);
    UITextField *field = [self createTextField];
    field.clearButtonMode = UITextFieldViewModeAlways;
    field.tag = 1000;
    [view sd_addSubviews:@[label,line,field]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field.sd_layout
    .leftSpaceToView(label, 10)
    .rightSpaceToView(view, 20)
    .topSpaceToView(view, 0)
    .heightIs(50);
    
    line.sd_layout
    .leftEqualToView(label)
    .bottomEqualToView(view)
    .rightEqualToView(view)
    .heightIs(0.5);
    return view;
}


@end
