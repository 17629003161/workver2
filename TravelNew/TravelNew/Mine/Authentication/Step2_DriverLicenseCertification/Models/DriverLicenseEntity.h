//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//

//驾驶证

#import <Foundation/Foundation.h>


@interface DriverLicenseEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *valid_from;
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *nationality;
@property (nonatomic,copy) NSString *version;
@property (nonatomic,copy) NSString *image_base64;
@property (nonatomic,copy) NSString *date_of_first_issue;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *issue_by;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *birthday;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *valid_date;
@property (nonatomic,copy) NSString *valid_for;
@property (nonatomic,copy) NSString *gender;
@property (nonatomic,copy) NSString *name;
 


-(id)initWithJson:(NSDictionary *)json;

@end
