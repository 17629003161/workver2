//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "DriverInfoEntity.h"

@implementation DriverInfoEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.valid_from  = [json objectForKey:@"valid_from"];
        self.id  = [json objectForKey:@"id"];
        self.nationality  = [json objectForKey:@"nationality"];
        self.version  = [json objectForKey:@"version"];
        self.image_base64  = [json objectForKey:@"image_base64"];
        self.driverId  = [json objectForKey:@"driverId"];
        self.file_number  = [json objectForKey:@"file_number"];
        self.date_of_first_issue  = [json objectForKey:@"date_of_first_issue"];
        self.type  = [json objectForKey:@"type"];
        self.issue_by  = [json objectForKey:@"issue_by"];
        self.address  = [json objectForKey:@"address"];
        self.birthday  = [json objectForKey:@"birthday"];
        self.userId  = [json objectForKey:@"userId"];
        self.valid_date  = [json objectForKey:@"valid_date"];
        self.valid_for  = [json objectForKey:@"valid_for"];
        self.score  = [json objectForKey:@"score"];
        self.gender  = [json objectForKey:@"gender"];
        self.name  = [json objectForKey:@"name"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.valid_from forKey:@"zx_valid_from"];
    [aCoder encodeObject:self.id forKey:@"zx_id"];
    [aCoder encodeObject:self.nationality forKey:@"zx_nationality"];
    [aCoder encodeObject:self.version forKey:@"zx_version"];
    [aCoder encodeObject:self.image_base64 forKey:@"zx_image_base64"];
    [aCoder encodeObject:self.driverId forKey:@"zx_driverId"];
    [aCoder encodeObject:self.file_number forKey:@"zx_file_number"];
    [aCoder encodeObject:self.date_of_first_issue forKey:@"zx_date_of_first_issue"];
    [aCoder encodeObject:self.type forKey:@"zx_type"];
    [aCoder encodeObject:self.issue_by forKey:@"zx_issue_by"];
    [aCoder encodeObject:self.address forKey:@"zx_address"];
    [aCoder encodeObject:self.birthday forKey:@"zx_birthday"];
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
    [aCoder encodeObject:self.valid_date forKey:@"zx_valid_date"];
    [aCoder encodeObject:self.valid_for forKey:@"zx_valid_for"];
    [aCoder encodeObject:self.score forKey:@"zx_score"];
    [aCoder encodeObject:self.gender forKey:@"zx_gender"];
    [aCoder encodeObject:self.name forKey:@"zx_name"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.valid_from = [aDecoder decodeObjectForKey:@"zx_valid_from"];
        self.id = [aDecoder decodeObjectForKey:@"zx_id"];
        self.nationality = [aDecoder decodeObjectForKey:@"zx_nationality"];
        self.version = [aDecoder decodeObjectForKey:@"zx_version"];
        self.image_base64 = [aDecoder decodeObjectForKey:@"zx_image_base64"];
        self.driverId = [aDecoder decodeObjectForKey:@"zx_driverId"];
        self.file_number = [aDecoder decodeObjectForKey:@"zx_file_number"];
        self.date_of_first_issue = [aDecoder decodeObjectForKey:@"zx_date_of_first_issue"];
        self.type = [aDecoder decodeObjectForKey:@"zx_type"];
        self.issue_by = [aDecoder decodeObjectForKey:@"zx_issue_by"];
        self.address = [aDecoder decodeObjectForKey:@"zx_address"];
        self.birthday = [aDecoder decodeObjectForKey:@"zx_birthday"];
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
        self.valid_date = [aDecoder decodeObjectForKey:@"zx_valid_date"];
        self.valid_for = [aDecoder decodeObjectForKey:@"zx_valid_for"];
        self.score = [aDecoder decodeObjectForKey:@"zx_score"];
        self.gender = [aDecoder decodeObjectForKey:@"zx_gender"];
        self.name = [aDecoder decodeObjectForKey:@"zx_name"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"valid_from : %@\n",self.valid_from];
    result = [result stringByAppendingFormat:@"id : %@\n",self.id];
    result = [result stringByAppendingFormat:@"nationality : %@\n",self.nationality];
    result = [result stringByAppendingFormat:@"version : %@\n",self.version];
    result = [result stringByAppendingFormat:@"image_base64 : %@\n",self.image_base64];
    result = [result stringByAppendingFormat:@"driverId : %@\n",self.driverId];
    result = [result stringByAppendingFormat:@"file_number : %@\n",self.file_number];
    result = [result stringByAppendingFormat:@"date_of_first_issue : %@\n",self.date_of_first_issue];
    result = [result stringByAppendingFormat:@"type : %@\n",self.type];
    result = [result stringByAppendingFormat:@"issue_by : %@\n",self.issue_by];
    result = [result stringByAppendingFormat:@"address : %@\n",self.address];
    result = [result stringByAppendingFormat:@"birthday : %@\n",self.birthday];
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    result = [result stringByAppendingFormat:@"valid_date : %@\n",self.valid_date];
    result = [result stringByAppendingFormat:@"valid_for : %@\n",self.valid_for];
    result = [result stringByAppendingFormat:@"score : %@\n",self.score];
    result = [result stringByAppendingFormat:@"gender : %@\n",self.gender];
    result = [result stringByAppendingFormat:@"name : %@\n",self.name];
    
    return result;
}


@end
