//
//  DriverLicenseCertificationCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DriverLicenseCertificationCtrl : LKBaseViewController
@property(nonatomic,copy) void (^FinishBlock)(void);
@end

NS_ASSUME_NONNULL_END
