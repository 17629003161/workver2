//
//  XingShiZhengCertificationCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

//行驶证认证

#import "XingShiZhengCertificationCtrl.h"
#import "MD5Encrypt.h"
#import "DetectApi.h"
#import "UIImage+ToBase64.h"
#import "AuthFinishCtrl.h"
#import "TakeIDPictureCtrl.h"
#import "DrivingLicenseInfoEntity.h"
#import "LKShowPictureView.h"
#import "LKKeyBoardView.h"
#import "LKSingleSelectDlg.h"
#import "SelectCarBrandCtrl.h"

@interface XingShiZhengCertificationCtrl ()

@property(nonatomic,strong) UIImageView *idImageView;
@property(nonatomic,strong) UIImage *image;

@property(nonatomic,strong) UIImageView *carImageView;
@property(nonatomic,strong) UIImage *carImage;

@property(nonatomic,assign) NSInteger siteNumber;
@property(nonatomic,strong) NSMutableArray *btnArray;
@property(nonatomic,strong) UITextField *provenceField;
@property(nonatomic,strong) UITextField *carNumberField;
@property(nonatomic,strong) UITextField *carBrandField;
@property(nonatomic,strong) UITextField *carColorField;
@property(nonatomic,strong) UITextField *carYearField;
@property(nonatomic,strong) CarBrandEntity *carBrandEntity;
@property(nonatomic,strong) LKGadentButton *btnCommit;

@end

@implementation XingShiZhengCertificationCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF5F5F7);
    [self setTitle:@"车辆信息" titleColor:nil];
    [self.view addSubview:self.scrollView];
    self.scrollView.frame = self.view.bounds;
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight+260);
    _btnArray = [NSMutableArray new];
    [self layout];
    self.siteNumber = 5;
    [self.scrollView addTapGestureTarget:self action:@selector(viewIsTaped:)];
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    [self.carNumberField resignFirstResponder];
}

-(void)transformView:(NSNotification *)aNSNotification
{

}

#pragma mark -- 确认提交

- (void)submitIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    if(self.image==nil){
        [MBProgressHUD showMessage:@"请拍摄您的行驶证照片"];
        return;
    }
    if(self.carImage==nil){
        [MBProgressHUD showMessage:@"请拍摄车辆照片"];
        return;
    }
    if(self.provenceField.text.length==0){
        [MBProgressHUD showMessage:@"请选择省份"];
        return;
    }
    NSString *carNumber = [NSString stringWithFormat:@"%@%@",self.provenceField.text,self.carNumberField.text];
    carNumber = [carNumber uppercaseString];
    DLog(@"车牌号码：%@\n",carNumber);
    if(carNumber==0){
        [MBProgressHUD showMessage:@"请输入车牌号码"];
        return;
    }
    if(![LKInputChecker checkCarNumber:carNumber]){
        [MBProgressHUD showMessage:@"车牌号不合法"];
        return;
    }
    if(self.carBrandField.text.length==0){
        [MBProgressHUD showMessage:@"请选择车辆品牌"];
        return;
    }
    if(self.carYearField.text.length==0){
        [MBProgressHUD showMessage:@"请选择车辆年限"];
        return;
    }
    if(self.carYearField.text.length==0){
        [MBProgressHUD showMessage:@"请选择车辆颜色"];
        return;
    }
    
    NSString *carBrand = self.carBrandEntity.id;
    NSString *valstr = [self.carYearField.text stringByReplacingOccurrencesOfString:@"年内" withString:@""];
    NSInteger carYear = [valstr integerValue];
    NSString *carColor = self.carColorField.text;
    
    NSString *imagebase64 = [self.image encodeToBase64String];
    NSString *carImageBase64 = [self.carImage encodeToBase64String];
    if(!self.btnCommit.isEnabled) return;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/vehicleLicenceDriverDataValidation_block1A"
                          para:@{@"imgBase64":imagebase64,
                                 @"carNumble":carNumber,
                                 @"carBrand":carBrand,
                                 @"carYear":@(carYear),
                                 @"carColor":carColor,
                                 @"numberOfSeats":@(self.siteNumber),
                                 @"vehiclePhotos":carImageBase64
                          }
                   description:@"行驶证数据确认_block1A"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString  *jsonstr = request.responseString;
        DLog(@"%@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            if([[json objectForKey:@"data"] boolValue]==YES){
                if(weakSelf.FinishBlock)
                    weakSelf.FinishBlock();
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        NSString *msg = [json objectForKey:@"msg"];
        [MBProgressHUD showMessage:msg];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@,%@",request.error,request.responseString);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

#pragma mark - 拍照按钮按下动作
- (void)takeIDCardPhotoIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    TakeIDPictureCtrl *vc = [TakeIDPictureCtrl new];
    vc.didSelectImageBlock = ^(UIImage * _Nonnull image) {
        self.image = image;
        weakSelf.idImageView.image = image;
        UIImageView *sampleImageView = [weakSelf.idImageView viewWithTag:2000];
        sampleImageView.hidden = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)takeCarPhotoIsClicked:(UITapGestureRecognizer *)reg{
    LKWeakSelf
    TakeIDPictureCtrl *vc = [TakeIDPictureCtrl new];
    vc.didSelectImageBlock = ^(UIImage * _Nonnull image) {
        self.carImage = image;
        weakSelf.carImageView.image = image;
        UIImageView *sampleImageView = [weakSelf.carImageView viewWithTag:2000];
        sampleImageView.hidden = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)layout{
    UIImageView *idImgv = [self createPhotoView:@"photo_xingshizheng" tipTitle:@"点击拍摄行驶证"];
    UIImageView *carema1 = [idImgv viewWithTag:1000];
    [carema1 addTapGestureTarget:self action:@selector(takeIDCardPhotoIsClicked:)];
    self.idImageView = idImgv;
    
    UIImageView *carImgv = [self createPhotoView:@"photo_car_sample" tipTitle:@"上传车辆照片"];
    UIImageView *carema2 = [carImgv viewWithTag:1000];
    [carema2 addTapGestureTarget:self action:@selector(takeCarPhotoIsClicked:)];
    self.carImageView = carImgv;
    
    UIView *bottomView = [self createBottomView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认提交" font:LKSystemFont(17) cornerRadius:22];
    btn.layer.cornerRadius = 22;
    btn.layer.masksToBounds = YES;
    [btn addTapGestureTarget:self action:@selector(submitIsClicked:)];
    self.btnCommit = btn;
    
    [self.scrollView addSubview:idImgv];
    [self.scrollView addSubview:carImgv];
    [self.scrollView addSubview:bottomView];
    [self.scrollView addSubview:btn];
    idImgv.sd_layout
    .topSpaceToView(self.scrollView, 20)
    .centerXEqualToView(self.scrollView)
    .widthIs(260)
    .heightIs(120);
    
    carImgv.sd_layout
    .topSpaceToView(idImgv, 20)
    .centerXEqualToView(self.scrollView)
    .widthIs(260)
    .heightIs(120);
    
    bottomView.sd_layout
    .topSpaceToView(carImgv, 20)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    btn.sd_layout
    .topSpaceToView(bottomView, 40)
    .leftSpaceToView(self.scrollView, 28)
    .rightSpaceToView(self.scrollView, 28)
    .heightIs(44);

}

- (void)photoIsClicked:(UITapGestureRecognizer *)reg
{
        UIImageView *imgv = (UIImageView *)reg.view;
  //      CGRect startFrame = [tapeView convertRect:tapeView.bounds toView:self.view];
        LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        view.pictureArray = @[imgv.image];
        [view show:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}

- (UIImageView *)createPhotoView:(NSString *)sampleImgvName tipTitle:(NSString *)tip{
    UIImageView *bgimgv = [UIImageView new];
    bgimgv.layer.cornerRadius = 4;
    bgimgv.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.57].CGColor;
    bgimgv.layer.shadowOffset = CGSizeMake(0,2);
    bgimgv.layer.shadowOpacity = 1;
    bgimgv.userInteractionEnabled = YES;
    bgimgv.image = [UIImage imageWithColor:HexColor(0xAECAF8)];
 //   [bgimgv addTapGestureTarget:self action:@selector(photoIsClicked:)];
    UIImageView *sampleImgv = ImageViewWithImage(sampleImgvName);
    sampleImgv.tag = 2000;
    UIImageView *caremaimgv = ImageViewWithImage(@"takephoto");
    caremaimgv.tag = 1000;
    UILabel *tipLabel = [Utils createLabelWithTitle:tip titleColor:HexColor(0xffffff) fontSize:14];
    
    [bgimgv sd_addSubviews:@[sampleImgv,caremaimgv,tipLabel]];
    
    sampleImgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .leftSpaceToView(bgimgv, 50)
    .rightSpaceToView(bgimgv, 50)
    .topSpaceToView(bgimgv, 10)
    .bottomSpaceToView(bgimgv, 10);
    
    caremaimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .widthIs(30)
    .heightIs(30);
    
    tipLabel.sd_layout
    .topSpaceToView(sampleImgv, 10)
    .centerXEqualToView(bgimgv)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return bgimgv;
}




- (UITextField *)createInputField{
    UITextField *textField =  [[UITextField alloc] init];
    textField.font = [UIFont boldSystemFontOfSize:16];
    [textField textLeftOffset:16];
    textField.textColor = HexColor(0x666666);
    textField.backgroundColor = HexColor(0xF8F9FB);
    textField.layer.cornerRadius = 4;
    UIImageView *arrow = [UIImageView new];
    arrow.tag = 1000;
    arrow.image = [UIImage imageNamed:@"arrow_left_icon"];
    [textField sd_addSubviews:@[arrow]];
    arrow.sd_layout
    .centerYEqualToView(textField)
    .rightSpaceToView(textField, 10)
    .widthIs(20)
    .heightIs(20);
    return textField;
}

- (void)btnSiteIsClicked:(UITapGestureRecognizer *)reg
{
    LKGadentButton *button = (LKGadentButton *)reg.view;
    for (LKGadentButton *btn in self.btnArray) {
        if(button == btn){
            btn.isEnabled = YES;
            self.siteNumber = btn.tag;
        }else{
            btn.isEnabled = NO;
        }
    }
}

-(void)setSiteNumber:(NSInteger)siteNumber
{
    _siteNumber = siteNumber;
    for (LKGadentButton *btn in self.btnArray) {
        if(btn.tag == siteNumber){
            btn.isEnabled = YES;
        }else{
            btn.isEnabled = NO;
        }
    }
}

- (void)selectProvinceIsClicked:(UITapGestureRecognizer *)reg
{
    [self.carNumberField resignFirstResponder];
    LKKeyBoardView *view = [LKKeyBoardView new];
    view.onKeyBoardClickedBlock = ^(NSString * _Nonnull key) {
        self.provenceField.text = key;
    };
    [view show];
}

- (void)carBrandIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    [self.carNumberField resignFirstResponder];
    SelectCarBrandCtrl *vc = [SelectCarBrandCtrl new];
    vc.onSelectBlock = ^(CarBrandEntity * _Nonnull model) {
        self.carBrandField.text = model.name;
        weakSelf.carBrandEntity = model;
    };
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)carYearIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    [self.carNumberField resignFirstResponder];
    LKSingleSelectDlg *dlg = [LKSingleSelectDlg new];
    [dlg setDataArray:@[@"1年内",@"2年内",@"3年内",@"4年内",@"5年内",@"6年内",@"7年内",@"8年内",@"9年内",@"10年内"]];
    dlg.selectBlock = ^(NSString * _Nonnull string) {
        weakSelf.carYearField.text = string;
    };
    [dlg show];
}

- (void)carColorIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    [self.carNumberField resignFirstResponder];
    LKSingleSelectDlg *dlg = [LKSingleSelectDlg new];
    [dlg setDataArray:@[@"白",@"黑",@"银",@"灰",@"红",@"紫",@"金",@"黄",@"棕",@"橙",@"蓝",@"绿",@"其他"]];
    dlg.selectBlock = ^(NSString * _Nonnull string) {
        weakSelf.carColorField.text = [NSString stringWithFormat:@"%@色",string];
    };
    [dlg show];
}

- (UIView *)createBottomView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"车牌号"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    
    UITextField *field = [self createInputField];
    field.placeholder = @"省";
    [field setUserInteractionEnabled:NO];
    [field addTapGestureTarget:self action:@selector(selectProvinceIsClicked:)];
    self.provenceField = field;
    
    UITextField *field1 = [self createInputField];
    UIImageView *arrow = [field1 viewWithTag:1000];
    arrow.hidden = YES;
    field1.placeholder = @"请填写车牌号";
    self.carNumberField = field1;

    
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"车辆品牌"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    UITextField *field2 = [self createInputField];
    field2.placeholder = @"请选择";
    [field2 setUserInteractionEnabled:NO];
    [field2 addTapGestureTarget:self action:@selector(carBrandIsClicked:)];
    self.carBrandField = field2;
    
    
    UILabel *label3 = [Utils createBoldLabelWithTitle:@"车辆年限（年）"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    UITextField *field3 = [self createInputField];
    field3.placeholder = @"请选择";
    [field3 setUserInteractionEnabled:NO];
    [field3 addTapGestureTarget:self action:@selector(carYearIsClicked:)];
    self.carYearField = field3;

    
    UILabel *label4 = [Utils createBoldLabelWithTitle:@"车辆颜色"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    
    UITextField *field4 = [self createInputField];
    [field4 setUserInteractionEnabled:NO];
    field4.placeholder = @"请选择";
    [field4 addTapGestureTarget:self action:@selector(carColorIsClicked:)];
    self.carColorField = field4;
    
    UILabel *label5 = [Utils createBoldLabelWithTitle:@"座位数"
                                           titleColor:HexColor(0x333333)
                                             fontSize:16];
    LKGadentButton *btnSite5 = [LKGadentButton blueButtonWhithTitle:@"5座" font:LKSystemFont(16) cornerRadius:4];
    btnSite5.tag = 5;
    [btnSite5 addTapGestureTarget:self action:@selector(btnSiteIsClicked:)];
    LKGadentButton *btnSite6 = [LKGadentButton blueButtonWhithTitle:@"6座" font:LKSystemFont(16) cornerRadius:4];
    btnSite6.tag = 6;
    [btnSite6 addTapGestureTarget:self action:@selector(btnSiteIsClicked:)];
    LKGadentButton *btnSite7 = [LKGadentButton blueButtonWhithTitle:@"7座" font:LKSystemFont(16) cornerRadius:4];
    btnSite7.tag = 7;
    [btnSite7 addTapGestureTarget:self action:@selector(btnSiteIsClicked:)];
    [self.btnArray addObjectsFromArray:@[btnSite5,btnSite6,btnSite7]];
    
    [view sd_addSubviews:@[label1,field,field1,
                           label2,field2,
                           label3,field3,
                           label4,field4,
                           label5,
                           btnSite5,btnSite6,btnSite7]];
    label1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .autoWidthRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field.sd_layout
    .topSpaceToView(label1, 10)
    .leftSpaceToView(view, 20)
    .widthIs(67)
    .heightIs(40);
    
    field1.sd_layout
    .topSpaceToView(label1, 10)
    .leftSpaceToView(field, 10)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    label2.sd_layout
    .topSpaceToView(field1, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field2.sd_layout
    .topSpaceToView(label2, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    label3.sd_layout
    .topSpaceToView(field2, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field3.sd_layout
    .topSpaceToView(label3, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    label4.sd_layout
    .topSpaceToView(field3, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field4.sd_layout
    .topSpaceToView(label4, 10)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    
    
    label5.sd_layout
    .topSpaceToView(field4, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btnSite5.sd_layout
    .topSpaceToView(label5, 10)
    .leftSpaceToView(view, 20)
    .widthIs(77)
    .heightIs(40);
    
    btnSite6.sd_layout
    .centerYEqualToView(btnSite5)
    .leftSpaceToView(btnSite5, 16)
    .widthIs(77)
    .heightIs(40);
    
    btnSite7.sd_layout
    .centerYEqualToView(btnSite6)
    .leftSpaceToView(btnSite6, 16)
    .widthIs(77)
    .heightIs(40);
    [view setupAutoHeightWithBottomView:btnSite5 bottomMargin:30];
    
    return view;

}


- (UIImageView *)createTopView{
    UIImageView *bgimgv = [UIImageView new];
    bgimgv.layer.cornerRadius = 4;
    bgimgv.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.57].CGColor;
    bgimgv.layer.shadowOffset = CGSizeMake(0,2);
    bgimgv.layer.shadowOpacity = 1;
    bgimgv.userInteractionEnabled = YES;
    bgimgv.image = [UIImage imageWithColor:HexColor(0xAECAF8)];
    [bgimgv addTapGestureTarget:self action:@selector(photoIsClicked:)];
    self.idImageView = bgimgv;
    UIImageView *carimgv = ImageViewWithImage(@"jiashizhengzhengmian");
    UIImageView *caremaimgv = ImageViewWithImage(@"takephoto");
    [caremaimgv addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
    UILabel *tipLabel = [Utils createLabelWithTitle:@"点击扫描行驶证" titleColor:HexColor(0xffffff) fontSize:14];
    [bgimgv sd_addSubviews:@[carimgv,caremaimgv,tipLabel]];
    
    carimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .widthIs(96)
    .heightIs(60);
    
    caremaimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .centerXEqualToView(bgimgv)
    .widthIs(30)
    .heightIs(30);
    
    tipLabel.sd_layout
    .topSpaceToView(carimgv, 10)
    .centerXEqualToView(bgimgv)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return bgimgv;
}

- (UITextField *)createTextField{
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(0, 120, kScreenWidth-140, 50)];
    field.textColor = HexColor(0x333333);
    field.font = LKSystemFont(16);
    field.textAlignment = NSTextAlignmentRight;
    return field;
}


- (UIView *)createInputBgView:(NSString *)title{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0xB3B3B3) fontSize:16];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xB3B3B3);
    UITextField *field = [self createTextField];
    field.clearButtonMode = UITextFieldViewModeWhileEditing;
    field.tag = 1000;
    [view sd_addSubviews:@[label,line,field]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    field.sd_layout
    .leftSpaceToView(view, 140)
    .rightSpaceToView(view, 20)
    .topSpaceToView(view, 0)
    .heightIs(50);
    
    line.sd_layout
    .leftEqualToView(label)
    .bottomEqualToView(view)
    .rightEqualToView(view)
    .heightIs(0.5);
    return view;
}



@end
