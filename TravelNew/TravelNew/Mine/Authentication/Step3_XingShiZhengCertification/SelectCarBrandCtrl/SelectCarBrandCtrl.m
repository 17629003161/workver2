//
//  SelectCarBrandCtrl.m
//  TravelNew
//
//  Created by mac on 2021/1/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "SelectCarBrandCtrl.h"
#import "CarBrandCell.h"
#import "SelectCarSubBrandCtrl.h"

@interface SelectCarBrandCtrl ()

@end

@implementation SelectCarBrandCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[CarBrandCell class] forCellReuseIdentifier:@"CarBrandCell"];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getModelInformation"
                          para:@{}
                   description:@"获取车型信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSArray *data = [json objectForKey:@"data"];
            for (NSDictionary *dict in data) {
                CarBrandEntity *entity = [[CarBrandEntity alloc] initWithJson:dict];
                [weakSelf.dataArray addObject:entity];
                [weakSelf.tableView reloadData];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarBrandCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarBrandCell"];
    CarBrandEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarBrandEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    SelectCarSubBrandCtrl *vc = [SelectCarSubBrandCtrl new];
    vc.model = model;
    if(self.onSelectBlock){
        self.onSelectBlock(model);
        vc.onSelectBlock = self.onSelectBlock;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
