//
//  CarBrandCell.h
//  TravelNew
//
//  Created by mac on 2021/1/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "CarBrandEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarBrandCell : BaseCustomTableViewCell
@property(nonatomic,strong) CarBrandEntity *model;
@end

NS_ASSUME_NONNULL_END
