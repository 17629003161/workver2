//
//  CarBrandCell.m
//  TravelNew
//
//  Created by mac on 2021/1/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "CarBrandCell.h"

@interface CarBrandCell()
@property(nonatomic,strong) UIImageView *imagv;
@property(nonatomic,strong) UILabel *label;
@end

@implementation CarBrandCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(CarBrandEntity *)model
{
    _model = model;
    [self.imagv sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:nil];
    self.label.text  = model.name;
    [self.label updateLayout];
}

-(void)configViews{
    UIImageView *imgv = ImageViewWithImage(@"");
    self.imagv = imgv;
    UILabel *label = [Utils createLabelWithTitle:@"名称" titleColor:HexColor(0x666666) fontSize:17];
    self.label = label;
    [self.contentView sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 20)
    .widthIs(40)
    .heightIs(40);
    label.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

@end
