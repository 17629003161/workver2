//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "CarBrandEntity.h"

@implementation CarBrandEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.logo  = [json objectForKey:@"logo"];
		self.id  = [json objectForKey:@"id"];
		self.depth  = [json objectForKey:@"depth"];
		self.name  = [json objectForKey:@"name"];
		self.initial  = [json objectForKey:@"initial"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.logo forKey:@"zx_logo"];
	[aCoder encodeObject:self.id forKey:@"zx_id"];
	[aCoder encodeObject:self.depth forKey:@"zx_depth"];
	[aCoder encodeObject:self.name forKey:@"zx_name"];
	[aCoder encodeObject:self.initial forKey:@"zx_initial"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.logo = [aDecoder decodeObjectForKey:@"zx_logo"];
		self.id = [aDecoder decodeObjectForKey:@"zx_id"];
		self.depth = [aDecoder decodeObjectForKey:@"zx_depth"];
		self.name = [aDecoder decodeObjectForKey:@"zx_name"];
		self.initial = [aDecoder decodeObjectForKey:@"zx_initial"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"logo : %@\n",self.logo];
	result = [result stringByAppendingFormat:@"id : %@\n",self.id];
	result = [result stringByAppendingFormat:@"depth : %@\n",self.depth];
	result = [result stringByAppendingFormat:@"name : %@\n",self.name];
	result = [result stringByAppendingFormat:@"initial : %@\n",self.initial];
	
    return result;
}

@end
