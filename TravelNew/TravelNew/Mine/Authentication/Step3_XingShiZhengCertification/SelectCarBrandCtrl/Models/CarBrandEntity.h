//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface CarBrandEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *depth;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *initial;
 


-(id)initWithJson:(NSDictionary *)json;

@end
