//
//  SelectCarSubBrandCtrl.h
//  TravelNew
//
//  Created by mac on 2021/1/30.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "CarBrandEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectCarSubBrandCtrl : LKBaseTableViewController
@property(nonatomic,strong) CarBrandEntity *model;
@property(nonatomic,copy) void (^onSelectBlock)(CarBrandEntity *model);
@end

NS_ASSUME_NONNULL_END
