//
//  SelectCarSubBrandCtrl.m
//  TravelNew
//
//  Created by mac on 2021/1/30.
//  Copyright © 2021 lester. All rights reserved.
//

#import "SelectCarSubBrandCtrl.h"
#import "CarBrandCell.h"

@interface SelectCarSubBrandCtrl ()

@end

@implementation SelectCarSubBrandCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[CarBrandCell class] forCellReuseIdentifier:@"CarBrandCell"];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getModelInformation"
                          para:@{@"id":self.model.id}
                   description:@"获取车型信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSArray *data = [json objectForKey:@"data"];
            for (NSDictionary *dict in data) {
                CarBrandEntity *entity = [[CarBrandEntity alloc] initWithJson:dict];
                [weakSelf.dataArray addObject:entity];
                [weakSelf.tableView reloadData];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarBrandCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarBrandCell"];
    CarBrandEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarBrandEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    model.name = [NSString stringWithFormat:@"%@⋅%@",self.model.name,model.name];
    if(self.onSelectBlock){
        self.onSelectBlock(model);
    }
    [self popToViewControllerName:@"XingShiZhengCertificationCtrl"];
}

@end

