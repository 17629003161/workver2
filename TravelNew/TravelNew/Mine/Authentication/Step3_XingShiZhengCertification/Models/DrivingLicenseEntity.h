//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//

//行驶证

#import <Foundation/Foundation.h>


@interface DrivingLicenseEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,copy) NSString *register_date;
@property (nonatomic,copy) NSString *owner;
@property (nonatomic,copy) NSString *vin;
@property (nonatomic,copy) NSString *plate_no;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *engine_no;
@property (nonatomic,copy) NSString *vehicle_type;
@property (nonatomic,copy) NSString *use_character;
@property (nonatomic,copy) NSString *model;
@property (nonatomic,copy) NSString *issue_date;
 


-(id)initWithJson:(NSDictionary *)json;

@end
