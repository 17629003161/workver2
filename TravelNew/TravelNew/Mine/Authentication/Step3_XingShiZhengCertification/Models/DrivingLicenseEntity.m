//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "DrivingLicenseEntity.h"

@implementation DrivingLicenseEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.id  = [json objectForKey:@"id"];
		self.register_date  = [json objectForKey:@"register_date"];
		self.owner  = [json objectForKey:@"owner"];
		self.vin  = [json objectForKey:@"vin"];
		self.plate_no  = [json objectForKey:@"plate_no"];
		self.userId  = [json objectForKey:@"userId"];
		self.address  = [json objectForKey:@"address"];
		self.engine_no  = [json objectForKey:@"engine_no"];
		self.vehicle_type  = [json objectForKey:@"vehicle_type"];
		self.use_character  = [json objectForKey:@"use_character"];
		self.model  = [json objectForKey:@"model"];
		self.issue_date  = [json objectForKey:@"issue_date"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.id forKey:@"zx_id"];
	[aCoder encodeObject:self.register_date forKey:@"zx_register_date"];
	[aCoder encodeObject:self.owner forKey:@"zx_owner"];
	[aCoder encodeObject:self.vin forKey:@"zx_vin"];
	[aCoder encodeObject:self.plate_no forKey:@"zx_plate_no"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.engine_no forKey:@"zx_engine_no"];
	[aCoder encodeObject:self.vehicle_type forKey:@"zx_vehicle_type"];
	[aCoder encodeObject:self.use_character forKey:@"zx_use_character"];
	[aCoder encodeObject:self.model forKey:@"zx_model"];
	[aCoder encodeObject:self.issue_date forKey:@"zx_issue_date"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.id = [aDecoder decodeObjectForKey:@"zx_id"];
		self.register_date = [aDecoder decodeObjectForKey:@"zx_register_date"];
		self.owner = [aDecoder decodeObjectForKey:@"zx_owner"];
		self.vin = [aDecoder decodeObjectForKey:@"zx_vin"];
		self.plate_no = [aDecoder decodeObjectForKey:@"zx_plate_no"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.engine_no = [aDecoder decodeObjectForKey:@"zx_engine_no"];
		self.vehicle_type = [aDecoder decodeObjectForKey:@"zx_vehicle_type"];
		self.use_character = [aDecoder decodeObjectForKey:@"zx_use_character"];
		self.model = [aDecoder decodeObjectForKey:@"zx_model"];
		self.issue_date = [aDecoder decodeObjectForKey:@"zx_issue_date"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"id : %@\n",self.id];
	result = [result stringByAppendingFormat:@"register_date : %@\n",self.register_date];
	result = [result stringByAppendingFormat:@"owner : %@\n",self.owner];
	result = [result stringByAppendingFormat:@"vin : %@\n",self.vin];
	result = [result stringByAppendingFormat:@"plate_no : %@\n",self.plate_no];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"engine_no : %@\n",self.engine_no];
	result = [result stringByAppendingFormat:@"vehicle_type : %@\n",self.vehicle_type];
	result = [result stringByAppendingFormat:@"use_character : %@\n",self.use_character];
	result = [result stringByAppendingFormat:@"model : %@\n",self.model];
	result = [result stringByAppendingFormat:@"issue_date : %@\n",self.issue_date];
	
    return result;
}

@end
