//
//  AuthenticationCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AuthenticationCtrl.h"
#import "AuthenticationCell.h"

#import "RealNameAuthCtrl.h"
#import "DriverLicenseCertificationCtrl.h"
#import "XingShiZhengCertificationCtrl.h"
#import "AuthFinishCtrl.h"


@interface AuthenticationCtrl ()

@property(nonatomic,assign) NSInteger step;
@property(nonatomic,strong) UIImageView *headImageview;
@property(nonatomic,strong) LKGadentButton *btnCommit;

@end

@implementation AuthenticationCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"推荐官认证" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF5F5F7);
    self.tableView.backgroundColor = HexColor(0xF5F5F7);
    [self.tableView  registerClass:[AuthenticationCell class] forCellReuseIdentifier:@"AuthenticationCell"];
    [self.dataArray addObject:@[@"1",@"实名认证",@"输入真实姓名跟身份证号码",@"去完成"]];
    [self.dataArray addObject:@[@"2",@"驾驶证认证",@"驾领不得低于1年",@"去完成"]];
    [self.dataArray addObject:@[@"3",@"行驶证认证",@"车辆需在10年内",@"去完成"]];
    [self layout];
    [self getAuthStep];
    [self disabelPageRefresh];
}

- (void)setStep:(NSInteger)step
{
    _step = step;
    [self.tableView reloadData];
    if(step>=3){
        self.btnCommit.isEnabled = YES;
    }else{
        self.btnCommit.isEnabled = NO;
    }
}


- (void)layout{
    UIView *view = [self makeHeaderView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"提交审核" font:LKSystemFont(17) cornerRadius:22];
    btn.layer.cornerRadius = 20;
    self.btnCommit = btn;
    [btn addTapGestureTarget:self action:@selector(commitAuth:)];
    [self.view sd_addSubviews:@[view,btn]];
    
    btn.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .bottomSpaceToView(self.view, 42+SAFEVIEWBOTTOMMARGIN)
    .heightIs(40);

    self.tableView.sd_layout
    .topSpaceToView(view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomSpaceToView(btn, 10);
}


- (void)commitAuth:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    LKGadentButton *btn = (LKGadentButton *)reg.view;
    if(!btn.isEnabled)
        return;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/submitForReview_block1A" para:@{} description:@"提交审核_block1A"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        [MBProgressHUD showMessage:json[@"msg"]];
        if([[json objectForKey:@"code"] integerValue]==200){
            AuthFinishCtrl *vc = [AuthFinishCtrl new];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (UIView *)createFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
    view.backgroundColor = HexColor(0xF5F5F7);
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"提交审核" font:LKSystemFont(17) cornerRadius:22];
    btn.layer.cornerRadius = 8;
    self.btnCommit = btn;
    [btn addTapGestureTarget:self action:@selector(commitAuth:)];
    [view addSubview:btn];
    btn.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(40);
    return view;
}

- (UIView *)makeHeaderView{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xF5F5F7);
    view.frame = CGRectMake(0, 0, kScreenWidth, 194);
    UIImageView *bgimgv = ImageViewWithImage(@"shenqingtuijianguan_bg");
    UIImageView *headimgv = ImageViewWithImage(@"default_header");
    headimgv.layer.borderColor = [UIColor whiteColor].CGColor;
    headimgv.layer.borderWidth = 2.0;
    headimgv.layer.cornerRadius = 26;
    headimgv.layer.masksToBounds = YES;
    UserInfo *user = [UserInfo shareInstance];
    if(user.headerImage){
        [headimgv sd_setImageWithURL:[NSURL URLWithString:user.headerImage] placeholderImage:[UIImage imageNamed:@"default_header"]];
    }
    NSString *phone = user.phone;
    NSString *maskPhone = [phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    NSString *title = [NSString stringWithFormat:@"您好，%@",maskPhone];
    UILabel *welcomeLabel = [Utils createLabelWithTitle:title titleColor:HexColor(0xffffff) fontSize:14];
    UIView *line = [UILabel new];
    line.backgroundColor = HexColor(0xffffff);
    UILabel *tipLabel = [Utils createLabelWithTitle:@"旅咖出行出行，邀您成为推荐官" titleColor:HexColor(0xffffff) fontSize:12];
    UILabel *label = [Utils createLabelWithTitle:@"完成认证就可以当推荐官啦~" titleColor:HexColor(0xBCBCBC) fontSize:12];
    [bgimgv sd_addSubviews:@[headimgv,
                                welcomeLabel,
                                line,
                                tipLabel]];
    [view sd_addSubviews:@[bgimgv,label]];
    bgimgv.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 15)
    .rightSpaceToView(view, 15)
    .heightIs(125);

    label.sd_layout
    .topSpaceToView(bgimgv, 16)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    headimgv.sd_layout
    .centerYEqualToView(bgimgv)
    .leftSpaceToView(bgimgv, 45)
    .widthIs(52)
    .heightIs(52);

    line.sd_layout
    .centerYEqualToView(bgimgv)
    .leftSpaceToView(headimgv, 27)
    .widthIs(140)
    .heightIs(0.5);

    welcomeLabel.sd_layout
    .leftEqualToView(line)
    .bottomSpaceToView(line, 5)
    .autoHeightRatio(0);
    [welcomeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    tipLabel.sd_layout
    .leftEqualToView(line)
    .topSpaceToView(line, 5)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AuthenticationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AuthenticationCell"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSArray *ary = self.dataArray[indexPath.section];
    cell.numberLabel.text = ary[0];
    cell.nameLabel.text = ary[1];
    cell.descriptionLabel.text = ary[2];
    cell.resultLabel.textColor = HexColor(0x333333);
    if (indexPath.section < self.step) {
        cell.resultLabel.text = @"已完成";
        cell.isOk = YES;
        cell.resultLabel.textColor = HexColor(0x4cc119);
        cell.backgroundColor = [UIColor whiteColor];
        
    } else if(indexPath.section == self.step){
        cell.isOk = NO;
        cell.resultLabel.text = @"去完成";
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.isOk = NO;
        cell.resultLabel.text = @"去完成";
        cell.backgroundColor = HexColor(0xF5F5F7);
    }
    return cell;
}

- (void)getAuthStep{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getRealNameState_block1A"
                          para:@{}
                   description:@"获取实名认证状态block1A"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            self.step = [[json objectForKey:@"data"] integerValue];
        }else{
            self.step = 0;
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            self.step = 0;
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > self.step)
        return;                             //判断认证步骤到哪一步了
    switch (indexPath.section) {
        case 0:{
            RealNameAuthCtrl *vc = [RealNameAuthCtrl new];
            vc.FinishBlock = ^{
                [self getAuthStep];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:{
            DriverLicenseCertificationCtrl *vc = [DriverLicenseCertificationCtrl new];
            vc.FinishBlock = ^{
                [self getAuthStep];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{
            XingShiZhengCertificationCtrl *vc = [XingShiZhengCertificationCtrl new];
            vc.FinishBlock = ^{
                [self getAuthStep];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 5)];
    view.backgroundColor = HexColor(0xF5F5F7);
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}




@end
