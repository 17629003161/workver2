//
//  UserGuideCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserGuideCtrl : LKBaseViewController

@property(nonatomic,strong) NSString *tipTitle;
@property(nonatomic,strong) NSString *photoName;
@property(nonatomic,strong) NSString *buttonTitle;
@property(nonatomic,assign) BOOL buttonHide;
@property(nonatomic,copy) void (^funcBlock)(void);

@end

NS_ASSUME_NONNULL_END
