//
//  UserGuideCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UserGuideCtrl.h"
#import "AuthenticationCtrl.h"

@interface UserGuideCtrl ()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) LKGadentButton *btn;
@end

@implementation UserGuideCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // self.navigationController.navigationBar.barTintColor = HexColor(0x4D83FD);
    [self setTitle:_tipTitle titleColor:nil];
    [self.view addSubview:self.scrollView];
    UIImage *image = [UIImage imageNamed:self.photoName];
    CGFloat scale =  kScreenWidth/image.size.width;
    CGFloat height = image.size.height * scale;
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, height);
    
    self.imgv = [[UIImageView alloc] initWithImage:image];
    [self.scrollView addSubview:self.imgv];
    self.scrollView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    self.imgv.sd_layout
    .topSpaceToView(self.scrollView, 0)
    .leftSpaceToView(self.scrollView, 0)
    .rightSpaceToView(self.scrollView, 0)
    .heightIs(height);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.buttonHide)
        [self addFloatButton];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(!self.buttonHide)
        [self.btn removeFromSuperview];
}

- (void)addFloatButton{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    if(!self.btn){
        LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"我知道了" font:LKSystemFont(17) cornerRadius:22];
        if(self.buttonTitle){
            btn.titleLabel.text = self.buttonTitle;
            [btn.titleLabel updateLayout];
            if(self.buttonTitle.length==0){
                btn.hidden = YES;
            }
        }
        self.btn = btn;
        [btn addTapGestureTarget:self action:@selector(addAuth)];
        [keyWindow addSubview:btn];
        btn.sd_layout
        .leftSpaceToView(keyWindow, 20)
        .rightSpaceToView(keyWindow, 20)
        .bottomSpaceToView(keyWindow, 33)
        .heightIs(44);
    }else{
        [keyWindow addSubview:self.btn];
    }
}

- (void)addAuth{
    if(self.funcBlock){
        self.funcBlock();
    }else{
        AuthenticationCtrl *vc = [AuthenticationCtrl new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}




@end
