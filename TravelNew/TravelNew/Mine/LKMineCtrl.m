//
//  LKMineCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKMineCtrl.h"
#import "LoginViewController.h"
#import "MyCardCtrl.h"
#import "MyMoneyBagCtrl.h"
#import "MyPublishedCtrl.h"
#import "OrderViewController.h"
#import "AppointmentHallCtrl.h"
#import "MyMoneyBagCtrl.h"
#import "MyJiFenCtrl.h"
#import "MyNoticeCtrl.h"
#import "SetupCtrl.h"
#import "MyAdviceCtrl.h"
#import "SeviceCenterCtrl.h"
#import "SafeCenterCtrl.h"
#import "InventUserCtrl.h"
#import "LKWechatShareManager.h"
#import "UserGuideCtrl.h"
#import "ShowTextCtrl.h"
#import "PersonInfoCtrl.h"
#import "PersonInfoCustomCtrl.h"
#import "LKMessageCtrl.h"
#import "WithdrawalEntity.h"
#import "MainRecommenderCtrl.h"
#import "AuthenticationCtrl.h"
#import "ServiceSetupMainCtrl.h"
#import "PersonInfoCtrl.h"
#import <MessageUI/MessageUI.h>
#import "MakeApartmentCtrl.h"


@interface LKMineCtrl ()<MFMailComposeViewControllerDelegate>{
    MFMailComposeViewController *mailComposer;
}
@property(nonatomic,strong) UIButton *switchButton;
@property(nonatomic,strong) UIImageView *userImageview;
@property(nonatomic,strong) UILabel *userNameLabel;
@property(nonatomic,strong) UIImageView *modeImageView;
@property(nonatomic,strong) UILabel *balenceLabel;
@property(nonatomic,strong) WithdrawalEntity *model;
@property(nonatomic,strong) UIImageView *mainImgv;
@property(nonatomic,strong) UIView *fucView;
@property(nonatomic,strong) UIView *fucView2;

@end

@implementation LKMineCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xf5f5f7);
    [self layout];
    [self showCurrentUserStyleBeQuiet:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self showCurrentUserStyleBeQuiet:YES];
    [self refreshData];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth-32, CGRectGetMaxY(self.switchButton.frame)+30);
}

-(void)setModel:(WithdrawalEntity *)model
{
    _model = model;
    double money = [model.walletNumber doubleValue]/100.0;
  //  self.balenceLabel.text = [NSString stringWithFormat:@"%@元",[Utils formatMoney:money]];
    self.balenceLabel.text = [NSString stringWithFormat:@"%.2lf元",money];
}

- (void)checkBalence {
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/wallet/selectWalletByUserId"
                          para:@{}
                   description:@"查询钱包余额"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            WithdrawalEntity *model = [[WithdrawalEntity alloc] initWithJson:dict];
            weakSelf.model = model;
        };
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
}


- (BOOL)checkLogin{
    UserInfo *user = [UserInfo shareInstance];
    if(!user.isLogin){
        LoginViewController *vc = [[LoginViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    return YES;
}

- (void)refreshData{
    UserInfo *user = [UserInfo shareInstance];
    if(user.isLogin){
        [self checkBalence];
        if(!isNull(user.headerImage))
            [self.userImageview sd_setImageWithURL:[NSURL URLWithString:user.headerImage]
                              placeholderImage:[UIImage imageNamed:@"default_header"]];
        if(!isNull(user.userNickname)){
            self.userNameLabel.text = user.userNickname;
            [self.userNameLabel updateLayout];
        }
    }else{
        self.balenceLabel.text = @"0.0";
        [self.balenceLabel updateLayout];
        self.userImageview.image = [UIImage imageNamed:@"default_header"];
        self.userNameLabel.text = @"登录/注册";
        [self.userNameLabel updateLayout];
    }
}

#pragma mark -- 切换推荐官、用户
- (void)showCurrentUserStyleBeQuiet:(BOOL)isBeQuiet{
    UserInfo *user = [UserInfo shareInstance];
    self.fucView.hidden = YES;
    self.fucView2.hidden = YES;
    if(!user.isLogin){
        [_switchButton setTitle:@"当推荐官挣钱" forState:UIControlStateNormal];
        self.mainImgv.hidden = YES;         //不显示我的主页
        self.fucView.hidden = NO;
    }else{
        if([user isCurrentRecomander] && [user isRecomander]){
            self.fucView2.hidden = NO;
            self.modeImageView.image = [UIImage imageNamed:@"recommand_mode_photo"];
            [_switchButton setTitle:@"切换到游客" forState:UIControlStateNormal];
            if(!isBeQuiet){
                [Utils showMsg:@"您已切换到推荐官" imgName:@"mode_right_mark"];
            }
            self.mainImgv.hidden = NO;          //显示我的主页
        }else{
            self.fucView.hidden = NO;
            self.modeImageView.image = [UIImage imageNamed:@"custom_mode_photo"];
            [_switchButton setTitle:@"当推荐官挣钱" forState:UIControlStateNormal];
            if(!isBeQuiet){
                [Utils showMsg:@"您已切换到游客" imgName:@"mode_right_mark"];
            }
            self.mainImgv.hidden = YES;         //不显示我的主页
        }
    }
}

- (void)switchIsClicked:(id)sender
{
    if(![self checkLogin]) return;
    UserInfo *user = [UserInfo shareInstance];

    if(![user isRecomander]){
        UserGuideCtrl *vc = [UserGuideCtrl new];
        vc.tipTitle = @"推荐官协议";
        vc.photoName = @"RecomandProtocol";
        vc.buttonHide = NO;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [user switchCurrentStyle];
        [self showCurrentUserStyleBeQuiet:NO];
    }
}

- (void)layout{
    UIView *topView = [self makeTopView];
    UIView *orderView = [self OrderViewLayout];
    UIView *moneyView = [self moneyViewLayout];
    UIView *funcView = [self functionViewLayout];
    self.fucView = funcView;
    UIView *funcView2 = [self functionViewLayout2];
    self.fucView2 = funcView2;
    [self.view sd_addSubviews:@[topView,
                                self.scrollView]];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth-32, kScreenHeight*1.2);
    self.switchButton = [self createButton];
    [self.switchButton addTarget:self action:@selector(switchIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView sd_addSubviews:@[orderView,
                                      moneyView,
                                      funcView,
                                      funcView2,
                                      self.switchButton]];
    topView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(202);
    
    self.scrollView.sd_layout
    .topSpaceToView(self.view, 156)
    .leftSpaceToView(self.view, 16)
    .rightSpaceToView(self.view, 16)
    .bottomEqualToView(self.view);
    
    orderView.sd_layout
    .topSpaceToView(self.scrollView, 0)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(118);
    
    moneyView.sd_layout
    .topSpaceToView(orderView, 16)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(120);
    
    funcView.sd_layout
    .topSpaceToView(moneyView,16)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(259);
    
    funcView2.sd_layout
    .topSpaceToView(moneyView,16)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(259);
    
    self.switchButton.sd_layout
    .topSpaceToView(funcView, 28)
    .centerXEqualToView(self.scrollView)
    .widthIs(213)
    .heightIs(40);
}

- (UIButton *)createButton{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xEAEAEA)] forState:UIControlStateNormal];
    [btn setTitle:@"切换到游客" forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
    btn.titleLabel.font = LKSystemFont(14);
    btn.layer.cornerRadius = 20;
    btn.layer.masksToBounds = YES;
    return btn;
}


-(void)loginIsClicked:(UITapGestureRecognizer *)reg
{
    UserInfo *user = [UserInfo shareInstance];
    if(!user.isLogin){
        LoginViewController *vc = [[LoginViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        if([user isCurrentRecomander] && [user isRecomander]){
            PersonInfoCtrl *vc = [PersonInfoCtrl new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            PersonInfoCustomCtrl *vc = [PersonInfoCustomCtrl new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (void)cardIsClicked:(id)sender{
    if(![self checkLogin]) return;
    MyCardCtrl *vc = [[MyCardCtrl alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
//    AuthenticationCtrl *vc = [AuthenticationCtrl new];
//    vc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:vc animated:YES];
}

- (void)myMoneyBagIsClicked:(id)sender{
    if(![self checkLogin]) return;
    MyMoneyBagCtrl *vc = [[MyMoneyBagCtrl alloc] init];
    vc.model = self.model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)myNoticeBtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    MyNoticeCtrl *vc = [MyNoticeCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)shareTest:(UITapGestureRecognizer *)reg
{
    LKMessageCtrl *vc = [LKMessageCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)mainPageIsClicked:(UITapGestureRecognizer *)reg
{
    UserInfo *user = [UserInfo shareInstance];
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.userId = user.userId;
    [self.navigationController pushViewController:vc animated:YES];
}


- (UIView *)makeTopView{
    UIImageView *bgimgv = ImageViewWithImage(@"custom_mode_photo");
    [bgimgv setUserInteractionEnabled:YES];
    self.modeImageView = bgimgv;
    UIImageView *msgimgv = ImageViewWithImage(@"message_mine");
    msgimgv.hidden = YES;
    [msgimgv addTapGestureTarget:self action:@selector(shareTest:)];
    UIImageView *personimgv = ImageViewWithImage(@"default_header");
    personimgv.layer.cornerRadius = 21;
    personimgv.layer.masksToBounds = YES;
    [personimgv addTapGestureTarget:self action:@selector(loginIsClicked:)];
    self.userImageview = personimgv;
    UILabel *label1 = [Utils createLabelWithTitle:@"登录/注册"
                                       titleColor:[UIColor whiteColor]
                                         fontSize:20];
    [label1 addTapGestureTarget:self action:@selector(loginIsClicked:)];
    self.userNameLabel = label1;

    
    UIImageView *mainImgv = ImageViewWithImage(@"main_page");
    [mainImgv addTapGestureTarget:self action:@selector(mainPageIsClicked:)];
    mainImgv.hidden = YES;
    self.mainImgv = mainImgv;
    
    [bgimgv sd_addSubviews:@[msgimgv,
                             personimgv,label1,mainImgv
                             ]];
    
    msgimgv.sd_layout
    .topSpaceToView(bgimgv, 40)
    .rightSpaceToView(bgimgv, 23)
    .widthIs(19)
    .heightIs(18);
    
    personimgv.sd_layout
    .topSpaceToView(bgimgv, 86)
    .leftSpaceToView(bgimgv, 34)
    .widthIs(42)
    .heightIs(42);
    
    label1.sd_layout
    .centerYEqualToView(personimgv)
    .leftSpaceToView(personimgv, 13)
    .rightSpaceToView(bgimgv, 13)
    .heightIs(30);
    
    mainImgv.sd_layout
    .centerYEqualToView(personimgv)
    .rightEqualToView(bgimgv)
    .widthIs(84)
    .heightIs(28);
    
    return bgimgv;
}

- (void)allOrderIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    OrderViewController *vc = [OrderViewController new];
   // vc.orderSate = OrderStateIsInCommunication;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)orderIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
//    UIView *view = reg.view;
//    OrderViewController *vc = [OrderViewController new];
//    NSInteger index = view.tag-1000;
//    NSArray *statesAry = @[@(OrderStateIsInCommunication),
//                           @(OrderStateIsServing),
//                           @(OrderStateIsFinished)];
//    vc.orderSate = [statesAry[index] integerValue];
//    vc.hidesBottomBarWhenPushed = YES;
   // [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)OrderViewLayout{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 5;
    UILabel *label1 = [Utils createLabelWithTitle:@"我的订单"
                                       titleColor:HexColor(0x333333)
                                         fontSize:16];
    label1.font = [UIFont boldSystemFontOfSize:16];
    UILabel *label2 = [Utils createLabelWithTitle:@"全部订单"
                                       titleColor:HexColor(0x999999)
                                         fontSize:14];
    [label2 addTapGestureTarget:self action:@selector(allOrderIsClicked:)];
    UIImageView *arrow = ImageViewWithImage(@"arrow_right");
    
    UIView *view1 = [self viewType1WithIcon:@"make_price" title:@"沟通中"];
    view1.tag = 1000;
    [view1 addTapGestureTarget:self action:@selector(orderIsClicked:)];
    UIView *view2 = [self viewType1WithIcon:@"waite_out" title:@"服务中"];
    view2.tag = 1001;
    [view2 addTapGestureTarget:self action:@selector(orderIsClicked:)];
    UIView *view3 = [self viewType1WithIcon:@"wait_commont" title:@"已完成"];
    view3.tag = 1002;
    [view3 addTapGestureTarget:self action:@selector(orderIsClicked:)];
    [view sd_addSubviews:@[label1,label2,arrow,
                           view1,view2,view3]];
    label1.sd_layout
    .leftSpaceToView(view, 20)
    .topSpaceToView(view, 14)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(label1)
    .rightSpaceToView(view, 20)
    .widthIs(6)
    .heightIs(11);
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .rightSpaceToView(arrow, 6)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    view1.sd_layout
    .topSpaceToView(view, 50)
    .leftSpaceToView(view, 40)
    .widthIs(52)
    .heightIs(42);
    
    view2.sd_layout
    .centerYEqualToView(view1)
    .centerXEqualToView(view)
    .widthIs(52)
    .heightIs(42);
    
    view3.sd_layout
    .centerYEqualToView(view1)
    .rightSpaceToView(view, 50)
    .widthIs(52)
    .heightIs(42);

     return view;
    
}

- (UIView *)moneyViewLayout{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 5;
    UILabel *label1 = [Utils createLabelWithTitle:@"我的资产"
                                       titleColor:HexColor(0x333333)
                                         fontSize:16];
    
    UIImageView *bgview1 = ImageViewWithImage(@"money_bag");
    [bgview1 setUserInteractionEnabled:YES];
    UILabel *moneyL = [Utils createLabelWithTitle:@"0.00元"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    self.balenceLabel = moneyL;
    UILabel *moneyL1 = [Utils createLabelWithTitle:@"我的钱包"
                                        titleColor:HexColor(0x333333)
                                          fontSize:10];
    [bgview1 sd_addSubviews:@[moneyL,moneyL1]];
    [bgview1 addTapGestureTarget:self action:@selector(myMoneyBagIsClicked:)];

    UIImageView *bgview2 = ImageViewWithImage(@"card_bag");
    UILabel *cardL = [Utils createLabelWithTitle:@"0张"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    UILabel *cardL1 = [Utils createLabelWithTitle:@"卡包"
                                        titleColor:HexColor(0x333333)
                                          fontSize:10];
    [bgview2 sd_addSubviews:@[cardL,cardL1]];
    [bgview2 addTapGestureTarget:self action:@selector(cardIsClicked:)];
    [view sd_addSubviews:@[label1,bgview1,bgview2]];
    
    label1.sd_layout
    .leftSpaceToView(view, 20)
    .topSpaceToView(view, 14)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgview1.sd_layout
    .leftSpaceToView(view, 20)
    .topSpaceToView(label1, 10)
    .widthIs((kScreenWidth-87)/2.0)
    .heightIs(54);
    
    moneyL.sd_layout
    .leftSpaceToView(bgview1, 15)
    .topSpaceToView(bgview1, 10)
    .autoHeightRatio(0);
    [moneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    moneyL1.sd_layout
    .leftEqualToView(moneyL)
    .topSpaceToView(bgview1, 30)
    .autoHeightRatio(0);
    [moneyL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    
    bgview2.sd_layout
    .topEqualToView(bgview1)
    .rightSpaceToView(view, 20)
    .leftSpaceToView(bgview1, 15)
    .heightIs(54);
    
    cardL.sd_layout
    .leftSpaceToView(bgview2, 15)
    .topSpaceToView(bgview2, 10)
    .autoHeightRatio(0);
    [cardL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cardL1.sd_layout
    .leftEqualToView(cardL)
    .topSpaceToView(bgview2, 30)
    .autoHeightRatio(0);
    [cardL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (void)myPublishedIsClicked:(UITapGestureRecognizer  *)reg{
    if(![self checkLogin]) return;
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander] &&[user isRecomander]){
        AppointmentHallCtrl *vc = [AppointmentHallCtrl new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MyPublishedCtrl *vc = [[MyPublishedCtrl alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}
- (void)myFootprintIsClicked:(UITapGestureRecognizer *)reg
{

}

- (void)myJifenIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    MyJiFenCtrl *vc = [MyJiFenCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)setupIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    SetupCtrl *vc = [SetupCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)myAdviceIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    MyAdviceCtrl *vc = [MyAdviceCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)seviceCenterIsClicked:(id)sender
{
    if(![self checkLogin]) return;
    SeviceCenterCtrl *vc = [SeviceCenterCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)safeCenterIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    SafeCenterCtrl *vc = [SafeCenterCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)inventUserIsClicked:(UITapGestureRecognizer *)reg
{
    if(![self checkLogin]) return;
    InventUserCtrl *vc = [InventUserCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)userGuideIsClicked:(id)sender
{
    if(![self checkLogin]) return;
    UserInfo *user = [UserInfo shareInstance];
    
    if([user isCurrentRecomander] &&[user isRecomander]){
        UserGuideCtrl *vc = [UserGuideCtrl new];
        vc.tipTitle = @"用户指南";
        vc.photoName = @"user_guide";
        vc.buttonHide = YES;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        UserGuideCtrl *vc = [UserGuideCtrl new];
        vc.tipTitle = @"用户指南";
        vc.photoName = @"user_guide_user";
        vc.buttonHide = YES;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)switchServiceClicked:(UITapGestureRecognizer *)reg
{
    ServiceSetupMainCtrl *vc = [ServiceSetupMainCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)personInfoPageIsClicked:(UITapGestureRecognizer *)reg
{
    PersonInfoCtrl *vc = [PersonInfoCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 在应用内发送邮件
//激活邮件功能
- (void)makeApartmentIsClicked:(UITapGestureRecognizer *)reg
{
    MakeApartmentCtrl *vc = [MakeApartmentCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
  
- (UIView *)functionViewLayout2{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 5;
    UIView *view1 = [self viewType2WithIcon:@"lock_icon" title:@"开通服务"];
    [view1 addTapGestureTarget:self action:@selector(switchServiceClicked:)];
    
    UIView *view2 = [self viewType2WithIcon:@"advice" title:@"主页设置"];
    [view2 addTapGestureTarget:self action:@selector((loginIsClicked:))];
    
    UIView *view3 = [self viewType2WithIcon:@"shoucang-4" title:@"我的关注"];
    [view3 addTapGestureTarget:self action:@selector(myNoticeBtnIsClicked:)];
    
    UIView *view4 = [self viewType2WithIcon:@"safe" title:@"安全中心"];
    [view4 addTapGestureTarget:self action:@selector(safeCenterIsClicked:)];

    UIView *view5 = [self viewType2WithIcon:@"server_ear" title:@"客服中心"];
    [view5 addTapGestureTarget:self action:@selector(seviceCenterIsClicked:)];
    
    UIView *view6 = [self viewType2WithIcon:@"advice_mine" title:@"成为合伙人"];
    [view6 addTapGestureTarget:self action:@selector(makeApartmentIsClicked:)];
    
    UIView *view7 = [self viewType2WithIcon:@"setup" title:@"系统设置"];
    [view7 addTapGestureTarget:self action:@selector(setupIsClicked:)];
    
    UIView *view8 = [self viewType2WithIcon:@"zhinan" title:@"服务指南"];
    [view8 addTapGestureTarget:self action:@selector(userGuideIsClicked:)];
    
    UIView *view9 = [self viewType2WithIcon:@"foot_step" title:@"我的足迹"];
    [view9 addTapGestureTarget:self action:@selector(myFootprintIsClicked:)];
    
    [view sd_addSubviews:@[view1,view2,view3,view4,view5,view6,view7,view8,view9]];
    
    view1.sd_layout
    .leftSpaceToView(view, 40)
    .topSpaceToView(view, 32)
    .widthIs(56)
    .heightIs(52);
    
    view2.sd_layout
    .centerXEqualToView(view)
    .centerYEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
    view3.sd_layout
    .centerYEqualToView(view1)
    .rightSpaceToView(view, 40)
    .widthIs(56)
    .heightIs(52);
    
    view4.sd_layout
    .topSpaceToView(view1, 25)
    .centerXEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
    view5.sd_layout
    .centerYEqualToView(view4)
    .centerXEqualToView(view2)
    .widthIs(56)
    .heightIs(52);
    
    view6.sd_layout
    .centerYEqualToView(view4)
    .centerXEqualToView(view3)
    .widthIs(56)
    .heightIs(52);
    
    view7.sd_layout
    .topSpaceToView(view4, 25)
    .centerXEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
    view8.sd_layout
    .centerYEqualToView(view7)
    .centerXEqualToView(view2)
    .widthIs(56)
    .heightIs(52);

    view9.sd_layout
    .centerYEqualToView(view7)
    .centerXEqualToView(view3)
    .widthIs(56)
    .heightIs(52);
    
    return view;
    
}


- (UIView *)functionViewLayout{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 5;
    UIView *view1 = [self viewType2WithIcon:@"publish" title:@"我的发布"];
    [view1 addTapGestureTarget:self action:@selector(myPublishedIsClicked:)];
    UIView *view2 = [self viewType2WithIcon:@"foot_step" title:@"我的足迹"];
    [view2 addTapGestureTarget:self action:@selector((myFootprintIsClicked:))];
//    UIView *view3 = [self viewType2WithIcon:@"jifen" title:@"我的积分"];
//    [view3 addTapGestureTarget:self action:@selector(myJifenIsClicked:)];
    UIView *view3 = [self viewType2WithIcon:@"server_ear" title:@"客服中心"];
    
    [view3 addTapGestureTarget:self action:@selector(seviceCenterIsClicked:)];
    UIView *view4 = [self viewType2WithIcon:@"safe" title:@"安全中心"];
    [view4 addTapGestureTarget:self action:@selector(safeCenterIsClicked:)];
    UIView *view5 = [self viewType2WithIcon:@"zhinan" title:@"使用指南"];
    [view5 addTapGestureTarget:self action:@selector(userGuideIsClicked:)];
    UIView *view6 = [self viewType2WithIcon:@"advice" title:@"我的关注"];
    [view6 addTapGestureTarget:self action:@selector(myNoticeBtnIsClicked:)];
//    UIView *view8 = [self viewType2WithIcon:@"advice_mine" title:@"邀请赚现金"];
//    [view8 addTapGestureTarget:self action:@selector(inventUserIsClicked:)];
    UIView *view7 = [self viewType2WithIcon:@"setup" title:@"系统设置"];
    [view7 addTapGestureTarget:self action:@selector(setupIsClicked:)];
    [view sd_addSubviews:@[view1,view2,view3,view4,view5,view6,view7]];
    
    view1.sd_layout
    .leftSpaceToView(view, 40)
    .topSpaceToView(view, 32)
    .widthIs(56)
    .heightIs(52);
    
    view2.sd_layout
    .centerXEqualToView(view)
    .centerYEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
    view3.sd_layout
    .centerYEqualToView(view1)
    .rightSpaceToView(view, 40)
    .widthIs(56)
    .heightIs(52);
    
    view4.sd_layout
    .topSpaceToView(view1, 25)
    .centerXEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
    view5.sd_layout
    .centerYEqualToView(view4)
    .centerXEqualToView(view2)
    .widthIs(56)
    .heightIs(52);
    
    view6.sd_layout
    .centerYEqualToView(view4)
    .centerXEqualToView(view3)
    .widthIs(56)
    .heightIs(52);
    
    view7.sd_layout
    .topSpaceToView(view4, 25)
    .centerXEqualToView(view1)
    .widthIs(56)
    .heightIs(52);
    
//    view8.sd_layout
//    .centerYEqualToView(view7)
//    .centerXEqualToView(view2)
//    .widthIs(56)
//    .heightIs(52);
//    
//    view9.sd_layout
//    .centerYEqualToView(view7)
//    .centerXEqualToView(view3)
//    .widthIs(56)
//    .heightIs(52);
    
    return view;
    
}



- (UIView *)viewType1WithIcon:(NSString *)name title:(NSString *)title{
    UIView *view = [UIView new];
    UIImageView *imgv = ImageViewWithImage(name);
    UILabel *label = [Utils createLabelWithTitle:title
                                      titleColor:HexColor(0x4a4a4a)
                                        fontSize:14];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .topEqualToView(view)
    .centerXEqualToView(view)
    .widthIs(28)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(imgv, 3)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (UIView *)viewType2WithIcon:(NSString *)name title:(NSString *)title{
    UIView *view = [UIView new];
    UIImageView *imgv = ImageViewWithImage(name);
    imgv.tag = 1;
    UILabel *label = [Utils createLabelWithTitle:title
                                      titleColor:HexColor(0x333333)
                                        fontSize:14];
    label.tag = 2;
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .topEqualToView(view)
    .centerXEqualToView(view)
    .widthIs(22)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(imgv, 9)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


@end
