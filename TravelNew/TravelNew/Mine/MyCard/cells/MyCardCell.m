//
//  MyCardCell.m
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyCardCell.h"

@interface MyCardCell(){
    UILabel *_moneyL;
    UILabel *_moneyCondL;
    UILabel *_typeL;
    UILabel *_typeCondL;
    UIButton *_btn;
}

@end

@implementation MyCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self configViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)configViews{
    UIImageView *imgv1 = ImageViewWithImage(@"my_card2");
    _moneyL = [Utils createLabelWithTitle:@"10元" titleColor:HexColor(0xB3720A) fontSize:32];
    [_moneyL changeStrings:@[@"元"] toSize:16];
    _moneyCondL.font = [UIFont boldSystemFontOfSize:32];
    _moneyL.textAlignment = NSTextAlignmentCenter;
    _moneyCondL = [Utils createLabelWithTitle:@"满100可用" titleColor:HexColor(0xC4A26E) fontSize:12];
    _moneyCondL.textAlignment = NSTextAlignmentCenter;
    [imgv1 sd_addSubviews:@[_moneyL,
                            _moneyCondL]];
    
    UIImageView *imgv2 = ImageViewWithImage(@"my_card1-1");
    _typeL = [Utils createLabelWithTitle:@"旅咖出行券" titleColor:HexColor(0x333333) fontSize:16];
    _typeL.font = [UIFont boldSystemFontOfSize:16];
    _typeCondL = [Utils createLabelWithTitle:@"出行立减10元" titleColor:HexColor(0x666666) fontSize:12];
    
    _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn setTitle:@"去使用" forState:UIControlStateNormal];
    _btn.titleLabel.font = LKSystemFont(12);
    [_btn setTitleColor:HexColor(0xB3720A) forState:UIControlStateNormal];
    [_btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xFDF8F0)] forState:UIControlStateNormal];
    _btn.layer.borderColor = HexColor(0xC4A26E).CGColor;
    _btn.layer.borderWidth = 1.0;
    _btn.layer.cornerRadius = 10.0;
    [imgv2 sd_addSubviews:@[_typeL,
                            _typeCondL,_btn]];
    
    [self.contentView sd_addSubviews:@[imgv1,imgv2]];
    
    imgv1.sd_layout
    .leftSpaceToView(self.contentView, 20)
    .topSpaceToView(self.contentView, 16)
    .bottomEqualToView(self.contentView)
    .widthIs(147);
    
    _moneyL.sd_layout
    .topSpaceToView(imgv1, 10)
    .leftSpaceToView(imgv1, 2)
    .rightSpaceToView(imgv1, 2)
    .autoHeightRatio(0);
    
    _moneyCondL.sd_layout
    .topSpaceToView(_moneyL, 1)
    .leftEqualToView(_moneyL)
    .rightEqualToView(_moneyL)
    .autoHeightRatio(0);
    
    imgv2.sd_layout
    .leftSpaceToView(imgv1, 0)
    .rightSpaceToView(self.contentView, 20)
    .topSpaceToView(self.contentView, 16)
    .bottomEqualToView(self.contentView);
    
    _typeL.sd_layout
    .leftSpaceToView(imgv2, 7)
    .topSpaceToView(imgv2, 20)
    .rightSpaceToView(imgv2, 2)
    .autoHeightRatio(0);
    
    _btn.sd_layout
    .topSpaceToView(imgv2, 42)
    .rightSpaceToView(imgv2, 15)
    .widthIs(60)
    .heightIs(20);
    
    _typeCondL.sd_layout
    .leftEqualToView(_typeL)
    .centerYEqualToView(_btn)
    .rightSpaceToView(_btn, 2)
    .autoHeightRatio(0);
}


@end
