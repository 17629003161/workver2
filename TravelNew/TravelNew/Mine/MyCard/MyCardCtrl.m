//
//  MyCardCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyCardCtrl.h"
#import "MyCardCell.h"

@interface MyCardCtrl ()

@end

@implementation MyCardCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的卡券" titleColor:nil];
    [self.tableView registerClass:[MyCardCell class] forCellReuseIdentifier:@"cellid"];
   // [self.dataArray addObjectsFromArray:@[@(1),@(2),@(3)]];
    [self checkData];
}

- (void)checkData{
    if(self.dataArray.count>0){
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }else{
        self.tableView.tableFooterView = [Utils createNoDataView];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 91;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCardCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    return cell;
}


@end
