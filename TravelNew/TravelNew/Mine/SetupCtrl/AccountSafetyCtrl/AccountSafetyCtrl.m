//
//  AccountSafetyCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AccountSafetyCtrl.h"
#import "AccountSafetyCell.h"
#import "SetupCell.h"
#import "WXBindingCtrl.h"
#import "MobileNumberCtrl.h"
#import "LogoffCtrl.h"
#import "YTKBatchRequest.h"

@interface AccountSafetyCtrl ()
@property(nonatomic,assign) BOOL isWxBunding;
@property(nonatomic,assign) BOOL isAliBunding;
@property(nonatomic,strong) UILabel *wxBundingLabel;
@property(nonatomic,strong) UILabel *aliBundingLabel;

@end

@implementation AccountSafetyCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"账号与安全" titleColor:nil];
    [self disabelPageRefresh];
    [self.tableView registerClass:[AccountSafetyCell class] forCellReuseIdentifier:@"AccountSafetyCell"];
    [self.tableView registerClass:[SetupCell class] forCellReuseIdentifier:@"SetupCell"];
    UserInfo *user = [UserInfo shareInstance];
    if([user isRecomander]){
        [self.dataArray addObjectsFromArray:@[@[@"手机号"],@[@[@"微信",@"weixin_setup"],@[@"支付宝",@"zhifubao_setup"]],@[@"注销推荐官"]]];
    }else{
        [self.dataArray addObjectsFromArray:@[@[@"手机号"],@[@[@"微信",@"weixin_setup"],@[@"支付宝",@"zhifubao_setup"]]]];
    }
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.backgroundColor = HexColor(0xF9F9F9);
    self.isWxBunding = NO;
    self.isAliBunding = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self sendBatchRequest];
}

- (void)sendBatchRequest {
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"user/data/isBindingWx"
                          para:@{}
                   description:@"是否绑定微信"];
    
    Api *api2 = [Api apiPostUrl:@"user/data/isBindingAli"
                          para:@{}
                   description:@"是否绑定支付宝"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        DLog(@"jsonstr = %@",request1.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        if([[json objectForKey:@"code"] integerValue] ==200){
            weakSelf.isWxBunding = [[json objectForKey:@"data"] boolValue];
            [weakSelf.tableView reloadData];
        }
        
        YTKRequest *request2 = batchRequest.requestArray[1];
        DLog(@"jsonstr = %@",request2.responseString);
        json = [NSString jsonStringToDictionary:request2.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            weakSelf.isAliBunding = [[json objectForKey:@"data"] boolValue];
            [weakSelf.tableView reloadData];
        }
    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            DLog(@"%@",request1.error);
            if([request1.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            DLog(@"%@",request2.error);
            if([request2.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }
        
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1){
        NSArray *array = self.dataArray[indexPath.section];
        NSArray *itemArray = array[indexPath.row];
        AccountSafetyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountSafetyCell"];
        cell.leftLabel.text = itemArray[0];
        cell.rightLabel.text = @"去绑定";
        if(indexPath.row==0 && self.isWxBunding){
            cell.rightLabel.text = @"已绑定";
        }else if(indexPath.row==1 && self.isAliBunding){
            cell.rightLabel.text = @"已绑定";
        }
        cell.iconImgv.image = [UIImage imageNamed:itemArray[1]];
        return cell;
    }else{
        NSArray *array = self.dataArray[indexPath.section];
        SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetupCell"];
        cell.leftLabel.text = array[indexPath.row];
        if(indexPath.section==0){
            UserInfo *user = [UserInfo shareInstance];
            cell.rightLabel.text = user.phone;
        }else{
            cell.rightLabel.text = @"";
        }
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [self.dataArray objectAtIndex:section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    switch (section) {
        case 0:{
            MobileNumberCtrl *vc = [MobileNumberCtrl new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:{
            if(indexPath.row==0){
                if(!self.isWxBunding){
                    WXBindingCtrl *vc = [WXBindingCtrl new];
                    vc.isWeiXinBinding = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{
                if(!self.isAliBunding){
                    WXBindingCtrl *vc = [WXBindingCtrl new];
                    vc.isWeiXinBinding = NO;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
        }
            break;
        case 2:{
            UserInfo *user = [UserInfo shareInstance];
            if(![user isRecomander]){
                [MBProgressHUD showMessage:@"您不是推荐官无需注销"];
                return;
            }
            if([user isCurrentRecomander]){
                LogoffCtrl *vc = [LogoffCtrl new];
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [MBProgressHUD showMessage:@"注销需切换到推荐官身份"];
            }
        }
            break;
        default:
            break;
    }
}

@end
