//
//  AccountSafetyCell.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AccountSafetyCell.h"

@interface AccountSafetyCell()
@property(nonatomic,strong) UIImageView *arrow;
@end

@implementation AccountSafetyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configViews
{
    _leftLabel = [Utils createLabelWithTitle:@"微信" titleColor:HexColor(0x323232) fontSize:17];
    _iconImgv = ImageViewWithImage(@"weixin_setup");
    _rightLabel = [Utils createLabelWithTitle:@"1.0.0" titleColor:HexColor(0x999999) fontSize:16];
    _arrow = ImageViewWithImage(@"arrow_right");
    [self.contentView sd_addSubviews:@[_leftLabel,_iconImgv,_rightLabel,_arrow]];
    _leftLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 12)
    .autoHeightRatio(0);
    [_leftLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _arrow.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 12)
    .widthIs(8)
    .heightIs(14);
    
    _rightLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(_arrow, 5)
    .autoHeightRatio(0);
    [_rightLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _iconImgv.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(_rightLabel, 10)
    .widthIs(24)
    .heightIs(24);
}

@end
