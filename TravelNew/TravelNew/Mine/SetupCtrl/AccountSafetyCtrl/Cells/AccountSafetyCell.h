//
//  AccountSafetyCell.h
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountSafetyCell : BaseCustomTableViewCell
@property(nonatomic,strong) UILabel *leftLabel;
@property(nonatomic,strong) UILabel *rightLabel;
@property(nonatomic,strong) UIImageView *iconImgv;

@end

NS_ASSUME_NONNULL_END
