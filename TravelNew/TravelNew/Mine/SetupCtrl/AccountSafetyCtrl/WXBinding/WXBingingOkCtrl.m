//
//  WXBingingOkCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "WXBingingOkCtrl.h"

@interface WXBingingOkCtrl ()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *subTitleLabel;
@end

@implementation WXBingingOkCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isWeiXinBinding = _isWeiXinBinding;
}

-(void)setIsWeiXinBinding:(BOOL)isWeiXinBinding
{
    _isWeiXinBinding = isWeiXinBinding;
    if(isWeiXinBinding){
        [self setTitle:@"微信绑定" titleColor:nil];
        self.titleLabel.text = @"微信";
        self.subTitleLabel.text = @"您已绑定微信账号，可通过微信登录旅咖出行";
        self.imgv.image = [UIImage imageNamed:@"weixin_setup"];
    }else{
        [self setTitle:@"支付宝绑定" titleColor:nil];
        self.titleLabel.text = @"支付宝";
        self.subTitleLabel.text = @"您已绑定支付宝账号，可通过支付宝登录旅咖出行";
        self.imgv.image = [UIImage imageNamed:@"zhifubao_binding"];
    }
}

- (void)layout{
    UIImageView *imagv = ImageViewWithImage(@"wx_binding");
    self.imgv = imagv;
    UILabel *label1 = [Utils createLabelWithTitle:@"微信" titleColor:HexColor(0x323232) fontSize:17];
    self.titleLabel = label1;
    UILabel *label2 = [Utils createLabelWithTitle:@"您还未绑定支付宝账号" titleColor:HexColor(0x999999) fontSize:14];
    self.subTitleLabel = label2;
    [self.view sd_addSubviews:@[imagv,
                                label1,
                                label2,
                                ]];
    imagv.sd_layout
    .topSpaceToView(self.view, 41)
    .centerXEqualToView(self.view)
    .widthIs(60)
    .heightIs(60);
    
    label1.sd_layout
    .topSpaceToView(imagv, 9)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 12)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
}

@end
