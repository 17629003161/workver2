//
//  WXBindingCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "WXBindingCtrl.h"
#import "WXBingingOkCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXLoginShare.h"

@interface WXBindingCtrl ()<WXLoginDelegate>
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *subTitleLabel;
@end

@implementation WXBindingCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isWeiXinBinding = _isWeiXinBinding;
}

-(void)setIsWeiXinBinding:(BOOL)isWeiXinBinding
{
    _isWeiXinBinding = isWeiXinBinding;
    if(isWeiXinBinding){
        [self setTitle:@"微信绑定" titleColor:nil];
        self.titleLabel.text = @"微信";
        self.subTitleLabel.text = @"您还未绑定微信账号";
        self.imgv.image = [UIImage imageNamed:@"weixin_setup"];
    }else{
        [self setTitle:@"支付宝绑定" titleColor:nil];
        self.titleLabel.text = @"支付宝";
        self.subTitleLabel.text = @"您还未绑定支付宝账号";
        self.imgv.image = [UIImage imageNamed:@"zhifubao_binding"];
    }
}


- (void)pushToRsultPage{
    WXBingingOkCtrl *vc = [WXBingingOkCtrl new];
    vc.isWeiXinBinding = self.isWeiXinBinding;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)bindingBtnIsClicked:(id)sender
{
    if(self.isWeiXinBinding){
        WXLoginShare *loginShare = [WXLoginShare shareInstance];
        loginShare.delegate = self;
        [loginShare WXBunding];
    }else{
        [self doAliBunding];
    }
}

#pragma mark -绑定支付宝
- (void)doAliBunding
{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/data/alipayAuth" para:@{} description:@"获取支付宝userinfo"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSString *jsonstr = request.responseString;
            DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSString *authInfoStr = [json objectForKey:@"data"];
            DLog(@"authInfoStr = %@",authInfoStr);
            [weakSelf doAliAuthoWithAuthString:authInfoStr];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)doAliAuthoWithAuthString:(NSString *)authInfoStr
{
    LKWeakSelf
    [[AlipaySDK defaultService] auth_V2WithInfo:authInfoStr
                                     fromScheme:@"alisdkdemo"
                                       callback:^(NSDictionary *resultDic) {
                                           DLog(@"result = %@",resultDic);
                                           // 解析 auth code
                                           NSString *result = resultDic[@"result"];
                                           NSString *authCode = nil;
                                           if (result.length>0) {
                                               NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                                               for (NSString *subResult in resultArr) {
                                                   if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                                                       authCode = [subResult substringFromIndex:10];
                                                       break;
                                                   }
                                               }
                                           }
                                           DLog(@"授权结果 authCode = %@", authCode?:@"");
                                            if(authCode==nil){
                                                [MBProgressHUD showMessage:@"支付宝授权失败"];
                                            }else{
                                                [weakSelf aliBundingRequest:authCode];
                                            }
                                       }];
}

- (void)aliBundingRequest:(NSString *)authCode{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/data/bindingAli"
                          para:@{@"code":authCode}
                   description:@"绑定支付宝"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [weakSelf pushToRsultPage];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
        }];
}


- (void)wxLoginShareSuccess:(NSDictionary *)dict{
    
}
//微信绑定成功回调
- (void)wxBundingSucess:(NSDictionary *)dict{
    DLog(@"微信绑定成功！");
    [self pushToRsultPage];
}
/**
 d登录失败微信后台返回的数据
 */
- (void)wxLoginShareFail:(NSDictionary *)dict{
    
}





- (void)layout{
    UIImageView *imagv = ImageViewWithImage(@"wx_binding");
    self.imgv = imagv;
    UILabel *label1 = [Utils createLabelWithTitle:@"微信" titleColor:HexColor(0x323232) fontSize:17];
    self.titleLabel = label1;
    UILabel *label2 = [Utils createLabelWithTitle:@"您还未绑定支付宝账号" titleColor:HexColor(0x999999) fontSize:14];
    self.subTitleLabel = label2;
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"去绑定" font:LKSystemFont(17) cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(bindingBtnIsClicked:)];
    [self.view sd_addSubviews:@[imagv,
                                label1,
                                label2,
                                btn]];
    imagv.sd_layout
    .topSpaceToView(self.view, 41)
    .centerXEqualToView(self.view)
    .widthIs(60)
    .heightIs(60);
    
    label1.sd_layout
    .topSpaceToView(imagv, 9)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 12)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(label2, 55)
    .centerXEqualToView(self.view)
    .widthIs(kScreenWidth-56)
    .heightIs(44);
    
}


@end
