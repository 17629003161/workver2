//
//  NewPhoneNumberCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "NewPhoneNumberCtrl.h"
#include "VerifyCodeInputCtrl.h"

@interface NewPhoneNumberCtrl ()<UITextFieldDelegate>
@property(nonatomic,strong) UITextField *phoneField;
@property(nonatomic,strong) LKGadentButton *btn;
@end

@implementation NewPhoneNumberCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"手机号码" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [self.view addTapGestureTarget:self action:@selector(viewIsTaped:)];
    [self layout];
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    [self.phoneField resignFirstResponder];
}

- (void)btnIsClicked:(id)sender
{
    [self sendVerifyCode];
    VerifyCodeInputCtrl *vc = [VerifyCodeInputCtrl new];
    vc.phoneNumber = self.phoneField.text;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendVerifyCode{
    Api *api = [Api apiPostUrl:@"user/getReplacePhoneValidate"
                          para:@{@"phone":self.phoneField.text}
                   description:@"获取修改手机号码的验证码"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:@"验证码发送成功！"];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
        }];
}


- (void)layout{
    UIView *topview = [self makeTopView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"获取验证码" font:LKSystemFont(17)
                                   cornerRadius:22.0];
    btn.isEnabled = NO;
    self.btn = btn;
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [self.view sd_addSubviews:@[topview,btn]];
    topview.sd_layout
    .topSpaceToView(self.view, 11)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(50);
    
    btn.sd_layout
    .topSpaceToView(topview, 120)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(44);
    
}

- (UIView *)makeTopView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"手机号" titleColor:HexColor(0x323232) fontSize:17];
    UITextField *phoneField = [UITextField new];
    phoneField.clearButtonMode = UITextFieldViewModeAlways;
    phoneField.delegate = self;
    phoneField.placeholder = @"输入新手机号码";
    phoneField.font = LKSystemFont(17);
    phoneField.textAlignment = NSTextAlignmentRight;
    phoneField.keyboardType = UIKeyboardTypePhonePad;
    self.phoneField = phoneField;
    [view sd_addSubviews:@[label,phoneField]];
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 13)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneField.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .leftSpaceToView(label, 0)
    .heightRatioToView(view, 1);
    return view;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
     DLog(@"textField4 - 正在编辑, 当前输入框内容为: %@",textField.text);
    NSString *str = textField.text;
    str = [str stringByReplacingCharactersInRange:range withString:string];
    if(str.length>0){
        _btn.isEnabled = YES;
    }else{
        _btn.isEnabled = NO;
    }
    return YES;
}

@end
