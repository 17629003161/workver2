//
//  VerifyCodeInputCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "VerifyCodeInputCtrl.h"
#import "CodeInputView.h"
#import "ChangeMobileNumberOkCtrl.h"

@interface VerifyCodeInputCtrl ()
@property(nonatomic,strong)CodeInputView * codeView;
@end

@implementation VerifyCodeInputCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [self layout];
}

- (void)layout{
    UILabel *label = [Utils createBoldLabelWithTitle:@"输入短信验证码" titleColor:HexColor(0x2C2C2C) fontSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"验证码已发送至 +86 124 1249 2934" titleColor:HexColor(0xCACACA) fontSize:14];
    UILabel *label3 = [Utils createLabelWithTitle:@"59s后可重新发送" titleColor:HexColor(0xCACACA) fontSize:14];
    [self.view sd_addSubviews:@[label,label2,label3]];
    [self.view addSubview:self.codeView];
    label.sd_layout
    .topSpaceToView(self.view, 75)
    .leftSpaceToView(self.view, 36)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 1)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.codeView.sd_layout
    .topSpaceToView(label2, 80)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    label3.sd_layout
    .topSpaceToView(self.codeView, 32)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
}

- (CodeInputView *)codeView {
  LKWeakSelf
    if (!_codeView) {
        _codeView = [[CodeInputView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 80) inputType:6 selectCodeBlock:^(NSString * code) {
            DLog(@"code === %@",code);
            if ([code length]==6) {
                [weakSelf changePhoneNumber:code];
            }
        }];
        _codeView.center = self.view.center;
    }
    return _codeView;
}

- (void)changePhoneNumber:(NSString *)code
{
    LKWeakSelf
   // UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"user/changePhone"
                          para:@{
                              @"phone":self.phoneNumber,
                              @"validate":code}
                   description:@"修改绑定手机号"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            UserInfo *user = [UserInfo shareInstance];
            user.phone = self.phoneNumber;
            ChangeMobileNumberOkCtrl *vc = [ChangeMobileNumberOkCtrl new];
            vc.phoneNumber = self.phoneNumber;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            [MBProgressHUD showMessage:json[@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
}


@end
