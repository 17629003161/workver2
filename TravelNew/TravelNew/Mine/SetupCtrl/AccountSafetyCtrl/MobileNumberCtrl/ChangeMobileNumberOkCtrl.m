//
//  ChangeMobileNumberOkCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ChangeMobileNumberOkCtrl.h"

@interface ChangeMobileNumberOkCtrl ()

@end

@implementation ChangeMobileNumberOkCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [self layout];
}

- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    [self popToViewControllerName:@"AccountSafetyCtrl"];
}

- (void)viewWillDisappear:(BOOL)animated{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        [self popToViewControllerName:@"AccountSafetyCtrl"];
    }
}

- (void)layout{
    UIImageView *imgv = ImageViewWithImage(@"finish_rightmark");
    UILabel *label = [Utils createBoldLabelWithTitle:@"更换成功" titleColor:HexColor(0x323232) fontSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"新手机号可用于账号登录" titleColor:HexColor(0x666666) fontSize:15];
    UILabel *label3 = [Utils createLabelWithTitle:@"+138 5788 5235" titleColor:HexColor(0x666666) fontSize:15];
    label3.text = [NSString stringWithFormat:@"+%@",self.phoneNumber];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"完成" font:LKSystemFont(17) cornerRadius:22.0];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [self.view sd_addSubviews:@[imgv,
                                label,
                                label2,
                                label3,
                                btn]];
    imgv.sd_layout
    .topSpaceToView(self.view, 56)
    .centerXEqualToView(self.view)
    .widthIs(80)
    .heightIs(80);
    
    label.sd_layout
    .topSpaceToView(imgv, 30)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 9)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .topSpaceToView(label2, 2)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(label3, 74)
    .centerXEqualToView(self.view)
    .widthIs(kScreenWidth-56)
    .heightIs(44);
}

@end
