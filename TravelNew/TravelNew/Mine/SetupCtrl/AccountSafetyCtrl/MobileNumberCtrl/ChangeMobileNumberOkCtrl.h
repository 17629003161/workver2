//
//  ChangeMobileNumberOkCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangeMobileNumberOkCtrl : LKBaseViewController
@property(nonatomic,strong) NSString *phoneNumber;
@end

NS_ASSUME_NONNULL_END
