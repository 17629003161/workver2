//
//  MobileNumberCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MobileNumberCtrl.h"
#import "SendVerifyCodeCtrl.h"

@interface MobileNumberCtrl ()
@property(nonatomic,strong) UILabel *phoneNumber;
@end

@implementation MobileNumberCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [self layout];
    UserInfo *user = [UserInfo shareInstance];
    self.phoneNumber.text = user.phone;
}
- (void)btnIsClicked:(id)sender
{
    SendVerifyCodeCtrl *vc = [SendVerifyCodeCtrl new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)layout{
    UIView *topview = [self makeTopView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"更换手机号" font:LKSystemFont(17)
                                   cornerRadius:22.0];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [self.view sd_addSubviews:@[topview,btn]];
    topview.sd_layout
    .topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(50);
    
    btn.sd_layout
    .topSpaceToView(topview, 120)
    .centerXEqualToView(self.view)
    .widthIs(kScreenWidth-56)
    .heightIs(44);
}

- (UIView *)makeTopView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createBoldLabelWithTitle:@"手机号" titleColor:HexColor(0x323232) fontSize:17];
    _phoneNumber = [Utils createLabelWithTitle:@"13152524926" titleColor:HexColor(0x323232) fontSize:17];
    [view sd_addSubviews:@[label,_phoneNumber]];
    [self.view addSubview:view];
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 13)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _phoneNumber.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 13)
    .autoHeightRatio(0);
    [_phoneNumber setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

@end
