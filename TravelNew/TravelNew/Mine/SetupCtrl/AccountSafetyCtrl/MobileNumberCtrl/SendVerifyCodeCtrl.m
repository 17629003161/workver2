//
//  SendVerifyCodeCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SendVerifyCodeCtrl.h"
#import "NewPhoneNumberCtrl.h"

@interface SendVerifyCodeCtrl ()<UITextFieldDelegate>
@property(nonatomic,strong) UITextField *verifyField;
@property(nonatomic,strong) UIButton *btn;
@property(nonatomic,strong) LKGadentButton *verbtn;
@end

@implementation SendVerifyCodeCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [self.view addTapGestureTarget:self action:@selector(viewIsClicked:)];
    [self layout];
}

- (void)viewIsClicked:(UITapGestureRecognizer *)reg
{
    [self.verifyField resignFirstResponder];
}

- (void)btnChangeNumberIsClicked:(id)sender
{
    LKWeakSelf
   // UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"user/changePhone"
                          para:@{@"validate":self.verifyField.text}
                   description:@"修改绑定手机号"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [weakSelf nextStep];
        }else{
            [MBProgressHUD showMessage:json[@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
}

- (void)nextStep
{
    NewPhoneNumberCtrl *vc = [NewPhoneNumberCtrl new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)sendVerifyCode{
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"user/getReplacePhoneValidate"
                          para:@{@"phone":user.phone}
                   description:@"获取修改手机号码的验证码"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:@"验证码发送成功！"];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
        }];
}



- (void)layout{
    UserInfo *user = [UserInfo shareInstance];
    NSString *maskphone = [Utils maskPhoneNumber:user.phone];
    NSString *str = [NSString stringWithFormat:@"请使用当前绑定手机号 +86 %@ 接收验证码\n以完成验证",maskphone];
    UILabel *label1 = [Utils createLabelWithTitle:str titleColor:HexColor(0x666666) fontSize:13];
    label1.numberOfLines = 0;
    UIView *inputview = [self makeInputView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"更换手机号" font:LKSystemFont(17)
                                   cornerRadius:22.0];
    [btn addTapGestureTarget:self action:@selector(btnChangeNumberIsClicked:)];
    self.verbtn = btn;
    [self.view sd_addSubviews:@[label1,inputview,btn]];
    label1.sd_layout
    .topSpaceToView(self.view, 12)
    .leftSpaceToView(self.view, 14)
    .rightSpaceToView(self.view, 14)
    .autoHeightRatio(0);
    
    inputview.sd_layout
    .topSpaceToView(label1, 14)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(50);
    
    btn.sd_layout
    .topSpaceToView(inputview, 61)
    .centerXEqualToView(self.view)
    .widthIs(kScreenWidth-56)
    .heightIs(44);
    
}

- (UIView *)makeInputView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    
    UITextField *phoneField = [UITextField new];
    phoneField.clearButtonMode = UITextFieldViewModeAlways;
    phoneField.delegate = self;
    phoneField.placeholder = @"输入验证码";
    phoneField.font = LKSystemFont(17);
    self.verifyField = phoneField;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x4D89FD) forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0xB3B3B3) forState:UIControlStateDisabled];
    [btn setTitle:@"60s" forState:UIControlStateDisabled];
    [btn addTarget:self action:@selector(btnSendVerifyCodeIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.btn = btn;
    
    [view sd_addSubviews:@[phoneField,btn]];
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(86)
    .heightIs(40);
    
    phoneField.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 13)
    .rightSpaceToView(btn, 0)
    .bottomEqualToView(view);
    return view;
}

- (void)btnSendVerifyCodeIsClicked:(id)sender
{
    [self sendVerifyCode];
    _btn.enabled = NO;
    //创建Timer
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timer_callback:) userInfo:nil repeats:YES];
      //使用NSRunLoopCommonModes模式，把timer加入到当前Run Loop中。
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

//timer的回调方法
- (void)timer_callback:(NSTimer *)timer
{
    const NSInteger totalTime = 60;
    static NSInteger seconds = totalTime;
   // DLog(@"Timer %@", [NSThread currentThread]);
    if(seconds==0){
        _btn.enabled = YES;
        [_btn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _btn.enabled = YES;
        seconds = totalTime;
        [timer invalidate];
        timer = nil;
    }else{
        _btn.enabled = NO;
        NSString *str = [NSString stringWithFormat:@"%lds",seconds];
        [_btn setTitle:str forState:UIControlStateDisabled];
    }
    if(seconds>0){
        seconds--;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
     DLog(@"textField4 - 正在编辑, 当前输入框内容为: %@",textField.text);
    NSString *str = textField.text;
    str = [str stringByReplacingCharactersInRange:range withString:string];
    if(str.length>0){
        _verbtn.isEnabled = YES;
    }else{
        _verbtn.isEnabled = NO;
    }
    return YES;
}


@end
