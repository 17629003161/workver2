//
//  SetupCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SetupCtrl.h"
#import "SetupCell.h"
#import "AccountSafetyCtrl.h"
#import "PrivateSetupCtrl.h"
#import "PersonInfoCtrl.h"
#import "PersonInfoCustomCtrl.h"
#import "ServiceSetupMainCtrl.h"
#import "AboutCtrl.h"
#import "UserGuideCtrl.h"

@interface SetupCtrl ()

@end

@implementation SetupCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"设置" titleColor:nil];
    [self disabelPageRefresh];
    self.tableView.backgroundColor = HexColor(0xF9F9F9);
    [self.tableView registerClass:[SetupCell class] forCellReuseIdentifier:@"SetupCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.dataArray addObjectsFromArray:@[@[@"账号与安全"], @[@"个人资料",@"开通服务",@"关于旅咖出行"]]];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [btn setTitle:@"退出登录" forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x323232) forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(logoutIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    btn.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomSpaceToView(self.view, SAFEVIEWBOTTOMMARGIN)
    .heightIs(50);

    self.tableView.sd_layout
    .bottomSpaceToView(btn, 0);
    
}


- (void)logoutIsClicked:(id)sender
{
    UserInfo *user = [UserInfo shareInstance];
    if(user.isLogin){
        [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
            if (!aError) {
                DLog(@"退出登录成功");
            } else {
                DLog(@"退出登录失败的原因---%@", aError.errorDescription);
            }
        }];
        user.isLogin = NO;
        [user save];
        [MBProgressHUD showMessage:@"退出登录成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [self.dataArray objectAtIndex:section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetupCell"];
    NSArray *array = self.dataArray[indexPath.section];
    cell.leftLabel.text = array[indexPath.row];
    if(indexPath.section==1 && indexPath.row==2){
        cell.rightLabel.text = @"1.0.0";
    }else{
        cell.rightLabel.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        switch (indexPath.row) {
            case 0:{
                AccountSafetyCtrl *vc = [AccountSafetyCtrl new];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 1:{
                PrivateSetupCtrl *vc = [PrivateSetupCtrl new];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:{
                UserInfo *user = [UserInfo shareInstance];
                if([user isCurrentRecomander]){
                    PersonInfoCtrl *vc = [PersonInfoCtrl new];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    PersonInfoCustomCtrl *vc = [PersonInfoCustomCtrl new];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
                break;
            case 1:{
                UserInfo *user = [UserInfo shareInstance];
                if([user isRecomander]){
                    ServiceSetupMainCtrl *vc = [ServiceSetupMainCtrl new];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    UserGuideCtrl *vc = [UserGuideCtrl new];
                    vc.tipTitle = @"推荐官协议";
                    vc.photoName = @"RecomandProtocol";
                    vc.buttonHide = NO;
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
                break;
            case 2:{
                AboutCtrl *vc = [[AboutCtrl alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;;
            default:
                break;
        }
    }
}


@end
