//
//  SetupCell.h
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetupCell : BaseCustomTableViewCell
@property(nonatomic,strong) UIImageView *arrow;
@property(nonatomic,strong) UILabel *leftLabel;
@property(nonatomic,strong) UILabel *rightLabel;
@end

NS_ASSUME_NONNULL_END
