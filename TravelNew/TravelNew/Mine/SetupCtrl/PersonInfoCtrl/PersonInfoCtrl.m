//
//  PersonInfoCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PersonInfoCtrl.h"
#import "AddCarAuthCtrl.h"
#import "AddRoomAuthCtrl.h"
#import "RealNameInfoCtrl.h"
#import "SelectCityCtrl.h"
#import "EMTextView.h"
#import "YTKBatchRequest.h"

@interface PersonInfoCtrl ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) UIImageView *photoImgv;
@property(nonatomic,strong) UIImageView *headImageView;
@property(nonatomic,strong) UILabel *addressLabel;
@property(nonatomic,strong) EMTextView *signTextView;
@property(nonatomic,strong) AMapDistrict * dist;

@property(nonatomic,strong) UITextField *nickNameField;
@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSString *imageFilePath;
@property(nonatomic,strong) NSString *headImagePath;
@property(nonatomic,assign) NSInteger imgIndex;                 //imgIndex = 0   headImagePath      imgIndex = 1  imageFilePath
@property(nonatomic,strong) LKMessageDialog *dlg;
@property(nonatomic,assign) BOOL isModify;


@end

@implementation PersonInfoCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"个人资料" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF9F9F9);
    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self addRightButton];
    self.iskeyBordScrollUp = YES;
    [self.view addTapGestureTarget:self action:@selector(viewIsTaped:)];
    [self layout];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth,657);
    self.isModify = NO;
    [self loadData];
    self.iskeyBordScrollUp = NO;
    [self customBackButton];
}

// 自定义返回按钮
- (void)customBackButton{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"left_arrow_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = item;
}
// 返回按钮按下
- (void)backBtnClicked:(UIButton *)sender{
   LKWeakSelf
    UserInfo *user = [UserInfo shareInstance];
    if(![user.sign isEqualToString:self.signTextView.text]){
        self.isModify = YES;
    }
    if(!self.isModify){
      [self.navigationController popViewControllerAnimated:YES];
        return;
    }
   if(!_dlg){
       _dlg = [LKMessageDialog createMessageDialogWithTitle:@"你确保存？"
                                                                     content:@"您确认要保存所做的修改吗？"
                                                                leftBtnTitle:@"取消"
                                                               rightBtnTitle:@"确认"];
       _dlg.frame = CGRectMake(0, 0, 270, 160);
       _dlg.okBlock = ^{
            [weakSelf sendBatchRequest];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
       _dlg.cancleBlock = ^{
           [weakSelf.navigationController popViewControllerAnimated:YES];
       };
    }
    [self.dlg show];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.dlg hide];
}

- (void)loadData{
    UserInfo *user = [UserInfo shareInstance];
    if(!isNull(user.userNickname)){
        self.nickNameField.text = user.userNickname;
    }
    if(!isNull(user.headerImage)){
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:user.headerImage] placeholderImage:[UIImage imageNamed:@"default_header"]];
    }
    
    if(self.dist==nil){
        self.dist = [AMapDistrict new];
        self.dist.citycode = user.cityid;
        self.dist.name = user.city;
    }
    [self.photoImgv sd_setImageWithURL:[NSURL URLWithString:user.bgImage] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        self.image = image;
        self.imageFilePath = user.bgImage;
    }];
    if(!isNull(user.city))
        self.addressLabel.text = user.city;
    if(user.sign && user.sign.length>0)
        self.signTextView.text = user.sign;
    [self.addressLabel updateLayout];
}

-(void)setImage:(UIImage *)image
{
    _image = image;
}

-(void)setImageFilePath:(NSString *)imageFilePath
{
    _imageFilePath = imageFilePath;
}
#pragma 保存信息
- (void)btnSaveIsClicked:(id)sender
{
    [self sendBatchRequest];
}

- (void)sendBatchRequest{
    LKWeakSelf
    if(self.imageFilePath == nil){
        [MBProgressHUD showMessage:@"请选择背景图片"];
        return;
    }
    if(self.dist==nil){
        [MBProgressHUD showMessage:@"请选择城市"];
        return;
    }
    if(self.signTextView.text.length ==0){
        [MBProgressHUD showMessage:@"请输入自我简介"];
        return;
    }
    NSString *sign = self.signTextView.text;
    Api *api1 = [Api apiPostUrl:@"user/LtReferrerUser/updateRefinfo"
                          para:@{@"sign":sign,
                              @"bgImage":self.imageFilePath,
                               @"cityid":self.dist.citycode}
                   description:@"修改推荐官信息"];
    
    NSMutableDictionary *para = [NSMutableDictionary new];
    if(self.headImagePath){
        [para setObject:self.headImagePath forKey:@"headerImage"];
    }
    if(self.nickNameField.text.length >0){
        [para setObject:self.nickNameField.text forKey:@"userNickname"];
    }
    Api *api2 = [Api apiPostUrl:@"user/changeUserInfo"
                          para:para
                   description:@"user-修改用户信息"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            weakSelf.addressLabel.text = self.dist.name;
            [weakSelf.addressLabel updateLayout];
            UserInfo *user = [UserInfo shareInstance];
            user.city = self.dist.name;
            user.cityid = self.dist.citycode;
            user.sign = self.signTextView.text;
            user.bgImage = self.imageFilePath;
            [user save];
        }

        YTKRequest *request2 = batchRequest.requestArray[1];
        NSDictionary *json2 = [NSString jsonStringToDictionary:request2.responseString];;
        if([[json2 objectForKey:@"code"] integerValue]==200){
            UserInfo *user = [UserInfo shareInstance];
            if(self.headImagePath){
                user.headerImage = self.headImagePath;
            }
            if(self.nickNameField.text.length>0){
                user.userNickname = self.nickNameField.text;
            }
            [user save];
        }
        [self.navigationController popViewControllerAnimated:YES];

    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            DLog(@"api1 error:%@ responsestr = %@",request1.error,request1.responseString);
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            DLog(@"api2 error:%@ responsestr = %@",request2.error,request2.responseString);
        }
    }];
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    [self.signTextView resignFirstResponder];
}

-(void)setDist:(AMapDistrict *)dist
{
    _dist = dist;
    self.addressLabel.text = dist.name;
}

- (void)addRightButton{
    UIButton* btn= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    UIBarButtonItem* barItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    [btn addTarget:self action:@selector(btnSaveIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = barItem;
}

- (void)layout{
    UIImageView *imagv = ImageViewWithImage(@"header_person");
    imagv.layer.cornerRadius = 37;
    imagv.layer.masksToBounds = YES;
    self.headImageView = imagv;
    [imagv addTapGestureTarget:self action:@selector(headImageIsClicked:)];
    UIImageView *cameraimgv = ImageViewWithImage(@"camera_icon");
    cameraimgv.layer.cornerRadius = 10;
    cameraimgv.layer.masksToBounds = YES;
    [imagv addSubview:cameraimgv];
    UITextField *nameField = [UITextField new];
    nameField.textColor = HexColor(0x323232);
    nameField.font = LKSystemFont(20);
    nameField.text = @"蚂蚁一直在";
    [nameField setUserInteractionEnabled:NO];
    self.nickNameField = nameField;
    
    UIImageView *pen = ImageViewWithImage(@"pen_icon");
    [pen addTapGestureTarget:self action:@selector(penIsClicked:)];
    
    UIView *bottomView = [self createBottomView];
    self.bottomView = bottomView;
    [self.scrollView addSubview:bottomView];
    
    [self.view sd_addSubviews:@[imagv,nameField,pen,self.scrollView]];
    
    cameraimgv.sd_layout
    .rightSpaceToView(imagv, 3)
    .bottomSpaceToView(imagv, 9)
    .widthIs(20)
    .heightIs(20);
    
    imagv.sd_layout
    .topSpaceToView(self.view, 20)
    .leftSpaceToView(self.view, 18)
    .widthIs(74)
    .heightIs(74);
    
    nameField.sd_layout
    .centerYEqualToView(imagv)
    .leftSpaceToView(imagv, 23)
    .widthIs(180)
    .heightIs(24);
    
    pen.sd_layout
    .leftSpaceToView(nameField, 10)
    .centerYEqualToView(nameField)
    .widthIs(14)
    .heightIs(14);
    
    bottomView.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    self.scrollView.sd_layout
    .topSpaceToView(imagv, 17)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (void)realNameAuth:(UITapGestureRecognizer *)reg
{
    RealNameInfoCtrl *vc = [RealNameInfoCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)carAuth:(UITapGestureRecognizer *)reg
{
    AddCarAuthCtrl *vc = [AddCarAuthCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)roomAuth:(UITapGestureRecognizer *)reg
{
    AddRoomAuthCtrl *vc = [AddRoomAuthCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)selectCityIsClicked:(UITapGestureRecognizer *)reg
{
    SelectCityCtrl *vc = [SelectCityCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.OnSelectAddress = ^(AMapDistrict * dist) {
        self.dist = dist;
        self.isModify = YES;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)penIsClicked:(UITapGestureRecognizer *)reg
{
    static int i = 0;
    i = (i+1) % 2;
    if(i==1){
        [self.nickNameField setUserInteractionEnabled:YES];
        [self.nickNameField becomeFirstResponder];
    }else{
        [self.nickNameField resignFirstResponder];
        [self.nickNameField setUserInteractionEnabled:NO];
    }
    self.isModify = YES;
}

- (UIView *)createBottomView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIView *titleView1 = [self createTitleView:@"认证信息"
                                      subTitle:@"完善认证信息才能接单哦～"];
    UIView *verView1 = [self createVerifyView:@"实人认证"];
    [verView1 addTapGestureTarget:self action:@selector(realNameAuth:)];
    UIView *verView2 = [self createVerifyView:@"车辆认证"];
    [verView2 addTapGestureTarget:self action:@selector(carAuth:)];
    UIView *verView3 = [self createVerifyView:@"房屋认证"];
    [verView3 addTapGestureTarget:self action:@selector(roomAuth:)];
    UIView *titleView2 = [self createTitleView:@"个人信息"
                                      subTitle:@"完善个人信息更有优势哦～"];
    UIView *infov1 = [self createInfoView:@"服务地点"];
    [infov1 addTapGestureTarget:self action:@selector(selectCityIsClicked:)];
    LKLocationManager *manager = [LKLocationManager sharedManager];
    self.addressLabel = [Utils createBoldLabelWithTitle:manager.cityName titleColor:HexColor(0x323232) fontSize:16];
    [infov1 addSubview:_addressLabel];
    UIView *infov2 = [self createInfoView:@"主页背景"];
    [infov2 addTapGestureTarget:self action:@selector(imageIsClicked:)];
    self.photoImgv = ImageViewWithImage(@"home2");
    [infov2 addSubview:_photoImgv];
    UILabel *tipLabel = [Utils createLabelWithTitle:@"关于我" titleColor:HexColor(0x323232) fontSize:16];
    
    EMTextView *textView = [[EMTextView alloc] init];
    textView.backgroundColor = HexColor(0xededed);
    textView.placeholder = @"示例：展示内容不得含有违规，个人联系方式等，20个字以内即可。";
    textView.font = [UIFont systemFontOfSize:14];
    textView.returnKeyType = UIReturnKeySend;
    self.signTextView = textView;
    
    [view sd_addSubviews:@[titleView1,
                           verView1,
                           verView2,
                           verView3,
                           titleView2,
                           infov1,
                           infov2,
                           tipLabel,
                           textView]];
    _addressLabel.sd_layout
    .centerYEqualToView(infov1)
    .rightSpaceToView(infov1, 45)
    .autoHeightRatio(0);
    [_addressLabel setSingleLineAutoResizeWithMaxWidth:kScreenHeight];
    
    _photoImgv.sd_layout
    .centerYEqualToView(infov2)
    .rightSpaceToView(infov2, 45)
    .widthIs(40)
    .heightIs(40);
    
    titleView1.sd_layout
    .topSpaceToView(view, 16)
    .leftEqualToView(view)
    .rightEqualToView(view);
    
    verView1.sd_layout
    .topSpaceToView(titleView1, 20)
    .leftSpaceToView(view, 26)
    .rightSpaceToView(view, 26)
    .heightIs(56);
    
    verView2.sd_layout
    .topSpaceToView(verView1, 16)
    .leftEqualToView(verView1)
    .rightEqualToView(verView1)
    .heightIs(56);
    
    verView3.sd_layout
    .topSpaceToView(verView2, 16)
    .leftEqualToView(verView1)
    .rightEqualToView(verView1)
    .heightIs(56);
    
    titleView2.sd_layout
    .topSpaceToView(verView3, 30)
    .leftEqualToView(view)
    .rightEqualToView(view);
    
    infov1.sd_layout
    .topSpaceToView(titleView2, 25)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(50);
    
    infov2.sd_layout
    .topSpaceToView(infov1, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(50);
    
    tipLabel.sd_layout
    .topSpaceToView(infov2, 13)
    .leftSpaceToView(view, 26)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    textView.sd_layout
    .topSpaceToView(tipLabel, 11)
    .leftSpaceToView(view, 26)
    .rightSpaceToView(view, 26)
    .heightIs(104);
    [view setupAutoHeightWithBottomView:textView bottomMargin:45];
    
    return view;

}

- (UIView *)createInfoView:(NSString *)tipLabel
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:tipLabel titleColor:HexColor(0x323232) fontSize:16];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE5E5E5);
    UIImageView *arrow = ImageViewWithImage(@"rightArrow");
    [view sd_addSubviews:@[label,arrow,line]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 26)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    arrow.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 29)
    .widthIs(8)
    .heightIs(14);
    line.sd_layout
    .leftEqualToView(label)
    .rightEqualToView(arrow)
    .bottomEqualToView(view)
    .heightIs(0.5);
    return view;
}

- (UIView *)createTitleView:(NSString *)title subTitle:(NSString *)subTitle
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:title titleColor:HexColor(0x323232) fontSize:17];
    UILabel *label2 = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    [view sd_addSubviews:@[label1,label2]];
    
    label1.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label2 bottomMargin:0];
    return view;
}


- (UIView *)createVerifyView:(NSString *)title
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 2;
    view.layer.shadowColor = [UIColor colorWithRed:172/255.0 green:169/255.0 blue:169/255.0 alpha:0.3].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,2);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 5;
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x323232) fontSize:16];
    UIImageView *btn = ImageViewWithImage(@"btn_ver");
    btn.hidden = YES;
    [view sd_addSubviews:@[label,btn]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(41)
    .heightIs(16);
    return view;
}


- (void)headImageIsClicked:(UITapGestureRecognizer *)reg
{
    self.imgIndex = 0;
    [self chooseImage];
}

- (void)imageIsClicked:(UITapGestureRecognizer *)reg
{
    self.imgIndex = 1;
    [self chooseImage];
}


-(void)chooseImage {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//获取选择的图片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
   // [self cosUploadImgae:image];
    UIImage *littleImage = [Utils compressImageSize:image toByte:8*1024];
    DLog(@"imagesize : %lf    %lf",littleImage.size.width,  littleImage.size.height);
    DLog(@"screen: %lf, %lf",kScreenWidth,kScreenHeight);
    [self cosUploadImgae:littleImage];
}

//从相机或者相册界面弹出
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)cosUploadImgae:(UIImage *)image{
    LKWeakSelf
    self.isModify = YES;
    NSString *remoteDir = @"lvkauxing/img/";
    NSString* tempPath = QCloudTempFilePathWithExtension(@"png");
    QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
    // 存储桶名称，格式为 BucketName-APPID
    put.bucket = @"nlcx-1301827761";
    // 对象键，是对象在 COS 上的完整路径，如果带目录的话，格式为 "dir1/object1"
    put.object = [remoteDir stringByAppendingPathComponent:tempPath];
    //需要上传的对象内容。可以传入NSData*或者NSURL*类型的变量
    NSData *data = UIImagePNGRepresentation(image);
    put.body =  data;
    //监听上传进度
    [put setSendProcessBlock:^(int64_t bytesSent,
                                int64_t totalBytesSent,
                                int64_t totalBytesExpectedToSend) {
        //      bytesSent                   新增字节数
        //      totalBytesSent              本次上传的总字节数
        //      totalBytesExpectedToSend    本地上传的目标字节数
    }];

    //监听上传结果
    [put setFinishBlock:^(id outputObject, NSError *error) {
        //可以从 outputObject 中获取 response 中 etag 或者自定义头部等信息
        QCloudUploadObjectResult *result = (QCloudUploadObjectResult *)outputObject;
       // NSDictionary * result = (NSDictionary *)outputObject;
        DLog(@"file location = %@",result.location);
        if(self.imgIndex==0){
            weakSelf.headImagePath = result.location;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.headImageView sd_setImageWithURL:[NSURL URLWithString:weakSelf.headImagePath]];
            });
        }else{
            weakSelf.imageFilePath = result.location;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.photoImgv sd_setImageWithURL:[NSURL URLWithString:weakSelf.imageFilePath]];
            });
        }
    }];

    [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
}


@end
