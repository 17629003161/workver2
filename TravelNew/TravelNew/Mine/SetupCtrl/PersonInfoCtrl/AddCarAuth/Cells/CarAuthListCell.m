//
//  CarAuthListCell.m
//  TravelNew
//
//  Created by mac on 2020/12/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "CarAuthListCell.h"

@interface CarAuthListCell()
@property(nonatomic,strong) UILabel *carTypeLabel;
@property(nonatomic,strong) UILabel *carNumberLabel;
@property(nonatomic,strong) UILabel *carAgeLabel;

@end

@implementation CarAuthListCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(CarListEntity *)model
{
    _model = model;
    self.carTypeLabel.text = model.carModel;
    self.carNumberLabel.text = model.plate_no;
    self.carAgeLabel.text = [model.carAge stringValue];
    [self.carTypeLabel updateLayout];
    [self.carNumberLabel updateLayout];
    [self.carAgeLabel updateLayout];
}

- (void)setCarSetupListModel:(CarSetupListEntity *)carSetupListModel
{
    _carSetupListModel = carSetupListModel;
    self.carTypeLabel.text = carSetupListModel.carModel;
    self.carNumberLabel.text = carSetupListModel.plate_no;
    self.carAgeLabel.text = [carSetupListModel.carAge stringValue];
    [self.carTypeLabel updateLayout];
    [self.carNumberLabel updateLayout];
    [self.carAgeLabel updateLayout];
}


- (void)configViews{
    UIView *bgview = [UIView new];
    self.backgroundColor = HexColor(0xF9F9F9);
    bgview.backgroundColor = HexColor(0xF9F9F9);
    bgview.layer.borderColor = HexColor(0xD5D5D5).CGColor;
    bgview.layer.borderWidth = 0.5;
    bgview.layer.cornerRadius = 2.0;
    [self.contentView addSubview:bgview];
    
    UILabel *label1 = [Utils createLabelWithTitle:@"丰田 凯美瑞" titleColor:HexColor(0x323232) fontSize:18];
    self.carTypeLabel = label1;
    UIImageView *imgv = ImageViewWithImage(@"add_car");
    [self.contentView addSubview:imgv];
    UIView *boxview1 = [self createBoxView];
    UIView *boxview2 = [self createBoxView];
    [bgview sd_addSubviews:@[label1,
                             boxview1,boxview2]];
    bgview.sd_layout
    .topSpaceToView(self.contentView, 30)
    .bottomSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20);
    
    label1.sd_layout
    .topSpaceToView(bgview, 17)
    .leftSpaceToView(bgview, 16)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .topSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 46)
    .widthIs(82)
    .heightIs(34);
    
    CGFloat width = (kScreenWidth-72-19)/2.0;
    boxview1.sd_layout
    .topSpaceToView(label1, 20)
    .leftEqualToView(label1)
    .widthIs(width)
    .heightIs(40);
    
    boxview2.sd_layout
    .centerYEqualToView(boxview1)
    .leftSpaceToView(boxview1, 19)
    .rightSpaceToView(bgview, 16)
    .heightIs(40);
    
    UILabel *label11 = [boxview1 viewWithTag:1];
    label11.text = @"车牌号";
    UILabel *label12 = [boxview1 viewWithTag:2];
    label12.text = @"陕A*****";
    self.carNumberLabel = label12;
    
    UILabel *label13 = [boxview2 viewWithTag:1];
    label13.text = @"车龄(年)";
    UILabel *label14 = [boxview2 viewWithTag:2];
    label14.text = @"3";
    self.carAgeLabel = label14;
    
    
}

- (UIView *)createBoxView{
    UIView *view = [UIView new];
    UILabel *label1 = [Utils createLabelWithTitle:@"title" titleColor:HexColor(0x999999) fontSize:10];
    label1.tag = 1;
    UILabel *label2 = [Utils createLabelWithTitle:@"title2" titleColor:HexColor(0x323232) fontSize:14];
    label2.tag = 2;
    [view sd_addSubviews:@[label1,
                           label2]];
    label1.sd_layout
    .topSpaceToView(view, 4)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerXEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label2 bottomMargin:3];
    view.layer.borderColor = HexColor(0xE1E1E1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 4;
    
    return view;
}
@end
