//
//  CarAuthListCell.h
//  TravelNew
//
//  Created by mac on 2020/12/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomCollectionCell.h"
#import "CarListEntity.h"
#import "CarSetupListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarAuthListCell : BaseCustomTableViewCell
@property(nonatomic,strong) CarListEntity *model;
@property(nonatomic,strong) CarSetupListEntity *carSetupListModel;
@end

NS_ASSUME_NONNULL_END
