//
//  AddCarAuthCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AddCarAuthCtrl.h"
#import "CarAuthListCell.h"
#import "XingShiZhengCertificationCtrl.h"

@interface AddCarAuthCtrl ()
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) UILabel *carNumberLabel;
@end

@implementation AddCarAuthCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的车辆" titleColor:nil];
    self.tableView.backgroundColor = HexColor(0xF9F9F9);
    self.tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [self.tableView registerClass:[CarAuthListCell class] forCellReuseIdentifier:@"CarAuthListCell"];
    self.tableView.tableHeaderView = [self createHeaderView];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/price/list" para:@{} description:@"查询车辆报价列表"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [weakSelf.dataArray removeAllObjects];
            NSArray *data = [json objectForKey:@"data"];
            for (NSDictionary *dict in data) {
                CarListEntity *model = [[CarListEntity alloc] initWithJson:dict];
                [weakSelf.dataArray addObject:model];
            }
            [weakSelf.tableView reloadData];
            weakSelf.carNumberLabel.text = [NSString stringWithFormat:@"我的车辆（%ld）",weakSelf.dataArray.count];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (void)addCarAuth:(UITapGestureRecognizer *)reg
{
    XingShiZhengCertificationCtrl *vc = [XingShiZhengCertificationCtrl new];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarAuthListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarAuthListCell"];
    CarListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 152;
}

- (UIView *)createHeaderView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
    view.backgroundColor = HexColor(0xf9f9f9);
    UILabel *label = [Utils createBoldLabelWithTitle:@"我的车辆（0）" titleColor:HexColor(0x323232) fontSize:17];
    self.carNumberLabel = label;
    UILabel *label2 = [Utils createLabelWithTitle:@"接送机，包车自由行" titleColor:HexColor(0x999999) fontSize:12];
    [view sd_addSubviews:@[label,label2]];
    label.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .centerYEqualToView(label)
    .leftSpaceToView(label, 8)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}



@end
