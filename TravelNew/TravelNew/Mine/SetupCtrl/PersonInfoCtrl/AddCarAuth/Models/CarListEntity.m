//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "CarListEntity.h"

@implementation CarListEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.maximumPrice  = [json objectForKey:@"maximumPrice"];
		self.plate_no  = [json objectForKey:@"plate_no"];
		self.carAge  = [json objectForKey:@"carAge"];
		self.minimumPrice  = [json objectForKey:@"minimumPrice"];
		self.maximumMileage  = [json objectForKey:@"maximumMileage"];
		self.carId  = [json objectForKey:@"carId"];
		self.minimumMileage  = [json objectForKey:@"minimumMileage"];
		self.carModel  = [json objectForKey:@"carModel"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.maximumPrice forKey:@"zx_maximumPrice"];
	[aCoder encodeObject:self.plate_no forKey:@"zx_plate_no"];
	[aCoder encodeObject:self.carAge forKey:@"zx_carAge"];
	[aCoder encodeObject:self.minimumPrice forKey:@"zx_minimumPrice"];
	[aCoder encodeObject:self.maximumMileage forKey:@"zx_maximumMileage"];
	[aCoder encodeObject:self.carId forKey:@"zx_carId"];
	[aCoder encodeObject:self.minimumMileage forKey:@"zx_minimumMileage"];
	[aCoder encodeObject:self.carModel forKey:@"zx_carModel"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.maximumPrice = [aDecoder decodeObjectForKey:@"zx_maximumPrice"];
		self.plate_no = [aDecoder decodeObjectForKey:@"zx_plate_no"];
		self.carAge = [aDecoder decodeObjectForKey:@"zx_carAge"];
		self.minimumPrice = [aDecoder decodeObjectForKey:@"zx_minimumPrice"];
		self.maximumMileage = [aDecoder decodeObjectForKey:@"zx_maximumMileage"];
		self.carId = [aDecoder decodeObjectForKey:@"zx_carId"];
		self.minimumMileage = [aDecoder decodeObjectForKey:@"zx_minimumMileage"];
		self.carModel = [aDecoder decodeObjectForKey:@"zx_carModel"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"maximumPrice : %@\n",self.maximumPrice];
	result = [result stringByAppendingFormat:@"plate_no : %@\n",self.plate_no];
	result = [result stringByAppendingFormat:@"carAge : %@\n",self.carAge];
	result = [result stringByAppendingFormat:@"minimumPrice : %@\n",self.minimumPrice];
	result = [result stringByAppendingFormat:@"maximumMileage : %@\n",self.maximumMileage];
	result = [result stringByAppendingFormat:@"carId : %@\n",self.carId];
	result = [result stringByAppendingFormat:@"minimumMileage : %@\n",self.minimumMileage];
	result = [result stringByAppendingFormat:@"carModel : %@\n",self.carModel];
	
    return result;
}

@end
