//
//  AddRoomAuthCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AddRoomAuthCtrl.h"
#import "HotelInfoCell.h"

@interface AddRoomAuthCtrl ()
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) UILabel *roomNumberLabel;
@end

@implementation AddRoomAuthCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的房屋" titleColor:nil];
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    self.tableView.tableHeaderView = [self createHeaderView];
    [self.tableView registerClass:[HotelInfoCell class] forCellReuseIdentifier:@"HotelInfoCell"];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)loadData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/house/list"
                           para:@{}
                    description:@"住宿信息列表"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
            if([[json objectForKey:@"code"] integerValue]==200){
                if([json containsKey:@"data"]){
                    NSArray *data = [json objectForKey:@"data"];
                    [self.dataArray removeAllObjects];
                    for(NSDictionary *dict in data){
                        RoomSeupListEntity *model = [[RoomSeupListEntity alloc] initWithJson:dict];
                        [self.dataArray addObject:model];
                    }
                    [weakSelf.tableView reloadData];
                    weakSelf.roomNumberLabel.text = [NSString stringWithFormat:@"我的房屋（%ld）",weakSelf.dataArray.count];
                }
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
        
}


- (UIView *)createHeaderView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
    view.backgroundColor = HexColor(0xf9f9f9);
    UILabel *label = [Utils createBoldLabelWithTitle:@"我的房屋（0）" titleColor:HexColor(0x323232) fontSize:17];
    self.roomNumberLabel = label;
    UILabel *label2 = [Utils createLabelWithTitle:@"旅行安心住" titleColor:HexColor(0x999999) fontSize:12];
    [view sd_addSubviews:@[label,label2]];
    label.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .centerYEqualToView(label)
    .leftSpaceToView(label, 8)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (void)addRoomAuth:(UITapGestureRecognizer *)reg
{
    
}

- (void)addFloatButton{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    if(!self.btn){
        LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"添加房屋" font:LKSystemFont(17) cornerRadius:22];
        self.btn = btn;
        [btn addTapGestureTarget:self action:@selector(addRoomAuth:)];
        [keyWindow addSubview:btn];
        btn.sd_layout
        .leftSpaceToView(keyWindow, 20)
        .rightSpaceToView(keyWindow, 20)
        .bottomSpaceToView(keyWindow, 33)
        .heightIs(44);
    }else{
        [keyWindow addSubview:self.btn];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.btn removeFromSuperview];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RoomSeupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    HotelInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelInfoCell"];
    cell.model = model;
    cell.navigationBlock = ^(RoomSeupListEntity * _Nonnull model) {
        LKWeakSelf
        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
        [weakSelf doNavigationWithEndLocation:endLocation];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.dataArray.count;
}


@end
