//
//  RealNameInfoCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/12.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RealNameInfoCtrl.h"
#import "RealNameAuthEntity.h"


@interface RealNameInfoCtrl ()<UITextFieldDelegate>
@property(nonatomic,strong) UITextField *fieldIDCard;
@property(nonatomic,strong) UITextField *fieldName;
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) RealNameAuthEntity *model;
@end

@implementation RealNameInfoCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的实名认证" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF5F5F7);
    [self layout];
    [self getAuthInfo];
}

-(void)viewIsClicked:(UITapGestureRecognizer *)reg
{
    [self.fieldIDCard resignFirstResponder];
    [self.fieldName resignFirstResponder];
}

-(void)setModel:(RealNameAuthEntity *)model
{
    _model = model;
    self.fieldIDCard.text = model.idCard;
    self.fieldName.text = model.name;
}

- (void)getAuthInfo{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getReferrerUserRealNameAuthenticationInfo"
                          para:@{} description:@"获取推荐官实名认证信息信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            RealNameAuthEntity *model = [[RealNameAuthEntity alloc] initWithJson:data];
            self.model = model;
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (void)layout{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, 150)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view sd_addSubviews:@[view,]];

    UILabel *tipLabel1 = [self createLabel:@"姓名"];
    UITextField *nameField = [self createTextField:@"请输入真实姓名"];
    //nameField.clearButtonMode = UITextFieldViewModeAlways;
    nameField.enabled = NO;
    self.fieldName = nameField;
    
    UILabel *tipLabel2 = [self createLabel:@"证件号码"];
    UITextField *fieldIDCard = [self createTextField:@"请输入身份证号码"];
    //fieldIDCard.clearButtonMode = UITextFieldViewModeAlways;
    fieldIDCard.enabled = NO;
    fieldIDCard.keyboardType = UIKeyboardTypeNumberPad;
    self.fieldIDCard = fieldIDCard;

    UIView *line1 = [self createLine];
    [view sd_addSubviews:@[tipLabel1,nameField,
                           line1,
                           tipLabel2,fieldIDCard
                           ]];
    tipLabel1.sd_layout
    .topSpaceToView(view, 14)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [tipLabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    nameField.sd_layout
    .leftSpaceToView(view, 94)
    .rightSpaceToView(view, 20)
    .centerYEqualToView(tipLabel1)
    .heightIs(30);
    
    line1.sd_layout
    .topSpaceToView(tipLabel1, 14)
    .leftEqualToView(tipLabel1)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    tipLabel2.sd_layout
    .topSpaceToView(line1, 14)
    .leftEqualToView(tipLabel1)
    .autoHeightRatio(0);
    [tipLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    fieldIDCard.sd_layout
    .leftEqualToView(nameField)
    .rightSpaceToView(view, 20)
    .centerYEqualToView(tipLabel2)
    .heightIs(30);
    
    [view setupAutoHeightWithBottomView:fieldIDCard bottomMargin:14];
}


- (UILabel *)createLabel:(NSString *)str
{
    UILabel *label = [Utils createLabelWithTitle:str titleColor:HexColor(0x333333) fontSize:16];
    return label;
}

- (UITextField *)createTextField:(NSString *)placeHolder
{
    UITextField *field = [UITextField new];
    field.font = LKSystemFont(16);
    field.textColor = HexColor(0x333333);
    field.placeholder = placeHolder;
    return field;
}

- (UIView *)createLine
{
    UIView *view = [UIView new];
    view.backgroundColor = HexColor(0xefefef);
    return view;
}

@end
