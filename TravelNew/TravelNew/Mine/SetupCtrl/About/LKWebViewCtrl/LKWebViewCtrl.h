//
//  LKWebViewCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKWebViewCtrl : LKBaseViewController
@property(nonnull,strong) NSString *urlstr;
@end

NS_ASSUME_NONNULL_END
