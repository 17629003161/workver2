//
//  LKWebViewCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKWebViewCtrl.h"
#import <WebKit/WebKit.h>

@interface LKWebViewCtrl ()

@end

@implementation LKWebViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self simpleExampleTest];
}

- (void)simpleExampleTest {
    // 1.创建webview，并设置大小，"20"为状态栏高度
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    // 2.创建请求
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlstr]];
    // 3.加载网页
    [webView loadRequest:request];

    // 最后将webView添加到界面
    [self.view addSubview:webView];
}

@end
