//
//  ShowTextCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ShowTextCtrl.h"

@interface ShowTextCtrl ()
@property(nonatomic,strong) UIImageView *imgv;
@end

@implementation ShowTextCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:_tipTitle titleColor:nil];
    [self.view addSubview:self.scrollView];
    UIImage *image = [UIImage imageNamed:self.photoName];
    CGFloat scale =  (kScreenWidth-20)/image.size.width;
    CGFloat height = image.size.height * scale;
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, height+48);
    
    self.imgv = [[UIImageView alloc] initWithImage:image];
    [self.scrollView addSubview:self.imgv];
    self.scrollView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    self.imgv.sd_layout
    .topSpaceToView(self.scrollView, 24)
    .leftSpaceToView(self.scrollView, 10)
    .rightSpaceToView(self.scrollView, 10)
    .heightIs(height);
}




@end
