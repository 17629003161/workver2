//
//  ShowTextCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShowTextCtrl : LKBaseViewController
@property(nonatomic,strong) NSString *tipTitle;
@property(nonatomic,strong) NSString *photoName;
@end

NS_ASSUME_NONNULL_END
