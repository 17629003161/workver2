//
//  AboutCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AboutCtrl.h"
#import "SetupCell.h"
#import "ShowTextCtrl.h"
#import "LKWebViewCtrl.h"


@interface AboutCtrl ()

@end

@implementation AboutCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"关于旅咖出行" titleColor:nil];
    [self disabelPageRefresh];
    self.tableView.backgroundColor = HexColor(0xF9F9F9);
    [self.tableView registerClass:[SetupCell class] forCellReuseIdentifier:@"SetupCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.dataArray addObjectsFromArray:@[@"版本号",@"隐私政策",@"法律声明",@"用户协议",@"推荐官规则",@"免责声明"]];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 200)];
    view.backgroundColor = HexColor(0xF9F9F9);
    UIImageView *imgv = ImageViewWithImage(@"logo");
    UILabel *label = [Utils createLabelWithTitle:@"旅咖出行出行" titleColor:HexColor(0x333333) fontSize:18];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(view, 49)
    .widthIs(73)
    .heightIs(68);
    
    label.sd_layout
    .topSpaceToView(imgv, 13)
    .centerXEqualToView(imgv)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 200.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetupCell"];
    cell.leftLabel.text = self.dataArray[indexPath.row];
    if(indexPath.row==0){
        cell.rightLabel.text = @"1.0.0";
        cell.arrow.hidden = YES;
    }else{
        cell.rightLabel.text = @"";
        cell.arrow.hidden = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:{
            LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
            [vc setTitle:@"隐私政策" titleColor:nil];
            vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecyyinsi.html";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{
            LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
            [vc setTitle:@"法律声明" titleColor:nil];
            vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecyfalv.html";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:{
            LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
            [vc setTitle:@"用户协议" titleColor:nil];
            vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecyxieyi.html";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 4:{
            LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
            [vc setTitle:@"推荐官规则" titleColor:nil];
            vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecytuijian.html";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 5:{
            LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
            [vc setTitle:@"免责声明" titleColor:nil];
            vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecymianze.html";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }

}

@end
