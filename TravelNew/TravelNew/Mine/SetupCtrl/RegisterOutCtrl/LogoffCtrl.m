//
//  LogoffCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LogoffCtrl.h"
#import "LKInputDialog.h"

@interface LogoffCtrl ()

@end

@implementation LogoffCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"注销推荐官" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [self layout];
}

- (void)layout{
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"亲爱的旅咖出行用户：" titleColor:HexColor(0x323232) fontSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"感谢您长期以来对本平台的支持，在完成账户注销前，请仔细阅读以下内容" titleColor:HexColor(0x323232) fontSize:16];
    label2.numberOfLines = 0;
    UIView *dotView1 = [self createDotView:@"选择注销账户后，您账户内的所有订单、卡券、其他优惠及个人数据将无法恢复"];
    UIView *dotView2 = [self createDotView:@"如推荐官注销，您的所有认证信息及个人数据将清空且无法恢复"];
    UIView *dotView3 = [self createDotView:@"因数据清空有一定得时效性，点击注销后服务器预计将在7个工作日内清空您的数据，返还您的余额及服务保障金。"];
    UIView *dotView4 = [self createDotView:@"系统将审查您的账户信息，确认没有纠纷后，立即给您退款。所有退款将扣除您1%的手续费。"];
    UIView *btnView = [self createButtonView];
    [self.view sd_addSubviews:@[label1,
                                label2,
                                dotView1,
                                dotView2,
                                dotView3,
                                dotView4,
                                btnView]];
    label1.sd_layout
    .topSpaceToView(self.view, 20)
    .leftSpaceToView(self.view, 20)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 12)
    .leftEqualToView(label1)
    .rightSpaceToView(self.view, 36)
    .autoHeightRatio(0);
    
    dotView1.sd_layout
    .topSpaceToView(label2, 18)
    .leftEqualToView(label2)
    .rightSpaceToView(self.view, 17);
    
    dotView2.sd_layout
    .topSpaceToView(dotView1, 12)
    .leftEqualToView(label2)
    .rightSpaceToView(self.view, 17);
    
    dotView3.sd_layout
    .topSpaceToView(dotView2, 12)
    .leftEqualToView(label2)
    .rightSpaceToView(self.view, 17);
    
    dotView4.sd_layout
    .topSpaceToView(dotView3, 12)
    .leftEqualToView(label2)
    .rightSpaceToView(self.view, 17);
    
    btnView.sd_layout
    .bottomSpaceToView(self.view, 48)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(44);
}

- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"administration/cancellationGetValidate"
                          para:@{}
                   description:@"发送推荐官账户注销验证码"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString ;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            [weakSelf showInputDialog];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@,%@",request.error,request.responseString);
    }];
}

- (void)showInputDialog{
    LKInputDialog *dlg = [[LKInputDialog alloc] initWithFrame:CGRectMake(0, 0, 270, 189)];
    [dlg setTitle:@"输入验证码" subTitle:@"请输入手机验证码" placeHolder:@"输入手机验证码"];
    dlg.okBlock = ^(NSString * _Nonnull phone) {
        [self doDestoyAccount:phone];
    };
    [dlg show];
}


- (void)doDestoyAccount:(NSString *)phone{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"administration/cancellation"
                          para:@{@"verifyCode":phone}
                   description:@"推荐官账户注销"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString ;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:json[@"msg"]];
            UserInfo *user = [UserInfo shareInstance];
            if([user isRecomander]){
                [user switchCurrentStyle];
            }
            user.userStyle = @(UserStyleCustom);
            user.currentUserStyle = @(UserStyleCustom);
            [user save];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
        }];
}

- (UIView *)createButtonView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth-56, 44)];
    view.layer.cornerRadius = 22;
    view.layer.masksToBounds = YES;
    [view gradientFromColor:HexColor(0xD8E4FF) toColor:HexColor(0xCCE3FF) bounds:view.bounds];
    UILabel *label = [Utils createLabelWithTitle:@"发送验证码" titleColor:HexColor(0x4D88FD) fontSize:17];
    [label addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [view addSubview:label];
    label.sd_layout
    .centerXEqualToView(view)
    .centerYEqualToView(view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (UIView *)createDotView:(NSString *)title
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *dot = ImageViewWithImage(@"log_out_dot");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:15];
    label.numberOfLines = 0;
    [view sd_addSubviews:@[dot,label]];
    dot.sd_layout
    .topSpaceToView(view, 9)
    .leftSpaceToView(view, 20)
    .widthIs(7)
    .heightIs(7);
    label.sd_layout
    .leftSpaceToView(dot, 11)
    .topEqualToView(view)
    .rightSpaceToView(view, 17)
    .autoHeightRatio(0);
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}

@end
