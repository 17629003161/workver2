//
//  PrivateSetupCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PrivateSetupCtrl.h"

@interface PrivateSetupCtrl ()
@property(nonatomic,assign) BOOL isOn;
@property(nonatomic,strong) UIButton *btn;
@end

@implementation PrivateSetupCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF9F9F9);
    [self setTitle:@"隐私设置" titleColor:nil];
    [self layout];
    self.isOn = YES;
}

- (void)layout{
    UIView *view = [self createTopView];
    [self.view addSubview:view];
    view.sd_layout
    .topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(60);
}

- (void)setIsOn:(BOOL)isOn
{
    _isOn = isOn;
    _btn.selected = isOn;
}

- (void)btnIsClicked:(id)sender
{
    self.isOn = !_isOn;
}

- (UIView *)createTopView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"允许他人查看我的足迹" titleColor:HexColor(0x323232) fontSize:16];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *onImage = [UIImage imageNamed:@"switch_on"];
    // 四个数值对应图片中距离上、左、下、右边界的不拉伸部分的范围宽度
    onImage = [onImage resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2) resizingMode:UIImageResizingModeStretch];
    UIImage *offImage = [UIImage imageNamed:@"switch_off"];
    offImage = [offImage resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2) resizingMode:UIImageResizingModeStretch];
    
    [btn setImage:onImage forState:UIControlStateNormal];
    [btn setImage:offImage forState:UIControlStateSelected];
    self.btn = btn;
    [btn addTarget:self action:@selector(btnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view sd_addSubviews:@[label,btn]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 16)
    .widthIs(44)
    .heightIs(36);
    return view;
}


@end
