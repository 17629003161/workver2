//
//  PersonInfoCustomCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PersonInfoCustomCtrl.h"
#import "SetupCell.h"
#import "SetupSelectCityDlg.h"
#import "SetupSelectSexDialog.h"
#import "SetupSelectDateDialog.h"

@interface PersonInfoCustomCtrl ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong) NSString *sexStr;
@property(nonatomic,strong) NSString *ageStr;
@property(nonatomic,strong) NSString *cityStr;

@property(nonatomic,strong) UIImageView *photoImgv;
@property(nonatomic,strong) UITextField *nickNameField;

@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSString *imageFilePath;
@property(nonatomic,strong) LKMessageDialog *dlg;
@property(nonatomic,assign) BOOL isModify;


@end

@implementation PersonInfoCustomCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"个人资料" titleColor:nil];
    //self.view.backgroundColor = HexColor(0xF9F9F9);
    [self.dataArray addObjectsFromArray:@[@[@"性别",@"选择你的性别"],@[@"年龄",@"选择你的年龄"],@[@"所在城市",@"选择你的所在城市"]]];
    [self.tableView registerClass:[SetupCell class] forCellReuseIdentifier:@"SetupCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.isModify = NO;
    [self disabelPageRefresh];
    [self addRightBtn];
    [self layout];
    [self loadUserData];
    [self customBackButton];
}

// 自定义返回按钮
- (void)customBackButton{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"left_arrow_black"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = item;
}
// 返回按钮按下
- (void)backBtnClicked:(UIButton *)sender{
   LKWeakSelf
    if(!self.isModify){
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
   if(!_dlg){
       _dlg = [LKMessageDialog createMessageDialogWithTitle:@"你确保存？"
                                                                     content:@"您确认要保存所做的修改吗？"
                                                                leftBtnTitle:@"取消"
                                                               rightBtnTitle:@"确认"];
       _dlg.frame = CGRectMake(0, 0, 270, 160);
       _dlg.okBlock = ^{
            [weakSelf onClickedSavebtn];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
       _dlg.cancleBlock = ^{
           [weakSelf.navigationController popViewControllerAnimated:YES];
       };
    }
    [self.dlg show];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.dlg hide];
}

- (void)loadUserData{
    UserInfo *user = [UserInfo shareInstance];
    if(!isNull(user.address)){
        self.cityStr = user.address;
    }
    if(!isNull(user.sex)){
        self.sexStr = [Utils sexToString:user.sex];
    }
    if(!isNull(user.birthTime)){
        LKDateUtils *date = [LKDateUtils new];
        date.timeInterval = [user.birthTime doubleValue]/1000;
        self.ageStr = [date dateString];
    }
    if(!isNull(user.userNickname)){
        self.nickNameField.text = user.userNickname;
    }
    if(!isNull(user.headerImage) && user.headerImage.length>0){
        [self.photoImgv sd_setImageWithURL:[NSURL URLWithString:user.headerImage] placeholderImage:[UIImage imageNamed: @"default_header"]];
    }
}

- (void)addRightBtn {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    btn.titleLabel.font = LKSystemFont(14);
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(onClickedSavebtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
}
  
- (void)onClickedSavebtn {
    if(isNull(self.ageStr)){
        [MBProgressHUD showMessage:@"请选择生日"];
        return;
    }
    if(isNull(self.cityStr)){
        [MBProgressHUD showMessage:@"请选择城市"];
        return;
    }
    
    NSString *dateStr = [NSString stringWithFormat:@"%@ %@",self.ageStr,@"00:00:00"];
    LKDateUtils *date = [LKDateUtils new];
    date.dateTimeString = dateStr;
    NSTimeInterval interval = date.timeInterval *1000;
    NSNumber *birthDay = [NSNumber numberWithDouble:interval];
    NSNumber *sex = [Utils sexStringToSex:self.sexStr];
    NSMutableDictionary *para = [NSMutableDictionary new];
    [para setObject:birthDay forKey:@"birthTime"];
    [para setObject:self.cityStr forKey:@"address"];
    [para setObject:sex forKey:@"sex"];
    if(self.imageFilePath){
        [para setObject:self.imageFilePath forKey:@"headerImage"];
    }
    if(self.nickNameField.text.length >0){
        [para setObject:self.nickNameField.text forKey:@"userNickname"];
    }
    Api *api = [Api apiPostUrl:@"user/changeUserInfo"
                          para:para
                   description:@"user-修改用户信息"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *json = request.responseObject;
            if([[json objectForKey:@"code"] integerValue]==200){
                UserInfo *user = [UserInfo shareInstance];
                user.address = self.cityStr;
                user.birthTime = birthDay;
                user.sex = sex;
                if(self.imageFilePath){
                    user.headerImage = self.imageFilePath;
                }
                if(self.nickNameField.text.length>0){
                    user.userNickname = self.nickNameField.text;
                }
                [user save];
                [MBProgressHUD showMessage:@"保存成功"];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }else{
                [MBProgressHUD showMessage:@"操作失败"];
            }
        }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SetupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetupCell"];
    NSArray *ary = [self.dataArray objectAtIndex:indexPath.row];
    cell.leftLabel.text = ary[0];
    cell.rightLabel.text = ary[1];
    if(indexPath.row==0){
        if(self.sexStr)
            cell.rightLabel.text = self.sexStr;
    }else if(indexPath.row==1){
        if(self.ageStr)
            cell.rightLabel.text = self.ageStr;
    }else if(indexPath.row==2){
        if(self.cityStr)
            cell.rightLabel.text = self.cityStr;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    UserInfo *user = [UserInfo shareInstance];
    [self.nickNameField resignFirstResponder];
    switch (indexPath.row) {
        case 0:{
            if ([user isRecomander]) {
                [MBProgressHUD showMessage:@"您已是推荐官，不能修改性别"];
                return;
            }
            SetupSelectSexDialog *dlg = [SetupSelectSexDialog new];
            dlg.sex = weakSelf.sexStr;
            dlg.selectSexBlock = ^(NSString * _Nonnull sex) {
                weakSelf.sexStr = sex;
                [weakSelf.tableView reloadData];
                self.isModify = YES;
            };
            [dlg show];
        }
            break;
        case 1:{
            if ([user isRecomander]) {
                if ([user isRecomander]) {
                    [MBProgressHUD showMessage:@"您已是推荐官，不能修改年龄"];
                    return;
                }
            }
            SetupSelectDateDialog *dlg = [SetupSelectDateDialog new];
            dlg.selectDateBlock = ^(NSString * _Nonnull dateStr) {
                self.ageStr = dateStr;
                [weakSelf.tableView reloadData];
                self.isModify = YES;
            };
            [dlg show];
        }
            break;
        case 2:{
            SetupSelectCityDlg *dlg = [SetupSelectCityDlg new];
            dlg.selectCityBlock = ^(NSString * _Nonnull provence,NSString * _Nonnull city) {
                weakSelf.cityStr = city;
                [weakSelf.tableView reloadData];
                self.isModify = YES;
            };
            [dlg show];
        }
            break;
            
        default:
            break;
    }
}

- (void)penIsClicked:(UITapGestureRecognizer *)reg
{
    static int i = 0;
    i = (i+1) % 2;
    if(i==1){
        [self.nickNameField setUserInteractionEnabled:YES];
        [self.nickNameField becomeFirstResponder];
    }else{
        [self.nickNameField resignFirstResponder];
        [self.nickNameField setUserInteractionEnabled:NO];
    }
    self.isModify = YES;
}

- (void)layout{
    UIImageView *imagv = ImageViewWithImage(@"header_person");
    imagv.layer.cornerRadius = 37;
    imagv.layer.masksToBounds = YES;
    self.photoImgv = imagv;
    [imagv addTapGestureTarget:self action:@selector(chooseImage)];
    UIImageView *cameraimgv = ImageViewWithImage(@"camera_icon");
    cameraimgv.layer.cornerRadius = 10;
    cameraimgv.layer.masksToBounds = YES;
    [imagv addSubview:cameraimgv];
    UITextField *nameField = [UITextField new];
    nameField.textColor = HexColor(0x323232);
    nameField.clearButtonMode = UITextFieldViewModeAlways;
    nameField.font = LKSystemFont(20);
    nameField.text = @"蚂蚁一直在";
    [nameField setUserInteractionEnabled:NO];
    self.nickNameField = nameField;
    
    UIImageView *pen = ImageViewWithImage(@"pen_icon");
    [pen addTapGestureTarget:self action:@selector(penIsClicked:)];
    
    UIView *titleView = [self createTitleView:@"个人信息"
                                      subTitle:@"完善个人信息更有优势哦～"];

    
    [self.view sd_addSubviews:@[imagv,nameField,pen,titleView]];
    
    cameraimgv.sd_layout
    .rightSpaceToView(imagv, 3)
    .bottomSpaceToView(imagv, 9)
    .widthIs(20)
    .heightIs(20);
    
    imagv.sd_layout
    .topSpaceToView(self.view, 20)
    .leftSpaceToView(self.view, 18)
    .widthIs(74)
    .heightIs(74);
    
    nameField.sd_layout
    .centerYEqualToView(imagv)
    .leftSpaceToView(imagv, 23)
    .widthIs(180)
    .heightIs(24);
    
    pen.sd_layout
    .leftSpaceToView(nameField, 10)
    .centerYEqualToView(nameField)
    .widthIs(20)
    .heightIs(20);
    
    titleView.sd_layout
    .topSpaceToView(imagv, 16)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    self.tableView.sd_layout
    .topSpaceToView(titleView, 0)
    .leftSpaceToView(self.view, 25)
    .rightSpaceToView(self.view, 25)
    .bottomEqualToView(self.view);
}

- (UIView *)createTitleView:(NSString *)title subTitle:(NSString *)subTitle
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:title titleColor:HexColor(0x323232) fontSize:17];
    UILabel *label2 = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    [view sd_addSubviews:@[label1,label2]];
    
    label1.sd_layout
    .topSpaceToView(view, 16)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label2 bottomMargin:16];
    return view;
}

-(void)setImage:(UIImage *)image
{
    _image = image;
}

-(void)setImageFilePath:(NSString *)imageFilePath
{
    _imageFilePath = imageFilePath;
}

-(void)chooseImage {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//获取选择的图片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
   // [self cosUploadImgae:image];
    UIImage *littleImage = [Utils compressImageSize:image toByte:8*1024];
    DLog(@"imagesize : %lf    %lf",littleImage.size.width,  littleImage.size.height);
    DLog(@"screen: %lf, %lf",kScreenWidth,kScreenHeight);
    [self cosUploadImgae:littleImage];
}

//从相机或者相册界面弹出
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)cosUploadImgae:(UIImage *)image{
    LKWeakSelf
    NSString *remoteDir = @"lvkauxing/img/";
    NSString* tempPath = QCloudTempFilePathWithExtension(@"png");
    QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
    // 存储桶名称，格式为 BucketName-APPID
    put.bucket = @"nlcx-1301827761";
    // 对象键，是对象在 COS 上的完整路径，如果带目录的话，格式为 "dir1/object1"
    put.object = [remoteDir stringByAppendingPathComponent:tempPath];
    //需要上传的对象内容。可以传入NSData*或者NSURL*类型的变量
    NSData *data = UIImagePNGRepresentation(image);
    put.body =  data;
    //监听上传进度
    [put setSendProcessBlock:^(int64_t bytesSent,
                                int64_t totalBytesSent,
                                int64_t totalBytesExpectedToSend) {
        //      bytesSent                   新增字节数
        //      totalBytesSent              本次上传的总字节数
        //      totalBytesExpectedToSend    本地上传的目标字节数
    }];

    //监听上传结果
    [put setFinishBlock:^(id outputObject, NSError *error) {
        //可以从 outputObject 中获取 response 中 etag 或者自定义头部等信息
        QCloudUploadObjectResult *result = (QCloudUploadObjectResult *)outputObject;
       // NSDictionary * result = (NSDictionary *)outputObject;
        DLog(@"file location = %@",result.location);
        weakSelf.imageFilePath = result.location;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.photoImgv sd_setImageWithURL:[NSURL URLWithString:weakSelf.imageFilePath]];
        });
    }];

    [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
}



@end
