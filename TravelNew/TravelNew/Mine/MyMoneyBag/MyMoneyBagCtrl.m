//
//  MyMoneyBagCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyMoneyBagCtrl.h"
#import "MyMoneyBagCell.h"
#import "WithdrawalCtrl.h"
#import "SecurityFundCtrl.h"
#import "YTKBatchRequest.h"
#import "SecurityFundEntity.h"
#import "LKTipDialog.h"

@interface MyMoneyBagCtrl ()
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,strong) UILabel *ganeLabel;
@property(nonatomic,assign) double balanceOfsecuritFund;
@property(nonatomic,strong) UILabel *securitFundLabel;

@end

@implementation MyMoneyBagCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的钱包" titleColor:nil];
    UIView *topView = [self createHeaderView];
    [self.view addSubview:topView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[MyMoneyBagCell class] forCellReuseIdentifier:@"cellid"];

    topView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(239);
    
    self.tableView.sd_layout
    .topSpaceToView(self.view, 239)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self sendBatchRequest];
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/record/selectLtWalletRecordList"
                          para:@{ @"pageSize":@(self.pageSize),
                                  @"pageIndex":@(self.pageIndex)}
                   description:@"work-查询钱包记录"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(MoneyBagEntity.class);
    return name;
}


-(void)setModel:(WithdrawalEntity *)model
{
    _model = model;
    double money = [model.walletNumber doubleValue]/100.0;
    self.moneyLabel.text = [Utils formatMoney:money];
    double tmp = [model.accruingAmounts doubleValue]/100.0;
    self.ganeLabel.text = [Utils formatMoney:tmp];
}

- (void)setBalanceOfsecuritFund:(double)balanceOfsecuritFund
{
    _balanceOfsecuritFund = balanceOfsecuritFund;
    self.securitFundLabel.text = [NSString stringWithFormat:@"%@元",[Utils formatMoney:balanceOfsecuritFund]];
}

- (void)sendBatchRequest {
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"work/wallet/selectWalletByUserId"
                          para:@{}
                   description:@"查询钱包余额"];
    Api *api2 = [Api apiPostUrl:@"work/deposit/selectCashDeposit"
                          para:@{}
                   description:@"查看保证金余额"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        NSString *jsonstr = request1.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            WithdrawalEntity *model = [[WithdrawalEntity alloc] initWithJson:dict];
            weakSelf.model = model;
            [weakSelf loadFirstPage];
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        NSString *jsonstr2 = request2.responseString;
        DLog(@"jsonstr = %@",jsonstr2);
        NSDictionary *json2 = [NSString jsonStringToDictionary:jsonstr2];
        if([[json2 objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json2 objectForKey:@"data"];
            SecurityFundEntity *model = [[SecurityFundEntity alloc] initWithJson:data];
            weakSelf.balanceOfsecuritFund = [model.balancec doubleValue]/100.0;
        }
    } failure:^(YTKBatchRequest *batchRequest) {
        DLog(@"failed");
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 79;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    view.backgroundColor = HexColor(0xf5f5f5);
    UILabel *label = [Utils createLabelWithTitle:@"收益明细" titleColor:HexColor(0x444444) fontSize:14];
    label.font = LKSystemFont(14);
    [view addSubview:label];
    label.sd_layout
    .topSpaceToView(view, 10)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyMoneyBagCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    MoneyBagEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

#pragma mark --提现
- (void)withDrawalIsClicked:(id)sender{
    LKWeakSelf
    NSDate *date = [NSDate date];
    LKDateUtils *utils = [[LKDateUtils alloc] init];
    utils.date = date;
 //   DLog(@"%@",utils.weekday);
    if([utils.weekday isEqualToString:@"周二"]){
        WithdrawalCtrl *vc = [[WithdrawalCtrl alloc] init];
        vc.refreshBlock = ^{
            [weakSelf sendBatchRequest];
        };
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        LKTipDialog *dlg = [[LKTipDialog alloc] initWithFrame:CGRectMake(0, 0, 270, 181)];
        dlg.message = @"提现日为每周周二";
        [dlg show];
        return;
    }
}

- (void)securityFundIsClicked:(id)sender
{
    SecurityFundCtrl *vc = [SecurityFundCtrl new];
    vc.money = self.balanceOfsecuritFund;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)createHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 239)];
    UIImageView *bgImgv = ImageViewWithImage(@"money_bg");
    [bgImgv setUserInteractionEnabled:YES];
    UILabel *label = [Utils createLabelWithTitle:@"资产(元)" titleColor:[UIColor whiteColor] fontSize:15];
    UILabel *label2 = [Utils createLabelWithTitle:@"(元)" titleColor:[UIColor whiteColor] fontSize:13];
    label2.backgroundColor = [UIColor clearColor];
    label2.alpha = 0.6;
    UILabel *label3 = [Utils createLabelWithTitle:@"累计收益" titleColor:[UIColor whiteColor] fontSize:13];
    label3.backgroundColor = [UIColor clearColor];
    label3.alpha = 0.6;
    UILabel *totoMoneyL = [Utils createLabelWithTitle:@"0.00" titleColor:HexColor(0xffffff) fontSize:33];
    self.moneyLabel = totoMoneyL;
    UILabel *addedMoneyL = [Utils createLabelWithTitle:@"+0.00" titleColor:HexColor(0xffffff) fontSize:16];
    self.ganeLabel = addedMoneyL;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"去提现" forState:UIControlStateNormal];
    btn.titleLabel.font = LKSystemFont(14);
    [btn setTitleColor:HexColor(0x6092FC) forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 4;
    //此行代码必须有（UIView例外）
    btn.layer.masksToBounds = YES;
    [btn addTarget:self action:@selector(withDrawalIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [bgImgv sd_addSubviews:@[label,label2,
                         totoMoneyL,
                         label3,
                         addedMoneyL,btn]];

    UILabel *leftL = [Utils createLabelWithTitle:@"我的保障金" titleColor:HexColor(0x444444) fontSize:17];
    UILabel *rightL = [Utils createLabelWithTitle:@"0.00元" titleColor:HexColor(0x333333) fontSize:18];
    self.securitFundLabel = rightL;
    [rightL addTapGestureTarget:self action:@selector(securityFundIsClicked:)];
    [rightL changeStrings:@[@"元"] toSize:14];
    UIImageView *arrow = ImageViewWithImage(@"arrow_right");
    [view sd_addSubviews:@[bgImgv,
                                leftL,rightL,arrow]];
    bgImgv.sd_layout
    .leftSpaceToView(view, 15)
    .topSpaceToView(view, 19)
    .rightSpaceToView(view, 15)
    .heightIs(164);
    
    label.sd_layout
    .leftSpaceToView(bgImgv, 26)
    .topSpaceToView(bgImgv, 17)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    label2.sd_layout
    .leftSpaceToView(label, 1)
    .centerYEqualToView(label)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    

    totoMoneyL.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(label, 6)
    .autoHeightRatio(0);
    [totoMoneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    label3.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(totoMoneyL, 15)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    addedMoneyL.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(label3, 2)
    .autoHeightRatio(0);
    [addedMoneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    btn.sd_layout
    .topSpaceToView(bgImgv, 108)
    .rightSpaceToView(bgImgv, 20)
    .widthIs(70)
    .heightIs(28);

    leftL.sd_layout
    .topSpaceToView(bgImgv, 16)
    .leftEqualToView(bgImgv)
    .autoHeightRatio(0);
    [leftL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    rightL.sd_layout
    .centerYEqualToView(leftL)
    .rightSpaceToView(view, 31)
    .autoHeightRatio(0);
    [rightL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(rightL)
    .rightSpaceToView(view, 16)
    .widthIs(7)
    .heightIs(12);
    
    return view;
    
}




@end
