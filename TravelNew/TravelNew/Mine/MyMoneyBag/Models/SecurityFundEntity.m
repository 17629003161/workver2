//
//
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "SecurityFundEntity.h"

@implementation SecurityFundEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.total  = [json objectForKey:@"total"];
        self.payable  = [json objectForKey:@"payable"];
        self.balancec  = [json objectForKey:@"balancec"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.total forKey:@"zx_total"];
    [aCoder encodeObject:self.payable forKey:@"zx_payable"];
    [aCoder encodeObject:self.balancec forKey:@"zx_balancec"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.total = [aDecoder decodeObjectForKey:@"zx_total"];
        self.payable = [aDecoder decodeObjectForKey:@"zx_payable"];
        self.balancec = [aDecoder decodeObjectForKey:@"zx_balancec"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"total : %@\n",self.total];
    result = [result stringByAppendingFormat:@"payable : %@\n",self.payable];
    result = [result stringByAppendingFormat:@"balancec : %@\n",self.balancec];
    
    return result;
}

@end
