//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface MoneyBagEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *remarks;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,strong) NSNumber *recordId;
@property (nonatomic,strong) NSNumber *intercourseCapital;
@property (nonatomic,strong) NSNumber *createTime;
@property (nonatomic,strong) NSNumber *state;
 


-(id)initWithJson:(NSDictionary *)json;

@end
