//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface SecurityFundEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *total;
@property (nonatomic,strong) NSNumber *payable;
@property (nonatomic,strong) NSNumber *balancec;
 


-(id)initWithJson:(NSDictionary *)json;

@end
