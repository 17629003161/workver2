//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "MoneyBagEntity.h"

@implementation MoneyBagEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
		self.remarks  = [json objectForKey:@"remarks"];
		self.nickname  = [json objectForKey:@"nickname"];
		self.recordId  = [json objectForKey:@"recordId"];
		self.intercourseCapital  = [json objectForKey:@"intercourseCapital"];
		self.createTime  = [json objectForKey:@"createTime"];
		self.state  = [json objectForKey:@"state"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.remarks forKey:@"zx_remarks"];
	[aCoder encodeObject:self.nickname forKey:@"zx_nickname"];
	[aCoder encodeObject:self.recordId forKey:@"zx_recordId"];
	[aCoder encodeObject:self.intercourseCapital forKey:@"zx_intercourseCapital"];
	[aCoder encodeObject:self.createTime forKey:@"zx_createTime"];
	[aCoder encodeObject:self.state forKey:@"zx_state"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.remarks = [aDecoder decodeObjectForKey:@"zx_remarks"];
		self.nickname = [aDecoder decodeObjectForKey:@"zx_nickname"];
		self.recordId = [aDecoder decodeObjectForKey:@"zx_recordId"];
		self.intercourseCapital = [aDecoder decodeObjectForKey:@"zx_intercourseCapital"];
		self.createTime = [aDecoder decodeObjectForKey:@"zx_createTime"];
		self.state = [aDecoder decodeObjectForKey:@"zx_state"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"remarks : %@\n",self.remarks];
	result = [result stringByAppendingFormat:@"nickname : %@\n",self.nickname];
	result = [result stringByAppendingFormat:@"recordId : %@\n",self.recordId];
	result = [result stringByAppendingFormat:@"intercourseCapital : %@\n",self.intercourseCapital];
	result = [result stringByAppendingFormat:@"createTime : %@\n",self.createTime];
	result = [result stringByAppendingFormat:@"state : %@\n",self.state];
	
    return result;
}

@end
