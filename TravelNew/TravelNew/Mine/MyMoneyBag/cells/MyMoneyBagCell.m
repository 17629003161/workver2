//
//  MyMoneyBagCell.m
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyMoneyBagCell.h"

@interface MyMoneyBagCell(){
}
@property(nonatomic,strong) UILabel *typeLabel;
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *dateTimeLabel;


@end

@implementation MyMoneyBagCell

-(void)setModel:(MoneyBagEntity *)model
{
    _model = model;
    self.typeLabel.text = model.remarks;
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2lf",[model.intercourseCapital doubleValue]/100.0];
    self.nameLabel.text = model.nickname;
    LKDateUtils *data = [[LKDateUtils alloc] init];
    data.timeInterval = [model.createTime doubleValue]/1000.0;
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@",data.dateTimeString];
    [self.typeLabel updateLayout];
    [self.moneyLabel updateLayout];
    [self.nameLabel updateLayout];
    [self.dateTimeLabel updateLayout];
}

- (void)configViews{
    UILabel *label1 = [Utils createLabelWithTitle:@"转账" titleColor:HexColor(0x444444) fontSize:17];
    self.typeLabel = label1;
    UILabel *label2 = [Utils createLabelWithTitle:@"-1000" titleColor:HexColor(0x4CC119) fontSize:17];
    self.moneyLabel = label2;
    UILabel *label3 = [Utils createLabelWithTitle:@"林亚都 "titleColor:HexColor(0xb3b3b3) fontSize:14];
    self.nameLabel = label3;
    UILabel *label4 = [Utils createLabelWithTitle:@"昨天16:05" titleColor:HexColor(0xb3b3b3) fontSize:14];
    self.dateTimeLabel = label4;
    [self.contentView sd_addSubviews:@[label1,label2,
                                       label3,label4]];
    label1.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(kScreenWidth/2.0)
    .heightIs(20);
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .rightSpaceToView(self.contentView, 13)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .leftSpaceToView(self.contentView, 15)
    .bottomSpaceToView(self.contentView, 15)
    .heightIs(20);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label4.sd_layout
    .bottomSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 13)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    [self setupAutoHeightWithBottomViewsArray:@[label3,label4] bottomMargin:15];
    
}

@end
