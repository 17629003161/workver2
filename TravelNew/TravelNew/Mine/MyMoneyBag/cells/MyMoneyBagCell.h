//
//  MyMoneyBagCell.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyBagEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMoneyBagCell : BaseCustomTableViewCell
@property(nonatomic,strong) MoneyBagEntity *model;
@end

NS_ASSUME_NONNULL_END
