//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "WithdrawalEntity.h"

@implementation WithdrawalEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.accruingAmounts  = [json objectForKey:@"accruingAmounts"];
		self.walletId  = [json objectForKey:@"walletId"];
		self.userId  = [json objectForKey:@"userId"];
		self.walletNumber  = [json objectForKey:@"walletNumber"];
		self.withdrawalAmounts  = [json objectForKey:@"withdrawalAmounts"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.accruingAmounts forKey:@"zx_accruingAmounts"];
	[aCoder encodeObject:self.walletId forKey:@"zx_walletId"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.walletNumber forKey:@"zx_walletNumber"];
	[aCoder encodeObject:self.withdrawalAmounts forKey:@"zx_withdrawalAmounts"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.accruingAmounts = [aDecoder decodeObjectForKey:@"zx_accruingAmounts"];
		self.walletId = [aDecoder decodeObjectForKey:@"zx_walletId"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.walletNumber = [aDecoder decodeObjectForKey:@"zx_walletNumber"];
		self.withdrawalAmounts = [aDecoder decodeObjectForKey:@"zx_withdrawalAmounts"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"accruingAmounts : %@\n",self.accruingAmounts];
	result = [result stringByAppendingFormat:@"walletId : %@\n",self.walletId];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"walletNumber : %@\n",self.walletNumber];
	result = [result stringByAppendingFormat:@"withdrawalAmounts : %@\n",self.withdrawalAmounts];
	
    return result;
}

@end
