//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface WithdrawalEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *accruingAmounts;
@property (nonatomic,strong) NSNumber *walletId;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSNumber *walletNumber;
@property (nonatomic,strong) NSNumber *withdrawalAmounts;
 


-(id)initWithJson:(NSDictionary *)json;

@end
