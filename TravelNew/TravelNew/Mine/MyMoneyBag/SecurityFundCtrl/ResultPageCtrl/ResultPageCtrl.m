
//
//  WithdrawalResultCtrl.m
//  TravelNew
//
//  Created by mac on 2021/1/11.
//  Copyright © 2021 lester. All rights reserved.
//

#import "WithdrawalResultCtrl.h"
#import "ResultPageCtrl.h"

@interface ResultPageCtrl ()
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UILabel *subLabel;

@end

@implementation ResultPageCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self viewLayout];
    if(self.titleText){
        [self setTitle:self.titleText titleColor:nil];
    }
    if(self.text){
        self.label.text = self.text;
        [self.label updateLayout];
    }
    if(self.subText){
        self.subLabel.text = self.subText;
        [self.subLabel updateLayout];
    }
}


- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewLayout{
    UIImageView *imgv = ImageViewWithImage(@"right_mark");
    UILabel *label = [Utils createBoldLabelWithTitle:@"提现申请已提交" titleColor:HexColor(0x323232) fontSize:28];
    self.label = label;
    UILabel *sublabel = [Utils createLabelWithTitle:@"申请已提交，预计24小时内到账" titleColor:HexColor(0x666666) fontSize:15];
    self.subLabel = sublabel;
    LKGadentButton *btn = [LKGadentButton new];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    btn.titleLabel.text = @"完成";
    btn.layer.cornerRadius = 22;
    btn.layer.masksToBounds = YES;
    [self.view sd_addSubviews:@[imgv,
                                label,
                                sublabel,
                                btn]];
    imgv.sd_layout
    .topSpaceToView(self.view, 56)
    .centerXEqualToView(self.view)
    .widthIs(80)
    .heightIs(80);
    
    label.sd_layout
    .topSpaceToView(imgv, 30)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    sublabel.sd_layout
    .topSpaceToView(label, 9)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [sublabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(sublabel, 95)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 20)
    .heightIs(44);
}

@end
