//
//  ResultPageCtrl.h
//  TravelNew
//
//  Created by mac on 2021/1/21.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResultPageCtrl : LKBaseViewController
@property(nonatomic,strong) NSString *titleText;
@property(nonatomic,strong) NSString *text;
@property(nonatomic,strong) NSString *subText;

@end

NS_ASSUME_NONNULL_END
