//
//  SecurityFundCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SecurityFundCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WithdrawalResultCtrl.h"
#import "WXPayDataEntity.h"
#import <WXApi.h>
#import "WechatManager.h"
#import "ResultPageCtrl.h"
#import "UserGuideCtrl.h"
#import "SecurityFundPaymentCtrl.h"

@interface SecurityFundCtrl ()
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,strong) UITextField *moneyField;
@property(nonatomic,strong) UIView *zfbView;
@property(nonatomic,strong) UIView *wxView;
@property(nonatomic,assign) double remainMoney;
@property(nonatomic,strong) LKGadentButton *btn;

@end

@implementation SecurityFundCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xf3f3f3);
    [self setTitle:@"保障金" titleColor:nil];
    [self addRightButton];
    [self viewLayout];
    self.moneyLabel.text = [NSString stringWithFormat:@"%@元",[Utils formatMoney:self.money]];
    [self.moneyLabel updateLayout];
    self.selectIndex = 0;
    self.btn.isEnabled = NO;
    [self requestSecurityFund];
}

- (void)addRightButton{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [btn setTitle:@"?" forState:normal];
            [btn addTarget:self action:@selector(showDetail) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
            self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)showDetail{
    //需要缴纳保障金
    UserGuideCtrl *vc = [UserGuideCtrl new];
    vc.tipTitle = @"平台保证金";
    vc.photoName = @"SecurityFund";
    vc.buttonHide = NO;
    vc.buttonTitle = @"";
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)setRemainMoney:(double)remainMoney
{
    _remainMoney = remainMoney;
    self.moneyField.text = [NSString stringWithFormat:@"%.2lf",remainMoney];
    [self.moneyField updateLayout];
}

- (UIView *)createFuncViewImage:(NSString *)img title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0x6295fd).CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 2;
    UIImageView *imgv1 = ImageViewWithImage(@"wd_selecet");
    imgv1.tag = 100;
    imgv1.layer.cornerRadius = 2;
    UIImageView *imgv2 = ImageViewWithImage(img);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:15];
    [view sd_addSubviews:@[imgv1,
                           imgv2,label]];
    imgv1.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .widthIs(22)
    .heightEqualToWidth();
    
    imgv2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 35)
    .widthIs(24)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(imgv2, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

#pragma mark - 支付保障金
- (void)btnPayIsClicked:(UITapGestureRecognizer *)reg{
    LKGadentButton *btn = (LKGadentButton *)reg.view;
    if(!btn.isEnabled)
        return;
    LKWeakSelf
    NSArray *ary = @[@"ali",@"wx"];
    Api *api = [Api apiPostUrl:@"work/deposit/cashDeposit"
                          para:@{@"amount":@((int)(self.remainMoney*100)),
                                 @"payChannel":ary[self.selectIndex],
                                 }
                   description:@"交保证金"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSString *plat = ary[weakSelf.selectIndex];
            if([plat isEqualToString:@"ali"]){
                NSString *orderString = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
                DLog(@"orderStr ---- = %@",orderString);
                // NOTE: 调用支付结果开始支付
                NSString *appScheme = @"alisdkdemo";
                [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
    //                DLog(@"reslut = %@",resultDic);
    //                DLog(@"%@",[resultDic objectForKey:@"memo"]);
                    if ([resultDic[@"resultStatus"]intValue] == 9000) {
                        DLog(@"成功");
                        ResultPageCtrl *vc = [ResultPageCtrl new];
                        vc.titleText = @"支付完成";
                        vc.text = @"保证金已缴纳";
                        vc.subText = @"快去接单吧";
                        [self.navigationController pushViewController:vc animated:YES];
                    } else {
                        DLog(@"失败");
                    }
                }];
            }else if([plat isEqualToString:@"wx"]){
                NSString *jsonstr = [json objectForKey:@"data"];      //得到从服务器返回到订单信息签名串
                NSDictionary *dict = [NSString jsonStringToDictionary:jsonstr];
                WXPayDataEntity *model = [[WXPayDataEntity alloc] initWithJson:dict];
                PayReq *req  = [[PayReq alloc] init];
                 req.partnerId = model.partnerid;
                 req.prepayId = model.prepayid;
                 req.package = model.wx_package;
                 req.nonceStr = model.noncestr;
                 req.timeStamp = (UInt32)[model.timestamp longLongValue];
                 req.sign = model.paySign;
                //调起微信支付
                [WechatManager hangleWechatPayWith:req];
            }

        }else{
           [MBProgressHUD showMessage:@"请求支付数据出错"];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

//查看需缴纳的保证金
- (void)requestSecurityFund{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/deposit/selectIsCashDeposit" para:@{} description:@"查看需缴纳的保证金"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            double money = [[json objectForKey:@"data"] doubleValue]/100.0;
            weakSelf.remainMoney = money;
            if(money>0){
                self.btn.isEnabled = YES;
            }else{
                self.btn.isEnabled = NO;
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@, %@",request.error,request.responseString);
            self.btn.isEnabled = NO;
        }];
}

- (void)viewLayout{
    UIView *topView = [self createTopView];
    UIView *bootomView = [self createBottomView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"补交保障金" font:LKSystemFont(17)
                                   cornerRadius:12.5];
    self.btn = btn;
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    [self.view sd_addSubviews:@[topView,bootomView,btn]];
    topView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(162);
    
    bootomView.sd_layout
    .topSpaceToView(topView, 30)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    btn.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.view, 30)
    .widthIs(320)
    .heightIs(45);
}

- (UIView *)createTopView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 162)];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(@"wd_background");
    UILabel *label1 = [Utils createLabelWithTitle:@"我的保障金"
                                       titleColor:HexColor(0xffffff)
                                         fontSize:16];
    UILabel *moneyL = [Utils createLabelWithTitle:@"00,000.00元"
                                       titleColor:HexColor(0xffffff)
                                         fontSize:18];
    self.moneyLabel = moneyL;
    [imgv sd_addSubviews:@[label1,moneyL]];
    
    UILabel *lable2 = [Utils createLabelWithTitle:@"补交押金" titleColor:HexColor(0x272727) fontSize:16];
    UILabel *labelDoller = [Utils createLabelWithTitle:@"￥" titleColor:HexColor(0x272727) fontSize:20];
    UITextField *moneyField = [UITextField new];
    moneyField.userInteractionEnabled = NO;
    moneyField.text = @"0";
    self.moneyField = moneyField;
    UILabel *unitLabel = [Utils createLabelWithTitle:@"元" titleColor:HexColor(0x272727) fontSize:20];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE4E5E6);
    [view sd_addSubviews:@[imgv,
                           lable2,
                           labelDoller,moneyField,unitLabel,
                           line]];
    imgv.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(view, 0)
    .rightSpaceToView(view, 0)
    .heightIs(50);
    
    label1.sd_layout
    .leftSpaceToView(imgv, 25)
    .centerYEqualToView(imgv)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    moneyL.sd_layout
    .centerYEqualToView(imgv)
    .rightSpaceToView(imgv, 21)
    .autoHeightRatio(0);
    [moneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    lable2.sd_layout
    .topSpaceToView(imgv, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [lable2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelDoller.sd_layout
    .topSpaceToView(imgv, 54)
    .leftSpaceToView(view, 23)
    .widthIs(20)
    .heightIs(28);
    
    moneyField.sd_layout
    .topSpaceToView(imgv, 52)
    .leftSpaceToView(view, 50)
    .heightIs(26)
    .widthIs(120);
    
    unitLabel.sd_layout
    .centerYEqualToView(moneyField)
    .leftSpaceToView(moneyField, 4)
    .autoWidthRatio(0);
    [unitLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .leftEqualToView(unitLabel)
    .topSpaceToView(unitLabel, 8)
    .rightSpaceToView(view, 25)
    .heightIs(0.5);
    
    return view;
}

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    UIButton *zfbBtn = [_zfbView viewWithTag:100];
    UIButton *wxBtn = [_wxView viewWithTag:100];
    [wxBtn setSelected:NO];
    [zfbBtn setSelected:NO];
    if(selectIndex==0){
        [zfbBtn setSelected:YES];
    }else if(selectIndex==1){
        [wxBtn setSelected:YES];
    }
}

- (void)selectIsClicked:(UITapGestureRecognizer *)reg{
    UIView *view = reg.view;
    if(view == _wxView){
        self.selectIndex = 1;
    }else if(view == _zfbView){
        self.selectIndex = 0;
    }
}


- (UIView *)createBottomView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 152)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *labe1 = [Utils createLabelWithTitle:@"提现方式"
                                      titleColor:HexColor(0x272727)
                                        fontSize:16];
    UIView *wxView = [self createSwitchView:@"weixinzhif" title:@"微信支付" subTitle:@"亿万用户的选择，更快更安全"];
    self.wxView = wxView;
    UIView *zfbView = [self createSwitchView:@"zfb_logo" title:@"支付宝" subTitle:@"10亿人都在用，真安全，更方便"];
    self.zfbView = zfbView;
    [view sd_addSubviews:@[labe1,
                           zfbView,
                           wxView]];
    labe1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [labe1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbView.sd_layout
    .topSpaceToView(labe1, 15)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    
    wxView.sd_layout
    .topSpaceToView(zfbView, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    [view setupAutoHeightWithBottomView:wxView bottomMargin:0];

    return view;
}

- (UIView *)createSwitchView:(NSString *)imageName title:(NSString *)title subTitle:(NSString *)subTitle{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [view addTapGestureTarget:self action:@selector(selectIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    UIImageView *imgv = ImageViewWithImage(imageName);
    UILabel *titleLabel = [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subTitleLabel = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn = [self createSelectBtn];
    btn.tag = 100;
    [view sd_addSubviews:@[line,
                           imgv,
                           titleLabel,subTitleLabel,
                           btn]];
    line.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 20)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(line)
    .widthIs(40)
    .heightIs(40);
    
    titleLabel.sd_layout
    .topEqualToView(imgv)
    .leftSpaceToView(imgv, 15)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleLabel.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel, 2)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(20)
    .heightIs(20);
    [subTitleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (UIButton *)createSelectBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_not_select"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_selected"] forState:UIControlStateSelected];
    return btn;
}

@end
