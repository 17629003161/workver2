//
//  MyMoneyBagCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "WithdrawalEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMoneyBagCtrl : LKBaseTableViewController
@property(nonatomic,strong) WithdrawalEntity *model;
@end

NS_ASSUME_NONNULL_END
