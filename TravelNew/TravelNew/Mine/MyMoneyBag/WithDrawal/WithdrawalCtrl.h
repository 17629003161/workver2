//
//  WithdrawalCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "WithdrawalEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalCtrl : LKBaseViewController
@property(nonatomic,strong) void (^refreshBlock)(void);
@end

NS_ASSUME_NONNULL_END
