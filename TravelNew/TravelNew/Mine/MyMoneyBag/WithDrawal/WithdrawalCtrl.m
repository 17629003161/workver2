//
//  WithdrawalCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "WithdrawalCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WithdrawalResultCtrl.h"
#import "LKTipDialog.h"

@interface WithdrawalCtrl ()<UITextFieldDelegate>
@property(nonatomic,strong) WithdrawalEntity *model;
@property(nonatomic,strong) UIView *wxView;
@property(nonatomic,strong) UIView *zfbView;
@property(nonatomic,strong) UILabel *balanceLabel;
@property(nonatomic,strong) UITextField *balanceField;
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UITextField *nameField;
@property(nonatomic,strong) LKGadentButton *btn;
@end

@implementation WithdrawalCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xf3f3f3);
    [self setTitle:@"提现" titleColor:nil];
    [self.view addSubview:self.scrollView];
    self.scrollView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight+60);
    [self.scrollView addTapGestureTarget:self action:@selector(viewIsTyped:)];
    [self viewLayout];
    self.selectIndex = 0;
    self.btn.isEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self checkBalence];
}

-(void)setModel:(WithdrawalEntity *)model
{
    _model = model;
    self.balanceLabel.text = [NSString stringWithFormat:@"%.2lf元",[self.model.walletNumber doubleValue]/100.0];
    [self.balanceLabel updateLayout];
}

- (void)checkBalence {
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/wallet/selectWalletByUserId"
                          para:@{}
                   description:@"查询钱包余额"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            WithdrawalEntity *model = [[WithdrawalEntity alloc] initWithJson:dict];
            weakSelf.model = model;
        };
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
}


- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self.balanceField resignFirstResponder];
    [self.nameField resignFirstResponder];
}


- (UIView *)createFuncViewImage:(NSString *)img title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0x6295fd).CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 2;
    UIImageView *imgv1 = ImageViewWithImage(@"wd_selecet");
    imgv1.tag = 100;
    imgv1.layer.cornerRadius = 2;
    UIImageView *imgv2 = ImageViewWithImage(img);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:15];
    [view sd_addSubviews:@[imgv1,
                           imgv2,label]];
    imgv1.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .widthIs(22)
    .heightEqualToWidth();
    
    imgv2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 35)
    .widthIs(24)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(imgv2, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

#pragma mark - 提现

- (void)btnPayIsClicked:(UITapGestureRecognizer *)reg
{
    LKGadentButton *btn = (LKGadentButton *)reg.view;
    if(!btn.isEnabled){
        return;
    }
    if(self.selectIndex==1){
        LKTipDialog *dlg = [[LKTipDialog alloc] initWithFrame:CGRectMake(0, 0, 270, 181)];
        dlg.message = @"系统目前暂不支持微信提现";
        [dlg show];
        return;
    }
    LKWeakSelf
    double totle = [self.model.walletNumber doubleValue]/100.0;
    double money = [self.balanceField.text doubleValue];
    NSString *name = self.nameField.text;
    if(name.length==0){
        [MBProgressHUD showMessage:@"请输入提现的账户名称"];
        return;
    }
    if(money>totle){
        [MBProgressHUD showMessage:@"提现金额不正确！"];
        return;
    }
    NSArray *ary = @[@"ali",@"wx"];
    NSString *payChannel = [ary objectAtIndex:self.selectIndex];
    Api *api = [Api apiPostUrl:@"work/account/transferAccounts"
                          para:@{@"transferAccount":@((int)(money*100)),
                                 @"transferName":name,
                                 @"payChannel":payChannel
                          }
                   description:@"提交提现请求"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        NSInteger code = [[json objectForKey:@"code"] integerValue];
        if(code ==200){
            BOOL isok = [[json objectForKey:@"data"] boolValue];
            if(isok){
                if(self.refreshBlock){
                    self.refreshBlock();
                }
                WithdrawalResultCtrl *vc = [WithdrawalResultCtrl new];
                weakSelf.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [MBProgressHUD showMessage:@"提现失败！"];
            }
        }else if(code==500){
            [MBProgressHUD showMessage:[json objectForKey:@"msg"]];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSLog(@"%@,%@",request.error,request.responseString);
        }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)aTextfield {
    [self.balanceField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self btnPayIsClicked:nil];
    return YES;
}

- (void)viewLayout{
    UIView *topView = [self createTopView];
   // [topView addTapGestureTarget:self action:@selector(viewIsTyped:)];
    UIView *bootomView = [self createBottomView];
   // [bootomView addTapGestureTarget:self action:@selector(viewIsTyped:)];
    
    UIView *inputView = [self createInputView];
    
    UILabel *tipLabel = [Utils createLabelWithTitle:@"每笔提现将扣除您提现金额的1%，如您有任何疑问，请致电旅咖出行400-0290072" titleColor:HexColor(0x999999) fontSize:13];
    tipLabel.numberOfLines = 0;

    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"立即提现" font:LKSystemFont(17)
                                   cornerRadius:12.5];
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    btn.layer.cornerRadius = 22;
    btn.layer.masksToBounds = YES;
    self.btn = btn;
    
    [self.scrollView sd_addSubviews:@[topView,bootomView,tipLabel,inputView,btn]];
    topView.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(162);
    
    bootomView.sd_layout
    .topSpaceToView(topView, 10)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(152);
    
    inputView.sd_layout
    .topSpaceToView(bootomView, 10)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(110);
    
    tipLabel.sd_layout
    .topSpaceToView(inputView, 20)
    .leftSpaceToView(self.scrollView, 25)
    .rightSpaceToView(self.scrollView, 25)
    .autoHeightRatio(0);
    
    
    btn.sd_layout
    .topSpaceToView(tipLabel, 25)
    .leftSpaceToView(self.scrollView, 25)
    .rightSpaceToView(self.scrollView, 25)
    .heightIs(44);
}

- (UIView *)createInputView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 162)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"验证信息" titleColor:HexColor(0x272727) fontSize:16];
    UILabel *label2 = [Utils createLabelWithTitle:@"对应账户的真实姓名" titleColor:HexColor(0x666666) fontSize:15];
    UITextField *field = [UITextField new];
    field.layer.borderColor = HexColor(0xC5C5C5).CGColor;
    field.layer.borderWidth = 0.5;
    field.delegate = self;
    field.returnKeyType = UIReturnKeyDone;
    field.layer.backgroundColor = [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0].CGColor;
    [field textLeftOffset:8];
    self.nameField = field;
    [view sd_addSubviews:@[label1,label2,field]];
    label1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 19)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    field.sd_layout
    .centerYEqualToView(label2)
    .leftSpaceToView(label2, 20)
    .rightSpaceToView(view, 25)
    .heightIs(30);
    return view;
}

- (UIView *)createTopView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 162)];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(@"wd_background");
    UILabel *label1 = [Utils createLabelWithTitle:@"可用余额"
                                       titleColor:HexColor(0xffffff)
                                         fontSize:16];
    UILabel *moneyL = [Utils createLabelWithTitle:@"0.00元"
                                       titleColor:HexColor(0xffffff)
                                         fontSize:18];
    self.balanceLabel = moneyL;
    [imgv sd_addSubviews:@[label1,moneyL]];
    
    UILabel *lable2 = [Utils createLabelWithTitle:@"提现金额" titleColor:HexColor(0x272727) fontSize:16];
    UILabel *labelDoller = [Utils createLabelWithTitle:@"￥" titleColor:HexColor(0x272727) fontSize:20];
    UITextField *textField = [UITextField new];
    textField.clearButtonMode = UITextFieldViewModeAlways;
    //textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.delegate = self;
    textField.font = LKSystemFont(20);
    textField.textColor = HexColor(0x272727);
    textField.text = @"0.0";
    textField.delegate = self;
    self.balanceField = textField;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE4E5E6);
    [view sd_addSubviews:@[imgv,
                           lable2,
                           labelDoller,textField,
                           line]];
    imgv.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(view, 0)
    .rightSpaceToView(view, 0)
    .heightIs(50);
    
    label1.sd_layout
    .leftSpaceToView(imgv, 25)
    .centerYEqualToView(imgv)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    moneyL.sd_layout
    .centerYEqualToView(imgv)
    .rightSpaceToView(imgv, 21)
    .autoHeightRatio(0);
    [moneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    lable2.sd_layout
    .topSpaceToView(imgv, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [lable2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelDoller.sd_layout
    .topSpaceToView(imgv, 54)
    .leftSpaceToView(view, 23)
    .widthIs(20)
    .heightIs(28);
    
    textField.sd_layout
    .topSpaceToView(imgv, 52)
    .leftSpaceToView(view, 50)
    .rightSpaceToView(view, 50)
    .heightIs(20);
    
    line.sd_layout
    .leftEqualToView(textField)
    .topSpaceToView(textField, 8)
    .rightSpaceToView(view, 25)
    .heightIs(0.5);
    
    return view;
}

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    UIButton *zfbBtn = [_zfbView viewWithTag:100];
    UIButton *wxBtn = [_wxView viewWithTag:100];
    [wxBtn setSelected:NO];
    [zfbBtn setSelected:NO];
    if(selectIndex==0){
        [zfbBtn setSelected:YES];
    }else if(selectIndex==1){
        [wxBtn setSelected:YES];
    }
}

- (void)selectIsClicked:(UITapGestureRecognizer *)reg{
    UIView *view = reg.view;
    if(view == _wxView){
        self.selectIndex = 1;
    }else if(view == _zfbView){
        self.selectIndex = 0;
    }
}


- (UIView *)createBottomView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 152)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *labe1 = [Utils createLabelWithTitle:@"提现方式"
                                      titleColor:HexColor(0x272727)
                                        fontSize:16];
    UIView *wxView = [self createSwitchView:@"weixinzhif" title:@"微信支付" subTitle:@"亿万用户的选择，更快更安全"];
    self.wxView = wxView;
    UIView *zfbView = [self createSwitchView:@"zfb_logo" title:@"支付宝" subTitle:@"10亿人都在用，真安全，更方便"];
    self.zfbView = zfbView;
    [view sd_addSubviews:@[labe1,
                           zfbView,
                           wxView]];
    labe1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [labe1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbView.sd_layout
    .topSpaceToView(labe1, 15)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    
    wxView.sd_layout
    .topSpaceToView(zfbView, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    [view setupAutoHeightWithBottomView:wxView bottomMargin:0];

    return view;
}

- (UIView *)createSwitchView:(NSString *)imageName title:(NSString *)title subTitle:(NSString *)subTitle{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [view addTapGestureTarget:self action:@selector(selectIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    UIImageView *imgv = ImageViewWithImage(imageName);
    UILabel *titleLabel = [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subTitleLabel = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn = [self createSelectBtn];
    btn.tag = 100;
    [view sd_addSubviews:@[line,
                           imgv,
                           titleLabel,subTitleLabel,
                           btn]];
    line.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 20)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(line)
    .widthIs(40)
    .heightIs(40);
    
    titleLabel.sd_layout
    .topEqualToView(imgv)
    .leftSpaceToView(imgv, 15)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleLabel.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel, 2)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(20)
    .heightIs(20);
    [subTitleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (UIButton *)createSelectBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_not_select"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_selected"] forState:UIControlStateSelected];
    return btn;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.balanceField){
        NSString *strContent = [textField.text stringByReplacingCharactersInRange:range withString:string];
        CGFloat balance = [strContent doubleValue];
        if(balance==0){
            self.btn.isEnabled = NO;
        }else{
            self.btn.isEnabled = YES;
        }
        CGFloat total = [self.model.walletNumber doubleValue]/100.0;
        if(balance > total){
            return NO;
        }
    }
    return YES;
}


@end
