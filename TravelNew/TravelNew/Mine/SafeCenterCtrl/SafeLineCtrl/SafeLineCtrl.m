//
//  SafeLineCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SafeLineCtrl.h"
#import <MessageUI/MessageUI.h>
#import "ContactManModel.h"

@interface SafeLineCtrl ()<MFMessageComposeViewControllerDelegate>
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) UILabel *positionLabel;
@end

@implementation SafeLineCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF3F3F3);
    [self layout];
   // [self buildUI];
    DLog(@"kStatusBarHeight= %lf,kNavigationHeight = %lf",kStatusBarHeight,kNavigationHeight);
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self locationLocate];
}

- (void)locationLocate{
    LKWeakSelf
    LKLocationManager *manager = [LKLocationManager sharedManager];
    self.positionLabel.text = manager.name;
    [self.positionLabel updateLayout];
    manager.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, CLLocationCoordinate2D location, NSString * _Nonnull cityName) {
        weakSelf.positionLabel.text = name;
        [weakSelf.positionLabel updateLayout];
    };
}

-(NSMutableArray *)dataArray
{
    if(!_dataArray){
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

- (void)loadData{
    Api *api = [Api apiPostUrl:@"work/contact/list" para:@{} description:@"work-查询紧急联系人"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [self.dataArray removeAllObjects];
            for(NSDictionary *item in [json objectForKey:@"data"]){
                ContactManModel *model = [[ContactManModel alloc] initWithJson:item];
                [self.dataArray addObject:model];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (void)buildUI
{
    DLog(@"松 viewDidLoad top: %lf",
          [[UIApplication sharedApplication] statusBarFrame].size.height
          );
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.leading.trailing.top.bottom.equalTo(self.view);
        }

    }];
}


- (BOOL)isPhoneX {
    BOOL iPhoneX = NO;
    if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {//判断是否是手机
        return iPhoneX;
    }
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            iPhoneX = YES;
        }
    }
    return iPhoneX;
}


- (void)layout{
    UIView *topView = [self makeTopView];
    UIView *addrView = [self makeAddrView];
    UIView *contentView = [self createContentView];
    UILabel *label = [Utils createLabelWithTitle:@"随意拨打占用线路，我们将暂停为你提供服务" titleColor:HexColor(0xD2D2D2) fontSize:13];
    UIImageView *imgv = [self createButtonView];
    imgv.layer.cornerRadius = 12;
    imgv.layer.masksToBounds = YES;
    UIView *doubleBtn = [self createDubleView];
    [self.view sd_addSubviews:@[topView,
                                addrView,
                                contentView,
                                label,
                                imgv,
                                doubleBtn]];
    topView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(200);
    
    contentView.sd_layout
    .topSpaceToView(addrView, 40+SAFEVIEWTOPMARGIN)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    label.sd_layout
    .topSpaceToView(contentView, 52)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .topSpaceToView(label, 10)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(44);
    
    doubleBtn.sd_layout
    .topSpaceToView(imgv, 15)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(44);
    
}

- (void)safeCallIsClicked:(UITapGestureRecognizer *)reg
{
    [Utils callNumber:@"4000290072"];
}

- (void)call110IsClicked:(UITapGestureRecognizer *)reg
{
    [Utils callNumber:@"110"];
}

- (void)sendMessageIsClicked:(UITapGestureRecognizer *)reg
{
    NSMutableArray *phoneAry = [NSMutableArray new];
    for (ContactManModel *model in self.dataArray) {
        [phoneAry addObject:model.contactPhone];
    }
    if(phoneAry.count>0){
        if ([MFMessageComposeViewController canSendText]) {
            //  判断一下是否支持发送短信，比如模拟器
            MFMessageComposeViewController *messageVC = [[MFMessageComposeViewController alloc] init];
            messageVC.recipients = phoneAry; //需要发送的手机号数组
            messageVC.body = @"发送的内容";
            messageVC.messageComposeDelegate = self; //指定代理
            [self presentViewController:messageVC animated:YES completion:nil];
        } else {
            [MBProgressHUD showMessage:@"设备不支持短信功能"];
        }
    }else{
        [MBProgressHUD showMessage:@"无安全联系人,请添加安全联系人"];
    }
}

#pragma mark MFmessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if (result == MessageComposeResultCancelled) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    } else if (result == MessageComposeResultFailed) {
        [controller dismissViewControllerAnimated:YES completion:^{
            [MBProgressHUD showMessage:@"邀请发送失败，请稍后重试"];
        }];
    } else {
        [controller dismissViewControllerAnimated:YES completion:^{
            [MBProgressHUD showMessage:@"邀请发送成功"];
        }];
    }
}


- (UIImageView *)createButtonView{
    UIImageView *imgv = ImageViewWithImage(@"rect_bg_sel");
    UIImageView *icon = ImageViewWithImage(@"telephone_icon2");
    UILabel *label = [Utils createLabelWithTitle:@"呼叫安全专线" titleColor:[UIColor whiteColor] fontSize:16];
    [imgv sd_addSubviews:@[icon,label]];
    [imgv addTapGestureTarget:self action:@selector(safeCallIsClicked:)];
    icon.sd_layout
    .leftSpaceToView(imgv, 97)
    .centerYEqualToView(imgv)
    .widthIs(22)
    .heightIs(26);
    label.sd_layout
    .leftSpaceToView(icon, 14)
    .centerYEqualToView(imgv)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return imgv;
}

- (UIView *)createDubleView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 22.5;
    view.layer.shadowColor = [UIColor colorWithRed:229/255.0 green:224/255.0 blue:224/255.0 alpha:0.5].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 5;
    
    UILabel *label1 = [Utils createLabelWithTitle:@"短信求助" titleColor:HexColor(0x36364D) fontSize:15];
    [label1 addTapGestureTarget:self action:@selector(sendMessageIsClicked:)];
    UILabel *label2 = [Utils createLabelWithTitle:@"拨打110" titleColor:HexColor(0x36364D) fontSize:15];
    [label2 addTapGestureTarget:self action:@selector(call110IsClicked:)];
    UIImageView *icon = ImageViewWithImage(@"enmergency");
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xCECDD1);
    [view sd_addSubviews:@[label1,line,label2,icon]];
    
    line.sd_layout
    .centerYEqualToView(view)
    .centerXEqualToView(view)
    .widthIs(1)
    .heightIs(20);
    
    label1.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(line, 50)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(line, 50)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    icon.sd_layout
    .topEqualToView(label2)
    .leftSpaceToView(label2, 3)
    .widthIs(22)
    .heightIs(11);
    return view;
}


- (UIView *)createContentView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = HexColor(0xF3F3F3);
    UILabel *label = [Utils createBoldLabelWithTitle:@"安全专线仅处理以下人身安全问题" titleColor:HexColor(0x4A4A4A) fontSize:16];
    UIView *dotView1 = [self creatdotView:@"遇到交通事故"];
    UIView *dotView2 = [self creatdotView:@"联系不上乘车的亲友"];
    UIView *dotView3 = [self creatdotView:@"遭遇性骚扰或性侵"];
    UIView *dotView4 = [self creatdotView:@"与他人发生肢体冲突"];
    UIView *dotView5 = [self creatdotView:@"被限制人身自由"];
    UIView *dotView6 = [self creatdotView:@"遭遇其他人身伤害"];
    [view sd_addSubviews:@[label,dotView1,dotView2,dotView3,dotView4,dotView5,dotView6]];
    label.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    dotView1.sd_layout
    .topSpaceToView(label, 8)
    .leftEqualToView(label);
    
    dotView2.sd_layout
    .topSpaceToView(dotView1, 8)
    .leftEqualToView(label);
    
    dotView3.sd_layout
    .topSpaceToView(dotView2, 8)
    .leftEqualToView(label);
    
    dotView4.sd_layout
    .topSpaceToView(dotView3, 8)
    .leftEqualToView(label);
    
    dotView5.sd_layout
    .topSpaceToView(dotView4, 8)
    .leftEqualToView(label);
    
    dotView6.sd_layout
    .topSpaceToView(dotView5, 8)
    .leftEqualToView(label);
    
    [view setupAutoHeightWithBottomView:dotView6 bottomMargin:0];
    return view;
}


- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)makeAddrView{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(20,166,kScreenWidth-40,90);
    view.layer.borderWidth = 0.5;
    view.layer.borderColor = [UIColor colorWithRed:168/255.0 green:172/255.0 blue:188/255.0 alpha:1.0].CGColor;
    view.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    view.layer.cornerRadius = 8;
    
    UIImageView *imgv = ImageViewWithImage(@"dingwei-3");
    UILabel *label = [Utils createLabelWithTitle:@"当前位置(仅供参考)" titleColor:HexColor(0x4A4A4A) fontSize:14];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"中国陕西省西安市未央区谭家花园街道" titleColor:HexColor(0x4A4A4A) fontSize:18];
    self.positionLabel = label2;
    [view sd_addSubviews:@[imgv,label,label2]];
    imgv.sd_layout
    .topSpaceToView(view, 16)
    .leftSpaceToView(view, 16)
    .widthIs(12)
    .heightIs(12);
    
    label.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 2)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 13)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (UIImageView *)makeTopView{
    UIImageView *imgv = ImageViewWithImage(@"safe_line_bg");
    imgv.userInteractionEnabled = YES;
    UIImageView *arrow = ImageViewWithImage(@"left_arrow_black");
    [arrow addTapGestureTarget:self action:@selector(backIsClicked:)];
    UILabel *label = [Utils createBoldLabelWithTitle:@"如遇险情，请保持冷静" titleColor:HexColor(0x333333) fontSize:24];
    UILabel *label2 = [Utils createLabelWithTitle:@"确保自身安全的情况下寻求帮助" titleColor:HexColor(0x4A4A4A) fontSize:14];
    [imgv sd_addSubviews:@[label,label2,arrow]];
    
    arrow.sd_layout
    .topSpaceToView(imgv, kStatusBarHeight)
    .leftSpaceToView(imgv, 14)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .topSpaceToView(arrow, 25)
    .leftSpaceToView(imgv, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 7)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return imgv;
}

- (UIView *)creatdotView:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *dot = ImageViewWithImage(@"dot_blue");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x999999) fontSize:14];
    [view sd_addSubviews:@[dot,label]];
    
    dot.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .widthIs(7)
    .heightIs(7);
    
    label.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(dot, 9)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}


@end
