//
//  AddContactManCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "ContactManModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddContactManCtrl : LKBaseViewController
@property(nonatomic,copy) void (^refreshBlock)(void);
@property(nonatomic,strong) ContactManModel *contactModel;
@end

NS_ASSUME_NONNULL_END
