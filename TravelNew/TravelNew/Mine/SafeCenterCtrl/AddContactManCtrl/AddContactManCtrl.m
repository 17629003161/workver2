//
//  AddContactManCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AddContactManCtrl.h"

@interface AddContactManCtrl ()
@property(nonatomic,strong) UILabel *promptLabel;
@property(nonatomic,strong) UITextField *phoneField;
@property(nonatomic,strong) UITextField *noteField;

@end

@implementation AddContactManCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    if (self.contactModel) {
        self.promptLabel.text = @"修改安全联系人";
        self.phoneField.text = _contactModel.contactPhone;
        self.noteField.text = _contactModel.contactName;
        [self.promptLabel updateLayout];
    }
}

- (void)btnIsClicked:(id)sender
{
    if(self.contactModel){
        [self modifyContactMan];
    }else{
        [self addContactMan];
    }
}

- (void)modifyContactMan{
    NSString *namestr = self.noteField.text;
    if(namestr.length==0){
        [MBProgressHUD showMessage:@"请输入联系人姓名"];
        return;
    }
    NSString *phonestr = self.phoneField.text;
    if(![LKInputChecker checkTelNumber:phonestr]){
        [MBProgressHUD showMessage:@"手机号格式不正确！"];
        return;
    }
    Api *api = [Api apiPostUrl:@"work/contact/edit" para:@{@"contactName":namestr,
                                                           @"contactPhone":phonestr,
                                                           @"id":self.contactModel.id}
                   description:@"work-修改紧急联系人"];
    api.withToken = YES;
    LKWeakSelf
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *json = request.responseObject;
        DLog(@"json = %@",json);
        if([[json objectForKey:@"code"] integerValue]==200){
            if(weakSelf.refreshBlock)
                weakSelf.refreshBlock();
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)addContactMan{
    NSString *namestr = self.noteField.text;
    NSString *phonestr = self.phoneField.text;
    Api *api = [Api apiPostUrl:@"work/contact/add" para:@{@"contactName":namestr,
                                                          @"contactPhone":phonestr}
                   description:@"work-添加紧急联系人"];
    api.withToken = YES;
    LKWeakSelf
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *json = request.responseObject;
        DLog(@"json = %@",json);
        if([[json objectForKey:@"code"] integerValue]==200){
            if(weakSelf.refreshBlock)
                weakSelf.refreshBlock();
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self.phoneField resignFirstResponder];
    [self.noteField resignFirstResponder];
}

- (void)layout{
    [self.view addTapGestureTarget:self action:@selector(viewIsTyped:)];
    UILabel *label = [Utils createBoldLabelWithTitle:@"添加安全联系人" titleColor:HexColor(0x333333) fontSize:24];
    self.promptLabel = label;
    UILabel *label2 = [Utils createLabelWithTitle:@"安全联系人可在您遇险时帮你报警或拨打安全专线，建议添加信任的家人或者好友" titleColor:HexColor(0x4A4A4A) fontSize:14];
    label2.numberOfLines = 0;
    UIView *inputView1 = [self createInputView:@"shouji" prompt:@"手机号" placeHolder:@"请输入您的手机号"];
    self.phoneField = (UITextField *)[inputView1 viewWithTag:100];
    self.phoneField.clearButtonMode = UITextFieldViewModeAlways;
    self.phoneField.keyboardType = UIKeyboardTypePhonePad;
    UIView *inputView2 = [self createInputView:@"beizhu" prompt:@"备注" placeHolder:@"请输入联系人备注(必填)"];
    self.noteField = (UITextField *)[inputView2 viewWithTag:100];
    self.noteField.keyboardType = UIKeyboardTypeDefault;
    UIImageView *dot = ImageViewWithImage(@"dot_orange 4");
    UILabel *label3 = [Utils createLabelWithTitle:@"建议填写真实姓名，方便沟通" titleColor:HexColor(0xFF9739) fontSize:13];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"保存联系人" font:LKSystemFont(17) cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [self.view sd_addSubviews:@[label,
                                label2,
                                inputView1,
                                inputView2,
                                dot,label3,
                                btn]];
    label.sd_layout
    .topSpaceToView(self.view, 13)
    .leftSpaceToView(self.view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 7)
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .autoHeightRatio(0);
    
    inputView1.sd_layout
    .topSpaceToView(label2, 40)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    inputView2.sd_layout
    .topSpaceToView(inputView1, 21)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    dot.sd_layout
    .topSpaceToView(inputView2, 18)
    .leftSpaceToView(self.view, 28)
    .widthIs(7)
    .heightIs(7);
    
    label3.sd_layout
    .centerYEqualToView(dot)
    .leftSpaceToView(dot, 15)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .bottomSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(44);
}

- (UIView *)createInputView:(NSString *)icon
                     prompt:(NSString *)prompt
                placeHolder:(NSString *)placeHolder{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(icon);
    UILabel *label = [Utils createBoldLabelWithTitle:prompt titleColor:HexColor(0x333333) fontSize:14];
    UITextField *textField = [UITextField new];
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.placeholder = placeHolder;
    textField.tag = 100;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    [view sd_addSubviews:@[imgv,label,
                           textField,
                           line]];
    imgv.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 24)
    .widthIs(14)
    .heightIs(20);
    
    label.sd_layout
    .leftSpaceToView(imgv, 12)
    .topEqualToView(imgv)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    textField.sd_layout
    .topSpaceToView(label, 13)
    .leftEqualToView(label)
    .rightSpaceToView(view, 50)
    .heightIs(20);
    
    line.sd_layout
    .topSpaceToView(imgv, 48)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(1);
    [view setupAutoHeightWithBottomView:line bottomMargin:0];
    return view;
}


@end
