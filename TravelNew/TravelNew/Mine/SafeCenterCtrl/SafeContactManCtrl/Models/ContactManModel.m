//
//
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "ContactManModel.h"

@implementation ContactManModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.id  = [json objectForKey:@"id"];
        self.contactName  = [json objectForKey:@"contactName"];
        self.userId  = [json objectForKey:@"userId"];
        self.contactPhone  = [json objectForKey:@"contactPhone"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.id forKey:@"zx_id"];
    [aCoder encodeObject:self.contactName forKey:@"zx_contactName"];
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
    [aCoder encodeObject:self.contactPhone forKey:@"zx_contactPhone"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.id = [aDecoder decodeObjectForKey:@"zx_id"];
        self.contactName = [aDecoder decodeObjectForKey:@"zx_contactName"];
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
        self.contactPhone = [aDecoder decodeObjectForKey:@"zx_contactPhone"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"id : %@\n",self.id];
    result = [result stringByAppendingFormat:@"contactName : %@\n",self.contactName];
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    result = [result stringByAppendingFormat:@"contactPhone : %@\n",self.contactPhone];
    
    return result;
}

@end

