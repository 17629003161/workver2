//
//  ContactManModel.h
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactManModel : NSObject

@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,copy) NSString *contactName;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *contactPhone;
 
-(id)initWithJson:(NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
