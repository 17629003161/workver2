//
//  SafeContactManCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SafeContactManCtrl.h"
#import "AddContactManCtrl.h"
#import "ContactManCell.h"

@interface SafeContactManCtrl ()

@end

@implementation SafeContactManCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF3F3F3);
    self.tableView.backgroundColor = HexColor(0xF3F3F3);
    [self.tableView registerClass:[ContactManCell class] forCellReuseIdentifier:@"ContactManCell"];
    self.tableView.tableFooterView = [self makeFooterView];
    [self layout];
    [self disabelPageRefresh];
    [self loadData];
}

- (void)loadData{
    Api *api = [Api apiPostUrl:@"work/contact/list" para:@{} description:@"work-查询紧急联系人"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [self.dataArray removeAllObjects];
            for(NSDictionary *item in [json objectForKey:@"data"]){
                ContactManModel *model = [[ContactManModel alloc] initWithJson:item];
                [self.dataArray addObject:model];
            }
            [self.tableView reloadData];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)addContactManIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    AddContactManCtrl *vc = [AddContactManCtrl new];
    vc.refreshBlock = ^{
        [weakSelf loadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)layout{
    UIView *topView = [self makeTopView];
    [self.view sd_addSubviews:@[topView]];
    topView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(200+SAFEVIEWTOPMARGIN);
    
    self.tableView.sd_layout
    .topSpaceToView(topView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (UIView *)makeFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 83)];
    view.backgroundColor = HexColor(0xF3F3F3);
    UIView *btnView = [self makeButtonView];
    [btnView addTapGestureTarget:self action:@selector(addContactManIsClicked:)];
    [view addSubview:btnView];
    
    btnView.sd_layout
    .leftSpaceToView(view, 20)
    .bottomEqualToView(view)
    .widthIs(230)
    .heightIs(45);
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactManCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactManCell"];
    cell.model = self.dataArray[indexPath.row];
    LKWeakSelf
    cell.modifyBlock = ^(ContactManModel * _Nonnull model) {
        AddContactManCtrl *vc = [AddContactManCtrl new];
        vc.contactModel = model;
        vc.refreshBlock = ^{
            [weakSelf loadData];
        };
        [self.navigationController pushViewController:vc animated:YES];
    };
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
 
// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
 
// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteContactManInRow:indexPath.row];
    }
}
 
// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)deleteContactManInRow:(NSInteger)row{
    ContactManModel *model = self.dataArray[row];
    Api *api = [Api apiPostUrl:@"work/contact/remove"
                          para:@{@"id":model.id}
                   description:@"work-删除紧急联系人"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [self.dataArray removeObjectAtIndex:row];
            [self.tableView reloadData];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}




- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIImageView *)makeTopView{
    UIImageView *imgv = ImageViewWithImage(@"safe_line_bg");
    imgv.userInteractionEnabled = YES;
    UIImageView *arrow = ImageViewWithImage(@"left_arrow_black");
    [arrow addTapGestureTarget:self action:@selector(backIsClicked:)];
    UILabel *label = [Utils createBoldLabelWithTitle:@"安全联系人" titleColor:HexColor(0x333333) fontSize:24];
    UILabel *label2 = [Utils createLabelWithTitle:@"添加后，当你使用 “110报警” 时，行程信息将自动分享至安全联系人，行程中如遇危险可一键求助" titleColor:HexColor(0x4A4A4A) fontSize:14];
    label2.numberOfLines = 0;
    [imgv sd_addSubviews:@[label,label2,arrow]];
    
    arrow.sd_layout
    .topSpaceToView(imgv, 32+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(imgv, 14)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .topSpaceToView(arrow, 25)
    .leftSpaceToView(imgv, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 7)
    .leftEqualToView(label)
    .rightSpaceToView(imgv, 20)
    .autoHeightRatio(0);
    return imgv;
}

- (UIView *)makeButtonView{
    UIView *view = [UIView new];
    view.frame = CGRectMake(20,304,230,45);
    view.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    view.layer.cornerRadius = 22.5;
    view.layer.shadowColor = [UIColor colorWithRed:229/255.0 green:224/255.0 blue:224/255.0 alpha:0.5].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 5;
    UILabel *label = [Utils createBoldLabelWithTitle:@"+  添加安全联系人" titleColor:HexColor(0x36364D) fontSize:15];
    [view addSubview:label];
    label.sd_layout
    .centerYEqualToView(view)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


@end
