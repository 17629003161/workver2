//
//  AddContactCell.h
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "ContactManModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactManCell : BaseCustomTableViewCell
@property(nonatomic,copy) void (^modifyBlock)(ContactManModel *);
@property(nonatomic,strong) ContactManModel *model;
@end

NS_ASSUME_NONNULL_END
