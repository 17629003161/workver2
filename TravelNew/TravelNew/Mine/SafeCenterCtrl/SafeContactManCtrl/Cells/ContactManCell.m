//
//  AddContactCell.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ContactManCell.h"

@interface ContactManCell()

@property(nonatomic,strong) UIImageView *headImgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *phoneLabel;

@end


@implementation ContactManCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ContactManModel *)model
{
    _model = model;
    self.phoneLabel.text = model.contactPhone;
    self.nameLabel.text = model.contactName;
    [self.phoneLabel updateLayout];
    [self.nameLabel updateLayout];
}

- (void)modifyIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.modifyBlock){
        self.modifyBlock(self.model);
    }
}

-(void)configViews
{
    self.contentView.backgroundColor = HexColor(0xF3F3F3);
    _headImgv = ImageViewWithImage(@"lianxiren");
    UILabel *label = [Utils createLabelWithTitle:@"联系人" titleColor:HexColor(0x333333) fontSize:14];
    _phoneLabel = [Utils createLabelWithTitle:@"13151572938" titleColor:HexColor(0x333333) fontSize:15];
    _nameLabel = [Utils createLabelWithTitle:@"王亚荣" titleColor:HexColor(0x333333) fontSize:15];
    UIImageView *imgv = ImageViewWithImage(@"pingjia");
    [imgv addTapGestureTarget:self action:@selector(modifyIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    [self.contentView sd_addSubviews:@[_headImgv,label,
                                       _phoneLabel,_nameLabel,imgv,
                                       line]];
    _headImgv.sd_layout
    .topSpaceToView(self.contentView, 13)
    .leftSpaceToView(self.contentView, 23)
    .widthIs(17)
    .heightIs(16);
    
    label.sd_layout
    .centerYEqualToView(_headImgv)
    .leftSpaceToView(_headImgv, 9)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _phoneLabel.sd_layout
    .topSpaceToView(label, 14)
    .leftEqualToView(label)
    .autoHeightRatio(0);
    [_phoneLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _nameLabel.sd_layout
    .centerYEqualToView(_phoneLabel)
    .leftSpaceToView(_phoneLabel, 56)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(_nameLabel)
    .rightSpaceToView(self.contentView, 21)
    .widthIs(14)
    .heightIs(14);
    
    line.sd_layout
    .topSpaceToView(_phoneLabel, 13)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20)
    .heightIs(1);
    [self setupAutoHeightWithBottomView:line bottomMargin:0];
}

@end
