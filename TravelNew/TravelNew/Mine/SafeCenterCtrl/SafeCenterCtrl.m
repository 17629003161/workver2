//
//  SafeCenterCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SafeCenterCtrl.h"
#import "SafeCenterCell.h"
#import "SafeLineCtrl.h"
#import "SafeContactManCtrl.h"
#import "SafeAlertCtrl.h"

@interface SafeCenterCtrl ()

@end

@implementation SafeCenterCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"安全中心" titleColor:nil];
    [self disabelPageRefresh];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.dataArray addObjectsFromArray:@[@[@"safe_line",@"安全专线",@"使用中遇到的特殊情况请咨询"],
                                          @[@"safe_contact_men",@"安全联系人",@"如遇险情，请及时联系安全联系人"],
                                          @[@"safe_alert",@"一键报警",@"遭遇危险时用于求助联系人或警察"],
                                          ]];
    [self.tableView registerClass:[SafeCenterCell class] forCellReuseIdentifier:@"SafeCenterCell"];
    self.tableView.backgroundColor = [UIColor clearColor];
   // self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableHeaderView = [self makeHeaderView];
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}


- (void)layout{
    UIImageView *imgv = ImageViewWithImage(@"safe_center");
    [self.view addSubview:imgv];
    [self.view bringSubviewToFront:self.tableView];
    imgv.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(250);
    
    self.tableView.sd_layout
    .topSpaceToView(self.view, 200)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SafeCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SafeCenterCell"];
    NSArray *ary = self.dataArray[indexPath.row];
    cell.imgv.image = [UIImage imageNamed:ary[0]];
    cell.nameLabel.text = ary[1];
    cell.descLabel.text = ary[2];
    if(indexPath.row == self.dataArray.count-1){
        cell.line.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            SafeLineCtrl *vc = [SafeLineCtrl new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:{
            SafeContactManCtrl *vc = [SafeContactManCtrl new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{
            SafeAlertCtrl *vc = [SafeAlertCtrl new];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }

}


- (UIView *)makeHeaderView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    view.frame = CGRectMake(0, 0, kScreenWidth, 20);
    UIView *view1 = [UIView new];
    view1.frame = view.bounds;
    view1.backgroundColor = [UIColor whiteColor];
    [view1 makeTopAngleSize:CGSizeMake(10,10) bounds:CGRectMake(0, 0, kScreenWidth, 20)];
    [view addSubview:view1];
    return view;
}

@end
