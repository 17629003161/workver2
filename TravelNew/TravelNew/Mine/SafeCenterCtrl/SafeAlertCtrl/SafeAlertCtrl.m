//
//  SafeAlertCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SafeAlertCtrl.h"
#import "LKMessageDialog.h"

@interface SafeAlertCtrl ()

@end

@implementation SafeAlertCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF3F3F3);
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)layout{
    UIView *topView = [self makeTopView];
    UIImageView *imgv = ImageViewWithImage(@"alert_convence");
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"一键报警" font:LKSystemFont(17) cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(alertIsClicked:)];
    [self.view sd_addSubviews:@[topView,imgv,btn]];
    topView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(200+SAFEVIEWTOPMARGIN);
    
    imgv.sd_layout
    .topSpaceToView(topView, 22)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(292);
    
    btn.sd_layout
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .bottomSpaceToView(self.view, 30)
    .widthIs(kScreenWidth-56)
    .heightIs(44);
}

- (void)alertIsClicked:(id)sender
{
    LKMessageDialog *dlg = [LKMessageDialog createMessageDialogWithTitle:@"确认拨打110报警吗？"
                                                                 content:@"虚假报警将承担法律责任，处以拘留，谨慎使用"
                                                            leftBtnTitle:@"再想想"
                                                           rightBtnTitle:@"确认拨打"];
    dlg.frame = CGRectMake(0, 0, 270, 160);
    dlg.okBlock = ^{
        [Utils callNumber:@"110"];
    };
    [dlg show];
    
}

- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)phoneIsClicked:(UITapGestureRecognizer *)reg
{
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"17629003161"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (UIImageView *)makeTopView{
    UIImageView *imgv = ImageViewWithImage(@"safe_line_bg");
    imgv.userInteractionEnabled = YES;
    UIImageView *arrow = ImageViewWithImage(@"left_arrow_black");
    [arrow addTapGestureTarget:self action:@selector(backIsClicked:)];
    UILabel *label = [Utils createBoldLabelWithTitle:@"一键报警" titleColor:HexColor(0x232323) fontSize:24];
    [label addTapGestureTarget:self action:@selector(phoneIsClicked:)];
    UILabel *label2 = [Utils createLabelWithTitle:@"虚假报警将承担法律责任，处以拘留，谨慎使用\n报警后，行程将同步分享至警方和安全联系人" titleColor:HexColor(0x4A4A4A) fontSize:14];
    label2.numberOfLines = 0;
    [imgv sd_addSubviews:@[arrow,label,label2]];
    
    arrow.sd_layout
    .topSpaceToView(imgv, 32+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(imgv, 14)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .topSpaceToView(arrow, 25)
    .leftSpaceToView(imgv, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label, 7)
    .leftSpaceToView(imgv, 20)
    .rightSpaceToView(imgv, 20)
    .autoHeightRatio(0);
    return imgv;
}



@end
