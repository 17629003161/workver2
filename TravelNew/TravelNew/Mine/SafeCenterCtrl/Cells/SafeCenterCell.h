//
//  SafeCenterCell.h
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SafeCenterCell : BaseCustomTableViewCell
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UIView *line;
@end

NS_ASSUME_NONNULL_END
