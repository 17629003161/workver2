//
//  SafeCenterCell.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SafeCenterCell.h"

@implementation SafeCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configViews
{
    _imgv = ImageViewWithImage(@"");
    _nameLabel = [Utils createBoldLabelWithTitle:@"安全专线" titleColor:HexColor(0x323232) fontSize:17];
    _descLabel = [Utils createLabelWithTitle:@"#323232" titleColor:HexColor(0x999999) fontSize:13];
    UIImageView *arrow = ImageViewWithImage(@"arriw_black_right");
    _line = [UIView new];
    _line.backgroundColor = HexColor(0xEDEDED);
    [self.contentView sd_addSubviews:@[_imgv,_nameLabel,_descLabel,arrow,_line]];
    _imgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 30)
    .widthIs(52)
    .heightIs(52);
    
    _nameLabel.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(_imgv, 12)
    .rightSpaceToView(self.contentView, 12)
    .autoHeightRatio(0);
    
    _descLabel.sd_layout
    .topSpaceToView(_nameLabel, 8)
    .leftEqualToView(_nameLabel)
    .rightSpaceToView(self.contentView, 12)
    .autoHeightRatio(0);
    
    arrow.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 16)
    .widthIs(6)
    .heightIs(10);
    
    _line.sd_layout
    .leftEqualToView(_imgv)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .heightIs(1);
}

@end
