//
//  MyNoticeCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyNoticeCtrl.h"
#import "MyNoticeCell.h"
#import "MainRecommenderCtrl.h"

@interface MyNoticeCtrl ()

@end

@implementation MyNoticeCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的关注" titleColor:nil];
    self.tableView.backgroundColor = HexColor(0xF3F3F3);
    [self.tableView registerClass:[MyNoticeCell class] forCellReuseIdentifier:@"MyNoticeCell"];
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/attention/selectAttentionUserList"
                          para:@{
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"查询关注列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(MyNoticeDataEntity.class);
    return name;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    MyNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyNoticeCell"];
    MyNoticeDataEntity *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.followBlock = ^(MyNoticeDataEntity * _Nonnull model) {
        weakSelf.dataArray[indexPath.row] = model;
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.dataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyNoticeDataEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = entity.userId;
    [self.navigationController pushViewController:vc animated:YES];
    
}



@end
