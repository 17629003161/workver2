//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "MyNoticeDataEntity.h"

@implementation MyNoticeDataEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.style  = [json objectForKey:@"style"];
		self.userId  = [json objectForKey:@"userId"];
		self.userNickname  = [json objectForKey:@"userNickname"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.style forKey:@"zx_style"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.style = [aDecoder decodeObjectForKey:@"zx_style"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"style : %@\n",self.style];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	
    return result;
}

@end
