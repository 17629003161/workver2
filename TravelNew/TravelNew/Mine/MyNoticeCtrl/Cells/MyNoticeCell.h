//
//  MyNoticeCell.h
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "MyNoticeDataEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyNoticeCell : BaseCustomTableViewCell
@property(nonatomic,strong) MyNoticeDataEntity *model;
@property(nonatomic,copy) void (^followBlock)(MyNoticeDataEntity *);
@end

NS_ASSUME_NONNULL_END
