//
//  MyNoticeCell.m
//  TravelNew
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyNoticeCell.h"

@interface MyNoticeCell()

@property(nonatomic,strong) UIImageView *headImgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *addressLabel;
@property(nonatomic,strong) UIButton *btn;

@end

@implementation MyNoticeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setModel:(MyNoticeDataEntity *)model
{
    _model = model;
    [self.headImgv sd_setImageWithURL:[NSURL URLWithString:model.headerImage]];
    self.nameLabel.text = model.userNickname;
    self.addressLabel.text = model.userNickname;
    if([model.style integerValue]==1){
        self.btn.selected = YES;
    }else{
        self.btn.selected = NO;
    }
    [self.nameLabel updateLayout];
    [self.addressLabel updateLayout];
}


- (void)btnIsClicked:(id)sender
{
    [self AddFollow:self.model.userId];
}

- (void)AddFollow:(NSNumber *)userId{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/attention/attentionUser"
                          para:@{@"userId":userId,
                                 @"isAttention":self.model.style
                          }
                   description:@"推荐官关注"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            BOOL isFollow = [[json objectForKey:@"data"] boolValue];
            if(isFollow){
                weakSelf.model.style = @(1);
            }else{
                weakSelf.model.style = @(0);
            }
            [self setModel:weakSelf.model];
            if(weakSelf.followBlock){
                weakSelf.followBlock(self.model);
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}



- (void)configViews
{
    self.contentView.backgroundColor = HexColor(0xF3F3F3);
    _headImgv = ImageViewWithImage(@"header_image");
    _headImgv.layer.cornerRadius = 25.0;
    _headImgv.layer.masksToBounds = YES;
    _nameLabel = [Utils createLabelWithTitle:@"Ahmad Nazeri" titleColor:HexColor(0x333333) fontSize:16];
    _addressLabel = [Utils createLabelWithTitle:@"西安本地人" titleColor:HexColor(0x333333) fontSize:13];
    _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    _btn.titleLabel.font = LKSystemFont(13);
    [_btn setTitle:@"关注TA" forState:UIControlStateNormal];
    [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn setBackgroundImage:[UIImage imageWithColor:HexColor(0x1E69FF)] forState:UIControlStateNormal];
    [_btn setTitle:@"已关注" forState:UIControlStateSelected];
    [_btn setTitleColor:HexColor(0x333333) forState:UIControlStateSelected];
    [_btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
    _btn.layer.cornerRadius = 1.0;
    [_btn addTarget:self action:@selector(btnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView sd_addSubviews:@[_headImgv,_nameLabel,_addressLabel,_btn]];
    _headImgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 17)
    .widthIs(50)
    .heightIs(50);
    
    _nameLabel.sd_layout
    .topSpaceToView(self.contentView, 23)
    .leftSpaceToView(_headImgv, 8)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _addressLabel.sd_layout
    .topSpaceToView(_nameLabel, 3)
    .leftEqualToView(_nameLabel)
    .autoHeightRatio(0);
    [_addressLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _btn.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 16)
    .widthIs(82)
    .heightIs(28);
}

@end
