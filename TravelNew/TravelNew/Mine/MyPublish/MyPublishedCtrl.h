//
//  MyPublishedCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "MyPublishedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyPublishedCtrl : LKBaseTableViewController
@property (nonatomic, copy) void(^onSelectBlock)(MyPublishedModel *);
@end

NS_ASSUME_NONNULL_END
