//
//  MyPublishedCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyPublishedCtrl.h"
#import "MyPublishedCell.h"
#import "EMChatViewController.h"
#import "AppointmentModel.h"
#import "AllRecommenderCtrl.h"

@interface MyPublishedCtrl ()

@end

@implementation MyPublishedCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我的发布" titleColor:nil];
    [self.tableView registerClass:[MyPublishedCell class] forCellReuseIdentifier:@"MyPublishedCell"];
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    self.tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kStatusBarHeight-kNavigationHeight);
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/requirement/getMyOrders"
                          para:@{
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"我的发布"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(MyPublishedModel.class);
    return name;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyPublishedModel *model = [self.dataArray objectAtIndex:indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[MyPublishedCell class] contentViewWidth:kScreenWidth];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyPublishedModel *model = [self.dataArray objectAtIndex:indexPath.section];
    if(self.onSelectBlock){
        self.onSelectBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    view.backgroundColor = HexColor(0xf9f9f9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyPublishedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPublishedCell"];
    MyPublishedModel *model = [self.dataArray objectAtIndex:indexPath.section];
    cell.model = model;
    LKWeakSelf
    cell.cancleBlock = ^(MyPublishedModel * _Nonnull model) {
        LKMessageDialog *dlg = [LKMessageDialog createMessageDialogWithTitle:@"取消定单"
                                                                      content:@"您确认要取消订单吗？根据《订单退改规则》，取消此订单将扣除您订单金额的百分之三十"
                                                                 leftBtnTitle:@"再想想" rightBtnTitle:@"确定"];
        dlg.frame = CGRectMake(0, 0, 270, 140);
        dlg.okBlock = ^{
            [weakSelf cancleOrder:model];
        };
        [dlg show];
    };
    cell.checkBlock = ^(MyPublishedModel * _Nonnull model) {
        AllRecommenderCtrl *vc = [AllRecommenderCtrl new];
        [self.navigationController pushViewController:vc animated:YES];
    };
    cell.navigationBlock = ^(MyPublishedModel * _Nonnull model) {
        LKWeakSelf
        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
        [weakSelf doNavigationWithEndLocation:endLocation];
    };
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 16)];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}

- (void)cancleOrder:(MyPublishedModel *)model
{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/requirement/cancelRequirement"
                          para:@{@"ltRequirementInfoId":model.ltRequirementInfoId,
                                 @"message":@"未付款"}
                   description:@"work-取消订单"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:@"取消成功"];
            [weakSelf loadFirstPage];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

@end
