//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "MyPublishedModel.h"

@implementation MyPublishedModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.state  = [json objectForKey:@"state"];
		self.travelTime  = [json objectForKey:@"travelTime"];
		self.creationTime  = [json objectForKey:@"creationTime"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.referrerUserId  = [json objectForKey:@"referrerUserId"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.userId  = [json objectForKey:@"userId"];
		self.address  = [json objectForKey:@"address"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.ltRequirementInfoId  = [json objectForKey:@"ltRequirementInfoId"];
		self.serviceCount  = [json objectForKey:@"serviceCount"];
		self.easemobId  = [json objectForKey:@"easemobId"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.nickname  = [json objectForKey:@"nickname"];
		self.requirementDescription  = [json objectForKey:@"requirementDescription"];
		self.number  = [json objectForKey:@"number"];
		self.endTime  = [json objectForKey:@"endTime"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.state forKey:@"zx_state"];
	[aCoder encodeObject:self.travelTime forKey:@"zx_travelTime"];
	[aCoder encodeObject:self.creationTime forKey:@"zx_creationTime"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.referrerUserId forKey:@"zx_referrerUserId"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.ltRequirementInfoId forKey:@"zx_ltRequirementInfoId"];
	[aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
	[aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.nickname forKey:@"zx_nickname"];
	[aCoder encodeObject:self.requirementDescription forKey:@"zx_requirementDescription"];
	[aCoder encodeObject:self.number forKey:@"zx_number"];
	[aCoder encodeObject:self.endTime forKey:@"zx_endTime"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.state = [aDecoder decodeObjectForKey:@"zx_state"];
		self.travelTime = [aDecoder decodeObjectForKey:@"zx_travelTime"];
		self.creationTime = [aDecoder decodeObjectForKey:@"zx_creationTime"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.referrerUserId = [aDecoder decodeObjectForKey:@"zx_referrerUserId"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.ltRequirementInfoId = [aDecoder decodeObjectForKey:@"zx_ltRequirementInfoId"];
		self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
		self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.nickname = [aDecoder decodeObjectForKey:@"zx_nickname"];
		self.requirementDescription = [aDecoder decodeObjectForKey:@"zx_requirementDescription"];
		self.number = [aDecoder decodeObjectForKey:@"zx_number"];
		self.endTime = [aDecoder decodeObjectForKey:@"zx_endTime"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"state : %@\n",self.state];
	result = [result stringByAppendingFormat:@"travelTime : %@\n",self.travelTime];
	result = [result stringByAppendingFormat:@"creationTime : %@\n",self.creationTime];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"referrerUserId : %@\n",self.referrerUserId];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"ltRequirementInfoId : %@\n",self.ltRequirementInfoId];
	result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
	result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"nickname : %@\n",self.nickname];
	result = [result stringByAppendingFormat:@"requirementDescription : %@\n",self.requirementDescription];
	result = [result stringByAppendingFormat:@"number : %@\n",self.number];
	result = [result stringByAppendingFormat:@"endTime : %@\n",self.endTime];
	
    return result;
}

- (LKChatModel *)toChatModel{
    LKChatModel *chatModel = [[LKChatModel alloc] init];
    chatModel.ltRequirementInfoId = self.ltRequirementInfoId;    //需求ID
   // chatModel.type = ;                //订单类型（出行单or获取联系方式订单）
    chatModel.state = self.state;               //订单状态
    chatModel.cityid = self.cityid;                //发布单所在城市代码
    chatModel.creationTime = self.creationTime;        //订单创建时间
    chatModel.travelTime = self.travelTime;          //travelTime
    chatModel.endTime = self.endTime;             //订单结束时间
    chatModel.latitude = self.latitude;            //始发地坐标
    chatModel.longitude = self.longitude;
    chatModel.address = self.address;               //目标地点详细地点名称
    chatModel.number = self.number;              //出行人数
    chatModel.requirementDescription = self.requirementDescription;         //需求单描述
    chatModel.easemobId = self.easemobId;                 //创建需求单用户的即时通讯ID
    chatModel.userId = self.userId;                  //创建需求单用户ID
    chatModel.offerPriceId = @(0);             //报价id
    chatModel.offerPrice = @(0);          //报价金额
    chatModel.isPayed = @(NO);             //是否支付
    return chatModel;
}



@end
