//
//  MyPublishedCell.m
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyPublishedCell.h"

@interface MyPublishedCell()
@property(nonatomic,strong) UIImageView *imagv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *createTimeLabel;
@property(nonatomic,strong) UILabel *addrLabel;
@property(nonatomic,strong) UILabel *outTimeLabel;
@property(nonatomic,strong) UILabel *customerNumberLabel;
@property(nonatomic,strong) UILabel *demandLabel;

@end


@implementation MyPublishedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MyPublishedModel *)model{
    _model = model;
    if(!isNull(model.headerImage)){
        [self.imagv sd_setImageWithURL:[NSURL URLWithString:model.headerImage]];
    }else{
        self.imagv.image = [UIImage imageNamed:@"header_person"];
    }
    self.nameLabel.text = model.nickname;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.creationTime doubleValue]/1000;
    self.createTimeLabel.text = [date dateString];
    self.addrLabel.text = model.address;
    date.timeInterval = [model.travelTime doubleValue]/1000;
    NSString *startStr = [NSString stringWithFormat:@"%ld月%ld日 %02ld:%02ld",date.month,date.day,date.hour,date.minute];
    date.timeInterval = [model.endTime doubleValue]/1000;
    NSString *stopStr = [NSString stringWithFormat:@"%ld月%ld日 %02ld:%02ld",date.month,date.day,date.hour,date.minute];;
    self.outTimeLabel.text = [NSString stringWithFormat:@"%@ - %@",startStr,stopStr];
    self.customerNumberLabel.text = [NSString stringWithFormat:@"%ld", [model.number integerValue] ];
    self.demandLabel.text = model.requirementDescription;
    [self.createTimeLabel updateLayout];
    [self.addrLabel updateLayout];
    [self.outTimeLabel updateLayout];
    [self.customerNumberLabel updateLayout];
    [self.demandLabel updateLayout];
    [self.nameLabel updateLayout];
}

- (void)btnCancleIsClicked:(id)sender
{
    if(self.cancleBlock)
        self.cancleBlock(self.model);
}

- (void)btnCheckRecomandIsClicked:(id)sender
{
    if(self.checkBlock)
        self.checkBlock(self.model);
}

- (void)arrowIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.navigationBlock){
        self.navigationBlock(self.model);
    }
}


- (void)configViews{
    self.backgroundColor = HexColor(0xf9f9f9);
    UIView *view = [UIView new];
    view.layer.borderColor = HexColor(0xD8D7DF).CGColor;
    view.layer.borderWidth = 0.5;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 4.0;
    
    UIImageView *imgv = [UIImageView new];
    imgv.layer.cornerRadius = 15;
    imgv.layer.masksToBounds = YES;
    self.imagv = imgv;
    UILabel *nameLabel = [Utils createLabelWithTitle:@"SyrenaCC"
                                          titleColor:HexColor(0x333333)
                                            fontSize:14];
    self.nameLabel = nameLabel;
    UILabel *labelL1 = [Utils createLabelWithTitle:@"发布时间"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR1 = [Utils createLabelWithTitle:@"2020-08-12"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.createTimeLabel = labelR1;
    
    
    UILabel *labelL2 = [Utils createLabelWithTitle:@"始发地"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR2 = [Utils createLabelWithTitle:@"利君未来城二期"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    labelR2.textAlignment = NSTextAlignmentRight;
    self.addrLabel = labelR2;
    UIImageView *arrow = ImageViewWithImage(@"daohang");
    [arrow addTapGestureTarget:self action:@selector(arrowIsClicked:)];
    
    UILabel *labelL3 = [Utils createLabelWithTitle:@"出行时间"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR3 = [Utils createLabelWithTitle:@"7月3日周五9：00-7月5日周日15:00"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.outTimeLabel = labelR3;
    
    UILabel *labelL4 = [Utils createLabelWithTitle:@"乘车人数"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR4 = [Utils createLabelWithTitle:@"4"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.customerNumberLabel = labelR4;
    
    UIView *contentbgview = [UIView new];
    contentbgview.backgroundColor = HexColor(0xf8f9fb);
    UILabel *contentL = [Utils createLabelWithTitle:@"想去西安，游玩两天，包车出行。想去西安，游玩两天，包车出行。"
                                         titleColor:HexColor(0x444444)
                                           fontSize:14];
    contentL.numberOfLines = 0;
    self.demandLabel = contentL;
    [contentbgview addSubview:contentL];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setTitle:@"取消订单" forState:UIControlStateNormal];
    [btn1 setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    btn1.titleLabel.font = LKSystemFont(16);
    [btn1 setBackgroundImage:[UIImage imageWithColor:HexColor(0xf8f9fb)] forState:UIControlStateNormal];
    btn1.layer.cornerRadius = 4.0;
    [btn1 addTarget:self action:@selector(btnCancleIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    LKGadentButton *btn2 = [LKGadentButton blueButtonWhithTitle:@"查看推荐官" font:LKSystemFont(16)
                                    cornerRadius:4];
    [btn2 addTapGestureTarget:self action:@selector(btnCheckRecomandIsClicked:)];
    [self.contentView addSubview:view];
    [view sd_addSubviews:@[imgv,nameLabel,
                           labelL1,labelR1,
                             labelL2,labelR2,arrow,
                             labelL3,labelR3,
                             labelL4,labelR4,
                             contentbgview,
                             btn1,btn2]];
    
    imgv.sd_layout
    .topSpaceToView(view, 14)
    .leftSpaceToView(view, 16)
    .widthIs(30)
    .heightIs(30);
    
    nameLabel.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 10)
    .autoHeightRatio(0);
    [nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL1.sd_layout
    .topSpaceToView(imgv, 12)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelL1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR1.sd_layout
    .centerYEqualToView(labelL1)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelR1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL2.sd_layout
    .topSpaceToView(labelL1, 16)
    .leftEqualToView(labelL1)
    .autoHeightRatio(0);
    [labelL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(view, 18)
    .widthIs(14)
    .heightIs(14);
    
    labelR2.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(arrow, 4)
    .leftSpaceToView(labelL2, 8)
    .heightIs(18);
    
    labelL3.sd_layout
    .leftEqualToView(labelL2)
    .topSpaceToView(labelR2, 15)
    .autoHeightRatio(0);
    [labelL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR3.sd_layout
    .rightEqualToView(labelR2)
    .centerYEqualToView(labelL3)
    .autoHeightRatio(0);
    [labelR3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL4.sd_layout
    .leftEqualToView(labelL3)
    .topSpaceToView(labelL3, 15)
    .autoHeightRatio(0);
    [labelL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR4.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL4)
    .autoHeightRatio(0);
    [labelR4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(contentbgview, 12)
    .rightSpaceToView(contentbgview, 10)
    .leftSpaceToView(contentbgview, 10)
    .autoHeightRatio(0);
    
    contentbgview.sd_layout
    .topSpaceToView(labelL4, 11)
    .leftEqualToView(labelL4)
    .rightEqualToView(labelR4);
    [contentbgview setupAutoHeightWithBottomViewsArray:@[contentL] bottomMargin:12];
    
    btn1.sd_layout
    .topSpaceToView(contentbgview, 12)
    .leftEqualToView(contentbgview)
    .widthIs(144)
    .heightIs(36);
    
    btn2.sd_layout
    .centerYEqualToView(btn1)
    .rightEqualToView(contentbgview)
    .widthIs(144)
    .heightIs(36);
    
    
    view.sd_layout
    .topSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15);
    
    [view setupAutoHeightWithBottomViewsArray:@[btn1,btn2] bottomMargin:16];
    [self setupAutoHeightWithBottomViewsArray:@[view] bottomMargin:10];
    
}

@end
