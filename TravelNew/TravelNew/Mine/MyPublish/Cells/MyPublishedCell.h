//
//  MyPublishedCell.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "MyPublishedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyPublishedCell : BaseCustomTableViewCell

@property(nonatomic,strong) MyPublishedModel *model;
@property(nonatomic,copy) void(^cancleBlock)(MyPublishedModel *);
@property(nonatomic,copy) void(^checkBlock)(MyPublishedModel *);
@property(nonatomic,copy) void(^navigationBlock)(MyPublishedModel *);


@end

NS_ASSUME_NONNULL_END
