//
//  UserGuideCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TakeIDPictureCtrl : LKBaseViewController
@property (nonatomic, copy) void (^didSelectImageBlock)(UIImage *);
@end

NS_ASSUME_NONNULL_END
