//
//  ShowImageView.m
//  自定义相机
//
//  Created by macbook on 16/9/6.
//  Copyright © 2016年 QIYIKE. All rights reserved.
//

#import "ShowImageView.h"

@interface ShowImageView ()

@property (nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIButton *btnSave;
@property(nonatomic,strong) UIButton *btnCancle;

@end

@implementation ShowImageView


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self layout];
    }
    return self;
}

- (void)layout{
    _imageView = [UIImageView new];
    _btnSave = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnSave setTitle:@"选择" forState:UIControlStateNormal];
    [_btnSave setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnSave addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    _btnCancle = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [_btnCancle setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnCancle addTarget:self action:@selector(cancle:) forControlEvents:UIControlEventTouchUpInside];
    [self sd_addSubviews:@[_imageView,_btnSave,_btnCancle]];
    _imageView.sd_layout
    .centerYEqualToView(self)
    .centerXEqualToView(self)
    .widthIs(kScreenWidth)
    .heightIs(240);
    
    CGFloat gap = 30;
    _btnSave.sd_layout
    .topSpaceToView(_imageView, 40)
    .rightSpaceToView(self, (kScreenWidth - kScreenWidth/2.0-gap-80))
    .widthIs(80)
    .heightIs(40);
    
    _btnCancle.sd_layout
    .topSpaceToView(_imageView, 40)
    .leftSpaceToView(self, kScreenWidth/2.0-gap-80)
    .widthIs(80)
    .heightIs(40);

}


+ (instancetype)showImageView
{
    return [[ShowImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)] ;
}


- (void)save:(id)sender {
    self.didClickSaveBtn();
}

- (void)cancle:(id)sender {
    self.didCancleSaveBtn();
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    _imageView.image = image;
}

@end
