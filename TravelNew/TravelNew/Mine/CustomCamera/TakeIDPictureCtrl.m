//
//  UserGuideCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "TakeIDPictureCtrl.h"
#import "TakeAPictureView.h"
#import "ShowImageView.h"

@interface TakeIDPictureCtrl ()
@property (strong, nonatomic) TakeAPictureView *cameraView;
@property (strong, nonatomic) ShowImageView *showImageView;
@property (strong, nonatomic) UIView *bottmView;
@end

@implementation TakeIDPictureCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    LKWeakSelf
    // Do any additional setup after loading the view.
    self.bottmView = [self createBottomView];
    self.cameraView = [[TakeAPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-200)];
    self.cameraView.getImage = ^(UIImage *image){
        weakSelf.cameraView.hidden = YES;
        weakSelf.bottmView.hidden = YES;
        weakSelf.showImageView.image = image;
    };
    [self.view sd_addSubviews:@[self.cameraView,self.bottmView]];
    
    self.bottmView.sd_layout
    .topSpaceToView(self.cameraView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (UIView *)createBottomView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"拍照" font:LKSystemFont(14) cornerRadius:2];
    [btn addTapGestureTarget:self action:@selector(takeAPicker:)];
    LKGadentButton *btn2 = [LKGadentButton blueButtonWhithTitle:@"切换" font:LKSystemFont(14) cornerRadius:2];
    [btn2 addTapGestureTarget:self action:@selector(change:)];
    [view sd_addSubviews:@[btn,btn2]];
    CGFloat gap = 30;
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, (kScreenWidth - kScreenWidth/2.0-gap-80))
    .widthIs(80)
    .heightIs(40);
    
    btn2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, kScreenWidth/2.0-gap-80)
    .widthIs(80)
    .heightIs(40);
    return view;
}

- (ShowImageView *)showImageView
{
    if (!_showImageView) {
        _showImageView = [ShowImageView showImageView];
        
        LKWeakSelf
        _showImageView.didCancleSaveBtn = ^ (void) {
            [weakSelf.view bringSubviewToFront:weakSelf.cameraView];
            [weakSelf.view bringSubviewToFront:weakSelf.bottmView];
            weakSelf.cameraView.hidden = NO;
            weakSelf.bottmView.hidden = NO;
        };
        
        _showImageView.didClickSaveBtn = ^ (void) {
            // 保存之后的操作
            DLog(@"用户选定这张图片");
            // 保存到本地
          //  [weakSelf.cameraView writeToSavedPhotos];
            if(weakSelf.didSelectImageBlock){
                weakSelf.didSelectImageBlock(weakSelf.showImageView.image);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        
        [self.view addSubview:_showImageView];
    }
    return _showImageView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.cameraView startRunning];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.cameraView stopRunning];
}

// 拍照
- (void)takeAPicker:(id)sender {
    [self.cameraView takeAPicture];
}

// 切换前置后置镜头
- (void)change:(UIButton *)sender {
    
    [self.cameraView setFrontOrBackFacingCamera:sender.selected];
    sender.selected = !sender.selected;
    
}

@end
