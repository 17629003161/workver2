//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LKChatModel.h"

@interface AppointmentModel : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *state;
@property (nonatomic,strong) NSNumber *travelTime;
@property (nonatomic,strong) NSNumber *creationTime;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,strong) NSNumber *referrerUserId;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,copy) NSNumber *ltRequirementInfoId;
@property (nonatomic,strong) NSNumber *serviceCount;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *requirementDescription;
@property (nonatomic,strong) NSNumber *number;
@property (nonatomic,strong) NSNumber *endTime;
@property (nonatomic,strong) NSNumber *offerPrice;          //报价
@property (nonatomic,strong) NSString *offerDetail;         //任务明细
@property (nonatomic,strong) NSNumber *offerId;             //报价id
@property (nonatomic,assign) NSTimeInterval timeStamp;      //消息发布时间

-(id)initWithJson:(NSDictionary *)json;

- (BOOL)isExceedTheTimeLimitInSenconds:(NSInteger)timeLimitSeconds;               //报价信息是否逾期
- (LKChatModel *)toChatModel;

@end

