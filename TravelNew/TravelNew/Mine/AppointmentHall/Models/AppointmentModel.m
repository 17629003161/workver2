//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "AppointmentModel.h"

@implementation AppointmentModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.state  = [json objectForKey:@"state"];
		self.travelTime  = [json objectForKey:@"travelTime"];
		self.creationTime  = [json objectForKey:@"creationTime"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.referrerUserId  = [json objectForKey:@"referrerUserId"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.userId  = [json objectForKey:@"userId"];
		self.address  = [json objectForKey:@"address"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.ltRequirementInfoId  = [json objectForKey:@"ltRequirementInfoId"];
		self.serviceCount  = [json objectForKey:@"serviceCount"];
		self.easemobId  = [json objectForKey:@"easemobId"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.nickname  = [json objectForKey:@"nickname"];
		self.requirementDescription  = [json objectForKey:@"requirementDescription"];
		self.number  = [json objectForKey:@"number"];
		self.endTime  = [json objectForKey:@"endTime"];
		
    }
    }
    return self;
}

- (BOOL)isExceedTheTimeLimitInSenconds:(NSInteger)timeLimitSeconds{
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0]; // 获取当前时间0秒后的时间
    NSTimeInterval curInterval = [date timeIntervalSince1970]*1000;
    NSInteger dlt = curInterval - self.timeStamp;
    if(dlt >= timeLimitSeconds*1000){
        return YES;
    }else{
        return NO;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.state forKey:@"zx_state"];
	[aCoder encodeObject:self.travelTime forKey:@"zx_travelTime"];
	[aCoder encodeObject:self.creationTime forKey:@"zx_creationTime"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.referrerUserId forKey:@"zx_referrerUserId"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.ltRequirementInfoId forKey:@"zx_ltRequirementInfoId"];
	[aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
	[aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.nickname forKey:@"zx_nickname"];
	[aCoder encodeObject:self.requirementDescription forKey:@"zx_requirementDescription"];
	[aCoder encodeObject:self.number forKey:@"zx_number"];
	[aCoder encodeObject:self.endTime forKey:@"zx_endTime"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.state = [aDecoder decodeObjectForKey:@"zx_state"];
		self.travelTime = [aDecoder decodeObjectForKey:@"zx_travelTime"];
		self.creationTime = [aDecoder decodeObjectForKey:@"zx_creationTime"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.referrerUserId = [aDecoder decodeObjectForKey:@"zx_referrerUserId"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.ltRequirementInfoId = [aDecoder decodeObjectForKey:@"zx_ltRequirementInfoId"];
		self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
		self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.nickname = [aDecoder decodeObjectForKey:@"zx_nickname"];
		self.requirementDescription = [aDecoder decodeObjectForKey:@"zx_requirementDescription"];
		self.number = [aDecoder decodeObjectForKey:@"zx_number"];
		self.endTime = [aDecoder decodeObjectForKey:@"zx_endTime"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"state : %@\n",self.state];
	result = [result stringByAppendingFormat:@"travelTime : %@\n",self.travelTime];
	result = [result stringByAppendingFormat:@"creationTime : %@\n",self.creationTime];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"referrerUserId : %@\n",self.referrerUserId];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"ltRequirementInfoId : %@\n",self.ltRequirementInfoId];
	result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
	result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"nickname : %@\n",self.nickname];
	result = [result stringByAppendingFormat:@"requirementDescription : %@\n",self.requirementDescription];
	result = [result stringByAppendingFormat:@"number : %@\n",self.number];
	result = [result stringByAppendingFormat:@"endTime : %@\n",self.endTime];
	
    return result;
}

- (LKChatModel *)toChatModel{
    LKChatModel *model = [[LKChatModel alloc] init];
    model.ltRequirementInfoId = self.ltRequirementInfoId;
    model.type = [NSNumber numberWithInt:1];
    model.state = self.state;
    model.cityid = self.cityid;
    model.creationTime = self.creationTime;
    model.travelTime = self.travelTime;
    model.endTime = self.endTime;
    model.latitude = self.latitude;
    model.longitude = self.longitude;
    model.address = self.address;
    model.number = self.number;
    model.requirementDescription = self.requirementDescription;
    model.easemobId = self.easemobId;
    model.userId = self.userId;
    model.offerPriceId = @(0);
    model.offerPrice = self.offerPrice;
    model.isPayed = @(NO);
    return model;
}



@end
