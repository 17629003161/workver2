//
//  AppointmentCell.h
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

#import "AppointmentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppointmentCell : BaseCustomTableViewCell
@property (nonatomic, copy) void(^OnContect)(AppointmentModel *);  //传递1个参数
@property(nonatomic,strong) AppointmentModel *model;
@property(nonatomic,copy) void(^navigationBlock)(AppointmentModel *);

@end

NS_ASSUME_NONNULL_END
