//
//  SecurityFundPaymentCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SecurityFundPaymentCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXPayDataEntity.h"
#import <WXApi.h>
#import "WechatManager.h"

@interface SecurityFundPaymentCtrl ()
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UIView *wxView;
@property(nonatomic,strong) UIView *zfbView;
@end

@implementation SecurityFundPaymentCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"缴纳保障金" titleColor:nil];
    self.view.backgroundColor = HexColor(0xf3f3f3);
    [self viewLayout];
    self.selectIndex = 1;
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2lf元",self.money];
    [self.moneyLabel updateLayout];
}
- (void)payIsFinished:(NSNotification *)notification
{
    [MBProgressHUD showMessage:@"充值成功"];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(payIsFinished:)
                                                 name:Notification_PayIsFinished
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Notification_PayIsFinished
                                                  object:nil];
}


-(void)setMoney:(CGFloat)money
{
    _money = money;
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2lf元",self.money];
    [self.moneyLabel updateLayout];
}



- (UIView *)createFuncViewImage:(NSString *)img title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0x6295fd).CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 2;
    UIImageView *imgv1 = ImageViewWithImage(@"wd_selecet");
    imgv1.tag = 100;
    imgv1.layer.cornerRadius = 2;
    UIImageView *imgv2 = ImageViewWithImage(img);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:15];
    [view sd_addSubviews:@[imgv1,
                           imgv2,label]];
    imgv1.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .widthIs(22)
    .heightEqualToWidth();
    
    imgv2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 35)
    .widthIs(24)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(imgv2, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

#pragma mark - 支付保障金
- (void)btnPayIsClicked:(id)sender{
    LKWeakSelf
    NSArray *ary = @[@"ali",@"wx"];
    Api *api = [Api apiPostUrl:@"work/deposit/cashDeposit"
                          para:@{@"amount":@((int)(self.money*100)),
                                 @"payChannel":ary[self.selectIndex],
                                 }
                   description:@"交保证金"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSString *plat = ary[weakSelf.selectIndex];
            if([plat isEqualToString:@"ali"]){
                NSString *orderString = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
                DLog(@"orderStr ---- = %@",orderString);
                // NOTE: 调用支付结果开始支付
                NSString *appScheme = @"alisdkdemo";
                [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
    //                DLog(@"reslut = %@",resultDic);
    //                DLog(@"%@",[resultDic objectForKey:@"memo"]);
                    if ([resultDic[@"resultStatus"]intValue] == 9000) {
                        DLog(@"成功");
                        [self.navigationController popViewControllerAnimated:YES];
                      //  [self checkOrderPay:orderNumber];
                    } else {
                        DLog(@"失败");
                    }
                }];
            }else if([plat isEqualToString:@"wx"]){
                NSString *jsonstr = [json objectForKey:@"data"];      //得到从服务器返回到订单信息签名串
                NSDictionary *dict = [NSString jsonStringToDictionary:jsonstr];
                WXPayDataEntity *model = [[WXPayDataEntity alloc] initWithJson:dict];
                PayReq *req  = [[PayReq alloc] init];
                 req.partnerId = model.partnerid;
                 req.prepayId = model.prepayid;
                 req.package = model.wx_package;
                 req.nonceStr = model.noncestr;
                 req.timeStamp = (UInt32)[model.timestamp longLongValue];
                 req.sign = model.paySign;
                //调起微信支付
                [WechatManager hangleWechatPayWith:req];
            }

        }else{
           [MBProgressHUD showMessage:@"请求支付数据出错"];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
    
    
}



- (void)viewLayout{
    UIView *topView = [self createTopView];
    UIView *bootomView = [self createBottomView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"立即支付" font:LKSystemFont(17)
                                   cornerRadius:12.5];
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    [self.view sd_addSubviews:@[topView,bootomView,btn]];
    topView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    bootomView.sd_layout
    .topSpaceToView(topView, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    btn.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.view, 30)
    .widthIs(320)
    .heightIs(45);
    
}

- (UIView *)createTopView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 162)];
    view.backgroundColor = HexColor(0xf3f3f3);
    UILabel *label1 = [Utils createLabelWithTitle:@"￥" titleColor:HexColor(0x272727) fontSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"980.00" titleColor:HexColor(0x272727) fontSize:30];
    self.moneyLabel = label2;
    UILabel *lable3 = [Utils createLabelWithTitle:@"平台保障金" titleColor:HexColor(0x272727) fontSize:16];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE4E5E6);
    [view sd_addSubviews:@[
                           label1,label2,
                           lable3]];
    label2.sd_layout
    .topSpaceToView(view, 40)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label1.sd_layout
    .centerYEqualToView(label2)
    .rightSpaceToView(label2, 4)
    .widthIs(20)
    .heightIs(28);
    
    lable3.sd_layout
    .leftEqualToView(label2)
    .topSpaceToView(label1, 4)
    .autoHeightRatio(0);
    [lable3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    [view setupAutoHeightWithBottomView:lable3 bottomMargin:40];

    return view;
}

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    UIButton *zfbBtn = [_zfbView viewWithTag:100];
    UIButton *wxBtn = [_wxView viewWithTag:100];
    [wxBtn setSelected:NO];
    [zfbBtn setSelected:NO];
    if(selectIndex==0){
        [zfbBtn setSelected:YES];
    }else if(selectIndex==1){
        [wxBtn setSelected:YES];
    }
}

- (void)selectIsClicked:(UITapGestureRecognizer *)reg{
    UIView *view = reg.view;
    if(view == _wxView){
        self.selectIndex = 1;
    }else if(view == _zfbView){
        self.selectIndex = 0;
    }
}


- (UIView *)createBottomView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 152)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *labe1 = [Utils createLabelWithTitle:@"提现方式"
                                      titleColor:HexColor(0x272727)
                                        fontSize:16];
    UIView *wxView = [self createSwitchView:@"weixinzhif" title:@"微信支付" subTitle:@"亿万用户的选择，更快更安全"];
    self.wxView = wxView;
    UIView *zfbView = [self createSwitchView:@"zfb_logo" title:@"支付宝" subTitle:@"10亿人都在用，真安全，更方便"];
    self.zfbView = zfbView;
    [view sd_addSubviews:@[labe1,
                           zfbView,
                           wxView]];
    labe1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [labe1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbView.sd_layout
    .topSpaceToView(labe1, 15)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    
    wxView.sd_layout
    .topSpaceToView(zfbView, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    [view setupAutoHeightWithBottomView:wxView bottomMargin:0];

    return view;
}

- (UIView *)createSwitchView:(NSString *)imageName title:(NSString *)title subTitle:(NSString *)subTitle{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [view addTapGestureTarget:self action:@selector(selectIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    UIImageView *imgv = ImageViewWithImage(imageName);
    UILabel *titleLabel = [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subTitleLabel = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn = [self createSelectBtn];
    btn.tag = 100;
    [view sd_addSubviews:@[line,
                           imgv,
                           titleLabel,subTitleLabel,
                           btn]];
    line.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 20)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(line)
    .widthIs(40)
    .heightIs(40);
    
    titleLabel.sd_layout
    .topEqualToView(imgv)
    .leftSpaceToView(imgv, 15)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleLabel.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel, 2)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(20)
    .heightIs(20);
    [subTitleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (UIButton *)createSelectBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_not_select"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_selected"] forState:UIControlStateSelected];
    return btn;
}


@end

