//
//  SecurityFundPaymentCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecurityFundPaymentCtrl : LKBaseViewController
@property(nonatomic,assign) CGFloat money;
@end

NS_ASSUME_NONNULL_END
