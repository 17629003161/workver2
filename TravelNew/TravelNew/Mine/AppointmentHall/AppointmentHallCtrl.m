//
//  AppointmentHallCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AppointmentHallCtrl.h"
#import "AppointmentCell.h"
#import "AppointmentModel.h"
#import "SecurityFundPaymentCtrl.h"
#import "EMChatViewController.h"
#import "UserGuideCtrl.h"

@interface AppointmentHallCtrl ()
@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) UIImageView *arrowImgv;
@property(nonatomic,assign) BOOL isPay;
@property(nonatomic,assign) BOOL isJudgeScroll;
@end

@implementation AppointmentHallCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"预约大厅" titleColor:nil];
    self.isPay = NO;
    [self.tableView registerClass:[AppointmentCell class] forCellReuseIdentifier:@"AppointmentCell"];
    self.tableView.backgroundColor = HexColor(0xf9f9f9);
    self.tableView.tableHeaderView = [self createHeaderView];
    self.tableView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self requreDemandNumber];
    [self loadFirstPage];
    [self checkBlankView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isJudgeScroll = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.isJudgeScroll = NO;
}

- (Api *)getListApi{
    UserInfo *user = [UserInfo shareInstance];
    if(user.selectCityId==nil)
        return nil;
    Api *api = [Api apiPostUrl:@"work/requirement/receivingOrders"
                          para:@{@"cityid":user.selectCityId,
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"接单大厅"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(AppointmentModel.class);
    return name;
}

-(void)requreDemandNumber
{
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"work/requirement/selectReqNum" para:@{@"cityid":user.selectCityId} description:@"查询城市需求总数"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSInteger num = [[json objectForKey:@"data"] integerValue];
            self.tipLabel.text = [NSString stringWithFormat:@"%@共%ld单待服务",user.selectCity,num];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentModel *model = [self.dataArray objectAtIndex:indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[AppointmentCell class] contentViewWidth:kScreenWidth];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    view.backgroundColor = HexColor(0xf9f9f9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AppointmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppointmentCell"];
    AppointmentModel *model = [self.dataArray objectAtIndex:indexPath.section];
    cell.model = model;
    LKWeakSelf
    cell.OnContect = ^(AppointmentModel * _Nonnull model) {
        [weakSelf requestSecurityFund:model];
    };
    cell.navigationBlock = ^(AppointmentModel * _Nonnull model) {
        LKWeakSelf
        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
        [weakSelf doNavigationWithEndLocation:endLocation];
    };
    return cell;
}

#pragma mark 判断是否需要缴纳保障金
//查看需缴纳的保证金
- (void)requestSecurityFund:(AppointmentModel *)model{
    if(self.isPay){
        if(model){
            [self doContact:model];
        }
        return;
    }
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/deposit/selectIsCashDeposit" para:@{} description:@"查看需缴纳的保证金"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            double money = [[json objectForKey:@"data"] doubleValue]/100.0;
            if(money > 0){
                //需要缴纳保障金
                self.isPay = NO;
                UserGuideCtrl *vc = [UserGuideCtrl new];
                vc.tipTitle = @"平台保证金";
                vc.photoName = @"SecurityFund";
                vc.buttonHide = NO;
                vc.buttonTitle = @"去缴纳";
                vc.funcBlock = ^{
                    SecurityFundPaymentCtrl *vc = [SecurityFundPaymentCtrl new];
                    vc.money = money;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                };
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                self.isPay = YES;
                if(model){
                    [weakSelf doContact:model];
                }
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@, %@",request.error,request.responseString);
        }];
}

- (void)doContact:(AppointmentModel *)model{
    [self orderAction:model];
    EMChatViewController *vc = [[EMChatViewController alloc] initWithConversationId:model.easemobId type:EMConversationTypeChat createIfNotExist:YES];
    vc.chatModel = [model toChatModel];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

//订单行为
- (void)orderAction:(AppointmentModel * _Nonnull) model{
    NSNumber *userId = nil;
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander]){
        userId = user.userId;
    }else{
        userId = model.referrerUserId;
    }
    LKLocationManager *manager = [LKLocationManager sharedManager];
    Api *api = [Api apiPostUrl:@"work/requirement/orderBehavior"
                          para:@{@"ltRequirementInfoId":model.ltRequirementInfoId,
                                 @"userId":userId,
                                 @"lon":@(manager.coordinate.longitude),
                                 @"lat":@(manager.coordinate.latitude)}
                   description:@"订单行为"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            BOOL isOk = [json objectForKey:@"data"];
            if(isOk){
                DLog(@"动作成功");
            }else{
                DLog(@"操作失败");
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@, %@",request.error, request.responseString);
        }];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 16)];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}

- (UIView *)createHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 48)];
    view.backgroundColor = HexColor(0xf9f9f9);
    UILabel *label = [Utils createLabelWithTitle:@"陕西省共283单待服务" titleColor:HexColor(0x999999) fontSize:15];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    label.frame = view.bounds;
    self.tipLabel = label;
    return view;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(!self.isPay && self.isJudgeScroll){
        CGFloat oldOffsetY = 0;
        CGFloat offsetY = scrollView.contentOffset.y;
        CGFloat dlt = (offsetY-oldOffsetY)>0 ? (offsetY-oldOffsetY):(oldOffsetY-offsetY);
        if(dlt > 320){
            self.isJudgeScroll = NO;
            oldOffsetY = offsetY;
            [self requestSecurityFund:nil];
        }
    }
}


@end
