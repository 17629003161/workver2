//
//  JiFenDetailCell.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "JiFenDetailCell.h"
@interface JiFenDetailCell()
@property(nonatomic,strong) UILabel *typeLabel;
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *dateTimeLabel;

@end

@implementation JiFenDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configViews
{
    _typeLabel = [Utils createBoldLabelWithTitle:@"转账" titleColor:HexColor(0x444444) fontSize:17];
    _moneyLabel = [Utils createLabelWithTitle:@"+5" titleColor:HexColor(0xE01A38) fontSize:17];
    _nameLabel = [Utils createLabelWithTitle:@"林亚都" titleColor:HexColor(0xB3B3B3) fontSize:14];
    _dateTimeLabel = [Utils createLabelWithTitle:@"2016-08-16 19:05" titleColor:HexColor(0xB3B3B3) fontSize:14];
    [self.contentView sd_addSubviews:@[_typeLabel,_moneyLabel,
                                       _nameLabel,_dateTimeLabel]];
    _typeLabel.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_typeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _moneyLabel.sd_layout
    .centerYEqualToView(_typeLabel)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_moneyLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _nameLabel.sd_layout
    .topSpaceToView(_typeLabel, 5)
    .leftEqualToView(_typeLabel)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _dateTimeLabel.sd_layout
    .centerYEqualToView(_nameLabel)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_dateTimeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

@end
