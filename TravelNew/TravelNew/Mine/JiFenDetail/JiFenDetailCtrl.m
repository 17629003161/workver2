//
//  JiFenDetailCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "JiFenDetailCtrl.h"
#import "JiFenDetailCell.h"

@interface JiFenDetailCtrl ()

@end

@implementation JiFenDetailCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"积分明细" titleColor:nil];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[JiFenDetailCell class] forCellReuseIdentifier:@"JiFenDetailCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 79;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JiFenDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JiFenDetailCell"];
    return cell;
}

@end
