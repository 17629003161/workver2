//
//  SeviceCenterAnswerCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SeviceCenterAnswerCtrl : LKBaseViewController
@property(nonatomic,strong) NSString *titlePrompt;
@property(nonatomic,strong) NSString *question;
@property(nonatomic,strong) NSString *answer;

@end

NS_ASSUME_NONNULL_END
