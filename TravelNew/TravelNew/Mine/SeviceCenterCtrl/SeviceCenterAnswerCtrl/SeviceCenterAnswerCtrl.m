//
//  SeviceCenterAnswerCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SeviceCenterAnswerCtrl.h"

@interface SeviceCenterAnswerCtrl ()
@property(nonatomic,strong) UILabel *questionLabel;
@property(nonatomic,strong) UILabel *answerLabel;

@end

@implementation SeviceCenterAnswerCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:self.titlePrompt titleColor:nil];
    self.view.backgroundColor = HexColor(0xF3F3F3);
    [self layout];
    self.question = _question;
    self.answer = _answer; 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)layout{
    self.questionLabel = [Utils createBoldLabelWithTitle:@"怎样预订服务？" titleColor:HexColor(0x2C2C2C) fontSize:16];
    self.answerLabel = [Utils createLabelWithTitle:@"dd" titleColor:HexColor(0x2C2C2C) fontSize:14];
    self.answerLabel.numberOfLines = 0;
    [self.view sd_addSubviews:@[self.questionLabel,self.answerLabel]];
    
    self.questionLabel.sd_layout
    .topSpaceToView(self.view, 18)
    .leftSpaceToView(self.view, 14)
    .autoHeightRatio(0);
    [self.questionLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.answerLabel.sd_layout
    .topSpaceToView(self.questionLabel, 14)
    .leftSpaceToView(self.view, 14)
    .rightSpaceToView(self.view, 14)
    .autoHeightRatio(0);
}

- (void)setQuestion:(NSString *)question
{
    _question = question;
    self.questionLabel.text = question;
    [self.questionLabel updateLayout];
}

- (void)setAnswer:(NSString *)answer
{
    _answer = answer;
    self.answerLabel.text = answer;
    [self.answerLabel updateLayout];
}


@end
