//
//  SeviceCenterCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SeviceCenterCtrl.h"
#import "SeviceCenterCell.h"
#import "SeviceCenterAnswerCtrl.h"
#import "EMChatViewController.h"

@interface SeviceCenterCtrl ()
@property(nonatomic,strong) NSArray *titleArray;
@property(nonatomic,strong) NSArray *array0;
@property(nonatomic,strong) NSArray *array1;
@property(nonatomic,strong) NSArray *array2;
@property(nonatomic,strong) NSArray *array3;

@property(nonatomic,strong) NSArray *answerArray0;
@property(nonatomic,strong) NSArray *answerArray1;
@property(nonatomic,strong) NSArray *answerArray2;
@property(nonatomic,strong) NSArray *answerArray3;
@property(nonatomic,strong) NSArray *answers;


@property(nonatomic,strong) UIView *leftView;
@property(nonatomic,assign) BOOL type0IsFold;
@property(nonatomic,assign) BOOL type1IsFold;
@property(nonatomic,assign) BOOL type2IsFold;
@property(nonatomic,assign) BOOL type3IsFold;

@property(nonatomic,strong) NSDictionary *contentDict;


@end

@implementation SeviceCenterCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _type0IsFold = YES;
    _type1IsFold = YES;
    _type2IsFold = YES;
    _type3IsFold = YES;
    
    [self readTextFromFile];
    
    self.view.backgroundColor = HexColor(0xF3F3F3);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[SeviceCenterCell class] forCellReuseIdentifier:@"SeviceCenterCell"];
    self.tableView.tableFooterView = [self makeFooterView];
    self.titleArray = @[@"订单问题",@"支付问题",@"安全问题",@"其他问题"];
    
    self.array0 = @[@"怎样预订服务？",
                    @"当推荐官让我私下交易怎么办？",
                    @"为什么给推荐官的消息发送不成功或未回复？",
                    @"我可以携带多少行李？",
                    @"如果游客与我协商，要求退款怎么办？",
                    @"通过“旅咖出行”“预订当地服务后我还可以在…",
                    @"行程前或行程中，无法联系到推荐官、推…"];
    self.answerArray0 = @[@"您可在首页“热门推荐官”里选择推荐官进行沟通，如果提供服务的推荐让您满意，您便可在聊天页面直接下单，付款确认订单后即可生效；您也可在“需求”页面直接发布您的要求与条件，推荐官们在“预约大厅”看到后，会及时与您联系，提供相应的报价与服务，您满意后，可直接预订。"
        ,@"为了保障您的安全和切身利益，我们平台为您提供爽约赔付，虚假赔付，意外保险，极速退款等多重保障，维护“旅咖出行”出行平台交易秩序、保护双方的隐私及安全，请您拒绝此类要求，坚持线上交易。如有这类情况发生，可向“旅咖出行”客服中心反馈，平台会保护您的隐私，并对推荐官作出相应的处罚。",@"请先检查您的网络连接。仔细查看您发送的信息中是否包含违法或违规的消息。请您按照“旅咖出行”出行的规则进行安全交易，谨慎诈骗等违法行为。成功发布需求后，会有不同的推荐官与您联系，如某推荐官迟迟未回复，可联系已回复的其他推荐官。",@"不同的推荐官提供的车辆，可容纳的行李数量是不同的，需要您与推荐官进行详细沟通。如不满意，可联系其他推荐官。",@"5.为维护平台交易规则，保护各方利益，请您与“旅咖出行”出行坚决遵守平台规则。“旅咖出行”会根据交易规则计算违约订金及结算金额并按时支付与您。\n在服务的过程当中，您如果发现服务与描述不符的情况，请您及时与平台的客服人员联系。我们会在第一时间核对情况，为了避免出现更多的纠纷，建议您将推荐官提供的车辆、服务人员进行拍照。我们会视情节根据“旅咖出行”的投诉流程标准进行调查处理。并在第一时间为您免费提供同类推荐官继续服务或订单的全额赔付，任选其一即可。",@"可以，但具体要看推荐官的时间安排。建议您与推荐官进行沟通，推荐官同意后，在您与推荐官的聊天界面，请推荐官填写增加服务订单，您成功支付即可。如果因推荐官申请而产生的服务价格上涨或直接支付现金与推荐官产生纠纷，”旅咖出行“不负任何责任。请注意：延长服务不保证推荐官一定有时间，延长服务价格也有可能变，具体以推荐官最终报价为准。",@"这些行为均属于当地推荐官的违约行为，如在行程前无法联系到推荐官，可在客服中心联系我们，平台将会与推荐官取得联系，如无法取得联系，您的款项将会原路全额退还； 如在行程与服务中，推荐官无法继续服务，请您及时在客服中心联系我们。客服人员立即核实情况，并在第一时间为您免费提供同类推荐官继续服务或订单的全额赔付，任选其一即可。"
    ];
    
    self.array1 = @[@"支付的流程是怎么样的？ ",
                    @"如何确定一趟出行的费用？",
                    @"可以用什么方式支持购买？",
                    @"订单退改的规则是怎样的？",
                    @"如取消订单，资金通过何种方式退款？"];
    
    self.answerArray1 =@[@"您与推荐官协商好订单详情，若推荐官提供的服务满意后，您可在推荐官发送的订单里进行付款。平台提供担保交易，在行程正常结束后，资金才会进入推荐官账户。所以请勿线下交易。",@"如果未超出服务限制，页面上的价格即是一趟出行的准确价格。对于超出产品限制的行程，您需要告知当地推荐官具体的细节，费用与当地推荐官进行协商，双方确认后，订单才能生效。",@"您可以通过支付宝（支持花呗、储蓄卡，信用卡），微信（支持信用卡，储蓄卡）进行付款。",@"【退订】\r\n预订后，已超出24小时订单退订？\r\n▪️使用日期前7天之前，无责取消；\n▪️使用日期前7（含）-1（含）天，扣除30%；\n▪️使用日期前1天以后，扣除60%；\n\n24小时内预订的订单退订？\n▪️付款30分钟内，无责取消；\n▪️付款30分钟（含）-1（含）小时内，扣除10%；\n▪️付款1小时-3小时（含）内，扣除30%；\n▪️付款3小时-5小时（含）内，扣除60%；\n▪️付款5小时后，扣除80%；\n\n针对所有订单退订，离行程开始前1小时，扣除100%。\n\n【改订】\n如您行程有变，请及时与推荐官沟通商议，修改订单开始时间。如推荐官提出不合理的要求时，可联系平台客服介入协调。",@"如您符合交易规则，无违约的情况，将原路返还至您支付的账户。如存在退改订单情况，将根据“旅咖出行”出行的退改规则，扣除违约金后，将余额原路返还至您的支付账户。预计在3个工作日内到账，款项具体的到账时间依第三方（如支付宝、微信）的处理时间为准。"];
    
    self.array2 = @[@"如果在用车中途发生了故障怎么办？",@"如果在旅行途中发生涉及人身安全的事故…"];
    self.answerArray2 = @[@"若车辆出现了故障，我们将为您及时提供替代车辆，尽可能地减少您的损失。",@"我们的平台为您提供了多重保障，您可在“安全中心”里面选择一键报警、安全联系人、7*24小时安全专线，最高额度200万的保险等服务。如果您在旅途中出现涉及人身安全的事故，我们强烈建议您第一时间寻求当地警方和医疗机构获得帮助，以获得及时的救助；平台收到求助后，也会在第一时间启动安全预案，如有需要也会联系当地警方。尽量为您提供力所能及的帮助，减少您的损失。"];
    
    self.array3 = @[@"推荐官保证金的作用？",@"平台收取佣金吗？",@"哪些人可以在“旅咖出行”平台发布服务？",@"如何使我成为热门推荐官或被推荐？",@"行程前需要做些什么准备？"];
    self.answerArray3 = @[@"平台先期不收取任何佣金，推荐官所赚即所得。在服务过程中，为杜绝不良情况的发生及推荐官服务规范化、安全化，平台将收取推荐官一定金额的保证金，规范推荐官的行为准则与服务质量。",@"用户发布需求，推荐官发布服务与提供服务都是免费的。",@"任何人都可以成为当地推荐官，只要你对当地熟悉，或者您有一辆车，或者您的服务能为出游的游客提供帮助，您都可以加入“旅咖出行”的平台。您可以免费注册和发布。",@"只要您发布的服务信息填写完整、照片清晰精美、服务预约率高、私信回复率高、游客点评率高等，您就有被编辑推上首页的机会。",@"建议您在服务前联系推荐官，确认行程及详细地点情况，以便能更好的为您服务。另外，建议您请在出行前检查好自己的随身物品，避免折返浪费时间。"];
    
    
    [self.dataArray addObjectsFromArray:@[_array0,_array1,_array2,_array3]];
    self.answers = @[_answerArray0,_answerArray1,_answerArray2,_answerArray3];
    
    [self layout];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"question.plist" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
       // NSLog(@"%@", data);//直接打印数据。
    [self disabelPageRefresh];
}


//读取文本文件
-(void)readTextFromFile
{
    NSError *error;
    NSString *jsonstr=[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"customer_service" ofType:@"json"] encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"{key:%@}",jsonstr);
    if (jsonstr==nil) {
        NSLog(@"---error--%@",[error localizedDescription]);
    }
    NSArray *ary = (NSArray *)[NSString jsonStringToDictionary:jsonstr];
    NSLog(@"%ld",ary.count);
    
    NSDictionary *dictionary = @{@"data":jsonstr};
    
    NSData*data=[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString*jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"jsonStr2==%@",jsonStr);
    
}



- (void)setType0IsFold:(BOOL)type0IsFold
{
    _type0IsFold = type0IsFold;
    UIView *view = [self.leftView viewWithTag:100];
    UIImageView *imgv = (UIImageView *)[view viewWithTag:1000];
    NSInteger height = 100;
    if(!_type0IsFold){
        height = 50 * _array0.count;
        imgv.transform = CGAffineTransformRotate(imgv.transform, M_PI);//旋转180
    }else{
        imgv.transform = CGAffineTransformIdentity;//还原
    }
    view.sd_layout
    .heightIs(height);
    [view updateLayout];
    [self.leftView updateLayout];
    [self.tableView reloadData];
}

- (void)setType1IsFold:(BOOL)type1IsFold
{
    _type1IsFold = type1IsFold;
    UIView *view = [self.leftView viewWithTag:101];
    UIImageView *imgv = (UIImageView *)[view viewWithTag:1000];
    NSInteger height = 100;
    if(!_type1IsFold){
        height = 50 * _array1.count;
        imgv.transform = CGAffineTransformRotate(imgv.transform, M_PI);//旋转180
    }else{
        imgv.transform = CGAffineTransformIdentity;//还原
    }
    view.sd_layout
    .heightIs(height);
    [view updateLayout];
    [self.leftView updateLayout];
    [self.tableView reloadData];
}

- (void)setType2IsFold:(BOOL)type2IsFold
{
    _type2IsFold = type2IsFold;
    UIView *view = [self.leftView viewWithTag:102];
    UIImageView *imgv = (UIImageView *)[view viewWithTag:1000];
    NSInteger height = 100;
    if(!_type2IsFold){
        height = 50 * _array2.count;
        imgv.transform = CGAffineTransformRotate(imgv.transform, M_PI);//旋转180
    }else{
        imgv.transform = CGAffineTransformIdentity;//还原
    }
    view.sd_layout
    .heightIs(height);
    [view updateLayout];
    [self.leftView updateLayout];
    [self.tableView reloadData];
}

- (void)setType3IsFold:(BOOL)type3IsFold
{
    _type3IsFold = type3IsFold;
    UIView *view = [self.leftView viewWithTag:103];
    UIImageView *imgv = (UIImageView *)[view viewWithTag:1000];
    NSInteger height = 100;
    if(!_type3IsFold){
        height = 50 *  _array3.count;
        imgv.transform = CGAffineTransformRotate(imgv.transform, M_PI);//旋转180
    }else{
        imgv.transform = CGAffineTransformIdentity;//还原
    }
    view.sd_layout
    .heightIs(height);
    [view updateLayout];
    [self.leftView updateLayout];
    [self.tableView reloadData];
}

- (void)layout{
    UIImageView *imgv = [self makeTopView];
    UIView *textView = [self makeTextView];
    [self.view sd_addSubviews:@[imgv,textView]];
    imgv.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(160+SAFEVIEWTOPMARGIN);
    
    textView.sd_layout
    .topSpaceToView(imgv, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(32);
    
    self.leftView = [self makeLeftView];
    [self.tableView addSubview:self.leftView];
    
    self.leftView.sd_layout
    .topEqualToView(self.tableView)
    .leftEqualToView(self.tableView)
    .widthIs(72);
    
    self.tableView.sd_layout
    .topSpaceToView(textView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (void)funcIsClicked:(UITapGestureRecognizer *)reg
{
    UIView *view = reg.view;
    NSInteger tag = view.tag;
    switch (tag) {
        case 100:
            self.type0IsFold = !_type0IsFold;
            break;
        case 101:
            self.type1IsFold = !_type1IsFold;
            break;
        case 102:
            self.type2IsFold = !_type2IsFold;
            break;
        case 103:
            self.type3IsFold = !_type3IsFold;
            break;
        default:
            break;
    }
}

- (UIView *)makeLeftView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIView *view1 = [self makeCategoryView:@"about_order" title:@"订单问题"];
    view1.tag = 100;
    [view1 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    UIView *view2 = [self makeCategoryView:@"about_pay" title:@"支付问题"];
    view2.tag = 101;
    [view2 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    UIView *view3 = [self makeCategoryView:@"about_safe" title:@"安全问题"];
    view3.tag = 102;
    [view3 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    UIView *view4 = [self makeCategoryView:@"other_question" title:@"其他问题"];
    view4.tag = 103;
    [view4 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    [view sd_addSubviews:@[view1,view2,view3,view4]];
    view1.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(100);
    view2.sd_layout
    .topSpaceToView(view1, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(100);
    view3.sd_layout
    .topSpaceToView(view2, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(100);
    view4.sd_layout
    .topSpaceToView(view3, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(100);
    [view setupAutoHeightWithBottomView:view4 bottomMargin:0];
    return view;
}


- (UIView *)makeCategoryView:(NSString *)imageName title:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = ImageViewWithImage(imageName);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x2C2C2C) fontSize:11];
    UIImageView *arrow = ImageViewWithImage(@"arrow_down");
    arrow.tag = 1000;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEAE9ED);
    [view sd_addSubviews:@[imageView,
                           label,
                           arrow,
                           line]];
    imageView.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(view, 13)
    .widthIs(31)
    .heightIs(31);
    
    label.sd_layout
    .topSpaceToView(imageView, 4)
    .centerXEqualToView(imageView)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .bottomSpaceToView(view, 10)
    .centerXEqualToView(view)
    .widthIs(11)
    .heightIs(7);
    
    line.sd_layout
    .leftEqualToView(view)
    .rightEqualToView(view)
    .bottomEqualToView(view)
    .heightIs(1.0);
    return view;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)makeTextView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"您可能想要了解以下问题" titleColor:HexColor(0x2c2c2c) fontSize:11];
    label.alpha = 0.4;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEAE9ED);
    [view sd_addSubviews:@[label,line]];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 19)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .leftEqualToView(view)
    .rightEqualToView(view)
    .bottomEqualToView(view)
    .heightIs(0.5);
    return view;
}

- (UIImageView *)makeTopView{
    UIImageView *imgv = ImageViewWithImage(@"Rectangle2");
    imgv.userInteractionEnabled = YES;
    UIImageView *arrow = ImageViewWithImage(@"return_white");
    [arrow addTapGestureTarget:self action:@selector(backIsClicked:)];
    UILabel *titleLabel = [Utils createLabelWithTitle:@"客服中心" titleColor:[UIColor whiteColor] fontSize:16];
    UIImageView *headerimgv = ImageViewWithImage(@"header_person");
    UIImage *image = [UIImage resizableImageWithName:@"CombinedShape112"];
    UIImageView *tipbg = [[UIImageView alloc] initWithImage:image];
    UILabel *tipLabel = [Utils createLabelWithTitle:@"您好欢迎光临旅咖出行服务大厅"
                                      titleColor:HexColor(0xFEFEFE)
                                        fontSize:12];
    UILabel *tipLabel2 = [Utils createLabelWithTitle:@"有啥疑问问我吧～"
                                      titleColor:HexColor(0xFEFEFE)
                                        fontSize:12];
    [tipbg sd_addSubviews:@[tipLabel,tipLabel2]];
    [imgv sd_addSubviews:@[arrow,titleLabel,headerimgv,tipbg]];
    [self.view addSubview:imgv];
    
    arrow.sd_layout
    .topSpaceToView(imgv, 32+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(imgv, 20)
    .widthIs(30)
    .heightIs(30);
    
    titleLabel.sd_layout
    .centerYEqualToView(arrow)
    .centerXEqualToView(imgv)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    headerimgv.sd_layout
    .topSpaceToView(imgv, 80+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(imgv, 30)
    .widthIs(60)
    .heightIs(60);
    
    tipbg.sd_layout
    .centerYEqualToView(headerimgv)
    .leftSpaceToView(headerimgv, 15)
    .widthIs(250)
    .heightIs(60);
    
    tipLabel.sd_layout
    .topSpaceToView(tipbg, 10)
    .leftSpaceToView(tipbg, 30)
    .rightSpaceToView(tipbg, 30)
    .autoHeightRatio(0);
    
    tipLabel2.sd_layout
    .topSpaceToView(tipLabel, 6)
    .leftSpaceToView(tipbg, 30)
    .rightSpaceToView(tipbg, 30)
    .autoHeightRatio(0);
    
    
    
    return imgv;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if(self.type0IsFold){
                return 2;
            }else{
                return _array0.count;
            }
            break;
        case 1:
            if(self.type1IsFold){
                return 2;
            }else{
                return _array1.count;
            }
            break;
        case 2:
            if(self.type2IsFold){
                return 2;
            }else{
                return _array2.count;
            }
            break;
        case 3:
            if(self.type3IsFold){
                return 2;
            }else{
                return _array3.count;
            }
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SeviceCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SeviceCenterCell"];
    NSArray *ary = self.dataArray[indexPath.section];
    cell.label.text = ary[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = self.titleArray[indexPath.section];
    NSArray *ary = self.dataArray[indexPath.section];
    NSArray *ansAry = self.answers[indexPath.section];
    NSString *question = ary[indexPath.row];
    SeviceCenterAnswerCtrl *vc = [SeviceCenterAnswerCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.titlePrompt = title;
    vc.question = question;
    vc.answer = ansAry[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)makeFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 93)];
    view.backgroundColor = HexColor(0xf3f3f3);
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"在线客服" font:LKSystemFont(17) cornerRadius:22];
    [btn addTapGestureTarget:self action:@selector(btnServiceClicked:)];
    [view addSubview:btn];
    btn.sd_layout
    .leftSpaceToView(view, 28)
    .rightSpaceToView(view, 28)
    .bottomSpaceToView(view, 18)
    .heightIs(44);
    return view;
}

- (void)btnServiceClicked:(UITapGestureRecognizer *)reg
{
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"message/customerService/getMessage" para:@{@"easemobId":user.easemobId} description:@""];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"%@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSString *easemobId = [json objectForKey:@"data"];
            EMChatViewController *chatController = [[EMChatViewController alloc] initWithConversationId:easemobId
                                                                                                   type:EMConversationTypeChat
                                                                                       createIfNotExist:YES];
            chatController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatController animated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.responseString);
        }];
}


@end
