//
//  SeviceCenterCell.m
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SeviceCenterCell.h"

@implementation SeviceCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configViews{
    self.label = [Utils createLabelWithTitle:@"怎样预订服务？" titleColor:HexColor(0x2c2c2c) fontSize:14];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEAE9ED);
    [self.contentView sd_addSubviews:@[self.label,line]];
    
    line.sd_layout
    .topEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 72)
    .bottomEqualToView(self.contentView)
    .widthIs(0.5);
    
    self.label.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 92)
    .rightSpaceToView(self.contentView, 19)
    .heightIs(20);
}

@end
