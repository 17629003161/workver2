//
//  SeviceCenterCell.h
//  TravelNew
//
//  Created by mac on 2020/11/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SeviceCenterCell : BaseCustomTableViewCell
@property(nonatomic,strong) UILabel *label;
@end

NS_ASSUME_NONNULL_END
