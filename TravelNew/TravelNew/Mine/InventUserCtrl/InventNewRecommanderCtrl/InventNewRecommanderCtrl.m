//
//  InventNewRecommanderCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "InventNewRecommanderCtrl.h"

@interface InventNewRecommanderCtrl ()

@end

@implementation InventNewRecommanderCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = HexColor(0xFFF5E7);
    self.scrollView.frame = self.view.bounds;
    [self layout];
}

- (void)layout{
    UIImageView *imgv = ImageViewWithImage(@"Invite_top9");
    imgv.userInteractionEnabled = YES;
    UIImageView *ruleImgv = ImageViewWithImage(@"group10");
    [imgv addSubview:ruleImgv];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"立即邀请" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"rectangle_bg10"] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 24;
    btn.layer.masksToBounds = YES;
    
    [self.scrollView sd_addSubviews:@[imgv,btn]];
    imgv.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(360);
    
    ruleImgv.sd_layout
    .topSpaceToView(imgv, 16)
    .rightEqualToView(imgv)
    .widthIs(68)
    .heightIs(28);
    
    btn.sd_layout
    .topSpaceToView(imgv, 30)
    .leftSpaceToView(self.scrollView, 40)
    .rightSpaceToView(self.scrollView, 40)
    .heightIs(48);
}

@end
