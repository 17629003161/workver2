//
//  InventUserCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "InventUserCtrl.h"
#import "InventNewUserCtrl.h"
#import "InventNewRecommanderCtrl.h"

@interface InventUserCtrl ()
@property(nonatomic,strong) NSMutableArray *btnArray;
@property(nonatomic,strong) UIView *indicateLine;
@property(nonatomic,assign) NSInteger selectedIndex;
@property(nonatomic,strong) NSMutableArray *viewsArray;
@end

@implementation InventUserCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"邀请新用户" titleColor:nil];
    [self layout];
}
-(NSMutableArray *)btnArray
{
    if(!_btnArray){
        _btnArray = [NSMutableArray new];
    }
    return _btnArray;
}
-(NSMutableArray *)viewsArray
{
    if(!_viewsArray){
        _viewsArray = [NSMutableArray new];
    }
    return _viewsArray;
}

- (void)layout{
    UIView *switchBar = [self createSwitchBar];
    [self.view sd_addSubviews:@[
                                switchBar]];
    switchBar.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(46);
    
    InventNewUserCtrl *vc1 = [InventNewUserCtrl new];
    [self.view addSubview:vc1.view];
    [self addChildViewController:vc1];
    [self.viewsArray addObject:vc1.view];
    
    vc1.view.sd_layout
    .topSpaceToView(switchBar, 12)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    InventNewRecommanderCtrl *vc2 = [InventNewRecommanderCtrl new];
    [self.view addSubview:vc2.view];
    [self addChildViewController:vc2];
    [self.viewsArray addObject:vc2.view];
    vc2.view.sd_layout
    .topSpaceToView(switchBar, 12)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    [self setSelectedIndex:0];
    
}

-(void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    for(UIView *view in self.viewsArray){
        view.hidden = YES;
    }
    for(UIButton *btn in self.btnArray){
        if(btn.tag == _selectedIndex){
            [btn setSelected:YES];
            CGPoint point = self.indicateLine.center;
            point.x = btn.center.x;
            self.indicateLine.center = point;
            UIView *view = self.viewsArray[btn.tag];
            view.hidden = NO;
        }else{
            [btn setSelected:NO];
        }
    }
    if(_selectedIndex==0){
        [self setTitle:@"邀请新用户" titleColor:nil];
    }else{
        [self setTitle:@"邀请推荐官" titleColor:nil];
    }
    
}


- (UIButton *)createSwitchButton:(NSString *)title
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x333333) forState:UIControlStateSelected];
    return btn;
}

- (void)switchBtnIsClicked:(id)sender
{
    UIButton *btn =  (UIButton *)sender;
    [self setSelectedIndex:btn.tag];
}

- (UIView *)createSwitchBar{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIButton *btn0 = [self createSwitchButton:@"邀请新用户"];
    [btn0 setSelected:YES];
    btn0.tag = 0;
    [btn0 addTarget:self action:@selector(switchBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnArray addObject:btn0];
    UIButton *btn1 = [self createSwitchButton:@"邀请推荐官"];
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(switchBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnArray addObject:btn1];
    
    UIView *indicateLine = [UIView new];
    indicateLine.backgroundColor = HexColor(0xFD7025);
    self.indicateLine = indicateLine;
    [view sd_addSubviews:@[btn0,btn1,
                           indicateLine,
                           ]];
    CGFloat gap = 10;
    CGFloat width = (kScreenWidth-3*gap)/2.0;
    btn0.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, gap)
    .widthIs(width)
    .heightIs(30);
    
    btn1.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, gap)
    .widthIs(width)
    .heightIs(30);
    
    indicateLine.sd_layout
    .centerXEqualToView(btn0)
    .bottomEqualToView(view)
    .widthIs(64)
    .heightIs(3);
    
    return view;
}

- (void)jiFenDetailBtnIsClicked:(UITapGestureRecognizer *)reg
{
//    JiFenDetailCtrl *vc = [JiFenDetailCtrl new];
//    [self.navigationController pushViewController:vc animated:YES];
}



@end
