//
//  MakeApartmentCtrl.m
//  TravelNew
//
//  Created by mac on 2021/2/2.
//  Copyright © 2021 lester. All rights reserved.
//

#import "MakeApartmentCtrl.h"

@interface MakeApartmentCtrl ()

@end

@implementation MakeApartmentCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xf9f9f9);
    [self setTitle:@"成为合伙人" titleColor:nil];
    [self configViews];
}

- (void)configViews{
    UIImageView *imagv = ImageViewWithImage(@"img_apartment4");
    UIView *dotView1 = [self creatdotView:@"零费用加盟，让你轻松进场"];
    UIView *dotView2 = [self creatdotView:@"负责本区域人员运营管理"];
    UIView *dotView3 = [self creatdotView:@"负责本区域市场调研及反馈"];
    UILabel *label = [Utils createLabelWithTitle:@"要求：需有营业执照，办公地点" titleColor:HexColor(0xFE9A53) fontSize:16];
    [self.view sd_addSubviews:@[imagv,
                                dotView1,
                                dotView2,
                                dotView3,
                                label]];
    imagv.sd_layout
    .topSpaceToView(self.view,11)
    .leftSpaceToView(self.view, 11)
    .rightSpaceToView(self.view, 11)
    .heightIs(237);
    
    dotView1.sd_layout
    .topSpaceToView(imagv, 11)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    dotView2.sd_layout
    .topSpaceToView(dotView1, 11)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    dotView3.sd_layout
    .topSpaceToView(dotView2, 11)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    label.sd_layout
    .topSpaceToView(dotView3, 11)
    .leftSpaceToView(self.view,33)
    .rightSpaceToView(self.view, 20)
    .autoHeightRatio(0);
    
}

- (UIView *)creatdotView:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *dot = ImageViewWithImage(@"dot_blue");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x999999) fontSize:14];
    [view sd_addSubviews:@[dot,label]];
    
    dot.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .widthIs(7)
    .heightIs(7);
    
    label.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(dot, 9)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}



@end
