//
//  JiFenExchangeCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "JiFenExchangeCtrl.h"

@interface JiFenExchangeCtrl ()

@end

@implementation JiFenExchangeCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *footerView = [Utils createNoDataView];
    footerView.frame = CGRectMake(0, 0, kScreenWidth, 440);
    self.tableView.tableFooterView = footerView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}


@end
