//
//  MyJiFenCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyJiFenCtrl.h"
#import "JiFenTaskCtrl.h"
#import "JiFenExchangeCtrl.h"
#import "JiFenDetailCtrl.h"

@interface MyJiFenCtrl ()
@property(nonatomic,strong) NSMutableArray *btnArray;
@property(nonatomic,strong) UIView *indicateLine;
@property(nonatomic,assign) NSInteger selectedIndex;
@property(nonatomic,strong) NSMutableArray *viewsArray;

@end

@implementation MyJiFenCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self layout];
}
-(NSMutableArray *)btnArray
{
    if(!_btnArray){
        _btnArray = [NSMutableArray new];
    }
    return _btnArray;
}
-(NSMutableArray *)viewsArray
{
    if(!_viewsArray){
        _viewsArray = [NSMutableArray new];
    }
    return _viewsArray;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)layout{
    UIView *topView = [self makeTopView];
    UIView *switchBar = [self createSwitchBar];
    [self.view sd_addSubviews:@[topView,
                                switchBar]];
    topView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(179);
    
    switchBar.sd_layout
    .topSpaceToView(topView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(46);
    
    JiFenTaskCtrl *vc1 = [JiFenTaskCtrl new];
    [self.view addSubview:vc1.view];
    [self addChildViewController:vc1];
    [self.viewsArray addObject:vc1.view];
    vc1.view.sd_layout
    .topSpaceToView(switchBar, 12)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    JiFenExchangeCtrl *vc2 = [JiFenExchangeCtrl new];
    [self.view addSubview:vc2.view];
    [self addChildViewController:vc2];
    [self.viewsArray addObject:vc2.view];
    vc2.view.sd_layout
    .topSpaceToView(switchBar, 12)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    [self setSelectedIndex:0];
    
}

-(void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    for(UIView *view in self.viewsArray){
        view.hidden = YES;
    }
    for(UIButton *btn in self.btnArray){
        if(btn.tag == _selectedIndex){
            [btn setSelected:YES];
            CGPoint point = self.indicateLine.center;
            point.x = btn.center.x;
            self.indicateLine.center = point;
            UIView *view = self.viewsArray[btn.tag];
            view.hidden = NO;
        }else{
            [btn setSelected:NO];
        }
    }
    
}


- (UIButton *)createSwitchButton:(NSString *)title
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(16);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
    [btn setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x4D86FD) forState:UIControlStateSelected];
    return btn;
}

- (void)switchBtnIsClicked:(id)sender
{
    UIButton *btn =  (UIButton *)sender;
    [self setSelectedIndex:btn.tag];
}

- (UIView *)createSwitchBar{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIButton *btn0 = [self createSwitchButton:@"积分任务"];
    [btn0 setSelected:YES];
    btn0.tag = 0;
    [btn0 addTarget:self action:@selector(switchBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnArray addObject:btn0];
    UIButton *btn1 = [self createSwitchButton:@"积分兑换"];
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(switchBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnArray addObject:btn1];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    
    UIView *indicateLine = [UIView new];
    indicateLine.backgroundColor = HexColor(0x4D86FD);
    self.indicateLine = indicateLine;
    [view sd_addSubviews:@[btn0,btn1,
                           line,
                           indicateLine,
                           ]];
    CGFloat gap = 10;
    CGFloat width = (kScreenWidth-3*gap)/2.0;
    btn0.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, gap)
    .widthIs(width)
    .heightIs(30);
    
    btn1.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, gap)
    .widthIs(width)
    .heightIs(30);
    
    indicateLine.sd_layout
    .centerXEqualToView(btn0)
    .bottomEqualToView(view)
    .widthIs(64)
    .heightIs(1);
    
    line.sd_layout
    .leftEqualToView(view)
    .rightEqualToView(view)
    .bottomEqualToView(view)
    .heightIs(0.5);
    return view;
}

- (void)jiFenDetailBtnIsClicked:(UITapGestureRecognizer *)reg
{
    JiFenDetailCtrl *vc = [JiFenDetailCtrl new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)backBtnIsClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)makeTopView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *bgimgv = ImageViewWithImage(@"bg_moneybag");
    UIImageView *backArrow = ImageViewWithImage(@"return_white");
    [backArrow addTapGestureTarget:self action:@selector(backBtnIsClicked:)];
    UILabel *titleLabel = [Utils createLabelWithTitle:@"我的积分" titleColor:[UIColor whiteColor] fontSize:16];
    UILabel *detailLabel = [Utils createLabelWithTitle:@"积分明细" titleColor:HexColor(0xDCE9FF) fontSize:14];
    [detailLabel addTapGestureTarget:self action:@selector(jiFenDetailBtnIsClicked:)];
    UILabel *scoreLabel = [Utils createBoldLabelWithTitle:@"862" titleColor:[UIColor whiteColor] fontSize:50];
    UILabel *label = [Utils createLabelWithTitle:@"积分" titleColor:[UIColor whiteColor] fontSize:16];
    [view sd_addSubviews:@[bgimgv,
                           backArrow,titleLabel,detailLabel,
                           scoreLabel,label]];
    bgimgv.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .bottomEqualToView(view);
    
    backArrow.sd_layout
    .topSpaceToView(view, 31+20)
    .leftSpaceToView(view, 20)
    .widthIs(30)
    .heightIs(30);
    
    titleLabel.sd_layout
    .centerYEqualToView(backArrow)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    detailLabel.sd_layout
    .centerYEqualToView(titleLabel)
    .rightSpaceToView(view, 14)
    .autoHeightRatio(0);
    [detailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    scoreLabel.sd_layout
    .topSpaceToView(titleLabel, 19)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [scoreLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label.sd_layout
    .leftSpaceToView(scoreLabel, 7)
    .bottomEqualToView(scoreLabel)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return  view;

}


@end
