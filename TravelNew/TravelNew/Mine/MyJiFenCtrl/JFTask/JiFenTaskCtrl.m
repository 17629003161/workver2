//
//  JiFenTaskCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "JiFenTaskCtrl.h"
#import "JiFenTaskCell.h"

@interface JiFenTaskCtrl ()

@end

@implementation JiFenTaskCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[JiFenTaskCell class] forCellReuseIdentifier:@"JiFenTaskCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.dataArray addObject:@[@"qiandao_jifen",@"每日签到",@"连续签到可获得更高积分",@"签到+5"]];
    [self.dataArray addObject:@[@"share_jifen",@"分享精彩",@"每天分享新精彩都会有30积分",@"分享"]];
    [self.dataArray addObject:@[@"order_jifen",@"完成订单",@"完成一笔订单送100积分",@"预约"]];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JiFenTaskCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JiFenTaskCell"];
    NSArray *array = [self.dataArray objectAtIndex:indexPath.row];
    cell.imgv.image = [UIImage imageNamed:array[0]];
    cell.nameLabel.text = array[1];
    cell.detailLabel.text = array[2];
    [cell.btn setTitle:array[3] forState:UIControlStateNormal];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

@end
