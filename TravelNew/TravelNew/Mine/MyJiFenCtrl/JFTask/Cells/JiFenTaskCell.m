//
//  JiFenTaskCell.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "JiFenTaskCell.h"

@implementation JiFenTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configViews
{
    UIImageView *imgv = ImageViewWithImage(@"order_jifen");
    self.imgv = imgv;
    UILabel *titleLabel = [Utils createLabelWithTitle:@"每日签到" titleColor:HexColor(0x323232) fontSize:17];
    self.nameLabel = titleLabel;
    UILabel *subTitleLabel = [Utils createLabelWithTitle:@"连续签到可获得更高积分" titleColor:HexColor(0x999999) fontSize:13];
    self.detailLabel = subTitleLabel;
    UIButton *btn = [self createBtn:@"签到+5"];
    self.btn = btn;
    [self.contentView sd_addSubviews:@[imgv,titleLabel,subTitleLabel,btn]];
    imgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 20)
    .widthIs(40)
    .heightIs(40);
    
    titleLabel.sd_layout
    .topEqualToView(imgv)
    .leftSpaceToView(imgv, 16)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleLabel.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel, 1)
    .autoHeightRatio(0);
    [subTitleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .centerYEqualToView(imgv)
    .rightSpaceToView(self.contentView, 19)
    .widthIs(60)
    .heightIs(28);
    
}

- (UIButton *)createBtn:(NSString *)title
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(13);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x4D92FD) forState:UIControlStateNormal];
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = [UIColor colorWithRed:77/255.0 green:146/255.0 blue:253/255.0 alpha:1.0].CGColor;
    btn.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    btn.layer.cornerRadius = 4;
    return btn;
}

@end
