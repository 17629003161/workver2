//
//  JiFenTaskCell.h
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface JiFenTaskCell : BaseCustomTableViewCell
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *detailLabel;
@property(nonatomic,strong) UIButton *btn;

@end

NS_ASSUME_NONNULL_END
