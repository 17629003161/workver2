//
//  MyAdviceCell.m
//  TravelNew
//
//  Created by mac on 2020/11/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyAdviceCell.h"

@implementation MyAdviceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configViews{
    self.titleLabel = [Utils createLabelWithTitle:@"附近推荐官" titleColor:HexColor(0x515151) fontSize:15];
    _arrow = ImageViewWithImage(@"arrow_right");
    [self.contentView sd_addSubviews:@[self.titleLabel,_arrow]];
    self.titleLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 14)
    .rightSpaceToView(self.contentView, 14)
    .autoHeightRatio(0);
    
    _arrow.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 12)
    .widthIs(8)
    .heightIs(14);
}

@end
