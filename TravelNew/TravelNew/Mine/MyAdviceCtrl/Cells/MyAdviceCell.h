//
//  MyAdviceCell.h
//  TravelNew
//
//  Created by mac on 2020/11/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyAdviceCell : BaseCustomTableViewCell
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIImageView *arrow;
@end

NS_ASSUME_NONNULL_END
