//
//  MyAdviceCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyAdviceCtrl.h"
#import "MyAdviceCell.h"
#import "MyAdviceInputCtrl.h"

@interface MyAdviceCtrl ()

@end

@implementation MyAdviceCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我有建议" titleColor:nil];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[MyAdviceCell class] forCellReuseIdentifier:@"MyAdviceCell"];
    [self.dataArray addObjectsFromArray:@[@"线上帮助",@"附近推荐官",@"分享精彩",@"登录注册",@"其他"]];
    [self disabelPageRefresh];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyAdviceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAdviceCell"];
    cell.titleLabel.text = self.dataArray[indexPath.row];
    return  cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    view.backgroundColor = HexColor(0xF3F3F3);
    UILabel *label = [Utils createLabelWithTitle:@"请选择问题发生的场景" titleColor:HexColor(0x666666) fontSize:13];
    [view addSubview:label];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 13)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyAdviceInputCtrl *vc = [MyAdviceInputCtrl new];
    vc.index = indexPath.row;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
