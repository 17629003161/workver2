#import "MyAdviceInputCtrl.h"
#import "EMTextView.h"
#define C_PhoteSize  ((kScreenWidth-80-24)/3.0)
@interface MyAdviceInputCtrl ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property(nonatomic,strong) NSMutableArray *imageviewArray;
@property (nonatomic,strong) NSMutableArray *imagePathArray;
@property (nonatomic,assign) NSInteger curImageIndex;
@property (nonatomic,strong) EMTextView *textView;

@end

@implementation MyAdviceInputCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"我有建议" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF3F3F3);
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.imagePathArray = [NSMutableArray new];
    self.curImageIndex = -1;
    
    [self layout];
    [self makeJsonstr];
    
}

- (NSString *)makeJsonstr{
    NSMutableArray *ary = [[NSMutableArray alloc] init];
    for(int i=0;i<self.imagePathArray.count;i++){
        NSString *url = self.imagePathArray[i];
        NSDictionary *dict = @{@"imageurl":url,@"sequence":@(i)};
        [ary addObject:dict];
    }
    NSData *data=[NSJSONSerialization dataWithJSONObject:ary options:NSJSONWritingPrettyPrinted error:nil];;
    NSString *jsonStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    DLog(@"jsonStr==%@",jsonStr);
    return jsonStr;
}

//上传图片
- (void)cosUploadImgae:(UIImage *)image{
    NSString *remoteDir = @"lvkauxing/img/";
    NSString* tempPath = QCloudTempFilePathWithExtension(@"png");
    QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
    // 存储桶名称，格式为 BucketName-APPID
    put.bucket = @"nlcx-1301827761";
    // 对象键，是对象在 COS 上的完整路径，如果带目录的话，格式为 "dir1/object1"
    put.object = [remoteDir stringByAppendingPathComponent:tempPath];
    //需要上传的对象内容。可以传入NSData*或者NSURL*类型的变量
    NSData *data = UIImagePNGRepresentation(image);
    put.body =  data;
    //监听上传进度
    [put setSendProcessBlock:^(int64_t bytesSent,
                                int64_t totalBytesSent,
                                int64_t totalBytesExpectedToSend) {
        //      bytesSent                   新增字节数
        //      totalBytesSent              本次上传的总字节数
        //      totalBytesExpectedToSend    本地上传的目标字节数
    }];

    LKWeakSelf
    //监听上传结果
    [put setFinishBlock:^(id outputObject, NSError *error) {
        //可以从 outputObject 中获取 response 中 etag 或者自定义头部等信息
        QCloudUploadObjectResult *result = (QCloudUploadObjectResult *)outputObject;
       // NSDictionary * result = (NSDictionary *)outputObject;
        DLog(@"file location = %@",result.location);
        [weakSelf.imagePathArray addObject:result.location];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showPictures];
        });
    }];

    [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
}

- (void)typeView:(UITapGestureRecognizer *)reg
{
    [self.textView resignFirstResponder];
}

- (void)layout{
    
    self.scrollView.frame = self.view.bounds;
    self.scrollView.backgroundColor = HexColor(0xF3F3F3);
    [self.view addSubview:self.scrollView];
    [self.scrollView addTapGestureTarget:self action:@selector(typeView:)];
    
    UIView *topView = [self makeTopView];
    [self.scrollView sd_addSubviews:@[topView]];
    
    UIView *contentView = [self createContentView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"提交"
                                           font:LKSystemFont(18)
                                   cornerRadius:25];
    [btn addTapGestureTarget:self action:@selector(onSubmit:)];
    [self.scrollView sd_addSubviews:@[topView,
                                      contentView,
                                      btn]];
    topView.sd_layout
    .topSpaceToView(self.scrollView, 20)
    .leftSpaceToView(self.scrollView, 20)
    .rightSpaceToView(self.scrollView, 20);
    
    contentView.sd_layout
    .topSpaceToView(topView, 20)
    .leftSpaceToView(self.scrollView, 20)
    .rightSpaceToView(self.scrollView, 20);
    
    btn.sd_layout
    .topSpaceToView(contentView, 56)
    .leftSpaceToView(self.scrollView, 28)
    .rightSpaceToView(self.scrollView, 28)
    .heightIs(44);
    
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight+300);
}


- (UIView *)createContentView{

    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:139/255.0 green:149/255.0 blue:169/255.0 alpha:0.2].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    
    UIImageView *imgv = ImageViewWithImage(@"picture_icon11");
    UILabel *label = [Utils createLabelWithTitle:@"请提供问题的截图或照片???" titleColor:HexColor(0x666666) fontSize:13];
    [view sd_addSubviews:@[imgv,label,
                           ]];
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view sd_addSubviews:self.imageviewArray];
    
    imgv.sd_layout
    .topSpaceToView(view, 23)
    .leftSpaceToView(view, 20)
    .widthIs(12)
    .heightIs(12);

    label.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 4)
    .autoHeightRatio(0);
    
    UIImageView *pview0 = self.imageviewArray[0];
    pview0.hidden = NO;
    pview0.sd_layout
    .topSpaceToView(label, 20)
    .leftSpaceToView(view, 20)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview1 = self.imageviewArray[1];
    pview1.sd_layout
    .topEqualToView(pview0)
    .leftSpaceToView(pview0, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview2 = self.imageviewArray[2];
    pview2.sd_layout
    .topEqualToView(pview0)
    .leftSpaceToView(pview1, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview3 = self.imageviewArray[3];
    pview3.sd_layout
    .topSpaceToView(pview0, 8)
    .leftEqualToView(pview0)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview4 = self.imageviewArray[4];
    pview4.sd_layout
    .topEqualToView(pview3)
    .leftSpaceToView(pview3, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview5 = self.imageviewArray[5];
    pview5.sd_layout
    .topEqualToView(pview4)
    .leftSpaceToView(pview4, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    UIImageView *pview6 = self.imageviewArray[6];
    pview6.sd_layout
    .topSpaceToView(pview3, 8)
    .leftEqualToView(pview3)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview7 = self.imageviewArray[7];
    pview7.sd_layout
    .topEqualToView(pview6)
    .leftSpaceToView(pview6, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);

    UIImageView *pview8 = self.imageviewArray[8];
    pview8.sd_layout
    .topEqualToView(pview7)
    .leftSpaceToView(pview7, 12)
    .widthIs(C_PhoteSize)
    .heightIs(C_PhoteSize);
    
    [view setupAutoHeightWithBottomView:pview0 bottomMargin:30];
    return view;
}

- (void)takePhotoIsClicked:(UITapGestureRecognizer *)reg
{
    UIImageView *imgv = (UIImageView *)reg.view;
    self.curImageIndex = imgv.tag;
    [self chooseImage];
    return;
}

- (void)deletePhotoIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    self.curImageIndex = btn.tag;
    if(self.curImageIndex<self.imagePathArray.count){
        [self.imagePathArray removeObjectAtIndex:self.curImageIndex];
        [self showPictures];
    }
}

- (void)showPictures{
    for(int i=0; i<self.imageviewArray.count; i++){
        UIImageView *imgv = self.imageviewArray[i];
        imgv.image = [UIImage imageNamed:@"share_bg_7"];
        NSMutableArray *newges = [NSMutableArray arrayWithArray:imgv.gestureRecognizers];
        for (int j =0; j<[newges count]; j++) {
            [imgv removeGestureRecognizer:[newges objectAtIndex:j]];
        }
        imgv.hidden = YES;
    }
    for (int i=0; i<self.imagePathArray.count; i++) {
        UIImageView *imgv = self.imageviewArray[i];
        [imgv sd_setImageWithURL:[NSURL URLWithString:self.imagePathArray[i]]];
        imgv.hidden = NO;
    };
    if(self.imagePathArray.count<9){
        UIImageView *lastview = self.imageviewArray[self.imagePathArray.count];
        [lastview addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
        lastview.hidden = NO;
        UIView *contentView = [lastview superview];
        [contentView setupAutoHeightWithBottomView:lastview bottomMargin:30];
        [contentView updateLayout];
    }
}

- (void)onSubmit:(UITapGestureRecognizer *)reg{
    if(self.textView.text.length==0){
        [MBProgressHUD showMessage:@"请填写意见建议"];
        return;
    }
    NSMutableString *linkedstr = [NSMutableString new];
    for (int i=0; i<self.imagePathArray.count; i++){
        if(i==0){
            [linkedstr appendString:self.imagePathArray[i]];
        }else{
            [linkedstr appendFormat:@"||%@",self.imagePathArray[i]];
        }
    }
    Api *api = [Api apiPostUrl:@"work/feedback/add"
                          para:@{@"suggestionMsg":self.textView.text,
                                 @"ltSuggestionFeedbackImages":linkedstr,
                                     @"suggestionType":@(self.index)
                                 
                          }
                   description:@"新增意见反馈"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [MBProgressHUD showMessage:@"操作成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
    
}


-(void)chooseImage {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//获取选择的图片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *image1 = [Utils compressImageSize:image toByte:10*1024];
    [self cosUploadImgae:image1];
}


//从相机或者相册界面弹出
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableArray *)imageviewArray
{
    if(!_imageviewArray){
        _imageviewArray = [NSMutableArray new];
        for(int i=0;i<9;i++){
            UIImageView *picView = [self makePhotoViewTag:i];
            picView.userInteractionEnabled = YES;
            [picView addTapGestureTarget:self action:@selector(takePhotoIsClicked:)];
            picView.hidden = YES;
            picView.tag = i;
            [_imageviewArray addObject:picView];
        }
    }
    return _imageviewArray;
}

- (UIImageView *)makePhotoViewTag:(NSInteger)tag{
    UIImageView *imgv = ImageViewWithImage(@"share_bg_7");
    imgv.tag = tag;
    imgv.layer.cornerRadius = 3;
    imgv.layer.masksToBounds = YES;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = tag;
    [btn setBackgroundColor:HexColor(0x64A8FD)];
    [btn addTarget:self action:@selector(deletePhotoIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"X" forState:UIControlStateNormal];
    [imgv addSubview:btn];
    btn.sd_layout
    .topEqualToView(imgv)
    .rightEqualToView(imgv)
    .widthIs(18)
    .heightIs(18);
    return imgv;
}

- (UIView *)makeTopView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:139/255.0 green:149/255.0 blue:169/255.0 alpha:0.2].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;

    UIImageView *imgv = ImageViewWithImage(@"prob_icon");
    UILabel *label = [Utils createLabelWithTitle:@"反馈的问题" titleColor:HexColor(0x666666) fontSize:13];

    EMTextView *textView = [[EMTextView alloc] init];
    textView.backgroundColor = HexColor(0xF4F4F4);
    //textView.delegate = self;
    textView.placeholder = @"示例：大兴善寺，/“佛教八宗/”之一/“密宗/”祖庭，是隋唐皇家寺院，帝都长安三大译经场之一";
    textView.font = [UIFont systemFontOfSize:14];
    textView.returnKeyType = UIReturnKeySend;
    self.textView = textView;

    [view sd_addSubviews:@[imgv,label,
                           textView]];
    imgv.sd_layout
    .topSpaceToView(view, 23)
    .leftSpaceToView(view, 20)
    .widthIs(12)
    .heightIs(12);

    label.sd_layout
    .centerYEqualToView(imgv)
    .leftSpaceToView(imgv, 4)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    textView.sd_layout
    .topSpaceToView(imgv, 11)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(150);

    [view setupAutoHeightWithBottomView:textView bottomMargin:30];
    return view;
}

@end
