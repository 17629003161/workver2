//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface UserEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *isBindWechat;
@property (nonatomic,copy) NSString *userLabel;
@property (nonatomic,copy) NSString *referralCode;
@property (nonatomic,copy) NSString *creatTime;
@property (nonatomic,strong) NSNumber *creatTimeVO;
@property (nonatomic,strong) NSNumber *sex;
@property (nonatomic,copy) NSString *birthTime;
@property (nonatomic,assign) BOOL isRealname;
@property (nonatomic,copy) NSString *job;
@property (nonatomic,copy) NSString *authQqId;
@property (nonatomic,copy) NSString *sign;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,strong) NSNumber *userStyle;
@property (nonatomic,strong) NSNumber *birthTimeVO;
@property (nonatomic,copy) NSString *referralUserId;
@property (nonatomic,strong) NSNumber *phone;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,copy) NSString *authWechatId;
@property (nonatomic,copy) NSString *idNumber;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *password;
@property (nonatomic,copy) NSString *isBindQq;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,strong) NSNumber *userId;
 


-(id)initWithJson:(NSDictionary *)json;

@end
