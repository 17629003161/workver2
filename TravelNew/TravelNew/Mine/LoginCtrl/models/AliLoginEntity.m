//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "AliLoginEntity.h"

@implementation AliLoginEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
		self.nickname  = [json objectForKey:@"nickname"];
		self.aliUserid  = [json objectForKey:@"aliUserid"];
		self.headimgurl  = [json objectForKey:@"headimgurl"];
		self.unionid  = [json objectForKey:@"unionid"];
		self.token  = [json objectForKey:@"token"];
		self.sex  = [json objectForKey:@"sex"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.nickname forKey:@"zx_nickname"];
	[aCoder encodeObject:self.aliUserid forKey:@"zx_aliUserid"];
	[aCoder encodeObject:self.headimgurl forKey:@"zx_headimgurl"];
	[aCoder encodeObject:self.unionid forKey:@"zx_unionid"];
	[aCoder encodeObject:self.token forKey:@"zx_token"];
	[aCoder encodeObject:self.sex forKey:@"zx_sex"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.nickname = [aDecoder decodeObjectForKey:@"zx_nickname"];
		self.aliUserid = [aDecoder decodeObjectForKey:@"zx_aliUserid"];
		self.headimgurl = [aDecoder decodeObjectForKey:@"zx_headimgurl"];
		self.unionid = [aDecoder decodeObjectForKey:@"zx_unionid"];
		self.token = [aDecoder decodeObjectForKey:@"zx_token"];
		self.sex = [aDecoder decodeObjectForKey:@"zx_sex"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"nickname : %@\n",self.nickname];
	result = [result stringByAppendingFormat:@"aliUserid : %@\n",self.aliUserid];
	result = [result stringByAppendingFormat:@"headimgurl : %@\n",self.headimgurl];
	result = [result stringByAppendingFormat:@"unionid : %@\n",self.unionid];
	result = [result stringByAppendingFormat:@"token : %@\n",self.token];
	result = [result stringByAppendingFormat:@"sex : %@\n",self.sex];
	
    return result;
}

@end
