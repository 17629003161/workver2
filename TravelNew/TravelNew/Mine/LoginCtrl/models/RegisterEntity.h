//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface RegisterEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *token;
@property (nonatomic,strong) NSNumber *userId;
 


-(id)initWithJson:(NSDictionary *)json;

@end
