//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface AliLoginEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *aliUserid;
@property (nonatomic,copy) NSString *headimgurl;
@property (nonatomic,copy) NSString *unionid;
@property (nonatomic,copy) NSString *token;
@property (nonatomic,strong) NSNumber *sex;

-(id)initWithJson:(NSDictionary *)json;

@end
