//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "UserEntity.h"

@implementation UserEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.isBindWechat  = [json objectForKey:@"isBindWechat"];
		self.userLabel  = [json objectForKey:@"userLabel"];
		self.referralCode  = [json objectForKey:@"referralCode"];
		self.creatTime  = [json objectForKey:@"creatTime"];
		self.creatTimeVO  = [json objectForKey:@"creatTimeVO"];
		self.sex  = [json objectForKey:@"sex"];
		self.birthTime  = [json objectForKey:@"birthTime"];
		self.isRealname = [[json objectForKey:@"isRealname"]boolValue];
		self.job  = [json objectForKey:@"job"];
		self.authQqId  = [json objectForKey:@"authQqId"];
		self.sign  = [json objectForKey:@"sign"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.userStyle  = [json objectForKey:@"userStyle"];
		self.birthTimeVO  = [json objectForKey:@"birthTimeVO"];
		self.referralUserId  = [json objectForKey:@"referralUserId"];
		self.phone  = [json objectForKey:@"phone"];
		self.userNickname  = [json objectForKey:@"userNickname"];
		self.authWechatId  = [json objectForKey:@"authWechatId"];
		self.idNumber  = [json objectForKey:@"idNumber"];
		self.userName  = [json objectForKey:@"userName"];
		self.easemobId  = [json objectForKey:@"easemobId"];
		self.password  = [json objectForKey:@"password"];
		self.isBindQq  = [json objectForKey:@"isBindQq"];
		self.address  = [json objectForKey:@"address"];
		self.userId  = [json objectForKey:@"userId"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.isBindWechat forKey:@"zx_isBindWechat"];
	[aCoder encodeObject:self.userLabel forKey:@"zx_userLabel"];
	[aCoder encodeObject:self.referralCode forKey:@"zx_referralCode"];
	[aCoder encodeObject:self.creatTime forKey:@"zx_creatTime"];
	[aCoder encodeObject:self.creatTimeVO forKey:@"zx_creatTimeVO"];
	[aCoder encodeObject:self.sex forKey:@"zx_sex"];
	[aCoder encodeObject:self.birthTime forKey:@"zx_birthTime"];
	[aCoder encodeBool:self.isRealname forKey:@"zx_isRealname"];
	[aCoder encodeObject:self.job forKey:@"zx_job"];
	[aCoder encodeObject:self.authQqId forKey:@"zx_authQqId"];
	[aCoder encodeObject:self.sign forKey:@"zx_sign"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.userStyle forKey:@"zx_userStyle"];
	[aCoder encodeObject:self.birthTimeVO forKey:@"zx_birthTimeVO"];
	[aCoder encodeObject:self.referralUserId forKey:@"zx_referralUserId"];
	[aCoder encodeObject:self.phone forKey:@"zx_phone"];
	[aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.authWechatId forKey:@"zx_authWechatId"];
	[aCoder encodeObject:self.idNumber forKey:@"zx_idNumber"];
	[aCoder encodeObject:self.userName forKey:@"zx_userName"];
	[aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
	[aCoder encodeObject:self.password forKey:@"zx_password"];
	[aCoder encodeObject:self.isBindQq forKey:@"zx_isBindQq"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.isBindWechat = [aDecoder decodeObjectForKey:@"zx_isBindWechat"];
		self.userLabel = [aDecoder decodeObjectForKey:@"zx_userLabel"];
		self.referralCode = [aDecoder decodeObjectForKey:@"zx_referralCode"];
		self.creatTime = [aDecoder decodeObjectForKey:@"zx_creatTime"];
		self.creatTimeVO = [aDecoder decodeObjectForKey:@"zx_creatTimeVO"];
		self.sex = [aDecoder decodeObjectForKey:@"zx_sex"];
		self.birthTime = [aDecoder decodeObjectForKey:@"zx_birthTime"];
		self.isRealname = [aDecoder decodeBoolForKey:@"zx_isRealname"];
	self.job = [aDecoder decodeObjectForKey:@"zx_job"];
		self.authQqId = [aDecoder decodeObjectForKey:@"zx_authQqId"];
		self.sign = [aDecoder decodeObjectForKey:@"zx_sign"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.userStyle = [aDecoder decodeObjectForKey:@"zx_userStyle"];
		self.birthTimeVO = [aDecoder decodeObjectForKey:@"zx_birthTimeVO"];
		self.referralUserId = [aDecoder decodeObjectForKey:@"zx_referralUserId"];
		self.phone = [aDecoder decodeObjectForKey:@"zx_phone"];
		self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.authWechatId = [aDecoder decodeObjectForKey:@"zx_authWechatId"];
		self.idNumber = [aDecoder decodeObjectForKey:@"zx_idNumber"];
		self.userName = [aDecoder decodeObjectForKey:@"zx_userName"];
		self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
		self.password = [aDecoder decodeObjectForKey:@"zx_password"];
		self.isBindQq = [aDecoder decodeObjectForKey:@"zx_isBindQq"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"isBindWechat : %@\n",self.isBindWechat];
	result = [result stringByAppendingFormat:@"userLabel : %@\n",self.userLabel];
	result = [result stringByAppendingFormat:@"referralCode : %@\n",self.referralCode];
	result = [result stringByAppendingFormat:@"creatTime : %@\n",self.creatTime];
	result = [result stringByAppendingFormat:@"creatTimeVO : %@\n",self.creatTimeVO];
	result = [result stringByAppendingFormat:@"sex : %@\n",self.sex];
	result = [result stringByAppendingFormat:@"birthTime : %@\n",self.birthTime];
	result = [result stringByAppendingFormat:@"isRealname : %@\n",self.isRealname?@"yes":@"no"];
	result = [result stringByAppendingFormat:@"job : %@\n",self.job];
	result = [result stringByAppendingFormat:@"authQqId : %@\n",self.authQqId];
	result = [result stringByAppendingFormat:@"sign : %@\n",self.sign];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"userStyle : %@\n",self.userStyle];
	result = [result stringByAppendingFormat:@"birthTimeVO : %@\n",self.birthTimeVO];
	result = [result stringByAppendingFormat:@"referralUserId : %@\n",self.referralUserId];
	result = [result stringByAppendingFormat:@"phone : %@\n",self.phone];
	result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"authWechatId : %@\n",self.authWechatId];
	result = [result stringByAppendingFormat:@"idNumber : %@\n",self.idNumber];
	result = [result stringByAppendingFormat:@"userName : %@\n",self.userName];
	result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
	result = [result stringByAppendingFormat:@"password : %@\n",self.password];
	result = [result stringByAppendingFormat:@"isBindQq : %@\n",self.isBindQq];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	
    return result;
}

@end
