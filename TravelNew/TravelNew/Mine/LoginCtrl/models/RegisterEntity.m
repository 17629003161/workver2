//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RegisterEntity.h"

@implementation RegisterEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.token  = [json objectForKey:@"token"];
		self.userId  = [json objectForKey:@"userId"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.token forKey:@"zx_token"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.token = [aDecoder decodeObjectForKey:@"zx_token"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"token : %@\n",self.token];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	
    return result;
}

@end
