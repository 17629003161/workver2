//
//  PhoneBundingCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "AliLoginEntity.h"
#import "AppleLoginEntity.h"
#import "WXLoginEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhoneBundingCtrl : LKBaseViewController
@property(nonatomic,strong) AliLoginEntity *alimodel;
@property(nonatomic,strong) AppleLoginEntity *appleModel;
@property(nonatomic,strong) WXLoginEntity *wxModel;
@end

NS_ASSUME_NONNULL_END
