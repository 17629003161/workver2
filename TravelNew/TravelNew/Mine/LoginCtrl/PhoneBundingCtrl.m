//
//  PhoneBundingCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PhoneBundingCtrl.h"
#import "YTKChainRequest.h"
#import "RegisterEntity.h"

@interface PhoneBundingCtrl ()<YTKChainRequestDelegate>{
    UIButton *_checkBtn;
    UITextField *_phoneField;
    UITextField *_checkField;
}

@end

@implementation PhoneBundingCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewLayout];

}

- (void)loginBtnIsClicked:(id)sender{
    NSString *phone = [_phoneField.text trimWhitespace];
    NSString *checkCode = [_checkField.text trimWhitespace];
    if([phone isEqualToString:@""] || [checkCode isEqualToString:@""]){
        [MBProgressHUD showError:@"手机号验证码不能为空" toView:self.view];
        return;
    }
    UserInfo *user = [UserInfo shareInstance];
    user.isLogin = NO;
    NSMutableDictionary *para = [[NSMutableDictionary alloc] init];
    [para setObject:phone forKey:@"phone"];
    [para setObject:checkCode forKey:@"validate"];
    if(self.alimodel){
        [para setObject:self.alimodel.aliUserid forKey:@"aliUserid"];
        [para setObject:self.alimodel.headimgurl forKey:@"headimgurl"];
        [para setObject:self.alimodel.sex forKey:@"sex"];
        [para setObject:self.alimodel.nickname forKey:@"nickname"];
    }else if(self.appleModel){
        [para setObject:self.appleModel.appleId forKey:@"appleId"];
    }else if(self.wxModel){
        [para setObject:self.wxModel.unionid forKey:@"unionid"];
    }
    Api *api = [Api apiPostUrl:@"user/register"
                          para:para
                   description:@"user-登录/注册"];
    api.withToken = NO;
    [self sendChainRequestApi:api];
}

- (void)sendChainRequestApi:(Api *)api {
    Api *reg = api;
    //1、先注册登录
    
    YTKChainRequest *chainReq = [[YTKChainRequest alloc] init];
    [chainReq addRequest:reg callback:^(YTKChainRequest *chainRequest, YTKBaseRequest *baseRequest) {
        Api *result = (Api *)baseRequest;
        NSString *json = result.responseString;
        DLog(@"jsonssss = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"code"] integerValue] == 200){
            NSString *msg = [dict objectForKey:@"msg"];
            NSDictionary *data = [dict objectForKey:@"data"];
            [MBProgressHUD showMessage:msg];
            RegisterEntity *entity = [[RegisterEntity alloc] initWithJson:data];
            UserInfo *user = [UserInfo shareInstance];
            user.token = entity.token;
            user.userId = entity.userId;
            user.isLogin = YES;
            //2、注册登录完成后，获取用户信息
            Api *getuserApi = [Api apiPostUrl:@"user/getUserInfo"
                                         para:@{}
                                  description:@"user-获取用户信息"];
            getuserApi.withToken = YES;
            [chainReq addRequest:getuserApi callback:^(YTKChainRequest *chainRequest, YTKBaseRequest *baseRequest){
                Api *result = (Api *)baseRequest;
                NSString *json = result.responseString;
                DLog(@"jsonstr = %@",json);
                NSDictionary *dict = [NSString jsonStringToDictionary:json];
                if([[dict objectForKey:@"code"] integerValue] == 200){
                    NSDictionary *data = [dict objectForKey:@"data"];
                    UserInfo *user = [UserInfo shareInstance];
                    user = [user initWithJson:data];
                    [user save];
                    DLog(@"user = %@",user);
                    //环信登录
                    [self loginEmClient];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
        }
    }];
    chainReq.delegate = self;
    // start to send request
    [chainReq start];
}

//环信登录
- (void)loginEmClient{
    UserInfo *user = [UserInfo shareInstance];
    //登录环信
    [[EMClient sharedClient] loginWithUsername:user.easemobId password:user.password completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            DLog(@"环信登录成功");
        } else {
            DLog(@"登录失败的原因---%@", aError.errorDescription);
        }
    }];
}

 
- (void)chainRequestFinished:(YTKChainRequest *)chainRequest {
    // all requests are done
    DLog(@"all requests are done");
}
 
- (void)chainRequestFailed:(YTKChainRequest *)chainRequest failedBaseRequest:(YTKBaseRequest*)request {
    // some one of request is failed
    DLog(@"some one of request is failed");
}




-(void)transformView:(NSNotification *)aNSNotification
{

}

- (void)checkBtnIsClicked:(id)sender{
    _checkBtn.enabled = NO;
    NSString *phoneNumber = _phoneField.text;
    [Utils sendVerifyCodeToPhoneNumber:phoneNumber];
    //创建Timer
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timer_callback:) userInfo:nil repeats:YES];
      //使用NSRunLoopCommonModes模式，把timer加入到当前Run Loop中。
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

//timer的回调方法
- (void)timer_callback:(NSTimer *)timer
{
    const NSInteger totalTime = 120;
    static NSInteger seconds = totalTime;
   // DLog(@"Timer %@", [NSThread currentThread]);
    if(seconds==0){
        [_checkBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _checkBtn.enabled = YES;
        seconds = totalTime;
        [timer invalidate];
        timer = nil;
    }else{
        NSString *str = [NSString stringWithFormat:@"%lds后获取",seconds];
        [_checkBtn setTitle:str forState:UIControlStateNormal];
    }
    if(seconds>0){
        seconds--;
    }
}


- (void)viewLayout{
    UILabel *label1 = [Utils createLabelWithTitle:@"绑定手机号"
                                       titleColor:HexColor(0x2c2c2c)
                                         fontSize:20];
    label1.font = [UIFont systemFontOfSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"绑定手机号码，完善账户信息"
                                       titleColor:HexColor(0xcacaca)
                                         fontSize:14];
    UIImageView *phoneImg = ImageViewWithImage(@"shouji");
    UILabel *phoneL = [Utils createLabelWithTitle:@"手机号"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    UITextField *phoneField = [UITextField new];
    phoneField.clearButtonMode = UITextFieldViewModeAlways;
    [phoneField setPlaceHolder:@"请输入您的手机号" Font:LKSystemFont(13) color:HexColor(0x999999)];
    phoneField.textColor = HexColor(0x333333);
    phoneField.font = LKSystemFont(15);
    phoneField.keyboardType = UIKeyboardTypePhonePad;
    _phoneField = phoneField;
    
    UIView *line1 = [self createLine];
    UIImageView *checkImg = ImageViewWithImage(@"yanzhengma");
    UILabel *checkL = [Utils createLabelWithTitle:@"验证码"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    UITextField *checkField = UITextField.new;
    checkField.clearButtonMode = UITextFieldViewModeAlways;
    [checkField setPlaceHolder:@"请输入您收到的验证码" Font:LKSystemFont(13) color:HexColor(0x999999)];
    checkField.font = LKSystemFont(15);
    checkField.textColor = HexColor(0x333333);
    checkField.keyboardType = UIKeyboardTypeNumberPad;
    _checkField = checkField;
    
    _checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _checkBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [_checkBtn setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
    _checkBtn.layer.cornerRadius = 15;
    [_checkBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    _checkBtn.layer.borderColor = HexColor(0xe7e7e7).CGColor;
    _checkBtn.layer.borderWidth = 1;
     [_checkBtn addTarget:self action:@selector(checkBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *line2 = [self createLine];
    
    LKGadentButton *loginBtn = [LKGadentButton blueButtonWhithTitle:@"绑定手机号"
                                           font:[UIFont systemFontOfSize:17]
                                   cornerRadius:22];
    [loginBtn addTapGestureTarget:self action:@selector(loginBtnIsClicked:)];
    UILabel *label = [Utils createLabelWithTitle:@"Ps:应国家法律要求，2019年6月1日起使用互联网服务进行账号实名。为保障旅咖出行账号的正常使用,请尽快完善手机号验证码，感谢您的理解和支持。"
                                      titleColor:HexColor(0x999999)
                                        fontSize:12];
    label.numberOfLines = 0;

    
    
    [self.view sd_addSubviews:@[label1,label2,
                                phoneImg,phoneL,
                                phoneField,
                                line1,
                                checkImg,checkL,
                                checkField,_checkBtn,
                                line2,
                                loginBtn,
                                label]];
    label1.sd_layout
    .topSpaceToView(self.view, 75+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(self.view, 36)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneImg.sd_layout
    .leftSpaceToView(self.view, 40)
    .topSpaceToView(label2, 57)
    .widthIs(13)
    .heightIs(20);
    
    phoneL.sd_layout
    .centerYEqualToView(phoneImg)
    .leftSpaceToView(phoneImg, 13)
    .autoHeightRatio(0);
    [phoneL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneField.sd_layout
    .topSpaceToView(phoneL, 13)
    .leftEqualToView(phoneL)
    .rightSpaceToView(self.view, 36)
    .heightIs(18);
    
    line1.sd_layout
    .topSpaceToView(phoneL, 47)
    .leftSpaceToView(self.view, 36)
    .rightSpaceToView(self.view, 36)
    .heightIs(1);
    
    checkImg.sd_layout
    .topSpaceToView(line1, 24)
    .leftEqualToView(phoneImg)
    .widthIs(13)
    .heightIs(20);
    
    checkL.sd_layout
    .centerYEqualToView(checkImg)
    .leftSpaceToView(checkImg, 13)
    .autoHeightRatio(0);
    [checkL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _checkBtn.sd_layout
    .topSpaceToView(line1, 48)
    .rightSpaceToView(self.view, 36)
    .widthIs(96)
    .heightIs(30);
    
    checkField.sd_layout
    .leftEqualToView(phoneField)
    .topSpaceToView(checkL, 13)
    .rightSpaceToView(_checkBtn, 2)
    .heightIs(18);
    
    line2.sd_layout
    .topSpaceToView(line1, 89)
    .leftEqualToView(line1)
    .rightEqualToView(line1)
    .heightIs(1);
    
    loginBtn.sd_layout
    .topSpaceToView(line2, 50)
    .centerXEqualToView(self.view)
    .widthIs(320)
    .heightIs(44);
    
    label.sd_layout
    .topSpaceToView(loginBtn, 26)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .autoHeightRatio(0);
    
    
    
}

- (UIView *)createLine{
    UIView *line = UIView.new;
    line.backgroundColor = HexColor(0xE7E7E7);
    return line;
}

@end
