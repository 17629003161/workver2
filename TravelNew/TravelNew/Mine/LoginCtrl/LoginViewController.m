//
//  LoginViewController.m
//  TravelNew
//
//  Created by mac on 2020/9/22.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LoginViewController.h"
#import "PhoneBundingCtrl.h"
#import "RegisterEntity.h"
#import "YTKChainRequest.h"
#import "WXLoginShare.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PhoneBundingCtrl.h"
#import "AliLoginEntity.h"
#import "WXLoginEntity.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import "SignInApple.h"
#import "LKWebViewCtrl.h"


@interface LoginViewController ()<YTKChainRequestDelegate,WXLoginDelegate,AppleSignInDelegate>{
    UIButton *_checkBtn;
    UITextField *_phoneField;
    UITextField *_checkField;
}
@property(nonatomic,strong) MBProgressHUD *hud;
@property(nonatomic,strong) WXLoginShare *wxLoginShare;
// 防止被释放，回调方法不会走，或者也可以把SignInApple这个类写成单例
@property(nonatomic,strong) SignInApple *signInApple;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"登录" titleColor:nil];
    [self viewLayout];
    self.wxLoginShare = [WXLoginShare shareInstance];
    self.wxLoginShare.delegate = self;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   // [self perfomExistingAccount];
}

-(void)transformView:(NSNotification *)aNSNotification
{

}
#pragma mark -微信登录
- (void)wxLoginIsClicked:(UITapGestureRecognizer *)reg
{
    [self.wxLoginShare WXLogin];
}

#pragma mark -- 支付宝登录
- (void)aliLoginIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/data/alipayAuth" para:@{} description:@"获取支付宝userinfo"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSString *jsonstr = request.responseString;
            DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSString *authInfoStr = [json objectForKey:@"data"];
            DLog(@"authInfoStr = %@",authInfoStr);
            [weakSelf doAliAuthoWithAuthString:authInfoStr];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

#pragma mark -- apple 登录
-(void)appleLoginIsClicked :(id)sender
{
    // 封装Sign In with Apple 登录工具类，使用这个类时要把类对象设置为全局变量，或者直接把这个工具类做成单例，如果使用局部变量，和IAP支付工具类一样，会导致苹果回调不会执行
    self.signInApple = [[SignInApple alloc] init];
    self.signInApple.delegate = self;
    [self.signInApple handleAuthorizationAppleIDButtonPress];
}


- (void)doAppleLoginToken:(NSString *)token authorizationCode:authorizationCodeStr appleId:(NSString *)appleId{
    Api *api = [Api apiPostUrl:@"user/data/appleData" para:@{@"identityToken":token,
                                                             @"suthorizationCode":authorizationCodeStr,
                                                             @"appleId":appleId}
                   description:@"appleid 登录请求"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            AppleLoginEntity *model = [[AppleLoginEntity alloc] initWithJson:data];
            UserInfo *user = [UserInfo shareInstance];
            user.userNickname = model.nickname;
            user.sex = model.sex;
            user.headerImage = model.headimgurl;
            user.unionid = model.unionid;
            user.userId = model.userId;
            user.aliUserid = model.aliUserid;
            user.token = model.token;
            user.isLogin = YES;
            [user save];
            
            if(isNull(model.token) || isNull(model.userId) || [model.token isEqualToString:@""]||[model.userId integerValue]==0){   //未绑定手机号
                PhoneBundingCtrl *vc = [PhoneBundingCtrl new];
                vc.appleModel = model;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                //已绑定手机号，则直接获取用户信息
                [self getUserInfo];
            }
        };
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.responseString);
        }];
}


// 如果存在iCloud Keychain 凭证或者AppleID 凭证提示用户
- (void)perfomExistingAccount{
    // 封装Sign In with Apple 登录工具类，使用这个类时要把类对象设置为全局变量，或者直接把这个工具类做成单例，如果使用局部变量，和IAP支付工具类一样，会导致苹果回调不会执行
    self.signInApple = [[SignInApple alloc] init];
    [self.signInApple perfomExistingAccountSetupFlows];
}

- (void)doAliAuthoWithAuthString:(NSString *)authInfoStr
{
    LKWeakSelf
    [[AlipaySDK defaultService] auth_V2WithInfo:authInfoStr
                                     fromScheme:@"alisdkdemo"
                                       callback:^(NSDictionary *resultDic) {
                                           DLog(@"result = %@",resultDic);
                                           // 解析 auth code
                                           NSString *result = resultDic[@"result"];
                                           NSString *authCode = nil;
                                           if (result.length>0) {
                                               NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                                               for (NSString *subResult in resultArr) {
                                                   if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                                                       authCode = [subResult substringFromIndex:10];
                                                       break;
                                                   }
                                               }
                                           }
                                           DLog(@"授权结果 authCode = %@", authCode?:@"");
                                            if(authCode==nil){
                                                [MBProgressHUD showMessage:@"支付宝授权失败"];
                                            }else{
                                                [weakSelf aliLoginWithAuthCode:authCode];
                                            }
                                       }];
}
 // 支付宝登录
- (void)aliLoginWithAuthCode:(NSString *)authCode
{
    [NSThread sleepForTimeInterval:0.2];
    Api *api = [Api apiPostUrl:@"user/data/aliData" para:@{@"code":authCode} description:@"支付宝登陆请求"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            AliLoginEntity *model = [[AliLoginEntity alloc] initWithJson:data];
            UserInfo *user = [UserInfo shareInstance];
            user.userNickname = model.nickname;
            user.sex = model.sex;
            user.headerImage = model.headimgurl;
            user.unionid = model.unionid;
            user.userId = model.userId;
            user.aliUserid = model.aliUserid;
            user.token = model.token;
            user.isLogin = YES;
            [user save];
            
            if([model.token isEqualToString:@""]||[model.userId integerValue]==0){   //未绑定手机号
                PhoneBundingCtrl *vc = [PhoneBundingCtrl new];
                vc.alimodel = model;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                //已绑定手机号，则直接获取用户信息
                [self getUserInfo];
            }
        }
        DLog(@"%@",jsonstr);
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)getUserInfo{
    Api *api = [Api apiPostUrl:@"user/getUserInfo"
                                 para:@{}
                          description:@"user-获取用户信息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *json = request.responseString;
        DLog(@"jsonstr = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"code"] integerValue] == 200){
            NSDictionary *data = [dict objectForKey:@"data"];
            UserInfo *user = [UserInfo shareInstance];
            user = [user initWithJson:data];
            user.isLogin = YES;
            [user save];
            DLog(@"user = %@",user);
            //登录环信
            [self loginEmClient];
            [self.navigationController popViewControllerAnimated:YES];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,%@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}



- (void)checkBtnIsClicked:(id)sender{
    _checkBtn.enabled = NO;
    NSString *phoneNumber = [_phoneField.text cutOutWhiteSpace];
    if(![LKInputChecker checkTelNumber:phoneNumber]){
        [MBProgressHUD showMessage:@"手机号码错误"];
        _checkBtn.enabled = YES;
        return;
    }
    [Utils sendVerifyCodeToPhoneNumber:phoneNumber];
    //创建Timer
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timer_callback:) userInfo:nil repeats:YES];
      //使用NSRunLoopCommonModes模式，把timer加入到当前Run Loop中。
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

//timer的回调方法
- (void)timer_callback:(NSTimer *)timer
{
    const NSInteger totalTime = 60;
    static NSInteger seconds = totalTime;
   // DLog(@"Timer %@", [NSThread currentThread]);
    if(seconds==0){
        [_checkBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _checkBtn.enabled = YES;
        seconds = totalTime;
        [timer invalidate];
        timer = nil;
    }else{
        NSString *str = [NSString stringWithFormat:@"%lds后获取",seconds];
        [_checkBtn setTitle:str forState:UIControlStateNormal];
    }
    if(seconds>0){
        seconds--;
    }
}

#pragma  mark -- 登录按键动作
- (void)loginBtnIsClicked:(id)sender{
    NSString *phone = [_phoneField.text trimWhitespace];
    NSString *checkCode = [_checkField.text trimWhitespace];
    if(![LKInputChecker checkTelNumber:phone]){
        [MBProgressHUD showMessage:@"手机号格式不正确"];
        return;
    }
    UserInfo *user = [UserInfo shareInstance];
    user.isLogin = NO;
    NSDictionary *para = @{
                           @"phone":phone,
                           @"validate":checkCode
                           };
    Api *api = [Api apiPostUrl:@"user/register"
                          para:para
                   description:@"user-登录/注册"];
    api.withToken = NO;
    [self sendChainRequestApi:api];
}

- (void)sendChainRequestApi:(Api *)api {
    Api *reg = api;
    //1、先注册登录
    LKWeakSelf
    YTKChainRequest *chainReq = [[YTKChainRequest alloc] init];
    [chainReq addRequest:reg callback:^(YTKChainRequest *chainRequest, YTKBaseRequest *baseRequest) {
        Api *result = (Api *)baseRequest;
        NSString *json = result.responseString;
        DLog(@"jsonssss = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"code"] integerValue] == 200){
            NSString *msg = [dict objectForKey:@"msg"];
            NSDictionary *data = [dict objectForKey:@"data"];
            [MBProgressHUD showMessage:msg];
            RegisterEntity *entity = [[RegisterEntity alloc] initWithJson:data];
            UserInfo *user = [UserInfo shareInstance];
            user.token = entity.token;
            user.userId = entity.userId;
            user.isLogin = YES;
            //2、注册登录完成后，获取用户信息
            Api *getuserApi = [Api apiPostUrl:@"user/getUserInfo"
                                         para:@{}
                                  description:@"user-获取用户信息"];
            getuserApi.withToken = YES;
            [chainReq addRequest:getuserApi callback:^(YTKChainRequest *chainRequest, YTKBaseRequest *baseRequest){
                Api *result = (Api *)baseRequest;
                NSString *json = result.responseString;
                DLog(@"jsonstr = %@",json);
                NSDictionary *dict = [NSString jsonStringToDictionary:json];
                if([[dict objectForKey:@"code"] integerValue] == 200){
                    NSDictionary *data = [dict objectForKey:@"data"];
                    UserInfo *user = [UserInfo shareInstance];
                    user = [user initWithJson:data];
                    [user save];
                    DLog(@"user = %@",user);
                    //登录环信
                    [weakSelf loginEmClient];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    chainReq.delegate = self;
    // start to send request
    [chainReq start];
}
 
- (void)loginEmClient{
    UserInfo *user = [UserInfo shareInstance];
    //登录环信
    [[EMClient sharedClient] loginWithUsername:user.easemobId password:user.password completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            DLog(@"环信登录成功");
        } else {
            DLog(@"登录失败的原因---%@", aError.errorDescription);
        }
    }];
}


- (void)chainRequestFinished:(YTKChainRequest *)chainRequest {
    // all requests are done
    DLog(@"all requests are done");
}
 
- (void)chainRequestFailed:(YTKChainRequest *)chainRequest failedBaseRequest:(YTKBaseRequest*)request {
    // some one of request is failed
    DLog(@"some one of request is failed");
}

#pragma  mark 微信登录回调方法

- (void)wxLoginShareSuccess:(NSDictionary *)dict{
    DLog(@"获取到的微信登录信息：dict=%@",dict);
    WXLoginEntity *entity = [[WXLoginEntity alloc] initWithJson:dict];
    UserInfo *user = [UserInfo shareInstance];
    user.sex = entity.sex;
    user.headerImage = entity.headimgurl;
    user.unionid = entity.unionid;
    user.userId = entity.userId;
    user.aliUserid = entity.aliUserid;
    user.token = entity.token;
    user.isLogin = YES;
    [user save];
    
    if([entity.token isEqualToString:@""]||[entity.userId integerValue]==0){   //未绑定手机号
        PhoneBundingCtrl *vc = [PhoneBundingCtrl new];
        vc.wxModel = entity;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //已绑定手机号，则直接获取用户信息
        [self getUserInfo];
    }
}

- (void)wxLoginShareFail:(NSDictionary *)dict{
    DLog(@"登录回调");
}

//微信绑定成功回调
- (void)wxBundingSucess:(NSDictionary *)dict{
    DLog(@"微信绑定成功");
}


#pragma mark -- TencentSessionDelegate
-(void)tencentDidLogin
{
    DLog(@"yes");
    //DLog(@"%@ -- %@",_tencentOAuth.accessToken, _tencentOAuth.openId);// 打印下accessToken和openId代表我成功了，存储起来用的时候直接用，或者此处请求服务器接口传给服务器，获取我们项目中用到的userSession
    //获取用户个人信息
   // [_tencentOAuth getUserInfo];
    
}
-(void)tencentDidNotLogin:(BOOL)cancelled
{
    if (cancelled) {
        DLog(@"取消登录");
    }
}
-(void)tencentDidNotNetWork
{
    DLog(@"tencentDidNotNetWork");
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [_phoneField resignFirstResponder];
    [_checkField resignFirstResponder];
}

- (void)viewLayout{
    [self.view addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth,660);
    [self.scrollView addTapGestureTarget:self action:@selector(viewIsTyped:)];
    self.scrollView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    UILabel *label1 = [Utils createLabelWithTitle:@"请填写以下登录信息"
                                       titleColor:HexColor(0x2c2c2c)
                                         fontSize:20];
    label1.font = [UIFont systemFontOfSize:20];
    UILabel *label2 = [Utils createLabelWithTitle:@"使用快捷登录更便捷"
                                       titleColor:HexColor(0xcacaca)
                                         fontSize:14];
    UIImageView *phoneImg = ImageViewWithImage(@"shouji");
    UILabel *phoneL = [Utils createLabelWithTitle:@"手机号"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    UITextField *phoneField = [UITextField new];
    phoneField.clearButtonMode = UITextFieldViewModeAlways;
    [phoneField setPlaceHolder:@"请输入您的手机号" Font:LKSystemFont(13) color:HexColor(0x999999)];
    phoneField.keyboardType = UIKeyboardTypePhonePad;
    phoneField.textColor = HexColor(0x333333);
    phoneField.font = [UIFont systemFontOfSize:15];
    
    phoneField.placeholder = @"请输入您的手机号";
    _phoneField = phoneField;
    
    UIView *line1 = [self createLine];
    UIImageView *checkImg = ImageViewWithImage(@"yanzhengma");
    UILabel *checkL = [Utils createLabelWithTitle:@"验证码"
                                       titleColor:HexColor(0x333333)
                                         fontSize:14];
    UITextField *checkField = UITextField.new;
    checkField.clearButtonMode = UITextFieldViewModeAlways;
    [checkField setPlaceHolder:@"请输入您的验证码" Font:LKSystemFont(13) color:HexColor(0x999999)];
    checkField.font = LKSystemFont(15);
    checkField.textColor = HexColor(0x333333);
    checkField.keyboardType = UIKeyboardTypeNumberPad;
    _checkField = checkField;
    
    _checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _checkBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [_checkBtn setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
    _checkBtn.layer.cornerRadius = 15;
    [_checkBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [_checkBtn addTarget:self action:@selector(checkBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    _checkBtn.layer.borderColor = HexColor(0xe7e7e7).CGColor;
    _checkBtn.layer.borderWidth = 1;
    
    UIView *line2 = [self createLine];
    
    LKGadentButton *loginBtn = [LKGadentButton blueButtonWhithTitle:@"登录"
                                          font:[UIFont systemFontOfSize:17]
                                   cornerRadius:22];
    [loginBtn addTapGestureTarget:self action:@selector(loginBtnIsClicked:)];
    UIView *line3 = [self createLine];
    UILabel *label = [Utils createLabelWithTitle:@"快捷登录"
                                      titleColor:HexColor(0x666666)
                                        fontSize:16];
    label.backgroundColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    UIImageView *wxImgv = ImageViewWithImage(@"weixin");
    [wxImgv addTapGestureTarget:self action:@selector(wxLoginIsClicked:)];
    UIImageView *zfbImgv = ImageViewWithImage(@"qq");
    [zfbImgv addTapGestureTarget:self action:@selector(aliLoginIsClicked:)];
    
    UILabel *agreeL = [Utils createLabelWithTitle:@"登录即同意《旅咖出行用户协议》"
                                       titleColor:HexColor(0x666666)
                                         fontSize:12];
    [agreeL addTapGestureTarget:self action:@selector(protocolBtnIsClicked:)];
    [agreeL changeStrings:@[@"《旅咖出行用户协议》"] toColor:HexColor(0x1E69FF)];
    
    [self.scrollView sd_addSubviews:@[label1,label2,
                                phoneImg,phoneL,
                                phoneField,
                                line1,
                                checkImg,checkL,
                                checkField,_checkBtn,
                                line2,
                                loginBtn,
                                line3,label,
                                wxImgv,zfbImgv,
                                agreeL]];
    label1.sd_layout
    .topSpaceToView(self.scrollView, 40+SAFEVIEWTOPMARGIN)
    .leftSpaceToView(self.scrollView, 30)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneImg.sd_layout
    .leftSpaceToView(self.scrollView, 40+SAFEVIEWTOPMARGIN)
    .topSpaceToView(label2, 40)
    .widthIs(13)
    .heightIs(20);
    
    phoneL.sd_layout
    .centerYEqualToView(phoneImg)
    .leftSpaceToView(phoneImg, 13)
    .autoHeightRatio(0);
    [phoneL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneField.sd_layout
    .topSpaceToView(phoneL, 13)
    .leftEqualToView(phoneL)
    .rightSpaceToView(self.scrollView, 36)
    .heightIs(18);
    
    line1.sd_layout
    .topSpaceToView(phoneL, 47)
    .leftSpaceToView(self.scrollView, 36)
    .rightSpaceToView(self.scrollView, 36)
    .heightIs(1);
    
    checkImg.sd_layout
    .topSpaceToView(line1, 24)
    .leftEqualToView(phoneImg)
    .widthIs(13)
    .heightIs(20);
    
    checkL.sd_layout
    .centerYEqualToView(checkImg)
    .leftSpaceToView(checkImg, 13)
    .autoHeightRatio(0);
    [checkL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _checkBtn.sd_layout
    .topSpaceToView(line1, 48)
    .rightSpaceToView(self.scrollView, 36)
    .widthIs(96)
    .heightIs(30);
    
    checkField.sd_layout
    .leftEqualToView(phoneField)
    .topSpaceToView(checkL, 13)
    .rightSpaceToView(_checkBtn, 2)
    .heightIs(18);
    
    line2.sd_layout
    .topSpaceToView(line1, 89)
    .leftEqualToView(line1)
    .rightEqualToView(line1)
    .heightIs(1);
    
    loginBtn.sd_layout
    .topSpaceToView(line2, 40)
    .centerXEqualToView(self.scrollView)
    .widthIs(320)
    .heightIs(44);
    
    line3.sd_layout
    .topSpaceToView(loginBtn, 40)
    .leftEqualToView(line2)
    .rightEqualToView(line2)
    .heightIs(1);
    
    label.sd_layout
    .centerXEqualToView(line3)
    .centerYEqualToView(line3)
    .widthIs(104)
    .heightIs(28);
    
    zfbImgv.sd_layout
    .topSpaceToView(line3, 42)
    .rightSpaceToView(self.scrollView, 100)
    .widthIs(50)
    .heightIs(50);
    
    wxImgv.sd_layout
    .centerYEqualToView(zfbImgv)
    .leftSpaceToView(self.scrollView, 100)
    .widthIs(50)
    .heightIs(50);
    
    if (@available(iOS 13.0, *)) {
        // Sign In With Apple Button
        ASAuthorizationAppleIDButton *appleIDBtn = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeDefault style:ASAuthorizationAppleIDButtonStyleWhiteOutline];
        appleIDBtn.frame = CGRectMake(30, self.view.bounds.size.height - 180, self.view.bounds.size.width/2.0, 40);
        [appleIDBtn addTapGestureTarget:self action:@selector(appleLoginIsClicked:)];
       // [appleIDBtn addTarget:self action:@selector(appleLoginIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:appleIDBtn];
        // Sign In With Apple Button
        appleIDBtn.sd_layout
        .topSpaceToView(wxImgv, 30)
        .centerXEqualToView(self.scrollView)
        .widthIs(kScreenWidth/2.0)
        .heightIs(40);
        
        agreeL.sd_layout
        .topSpaceToView(appleIDBtn, 20)
        .centerXEqualToView(self.scrollView)
        .autoHeightRatio(0);
        [agreeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    }else{
        agreeL.sd_layout
        .topSpaceToView(wxImgv, 20)
        .centerXEqualToView(self.scrollView)
        .autoHeightRatio(0);
        [agreeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    }
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"wechat://"]] ) {
        wxImgv.hidden = YES;
    }else{
        wxImgv.hidden = NO;
    }

    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"alipay:"]] ) {
        zfbImgv.hidden = YES;
    }else{
        zfbImgv.hidden = NO;
    }
}


- (void)protocolBtnIsClicked:(UITapGestureRecognizer *)reg
{
    LKWebViewCtrl *vc = [[LKWebViewCtrl alloc] init];
    [vc setTitle:@"用户协议" titleColor:nil];
    vc.urlstr = @"https://sxlkcx.cn/protocol/lawSecrecyxieyi.html";
    [self.navigationController pushViewController:vc animated:YES];
}


- (UIView *)createLine{
    UIView *line = UIView.new;
    line.backgroundColor = HexColor(0xE7E7E7);
    return line;
}



@end
