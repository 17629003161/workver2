//
//  CustomAnnotationView.m
//  CustomAnnotationDemo
//
//  Created by songjian on 13-3-11.
//  Copyright (c) 2013年 songjian. All rights reserved.
//


#import "CustomAnnotationView.h"
#import "CustomCalloutView.h"

@interface CustomAnnotationView (){
    
}
@property (nonatomic, strong)  CustomCalloutView *calloutView;
@property (nonatomic, strong) UIImageView *imgv;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation CustomAnnotationView

- (void)setImage:(NSString *)name{
    _imgv.image = [UIImage imageNamed:name];
}

-(void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)image;
{
    [_imgv sd_setImageWithURL:url placeholderImage:image];
}

#pragma mark - Override
- (void)setSelected:(BOOL)selected
{
    [self setSelected:selected animated:NO];
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    if(_title){
        [self setSelected:YES animated:YES];
        [self.calloutView setText:title];
    }else{
        self.calloutView.hidden = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    if (self.selected == selected){
//        return;
//    }
    DLog(@"--------标注被点击----------");
    if (selected){
        if (self.calloutView == nil){
            /* Construct custom callout. */
            self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, 114, 29)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,  +CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
        }
        [self addSubview:self.calloutView];
    }
    else{
        [self.calloutView removeFromSuperview];
    }
    [super setSelected:selected animated:animated];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL inside = [super pointInside:point withEvent:event];
    /* Points that lie outside the receiver’s bounds are never reported as hits,
     even if they actually lie within one of the receiver’s subviews.
     This can occur if the current view’s clipsToBounds property is set to NO and the affected subview extends beyond the view’s bounds.
     */
    if (!inside && self.selected)
    {
        inside = [self.calloutView pointInside:[self convertPoint:point toView:self.calloutView] withEvent:event];
    }
    
    return inside;
}

#pragma mark - Life Cycle

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.bounds = CGRectMake(0, 0, 32, 40);
        self.backgroundColor = [UIColor clearColor];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        view.backgroundColor = HexColor(0x0094FF);
        view.layer.cornerRadius = 16.0;
        view.layer.masksToBounds = YES;
        UIImageView *imgv = ImageViewWithImage(@"trangle_home");
        imgv.frame = CGRectMake(9, 31, 14, 8);
        _imgv = [[UIImageView alloc] initWithFrame:CGRectMake(1.5, 1.5, 29, 29)];
        _imgv.image = [UIImage imageWithColor:HexColor(0xE3E3E3)];
        _imgv.layer.cornerRadius = 14.5;
        _imgv.layer.masksToBounds = YES;
        [self sd_addSubviews:@[view,imgv,
                               _imgv]];
    }
    
    return self;
}

@end
