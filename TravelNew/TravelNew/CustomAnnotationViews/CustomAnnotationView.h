//
//  CustomAnnotationView.h
//  CustomAnnotationDemo
//
//  Created by songjian on 13-3-11.
//  Copyright (c) 2013年 songjian. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface CustomAnnotationView : MAAnnotationView
@property (nonatomic, strong) NSString *title;
- (void)setImage:(NSString *)name;
-(void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)image;

@end
