//
//  TimeSelectCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/13.
//  Copyright © 2020 lester. All rights reserved.
//

#import "TimeSelectCtrl.h"
#import "LXCalender.h"

@interface TimeSelectCtrl ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIButton *_button;
}

@property(nonatomic,strong)LXCalendarView *calenderView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) UIPickerView *pickView1;
@property(nonatomic,strong) UIPickerView *pickView2;


@property(nonatomic,strong) UILabel *startDateLabel;
@property(nonatomic,strong) UILabel *startTimeLabel;
@property(nonatomic,strong) UILabel *stopDateLabel;
@property(nonatomic,strong) UILabel *stopTimeLabel;


@property(nonatomic,strong) NSDate *curDate;

@end

@implementation TimeSelectCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"出行日期" titleColor:nil];
    [self layout];
    LKWeakSelf
    self.calenderView.selectBlock = ^(NSInteger year, NSInteger month, NSInteger day) {
        DLog(@"%ld年 - %ld月 - %ld日",year,month,day);
        //[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *datestr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld 00:00:00",year,month,day];
        LKDateUtils *date = [LKDateUtils new];
        date.dateTimeString = datestr;
        weakSelf.curDate = date.date;
    };
    
    self.startDate = _startDate;
    self.stopDate = _stopDate;
    self.curDate = [self zeroDateFromDate:_startDate];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    // 返回你所需要的状态栏样式
    return UIStatusBarStyleLightContent;
}

//获取当天零点的日期
- (NSDate *)zeroDateFromDate:(NSDate *)date{
    LKDateUtils *utils = [LKDateUtils new];
    utils.date = date;
    NSString *datestr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld 00:00:00",utils.year,utils.month,utils.day];
    utils.dateTimeString = datestr;
    return utils.date;
}

- (NSMutableArray *)dataArray
{
    if(!_dataArray){
        _dataArray = [NSMutableArray new];
        for(int i=0;i<24*60;i+=30){
            NSString *str = [NSString stringWithFormat:@"%02d:%02d",i/60,i%60];
            [_dataArray addObject:str];
        }
    }
    return _dataArray;
}

- (UIPickerView *)pickView1{
    UIPickerView *pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 560, kScreenWidth/2.0, 105)];
    pickView.tag = 1;
    pickView.delegate = self;
    pickView.dataSource = self;
    return pickView;
}
- (UIPickerView *)pickView2{
    UIPickerView *pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(kScreenWidth/2.0, 560, kScreenWidth/2.0, 105)];
    pickView.tag = 2;
    pickView.delegate = self;
    pickView.dataSource = self;
    return pickView;
}

- (LXCalendarView *)calenderView
{
    if(!_calenderView){
        _calenderView =[[LXCalendarView alloc]initWithFrame:CGRectMake(0,0, kScreenWidth, 275)];
        _calenderView.currentMonthTitleColor = HexColor(0x2c2c2c);
        _calenderView.lastMonthTitleColor = HexColor(0x8a8a8a);
        _calenderView.nextMonthTitleColor = HexColor(0x8a8a8a);
        _calenderView.isHaveAnimation = YES;
        _calenderView.isCanScroll = YES;
        _calenderView.isShowLastAndNextBtn = YES;
        _calenderView.todayTitleColor =[UIColor greenColor];
        _calenderView.selectBackColor =[UIColor redColor];
        _calenderView.isShowLastAndNextDate = NO;
        _calenderView.selectBackColor = HexColor(0x1E69FF);
        [_calenderView dealData];
        _calenderView.backgroundColor = HexColor(0xF2F2F2);
    }
    return _calenderView;
}

- (void)layout{
    [self.view addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, 2*kScreenHeight);

    UIView *showView = [self createShowView];
    UILabel *label3 = [Utils createBoldLabelWithTitle:@"开始时间" titleColor:HexColor(0x333333) fontSize:17];
    label3.textAlignment = NSTextAlignmentCenter;
    UILabel *label4 = [Utils createBoldLabelWithTitle:@"结束时间" titleColor:HexColor(0x333333) fontSize:17];
    label4.textAlignment = NSTextAlignmentCenter;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0x999999);
    UILabel *label5 = [Utils createLabelWithTitle:@"行程最短须四个小时" titleColor:HexColor(0x999999) fontSize:12];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xcccccc)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0x1E69FF)] forState:UIControlStateSelected];
    btn.layer.cornerRadius = 18;
    btn.layer.masksToBounds = YES;
    [btn setEnabled:NO];
    [btn addTarget:self action:@selector(btnOkIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    _button = btn;
    
    [self.scrollView sd_addSubviews:@[
                                   showView,self.calenderView,
                                label3,label4,
                                self.pickView1,self.pickView2,
                                line,
                                label5,btn]];
    self.scrollView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    showView.sd_layout
    .topSpaceToView(self.scrollView, 30)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    self.calenderView.sd_layout
    .topSpaceToView(showView, 31)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(340);
    
    label3.sd_layout
    .topSpaceToView(self.calenderView,18)
    .leftEqualToView(self.scrollView)
    .widthIs(kScreenWidth/2.0)
    .autoHeightRatio(0);

    label4.sd_layout
    .centerYEqualToView(label3)
    .leftSpaceToView(label3, 0)
    .rightSpaceToView(self.scrollView, 0)
    .autoHeightRatio(0);
    
    self.pickView1.sd_layout
    .topSpaceToView(label3, 18)
    .leftEqualToView(self.scrollView)
    .widthIs(kScreenWidth/2.0)
    .heightIs(105);
    
    self.pickView2.sd_layout
    .topEqualToView(self.pickView1)
    .leftSpaceToView(self.pickView1, 0)
    .rightEqualToView(self.scrollView)
    .heightIs(105);
    
    line.sd_layout
    .topSpaceToView(label3, 129)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView)
    .heightIs(0.5);
    
    label5.sd_layout
    .topSpaceToView(line, 19)
    .leftSpaceToView(self.scrollView, 20)
    .autoHeightRatio(0);
    [label5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .centerYEqualToView(label5)
    .rightSpaceToView(self.scrollView, 19)
    .widthIs(120)
    .heightIs(36);

}

- (void)btnOkIsClicked:(id)sender
{
    if(self.OnSelectDateTime){
        self.OnSelectDateTime(self.startDate, self.stopDate);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)createShowView{
    UIView *bgView2 = UIView.new;
    bgView2.backgroundColor = [UIColor whiteColor];
    bgView2.layer.cornerRadius = 4;
    UILabel *_dateStartL = [Utils createBoldLabelWithTitle:@"07月3日"
                                  titleColor:HexColor(0x333333)
                                    fontSize:20];
    self.startDateLabel = _dateStartL;
    UILabel *_timeStartL = [Utils createLabelWithTitle:@"周五 9:00"
                                  titleColor:HexColor(0x333333)
                                    fontSize:12];
    self.startTimeLabel = _timeStartL;
    UILabel *_dateStopL = [Utils createBoldLabelWithTitle:@"07月4日"
                                 titleColor:HexColor(0x333333)
                                   fontSize:20];
    self.stopDateLabel = _dateStopL;
    UILabel *_timeStopL = [Utils createLabelWithTitle:@"周六 9:00"
                                  titleColor:HexColor(0x333333)
                                    fontSize:12];
    self.stopTimeLabel = _timeStopL;
    UIImageView *arrow2 = [UIImageView new];
    arrow2.image = [UIImage imageNamed:@"arrow_icon"];
    [bgView2 sd_addSubviews:@[_dateStartL,_timeStartL,
                              arrow2,
                              _dateStopL,_timeStopL]];

    _dateStartL.sd_layout
    .leftSpaceToView(bgView2, 14)
    .topSpaceToView(bgView2, 0)
    .autoHeightRatio(0);
    [_dateStartL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _timeStartL.sd_layout
    .leftEqualToView(_dateStartL)
    .topSpaceToView(_dateStartL, 7)
    .autoHeightRatio(0);
    [_timeStartL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow2.sd_layout
    .centerXEqualToView(bgView2)
    .centerYEqualToView(bgView2)
    .widthIs(46)
    .heightIs(5);
    
    _dateStopL.sd_layout
    .topEqualToView(_dateStartL)
    .rightSpaceToView(bgView2, 14)
    .autoHeightRatio(0);
    [_dateStopL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _timeStopL.sd_layout
    .leftEqualToView(_dateStopL)
    .topSpaceToView(_dateStopL, 7)
    .autoHeightRatio(0);
    [_timeStopL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [bgView2 setupAutoHeightWithBottomViewsArray:@[_timeStopL,_timeStartL] bottomMargin:0];
    return bgView2;
}


#pragma mark - pickerviewdelegate

// UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件包含的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1; // 返回2表明该控件只包含2列
}

//UIPickerViewDataSource中定义的方法，该方法的返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // 如果该控件只包含一列，因此无须理会列序号参数component
    // 该方法返回teams.count，表明teams包含多少个元素，该控件就包含多少行
    return  self.dataArray.count;

}


// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为UIPickerView中指定列和列表项的标题文本
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    // 由于该控件只包含一列，因此无须理会列序号参数component
    // 该方法根据row参数返回teams中的元素，row参数代表列表项的编号，
    // 因此该方法表示第几个列表项，就使用teams中的第几个元素
    return [_dataArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}


// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为
// UIPickerView中指定列的宽度
-(CGFloat)pickerView:(UIPickerView *)pickerView
   widthForComponent:(NSInteger)component
{
    return kScreenWidth/2.0;

}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:30];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (void)setStartDate:(NSDate *)startDate
{
    _startDate = startDate;
    LKDateUtils *date = [LKDateUtils new];
    date.date = startDate;
    self.startDateLabel.text = [NSString stringWithFormat:@"%ld月%ld日",date.month,date.day];
    self.startTimeLabel.text = [NSString stringWithFormat:@"%@ %02ld:%02ld",date.weekday,date.hour,date.minute];
    [self checkTime];
}

- (void)setStopDate:(NSDate *)stopDate
{
    _stopDate = stopDate;
    LKDateUtils *date = [LKDateUtils new];
    date.date = stopDate;
    self.stopDateLabel.text = [NSString stringWithFormat:@"%ld月%ld日",date.month,date.day];;
    self.stopTimeLabel.text = [NSString stringWithFormat:@"%@ %02ld:%02ld",date.weekday,date.hour,date.minute];;
    [self checkTime];
}

- (BOOL)checkTime{
    NSDate *today = [NSDate date];
    NSDate *todayzero = [self zeroDateFromDate:today];
    LKDateUtils *date = [LKDateUtils new];
    date.date = todayzero;
    NSTimeInterval todayzeroInterval = date.timeInterval;
    date.date = _startDate;
    NSTimeInterval interval1 =  date.timeInterval;
    date.date = _stopDate;
    NSTimeInterval interval2 = date.timeInterval;
    if(interval1 < todayzeroInterval){
        [_button setSelected:NO];
        _button.enabled = NO;
        return NO;
    }
    if((interval2-interval1) > 4*60*60){
        [_button setSelected:YES];
        _button.enabled = YES;
        return YES;
    }else{
        [_button setSelected:NO];
        _button.enabled = NO;
        return NO;
    }
}


// 当用户选中UIPickerViewDataSource中指定列和列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    NSString *str = self.dataArray[row];
    NSArray *temp = [str componentsSeparatedByString:@":"];
    NSString *hourstr = temp[0];
    NSString *minutstr = temp[1];
    NSInteger hour = [hourstr integerValue];
    NSInteger minute = [minutstr intValue];
    LKDateUtils *date = [LKDateUtils new];
    date.date = self.curDate;
    NSTimeInterval interval = date.timeInterval;
    interval += (hour*60*60 + minute*60);
    date.timeInterval = interval;
    if(pickerView.tag == 1){
        self.startDate = date.date;
    }else if(pickerView.tag == 2){
        self.stopDate = date.date;
    }
}

@end
