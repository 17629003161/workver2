//
//  TimeSelectCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/13.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TimeSelectCtrl : LKBaseViewController
@property(nonatomic,strong) NSDate *startDate;
@property(nonatomic,strong) NSDate *stopDate;
@property (nonatomic, copy) void(^OnSelectDateTime)(NSDate *startDate,NSDate *stopDate);  //传递2个参数
@end

NS_ASSUME_NONNULL_END
