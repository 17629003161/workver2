//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface JSONObject : NSObject<NSCoding>

@property (nonatomic,copy) NSString *msg;
@property (nonatomic,strong) NSNumber *data;
@property (nonatomic,strong) NSNumber *code;
 


-(id)initWithJson:(NSDictionary *)json;

@end
