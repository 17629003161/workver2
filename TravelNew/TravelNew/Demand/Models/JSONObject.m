//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "JSONObject.h"

@implementation JSONObject

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.msg  = [json objectForKey:@"msg"];
		self.data  = [json objectForKey:@"data"];
		self.code  = [json objectForKey:@"code"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.msg forKey:@"zx_msg"];
	[aCoder encodeObject:self.data forKey:@"zx_data"];
	[aCoder encodeObject:self.code forKey:@"zx_code"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.msg = [aDecoder decodeObjectForKey:@"zx_msg"];
		self.data = [aDecoder decodeObjectForKey:@"zx_data"];
		self.code = [aDecoder decodeObjectForKey:@"zx_code"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"msg : %@\n",self.msg];
	result = [result stringByAppendingFormat:@"data : %@\n",self.data];
	result = [result stringByAppendingFormat:@"code : %@\n",self.code];
	
    return result;
}

@end
