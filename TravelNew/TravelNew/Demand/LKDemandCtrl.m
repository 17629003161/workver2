//
//  LKTravelCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKDemandCtrl.h"
#import "LKAddressSelectCtrl.h"
#import "PublishOKDialog.h"
#import "TimeSelectCtrl.h"
#import "JSONObject.h"
#import "EMTextView.h"
#import "LKTipDialog.h"

@interface LKDemandCtrl ()<UITextFieldDelegate>

//记录上次选择的数据 时间
@property (nonatomic, strong) NSArray *timeArr;
@property(nonatomic,strong) UILabel *startDateLabel;
@property(nonatomic,strong) UILabel *startTimeLabel;
@property(nonatomic,strong) UILabel *stopDateLabel;
@property(nonatomic,strong) UILabel *stopTimeLabel;
@property(nonatomic,strong) UITextField *numberField;
@property(nonatomic,strong) EMTextView *recomandTextview;
@property(nonatomic,strong) UILabel *recomandNumberL;

@property(nonatomic,strong) UILabel *addressL;
@property(nonatomic,strong) NSString *cityId;
@property(nonatomic,assign) CLLocationCoordinate2D location;

@property(nonatomic,strong) NSString *address;

@end

@implementation LKDemandCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [self checkRecomender];
}


- (void)checkRecomender{
    LKWeakSelf
    __weak LKLocationManager *locationManager = [LKLocationManager sharedManager];
    locationManager.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, CLLocationCoordinate2D location,NSString *cityName) {
        [locationManager stopLocation];
        DLog(@"LocationManager = %@",locationManager);
        [weakSelf requestData:cityid];
    };
    [locationManager startLocation];
}

- (void)requestData:(NSString *)cityId{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerNum"
                          para:@{@"cityid":cityId}
                   description:@"user-获取城市推荐官数量"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        JSONObject *model = [[JSONObject alloc] initWithJson:json];
        if([model.code integerValue]==200){
            NSInteger num = [model.data integerValue];
            NSString *numstr = [NSString stringWithFormat:@"%ld",num];
            weakSelf.recomandNumberL.text = [NSString stringWithFormat:@"当地有%@位推荐官为您服务",numstr];
            [weakSelf.recomandNumberL changeStrings:@[numstr] toColor:HexColor(0xFF983A)];
        }
        
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
    }];
}


- (void)layout{
    
    UIImageView *imageView = ImageViewWithImage(@"travel_bg");
    [self.view sd_addSubviews:@[imageView,self.scrollView]];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView setContentSize:CGSizeMake(kScreenWidth, kScreenHeight+100)];
     
    imageView.sd_layout
    .topEqualToView(self.view)
     .leftEqualToView(self.view)
     .rightEqualToView(self.view)
     .heightIs(200);
     
    self.scrollView.sd_layout
    .topSpaceToView(self.view, 170)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    UIView *workview = [self makeWorkView];
    [workview addTapGestureTarget:self action:@selector(viewIsTaped:)];
    [self.scrollView addSubview:workview];
    
    workview.sd_layout
    .topEqualToView(self.scrollView)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.self.scrollView);

    self.startDateLabel.text = @"请选择开始";
    self.startTimeLabel.text = @"日期 时间";
    self.stopDateLabel.text = @"请选择结束";
    self.stopTimeLabel.text = @"日期 时间";
    
    LKLocationManager *manger = [LKLocationManager sharedManager];
    self.cityId = manger.cityCode;
    self.address = manger.name;
    self.location = manger.coordinate;
    self.addressL.text = self.address;
    [self.addressL updateLayout];
    
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    [_numberField resignFirstResponder];
    [_numberField resignFirstResponder];
    [_recomandTextview resignFirstResponder];
}

- (void)selectAddress:(UITapGestureRecognizer *)reg{
    LKAddressSelectCtrl *vc = [[LKAddressSelectCtrl alloc] init];
   // vc.address = self.addressL.text;
    LKWeakSelf
    vc.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, NSString * _Nonnull cityName, CLLocationCoordinate2D location) {
        weakSelf.addressL.text = name;
        weakSelf.address = name;
        weakSelf.cityId = cityid;
        weakSelf.location = location;
        [weakSelf.addressL updateLayout];
    };
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -发布需求

- (void)publishIsClicked:(id)sender{
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander] && [user isRecomander]){
        LKTipDialog *dlg = [[LKTipDialog alloc] initWithFrame:CGRectMake(0, 0, 270, 181)];
        [dlg show];
        return;
    }
    if (self.address==nil || self.cityId==nil) {
        [MBProgressHUD showMessage:@"请选择出游地"];
        return;
    };
    if(self.timeArr.count==0){
        [MBProgressHUD showMessage:@"请选择出行时间"];
        return;
    }
//    JGCalendarDayModel *startModel = [self.timeArr firstObject];
//    JGCalendarDayModel *stopModel = [self.timeArr lastObject];
//
//    NSInteger num = [_numberField.text integerValue];
//    if(num==0){
//        [MBProgressHUD showMessage:@"请输入出行人数"];
//        return;
//    }
//    if([_recomandTextview.text length]==0){
//        [MBProgressHUD showMessage:@"请输入出行需求"];
//        return;
//    }
//    NSInteger time1 = [startModel getTimeInterval]*1000;
//    NSInteger time2 = [stopModel getTimeInterval]*1000;
//
//    NSDictionary *para = @{
//        @"travelTime":@(time1),
//        @"requirementDescription":_recomandTextview.text,
//        @"number":@(num),
//        @"endTime":@(time2),
//        @"cityid":self.cityId,
//        @"longitude":@(self.location.longitude),
//        @"latitude":@(self.location.latitude),
//        @"address":self.address
//    };
//    LKWeakSelf
//    Api *api = [Api apiPostUrl:@"work/requirement/addRequirement"
//                          para:para
//                   description:@"work-添加需求"];
//    api.withToken = YES;
//    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//        NSString *json = request.responseString;
//        DLog(@"json = %@",json);
//        NSDictionary *dict = [NSString jsonStringToDictionary:json];
//        DLog(@"%@",dict);
//        NSInteger code = [[dict objectForKey:@"code"] integerValue];
//        if(code == 200){
//            [weakSelf clear];
//            NSString *msg = [dict objectForKey:@"msg"];
//            PublishOKDialog *dlg = [[PublishOKDialog alloc] initWithFrame:CGRectZero];
//            dlg.message = msg;
//            [dlg show];
//        }else if(code == 500){
//            [MBProgressHUD showMessage:dict[@"msg"]];
//        }
//    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//        DLog(@"%@",request.error);
//        if([request.responseString containsString:@"登录失效"]){
//            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
//        }
//    }];
}

- (void)clear{
    self.address = nil;
    self.cityId = nil;
    self.timeArr = nil;
    
    self.startDateLabel.text = @"请选择开始";
    self.startTimeLabel.text = @"日期 时间";
    self.stopDateLabel.text = @"请选择结束";
    self.stopTimeLabel.text = @"日期 时间";
    self.addressL.text = @"请选择出游地";
    self.numberField.text = @"";
    self.recomandTextview.text = @"";
    [self.startDateLabel updateLayout];
    [self.startTimeLabel updateLayout];
    [self.stopDateLabel updateLayout];
    [self.addressL updateLayout];
}

 /**
 * 让状态栏样式为白色
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)selectTimeIsClicked:(UITapGestureRecognizer *)reg
{
    [self CalenderClick];
}

- (void)CalenderClick {
//    JGAvailableTimeController *VC = [JGAvailableTimeController new];
//    VC.timeArr = self.timeArr;
//    WEAKSELF;
//    VC.TimeBackInfo = ^(NSArray *data) {
//        if (data.count) {
//            weakSelf.timeArr = data;
//            JGCalendarDayModel *LModel = [data firstObject];
//            JGCalendarDayModel *RModel = [data lastObject];
////            NSString *start_time = LModel.toString;
////            NSString *end_time = RModel.toString;
//          //  NSString *str = [NSString stringWithFormat:@"开始时间：%@\n结束时间:%@",start_time, end_time];
//            self.startDateLabel.text = [NSString stringWithFormat:@"%ld月%ld日",LModel.month,LModel.day];
//            self.startTimeLabel.text = [NSString stringWithFormat:@"%@ %@",[LModel getWeek],LModel.timeStr];
//            self.stopDateLabel.text = [NSString stringWithFormat:@"%ld月%ld日",RModel.month,RModel.day];
//            self.stopTimeLabel.text = [NSString stringWithFormat:@"%@ %@",[RModel getWeek],RModel.timeStr];
//        }else {
//            weakSelf.timeArr = @[];
//        }
//    };
//    //模拟不可用 日期 数据
//    //VC.items = self.DataArrM;
//    VC.items = @[];
//    [self.navigationController pushViewController:VC animated:YES];
}

- (UIView *)makeWorkView{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 20;
    _recomandNumberL = [self createLabelWithTitle:@"当地有24位推荐官为您服务"
                                       titleColor:HexColor(0x383232)
                                         fontSize:18];
    [_recomandNumberL changeStrings:@[@"24"] toColor:HexColor(0xFF983A)];
    
    UIImageView *imgv1 = [UIImageView new];
    imgv1.image = [UIImage imageNamed:@"return_icon"];
    UILabel *label1 = [self createLabelWithTitle:@"始发地"
                                     titleColor:HexColor(0x666666)
                                       fontSize:13];
    UIImageView *imgv2 = [UIImageView new];
    imgv2.image = [UIImage imageNamed:@"time_icon"];
    UILabel *label2 = [self createLabelWithTitle:@"出行日期"
                                     titleColor:HexColor(0x666666)
                                       fontSize:13];
    UIImageView *imgv3 = [UIImageView new];
    imgv3.image = [UIImage imageNamed:@"person_icon"];
    UILabel *label3 = [self createLabelWithTitle:@"出行人数"
                                     titleColor:HexColor(0x666666)
                                       fontSize:13];
    UIImageView *imgv4 = [UIImageView new];
    imgv4.image = [UIImage imageNamed:@"serive_icon"];
    UILabel *label4 = [self createLabelWithTitle:@"所需服务"
                                     titleColor:HexColor(0x666666)
                                       fontSize:13];
    
    UIView *bgView1 = UIView.new;
    bgView1.backgroundColor = HexColor(0xf8f9fb);;
    bgView1.layer.cornerRadius = 4;
    UIImageView *arrow1 = [UIImageView new];
    arrow1.image = [UIImage imageNamed:@"arrow_left_icon"];
    _addressL = [self createLabelWithTitle:@"请选择出发地点"
                                titleColor:HexColor(0x333333)
                                  fontSize:17];
    _addressL.font = [UIFont boldSystemFontOfSize:17];
    [bgView1 addTapGestureTarget:self action:@selector(selectAddress:)];
    [bgView1 sd_addSubviews:@[_addressL,arrow1]];
    
    UIView *bgView2 = UIView.new;
    bgView2.backgroundColor = HexColor(0xf8f9fb);
    bgView2.layer.cornerRadius = 4;
    
    UILabel *_dateStartL = [self createLabelWithTitle:@"07月3日"
                                  titleColor:HexColor(0x333333)
                                    fontSize:17];
    [_dateStartL addTapGestureTarget:self action:@selector(selectTimeIsClicked:)];
    self.startDateLabel = _dateStartL;
    UILabel *_timeStartL = [self createLabelWithTitle:@"周五 9:00"
                                  titleColor:HexColor(0x666666)
                                    fontSize:12];
    [_timeStartL addTapGestureTarget:self action:@selector(selectTimeIsClicked:)];
    self.startTimeLabel = _timeStartL;
    UILabel *_dateStopL = [self createLabelWithTitle:@"07月4日"
                                 titleColor:HexColor(0x333333)
                                   fontSize:17];
    [_dateStopL addTapGestureTarget:self action:@selector(selectTimeIsClicked:)];
    self.stopDateLabel = _dateStopL;
    
    UILabel *_timeStopL = [self createLabelWithTitle:@"周六 9:00"
                                  titleColor:HexColor(0x666666)
                                    fontSize:12];
    [_timeStopL addTapGestureTarget:self action:@selector(selectTimeIsClicked:)];
    self.stopTimeLabel = _timeStopL;
    UIImageView *arrow2 = [UIImageView new];
    arrow2.image = [UIImage imageNamed:@"arrow_icon"];
    [bgView2 sd_addSubviews:@[_dateStartL,_timeStartL,
                              arrow2,
                              _dateStopL,_timeStopL]];
    
    _numberField = [UITextField new];
    _numberField.backgroundColor = HexColor(0xf8f9fb);
    _numberField.layer.cornerRadius = 4;
    _numberField.font = [UIFont systemFontOfSize:14];
    _numberField.textColor = HexColor(0x333333);
    _numberField.placeholder = @"请填写出行人数";
    _numberField.keyboardType = UIKeyboardTypeNumberPad;
    _numberField.clearButtonMode = UITextFieldViewModeAlways;
    _numberField.delegate = self;

    
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 30)];
    _numberField.leftView=emptyView;
    _numberField.leftViewMode=UITextFieldViewModeAlways;
    
    
    EMTextView *textView = [[EMTextView alloc] init];
    textView.backgroundColor = HexColor(0xf8f9fb);
    //textView.delegate = self;
    textView.placeholder = @"填写你的更多信息，车辆/住宿/游玩线路等";
    textView.font = [UIFont systemFontOfSize:14];
    textView.returnKeyType = UIReturnKeySend;
    self.recomandTextview = textView;
    
    UIButton *_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btn setTitle:@"去发布" forState:UIControlStateNormal];
    _btn.titleLabel.font = [UIFont boldSystemFontOfSize:19];
    [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn setBackgroundImage:[UIImage imageNamed:@"rect_bg"] forState:UIControlStateNormal];
    [_btn setBackgroundImage:[UIImage imageNamed:@"rect_bg_sel-1"] forState:UIControlStateSelected];
    [_btn setSelected:YES];
    [_btn.layer setMasksToBounds:YES];
    [_btn.layer setCornerRadius:23];
    [_btn addTarget:self action:@selector(publishIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *tipLabel = [Utils createLabelWithTitle:@"请您填写正确的需求信息" titleColor:HexColor(0xFF9E47) fontSize:14];
    tipLabel.hidden = YES;
    
    UIImageView *bottomImgv = [UIImageView new];
    bottomImgv.image = [UIImage imageNamed:@"travel_img_1"];
    
    

    [view sd_addSubviews:@[_recomandNumberL,
                                       imgv1,label1,
                                       bgView1,
                                       imgv2,label2,
                                       bgView2,
                                       imgv3,label3,
                                       _numberField,
                                       imgv4,label4,
                                       _recomandTextview,
                                       _btn,
                                       tipLabel,
                                       bottomImgv]];
    
    _recomandNumberL.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(view, 24)
    .autoHeightRatio(0);
    [_recomandNumberL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label1.sd_layout
    .leftSpaceToView(view, 41)
    .topSpaceToView(_recomandNumberL, 25)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv1.sd_layout
    .centerYEqualToView(label1)
    .rightSpaceToView(label1, 3)
    .widthIs(12)
    .heightIs(12);
    
    bgView1.sd_layout
    .topSpaceToView(label1, 8)
    .leftEqualToView(imgv1)
    .rightSpaceToView(view, 25)
    .heightIs(40);
    
    _addressL.sd_layout
    .leftSpaceToView(bgView1, 16)
    .topSpaceToView(bgView1, 8)
    .bottomSpaceToView(bgView1, 8)
    .rightSpaceToView(arrow1,8);
    
    arrow1.sd_layout
    .centerYEqualToView(bgView1)
    .rightSpaceToView(bgView1, 10)
    .widthIs(20)
    .heightEqualToWidth();
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(bgView1, 23)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv2.sd_layout
    .centerYEqualToView(label2)
    .leftEqualToView(imgv1)
    .widthIs(12)
    .heightIs(12);
    
    bgView2.sd_layout
    .topSpaceToView(label2, 8)
    .leftEqualToView(bgView1)
    .rightEqualToView(bgView1)
    .heightIs(80);
    
    _dateStartL.sd_layout
    .leftSpaceToView(bgView2, 16)
    .topSpaceToView(bgView2, 16)
    .autoHeightRatio(0);
    [_dateStartL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _timeStartL.sd_layout
    .centerXEqualToView(_dateStartL)
    .topSpaceToView(_dateStartL, 9)
    .autoHeightRatio(0);
    [_timeStartL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow2.sd_layout
    .centerXEqualToView(bgView2)
    .centerYEqualToView(bgView2)
    .widthIs(46)
    .heightIs(5);
    
    _dateStopL.sd_layout
    .topSpaceToView(bgView2, 16)
    .rightSpaceToView(bgView2, 16)
    .autoHeightRatio(0);
    [_dateStopL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _timeStopL.sd_layout
    .centerXEqualToView(_dateStopL)
    .topSpaceToView(_dateStopL, 9)
    .autoHeightRatio(0);
    [_timeStopL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    
    label3.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(bgView2, 16)
    .heightIs(18);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv3.sd_layout
    .centerYEqualToView(label3)
    .leftEqualToView(imgv1)
    .widthIs(12)
    .heightIs(12);
    
    _numberField.sd_layout
    .topSpaceToView(label3, 8)
    .leftEqualToView(bgView1)
    .rightEqualToView(bgView1)
    .heightIs(40);
    
    label4.sd_layout
    .topSpaceToView(_numberField, 25)
    .leftEqualToView(label1)
    .heightIs(18);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv4.sd_layout
    .leftEqualToView(imgv1)
    .centerYEqualToView(label4)
    .widthIs(12)
    .heightIs(12);
    
    _recomandTextview.sd_layout
    .leftEqualToView(imgv1)
    .topSpaceToView(label4, 8)
    .rightEqualToView(bgView1)
    .heightIs(130);
    
    tipLabel.sd_layout
    .topSpaceToView(_recomandTextview, 20)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _btn.sd_layout
    .topSpaceToView(tipLabel, 17)
    .centerXEqualToView(view)
    .widthIs(295)
    .heightIs(46);
    
    bottomImgv.sd_layout
    .topSpaceToView(_btn, 16)
    .centerXEqualToView(view)
    .widthIs(233)
    .heightIs(14);
    [view setupAutoHeightWithBottomViewsArray: @[bottomImgv] bottomMargin:15];
    return view;
}

- (UILabel *)createLabelWithTitle:(NSString *)title
                       titleColor:(UIColor *)color
                         fontSize:(CGFloat)size{
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:size];
    label.textColor = color;
    label.text = title;
    return label;
}

- (UIView *)createPromptViewWithIcon:(NSString *)iconName
                               title:(NSString *)title{
    UIImageView *imgv = [UIImageView new];
    imgv.image = [UIImage imageNamed:iconName];
    UILabel *label = [self createLabelWithTitle:title
                                     titleColor:[UIColor colorWithHex:0x666666]
                                       fontSize:13];
    UIView *view = [[UIView alloc] init];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 0)
    .widthIs(13)
    .heightEqualToWidth();
    label.sd_layout
    .leftSpaceToView(imgv, 3)
    .centerYEqualToView(view)
    .rightSpaceToView(view, 0)
    .autoHeightRatio(0);
    return view;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = textField.text;
    str = [str stringByReplacingCharactersInRange:range withString:string];
    NSInteger num = [str integerValue];
    if(num > 10){
        [MBProgressHUD showMessage:@"控制游客在10人以内"];
        return NO;
    }else{
        return YES;
    }
}

@end
