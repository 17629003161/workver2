//
//  AddrSearchCell.h
//  TravelNew
//
//  Created by mac on 2020/10/10.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddrSearchCell : BaseCustomTableViewCell
@property(nonatomic,strong) AMapPOI *model;
@property(nonatomic,strong) UILabel *nameLable;
@property(nonatomic,strong) UILabel *addressLabel;
- (void)changeTextColor:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
