//
//  AddrSearchCell.m
//  TravelNew
//
//  Created by mac on 2020/10/10.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AddrSearchCell.h"

@implementation AddrSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(AMapPOI *)model
{
    _model = model;
    self.nameLable.text = _model.name;
    self.addressLabel.text = _model.address;
    [self.nameLable updateLayout];
    [self.addressLabel updateLayout];
}

- (void)configViews
{
    self.nameLable = [Utils createLabelWithTitle:@"西安喜来登大酒店" titleColor:HexColor(333333) fontSize:14];
    self.addressLabel = [Utils createLabelWithTitle:@"莲湖区未央路32号" titleColor:HexColor(0x999999) fontSize:10];
    [self.contentView sd_addSubviews:@[_nameLable,_addressLabel]];
    _nameLable.sd_layout
    .topSpaceToView(self.contentView, 8)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);
    
    _addressLabel.sd_layout
    .leftEqualToView(_nameLable)
    .topSpaceToView(_nameLable, 2)
    .rightEqualToView(_nameLable)
    .autoHeightRatio(0);
    [self setupAutoHeightWithBottomView:_addressLabel bottomMargin:8];
}

- (void)changeTextColor:(NSString *)text
{
    [self.nameLable changeStrings:@[text] toColor:HexColor(0x2473FF)];
}

@end
