//
//  LKAddressSelectCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKAddressSelectCtrl.h"
#import "SelectCityCtrl.h"
#import "LKDistrictManger.h"
#import "ScenicSearchEntity.h"
#import "AddrSearchCell.h"

@interface LKAddressSelectCtrl ()<UITextFieldDelegate,MAMapViewDelegate,AMapSearchDelegate>{
    //UIImageView *_targetAddrImgv;
}
@property(nonatomic,strong) UIImageView *centerImaggeView;
@property(nonatomic,strong)  UITextField *addrField;
@property(nonatomic, strong) AMapSearchAPI *search;
@property(nonatomic,strong) MAPointAnnotation *pointAnnotation;
@property(nonatomic,strong) UILabel *cityLabel;
@property(nonatomic,strong) NSString *cityId;                       //城市id
@property(nonatomic,assign) CLLocationCoordinate2D location;        //位置坐标
@property(nonatomic,strong) LKDistrictManger *districtManager;      //城市区划管理器

@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) NSString *contantString;
@property(nonatomic,strong) NSString *cityName;

@property(nonatomic,strong) UIView *footerView;


@end

@implementation LKAddressSelectCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"选择地点" titleColor:nil];
//    self.edgesForExtendedLayout = UIRectEdgeTop;
    [self.tableView registerClass:[AddrSearchCell class] forCellReuseIdentifier:@"AddrSearchCell"];
    [self makeTopView];
    [self layout];
    _districtManager = [[LKDistrictManger alloc] init];
    _pointAnnotation = [[MAPointAnnotation alloc] init];
    _pointAnnotation.coordinate = CLLocationCoordinate2DMake(39.989631, 116.481018);
    _pointAnnotation.title = @"方恒国际";
    _pointAnnotation.subtitle = @"阜通东大街6号";
    //[_mapView addAnnotation:_pointAnnotation];
    self.addrField.text = self.address;
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    self.cityName = @"西安";
    _footerView = [Utils createNoDataView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate=self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.OnSelectAddressBlock = nil;
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate=nil;
}

- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font
{
    CGRect rect = [string boundingRectWithSize:CGSizeMake(214, 32)//限制最大的宽度和高度
                                       options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading  |NSStringDrawingUsesLineFragmentOrigin//采用换行模式
                                    attributes:@{NSFontAttributeName: font}//传人的字体字典
                                       context:nil];
     
    return rect.size;
}

-(void)setContantString:(NSString *)contantString
{
    _contantString = contantString;
    self.tipLabel.text = [NSString stringWithFormat:@"%@   >", contantString];
    
    CGSize size =  [self sizeWithString:self.tipLabel.text font:self.tipLabel.font];
    CGFloat width = size.width+2 < 130 ? 130 : size.width+2;
    self.tipLabel.bounds = CGRectMake(0, 0, width, self.tipLabel.frame.size.height);
    [self.tipLabel updateLayout];
}


-(LKDistrictManger *)districtManager
{
    if(_districtManager){
        _districtManager = [[LKDistrictManger alloc] init];
    }
    return _districtManager;
}
-(void)transformView:(NSNotification *)aNSNotification
{

}

- (void)selectCityIsClicked:(id)sender
{
    LKWeakSelf
    SelectCityCtrl *vc = [SelectCityCtrl new];
    vc.OnSelectAddress = ^(AMapDistrict * dist) {
        weakSelf.cityName = dist.name;
        self.cityLabel.text = dist.name;
        [self.cityLabel updateLayout];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(dist.center.latitude, dist.center.longitude);
        weakSelf.mapView.centerCoordinate = coord;
    };
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)searchBtnIsClicked:(id)sender{
    [_addrField resignFirstResponder];
    [self doSearch];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchBtnIsClicked:nil];
    return [textField resignFirstResponder];
}


- (void)poperIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)layout{
  //  _targetAddrImgv = ImageViewWithImage(@"target_address");
    
    UIImageView *targetAddrImgv = ImageViewWithImage(@"target_address");
    self.centerImaggeView = targetAddrImgv;
    targetAddrImgv.frame = CGRectMake(0, 0,24,32);
    targetAddrImgv.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0-40);
    
    self.tipLabel = [Utils createLabelWithTitle:@"地址" titleColor:[UIColor whiteColor] fontSize:12];
    [self.tipLabel addTapGestureTarget:self action:@selector(poperIsClicked:)];
    self.tipLabel.textColor = [UIColor whiteColor];
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    self.tipLabel.frame = CGRectMake(0, 0, 114, 29);
    self.tipLabel.layer.cornerRadius = 14.5;
    self.tipLabel.layer.masksToBounds = YES;
    self.tipLabel.backgroundColor = HexColor(0xFE8300);
    self.tipLabel.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0-48);
    [self.view sd_addSubviews:@[self.tableView,self.mapView,targetAddrImgv,self.tipLabel]];
    
    self.tableView.sd_layout
    .topSpaceToView(self.view, 61)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    self.mapView.sd_layout
    .topSpaceToView(self.view, 61)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    self.tableView.hidden = YES;
    self.mapView.hidden = NO;
}

- (void)makeTopView{
    UIImageView *imgv1 = [UIImageView new];
    imgv1.image = [UIImage imageNamed:@"addr"];

    LKLocationManager *manager = [LKLocationManager sharedManager];
    UILabel *cityL = [Utils createLabelWithTitle:manager.cityName
                                      titleColor:HexColor(0x333333)
                                        fontSize:15];
    self.cityLabel = cityL;
    [cityL addTapGestureTarget:self action:@selector(selectCityIsClicked:)];
    UIImageView *imgv2 = [UIImageView new];
    imgv2.image = [UIImage imageNamed:@"arrow_down"];
    
    _addrField = [UITextField new];
    _addrField.placeholder = @"请输入您的出游地址";
    _addrField.backgroundColor = HexColor(0xf8f9fb);
    _addrField.layer.cornerRadius = 16;
    _addrField.clearButtonMode = UITextFieldViewModeAlways;
    _addrField.delegate = self;
    
    UIButton *addrBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addrBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [addrBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [addrBtn setTitleColor:HexColor(0x2473FF) forState:UIControlStateNormal];
    [addrBtn addTarget:self
                action:@selector(searchBtnIsClicked:)
      forControlEvents:UIControlEventTouchUpInside];
        
    [self.view sd_addSubviews:@[imgv1,cityL,imgv2,_addrField,addrBtn]];
    
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 38, 34)];
    _addrField.leftView=emptyView;
    _addrField.leftViewMode=UITextFieldViewModeAlways;
    UIImageView *imgv3 = ImageViewWithImage(@"search");
    [emptyView addSubview:imgv3];
    
    imgv3.sd_layout
    .centerYEqualToView(emptyView)
    .centerXEqualToView(emptyView)
    .widthIs(14)
    .heightEqualToWidth();
    
    imgv1.sd_layout
    .topSpaceToView(self.view, 22)
    .leftSpaceToView(self.view, 20)
    .widthIs(12)
    .heightIs(16);
    
    cityL.sd_layout
    .centerYEqualToView(imgv1)
    .leftSpaceToView(self.view, 37)
    .autoHeightRatio(0);
    [cityL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv2.sd_layout
    .centerYEqualToView(imgv1)
    .leftSpaceToView(self.view, 71)
    .widthIs(10)
    .heightIs(6);
    
    addrBtn.sd_layout
    .centerYEqualToView(imgv1)
    .rightSpaceToView(self.view, 14)
    .widthIs(30)
    .heightIs(22);
    
    _addrField.sd_layout
    .centerYEqualToView(imgv1)
    .leftSpaceToView(self.view, 95)
    .rightSpaceToView(addrBtn, 6)
    .heightIs(38);
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 48;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddrSearchCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AddrSearchCell" forIndexPath:indexPath];
    AMapPOI *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    [cell changeTextColor:_addrField.text];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 52;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AMapPOI *poi = [self.dataArray objectAtIndex:indexPath.row];
//    if(self.OnSearchResult){
//        self.OnSearchResult(poi);
//    }
    if(self.OnSelectAddressBlock){
        CLLocationCoordinate2D location;
        location.latitude = poi.location.latitude;
        location.longitude = poi.location.longitude;
        if([poi.name containsString:self.cityName]){
            self.OnSelectAddressBlock(poi.name,poi.citycode,poi.city,location);
        }else{
            NSString *name = [NSString stringWithFormat:@"%@⋅%@",self.cityName,poi.name];
            self.OnSelectAddressBlock(name,poi.citycode,poi.city,location);
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self.navigationController popToRootViewControllerAnimated:YES];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *strContent = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self doSearch];
    //DLog(@"当前输入内容：%@", strContent);
    if([strContent isBlankString]){
        self.mapView.delegate = self;
        self.mapView.hidden = NO;
        self.tipLabel.hidden = NO;
        self.centerImaggeView.hidden = NO;
        self.tableView.hidden = YES;
    }else{
        self.mapView.delegate = nil;
        self.mapView.hidden = YES;
        self.tipLabel.hidden = YES;
        self.centerImaggeView.hidden = YES;
        self.tableView.hidden = NO;
    }
    return YES;
}

//反编码
- (void)addressAntiEncoder:(CLLocationCoordinate2D)loc2d {
    //创建地理编码对象
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    //创建位置
    CLLocation *location=[[CLLocation alloc]initWithLatitude:loc2d.latitude
                                                   longitude:loc2d.longitude];
    LKWeakSelf
    //反地理编码
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        //判断是否有错误或者placemarks是否为空
        if (error !=nil || placemarks.count==0) {
            DLog(@"%@",error);
            [self arroundSearchWhithCoordinate:loc2d];
            return ;
        }
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",
                             placemark.thoroughfare,
                             placemark.locality,
                             placemark.subLocality,
                             placemark.administrativeArea,
                             placemark.postalCode,
                             placemark.country];
        DLog(@"%@", address);
            //赋值详细地址
        weakSelf.contantString = placemark.name;
        weakSelf.cityLabel.text = placemark.locality;
        [self.districtManager searchkeywords:placemark.locality onSelectResult:^(AMapDistrict * dist) {
            weakSelf.cityId = dist.citycode;
            if(self.OnSelectAddressBlock){
                if([placemark.name containsString:self.cityName]){
                    self.OnSelectAddressBlock(placemark.name,weakSelf.cityId,self.cityName,loc2d);
                }else{
                    NSString *name = [NSString stringWithFormat:@"%@⋅%@",self.cityName,placemark.name];
                    self.OnSelectAddressBlock(name,weakSelf.cityId,self.cityName,loc2d);
                }
            }
        }];
    }];
}

#pragma mark - 搜索周边
- (void)arroundSearchWhithCoordinate:(CLLocationCoordinate2D)coordinate{
    self.search =  [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location            = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    request.keywords            = @"";
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    [self.search AMapPOIAroundSearch:request];
}



- (void)doSearch{
    NSString *keywords = [_addrField.text trimWhitespace];
    AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
    request.keywords            = keywords;
    request.city                = self.cityName;
    request.types               = @"建筑|学校|商店|医院|道路|桥梁|小区|街道|便利店|影院|商场|广场|餐馆|车站|城市|旅游景点|景区";
    request.requireExtension    = NO;
    /*  搜索SDK 3.2.0 中新增加的功能，只搜索本城市的POI。*/
    request.cityLimit           = NO;
    request.requireSubPOIs      = YES;
    [self.search AMapPOIKeywordsSearch:request];
}


#pragma mark - AMapSearchDelegate
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    [self.dataArray removeAllObjects];
    if (response.pois.count == 0)
    {
        self.tableView.tableFooterView = _footerView;
        [self.tableView reloadData];
        return;
    }else{
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    for (AMapPOI *poi in response.pois) {
        DLog(@"pois:%@\n",poi.name);
        [self.dataArray addObject:poi];
    }
    [self.tableView reloadData];
    
    AMapPOI *poi = [response.pois firstObject];
    self.contantString = poi.name;
    CLLocationCoordinate2D location;
    location.latitude = poi.location.latitude;
    location.longitude = poi.location.longitude;
    if(self.OnSelectAddressBlock){
        if([poi.name containsString:self.cityName]){
            self.OnSelectAddressBlock(poi.name,poi.citycode,poi.city,location);
        }else{
            NSString *name = [NSString stringWithFormat:@"%@⋅%@",self.cityName,poi.name];
            self.OnSelectAddressBlock(name,poi.citycode,poi.city,location);
        }
    }
}


#pragma mark - MAMapViewDelegate
- (void)mapView:(MAMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    self.tipLabel.hidden = YES;
}


- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    self.tipLabel.hidden = NO;
    MACoordinateRegion region;
    CLLocationCoordinate2D centerCoordinate = self.mapView.region.center;
    region.center= centerCoordinate;
    _pointAnnotation.coordinate = centerCoordinate;
    DLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
//    [self setLocationWithLatitude:centerCoordinate.latitude AndLongitude:centerCoordinate.longitude];
    [self addressAntiEncoder:centerCoordinate];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAUserLocation class]]) {
        
        MAUserLocationRepresentation *r = [[MAUserLocationRepresentation alloc] init];
        r.showsAccuracyRing = NO;///精度圈是否显示，默认YES
        [self.mapView updateUserLocationRepresentation:r];
        
        static NSString *userLocationStyleReuseIndetifier = @"userLocationStyleReuseIndetifier";
        MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
        if (annotationView == nil) {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"address_icon3"];
        return annotationView;
    }
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"target_address"];
        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        return annotationView;
    }
    
    return nil;
}




@end
