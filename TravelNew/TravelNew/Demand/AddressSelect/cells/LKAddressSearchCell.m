//
//  LKAddressSearchCell.m
//  TravelNew
//
//  Created by mac on 2020/9/22.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKAddressSearchCell.h"

@implementation LKAddressSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{ self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self configViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)configViews{
    _upLabel = [Utils createLabelWithTitle:@"西安喜来登大酒店"
                                  titleColor:HexColor(0x333333)
                                    fontSize:14];
    _downLabel = [Utils createLabelWithTitle:@"莲湖区未央路32号"
                                   titleColor:HexColor(0x999999)
                                     fontSize:10];
    [self.contentView sd_addSubviews:@[_upLabel,_downLabel]];
    _upLabel.sd_layout
    .leftSpaceToView(self.contentView, 20)
    .topSpaceToView(self.contentView, 8)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);

    _downLabel.sd_layout
    .leftEqualToView(_upLabel)
    .topSpaceToView(_upLabel, 2)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);
   
}
@end
