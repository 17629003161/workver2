//
//  LKAddressSearchCell.h
//  TravelNew
//
//  Created by mac on 2020/9/22.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKAddressSearchCell : UITableViewCell
@property(nonatomic,strong) UILabel *upLabel;
@property(nonatomic,strong) UILabel *downLabel;
@end

NS_ASSUME_NONNULL_END
