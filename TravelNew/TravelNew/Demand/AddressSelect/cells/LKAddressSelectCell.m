//
//  LKAddressSelectCell.m
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKAddressSelectCell.h"

@interface LKAddressSelectCell(){
}

@end

@implementation LKAddressSelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{ self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self configViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)configViews{
    _leftLabel = [Utils createLabelWithTitle:@"利君未来城二期"
                                  titleColor:HexColor(0x333333)
                                    fontSize:14];
    _rightLabel = [Utils createLabelWithTitle:@"距我300m"
                                   titleColor:HexColor(0x999999)
                                     fontSize:10];
    [self.contentView sd_addSubviews:@[_leftLabel,_rightLabel]];
    _leftLabel.sd_layout
    .leftSpaceToView(self.contentView, 40)
    .centerYEqualToView(self.contentView)
    .autoHeightRatio(0);
    [_leftLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    _rightLabel.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);
    [_rightLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

@end
