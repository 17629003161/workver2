//
//  LKAddressSelectCell.h
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKAddressSelectCell : UITableViewCell
@property(nonatomic,strong) UILabel *leftLabel;
@property(nonatomic,strong) UILabel *rightLabel;
@end

NS_ASSUME_NONNULL_END
