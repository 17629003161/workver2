//
//  LKAddressSelectCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKAddressSelectCtrl : LKBaseTableViewController
@property (nonatomic, nullable,copy) void(^OnSelectAddressBlock)(NSString *name,NSString *cityid,NSString *cityName,CLLocationCoordinate2D location);  //传递1个参数
@property (nonatomic, strong) NSString *address;
@end

NS_ASSUME_NONNULL_END
