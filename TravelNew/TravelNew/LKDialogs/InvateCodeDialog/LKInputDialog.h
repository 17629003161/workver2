//
//  InvateCodeDialog.h
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^OkBlock)(NSString *);
typedef void (^CancleBlock)(void);

@interface LKInputDialog : LKBaseDialogView
@property(nonatomic, strong) OkBlock okBlock;
@property(nonatomic, copy) CancleBlock cancleBlock;

- (void)setTitle:(NSString *)title
        subTitle:(NSString *)subTitle
     placeHolder:(NSString *)placeHolder;

@end

NS_ASSUME_NONNULL_END
