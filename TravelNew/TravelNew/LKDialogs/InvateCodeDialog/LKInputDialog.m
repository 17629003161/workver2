//
//  InvateCodeDialog.m
//  TravelNew
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKInputDialog.h"

@interface LKInputDialog (){
    UIView *_line;
}
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *subTitleLabel;
@property(nonatomic,strong) UITextField *textField;

@end

@implementation LKInputDialog

- (void)viewIsClicked:(UITapGestureRecognizer *)reg
{
    [_textField resignFirstResponder];
}

- (void)setTitle:(NSString *)title
        subTitle:(NSString *)subTitle
     placeHolder:(NSString *)placeHolder
{
    self.titleLabel.text = title;
    self.subTitleLabel.text = subTitle;
    self.textField.placeholder = placeHolder;
    [self.titleLabel updateLayout];
    [self.subTitleLabel updateLayout];
}


- (void)configViews{
    self.backgroundColor = [UIColor whiteColor];
    [self addTapGestureTarget:self action:@selector(viewIsClicked:)];
    UILabel *titleL = [Utils createLabelWithTitle:@"title"
                                       titleColor:HexColor(0x030303)
                                         fontSize:18];
    self.titleLabel = titleL;
    UILabel *subTitleL = [Utils createLabelWithTitle:@"subTitle"
                                         titleColor:HexColor(0x444444)
                                           fontSize:15];
    self.subTitleLabel = subTitleL;
    
    _textField = [UITextField new];
    _textField.textColor = HexColor(0xc5c5c5);
    _textField.font = LKSystemFont(13);
    _textField.layer.borderColor = HexColor(0xc5c5c5).CGColor;
    _textField.layer.borderWidth = 0.5;
    _textField.backgroundColor = HexColor(0xf8f8f8);
    [_textField textLeftOffset:6];
    
    _line = [UIView new];
    _line.backgroundColor = HexColor(0xededed);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xededed);
    
    UIButton *btnCancle  = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancle setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    btnCancle.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancle addTarget:self action:@selector(btnCancleIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnOk  = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOk setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    btnOk.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnOk setTitle:@"确定" forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(btnOkIsClicked:) forControlEvents:UIControlEventTouchUpInside];

    [self sd_addSubviews:@[titleL,
                           subTitleL,
                           _textField,
                           _line,
                           btnCancle,line2,btnOk]];
    
    titleL.sd_layout
    .topSpaceToView(self, 20)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleL.sd_layout
    .topSpaceToView(titleL, 18)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [subTitleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _textField.sd_layout
    .topSpaceToView(subTitleL, 20)
    .leftSpaceToView(self, 17)
    .rightSpaceToView(self, 17)
    .heightIs(30);
    
    _line.sd_layout
    .topSpaceToView(self, 136)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(1.0);
    
    btnCancle.sd_layout
    .leftSpaceToView(self, 50)
    .topSpaceToView(_line, 10)
    .widthIs(36)
    .heightIs(26);
    
    line2.sd_layout
    .centerXEqualToView(self)
    .bottomEqualToView(self)
    .topSpaceToView(_line, 0)
    .widthIs(0.5);

    btnOk.sd_layout
    .centerYEqualToView(btnCancle)
    .rightSpaceToView(self, 50)
    .widthIs(36)
    .heightIs(26);
    
    [self setupAutoHeightWithBottomViewsArray:@[btnOk,btnCancle] bottomMargin:10];
    
}

- (void)btnCancleIsClicked:(id)sender{
    [self hide];
}

- (void)btnOkIsClicked:(id)sender{
    [self hide];
    if(self.okBlock){
        self.okBlock(_textField.text);
    }
}







@end
