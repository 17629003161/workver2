//
//  LKMessageDialog.m
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKMessageDialog.h"

@interface LKMessageDialog()
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *contentLabel;
@property(nonatomic,strong) UIButton *leftBtn;
@property(nonatomic,strong) UIButton *rightBtn;

@end


@implementation LKMessageDialog

- (void)cancleIsClicked:(id)sender
{
    if(self.cancleBlock)
        self.cancleBlock();
    [super hide];
}
- (void)okIsClicked:(id)sender
{
    if(self.okBlock)
        self.okBlock();
    [super hide];
}

+ (instancetype)createMessageDialogWithTitle:(NSString *)title
                                     content:(NSString *)content
                                leftBtnTitle:(NSString *)leftBtnTitle
                               rightBtnTitle:(NSString *)rightBtnTitle{
    LKMessageDialog *dlg = [LKMessageDialog new];
    dlg.titleLabel.text = title;
    dlg.contentLabel.text = content;
    [dlg.leftBtn setTitle:leftBtnTitle forState:UIControlStateNormal];
    [dlg.rightBtn setTitle:rightBtnTitle forState:UIControlStateNormal];
    [dlg.titleLabel updateLayout];
    [dlg.contentLabel updateLayout];
    return dlg;
}



- (void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    UILabel *titleLabel = [Utils createBoldLabelWithTitle:@"确认拨打110报警吗？" titleColor:HexColor(0x030303) fontSize:18];
    self.titleLabel = titleLabel;
    UILabel *contantLabel = [Utils createLabelWithTitle:@"虚假报警将承担法律责任，处以拘留，谨慎使用" titleColor:HexColor(0x444444) fontSize:15];
    self.contentLabel = contantLabel;
    contantLabel.numberOfLines = 0;
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xEDEDED);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xEDEDED);
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.titleLabel.font = LKSystemFont(17);
    [btnCancel setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btnCancel setTitle:@"再想想" forState:UIControlStateNormal];
    self.leftBtn = btnCancel;
    [btnCancel addTarget:self action:@selector(cancleIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.titleLabel.font = LKSystemFont(17);
    [btnOk setTitleColor:HexColor(0x0076FF) forState:UIControlStateNormal];
    [btnOk setTitle:@"确认拨打" forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(okIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.rightBtn = btnOk;
    [self sd_addSubviews:@[
                           titleLabel,
                           contantLabel,
                           line1,
                           btnCancel,line2,btnOk]];
    titleLabel.sd_layout
    .topSpaceToView(self, 22)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contantLabel.sd_layout
    .topSpaceToView(titleLabel, 8)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .autoHeightRatio(0);
    
    line1.sd_layout
    .topSpaceToView(contantLabel, 20)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(0.5);
    
    line2.sd_layout
    .topEqualToView(line1)
    .bottomEqualToView(self)
    .centerXEqualToView(self)
    .widthIs(0.5);
    
    btnCancel.sd_layout
    .topSpaceToView(line1, 10)
    .leftSpaceToView(self, 8)
    .rightSpaceToView(line2, 8)
    .heightIs(30);
    
    btnOk.sd_layout
    .topSpaceToView(line1, 10)
    .leftSpaceToView(line2, 8)
    .rightSpaceToView(self, 8)
    .heightIs(30);
    [self setupAutoHeightWithBottomView:btnOk bottomMargin:10];
}

@end
