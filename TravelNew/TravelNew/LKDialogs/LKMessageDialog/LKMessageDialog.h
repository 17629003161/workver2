//
//  LKMessageDialog.h
//  TravelNew
//
//  Created by mac on 2020/11/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKMessageDialog : LKBaseDialogView

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *leftBtnTitle;
@property(nonatomic,strong) NSString *rightBtnTitle;

@property(nonatomic,copy) void (^okBlock)(void);
@property(nonatomic,copy) void (^cancleBlock)(void);

+ (instancetype)createMessageDialogWithTitle:(NSString *)title
                                     content:(NSString *)content
                                leftBtnTitle:(NSString *)leftBtnTitle
                               rightBtnTitle:(NSString *)rightBtnTitle;


@end

NS_ASSUME_NONNULL_END
