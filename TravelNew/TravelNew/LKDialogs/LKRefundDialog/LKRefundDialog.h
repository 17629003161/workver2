//
//  LKCancleDialog.h
//  TravelNew
//
//  Created by mac on 2020/12/1.
//  Copyright © 2020 lester. All rights reserved.
//

//订单退款

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKRefundDialog : LKBaseDialogView
@property(nonatomic,strong) NSString *message;
@property(nonatomic,assign) double refund;          //退款金额
@property(nonatomic,assign) double offMoney;        //扣款金额
@property(nonatomic,copy) void (^okBlock)(NSString *resion);
@property(nonatomic,copy) void (^cancleBlock)(void);
@property(nonatomic,copy) void (^refundRulesBlock)(void);
@end

NS_ASSUME_NONNULL_END
