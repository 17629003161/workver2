//
//  LKCancleDialog.m
//  TravelNew
//
//  Created by mac on 2020/12/1.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKRefundDialog.h"

@interface LKRefundDialog()<UITextFieldDelegate>
@property(nonatomic,strong) NSMutableArray *fieldArray;
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UITextField *reasionField;
@property(nonatomic,strong) UILabel *contentLabel;
@property(nonatomic,strong) UILabel *refundLabel;
@end

@implementation LKRefundDialog

- (void)cancleIsClicked:(id)sender
{
    if(self.cancleBlock)
        self.cancleBlock();
    [super hide];
}
- (void)okIsClicked:(id)sender
{
    if(self.okBlock){
        if(self.selectIndex < self.fieldArray.count){
            UITextField *field = self.fieldArray[self.selectIndex];
            self.okBlock(field.text);
        }else{
            self.okBlock(self.reasionField.text);
        }
    }
    [super hide];
}

- (NSMutableArray *)fieldArray
{
    if(!_fieldArray){
        _fieldArray = [NSMutableArray new];
    }
    return _fieldArray;
}

-(void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    for (int i=0; i<self.fieldArray.count; i++) {
        UITextField *field = self.fieldArray[i];
        if(i==_selectIndex){
            field.layer.borderColor = HexColor(0xFA8919).CGColor;
            field.textColor = HexColor(0xFA8919);
        }else{
            field.layer.borderColor = HexColor(0xDCDCDC).CGColor;
            field.textColor = HexColor(0x323232);
        }
    }
}

- (UILabel *)createTextLabel:(NSString *)title
{
    UILabel *label = [UILabel new];
    label.font = LKSystemFont(16);
    label.textColor = HexColor(0x323232);
    label.text = title;
    label.layer.cornerRadius = 4;
    label.layer.borderColor = HexColor(0xDCDCDC).CGColor;
    label.layer.borderWidth = 1.0;
    label.layer.masksToBounds = YES;
    return label;
}

- (void)fieldIsClicked:(UITapGestureRecognizer *)reg
{
    UILabel *label = (UILabel *)reg.view;
    self.selectIndex = label.tag;
}

- (void)maskIsTyped:(UITapGestureRecognizer *)reg
{
    [self.reasionField resignFirstResponder];
}


-(void)setMessage:(NSString *)message
{
    _message = message;
    self.contentLabel.text = message;
    [self.contentLabel updateLayout];
}

-(void)setRefund:(double)refund
{
    _refund = refund;
    self.refundLabel.text = [NSString stringWithFormat:@"%.2lf元",refund];
    [self.refundLabel updateLayout];
}

-(void)setOffMoney:(double)offMoney
{
    _offMoney = offMoney;
    NSString *offMoneyStr = [NSString stringWithFormat:@"%.2lf元",offMoney];
    self.contentLabel.text = [NSString stringWithFormat:@"您确认要取消订单吗？为保障推荐官权益，根据《旅咖出行订单退改规则》本次取消订单将扣除您%@",offMoneyStr];
    [self.contentLabel changeStrings:@[@"《旅咖出行订单退改规则》"] toColor:HexColor(0x0076FF)];
    [self.contentLabel changeStrings:@[offMoneyStr] toColor:HexColor(0x0076FF)];
    [self.contentLabel updateLayout];
}

- (void)refundRulesIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
    if(self.refundRulesBlock)
        self.refundRulesBlock();
}

- (void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    UILabel *titleLabel = [Utils createBoldLabelWithTitle:@"取消订单" titleColor:HexColor(0x030303) fontSize:25];
    UILabel *contentLabel = [Utils createLabelWithTitle:@"您确认要取消订单吗？为保障推荐官权益，根据《旅咖出行订单退改规则》本次取消订单将扣除您 0元"
                                             titleColor:HexColor(0x444444)
                                               fontSize:15];
    [contentLabel addTapGestureTarget:self action:@selector(refundRulesIsClicked:)];
    self.contentLabel = contentLabel;
    UILabel *refundTipLabel = [Utils createLabelWithTitle:@"共计退款" titleColor:HexColor(0x030303) fontSize:15];
    UILabel *refundLabel = [Utils createLabelWithTitle:@"0元" titleColor:HexColor(0xFE2E2E) fontSize:15];
    self.refundLabel = refundLabel;
    UILabel *resionTipLabel = [Utils createLabelWithTitle:@"取消原因" titleColor:HexColor(0x818181) fontSize:13];
    self.contentLabel = contentLabel;
    UILabel *label0 = [self createTextLabel:@"  我不想旅行了"];
    label0.tag = 0;
    [label0 addTapGestureTarget:self action:@selector(fieldIsClicked:)];
    
    UILabel *label1 = [self createTextLabel:@"  行程有冲突"];
    label1.tag = 1;
    [label1 addTapGestureTarget:self action:@selector(fieldIsClicked:)];
    
    UILabel *label2 = [self createTextLabel:@"  推荐官临时有事"];
    label2.tag = 2;
    [label2 addTapGestureTarget:self action:@selector(fieldIsClicked:)];
    label2.enabled = NO;
    
    UITextField *field = [UITextField new];
    field.font = LKSystemFont(16);
    field.textColor = HexColor(0x323232);
    field.layer.cornerRadius = 4;
    field.layer.borderColor = HexColor(0xDCDCDC).CGColor;
    field.layer.borderWidth = 1.0;
    field.layer.masksToBounds = YES;
    field.placeholder = @"其他，请填写";
    [field textLeftOffset:5];
    self.reasionField = field;
    [self.fieldArray addObjectsFromArray:@[label0,label1,label2]];
    contentLabel.numberOfLines = 0;
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xEDEDED);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xEDEDED);
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.titleLabel.font = LKSystemFont(17);
    [btnCancel setTitleColor:HexColor(0x999999) forState:UIControlStateNormal];
    [btnCancel setTitle:@"再想想" forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(cancleIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.titleLabel.font = LKSystemFont(17);
    [btnOk setTitleColor:HexColor(0x0076FF) forState:UIControlStateNormal];
    [btnOk setTitle:@"确定" forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(okIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self sd_addSubviews:@[titleLabel,
                           contentLabel,
                           refundTipLabel,refundLabel,
                           resionTipLabel,
                           label0,
                           label1,
                           label2,
                           field,
                           line1,
                           btnCancel,line2,btnOk]];
    titleLabel.sd_layout
    .topSpaceToView(self, 20)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentLabel.sd_layout
    .topSpaceToView(titleLabel, 8)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .autoHeightRatio(0);
    
    refundTipLabel.sd_layout
    .topSpaceToView(contentLabel, 8)
    .leftEqualToView(contentLabel)
    .autoHeightRatio(0);
    [refundTipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    refundLabel.sd_layout
    .centerYEqualToView(refundTipLabel)
    .leftSpaceToView(refundTipLabel, 2)
    .autoHeightRatio(0);
    [refundLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    resionTipLabel.sd_layout
    .leftEqualToView(refundTipLabel)
    .topSpaceToView(refundTipLabel, 10)
    .autoHeightRatio(0);
    [resionTipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label0.sd_layout
    .topSpaceToView(resionTipLabel, 20)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(38);
    
    label1.sd_layout
    .topSpaceToView(label0, 10)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(38);
    
    label2.sd_layout
    .topSpaceToView(label1, 10)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(38);
    
    field.sd_layout
    .topSpaceToView(label2, 10)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(38);
    
    
    line1.sd_layout
    .topSpaceToView(field, 20)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(0.5);
    
    line2.sd_layout
    .topEqualToView(line1)
    .centerXEqualToView(self)
    .heightIs(40)
    .widthIs(0.5);
    
    btnCancel.sd_layout
    .topSpaceToView(line1, 10)
    .leftSpaceToView(self, 8)
    .rightSpaceToView(line2, 8)
    .heightIs(30);
    
    btnOk.sd_layout
    .topSpaceToView(line1, 10)
    .leftSpaceToView(line2, 8)
    .rightSpaceToView(self, 8)
    .heightIs(30);
    [self setupAutoHeightWithBottomView:line2 bottomMargin:10];
    
    self.selectIndex = 0;
}

@end
