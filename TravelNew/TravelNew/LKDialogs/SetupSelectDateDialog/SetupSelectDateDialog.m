//
//  SetupSelectDateDialog.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SetupSelectDateDialog.h"

@interface SetupSelectDateDialog ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIPickerView *_pickView;
    NSMutableArray *_yearArray;
    NSMutableArray *_monthArray;
    NSMutableArray *_dayArray;
    int _endYear;
    int _endMonth;
    int _endDay;
    
    int _startYear;
    int _selectedYear;
    int _selectedMonth;
    int _selectedDay;
}

@property(nonatomic,strong) NSString *date;


@end
    
@implementation SetupSelectDateDialog

- (void)cancleBtnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)okBtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.selectDateBlock){
        NSString *str = [NSString stringWithFormat:@"%04d-%02d-%-2d",_selectedYear,_selectedMonth,_selectedDay];
        self.selectDateBlock(str);
    }
    [self hide];
}

- (void)LoadDateFormYear:(NSInteger)year{
    _yearArray = [NSMutableArray new];
    _monthArray = [NSMutableArray new];
    _dayArray = [NSMutableArray new];
    LKDateUtils *date = [LKDateUtils today];
    _endYear = (int)date.year;
    _endMonth = (int)date.month;
    _endDay = (int)date.day;
    
    _selectedYear = _endYear;
    _selectedMonth = _endMonth;
    _selectedDay = _endDay;
    
    for(NSInteger i=year;i<=_endYear;i++){
        [_yearArray addObject:@(i)];
    }
    [_monthArray addObjectsFromArray:@[@(1),@(2),@(3),@(4),@(5),@(6),@(7),@(8),@(9),@(10),@(11),@(12)]];
    [self initDayArrayInYear:_selectedYear month:_selectedMonth];
    [_pickView selectRow:(_selectedYear-_startYear) inComponent:0 animated:YES];
    [_pickView selectRow:_selectedMonth-1 inComponent:1 animated:YES];
    [_pickView selectRow:_selectedDay-1 inComponent:2 animated:YES];
}

- (void)initDayArrayInYear:(int)year month:(int)month{
    [_dayArray removeAllObjects];
    int days = [self getDaysInYear:year month:month];
    for(int i=1;i<=days;i++){
        [_dayArray addObject:@(i)];
    }
}

- (int)getDaysInYear:(int)year month:(int)month {
    // imonth == 0的情况是应对在CourseViewController里month-1的情况
    if((month == 0)||(month == 1)||(month == 3)||(month == 5)||(month == 7)||(month == 8)||(month == 10)||(month == 12))
        return 31;
    if((month == 4)||(month == 6)||(month == 9)||(month == 11))
        return 30;
    if((year%4 == 1)||(year%4 == 2)||(year%4 == 3))
    {
        return 28;
    }
    if(year%400 == 0)
        return 29;
    if(year%100 == 0)
        return 28;
    return 29;
}



-(void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"取消" titleColor:HexColor(0x666666) fontSize:14];
    [label1 addTapGestureTarget:self action:@selector(cancleBtnIsClicked:)];
    UILabel *label2 = [Utils createLabelWithTitle:@"请选择" titleColor:HexColor(0x323232) fontSize:17];
    UILabel *label3 = [Utils createLabelWithTitle:@"确认" titleColor:HexColor(0x4D86FD) fontSize:14];
    [label3 addTapGestureTarget:self action:@selector(okBtnIsClicked:)];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    [self sd_addSubviews:@[label1,label2,label3,
                           _pickView]];
    label1.sd_layout
    .topSpaceToView(self, 20)
    .leftSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerYEqualToView(label2)
    .rightSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label2, 24)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(138);
    [self setupAutoHeightWithBottomView:_pickView bottomMargin:24];
    _startYear = 1930;
    [self LoadDateFormYear:_startYear];
}

#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _yearArray.count;
    }else if(component==1) {
        return _monthArray.count;
    }else if(component==2){
        return _dayArray.count;
    }
    return 0;
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return (kScreenWidth-20)/3.0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [NSString stringWithFormat:@"%@",_yearArray[row]];
    }else if(component==1){
        return [NSString stringWithFormat:@"%@",_monthArray[row]];
    }else{
        return [NSString stringWithFormat:@"%@",_dayArray[row]];
    }
}

//当改变省份时，重新加载第2列的数据，部分加载
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        _selectedYear = [_yearArray[row] intValue];
        [self initDayArrayInYear:_selectedYear month:_selectedMonth];
        [_pickView reloadComponent:2];
    }else if(component==1){
        _selectedMonth = [_monthArray[row] intValue];
        [self initDayArrayInYear:_selectedYear month:_selectedMonth];
        [_pickView reloadComponent:2];
    }else{
        _selectedDay = [_dayArray[row] intValue];
    }
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    
}



@end
