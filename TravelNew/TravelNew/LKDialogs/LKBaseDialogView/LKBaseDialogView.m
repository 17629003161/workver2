//
//  LKBaseDialogView.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

@implementation LKBaseDialogView

// 步骤3 在initWithFrame:方法中添加子控件
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 注意：该处不要给子控件设置frame与数据，可以在这里初始化子控件的属性
        self.isShowing = NO;
        [self configViews];
    }
    return self;
}

-(UIView *)maskView
{
    if(!_maskView){
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
        [_maskView addTapGestureTarget:self action:@selector(maskIsTyped:)];
    }
    return _maskView;
}


- (void)configViews{
    self.backgroundColor = [UIColor whiteColor];
}


// 步骤4 在`layoutSubviews`方法中设置子控件的`frame`（在该方法中一定要调用`[super layoutSubviews]`方法）
- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self.maskView];
    self.maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
    self.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0);
    [keyWindow addSubview:self];
    self.isShowing = YES;
    [self makeTopAngleSize:CGSizeMake(30, 30) bounds:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}


- (void)maskIsTyped:(UITapGestureRecognizer *)reg
{
    
}

- (void)hide{
    [self removeFromSuperview];
    [self.maskView removeFromSuperview];
    self.isShowing = NO;
}

@end
