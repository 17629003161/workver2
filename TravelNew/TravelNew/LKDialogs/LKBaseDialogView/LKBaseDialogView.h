//
//  LKBaseDialogView.h
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKBaseDialogView : UIView
@property(nonatomic,strong) UIView *maskView;
@property(nonatomic,assign) BOOL isShowing;
- (void)configViews;
- (void)show;
- (void)hide;
@end

NS_ASSUME_NONNULL_END
