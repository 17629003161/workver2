//
//  LKIDSampleDialg.m
//  TravelNew
//
//  Created by mac on 2021/2/2.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKIDSampleDialg.h"

@implementation LKIDSampleDialg

- (void)okIsClicked:(id)sender
{
    [super hide];
}

- (void)configViews{
    [super configViews];
    self.layer.cornerRadius = 8;
    UILabel *titleLabel = [Utils createBoldLabelWithTitle:@"温馨提示" titleColor:HexColor(0x030303) fontSize:18];
    UILabel *contantLabel = [Utils createLabelWithTitle:@"档案编号在驾驶证副页，档案编号后方的数字编码，如图" titleColor:HexColor(0x444444) fontSize:15];
    contantLabel.numberOfLines = 0;
    UIImageView *imagv = ImageViewWithImage(@"sample_idcard");
    
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEDEDED);
    
    UIButton *btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.titleLabel.font = LKSystemFont(17);
    [btnOk setTitleColor:HexColor(0x0076FF) forState:UIControlStateNormal];
    [btnOk setTitle:@"我知道了" forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(okIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self sd_addSubviews:@[
                           titleLabel,
                           contantLabel,
                           imagv,
                           line,
                           btnOk]];
    titleLabel.sd_layout
    .topSpaceToView(self, 22)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contantLabel.sd_layout
    .topSpaceToView(titleLabel, 8)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .autoHeightRatio(0);
    
    imagv.sd_layout
    .topSpaceToView(contantLabel, 14)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(148);
    
    line.sd_layout
    .topSpaceToView(imagv, 20)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(0.5);
    
    btnOk.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(line, 10)
    .widthIs(70)
    .heightIs(24);
    
    [self setupAutoHeightWithBottomView:btnOk bottomMargin:10];
}
@end
