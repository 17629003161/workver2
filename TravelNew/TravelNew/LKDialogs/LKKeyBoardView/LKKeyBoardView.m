//
//  LKKeyBoardView.m
//  TravelNew
//
//  Created by mac on 2021/1/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKKeyBoardView.h"

@interface LKKeyBoardView()
@property(nonatomic,strong) NSMutableArray *keyArray;
@end

@implementation LKKeyBoardView

- (void)configViews{
    self.keyArray = [NSMutableArray new];
    self.backgroundColor = [UIColor whiteColor];
    self.frame = CGRectMake(0, 0, kScreenWidth, 332);
    UILabel *label = [Utils createLabelWithTitle:@"选择车牌归属地" titleColor:HexColor(0x323232) fontSize:17];
    UIImageView *closeImgv = ImageViewWithImage(@"close_x");
    [closeImgv addTapGestureTarget:self action:@selector(closeBtnClicked:)];
    
    NSArray *ary = @[@"京",@"津",@"晋",@"冀",@"蒙",@"辽",@"吉",@"黑",@"沪",@"苏",@"浙",@"皖",@"闽",@"赣",@"鲁",@"豫",@"鄂",@"湘",@"粤",@"桂",@"琼",@"渝",@"川",@"贵",@"云",@"藏",@"陕",@"甘",@"青",@"宁",@"新"];
    
    NSInteger w = 40;
    NSInteger gap = 10;
    CGFloat hmargin = (kScreenWidth - w*7 - gap*6)/2.0;
    CGFloat offset = 60;
    for(int i=0; i<ary.count; i++){
        int col = i % 7;
        int row = i/7;
        NSString *title = ary[i];
        LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:title font:LKSystemFont(17) cornerRadius:4];
        btn.isEnabled = NO;
        btn.isEfectReverse = YES;
        btn.titleLabel.textColor = HexColor(0x666666);
        btn.frame = CGRectMake(hmargin+col*(w+gap), row *(w+gap)+offset , w, w);
        [btn addTapGestureTarget:self action:@selector(keyIsClicked:)];
        [self.keyArray addObject:btn];
    }
    [self sd_addSubviews:@[label,closeImgv]];
    [self sd_addSubviews:self.keyArray];
    label.sd_layout
    .topSpaceToView(self, 20)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    closeImgv.sd_layout
    .topSpaceToView(self, 5)
    .rightSpaceToView(self, 5)
    .widthIs(30)
    .heightIs(30);
}

- (void)closeBtnClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)keyIsClicked:(UITapGestureRecognizer *)reg
{
    LKGadentButton *btn = (LKGadentButton *)reg.view;
    if(self.onKeyBoardClickedBlock){
        self.onKeyBoardClickedBlock(btn.titleLabel.text);
        [self hide];
    }
}


// 步骤4 在`layoutSubviews`方法中设置子控件的`frame`（在该方法中一定要调用`[super layoutSubviews]`方法）
- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self.maskView];
    self.maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
    [keyWindow addSubview:self];
    self.sd_layout
    .leftEqualToView(keyWindow)
    .bottomEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .heightIs(332);
}


- (void)maskIsTyped:(UITapGestureRecognizer *)reg
{
    
}

- (void)hide{
    [self removeFromSuperview];
    [self.maskView removeFromSuperview];
}
@end
