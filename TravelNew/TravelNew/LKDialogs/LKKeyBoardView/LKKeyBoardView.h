//
//  LKKeyBoardView.h
//  TravelNew
//
//  Created by mac on 2021/1/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKKeyBoardView : LKBaseDialogView
@property(nonatomic,copy) void (^onKeyBoardClickedBlock)(NSString *key);
@end

NS_ASSUME_NONNULL_END
