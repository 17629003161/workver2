//
//  SetupSelectCityDlg.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SetupSelectCityDlg.h"

@interface SetupSelectCityDlg ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSMutableArray *_dataArray;
    NSMutableArray *_citiesArray;
    UIPickerView *_pickView;
}
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *provence;


@end
    
@implementation SetupSelectCityDlg

- (void)cancleBtnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)okBtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.selectCityBlock)
        self.selectCityBlock(self.provence,self.city);
    [self hide];
}

-(void)configViews{
    [super configViews];
    [self loadData];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"取消" titleColor:HexColor(0x666666) fontSize:14];
    [label1 addTapGestureTarget:self action:@selector(cancleBtnIsClicked:)];
    UILabel *label2 = [Utils createLabelWithTitle:@"请选择" titleColor:HexColor(0x323232) fontSize:17];
    UILabel *label3 = [Utils createLabelWithTitle:@"确认" titleColor:HexColor(0x4D86FD) fontSize:14];
    [label3 addTapGestureTarget:self action:@selector(okBtnIsClicked:)];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    [self sd_addSubviews:@[label1,label2,label3,
                           _pickView]];
    label1.sd_layout
    .topSpaceToView(self, 20)
    .leftSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerYEqualToView(label2)
    .rightSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label2, 24)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(138);
    [self setupAutoHeightWithBottomView:_pickView bottomMargin:24];
}

#pragma mark - loadData
- (void)loadData {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"city" ofType:@"plist"];
    _dataArray = [NSMutableArray arrayWithContentsOfFile:path];
    _citiesArray = _dataArray[0][@"cities"];
    
    self.provence = _dataArray[0][@"state"];
    self.city = _citiesArray[0];
}

#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _dataArray.count;
    }else {
        return _citiesArray.count;
    }
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return 150;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [NSString stringWithFormat:@"%@",_dataArray[row][@"state"]];
    }else {
        return [NSString stringWithFormat:@"%@",_citiesArray[row]];
    }
}

//当改变省份时，重新加载第2列的数据，部分加载
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        self.provence = _dataArray[row][@"state"];
        _citiesArray = _dataArray[row][@"cities"];
        [_pickView reloadComponent:1];
    }else if(component==1){
        self.city = _citiesArray[row];
    }
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    
}

@end
