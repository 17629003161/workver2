//
//  SetupSelectCityDlg.h
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetupSelectCityDlg : LKBaseDialogView
@property(nonatomic,copy) void (^selectCityBlock)(NSString *,NSString *);
@end

NS_ASSUME_NONNULL_END
