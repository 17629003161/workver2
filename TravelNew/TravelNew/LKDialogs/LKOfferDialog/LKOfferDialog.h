//
//  LKOfferDialog.h
//  TravelNew
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LKChatModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^OkBlock)(LKChatModel *);

@interface LKOfferDialog : UIView
@property(nonatomic, strong) OkBlock okBlock;
@property(nonatomic,strong) LKChatModel *model;
- (void)show;
- (void)hide;
@end

NS_ASSUME_NONNULL_END
