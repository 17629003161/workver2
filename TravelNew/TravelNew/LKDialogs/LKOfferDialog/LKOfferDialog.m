//
//  LKOfferDialog.m
//  TravelNew
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKOfferDialog.h"

@interface LKOfferDialog (){
    UIView *_maskView;
}
@property(nonatomic, strong) UITextField *priceField;
@property(nonatomic, strong) UITextView *contentView;
@end

@implementation LKOfferDialog

// 步骤3 在initWithFrame:方法中添加子控件
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 注意：该处不要给子控件设置frame与数据，可以在这里初始化子控件的属性
        self.frame = CGRectMake(0, 0, 325, 271);
        self.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0);
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8.0;
        [self configViews];
    }
    return self;
}


- (void)btnIsClicked:(id)sender
{
    if([_priceField.text length]==0){
        [MBProgressHUD showMessage:@"请输入报价"];
        return;
    }
    if(_contentView.text.length==0){
        [MBProgressHUD showMessage:@"请输入服务明细"];
        return;
    }
    [self doBaojia];
//    if(self.okBlock){
//        self.model.offerPrice = [NSNumber numberWithDouble:[_priceField.text doubleValue]];
//        self.model.offerDetail = _contentView.text;
//        self.okBlock(self.model);
//        [self doBaojia];
//    }
    [self hide];
}

- (void)doBaojia{
    LKWeakSelf
    DLog(@"%@",self.priceField.text);
    CGFloat price = [self.priceField.text floatValue]*100;
    NSInteger money = (NSInteger)price;
    Api *api = [Api apiPostUrl:@"work/bill/add"
                          para:@{@"ltRequirementInfoId":self.model.ltRequirementInfoId,
                                 @"money":@(money),
                                 @"remark":_contentView.text
                          }
                   description:@"报价"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        DLog(@"json = %@",json);
        if([[json objectForKey:@"code"] integerValue]==200){
            NSNumber *offerId = [json objectForKey:@"data"];
            weakSelf.model.offerPriceId = offerId;
            if(weakSelf.okBlock){
                CGFloat price = [self.priceField.text floatValue]*100;
                NSInteger money = (NSInteger)price;
                weakSelf.model.offerPrice = @(money);
                weakSelf.model.requirementDescription = weakSelf.contentView.text;
                weakSelf.okBlock(self.model);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
        }];
}

- (void)closeIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)configViews{
    UIImageView *bgimgv = ImageViewWithImage(@"bg_Fill1");
    self.userInteractionEnabled = YES;
    UIImageView *clearImgv = ImageViewWithImage(@"icon_clear");
    self.userInteractionEnabled = YES;
    [clearImgv addTapGestureTarget:self action:@selector(closeIsClicked:)];
    UILabel *titleL = [Utils createLabelWithTitle:@"订单报价"
                                       titleColor:HexColor(0x030303)
                                         fontSize:18];
    UILabel *subTitleL = [Utils createLabelWithTitle:@"您将对此次行程向游客进行报价，此次行程共计"
                                         titleColor:HexColor(0x333333)
                                           fontSize:14];
    
    _priceField = [UITextField new];
    _priceField.placeholder = @"输入报价";
    _priceField.textColor = HexColor(0xc5c5c5);
    _priceField.font = LKSystemFont(13);
    _priceField.layer.borderColor = HexColor(0xc5c5c5).CGColor;
    _priceField.layer.borderWidth = 0.5;
    _priceField.backgroundColor = HexColor(0xf8f8f8);
    _priceField.clearButtonMode = UITextFieldViewModeAlways;
    //_priceField.keyboardType = UIKeyboardTypeDecimalPad;
    [_priceField textLeftOffset:6];
    
    
    UILabel *unitLabel = [Utils createLabelWithTitle:@"元" titleColor:HexColor(0xFF9739) fontSize:14];
    
    _contentView = [UITextView new];
    
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认发送" font:LKSystemFont(14) cornerRadius:16];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];

    [self sd_addSubviews:@[bgimgv,
                           titleL,clearImgv,
                           subTitleL,
                           _priceField,unitLabel,
                           _contentView,
                           btn]];
    bgimgv.sd_layout
    .topEqualToView(self)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .bottomEqualToView(self);
    
    titleL.sd_layout
    .topSpaceToView(self, 20)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    clearImgv.sd_layout
    .topSpaceToView(self, 16)
    .rightSpaceToView(self, 16)
    .widthIs(16.0)
    .heightIs(16.0);
    
    subTitleL.sd_layout
    .topSpaceToView(titleL, 18)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [subTitleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _priceField.sd_layout
    .topSpaceToView(subTitleL,6)
    .leftSpaceToView(self, 15)
    .widthIs(93)
    .heightIs(30);
    
    unitLabel.sd_layout
    .centerYEqualToView(_priceField)
    .leftSpaceToView(_priceField, 3)
    .autoHeightRatio(0);
    [unitLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _contentView.sd_layout
    .topSpaceToView(_priceField, 15)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(64);

    btn.sd_layout
    .topSpaceToView(_contentView, 19)
    .centerXEqualToView(self)
    .widthIs(180)
    .heightIs(32);
    
    [self setupAutoHeightWithBottomViewsArray:@[btn] bottomMargin:22];
    
}


// 步骤4 在`layoutSubviews`方法中设置子控件的`frame`（在该方法中一定要调用`[super layoutSubviews]`方法）
- (void)layoutSubviews
{
    [super layoutSubviews];
}


- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    if(!_maskView){
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
        [_maskView addTapGestureTarget:self action:@selector(maskIsTyped:)];
    }
    [keyWindow addSubview:_maskView];
    [keyWindow addSubview:self];
}

- (void)maskIsTyped:(UITapGestureRecognizer *)reg
{
    [self.priceField resignFirstResponder];
    [self.contentView resignFirstResponder];
}

- (void)hide{
    [self removeFromSuperview];
    [_maskView removeFromSuperview];
}


@end
