//
//  SetupSelectSexDialog.m
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SetupSelectSexDialog.h"

@interface SetupSelectSexDialog ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSMutableArray *_dataArray;
}
@property(nonatomic,strong) UIPickerView *pickView;

@end
    
@implementation SetupSelectSexDialog

- (void)cancleBtnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)okBtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.selectSexBlock && self.sex)
        self.selectSexBlock(self.sex);
    [self hide];
}

-(void)configViews{
    [super configViews];
    _dataArray = [NSMutableArray arrayWithArray:@[@"男",@"女"]];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"取消" titleColor:HexColor(0x666666) fontSize:14];
    [label1 addTapGestureTarget:self action:@selector(cancleBtnIsClicked:)];
    UILabel *label2 = [Utils createLabelWithTitle:@"请选择" titleColor:HexColor(0x323232) fontSize:17];
    UILabel *label3 = [Utils createLabelWithTitle:@"确认" titleColor:HexColor(0x4D86FD) fontSize:14];
    [label3 addTapGestureTarget:self action:@selector(okBtnIsClicked:)];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    [self sd_addSubviews:@[label1,label2,label3,
                           _pickView]];
    label1.sd_layout
    .topSpaceToView(self, 20)
    .leftSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerYEqualToView(label2)
    .rightSpaceToView(self, 13)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label2, 24)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(138);
    [self setupAutoHeightWithBottomView:_pickView bottomMargin:24];
}

-(void)setSex:(NSString *)sex
{
    _sex = sex;
    if([sex isEqualToString:@"男"]){
        [self.pickView selectRow:0 inComponent:0 animated:YES];
    }else{
        [self.pickView selectRow:1 inComponent:0 animated:YES];
    }
}

#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _dataArray.count;
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return 150;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",_dataArray[row]];
}

//当改变省份时，重新加载第2列的数据，部分加载
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
        _sex = _dataArray[row];
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    
}




@end
