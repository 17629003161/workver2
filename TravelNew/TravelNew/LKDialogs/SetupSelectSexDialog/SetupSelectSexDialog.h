//
//  SetupSelectSexDialog.h
//  TravelNew
//
//  Created by mac on 2020/11/4.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetupSelectSexDialog : LKBaseDialogView
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,copy) void (^selectSexBlock)(NSString *);
@end

NS_ASSUME_NONNULL_END
