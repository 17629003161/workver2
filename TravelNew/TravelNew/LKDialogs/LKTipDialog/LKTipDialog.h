//
//  LKTipDialog.h
//  TravelNew
//
//  Created by mac on 2020/12/29.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKTipDialog : LKBaseDialogView
@property(nonatomic,strong) NSString *message;
@end

NS_ASSUME_NONNULL_END
