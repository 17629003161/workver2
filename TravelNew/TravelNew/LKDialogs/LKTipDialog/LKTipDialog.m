//
//  LKTipDialog.m
//  TravelNew
//
//  Created by mac on 2020/12/29.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKTipDialog.h"

@interface LKTipDialog()
@property(nonatomic,strong) UILabel *msgLabel;
@end

@implementation LKTipDialog

- (void)okIsClicked:(id)sender
{
    [super hide];
}

- (void)show{
    if(self.message){
        self.msgLabel.text = self.message;
        [self.msgLabel updateLayout];
    }
    [super show];
}

- (void)configViews{
    [super configViews];
    self.layer.cornerRadius = 8;
    UILabel *titleLabel = [Utils createBoldLabelWithTitle:@"温馨提示" titleColor:HexColor(0x030303) fontSize:18];
    UILabel *contantLabel = [Utils createLabelWithTitle:@"您现在是推荐官身份，暂不支持发布需求，切换身份后再发布需求吧～" titleColor:HexColor(0x444444) fontSize:15];
    self.msgLabel = contantLabel;
    contantLabel.numberOfLines = 0;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEDEDED);
    
    UIButton *btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOk.titleLabel.font = LKSystemFont(17);
    [btnOk setTitleColor:HexColor(0x0076FF) forState:UIControlStateNormal];
    [btnOk setTitle:@"我知道了" forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(okIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self sd_addSubviews:@[
                           titleLabel,
                           contantLabel,
                           line,
                           btnOk]];
    titleLabel.sd_layout
    .topSpaceToView(self, 22)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contantLabel.sd_layout
    .topSpaceToView(titleLabel, 8)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .autoHeightRatio(0);
    
    line.sd_layout
    .topSpaceToView(contantLabel, 20)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(0.5);
    
    btnOk.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(line, 10)
    .widthIs(70)
    .heightIs(24);
    
    [self setupAutoHeightWithBottomView:btnOk bottomMargin:10];
}


@end
