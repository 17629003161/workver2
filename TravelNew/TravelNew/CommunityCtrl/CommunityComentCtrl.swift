//
//  CommunityComentCtrl.swift
//  TravelNew
//
//  Created by mac on 2021/4/21.
//  Copyright © 2021 lester. All rights reserved.
// 6217 0041 6001 6738 355

import UIKit

class CommunityComentCtrl: UIViewController {
    var maskview : UIView?
    var imgview : UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI(){
        imgview = UIImageView()
        imgview?.image = UIImage(named:"bg_recommender")
        imgview?.frame = self.view.bounds
        view.addSubview(imgview!)
        maskview = UIView();
        maskview?.backgroundColor = UIColor.clear
        maskview?.frame = imgview!.bounds
        view.addSubview(maskview!)
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(hexString: "#afafaf")
        btn.layer.cornerRadius = 18
        btn.layer.masksToBounds = true;
        btn.setTitle("说点什么吧～", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        maskview?.addSubview(btn)
        btn.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.bottom.equalTo(-38.5)
            make.right.equalTo(-102)
            make.height.equalTo(36)
        }
    }

}
