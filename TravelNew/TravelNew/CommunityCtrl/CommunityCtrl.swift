//
//  CommunityCtrl.swift
//  TravelNew
//
//  Created by mac on 2021/4/20.
//  Copyright © 2021 lester. All rights reserved.
//

import UIKit

class CommunityCtrl: UIViewController {

    lazy var itemArray = [String]()
    var headImgv : UIImageView?
    var chatImgv : UIImageView?
    var btnNote : UIButton?
    var btnSel : UIButton?
    lazy var btnAry : [UIButton] = []
    var index : Int = 0{
        willSet(newValue){
            for i in 0..<self.btnAry.count {
                let btn = self.btnAry[i]
                if(newValue==i){
                    btn.setTitleColor(UIColor(hexString: "#1E69FF"), for: .normal)
                }else{
                    btn.setTitleColor(UIColor(hexString: "#383232"), for: .normal)
                }
            }
        }
    }
    
    @objc func btnIsClicked(_ btn : UIButton){
        self.index = btn.tag
    }
    
    @objc func handleTap(tap:UITapGestureRecognizer){
        print("tap test")
    }
    
    lazy var topView : UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 22, width: ScreenWidth, height: 44)
        self.headImgv = UIImageView()
        self.headImgv?.image = #imageLiteral(resourceName: "default_header")
        self.headImgv?.frame = CGRect(x: 15, y: 11, width: 21.95, height: 21.95)
        view.addSubview(self.headImgv!)
        self.chatImgv = UIImageView()
        self.chatImgv?.image = #imageLiteral(resourceName: "location_blue")
        self.chatImgv?.frame = CGRect(x: ScreenWidth-22-15, y: 11, width: 22, height: 22)
        self.chatImgv?.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tap:)))
        self.chatImgv?.addGestureRecognizer(tap)
        view.addSubview(self.chatImgv!)
        self.btnNote = UIButton(type: .custom)
        self.btnNote?.tag = 0;
        self.btnNote?.setTitle("关注", for: .normal)
        self.btnNote?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.btnNote?.setTitleColor(UIColor(hexString: "#383232"), for: .normal)
        self.btnNote?.addTarget(self, action:#selector(btnIsClicked(_:)) , for: .touchUpInside)
        view.addSubview(self.btnNote!)
        self.btnAry.append(self.btnNote!)
        self.btnSel = UIButton(type: .custom)
        self.btnSel?.tag = 1
        self.btnSel?.setTitle("精选", for: .normal)
        self.btnSel?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.btnSel?.setTitleColor(UIColor(hexString: "#383232"), for: .normal)
        view.addSubview(self.btnSel!)
        self.btnSel?.addTarget(self, action:#selector(btnIsClicked(_:)) , for: .touchUpInside)
        self.btnAry.append(self.btnSel!)
        self.btnNote?.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.rightMargin.equalTo(-ScreenWidth/2.0-25)
        })
        self.btnSel?.snp.makeConstraints({ (make) in
            make.centerY.equalToSuperview()
            make.leftMargin.equalTo(ScreenWidth/2.0+25)
        })
        return view
    }()
    lazy var collectionView : UICollectionView = {
        let fl = HWCollectionViewFlowLayout()
        fl.delegate = self
        let collectionView = UICollectionView.init(frame: CGRect(x: 0, y: 66, width: ScreenWidth, height: ScreenHeight-44-TabBarHeight), collectionViewLayout: fl)
        collectionView.register(CommunityCell.self, forCellWithReuseIdentifier: "CommunityCell")
        collectionView.backgroundColor = UIColor.white
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(topView) 
        view.addSubview(collectionView)
        self.index = 0;
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}

extension CommunityCtrl : HWCollectionViewFlowLayoutDelegate {
    func hw_setCellHeght(layout: HWCollectionViewFlowLayout, indexPath: NSIndexPath, itemWidth: CGFloat) -> CGFloat {
        var heigth = itemWidth * (267.0/171.0)
        if(indexPath.item == 1){
            heigth = itemWidth * (217/171.0)
        }
        return heigth;
    }
}

extension CommunityCtrl : UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return itemArray.count
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunityCell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = CommunityComentCtrl()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
