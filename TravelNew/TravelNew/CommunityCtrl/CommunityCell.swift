//
//  CommunityCell.swift
//  TravelNew
//
//  Created by mac on 2021/4/20.
//  Copyright © 2021 lester. All rights reserved.
//

import UIKit

class CommunityCell: UICollectionViewCell {
    var imageView: UIImageView?
    var headerImgv: UIImageView?
    var eyeImgv: UIImageView?
    var titleLabel: UILabel?
    var nameLabel: UILabel?
    var numberLabel : UILabel?
    
    required init?(coder: NSCoder) {
        fatalError("Cell 初始化失败")
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    func setupUI() -> Void {
        let view = UIView();
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor(hex: 0xefefef)?.cgColor
        view.layer.masksToBounds = true
        view.layer.shadowColor = UIColor(red: 144/255.0, green: 144/255.0, blue: 144/255.0, alpha: 1.0).cgColor
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowOpacity = 1;
        view.layer.shadowRadius = 3;
        view.layer.cornerRadius = 10;
        self.contentView.addSubview(view)
        imageView = UIImageView()
        imageView?.image = UIImage(named: "bg_recommender")
        view.addSubview(imageView!)
        titleLabel = UILabel()
        titleLabel?.text = "【十大自驾游景点】厦门 环岛路"
        titleLabel?.numberOfLines = 0
        titleLabel?.textColor = UIColor.init(hexString: "#202020")
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        view.addSubview(titleLabel!)
        headerImgv = UIImageView()
        headerImgv?.image = UIImage(named: "header_image")
        view.addSubview(headerImgv!)
        nameLabel = UILabel()
        nameLabel?.text = "会飞的鱼"
        nameLabel?.textColor = UIColor.init(hexString: "#303030")
        nameLabel?.font = UIFont.systemFont(ofSize: 12)
        view.addSubview(nameLabel!)
        eyeImgv = UIImageView()
        eyeImgv?.image = UIImage(named: "header_image")
        view.addSubview(eyeImgv!)
        numberLabel = UILabel()
        numberLabel?.text = "237"
        numberLabel?.textColor = UIColor.init(hexString: "#636363")
        numberLabel?.font = UIFont.systemFont(ofSize: 12)
        view.addSubview(numberLabel!)
        view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        imageView?.snp.makeConstraints({ (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(170)
        })
        titleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(imageView!.snp.bottomMargin).offset(15)
            make.left.equalTo(10)
            make.right.equalTo(-10)
        })
        headerImgv?.snp.makeConstraints({ (make) in
            make.left.equalTo(10)
            make.width.height.equalTo(24)
            make.top.equalTo(titleLabel!.snp.bottom).offset(17)
        })
        nameLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(headerImgv!)
            make.leftMargin.equalTo(headerImgv!.snp.rightMargin).offset(5.5)
        })
        numberLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(headerImgv!)
            make.right.equalTo(-10)
        })
        eyeImgv?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(nameLabel!)
            make.right.equalTo(-34.5)
            make.width.equalTo(15.5)
            make.height.equalTo(13)
        })
        
        
    }
}
