//
//  LKTopImageTableViewController.m
//  TravelNew
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKTopImageTableViewController.h"

@interface LKTopImageTableViewController (){
    CGRect orginYframe;
}

@end

@implementation LKTopImageTableViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)setImageHeigt:(CGFloat)ImageHeigt{
    _ImageHeigt = ImageHeigt;
    _bgImage.frame = CGRectMake(0, 0, kScreenWidth, _ImageHeigt);
    _tableHeaderView.frame = CGRectMake(0, 0, kScreenWidth, _ImageHeigt);
    orginYframe = _bgImage.frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_ImageHeigt<=0)
        _ImageHeigt = 164;
    _bgImage = ImageViewWithImage(@"home3");
    [_bgImage setUserInteractionEnabled:YES];
    _bgImage.frame = CGRectMake(0, 0, kScreenWidth, _ImageHeigt);
    orginYframe = _bgImage.frame;
    [self.view addSubview:_bgImage];
    
    _backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 64)];
    _backView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    [self.view addSubview:_backView];
    
    self.tableView.frame = CGRectMake(0, 64, kScreenWidth, kScreenHeight-64);
    self.tableView.backgroundColor = [UIColor clearColor];
    _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, _ImageHeigt)];
    _tableHeaderView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = _tableHeaderView;
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    [self addBackArrow];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.textLabel.text =@"测试一下";
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offSetY = scrollView.contentOffset.y;
    DLog(@"===%f",offSetY);
    if (offSetY < _ImageHeigt)
    {
        _backView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:offSetY/_ImageHeigt];
    }
    else
    {
        _backView.backgroundColor = [UIColor whiteColor];
    }
    if (offSetY > 0)
    {
        _bgImage.frame = ({
            CGRect frame = _bgImage.frame;
            frame.origin.y = orginYframe.origin.y - offSetY;
            frame;
        });
    }
    else
    {
        _bgImage.frame = ({
            CGRect frame = _bgImage.frame;
            frame.size.width =orginYframe.size.width -offSetY;
            frame.size.height = orginYframe.size.height *frame.size.width/ orginYframe.size.width;
            frame.origin.x = -(frame.size.width - orginYframe.size.width)/2;
            DLog(@"----%@",NSStringFromCGRect(frame));
            frame;
        });
    }
    if (offSetY ==0)
    {
    }
}




@end
