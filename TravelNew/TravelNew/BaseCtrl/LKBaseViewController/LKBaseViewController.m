//
//  LKBaseViewController.m
//  TravelNew
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import <MapKit/MapKit.h>
#import "LKAddressNode.h"

@interface LKBaseViewController ()<MAMapViewDelegate>

@end

@implementation LKBaseViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    // 返回你所需要的状态栏样式
    return UIStatusBarStyleDefault;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView=[[UIScrollView alloc]init];
        //设置代理
        //_scrollView.delegate=self;
        //设置分页
        _scrollView.pagingEnabled=NO;
        //去掉滚动条
        _scrollView.showsHorizontalScrollIndicator=NO;
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    return _scrollView;
}

- (MAMapView *)mapView {
    if (!_mapView) {
        _mapView = [[MAMapView alloc] initWithFrame: self.view.bounds];
        _mapView.delegate = self;
        // 显示比例尺
        _mapView.showsScale = NO;
        // 显示指南针
        _mapView.showsCompass = NO;
        // 设置缩放级别
        [_mapView setZoomLevel:12];
        // 设置当前地图的中心点：例如默认地图中心显示坐标为（39.9088230000, 116.3974700000）
      //  _mapView.centerCoordinate = CLLocationCoordinate2DMake(39.9088230000, 116.3974700000);
        _mapView.scrollEnabled = YES;
        
        // 显示定位蓝点
        _mapView.showsUserLocation = YES;
        // 用户定位模式
        _mapView.userTrackingMode = MAUserTrackingModeFollow;
        [self setupCustomMapOptions:_mapView];
    }
    return _mapView;
}
//设置自定义地图
- (void)setupCustomMapOptions:(MAMapView *)mapView{
    //自定义地图
    NSString *path1 = [NSString stringWithFormat:@"%@/style_extra.data", [NSBundle mainBundle].bundlePath];
    NSString *path2 = [NSString stringWithFormat:@"%@/style.data", [NSBundle mainBundle].bundlePath];
    NSData *data1 = [NSData dataWithContentsOfFile:path1];
    NSData *data2 = [NSData dataWithContentsOfFile:path2];
    MAMapCustomStyleOptions *options = [[MAMapCustomStyleOptions alloc] init];
    options.styleData= data2;
    options.styleExtraData= data1;
    [mapView setCustomMapStyleOptions:options];
    [mapView setCustomMapStyleEnabled:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self.navigationController.navigationBar.subviews objectAtIndex:0] setAlpha:0];
    self.iskeyBordScrollUp = NO;
    [self changeBackArrowToBlack];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @"";
    DLog(@"%@",[self class]);
   // self.view.backgroundColor = [UIColor colorWithHex:0xe9e9e9];
    self.view.backgroundColor = [UIColor whiteColor];
    //Y起点在导航条下面
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
#pragma mark ----注册观察者
 //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchRefreshNotification:) name:@"PageRefresh" object:nil];
    //注册观察键盘的变化
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

- (void)addBackArrow{
    UIImageView *backarrow = ImageViewWithImage(@"return_white");
    [backarrow setUserInteractionEnabled:YES];
    backarrow.frame = CGRectMake(20, kStatusBarHeight, 30, 30);
    [backarrow addTapGestureTarget:self action:@selector(backArrowIsClicked:)];
    [self.view addSubview:backarrow];
}

- (void)backArrowIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}
////键盘回收
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for(UIView *view in self.view.subviews)
    {
        [view resignFirstResponder];
    }
}
 
//移动UIView
-(void)transformView:(NSNotification *)aNSNotification
{
    if(!self.iskeyBordScrollUp) return;
    //获取键盘弹出前的Rect
    NSValue *keyBoardBeginBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect beginRect=[keyBoardBeginBounds CGRectValue];

    //获取键盘弹出后的Rect
    NSValue *keyBoardEndBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect  endRect=[keyBoardEndBounds CGRectValue];

    //获取键盘位置变化前后纵坐标Y的变化值
    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
    DLog(@"看看这个变化的Y值:%f",deltaY);

    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画
    [UIView animateWithDuration:0.25f animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+deltaY, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.tintColor = HexColor(0x333333);
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]}forState:UIControlStateNormal];
}

-(void)setTitle:(NSString *)title titleColor:(nullable UIColor *)color
{
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0,20, 80, 44)];
    titleLab.font = LKSystemFont(17);
    titleLab.textAlignment =NSTextAlignmentCenter;
    titleLab.text = title;
    if(color==nil){
        titleLab.textColor = HexColor(0x333333);
    }else{
        titleLab.textColor = color;
    }
     self.navigationItem.titleView = titleLab;
}

- (void)changeBackArrowToBlack{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"left_arrow_black"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = backItem;
}
- (void)changeBackArrowToWhite{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"left_arrow_white"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}


////--------------------------

//导航只需要目的地经纬度，endLocation为纬度、经度的数组
-(void)doNavigationWithEndLocation:(NSArray *)endLocation
{
    //NSArray * endLocation = [NSArray arrayWithObjects:@"26.08",@"119.28", nil];
    NSMutableArray *maps = [NSMutableArray array];
    
    //苹果原生地图-苹果原生地图方法和其他不一样
    NSMutableDictionary *iosMapDic = [NSMutableDictionary dictionary];
    iosMapDic[@"title"] = @"苹果地图";
    [maps addObject:iosMapDic];
    
    
    //百度地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        NSMutableDictionary *baiduMapDic = [NSMutableDictionary dictionary];
        baiduMapDic[@"title"] = @"百度地图";
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%@,%@|name=北京&mode=driving&coord_type=gcj02",endLocation[0],endLocation[1]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        baiduMapDic[@"url"] = urlString;
        [maps addObject:baiduMapDic];
    }
    
    //高德地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
        NSMutableDictionary *gaodeMapDic = [NSMutableDictionary dictionary];
        gaodeMapDic[@"title"] = @"高德地图";
        NSString *urlString = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&lat=%@&lon=%@&dev=0&style=2",@"导航功能",@"nav123456",endLocation[0],endLocation[1]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        gaodeMapDic[@"url"] = urlString;
        [maps addObject:gaodeMapDic];
    }
    
    //谷歌地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSMutableDictionary *googleMapDic = [NSMutableDictionary dictionary];
        googleMapDic[@"title"] = @"谷歌地图";
        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%@,%@&directionsmode=driving",@"导航测试",@"nav123456",endLocation[0], endLocation[1]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        googleMapDic[@"url"] = urlString;
        [maps addObject:googleMapDic];
    }
    
    //腾讯地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"qqmap://"]]) {
        NSMutableDictionary *qqMapDic = [NSMutableDictionary dictionary];
        qqMapDic[@"title"] = @"腾讯地图";
        NSString *urlString = [[NSString stringWithFormat:@"qqmap://map/routeplan?from=我的位置&type=drive&tocoord=%@,%@&to=终点&coord_type=1&policy=0",endLocation[0], endLocation[1]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        qqMapDic[@"url"] = urlString;
        [maps addObject:qqMapDic];
    }
    
    
    //选择
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"选择地图" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSInteger index = maps.count;
    
    for (int i = 0; i < index; i++) {
        
        NSString * title = maps[i][@"title"];
        
        //苹果原生地图方法
        if (i == 0) {
            
            UIAlertAction * action = [UIAlertAction actionWithTitle:title style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [self navAppleMap];
            }];
            [alert addAction:action];
            
            continue;
        }
        
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSString *urlString = maps[i][@"url"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }];
        
        [alert addAction:action];
        
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    
}





//苹果地图
- (void)navAppleMap
{
//    CLLocationCoordinate2D gps = [JZLocationConverter bd09ToWgs84:self.destinationCoordinate2D];
    
    //终点坐标
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(26.08, 119.28);
    
    
    //用户位置
    MKMapItem *currentLoc = [MKMapItem mapItemForCurrentLocation];
    //终点位置
    MKMapItem *toLocation = [[MKMapItem alloc]initWithPlacemark:[[MKPlacemark alloc]initWithCoordinate:loc addressDictionary:nil] ];
    
    
    NSArray *items = @[currentLoc,toLocation];
    //第一个
    NSDictionary *dic = @{
                          MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving,
                          MKLaunchOptionsMapTypeKey : @(MKMapTypeStandard),
                          MKLaunchOptionsShowsTrafficKey : @(YES)
                          };
    //第二个，都可以用
//    NSDictionary * dic = @{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,
//                           MKLaunchOptionsShowsTrafficKey: [NSNumber numberWithBool:YES]};
    
    [MKMapItem openMapsWithItems:items launchOptions:dic];
    
}

- (void)popToViewControllerName:(NSString *)claseName
{
    for(UIViewController *controller in self.navigationController.viewControllers) {
    if([controller isKindOfClass:[NSClassFromString(claseName) class]])
        [self.navigationController popToViewController:controller animated:YES];
    }
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineRenderer *polylineRenderer = [[MAPolylineRenderer alloc] initWithPolyline:overlay];
        
        polylineRenderer.lineWidth    = 8.f;
        polylineRenderer.strokeColor  = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.6];
        polylineRenderer.lineJoinType = kMALineJoinRound;
        polylineRenderer.lineCapType  = kMALineCapRound;
        
        return polylineRenderer;
    }
    return nil;
}

- (void)drawPolayLine:(NSArray *)addrAry
{
    CLLocationCoordinate2D commonPolylineCoords[4];
    for (int i=0; i<addrAry.count; i++) {
        LKAddressNode *node = addrAry[i];
        commonPolylineCoords[i] = node.coordinate;
    }
    //构造折线对象
    MAPolyline *commonPolyline = [MAPolyline polylineWithCoordinates:commonPolylineCoords count:addrAry.count];
    //在地图上添加折线对象
    [self.mapView addOverlay: commonPolyline];
}


@end
