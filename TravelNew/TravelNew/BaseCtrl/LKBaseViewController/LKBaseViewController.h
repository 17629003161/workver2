//
//  LKBaseViewController.h
//  TravelNew
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKBaseViewController : UIViewController
@property(nonatomic,assign) BOOL iskeyBordScrollUp;                //键盘是否上攻页面
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) MAMapView *mapView;
-(void)setTitle:(NSString *)title titleColor:(nullable UIColor *)color;
- (void)changeBackArrowToBlack;
- (void)changeBackArrowToWhite;
//- (void)setStatusBarBackgroundColor:(UIColor *)color;                                  //添加页面视图点击动作观察者，用于消除键盘
//- (void)viewIsClicked:(UITapGestureRecognizer *)reg;
//- (void)catchRefreshNotification:(NSNotification *)notification;
//- (void)sendRefreshNotificationWithObject:(NSDictionary *)object;
//- (void)showNavigationWithClearBG;
- (void)setupCustomMapOptions:(MAMapView *)mapView;
- (void)doNavigationWithEndLocation:(NSArray *)endLocation;              //跳转到第三方地图导航
- (void)popToViewControllerName:(NSString *)claseName;                  //跳转回某个名称的页面
- (void)addBackArrow;
- (void)drawPolayLine:(NSArray *)addrAry;

@end

NS_ASSUME_NONNULL_END
