//
//  BaseSwiftViewController.swift
//  TravelNew
//
//  Created by mac on 2021/4/14.
//  Copyright © 2021 lester. All rights reserved.
//

import UIKit

class BaseSwiftViewController: UIViewController {
    lazy var scrollView : UIScrollView = {
        let scv = UIScrollView()
        scv.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)//设置scrollview的大小
        scv.backgroundColor = UIColor.white//设置背景颜色
        scv.isPagingEnabled = false //是否支持分页
        scv.bounces = true //是否支持回弹效果
        scv.showsVerticalScrollIndicator = true //垂直滑动线隐藏
        scv.showsHorizontalScrollIndicator = true //水平滑动线隐藏
        //scrollView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10) //设置内边距
        scv.contentSize = CGSize(width:ScreenWidth,height:ScreenHeight*2)
        return scv
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
