//
//  BaseCustomView.h
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCustomView : UIView
- (void)configViews;
@end

NS_ASSUME_NONNULL_END
