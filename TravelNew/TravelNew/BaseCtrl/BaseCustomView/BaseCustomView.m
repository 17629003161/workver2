//
//  BaseCustomView.m
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomView.h"

@implementation BaseCustomView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 注意：该处不要给子控件设置frame与数据，可以在这里初始化子控件的属性
        [self configViews];
    }
    return self;
}

- (void)configViews{
}

@end
