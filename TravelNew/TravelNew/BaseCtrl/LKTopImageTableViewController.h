//
//  LKTopImageTableViewController.h
//  TravelNew
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKTopImageTableViewController : LKBaseTableViewController
@property(nonatomic,strong) UIImageView *bgImage;
@property(nonatomic,assign) CGFloat ImageHeigt;
@property(nonatomic,strong) UIView *backView;
@property(nonatomic,strong) UIView *tableHeaderView;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
@end

NS_ASSUME_NONNULL_END
