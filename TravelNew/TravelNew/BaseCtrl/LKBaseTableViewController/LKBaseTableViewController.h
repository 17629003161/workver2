//
//  LKBaseTableViewController.h
//  TravelNew
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKBaseTableViewController : LKBaseViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign) NSInteger pageIndex;
@property(nonatomic,assign) NSInteger pageSize;

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *dataArray;
- (Api *)getListApi;                    //在子类中重写此方法
- (NSString *)getListEntityName;        //在子类中重写此方法
- (void)loadFirstPage;
- (void)loadMorePage;
- (void)showEndView:(BOOL)isShow;
- (void)checkBlankView;    //没有数据的时候显示的提示视图
- (void)disabelPageRefresh;     //禁止页面刷新

@end

NS_ASSUME_NONNULL_END
