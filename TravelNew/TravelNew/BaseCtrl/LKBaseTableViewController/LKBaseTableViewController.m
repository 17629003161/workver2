//
//  LKBaseTableViewController.m
//  TravelNew
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"

@interface LKBaseTableViewController ()
@property(nonatomic,strong) UIView *blankView;
@end

@implementation LKBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"%@",[self class]);
    // Do any additional setup after loading the view.
    [self LayoutViews];
    self.pageIndex = 1;
    self.pageSize = 10;
}

- (void)disabelPageRefresh
{
    self.tableView.mj_header = nil;
    self.tableView.mj_footer = nil;
}


- (void)LayoutViews{
    if(!_dataArray){
        _dataArray = [[NSMutableArray alloc] init];
    }
   // self.dataArray = [NSMutableArray new];
    self.view.backgroundColor = [UIColor whiteColor];
    if(!_tableView){
        _tableView = [UITableView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    [self.view addSubview:self.tableView];
    self.tableView.frame = self.view.bounds;
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
#pragma mark - 主要是实现上拉刷新和下拉刷新
    //下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadFirstPage)];
    
    //自动更改透明度
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    
//    //进入刷新状态
//    [self.tableView.mj_header beginRefreshing];
    
    //上拉刷新
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMorePage)];
//    //结束头部刷新
//    [weakSelf.tableView.mj_header endRefreshing];
//
//    //结束尾部刷新
//    [weakSelf.tableView.mj_footer endRefreshing];
    

    //将分割线拉伸到屏幕的宽度
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //去掉底部多余的分割线
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_tableView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(UIView *)blankView
{
    if(!_blankView){
        _blankView = [Utils createNoDataView];
    }
    return _blankView;
}

- (void)enableUploadRefresh:(BOOL)isenable{
    //下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadFirstPage)];
    
    //自动更改透明度
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    
    //    //进入刷新状态
    //    [self.tableView.mj_header beginRefreshing];
    
    //上拉刷新
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMorePage)];
}

 - (void)loadFirstPage{
    DLog(@"下拉刷新");
    //结束头部刷新
    [self.tableView.mj_header endRefreshing];
     self.pageIndex = 1;
     self.pageSize = 10;
 //    [self.dataArray removeAllObjects];
     [self loadData];
 }

- (Api *)getListApi{
    return nil;
}

- (NSString *)getListEntityName{
    return  nil;
}

 - (void)loadMorePage{
    DLog(@"上拉刷新");
    //结束尾部刷新
    [self.tableView.mj_footer endRefreshing];
    [self loadData];
 }

- (void)loadData
{
    Api *api = [self getListApi];
    if(api==nil)
        return;
    LKWeakSelf
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        NSNumber *code = [json objectForKey:@"code"];
        if([code intValue]==200){
            NSArray *dataAry = [json objectForKey:@"data"];
            if(self.pageIndex==1){
                [self.dataArray removeAllObjects];
            }
            if(dataAry.count>0){
                for(NSDictionary *dict in dataAry){
                    NSString *entityName = [self getListEntityName];
                    if(entityName != nil){
                        id model = [[NSClassFromString(entityName) alloc] initWithJson:dict];
                        [weakSelf.dataArray addObject:model];
                    }
                }
                weakSelf.pageIndex++;
            }else{

            }
            [weakSelf.tableView reloadData];
            [weakSelf checkBlankView];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@,   %@",request.error,request.responseString);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }else{
            [MBProgressHUD showMessage:@"请求失败"];
        }
        [weakSelf checkBlankView];
    }];
}


//- (void)loadFirstPage{
//    DLog(@"下拉刷新");
//    //结束头部刷新
//    [self.tableView.mj_header endRefreshing];
//}
//
//- (void)loadMorePage{
//    DLog(@"上拉刷新");
//    //结束尾部刷新
//    [self.tableView.mj_footer endRefreshing];
//}

- (void)showEndView:(BOOL)isShow{
    if(isShow){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = [UIColor colorWithHex:0x333333];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"------已经到底了------";
        self.tableView.tableFooterView = label;
    }else{
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.textLabel.text =@"测试一下";
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //将分割线拉伸到屏幕的宽度
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)checkBlankView{
    if(self.dataArray.count==0){
        self.tableView.tableFooterView = self.blankView;
    }else{
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
}


@end
