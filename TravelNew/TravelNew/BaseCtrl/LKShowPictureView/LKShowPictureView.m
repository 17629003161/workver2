//
//  LKShowPictureView.m
//  TravelNew
//
//  Created by mac on 2020/12/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKShowPictureView.h"

@interface LKShowPictureView()
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,assign) CGRect orgFrame;
@property(nonatomic,assign) CGRect startFrame;
@end


@implementation LKShowPictureView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor blackColor];
        self.scrollView.backgroundColor = [UIColor blackColor];
        [self addSubview:self.scrollView];
        self.scrollView.frame = self.bounds;
        self.orgFrame = frame;
        [self addTapGestureTarget:self action:@selector(viewIsTyped:)];
    }
    return self;
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView=[[UIScrollView alloc]init];
        //设置代理
        //_scrollView.delegate=self;
        //设置分页
        _scrollView.pagingEnabled=YES;
        //去掉滚动条
        _scrollView.showsHorizontalScrollIndicator=NO;
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    return _scrollView;
}

-(void)setPictureArray:(NSArray *)pictureArray
{
    _pictureArray = pictureArray;
    for(int i=0;i<self.pictureArray.count;i++){
        UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth*i, 0, kScreenWidth,kScreenHeight)];
        imgv.layer.contentsGravity = kCAGravityResizeAspect;
        id item = pictureArray[i];
        if([item isKindOfClass:[NSString class]]){
            [imgv sd_setImageWithURL:[NSURL URLWithString:item]];
        }else if([item isKindOfClass:[UIImage class]]){
            imgv.image = item;
        }
        [self.scrollView addSubview:imgv];
    }
    self.scrollView.contentSize = CGSizeMake(kScreenWidth*self.pictureArray.count, kScreenHeight);
}

- (void)show:(CGRect)startFrame{
    _startFrame = startFrame;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
    self.frame = startFrame;
    [UIView animateWithDuration:0.3 animations:^{
            self.frame = self.orgFrame;
    }];
}

- (void)hide{
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = self.startFrame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}



@end
