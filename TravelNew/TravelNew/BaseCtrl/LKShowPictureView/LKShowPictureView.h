//
//  LKShowPictureView.h
//  TravelNew
//
//  Created by mac on 2020/12/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKShowPictureView : UIView
@property(nonatomic,strong) NSArray *pictureArray;
- (void)show:(CGRect)startFrame;
@end

NS_ASSUME_NONNULL_END
