//
//  BaseCustomTableViewCell.h
//  TravelNew
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCustomTableViewCell : UITableViewCell
- (void)configViews;
@end

NS_ASSUME_NONNULL_END
