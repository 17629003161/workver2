//
//  BaseCustomCollectionCell.h
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCustomCollectionCell : UICollectionViewCell
- (void)configView;
@end

NS_ASSUME_NONNULL_END
