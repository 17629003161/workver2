//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "UserInfo.h"

@interface UserInfo()
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,strong) NSMutableArray *dataArray;

@end


@implementation UserInfo


static UserInfo* _instance = nil;

+(instancetype) shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
        [_instance startCheckTimer];
        [[NSNotificationCenter defaultCenter] addObserver:_instance selector:@selector(messageIsRead:) name:Notification_NewMessageRead object:nil];
    }) ;
    return _instance ;
}

- (NSMutableArray *)dataArray{
    if(!_dataArray){
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
        self.userLabel  = [json objectForKey:@"userLabel"];
         self.referralCode  = [json objectForKey:@"referralCode"];
         self.creatTime  = [json objectForKey:@"creatTime"];
         self.aliUserid  = [json objectForKey:@"aliUserid"];
         self.sex  = [json objectForKey:@"sex"];
         self.birthTime  = [json objectForKey:@"birthTime"];
         self.isRealname = [[json objectForKey:@"isRealname"]boolValue];
         self.job  = [json objectForKey:@"job"];
         self.sign  = [json objectForKey:@"sign"];
         self.headerImage  = [json objectForKey:@"headerImage"];
         self.userStyle  = [json objectForKey:@"userStyle"];
         self.referralUserId  = [json objectForKey:@"referralUserId"];
         self.userNickname  = [json objectForKey:@"userNickname"];
         self.phone  = [json objectForKey:@"phone"];
         self.idNumber  = [json objectForKey:@"idNumber"];
         self.unionid  = [json objectForKey:@"unionid"];
         self.userName  = [json objectForKey:@"userName"];
         self.easemobId  = [json objectForKey:@"easemobId"];
         self.password  = [json objectForKey:@"password"];
         self.address  = [json objectForKey:@"address"];
         self.userId  = [json objectForKey:@"userId"];
         self.cityid  = [json objectForKey:@"cityid"];
         self.city  = [json objectForKey:@"city"];
         self.bgImage  = [json objectForKey:@"bgImage"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userLabel forKey:@"zx_userLabel"];
    [aCoder encodeObject:self.referralCode forKey:@"zx_referralCode"];
    [aCoder encodeObject:self.creatTime forKey:@"zx_creatTime"];
    [aCoder encodeObject:self.aliUserid forKey:@"zx_aliUserid"];
    [aCoder encodeObject:self.sex forKey:@"zx_sex"];
    [aCoder encodeObject:self.birthTime forKey:@"zx_birthTime"];
    [aCoder encodeBool:self.isRealname forKey:@"zx_isRealname"];
    [aCoder encodeObject:self.job forKey:@"zx_job"];
    [aCoder encodeObject:self.sign forKey:@"zx_sign"];
    [aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
    [aCoder encodeObject:self.userStyle forKey:@"zx_userStyle"];
    [aCoder encodeObject:self.currentUserStyle forKey:@"zx_currentUserStyle"];
    [aCoder encodeObject:self.referralUserId forKey:@"zx_referralUserId"];
    [aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
    [aCoder encodeObject:self.phone forKey:@"zx_phone"];
    [aCoder encodeObject:self.idNumber forKey:@"zx_idNumber"];
    [aCoder encodeObject:self.unionid forKey:@"zx_unionid"];
    [aCoder encodeObject:self.userName forKey:@"zx_userName"];
    [aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
    [aCoder encodeObject:self.password forKey:@"zx_password"];
    [aCoder encodeObject:self.address forKey:@"zx_address"];
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
    [aCoder encodeBool:self.isLogin forKey:@"zx_isLogin"];
    [aCoder encodeObject:self.token forKey:@"zx_token"];
    [aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
    [aCoder encodeObject:self.city forKey:@"zx_city"];
    [aCoder encodeObject:self.bgImage forKey:@"zx_bgImage"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userLabel = [aDecoder decodeObjectForKey:@"zx_userLabel"];
        self.referralCode = [aDecoder decodeObjectForKey:@"zx_referralCode"];
        self.creatTime = [aDecoder decodeObjectForKey:@"zx_creatTime"];
        self.aliUserid = [aDecoder decodeObjectForKey:@"zx_aliUserid"];
        self.sex = [aDecoder decodeObjectForKey:@"zx_sex"];
        self.birthTime = [aDecoder decodeObjectForKey:@"zx_birthTime"];
        self.isRealname = [aDecoder decodeBoolForKey:@"zx_isRealname"];
        self.job = [aDecoder decodeObjectForKey:@"zx_job"];
        self.sign = [aDecoder decodeObjectForKey:@"zx_sign"];
        self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
        self.userStyle = [aDecoder decodeObjectForKey:@"zx_userStyle"];
        self.currentUserStyle = [aDecoder decodeObjectForKey:@"zx_currentUserStyle"];
        self.referralUserId = [aDecoder decodeObjectForKey:@"zx_referralUserId"];
        self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
        self.phone = [aDecoder decodeObjectForKey:@"zx_phone"];
        self.idNumber = [aDecoder decodeObjectForKey:@"zx_idNumber"];
        self.unionid = [aDecoder decodeObjectForKey:@"zx_unionid"];
        self.userName = [aDecoder decodeObjectForKey:@"zx_userName"];
        self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
        self.password = [aDecoder decodeObjectForKey:@"zx_password"];
        self.address = [aDecoder decodeObjectForKey:@"zx_address"];
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
        self.isLogin = [aDecoder decodeBoolForKey:@"zx_isLogin"];
        self.token = [aDecoder decodeObjectForKey:@"zx_token"];
        self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
        self.city = [aDecoder decodeObjectForKey:@"zx_city"];
        self.bgImage = [aDecoder decodeObjectForKey:@"zx_bgImage"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userLabel : %@\n",self.userLabel];
    result = [result stringByAppendingFormat:@"referralCode : %@\n",self.referralCode];
    result = [result stringByAppendingFormat:@"creatTime : %@\n",self.creatTime];
    result = [result stringByAppendingFormat:@"aliUserid : %@\n",self.aliUserid];
    result = [result stringByAppendingFormat:@"sex : %@\n",self.sex];
    result = [result stringByAppendingFormat:@"birthTime : %@\n",self.birthTime];
    result = [result stringByAppendingFormat:@"isRealname : %@\n",self.isRealname?@"yes":@"no"];
    result = [result stringByAppendingFormat:@"job : %@\n",self.job];
    result = [result stringByAppendingFormat:@"sign : %@\n",self.sign];
    result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
    result = [result stringByAppendingFormat:@"userStyle : %@\n",self.userStyle];
    result = [result stringByAppendingFormat:@"currentUserStyle : %@\n",self.currentUserStyle];
    result = [result stringByAppendingFormat:@"referralUserId : %@\n",self.referralUserId];
    result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
    result = [result stringByAppendingFormat:@"phone : %@\n",self.phone];
    result = [result stringByAppendingFormat:@"idNumber : %@\n",self.idNumber];
    result = [result stringByAppendingFormat:@"unionid : %@\n",self.unionid];
    result = [result stringByAppendingFormat:@"userName : %@\n",self.userName];
    result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
    result = [result stringByAppendingFormat:@"password : %@\n",self.password];
    result = [result stringByAppendingFormat:@"address : %@\n",self.address];
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    result = [result stringByAppendingFormat:@"isLogin :%@\n",self.isLogin?@"yes":@"no"];
    result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
    result = [result stringByAppendingFormat:@"city : %@\n",self.city];
    result = [result stringByAppendingFormat:@"bgImage : %@\n",self.bgImage];
    result = [result stringByAppendingFormat:@"token: %@\n",self.token];
    
    return result;
}
- (void)save{
    //归档
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"userInfo.data"];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}

- (void)read{
    //解档
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"userInfo.data"];
    UserInfo *entity = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    if(entity){
        self.userLabel = entity.userLabel;
        self.referralCode = entity.referralCode;
        self.creatTime = entity.creatTime;
        self.aliUserid = entity.aliUserid;
        self.sex = entity.sex;
        self.birthTime = entity.birthTime;
        self.isRealname = entity.isRealname;
        self.job = entity.job;
        self.sign = entity.sign;
        self.headerImage = entity.headerImage;
        self.userStyle = entity.userStyle;
        self.currentUserStyle = entity.currentUserStyle;
        self.referralUserId = entity.referralUserId;
        self.userNickname = entity.userNickname;
        self.phone = entity.phone;
        self.idNumber = entity.idNumber;
        self.unionid = entity.unionid;
        self.userName = entity.userName;
        self.easemobId = entity.easemobId;
        self.password = entity.password;
        self.address = entity.address;
        self.userId = entity.userId;
        self.city = entity.city;
        self.cityid = entity.cityid;
        self.bgImage = entity.bgImage;
        self.token = entity.token;      //用户token
        self.isLogin = entity.isLogin;         //用户是否已登录
        self.selectCityId = self.cityid;
        self.selectCity = self.city;
        if(self.currentUserStyle==nil){
            self.currentUserStyle = self.userStyle;
        }
    }else{
        [self setLogin:NO];
    }
}

- (void)setLogin:(BOOL)isLogin{
    self.isLogin = isLogin;
    [self save];
}

- (BOOL)isRecomander{
    return YES;
    
    if([self.userStyle integerValue] == UserStyleRecommender){
        return YES;
    }else{
        return NO;
    }
}

- (BOOL)isCurrentRecomander
{
    if(([self.currentUserStyle integerValue] == UserStyleRecommender)&&[self isRecomander]){
        return YES;
    }else{
        return NO;
    }
    
}

- (void)switchCurrentStyle{
    [[NSNotificationCenter defaultCenter] postNotificationName:Notification_ActerChanged object:nil];
    if([self.currentUserStyle integerValue]==UserStyleRecommender){
        self.currentUserStyle = @(UserStyleCustom);
    }else{
        self.currentUserStyle = @(UserStyleRecommender);
    }
    [self save];
}


#pragma mark -- 查询新消息

- (void)messageIsRead:(NSNotification *)notification
{
    [self startCheckTimer];
}

- (void)startCheckTimer{
    if(!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];
    }
}

- (void)stopTimer
{
    if(self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}


- (void)onTimer:(NSTimer *)timer{
    NSLog(@"onTimer.....\n");
    UserInfo *user = [UserInfo shareInstance];
    if(!user.isLogin){
        return;
    }
    [self _loadAllConversationsFromDB];
    MessageModel *oldModel = [MessageModel read];
    Api *api = [Api apiPostUrl:@"work/message/getNewMessage" para:@{} description:@"获取最新的系统消息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *date = [json objectForKey:@"data"];
            MessageModel *model = [[MessageModel alloc] initWithJson:date];
            [model save];
           // DLog(@"newtime = %@, oldTime = %@",model.creatTime,oldModel.creatTime);
            if(oldModel==nil || [model.creatTime doubleValue] > [oldModel.creatTime doubleValue]){
                if(self.timer){
                    [self stopTimer];
                }
                model.badgeValue = 1;
                self.model = model;
                [[NSNotificationCenter defaultCenter] postNotificationName:Notification_NewMessageReceive object:nil];
            }else{
                model.badgeValue = 0;
                self.model = model;
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
    }];
}

#pragma mark -- 获取聊天信息
- (void)_loadAllConversationsFromDB
{
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
        NSArray *sorted = [conversations sortedArrayUsingComparator:^(EMConversation *obj1, EMConversation *obj2) {
            EMMessage *message1 = [obj1 latestMessage];
            EMMessage *message2 = [obj2 latestMessage];
            if(message1.timestamp > message2.timestamp) {
                return(NSComparisonResult)NSOrderedAscending;
            } else {
                return(NSComparisonResult)NSOrderedDescending;
            }}];
        [weakself.dataArray removeAllObjects];
        NSArray *models = [EMConversationHelper modelsFromEMConversations:sorted];
        [weakself.dataArray addObjectsFromArray:models];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for(EMConversationModel *model in self.dataArray){
                if(model.emModel.unreadMessagesCount>0){
                    [[NSNotificationCenter defaultCenter] postNotificationName:Notification_NewMessageReceive object:nil];
                }
            }
        });
    });
}



@end
