//
//  LKInputChecker.h
//  TravelNew
//
//  Created by mac on 2020/12/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKInputChecker : NSObject

#pragma 正则匹配手机号
+ (BOOL)checkTelNumber:(NSString *) telNumber;

#pragma 正则匹配用户密码6-18位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password;

#pragma 正则匹配用户姓名,20位的中文或英文
+ (BOOL)checkUserName : (NSString *) userName;

#pragma 正则匹配用户身份证号15或18位
+ (BOOL)checkUserIdCard: (NSString *) idCard;

#pragma 正则匹配URL
+ (BOOL)checkURL : (NSString *) url;

#pragma 判断车牌号是否合法
+ (BOOL)checkCarNumber:(NSString *)carNumber;

@end

NS_ASSUME_NONNULL_END
