//
//  LKGadentButton.h
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKGadentButton : UIView
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,assign) BOOL isEnabled;
@property(nonatomic,assign) BOOL isEfectReverse;
+ (LKGadentButton *)blueButtonWhithTitle:(NSString *)title
                                   font:(UIFont *)font
                            cornerRadius:(CGFloat)cornerRadius;
@end

NS_ASSUME_NONNULL_END
