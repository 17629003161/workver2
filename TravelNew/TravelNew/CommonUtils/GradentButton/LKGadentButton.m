//
//  LKGadentButton.m
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKGadentButton.h"

@interface LKGadentButton()
@property(nonatomic,strong) UIView *bgNormalView;
@property(nonatomic,strong) UIView *bgDisableView;
@property(nonatomic,assign) BOOL isGray;
@end

@implementation LKGadentButton

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self configView:(frame)];
        self.isEfectReverse = NO;
    }
    return self;
}

- (void)configView:(CGRect)frame{
    _bgNormalView = [[UIView alloc] initWithFrame:frame];
    [_bgNormalView gradientDefault:self.bounds];
    _bgDisableView = [[UIView alloc] initWithFrame:frame];
    [_bgDisableView gradientDisable:self.bounds];
    _titleLabel = [[UILabel alloc] initWithFrame:frame];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self sd_addSubviews:@[_bgNormalView,_bgDisableView]];
    [self addSubview:_titleLabel];
    self.layer.masksToBounds = YES;
    self.isEnabled = YES;
    self.isGray = NO;
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _bgNormalView.frame = self.bounds;
    [_bgNormalView gradientDefault:self.bounds];
    _bgDisableView.frame = self.bounds;
    [_bgDisableView gradientDisable:self.bounds];
    _titleLabel.frame = self.bounds;
}

-(void)setIsEnabled:(BOOL)isEnabled
{
    _isEnabled = isEnabled;
    if(_isEnabled){
        _bgNormalView.hidden = NO;
        _bgDisableView.hidden = YES;
    }else{
        _bgNormalView.hidden = YES;
        _bgDisableView.hidden = NO;
    }
}

-(void)setIsGray:(BOOL )isGray
{
    _isGray = isGray;
    if(!_isGray){
        _bgNormalView.hidden = NO;
        _bgDisableView.hidden = YES;
    }else{
        _bgNormalView.hidden = YES;
        _bgDisableView.hidden = NO;
    }
    
}

+ (LKGadentButton *)blueButtonWhithTitle:(NSString *)title
                                   font:(UIFont *)font
                            cornerRadius:(CGFloat)cornerRadius
{
    LKGadentButton *btn = [LKGadentButton new];
    btn.titleLabel.font = font;
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.titleLabel.text = title;
    btn.layer.cornerRadius = cornerRadius;
    btn.layer.masksToBounds = YES;
    btn.isEnabled = YES;
    return btn;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if(!self.isGray && self.isEnabled){
        if(!self.isEfectReverse){
            self.isGray = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.05*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.isGray = NO;
               });
        }else{
            self.isGray = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.05*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.isGray = YES;
               });
        }
    }
}


@end
