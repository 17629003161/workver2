//
//  LKDistrictManger.h
//  TravelNew
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 lester. All rights reserved.
//
//查询行政区划
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^OnSelectDistrictBlock)(AMapDistrict *);

@interface LKDistrictManger : NSObject

- (void)searchkeywords:(NSString *)cityName onSelectResult:(OnSelectDistrictBlock) onSelectResultBlock;

@end

NS_ASSUME_NONNULL_END
