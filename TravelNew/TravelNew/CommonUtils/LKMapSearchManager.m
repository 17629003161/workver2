//
//  LKMapSearchManager.m
//  TravelNew
//
//  Created by mac on 2021/3/23.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKMapSearchManager.h"
#import "LKDistrictManger.h"

@interface LKMapSearchManager()<AMapSearchDelegate>
@property(nonatomic,strong) NSMutableArray<AMapPOI *> *dataArray;
@property(nonatomic, strong) AMapSearchAPI *search;

@end

@implementation LKMapSearchManager

static LKMapSearchManager *_instance = nil;

+(instancetype) shareManager
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    });
    return _instance ;
}

-(NSMutableArray *)dataArray
{
    if(!_dataArray){
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

-(AMapSearchAPI *)search{
    if(!_search){
        _search = [AMapSearchAPI new];
        _search.delegate = self;
    }
    return _search;
}


//反编码
- (void)addressAntiEncoder:(CLLocationCoordinate2D)loc2d finishBlock:(AntiEcoderBlock)block{
    //创建地理编码对象
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    //创建位置
    CLLocation *location=[[CLLocation alloc]initWithLatitude:loc2d.latitude
                                                   longitude:loc2d.longitude];
    //反地理编码
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        //判断是否有错误或者placemarks是否为空
        if (error !=nil || placemarks.count==0) {
            block(nil);
            return;
        }
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",
                             placemark.name,
                             placemark.locality,
                             placemark.subLocality,
                             placemark.administrativeArea,
                             placemark.postalCode,
                             placemark.country];
        DLog(@"%@", address);
        block(placemark);
    }];
}

#pragma mark - 搜索周边
- (void)arroundSearchWhithCoordinate:(CLLocationCoordinate2D)coordinate{
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location            = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    request.keywords            = @"";
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    [self.search AMapPOIAroundSearch:request];
}
#pragma mark -POI搜索
- (void)doPOIKeywordsSearch:(NSString *)address{
    NSString *keywords = [address trimWhitespace];
    AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
    request.keywords            = keywords;
 //   request.city                = self.cityName;
    request.types               = @"建筑|学校|商店|医院|道路|桥梁|小区|街道|便利店|影院|商场|广场|餐馆|车站 ";
    request.requireExtension    = NO;
    /*  搜索SDK 3.2.0 中新增加的功能，只搜索本城市的POI。*/
    request.cityLimit           = NO;
    request.requireSubPOIs      = YES;
    [self.search AMapPOIKeywordsSearch:request];
}

#pragma mark - AMapSearchDelegate
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    [self.dataArray removeAllObjects];
    if (response.pois.count > 0)
    {
        for (AMapPOI *poi in response.pois) {
            DLog(@"pois:%@\n",poi.name);
            [self.dataArray addObject:poi];
        }
    }
    if(self.onSearchResultBlock){
        self.onSearchResultBlock(self.dataArray);
    }
}


@end
