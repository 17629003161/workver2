//
//  SelectCityCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectCityCtrl : LKBaseViewController
@property (nonatomic, copy) void(^OnSelectAddress)(AMapDistrict *);  //传递1个参数
@end

NS_ASSUME_NONNULL_END
