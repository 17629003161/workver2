//
//  SelectCityCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SelectCityCtrl.h"

@interface SelectCityCtrl ()<AMapSearchDelegate>
@property(nonatomic,strong) NSMutableArray *cityArray;
@property(nonatomic,strong) UIButton *btnCurrent;
@property(nonatomic,strong) AMapSearchAPI *search;
@property(nonatomic,strong) AMapDistrict *district;
@end

@implementation SelectCityCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xF3F3F3);
    [self layout];
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTitle:@"城市选择" titleColor:nil];
}

- (void)searchCity:(NSString *)name
{
    AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
    dist.keywords = name;
    dist.requireExtension = YES;
    [self.search AMapDistrictSearch:dist];
}

- (void)onDistrictSearchDone:(AMapDistrictSearchRequest *)request response:(AMapDistrictSearchResponse *)response
{
    if (response == nil)
    {
        return;
    }
    //解析response获取行政区划，具体解析见 Demo
    NSArray *ary = response.districts;
    AMapDistrict *dist = [ary lastObject];
    self.district = dist;
    if(self.OnSelectAddress){
        self.OnSelectAddress(self.district);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error
{
    DLog(@"Error: %@", error);
}


- (void)btnIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSString *title = [btn titleForState:UIControlStateNormal];
    [self searchCity:title];
}

- (void)layout{
    _cityArray = [NSMutableArray new];
    NSArray *array = @[@"西安",@"拉萨",@"三亚",@"丽江",@"成都",@"招募中..."];
    
    NSInteger top = 141;
    NSInteger edge = 20;
    
    NSInteger itemWidth = 90;
    NSInteger itemHeight = 32;
    
    NSInteger hgap = 16;
    NSInteger vgap = 12;
    
    NSInteger num = (kScreenWidth - edge*2 - itemWidth)/(itemWidth + hgap) + 1;
    
    for (int i=0; i<array.count; i++) {
        NSString *title = array[i];
        UIButton *btn = [self buttonWithTitle:title];
        if(![title isEqualToString:@"招募中..."])
            [btn addTarget:self action:@selector(btnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_cityArray addObject:btn];
        int row = i / num;
        int col = i % num;
        NSInteger x = edge + col * (itemWidth + hgap);
        NSInteger y = top + row * (itemHeight + vgap);
        CGRect frame = btn.frame;
        frame.origin = CGPointMake(x, y);
        btn.frame = frame;
    }
    UIImageView *locImgv = ImageViewWithImage(@"city_location");
    UIImageView *cityImgv = ImageViewWithImage(@"city_building");
    UILabel *label1 = [Utils createLabelWithTitle:@"当前定位城市"
                                       titleColor:HexColor(0x999999)
                                         fontSize:14];
    LKLocationManager *manager = [LKLocationManager sharedManager];
    UIButton *curCity = [self buttonWithTitle:manager.cityName];
    self.btnCurrent = curCity;
    UILabel *label2 = [Utils createLabelWithTitle:@"旅咖出行服务城市"
                                       titleColor:HexColor(0x999999)
                                         fontSize:14];
    [self.view sd_addSubviews:@[locImgv,label1,curCity,
                                cityImgv,label2]];
    [self.view sd_addSubviews:_cityArray];
    
    locImgv.sd_layout
    .topSpaceToView(self.view, 23)
    .leftSpaceToView(self.view, 20)
    .widthIs(12)
    .heightIs(16);
    
    label1.sd_layout
    .centerYEqualToView(locImgv)
    .leftSpaceToView(locImgv, 2)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    curCity.sd_layout
    .topSpaceToView(locImgv, 12)
    .leftEqualToView(locImgv)
    .widthIs(90)
    .heightIs(32);
    
    cityImgv.sd_layout
    .topSpaceToView(curCity, 31)
    .leftEqualToView(curCity)
    .widthIs(14)
    .heightIs(15);
    
    label2.sd_layout
    .centerYEqualToView(cityImgv)
    .leftSpaceToView(cityImgv, 2)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

- (UIButton *)buttonWithTitle:(NSString *)title
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = LKSystemFont(15);
    [btn setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
    btn.layer.borderColor = HexColor(0xE7E7E7).CGColor;
    btn.layer.borderWidth = 1.0;
    btn.layer.cornerRadius = 4.0;
    btn.frame = CGRectMake(0, 0, 90, 32);
    return btn;
}


@end
