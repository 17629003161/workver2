//
//  LKLocationManger.h
//  TravelNew
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 lester. All rights reserved.
//

//查询位置变化

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKLocationManager : NSObject
@property(nonatomic,strong) NSString *cityCode;
@property(nonatomic,strong) NSString *cityName;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) void(^OnSelectAddressBlock)(NSString *name,NSString *cityid,CLLocationCoordinate2D location,NSString *cityName);
- (void)startLocation;
- (void)stopLocation;
+(instancetype) sharedManager;
- (void)AntiEncoder:(CLLocationCoordinate2D)coordinate;
@end

NS_ASSUME_NONNULL_END

