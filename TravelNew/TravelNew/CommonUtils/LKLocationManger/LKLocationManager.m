//
//  LKLocationManger.m
//  TravelNew
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "LKDistrictManger.h"

@interface LKLocationManager()<AMapLocationManagerDelegate,AMapSearchDelegate>
@property(nonatomic,strong) AMapLocationManager *locationManager;
@property(nonatomic,strong) LKDistrictManger *districtManager;      //城市区划管理器
@property(nonatomic, strong) AMapSearchAPI *search;
@end

@implementation LKLocationManager

static LKLocationManager* _instance = nil;

+(instancetype) sharedManager
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
        _instance.cityCode = @"029";
        _instance.cityName = @"西安市";
    }) ;
    [_instance startLocation];
    return _instance ;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.districtManager = [[LKDistrictManger alloc] init];
        self.locationManager = [[AMapLocationManager alloc] init];
        self.locationManager.distanceFilter = 50;
        self.locationManager.delegate = self;
        [self.locationManager startUpdatingLocation];
    }
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"{name: %@, cityCode: %@, coordinate: { latitude: %lf, longitude: %lf}}",self.name,self.cityCode,self.coordinate.latitude,self.coordinate.longitude];
}

- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location
{
  //  [self.locationManager stopUpdatingLocation];
    DLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    [self sendLocation:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];   
    [self AntiEncoder:location.coordinate];
}

- (void)startLocation{
    [self.locationManager startUpdatingLocation];
}
- (void)stopLocation{
    [self.locationManager stopUpdatingLocation];
}

//反编码
- (void)AntiEncoder:(CLLocationCoordinate2D)coordinate {
    //创建地理编码对象
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    //创建位置
    CLLocation *location=[[CLLocation alloc]initWithLatitude:coordinate.latitude
                                                   longitude:coordinate.longitude];
    //反地理编码
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        //判断是否有错误或者placemarks是否为空
        if (error !=nil || placemarks.count==0) {
            DLog(@"%@",error);
            [self arroundSearchWhithCoordinate:coordinate];
            return ;
        }
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",
                             placemark.thoroughfare,
                             placemark.locality,            //城市
                             placemark.subLocality,
                             placemark.administrativeArea,
                             placemark.postalCode,
                             placemark.country];
        DLog(@"%@", address);
            //赋值详细地址
        self.name = placemark.name;
        self.coordinate = coordinate;
        self.cityName = placemark.locality;
        [self.districtManager searchkeywords:placemark.locality onSelectResult:^(AMapDistrict * dist) {
       //   NSString  *cityId = dist.citycode;
            if(self.OnSelectAddressBlock){
                self.cityCode = dist.citycode;
                self.OnSelectAddressBlock(placemark.name,dist.citycode,coordinate,placemark.locality);
            }
        }];
    }];
}

- (void)sendLocation:(CLLocationCoordinate2D) location
{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/updateLtReferrerUserAddress"
                          para:@{@"referrerLongitude":@(location.longitude),
                                     @"referrerLatitude":@(location.latitude)}
                   description:@"修改推荐官经纬度"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"%@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            BOOL isok = [[json objectForKey:@"data"] boolValue];
            if(!isok){
      //          [MBProgressHUD showMessage:@"上传位置信息失败"];
            }
            DLog(@"%@",json[@"msg"]);
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
      //      [MBProgressHUD showMessage:@"上传位置信息失败"];
        }];
}

- (void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager*)locationManager
{
    [locationManager requestAlwaysAuthorization];
}

#pragma mark - 搜索周边
- (void)arroundSearchWhithCoordinate:(CLLocationCoordinate2D)coordinate{
    self.search =  [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location            = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    request.keywords            = @"";
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    [self.search AMapPOIAroundSearch:request];
}

/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response.pois.count == 0)
    {
        return;
    }
    //解析response获取POI信息，具体解析见 Demo
//    [self.dataArray removeAllObjects];
//    for (AMapPOI *poi in response.pois) {
//        DLog(@"pois:%@\n",poi.name);
//        [self.dataArray addObject:poi];
//    }
//    [self.tableView reloadData];
    AMapPOI *poi = [response.pois firstObject];
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = poi.location.latitude;
    coordinate.longitude = poi.location.longitude;
    self.cityCode = poi.citycode;
    self.cityName = poi.city;
    self.name = poi.name;
    self.coordinate = coordinate;
    if(self.OnSelectAddressBlock){
        self.OnSelectAddressBlock(poi.name,poi.citycode,coordinate,poi.city);
    }
}





@end
