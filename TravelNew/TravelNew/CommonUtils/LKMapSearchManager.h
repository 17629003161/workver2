//
//  LKMapSearchManager.h
//  TravelNew
//
//  Created by mac on 2021/3/23.
//  Copyright © 2021 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^AntiEcoderBlock)(CLPlacemark * _Nullable placeMark);

NS_ASSUME_NONNULL_BEGIN

@interface LKMapSearchManager : NSObject
@property(nonatomic,copy) void (^onSearchResultBlock)(NSArray *);
+(instancetype) shareManager;
//关键字搜索
- (void)doPOIKeywordsSearch:(NSString *)address;
//周边搜索
- (void)arroundSearchWhithCoordinate:(CLLocationCoordinate2D)coordinate;
//地理信息反编码
- (void)addressAntiEncoder:(CLLocationCoordinate2D)loc2d finishBlock:(AntiEcoderBlock)block;

@end

NS_ASSUME_NONNULL_END
