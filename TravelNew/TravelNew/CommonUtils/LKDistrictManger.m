//
//  LKDistrictManger.m
//  TravelNew
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKDistrictManger.h"

@interface LKDistrictManger()<AMapSearchDelegate>
@property(nonatomic,strong) AMapSearchAPI *search;
@property(nonatomic,strong) AMapDistrict *district;
@property(nonatomic,copy) OnSelectDistrictBlock onSelectDistrictBlock;
@end

@implementation LKDistrictManger

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.search = [[AMapSearchAPI alloc] init];
        self.search.delegate = self;
    }
    return self;
}

- (void)searchkeywords:(NSString *)keywords onSelectResult:(OnSelectDistrictBlock) onSelectResultBlock{
    self.onSelectDistrictBlock = onSelectResultBlock;
    [self searchKeywords:keywords];
}

- (void)searchKeywords:(NSString *)name
{
    AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
    dist.keywords = name;
    dist.requireExtension = YES;
    [self.search AMapDistrictSearch:dist];
}


- (void)onDistrictSearchDone:(AMapDistrictSearchRequest *)request response:(AMapDistrictSearchResponse *)response
{
    if (response == nil)
    {
        return;
    }
    //解析response获取行政区划，具体解析见 Demo
    NSArray *ary = response.districts;
    AMapDistrict *dist = [ary lastObject];
    self.district = dist;
    if(self.onSelectDistrictBlock){
        self.onSelectDistrictBlock(self.district);
    }
}

- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error
{
    DLog(@"Error: %@", error);
}



@end
