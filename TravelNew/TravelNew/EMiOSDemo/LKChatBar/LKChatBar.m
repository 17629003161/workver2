//
//  EMChatBar.m
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 2019/1/25.
//  Copyright © 2019 XieYajie. All rights reserved.
//

#import "LKChatBar.h"

#define ktextViewMinHeight 40
#define ktextViewMaxHeight 120

@interface LKChatBar()<UITextViewDelegate>

@property (nonatomic) CGFloat version;

@property (nonatomic) CGFloat previousTextViewContentHeight;

@property (nonatomic, strong) UIButton *selectedButton;

@property (nonatomic, strong) UIView *currentMoreView;

@property (nonatomic, strong) UIButton *carButton;
@property (nonatomic, strong) UIButton *roomButton;
@property (nonatomic, strong) UILabel *label3;
@property (nonatomic, strong) UILabel *label4;



@end

@implementation LKChatBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        _version = [[[UIDevice currentDevice] systemVersion] floatValue];
        _previousTextViewContentHeight = ktextViewMinHeight;
        [self _setupSubviews];
    }
    
    return self;
}

- (void)makeForcus{
    [self.textView becomeFirstResponder];
}

- (void)setIsRecomander:(BOOL)isRecomander
{
    _isRecomander = isRecomander;
    if(isRecomander){
        [self.carButton setBackgroundImage:[UIImage imageNamed:@"chart_demand"] forState:(UIControlStateNormal)];
        self.label3.text = @"服务";
        [self.label3 updateLayout];
        self.roomButton.hidden = YES;
        self.label4.hidden = YES;
    }else{
        [self.carButton setBackgroundImage:[UIImage imageNamed:@"chart_car"] forState:(UIControlStateNormal)];
        self.label3.text = @"车辆";
        [self.label3 updateLayout];
        self.roomButton.hidden = NO;
        self.label4.hidden = NO;
        
    }
}

#pragma mark - Subviews

- (void)_setupSubviews{
    self.backgroundColor = HexColor(0xf9f9f9);
    
    UIView *topView = [UIView new];
    topView.backgroundColor = HexColor(0xf9f9f9);
    topView.layer.borderColor = HexColor(0xCDCDCD).CGColor;
    topView.layer.borderWidth = 0.5;
    UIButton *soundBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [soundBtn setBackgroundImage:[UIImage imageNamed:@"chart_audo25"] forState:UIControlStateNormal];
    [soundBtn addTarget:self action:@selector(audioButtonAction:) forControlEvents:UIControlEventTouchUpInside];

    self.textView = [[EMTextView alloc] init];
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.delegate = self;
    self.textView.placeholder = @"请输入消息内容";
    self.textView.font = [UIFont systemFontOfSize:16];
    self.textView.returnKeyType = UIReturnKeySend;
    self.textView.backgroundColor = [UIColor clearColor];
    
    UIButton *emoBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [emoBtn setBackgroundImage:[UIImage imageNamed:@"chart_emotion"] forState:UIControlStateNormal];
    [emoBtn addTarget:self action:@selector(emoticonButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [addButton setBackgroundImage:[UIImage imageNamed:@"chart_add"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [topView sd_addSubviews:@[soundBtn,self.textView,emoBtn,addButton]];
    
    UIButton *photoBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [photoBtn setBackgroundImage:[UIImage imageNamed:@"chart_photo"] forState:(UIControlStateNormal)];
    [photoBtn addTarget:self action:@selector(photoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label1 = [Utils createLabelWithTitle:@"照片" titleColor:HexColor(0x666666) fontSize:12];
    
    
    UIButton *cremaBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cremaBtn setBackgroundImage:[UIImage imageNamed:@"chart_carma"] forState:(UIControlStateNormal)];
    [cremaBtn addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label2 = [Utils createLabelWithTitle:@"拍照" titleColor:HexColor(0x666666) fontSize:12];
    
    UIButton *carBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [carBtn setBackgroundImage:[UIImage imageNamed:@"chart_car"] forState:(UIControlStateNormal)];
    [carBtn  addTarget:self action:@selector(carButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.carButton = carBtn;
    UILabel *label3 = [Utils createLabelWithTitle:@"车辆" titleColor:HexColor(0x666666) fontSize:12];
    self.label3 = label3;
    
    UIButton *roomBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [roomBtn setBackgroundImage:[UIImage imageNamed:@"chart_room"] forState:(UIControlStateNormal)];
    [roomBtn addTarget:self action:@selector(roomButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.roomButton = roomBtn;
    UILabel *label4 = [Utils createLabelWithTitle:@"住宿" titleColor:HexColor(0x666666) fontSize:12];
    self.label4 = label4;
    
    [self sd_addSubviews:@[topView,
                           photoBtn,cremaBtn,carBtn,roomBtn,
                           label1,label2,label3,label4]];
    topView.sd_layout
    .topEqualToView(self)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(50);
    
    
    soundBtn.sd_layout
    .centerYEqualToView(topView)
    .leftSpaceToView(topView, 6)
    .widthIs(28)
    .heightIs(28);
    
    self.textView.sd_layout
    .centerYEqualToView(topView)
    .leftSpaceToView(soundBtn, 8)
    .rightSpaceToView(topView, 80)
    .heightIs(35);
    
    emoBtn.sd_layout
    .centerYEqualToView(topView)
    .leftSpaceToView(self.textView, 8)
    .widthIs(28)
    .heightIs(28);
    
    addButton.sd_layout
    .centerYEqualToView(topView)
    .rightSpaceToView(topView, 6)
    .widthIs(28)
    .heightIs(28);
    
    photoBtn.sd_layout
    .topSpaceToView(topView, 30)
    .leftSpaceToView(self, 24)
    .widthIs(64)
    .heightIs(64);
    label1.sd_layout
    .topSpaceToView(photoBtn, 4)
    .centerXEqualToView(photoBtn)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cremaBtn.sd_layout
    .centerYEqualToView(photoBtn)
    .leftSpaceToView(photoBtn, 24)
    .widthIs(64)
    .heightIs(64);
    label2.sd_layout
    .topSpaceToView(cremaBtn, 4)
    .centerXEqualToView(cremaBtn)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    carBtn.sd_layout
    .centerYEqualToView(photoBtn)
    .leftSpaceToView(cremaBtn, 24)
    .widthIs(64)
    .heightIs(64);
    label3.sd_layout
    .topSpaceToView(carBtn, 4)
    .centerXEqualToView(carBtn)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    roomBtn.sd_layout
    .centerYEqualToView(photoBtn)
    .leftSpaceToView(carBtn, 24)
    .widthIs(64)
    .heightIs(64);
    label4.sd_layout
    .topSpaceToView(roomBtn, 4)
    .centerXEqualToView(roomBtn)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}




- (void)_setupButtonsView
{
    NSInteger count = 7;
    //NSInteger count = 6;
    CGFloat width = [UIScreen mainScreen].bounds.size.width / count;
    
    self.buttonArray = [[NSMutableArray alloc] init];
    
    UIButton *audioButton = [[UIButton alloc] init];
    [audioButton setImage:[UIImage imageNamed:@"chatbar_audio"] forState:UIControlStateNormal];
    [audioButton setImage:[UIImage imageNamed:@"chatbar_audio_blue"] forState:UIControlStateSelected];
    [audioButton addTarget:self action:@selector(audioButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:audioButton];
    [audioButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(self.buttonsView);
        make.bottom.equalTo(self.buttonsView);
        make.width.mas_equalTo(width);
    }];
    [self.buttonArray addObject:audioButton];
    
    UIButton *emojiButton = [[UIButton alloc] init];
    [emojiButton setImage:[UIImage imageNamed:@"chatbar_face"] forState:UIControlStateNormal];
    [emojiButton setImage:[UIImage imageNamed:@"chatbar_face_blue"] forState:UIControlStateSelected];
    [emojiButton addTarget:self action:@selector(emoticonButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:emojiButton];
    [emojiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(audioButton.mas_right);
        make.bottom.equalTo(audioButton);
        make.width.mas_equalTo(width);
    }];
    [self.buttonArray addObject:emojiButton];
    
    UIButton *cameraButton = [[UIButton alloc] init];
    [cameraButton setImage:[UIImage imageNamed:@"chatbar_camera"] forState:UIControlStateNormal];
    [cameraButton setImage:[UIImage imageNamed:@"chatbar_camera_blue"] forState:UIControlStateHighlighted];
    [cameraButton addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:cameraButton];
    [cameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(emojiButton.mas_right);
        make.bottom.equalTo(emojiButton);
        make.width.mas_equalTo(width);
    }];
    [self.buttonArray addObject:cameraButton];
    
    UIButton *photoButton = [[UIButton alloc] init];
    [photoButton setImage:[UIImage imageNamed:@"chatbar_photo"] forState:UIControlStateNormal];
    [photoButton setImage:[UIImage imageNamed:@"chatbar_photo_blue"] forState:UIControlStateHighlighted];
    [photoButton addTarget:self action:@selector(photoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:photoButton];
    [photoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(cameraButton.mas_right);
        make.bottom.equalTo(cameraButton);
        make.width.mas_equalTo(width);
    }];
    [self.buttonArray addObject:photoButton];
    
    UIButton *adressButton = [[UIButton alloc] init];
    [adressButton setImage:[UIImage imageNamed:@"chatbar_map"] forState:UIControlStateNormal];
    [adressButton setImage:[UIImage imageNamed:@"chatbar_map_blue"] forState:UIControlStateHighlighted];
    [adressButton addTarget:self action:@selector(addressButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:adressButton];
    [adressButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(photoButton.mas_right);
        make.bottom.equalTo(photoButton);
        make.width.mas_equalTo(width);
    }];
    [self.buttonArray addObject:adressButton];
    
    UIButton *callButton = [[UIButton alloc] init];
    [callButton setImage:[UIImage imageNamed:@"chatbar_call"] forState:UIControlStateNormal];
    [callButton setImage:[UIImage imageNamed:@"chatbar_call_blue"] forState:UIControlStateSelected];
    [callButton addTarget:self action:@selector(callButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:callButton];
    [callButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(adressButton.mas_right);
        make.bottom.equalTo(adressButton);
        make.width.mas_equalTo(width);
        //make.right.equalTo(self.buttonsView);
    }];
    [self.buttonArray addObject:callButton];
    
    UIButton *moreButton = [[UIButton alloc] init];
    [moreButton setImage:[UIImage imageNamed:@"chatbar_extend"] forState:UIControlStateNormal];
    [moreButton setImage:[UIImage imageNamed:@"chatbar_extend_blue"] forState:UIControlStateSelected];
    [moreButton addTarget:self action:@selector(moreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:moreButton];
    [moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buttonsView);
        make.left.equalTo(callButton.mas_right);
        make.bottom.equalTo(callButton);
        make.right.equalTo(self.buttonsView);
    }];
    [self.buttonArray addObject:moreButton];
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (self.currentMoreView) {
        [self.currentMoreView removeFromSuperview];
        [self.buttonsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-EMVIEWBOTTOMMARGIN);
        }];
    }
    
    for (UIButton *button in self.buttonArray) {
        if (button.isSelected) {
            button.selected = NO;
            break;
        }
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputView:shouldChangeTextInRange:replacementText:)]) {
        return [self.delegate inputView:self.textView shouldChangeTextInRange:range replacementText:text];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self _updatetextViewHeight];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputViewDidChange:)]) {
        [self.delegate inputViewDidChange:self.textView];
    }
}

#pragma mark - Private

- (CGFloat)_gettextViewContontHeight
{
    if (self.version >= 7.0) {
        return ceilf([self.textView sizeThatFits:self.textView.frame.size].height);
    } else {
        return self.textView.contentSize.height;
    }
}

- (void)_updatetextViewHeight
{
    CGFloat height = [self _gettextViewContontHeight];
    if (height < ktextViewMinHeight) {
        height = ktextViewMinHeight;
    }
    if (height > ktextViewMaxHeight) {
        height = ktextViewMaxHeight;
    }
    
    if (height == self.previousTextViewContentHeight) {
        return;
    }
    
    self.previousTextViewContentHeight = height;
    [self.textView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
}

- (void)_remakeButtonsViewConstraints
{
    if (self.currentMoreView) {
        [self.buttonsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.textView.mas_bottom).offset(5);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@55);
            make.bottom.equalTo(self.currentMoreView.mas_top);
        }];
    } else {
        [self.buttonsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.textView.mas_bottom).offset(5);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@55);
            make.bottom.equalTo(self).offset(-EMVIEWBOTTOMMARGIN);
        }];
    }
}

#pragma mark - Public

- (void)clearInputViewText
{
    self.textView.text = @"";
    [self _updatetextViewHeight];
}

- (void)inputViewAppendText:(NSString *)aText
{
    if ([aText length] > 0) {
        self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, aText];
        [self _updatetextViewHeight];
    }
}

- (void)clearMoreViewAndSelectedButton
{
    if (self.currentMoreView) {
        [self.currentMoreView removeFromSuperview];
        self.currentMoreView = nil;
        [self _remakeButtonsViewConstraints];
    }
    
    if (self.selectedButton) {
        self.selectedButton.selected = NO;
        self.selectedButton = nil;
    }
}

#pragma mark - Action

- (void)_buttonAction:(UIButton *)aButton
{
    [self.textView resignFirstResponder];
    
    aButton.selected = !aButton.selected;
    if (self.currentMoreView) {
        [self.currentMoreView removeFromSuperview];
        self.currentMoreView = nil;
        [self _remakeButtonsViewConstraints];
    }
    
    if (self.selectedButton != aButton) {
        self.selectedButton.selected = NO;
        self.selectedButton = nil;
        self.selectedButton = aButton;
    } else {
        self.selectedButton = nil;
    }
    
    if (aButton.selected) {
        self.selectedButton = aButton;
    }
}

- (void)audioButtonAction:(UIButton *)aButton
{
    [self _buttonAction:aButton];
    if (aButton.selected) {
        if (self.recordAudioView) {
            self.currentMoreView = self.recordAudioView;
            [self addSubview:self.recordAudioView];
            [self.recordAudioView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.right.equalTo(self);
                make.bottom.equalTo(self).offset(-EMVIEWBOTTOMMARGIN);
            }];
            [self _remakeButtonsViewConstraints];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidShowMoreViewAction)]) {
                [self.delegate chatBarDidShowMoreViewAction];
            }
        }
    }
}

- (void)emoticonButtonAction:(UIButton *)aButton
{
    [self _buttonAction:aButton];
    if (aButton.selected) {
        if (self.moreEmoticonView) {
            self.currentMoreView = self.moreEmoticonView;
            [self addSubview:self.moreEmoticonView];
            [self.moreEmoticonView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.right.equalTo(self);
                make.bottom.equalTo(self).offset(-EMVIEWBOTTOMMARGIN);
                make.height.mas_equalTo(self.moreEmoticonView.viewHeight);
            }];
            [self _remakeButtonsViewConstraints];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidShowMoreViewAction)]) {
                [self.delegate chatBarDidShowMoreViewAction];
            }
        }
    }
}

- (void)addButtonAction:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidAddAction)]){
        [self.delegate chatBarDidAddAction];
    }
}

- (void)carButtonAction:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidCarAction)]){
        [self.delegate chatBarDidCarAction];
    }
}

- (void)roomButtonAction:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidRoomAction)]){
        [self.delegate chatBarDidRoomAction];
    }
}

//更多
- (void)moreButtonAction:(UIButton *)aButton
{
    [self _buttonAction:aButton];
    if (aButton.selected){
        if(self.moreFunctionView) {
            self.currentMoreView = self.moreFunctionView;
            [self addSubview:self.moreFunctionView];
            [self.moreFunctionView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.right.equalTo(self);
                make.bottom.equalTo(self).offset(-EMVIEWBOTTOMMARGIN);
                make.height.mas_equalTo(@150);
            }];
            [self _remakeButtonsViewConstraints];
            if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidShowMoreViewAction)]) {
                [self.delegate chatBarDidShowMoreViewAction];
            }
        }
    }
}

- (void)callButtonAction:(UIButton *)aButton
{
    [self clearMoreViewAndSelectedButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidCallAction)]) {
        [self.delegate chatBarDidCallAction];
    }
}

- (void)cameraButtonAction:(UIButton *)aButton
{
    [self clearMoreViewAndSelectedButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidCameraAction)]) {
        [self.delegate chatBarDidCameraAction];
    }
}

- (void)photoButtonAction:(UIButton *)aButton
{
    [self clearMoreViewAndSelectedButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidPhotoAction)]) {
        [self.delegate chatBarDidPhotoAction];
    }
}

- (void)addressButtonAction:(UIButton *)aButton
{
    [self clearMoreViewAndSelectedButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatBarDidLocationAction)]) {
        [self.delegate chatBarDidLocationAction];
    }
}

@end
