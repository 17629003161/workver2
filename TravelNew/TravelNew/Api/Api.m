//
//  Api.m
//  rider
//
//  Created by 林海 on 2018/12/19.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "Api.h"
#import "YTKNetworkConfig.h"

@implementation Api {
    YTKRequestMethod _method;
    NSString *_url;
    NSDictionary *_para;
    NSString *_apiDesction;
}

- (id)initWithUrl:(NSString *)url
           method:(YTKRequestMethod)method
             para:(NSDictionary *)para
      description:(NSString *)desction{
    self = [super init];
    if (self) {
        _url = url;
        _method = method;
        _para = para;
        _withToken = YES;
        _apiDesction = desction;
        DLog(@"\n 描述：%@\n Api = %@ \n para = %@",desction==nil? @"api未描述":desction,[NSString stringWithFormat:@"%@%@",[self baseUrl],_url],_para);
    }
    return self;
}

- (BOOL)apiName{
    return _apiDesction;
}

+ (Api *)apiPostUrl:(NSString *)url para:(NSDictionary *)para description:(NSString *)desction;
{
    Api *api = [[Api alloc] initWithUrl:url
                                 method:YTKRequestMethodPOST
                                   para:para
                            description:desction];
    return api;
    
}
+ (Api *)apiGetUrl:(NSString *)url para:(NSDictionary *)para description:(NSString *)desction;
{
    Api *api = [[Api alloc] initWithUrl:url
                                 method:YTKRequestMethodGET
                                   para:para
                            description:desction];
    return api;
}


- (NSString *)baseUrl{
    return ApiBaseUrl;
}
- (NSString *)requestUrl {
    // “https://www.yuantiku.com” 在 YTKNetworkConfig 中设置，这里只填除去域名剩余的网址信息
    return _url;
}

- (YTKRequestMethod)requestMethod {
    return _method;
}   

- (id)requestArgument {
    return _para;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    UserInfo *user = [UserInfo shareInstance];
    if(user.isLogin){
        NSMutableDictionary *dict = [NSMutableDictionary new];
        if(!isNull(user.token))
            [dict setObject:user.token forKey:@"token"];
        if(!isNull(user.userId))
            [dict setObject:[user.userId stringValue] forKey:@"userId"];
        if(!isNull(user.currentUserStyle))
            [dict setObject:[user.currentUserStyle stringValue] forKey:@"userStyle"];
        return dict;
    }else{
        return @{};
    }
}

//返回大于零的数字，启动缓存机制
- (NSInteger)cacheTimeInSeconds{
    return 0;
}

@end
