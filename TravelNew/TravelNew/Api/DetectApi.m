//
//  DetectApi.m
//  TravelNew
//
//  Created by mac on 2020/11/20.
//  Copyright © 2020 lester. All rights reserved.
//

#import "DetectApi.h"
#import "YTKNetworkConfig.h"

@implementation DetectApi {
    YTKRequestMethod _method;
    NSString *_url;
    NSDictionary *_para;
}

- (id)initWithUrl:(NSString *)url
           method:(YTKRequestMethod)method
             para:(NSDictionary *)para
      description:(NSString *)desction{
    self = [super init];
    if (self) {
        _url = url;
        _method = method;
        _para = para;
        NSLog(@"\n 描述：%@\n Api = %@ \n para = %@",desction==nil? @"api未描述":desction,[NSString stringWithFormat:@"%@%@",[self baseUrl],_url],_para);
    }
    return self;
}

+ (DetectApi *)apiPostUrl:(NSString *)url para:(NSDictionary *)para description:(NSString *)desction;
{
    DetectApi *api = [[DetectApi alloc] initWithUrl:url
                                 method:YTKRequestMethodPOST
                                   para:para
                            description:desction];
    return api;
    
}
+ (DetectApi *)apiGetUrl:(NSString *)url para:(NSDictionary *)para description:(NSString *)desction;
{
    DetectApi *api = [[DetectApi alloc] initWithUrl:url
                                 method:YTKRequestMethodGET
                                   para:para
                            description:desction];
    return api;
}

- (NSString *)baseUrl{
    return @"";
}

- (NSString *)requestUrl {
    // “https://www.yuantiku.com” 在 YTKNetworkConfig 中设置，这里只填除去域名剩余的网址信息
    return _url;
}

- (YTKRequestMethod)requestMethod {
    return _method;
}

- (id)requestArgument {
    return _para;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    return @{@"Content-Type":@"application/x-www-form-urlencoded"};
}

//返回大于零的数字，启动缓存机制
- (NSInteger)cacheTimeInSeconds{
    return 10;
}

@end
