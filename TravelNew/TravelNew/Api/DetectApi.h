//
//  DetectApi.h
//  TravelNew
//
//  Created by mac on 2020/11/20.
//  Copyright © 2020 lester. All rights reserved.
//

#import <YTKNetwork/YTKNetwork.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetectApi : YTKRequest
- (id)initWithUrl:(NSString *)url
           method:(YTKRequestMethod)method
             para:(NSDictionary *)para
      description:(NSString *)desction;

+ (DetectApi *)apiPostUrl:(NSString *)url
               para:(NSDictionary *)para
        description:(NSString *)desction;

+ (DetectApi *)apiGetUrl:(NSString *)url
              para:(NSDictionary *)para
       description:(NSString *)desction;
@end

NS_ASSUME_NONNULL_END
