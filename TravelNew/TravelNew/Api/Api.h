//
//  Api.h
//  rider
//
//  Created by 林海 on 2018/12/19.
//  Copyright © 2018年 Mac. All rights reserved.
//
#import "YTKRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface Api : YTKRequest

@property(nonatomic,assign) BOOL withToken;      //设置为yes 表示请求头中包含userId和token

- (id)initWithUrl:(NSString *)url
           method:(YTKRequestMethod)method
             para:(NSDictionary *)para
      description:(NSString *)desction;

+ (Api *)apiPostUrl:(NSString *)url
               para:(NSDictionary *)para
        description:(NSString *)desction;

+ (Api *)apiGetUrl:(NSString *)url
              para:(NSDictionary *)para
       description:(NSString *)desction;

- (BOOL)apiName;
@end

NS_ASSUME_NONNULL_END
