//
//  TourSelectView.h
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TourSelectView : LKBaseDialogView
@property(nonatomic,copy) void (^selectBlock)(NSInteger);
@end

NS_ASSUME_NONNULL_END
