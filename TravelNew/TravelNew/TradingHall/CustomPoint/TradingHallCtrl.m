//
//  TradingHallCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import "TradingHallCtrl.h"

#import "MatchingTableViewCell.h"
#import "DownwindSelectTimeCtl.h"
#import "TourSelectView.h"
#import "DownwindSetPlacesCtrl.h"

@interface TradingHallCtrl ()

@end

@implementation TradingHallCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupUI{
    self.view.backgroundColor = HexColor(0x1E69FF);
    UILabel *label = [Utils createLabelWithTitle:@"需求大厅" titleColor:HexColor(0xffffff) fontSize:16];
    [self.view addSubview:label];
    label.sd_layout
    .topSpaceToView(self.view, 36)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    self.tableView.sd_layout
    .topSpaceToView(label, 34)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self.tableView registerClass:[MatchingTableViewCell class] forCellReuseIdentifier:@"MatchingTableViewCell"];
    self.tableView.tableHeaderView = [self makeHeaderView];
}

- (UIView *)makeHeaderView{
    LKWeakSelf
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 344)];
    view.backgroundColor = [UIColor whiteColor];
    TourSelectView *selectView = [[TourSelectView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 262)];
    selectView.selectBlock = ^(NSInteger index) {
        DownwindSetPlacesCtrl *vc = [[DownwindSetPlacesCtrl alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    UIView *payview = [self createPayView];
    payview.frame = CGRectMake(0, 262, kScreenWidth, 82);
    [view sd_addSubviews:@[selectView,payview]];
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    MatchingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MatchingTableViewCell"];
    cell.funcBlock = ^{
        DownwindSelectTimeCtl *vc = [DownwindSelectTimeCtl new];
      //  vc.tripStep = TripStepInvite;
       // vc.tripStep = TripSure;
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UIView *)createPayView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0xe1e1e1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 13;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;
    UILabel *label = [Utils createBoldLabelWithTitle:@"预付车费" titleColor:HexColor(0x141418) fontSize:15];
    UILabel *label2 = [Utils createLabelWithTitle:@"预付后，将自动确认与接单车主同行" titleColor:HexColor(0x333333) fontSize:12];
    UILabel *label3 = [Utils createLabelWithTitle:@"去支付" titleColor:HexColor(0x1E69FF) fontSize:12];
    [view sd_addSubviews:@[label,label2,label3]];
    label.sd_layout
    .topSpaceToView(view, 13)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(label, 9.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 19.5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


@end

