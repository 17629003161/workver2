//
//  TourSelectView.m
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import "TourSelectView.h"

@interface TourSelectView()
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UILabel *funcLabel1;
@property(nonatomic,strong) UILabel *funcLabel2;
@property(nonatomic,strong) UIView *indictLine;
@property(nonatomic,strong) UILabel *sourceAddrLabel;
@property(nonatomic,strong) UILabel *targetAddrLabel;
@property(nonatomic,strong) UILabel *endAddrLabel;
@property(nonatomic,strong) UIImageView *addimgv;
@end

@implementation TourSelectView

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    if(selectIndex==0){
        self.funcLabel1.textColor = HexColor(0x1E69FF);
        self.funcLabel2.textColor = HexColor(0x383232);
        self.sourceAddrLabel.text = @"你将从 利君未来城-2期 出发";
        [self.sourceAddrLabel changeStrings:@[@"利君未来城-2期"] toColor:HexColor(0x1E69FF)];
        self.targetAddrLabel.text = @"华清池+兵马俑";
        self.endAddrLabel.text = @"请选择您的返程目的地";
        self.addimgv.hidden = NO;
        [UIView animateWithDuration:0.2 animations:^{
                    self.indictLine.centerX = self.funcLabel1.centerX;
                }];
    }else if(selectIndex==1){
        self.funcLabel1.textColor = HexColor(0x383232);
        self.funcLabel2.textColor = HexColor(0x1E69FF);
        self.sourceAddrLabel.text = @"你将从 利君未来城-2期 开始旅行";
        [self.sourceAddrLabel changeStrings:@[@"利君未来城-2期"] toColor:HexColor(0x1E69FF)];
        self.targetAddrLabel.text = @"你想在途中那个城市/景区观光(选填)";
        self.endAddrLabel.text = @"选择你的旅行目的地";
        self.addimgv.hidden = YES;
        [UIView animateWithDuration:0.2 animations:^{
                    self.indictLine.centerX = self.funcLabel2.centerX;
                }];
    }
}

-(void)configViews{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = HexColor(0xe1e1e1).CGColor;
    self.layer.borderWidth = 0.5;
    self.layer.cornerRadius = 8;
    self.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,3);
    self.layer.shadowOpacity = 1;
    self.layer.cornerRadius = 10;
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"周边游" titleColor:HexColor(0x1E69FF) fontSize:16];
    self.funcLabel1 = label1;
    [label1 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"顺风游" titleColor:HexColor(0x383232) fontSize:16];
    self.funcLabel2 = label2;
    [label2 addTapGestureTarget:self action:@selector(funcIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0x1E69FF);
    self.indictLine = line;
    UIView *view1 = [self createItemViewColor:HexColor(0xF8F9FB) dotImage:@"home_icon_origin"];
    self.sourceAddrLabel = (UILabel *)[view1 viewWithTag:1000];
    self.sourceAddrLabel.text = @"你将从 利君未来城-2期 出发";
    [self.sourceAddrLabel changeStrings:@[@"利君未来城-2期"] toColor:HexColor(0x1E69FF)];
    UIView *view2 = [self createItemViewColor:HexColor(0xF2FCFF) dotImage:@"home_icon_attractions"];
    self.targetAddrLabel = (UILabel *)[view2 viewWithTag:1000];
    self.addimgv = (UIImageView *)[view2 viewWithTag:1001];
    self.addimgv.hidden = NO;
    UIView *view3 = [self createItemViewColor:HexColor(0xEDF3FF) dotImage:@"home_icon_destination"];
    self.endAddrLabel = (UILabel *)[view3 viewWithTag:1000];
    self.endAddrLabel.text = @"请选择您的返程目的地";
    [self sd_addSubviews:@[label1,label2,line,view1,view2,view3]];
    label1.sd_layout
    .topSpaceToView(self, 20)
    .rightSpaceToView(self, kScreenWidth/2.0+33)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(label1)
    .leftSpaceToView(label1, 66)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .centerXEqualToView(label1)
    .topSpaceToView(label1, 9.5)
    .widthIs(21)
    .heightIs(2);
    
    view1.sd_layout
    .topSpaceToView(line, 20)
    .leftSpaceToView(self, 12.5)
    .rightSpaceToView(self, 12.5)
    .heightIs(45);
    
    view2.sd_layout
    .topSpaceToView(view1, 15)
    .leftSpaceToView(self, 12.5)
    .rightSpaceToView(self, 12.5)
    .heightIs(45);
    
    view3.sd_layout
    .topSpaceToView(view2, 15)
    .leftSpaceToView(self, 12.5)
    .rightSpaceToView(self, 12.5)
    .heightIs(45);
    [self addTapGestureTarget:self action:@selector(viewIsTaped:)];
}

- (void)viewIsTaped:(UITapGestureRecognizer *)reg
{
    if(self.selectBlock){
        self.selectBlock(self.selectIndex);
    }
}


- (void)funcIsClicked:(UITapGestureRecognizer *)reg
{
    UILabel *label = (UILabel *)reg.view;
    if([label.text isEqualToString:@"周边游"]){
        self.selectIndex = 0;
    }else{
        self.selectIndex = 1;
    }
}

- (UIView *)createItemViewColor:(UIColor *)bgcolor dotImage:(NSString *)imgname{
    UIView *view = [UIView new];
    view.backgroundColor = bgcolor;
    UIImageView *imgv = ImageViewWithImage(imgname);
    UILabel *label = [Utils createLabelWithTitle:@"华清池+兵马俑" titleColor:HexColor(0x333333) fontSize:15];
    label.tag = 1000;
    UIImageView *addimgv = ImageViewWithImage(@"home_btn_add");
    addimgv.tag = 1001;
    [view sd_addSubviews:@[imgv,label,addimgv]];
    addimgv.hidden = YES;
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .widthIs(6)
    .heightIs(6);
    label.sd_layout
    .leftSpaceToView(imgv, 10)
    .rightSpaceToView(view, 10)
    .centerYEqualToView(view)
    .autoHeightRatio(0);
    addimgv.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 16.5)
    .widthIs(21)
    .heightIs(21);
    
    return view;
}

@end
