//
//  TradingHallDriverCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import "TradingHallDriverCtrl.h"

#import "DemendHallCell.h"
#import "DownwindSelectTimeCtl.h"
#import "TourSelectView.h"
#import "DownwindSetPlacesCtrl.h"

@interface TradingHallDriverCtrl ()

@end

@implementation TradingHallDriverCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupUI{
    self.view.backgroundColor = HexColor(0x1E69FF);
    UILabel *label = [Utils createLabelWithTitle:@"需求大厅" titleColor:HexColor(0xffffff) fontSize:16];
    [self.view addSubview:label];
    label.sd_layout
    .topSpaceToView(self.view, 36)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    self.tableView.sd_layout
    .topSpaceToView(label, 34)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self.tableView registerClass:[DemendHallCell class] forCellReuseIdentifier:@"DemendHallCell"];
    self.tableView.tableHeaderView = [self makeHeaderView];
}

- (UIView *)makeHeaderView{
    LKWeakSelf
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 280)];
    view.backgroundColor = [UIColor whiteColor];
    TourSelectView *selectView = [[TourSelectView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 262)];
    selectView.selectBlock = ^(NSInteger index) {
        DownwindSetPlacesCtrl *vc = [[DownwindSetPlacesCtrl alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    [view sd_addSubviews:@[selectView]];
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    DemendHallCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DemendHallCell"];
    cell.funcBlock = ^{
        DownwindSelectTimeCtl *vc = [DownwindSelectTimeCtl new];
      //  vc.tripStep = TripStepInvite;
       // vc.tripStep = TripSure;
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


@end
