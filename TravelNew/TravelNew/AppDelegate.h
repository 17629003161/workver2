//
//  AppDelegate.h
//  TravelNew
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 lester. All rights reserved.
//

//新版第二版

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow * window;

@end

