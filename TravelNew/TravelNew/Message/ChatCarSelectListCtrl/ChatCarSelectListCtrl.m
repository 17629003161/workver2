//
//  ChatCarSelectListCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ChatCarSelectListCtrl.h"
#import "CarInfoCell.h"

@interface ChatCarSelectListCtrl ()

@end

@implementation ChatCarSelectListCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"选择可用车辆" titleColor:nil];
    [self.tableView registerClass:[CarInfoCell class] forCellReuseIdentifier:@"CarInfoCell"];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    UserInfo *user = [UserInfo shareInstance];
    Api *api = [Api apiPostUrl:@"work/price/getUserCarsInfo"
                           para:@{@"userId":user.userId}
                                   description:@"查询推荐官车辆报价列表"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"jsonstr  = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        NSInteger code = [[json objectForKey:@"code"] integerValue];
        [weakSelf.dataArray removeAllObjects];
        if(code==200){
            NSArray *data = [json objectForKey:@"data"];
            [weakSelf.dataArray removeAllObjects];
            for(NSDictionary *dict in data){
                CarDataEntity *model = [[CarDataEntity alloc] initWithJson:dict];
                [weakSelf.dataArray addObject:model];
            }
        }else if(code==500){
            NSString *msg = [json objectForKey:@"msg"];
            [MBProgressHUD showMessage:msg];
        }
        [weakSelf.tableView reloadData];
        [weakSelf checkBlankView];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarInfoCell"];
    CarDataEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 46;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarDataEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    if(self.onSelectBlock){
        self.onSelectBlock(model);
    }
    [self.navigationController popViewControllerAnimated:YES];
}




@end
