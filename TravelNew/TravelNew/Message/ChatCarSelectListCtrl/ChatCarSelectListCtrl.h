//
//  ChatCarSelectListCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "CarDataEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatCarSelectListCtrl : LKBaseTableViewController
@property (nonatomic, copy) void(^onSelectBlock)(CarDataEntity *);
@end

NS_ASSUME_NONNULL_END
