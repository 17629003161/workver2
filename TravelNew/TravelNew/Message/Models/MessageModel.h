//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface MessageModel : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *messageId;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSNumber *teg;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,strong) NSNumber *creatTime;
@property (nonatomic,strong) NSNumber *messageType;
@property (nonatomic,assign) NSInteger badgeValue;
 
- (void)save;
+ (MessageModel *)read;

-(id)initWithJson:(NSDictionary *)json;

@end
