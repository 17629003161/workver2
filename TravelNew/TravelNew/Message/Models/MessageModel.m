//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "MessageModel.h"

@implementation MessageModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.messageId  = [json objectForKey:@"messageId"];
		self.content  = [json objectForKey:@"content"];
		self.userId  = [json objectForKey:@"userId"];
		self.teg  = [json objectForKey:@"teg"];
		self.title  = [json objectForKey:@"title"];
		self.creatTime  = [json objectForKey:@"creatTime"];
		self.messageType  = [json objectForKey:@"messageType"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.messageId forKey:@"zx_messageId"];
	[aCoder encodeObject:self.content forKey:@"zx_content"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.teg forKey:@"zx_teg"];
	[aCoder encodeObject:self.title forKey:@"zx_title"];
	[aCoder encodeObject:self.creatTime forKey:@"zx_creatTime"];
	[aCoder encodeObject:self.messageType forKey:@"zx_messageType"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.messageId = [aDecoder decodeObjectForKey:@"zx_messageId"];
		self.content = [aDecoder decodeObjectForKey:@"zx_content"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.teg = [aDecoder decodeObjectForKey:@"zx_teg"];
		self.title = [aDecoder decodeObjectForKey:@"zx_title"];
		self.creatTime = [aDecoder decodeObjectForKey:@"zx_creatTime"];
		self.messageType = [aDecoder decodeObjectForKey:@"zx_messageType"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"messageId : %@\n",self.messageId];
	result = [result stringByAppendingFormat:@"content : %@\n",self.content];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"teg : %@\n",self.teg];
	result = [result stringByAppendingFormat:@"title : %@\n",self.title];
	result = [result stringByAppendingFormat:@"creatTime : %@\n",self.creatTime];
	result = [result stringByAppendingFormat:@"messageType : %@\n",self.messageType];
	
    return result;
}

- (void)save{
    //归档
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"messageModel.data"];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}


+ (MessageModel *)read{
    //解档
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"messageModel.data"];
    MessageModel *entity = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    return entity;
}




@end
