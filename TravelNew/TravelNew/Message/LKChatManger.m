//
//  LKChatManger.m
//  TravelNew
//
//  Created by mac on 2020/11/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKChatManger.h"

@implementation LKChatManger

static LKChatManger* _instance = nil;

+(instancetype) shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    }) ;
    [_instance countInit];
    return _instance ;
}

- (void)countInit{
    UserInfo *user = [UserInfo shareInstance];
    if(user.isLogin){
        self.chartName = user.easemobId;
        self.chatPasswd = user.password;
    }
}

- (void)chatLogout{
    [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
        if (!aError) {
            DLog(@"退出登录成功");
            [MBProgressHUD showMessage:@"退出登录成功"];
        } else {
            DLog(@"退出登录失败的原因---%@", aError.errorDescription);
            [MBProgressHUD showMessage:[NSString stringWithFormat:@"退出登录失败的原因---%@", aError.errorDescription]];
        }
    }];
}

- (void)chatLogIn{
    [[EMClient sharedClient] loginWithUsername:self.chartName
                                      password:self.chatPasswd
                                    completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            DLog(@"环信登录成功");
            [MBProgressHUD showMessage:@"环信登录成功"];
        } else {
            DLog(@"登录失败的原因---%@", aError.errorDescription);
        }
    }];
}

@end
