//
//  LKChatManger.h
//  TravelNew
//
//  Created by mac on 2020/11/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKChatManger : NSObject
@property (nonatomic, copy, nullable) void (^successCompletionBlock)(void);
@property(nonatomic,strong) NSString *chartName;
@property(nonatomic,strong) NSString *chatPasswd;
@end

NS_ASSUME_NONNULL_END
