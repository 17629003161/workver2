//
//  ChatRoomSelectListCtrl.m
//  TravelNew
//
//  Created by mac on 2020/12/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ChatRoomSelectListCtrl.h"
#import "HotelInfoCell.h"
#import "LKShowPictureView.h"

@interface ChatRoomSelectListCtrl ()

@end

@implementation ChatRoomSelectListCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"选择可用房屋" titleColor:nil];
    [self.tableView registerClass:[HotelInfoCell class] forCellReuseIdentifier:@"HotelInfoCell"];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    UserInfo *user = [UserInfo shareInstance];
    Api *api1 = [Api apiPostUrl:@"work/house/getUserHouseInfo"
                           para:@{@"userId":user.userId}
                    description:@"查询推荐官住宿信息列表"];
    [api1 startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"api1 jsonstr  = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        NSInteger code = [[json objectForKey:@"code"] integerValue];
        [self.dataArray removeAllObjects];
        if(code==200){
            NSArray *data = [json objectForKey:@"data"];
            [self.dataArray removeAllObjects];
            for(NSDictionary *dict in data){
               RoomSeupListEntity *model = [[RoomSeupListEntity alloc] initWithJson:dict];
                [weakSelf.dataArray addObject:model];
            }
        }
        [weakSelf.tableView reloadData];
        [weakSelf checkBlankView];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelInfoCell"];
    RoomSeupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    cell.showPictureBlock = ^(NSArray * _Nonnull picAry, UIView *picView) {
        CGRect startFrame = [picView convertRect:picView.bounds toView:self.view];
        LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        view.pictureArray = picAry;
        [view show:startFrame];
    };
    cell.navigationBlock = ^(RoomSeupListEntity * _Nonnull model) {
        LKWeakSelf
        NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
        NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
        NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
        [weakSelf doNavigationWithEndLocation:endLocation];
    };
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 339;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 46;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RoomSeupListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    if(self.onSelectBlock){
        self.onSelectBlock(model);
    }
    [self.navigationController popViewControllerAnimated:YES];
}




@end
