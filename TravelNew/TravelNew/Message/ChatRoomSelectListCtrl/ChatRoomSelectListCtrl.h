//
//  ChatRoomSelectListCtrl.h
//  TravelNew
//
//  Created by mac on 2020/12/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "RoomSeupListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatRoomSelectListCtrl : LKBaseTableViewController
@property (nonatomic, copy) void(^onSelectBlock)(RoomSeupListEntity *);

@end

NS_ASSUME_NONNULL_END
