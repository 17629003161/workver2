//
//  LKMessageCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//


#import "LKMessageCtrl.h"
#import "SystemMessageCtrl.h"
#import "MessageCell.h"
#import "EMChatViewController.h"

@interface LKMessageCtrl ()

@end

@implementation LKMessageCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"消息" titleColor:nil];

    [self.tableView registerClass:[MessageCell class] forCellReuseIdentifier:@"MessageCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/message/messageList"
                          para:@{
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"获取消息列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(MessageModel.class);
    return name;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageModel *model = [self.dataArray objectAtIndex:indexPath.row];
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 16)];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 16.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageModel *model = [self.dataArray objectAtIndex:indexPath.row];
    if([model.messageType integerValue] == MessageTypeSystem){
        SystemMessageCtrl *vc = [SystemMessageCtrl new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{

    }
}

#pragma mark - 设置 tableView 能否编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - 左滑动哪种编辑状态
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - 修改删除字样
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}


// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteMessageInRow:indexPath.row];
    }
}

- (void)deleteMessageInRow:(NSInteger)row{
    Api *api = [Api apiPostUrl:@"work/message/cleanMessage"
                          para:@{}
                   description:@"清空系统消息"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
       //     [self.dataArray removeObjectAtIndex:row];
         //   [self.tableView reloadData];
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}



@end
