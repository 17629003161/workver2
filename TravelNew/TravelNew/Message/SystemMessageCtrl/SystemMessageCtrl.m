//
//  SystemMessageCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SystemMessageCell.h"
#import "SystemMessageCtrl.h"

@interface SystemMessageCtrl ()

@end

@implementation SystemMessageCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"系统消息" titleColor:nil];
    [self.tableView registerClass:[SystemMessageCell class] forCellReuseIdentifier:@"SystemMessageCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .bottomEqualToView(self.view)
    .rightEqualToView(self.view);
    [self loadFirstPage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:Notification_NewMessageRead object:nil];
}

- (Api *)getListApi{
    Api *api = [Api apiPostUrl:@"work/message/messageList"
                          para:@{
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"获取消息列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(MessageModel.class);
    return name;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SystemMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SystemMessageCell"];
    MessageModel *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageModel *model = [self.dataArray objectAtIndex:indexPath.row];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[SystemMessageCell class] contentViewWidth:kScreenWidth];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 16)];
    view.backgroundColor = HexColor(0xF9F9F9);
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 16.0;
}

@end
