//
//  SystemMessageCell.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SystemMessageCell.h"

@interface SystemMessageCell()
@property(nonatomic,strong) UILabel *titleL;
@property(nonatomic,strong) UILabel *subTitleL;
@property(nonatomic,strong) UILabel *dateTimeL;
@end

@implementation SystemMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MessageModel *)model
{
    _model = model;
    _titleL.text = model.title;
    _subTitleL.text = model.content;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.creatTime doubleValue]/1000.0;
    _dateTimeL.text = date.dateString;
    [_titleL updateLayout];
    [_subTitleL updateLayout];
    [_dateTimeL updateLayout];
    [self updateLayout];

}

- (void)configViews{
    _titleL = [Utils createLabelWithTitle:@"您的用车消息已发布您的用车消息已发布" titleColor:HexColor(0x333333) fontSize:16];
    _subTitleL = [Utils createLabelWithTitle:@"文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案文案" titleColor:HexColor(0x999999) fontSize:12];
    _subTitleL.numberOfLines = 0;
    _dateTimeL = [Utils createLabelWithTitle:@"2020-08-04  11:42" titleColor:HexColor(0x999999) fontSize:12];
    [self.contentView sd_addSubviews:@[_titleL,_subTitleL,_dateTimeL]];

    _titleL.sd_layout
    .topSpaceToView(self.contentView, 25)
    .leftSpaceToView(self.contentView, 14)
    .autoHeightRatio(0);
    [_titleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _subTitleL.sd_layout
    .topSpaceToView(_titleL, 6)
    .leftEqualToView(_titleL)
    .rightSpaceToView(self.contentView, 12)
    .autoHeightRatio(0);
    
    _dateTimeL.sd_layout
    .topSpaceToView(_subTitleL, 3)
    .rightSpaceToView(self.contentView, 14)
    .autoHeightRatio(0);
    [_dateTimeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [self setupAutoHeightWithBottomView:_dateTimeL bottomMargin:8];
}
@end
