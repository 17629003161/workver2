//
//  SystemMessageCell.h
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "MessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SystemMessageCell : BaseCustomTableViewCell
@property(nonatomic,strong) MessageModel *model;
@end

NS_ASSUME_NONNULL_END
