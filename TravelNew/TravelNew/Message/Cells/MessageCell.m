//
//  MessageCell.m
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MessageCell.h"

@interface MessageCell()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *title;
@property(nonatomic,strong) UILabel *subTitle;
@property(nonatomic,strong) EMBadgeLabel *badgeLabel;
@end


@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MessageModel *)model
{
    _model = model;
    if([model.messageType integerValue] == MessageTypeSystem){
        _imgv.image = [UIImage imageNamed:@"you_come"];
    }else{
        _imgv.image = [UIImage imageNamed:@"person_header"];
    }
    self.title.text = model.title;
    self.subTitle.text = model.content;
    if(model.badgeValue > 0){
        self.badgeLabel.value = [NSString stringWithFormat:@" %ld ", model.badgeValue];
        self.badgeLabel.hidden = NO;
    }else{
        self.badgeLabel.hidden = YES;
    }
    [self.title updateLayout];
    [self.subTitle updateLayout];
}

- (void)configViews{
    _imgv = ImageViewWithImage(@"person_header");
    _imgv.layer.cornerRadius = 20.0;
    _imgv.layer.masksToBounds = YES;
    _title = [Utils createLabelWithTitle:@"紫金汇讯通" titleColor:HexColor(0x333333) fontSize:16];
    _subTitle = [Utils createLabelWithTitle:@"【旅咖出行】您的需求已发布" titleColor:HexColor(0x666666) fontSize:13];
    
    _badgeLabel = [[EMBadgeLabel alloc] init];
    _badgeLabel.clipsToBounds = YES;
    _badgeLabel.layer.cornerRadius = 10;
    [self.contentView addSubview:_badgeLabel];
    
    [self.contentView sd_addSubviews:@[_imgv,_title,_subTitle,_badgeLabel]];
    _imgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 12)
    .widthIs(44)
    .heightIs(44);
    
    _title.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(_imgv, 12)
    .autoHeightRatio(0);
    [_title setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _subTitle.sd_layout
    .topSpaceToView(_title,6)
    .leftEqualToView(_title)
    .rightSpaceToView(self.contentView,19)
    .autoHeightRatio(0);
    
    _badgeLabel.sd_layout
    .topEqualToView(_subTitle)
    .rightSpaceToView(self.contentView,19)
    .widthIs(20)
    .heightIs(20);
    
}
@end
