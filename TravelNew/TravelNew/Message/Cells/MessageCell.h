//
//  MessageCell.h
//  TravelNew
//
//  Created by mac on 2020/10/16.
//  Copyright © 2020 lester. All rights reserved.
//
#import "MessageModel.h"
#import "EMBadgeLabel.h"
#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageCell : BaseCustomTableViewCell
@property(nonatomic,strong) MessageModel *model;
@end

NS_ASSUME_NONNULL_END
