//
//  ChatFunctionView.h
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChartFunctionView : BaseCustomView
@property (nonatomic, copy) void(^OnCameraBlock)(NSString *);  //传递1个参数
@property (nonatomic, copy) void(^OnLocationBlock)(NSString *);  //传递1个参数
@property (nonatomic, copy) void(^OnRuteBlock)(NSString *);  //传递1个参数
@property (nonatomic, copy) void(^OnPhotoBlock)(NSString *);  //传递1个参数
@end

NS_ASSUME_NONNULL_END
