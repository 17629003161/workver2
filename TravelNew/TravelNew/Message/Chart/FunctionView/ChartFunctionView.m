//
//  ChatFunctionView.m
//  TravelNew
//
//  Created by mac on 2020/10/17.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ChartFunctionView.h"

@implementation ChartFunctionView

- (void)onPhotoIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.OnPhotoBlock)
        self.OnPhotoBlock(@"");
}
- (void)onCameraIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.OnCameraBlock)
        self.OnCameraBlock(@"");
}
- (void)onLocationIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.OnLocationBlock)
        self.OnLocationBlock(@"");
}
- (void)onRuteIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.OnRuteBlock)
        self.OnRuteBlock(@"");
}

- (void)configViews{
    UIView *view1 = [self createViewWithImage:@"photo_chat" title:@"照片"];
    UIView *view2 = [self createViewWithImage:@"camera_chat" title:@"拍照"];
    UIView *view3 = [self createViewWithImage:@"location_chart" title:@"位置"];
    UIView *view4 = [self createViewWithImage:@"rute_chart" title:@"报价"];
    [view1 addTapGestureTarget:self action:@selector(onPhotoIsClicked:)];
    [view2 addTapGestureTarget:self action:@selector(onCameraIsClicked:)];
    [view3 addTapGestureTarget:self action:@selector(onLocationIsClicked:)];
    [view4 addTapGestureTarget:self action:@selector(onRuteIsClicked:)];
    [self sd_addSubviews:@[view1,view2,view3,view4]];
    CGFloat width = (kScreenWidth-5*24)/4.0;
    view1.sd_layout
    .topSpaceToView(self, 24)
    .leftSpaceToView(self, 24)
    .widthIs(width);
    view2.sd_layout
    .leftSpaceToView(view1, 24)
    .centerYEqualToView(view1)
    .widthIs(width);
    view3.sd_layout
    .leftSpaceToView(view2, 24)
    .centerYEqualToView(view1)
    .widthIs(width);
    view4.sd_layout
    .leftSpaceToView(view3, 24)
    .centerYEqualToView(view1)
    .widthIs(width);
    [self setupAutoHeightWithBottomView:view4 bottomMargin:24];
}

- (UIView *)createViewWithImage:(NSString *)image title:(NSString *)title
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *imgv = ImageViewWithImage(image);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:12];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(imgv, 4)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}

@end
