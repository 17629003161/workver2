//
//  ChatModel.h
//  TravelNew
//
//  Created by mac on 2020/12/15.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKChatModel : JSONModel
@property (nonatomic,strong) NSString *orderNo;              //订单编号
@property (nonatomic,copy) NSNumber *ltRequirementInfoId;    //需求ID
@property (nonatomic,strong) NSNumber *type;                //订单类型（出行单or获取联系方式订单）
@property (nonatomic,strong) NSNumber *state;               //订单状态
@property (nonatomic,copy) NSString *cityid;                //发布单所在城市代码
@property (nonatomic,strong) NSNumber *creationTime;        //订单创建时间
@property (nonatomic,strong) NSNumber *travelTime;          //travelTime
@property (nonatomic,strong) NSNumber *endTime;             //订单结束时间
@property (nonatomic,strong) NSNumber *latitude;            //目的地坐标
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,copy) NSString *address;               //目标地点详细地点名称
@property (nonatomic,strong) NSNumber *number;              //出行人数
@property (nonatomic,copy) NSString *requirementDescription;         //需求单描述
@property (nonatomic,copy) NSString *easemobId;                 //创建需求单用户的即时通讯ID
@property (nonatomic,strong) NSNumber *userId;                  //创建需求单用户ID
@property (nonatomic,strong) NSNumber *offerPriceId;             //报价id
@property (nonatomic,strong) NSNumber *offerPrice;          //报价金额
@property (nonatomic,strong) NSNumber *isPayed;             //是否支付

-(id)initWithJson:(NSDictionary *)json;
+(LKChatModel *)makeChatModelFromJsonstr:(NSString *)jsonstr;
- (NSString *)toJsonstr;

@end

NS_ASSUME_NONNULL_END
