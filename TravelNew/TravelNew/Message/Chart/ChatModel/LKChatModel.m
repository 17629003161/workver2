//
//  ChatModel.m
//  TravelNew
//
//  Created by mac on 2020/12/15.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKChatModel.h"
#import <objc/runtime.h>

@implementation LKChatModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
        if(json != nil)
        {
            self.orderNo = [json objectForKey:@"orderNo"];
            self.ltRequirementInfoId  = [json objectForKey:@"ltRequirementInfoId"];
            self.type = [json objectForKey:@"type"];
            self.state  = [json objectForKey:@"state"];
            self.cityid  = [json objectForKey:@"cityid"];
            self.creationTime  = [json objectForKey:@"creationTime"];
            self.travelTime  = [json objectForKey:@"travelTime"];
            self.endTime  = [json objectForKey:@"endTime"];
            self.longitude  = [json objectForKey:@"longitude"];
            self.latitude  = [json objectForKey:@"latitude"];
            self.address  = [json objectForKey:@"address"];
            self.number  = [json objectForKey:@"number"];
            self.requirementDescription  = [json objectForKey:@"requirementDescription"];
            self.easemobId  = [json objectForKey:@"easemobId"];
            self.userId  = [json objectForKey:@"userId"];
            self.offerPriceId = [json objectForKey:@"offerPriceId"];
            self.offerPrice = [json objectForKey:@"offerPrice"];
            self.isPayed = [json objectForKey:@"isPayed"];
        }
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"orderNo : %@\n",self.orderNo];
    result = [result stringByAppendingFormat:@"state : %@\n",self.state];
    result = [result stringByAppendingFormat:@"travelTime : %@\n",self.travelTime];
    result = [result stringByAppendingFormat:@"creationTime : %@\n",self.creationTime];
    result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
    result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    result = [result stringByAppendingFormat:@"address : %@\n",self.address];
    result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
    result = [result stringByAppendingFormat:@"ltRequirementInfoId : %@\n",self.ltRequirementInfoId];
    result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
    result = [result stringByAppendingFormat:@"requirementDescription : %@\n",self.requirementDescription];
    result = [result stringByAppendingFormat:@"number : %@\n",self.number];
    result = [result stringByAppendingFormat:@"endTime : %@\n",self.endTime];
    
    result = [result stringByAppendingFormat:@"offerPriceId : %@\n",self.offerPriceId];
    result = [result stringByAppendingFormat:@"offerPrice : %@\n",self.offerPrice];
    result = [result stringByAppendingFormat:@"isPayed : %@\n",self.isPayed];
    result = [result stringByAppendingFormat:@"type : %@\n",self.type];

    return result;
}

+(LKChatModel *)makeChatModelFromJsonstr:(NSString *)jsonstr
{
    NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
    LKChatModel *model = [[LKChatModel alloc] initWithJson:json];
    return model;
}

- (NSString *)toJsonstr{
    NSDictionary *dict = [self changeToDic];
    return [self dictToJsonstr:dict];
}

//将对象转成字典
-(NSDictionary*)changeToDic{
    NSMutableDictionary*mDic = [NSMutableDictionary dictionary];
    for (NSString*key in [self propertyKeys]) {
        [mDic setValue:[self valueForKey:key] forKey:key];
    }
    return mDic;
}

- (NSString *)dictToJsonstr:(NSDictionary *)dict{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

//获取属性列表
- (NSArray*)propertyKeys
{
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    NSMutableArray *propertys = [NSMutableArray arrayWithCapacity:outCount];
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        [propertys addObject:propertyName];
    }
    free(properties);
    return propertys;
}

@end
