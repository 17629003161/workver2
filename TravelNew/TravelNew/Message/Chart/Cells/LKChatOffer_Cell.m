//
//  LKChatOffer_Cell.m
//  TravelNew
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKChatOffer_Cell.h"

@interface LKChatOffer_Cell()
@property(nonatomic,strong) UILabel *orderNumberL;
@property(nonatomic,strong) UILabel *orderDate;

@property(nonatomic,strong) UILabel *targetTipL;
@property(nonatomic,strong) UILabel *targetAddressL;
@property(nonatomic,strong) UILabel *travelTimeL;
@property(nonatomic,strong) UILabel *peopleNumberL;
@property(nonatomic,strong) UILabel *requreL;
@property(nonatomic,strong) UIButton *orderButton;
@property(nonatomic,strong) UITextField *priceField;
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) UIView *contentbgView;

@end

@implementation LKChatOffer_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(LKChatModel *)model{
    _model = model;
    self.targetAddressL.text = model.address;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.travelTime doubleValue]/1000;
    NSString *startStr = [NSString stringWithFormat:@"%ld月%ld日 %02ld:%02ld",date.month,date.day,date.hour,date.minute];
    date.timeInterval = [model.endTime doubleValue]/1000;
    NSString *stopStr = [NSString stringWithFormat:@"%ld月%ld日 %02ld:%02ld",date.month,date.day,date.hour,date.minute];;
    self.travelTimeL.text = [NSString stringWithFormat:@"%@ - %@",startStr,stopStr];
    self.peopleNumberL.text = [NSString stringWithFormat:@"%ld", [model.number integerValue] ];
    self.requreL.text = model.requirementDescription;
    [self.targetAddressL updateLayout];
    [self.travelTimeL updateLayout];
    [self.peopleNumberL updateLayout];
    [self.requreL updateLayout];
    self.btn.isEnabled = YES;
//    if([model isExceedTheTimeLimitInSenconds:30]){
//        self.btn.isEnabled = NO;
//    }else{
//        self.btn.isEnabled = YES;
//    }
}

- (void)btnIsClicked:(id)sender
{
    if(self.btn.isEnabled == NO) return;
    if(self.OfferSelectBlock){
        self.OfferSelectBlock(self.model);
    }
}

- (void)arrowIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.navigationBlock){
        self.navigationBlock(self.model);
    }
}

- (void)configViews{
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.contentView.backgroundColor = [UIColor colorWithHex:0xf3f3f3];
    UIView *view = [UIView new];
    view.backgroundColor  = [UIColor whiteColor];
    UILabel *labelL2 = [Utils createLabelWithTitle:@"始发地"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR2 = [Utils createLabelWithTitle:@"利君未来城二期"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    labelR2.textAlignment = NSTextAlignmentRight;
    self.targetAddressL = labelR2;
    UIImageView *arrow = ImageViewWithImage(@"daohang");
    [arrow addTapGestureTarget:self action:@selector(arrowIsClicked:)];
    
    UILabel *labelL3 = [Utils createLabelWithTitle:@"出行时间"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR3 = [Utils createLabelWithTitle:@"7月3日周五9：00-7月5日周日15:00"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.travelTimeL = labelR3;
    
    UILabel *labelL4 = [Utils createLabelWithTitle:@"乘车人数"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR4 = [Utils createLabelWithTitle:@"4"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.peopleNumberL = labelR4;
    
    UIView *contentbgview = [UIView new];
    contentbgview.backgroundColor = HexColor(0xf8f9fb);
    UILabel *contentL = [Utils createLabelWithTitle:@"想去西安，游玩两天，包车出行。想去西安，游玩两天，包车出行。"
                                         titleColor:HexColor(0x444444)
                                           fontSize:14];
    contentL.numberOfLines = 0;
    [contentbgview addSubview:contentL];
    self.requreL = contentL;
    self.contentbgView = contentbgview;
    
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"立即报价" font:LKSystemFont(16)
                                    cornerRadius:16];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    self.btn = btn;
    
    [view sd_addSubviews:@[
                             labelL2,labelR2,arrow,
                             labelL3,labelR3,
                             labelL4,labelR4,
                             contentbgview,
                             btn
                             ]];
    [self.contentView addSubview:view];
    
    labelL2.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(view, 18)
    .widthIs(14)
    .heightIs(14);
    
    labelR2.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(arrow, 4)
    .leftSpaceToView(labelL2, 8)
    .heightIs(18);
    
    labelL3.sd_layout
    .leftEqualToView(labelL2)
    .topSpaceToView(labelR2, 15)
    .autoHeightRatio(0);
    [labelL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR3.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL3)
    .autoHeightRatio(0);
    [labelR3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL4.sd_layout
    .leftEqualToView(labelL3)
    .topSpaceToView(labelL3, 15)
    .autoHeightRatio(0);
    [labelL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR4.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL4)
    .autoHeightRatio(0);
    [labelR4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(contentbgview, 12)
    .rightSpaceToView(contentbgview, 10)
    .leftSpaceToView(contentbgview, 10)
    .autoHeightRatio(0);
    
    contentbgview.sd_layout
    .topSpaceToView(labelL4, 11)
    .leftEqualToView(labelL4)
    .rightEqualToView(labelR4);
    [contentbgview setupAutoHeightWithBottomViewsArray:@[contentL] bottomMargin:12];
    
    btn.sd_layout
    .topSpaceToView(contentbgview, 20)
    .centerXEqualToView(view)
    .widthIs(180)
    .heightIs(32);
    
    view.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15);
    [view setupAutoHeightWithBottomView:btn bottomMargin:20];
    
    [self setupAutoHeightWithBottomView:view bottomMargin:0];
}

@end
