//
//  LKChatOfferIsDone_ClientCell.m
//  TravelNew
//
//  Created by mac on 2020/10/31.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKChatOfferIsDone_ClientCell.h"

@interface LKChatOfferIsDone_ClientCell()
@property(nonatomic,strong) UILabel *orderNumberL;
@property(nonatomic,strong) UILabel *orderDate;

@property(nonatomic,strong) UILabel *targetTipL;
@property(nonatomic,strong) UILabel *targetAddressL;
@property(nonatomic,strong) UILabel *travelTimeL;
@property(nonatomic,strong) UILabel *peopleNumberL;
@property(nonatomic,strong) UILabel *requreL;
@property(nonatomic,strong) UIButton *btn;
@property(nonatomic,strong) UIView *contentbgView
;@property(nonatomic,strong) UILabel *priceLabel;
@property(nonatomic,strong) UILabel *offerLabel;
@end

@implementation LKChatOfferIsDone_ClientCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(LKChatModel *)model{
    _model = model;
    self.targetAddressL.text = model.address;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.travelTime doubleValue]/1000;
    NSString *startTime = [NSString stringWithFormat:@"%ld月%ld日%@ %02ld:%02ld",date.month,date.day,date.weekday,date.hour,date.minute];
    date.timeInterval = [model.endTime doubleValue]/1000;
    NSString *stopTime = [NSString stringWithFormat:@"%ld月%ld日%@ %02ld:%02ld",date.month,date.day,date.weekday,date.hour,date.minute];
    self.travelTimeL.text = [NSString stringWithFormat:@"%@ - %@",startTime,stopTime];
    self.peopleNumberL.text = [NSString stringWithFormat:@"%ld", [model.number integerValue] ];
    self.requreL.text = model.requirementDescription;
    CGFloat prc = [model.offerPrice floatValue]/100.0;
    NSString *price = [NSString stringWithFormat:@"%.2lf",prc];
    self.priceLabel.text =[NSString stringWithFormat:@"此次行程共计 %@元",price];
    [self.priceLabel changStrings:@[price] toSize:18 toColor:HexColor(0xFFA656)];
    self.offerLabel.text = model.requirementDescription;
    [self.priceLabel updateLayout];
    [self.offerLabel updateLayout];
    [self.targetAddressL updateLayout];
    [self.travelTimeL updateLayout];
    [self.peopleNumberL updateLayout];
    [self.requreL updateLayout];
//    if([_model isExceedTheTimeLimitInSenconds:30]){
//        [self.btn setEnabled:NO];
//    }else{
//        [self.btn setEnabled:YES];
//    }
}

- (void)btnPayIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [btn setEnabled:NO];
    if(self.payBlock){
        self.payBlock(self.model);
    }
}

- (void)arrowIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.navigationBlock){
        self.navigationBlock(self.model);
    }
}

- (void)configViews{
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.contentView.backgroundColor = [UIColor colorWithHex:0xf3f3f3];
    UIView *view = [UIView new];
    view.backgroundColor  = [UIColor whiteColor];
    UILabel *labelL2 = [Utils createLabelWithTitle:@"始发地"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR2 = [Utils createLabelWithTitle:@"利君未来城二期"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.targetAddressL = labelR2;
    UIImageView *arrow = ImageViewWithImage(@"daohang");
    [arrow addTapGestureTarget:self action:@selector(arrowIsClicked:)];
    
    UILabel *labelL3 = [Utils createLabelWithTitle:@"出行时间"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR3 = [Utils createLabelWithTitle:@"7月3日周五9：00-7月5日周日15:00"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.travelTimeL = labelR3;
    
    UILabel *labelL4 = [Utils createLabelWithTitle:@"乘车人数"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR4 = [Utils createLabelWithTitle:@"4"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.peopleNumberL = labelR4;
    
    UIView *contentbgview = [UIView new];
    contentbgview.backgroundColor = HexColor(0xf8f9fb);
    UILabel *contentL = [Utils createLabelWithTitle:@"想去西安，游玩两天，包车出行。想去西安，游玩两天，包车出行。"
                                         titleColor:HexColor(0x444444)
                                           fontSize:14];
    contentL.numberOfLines = 0;
    [contentbgview addSubview:contentL];
    self.requreL = contentL;
    self.contentbgView = contentbgview;
    
    UIImageView *imgv = ImageViewWithImage(@"bg_Fill2");
    [imgv setUserInteractionEnabled:YES];
    UILabel *priceLabel = [Utils createLabelWithTitle:@"此次行程共计 400元" titleColor:[UIColor whiteColor] fontSize:14];
    self.priceLabel = priceLabel;
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"确认并支付" forState:UIControlStateNormal];
    [btn setTitleColor:HexColor(0x4D8FFD) forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [btn setBackgroundColor:[UIColor whiteColor]];
    btn.layer.cornerRadius = 16;
    btn.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.4].CGColor;
    btn.layer.shadowOffset = CGSizeMake(0,2);
    btn.layer.shadowOpacity = 1;
    btn.layer.shadowRadius = 5;
    self.btn = btn;
    [btn addTarget:self action:@selector(btnPayIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIView *bgview = [UIView new];
    bgview.backgroundColor = HexColor(0xE3EDFF);
    bgview.layer.cornerRadius = 4;
    UILabel *offerLabel = [Utils createLabelWithTitle:@"西安自由行，用车一天，兵马俑，华清池" titleColor:HexColor(0x333333) fontSize:14];
    offerLabel.backgroundColor = HexColor(0xE3EDFF);
    offerLabel.numberOfLines = 0;
    self.offerLabel = offerLabel;
    
    [bgview addSubview:offerLabel];
    [imgv sd_addSubviews:@[priceLabel,
                           btn,
                           bgview]];
    
    [view sd_addSubviews:@[
                             labelL2,labelR2,arrow,
                             labelL3,labelR3,
                             labelL4,labelR4,
                             contentbgview,
                             imgv
                             ]];
    [self.contentView addSubview:view];
    
    labelL2.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(view, 18)
    .widthIs(14)
    .heightIs(14);
    
    labelR2.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(arrow, 4)
    .autoHeightRatio(0);
    [labelR2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL3.sd_layout
    .leftEqualToView(labelL2)
    .topSpaceToView(labelR2, 15)
    .autoHeightRatio(0);
    [labelL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR3.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL3)
    .autoHeightRatio(0);
    [labelR3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL4.sd_layout
    .leftEqualToView(labelL3)
    .topSpaceToView(labelL3, 15)
    .autoHeightRatio(0);
    [labelL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR4.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL4)
    .autoHeightRatio(0);
    [labelR4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(contentbgview, 12)
    .rightSpaceToView(contentbgview, 10)
    .leftSpaceToView(contentbgview, 10)
    .autoHeightRatio(0);
    
    contentbgview.sd_layout
    .topSpaceToView(labelL4, 11)
    .leftEqualToView(labelL4)
    .rightEqualToView(labelR4);
    [contentbgview setupAutoHeightWithBottomViewsArray:@[contentL] bottomMargin:12];
    
    priceLabel.sd_layout
    .topSpaceToView(imgv, 25)
    .leftSpaceToView(imgv, 15)
    .rightSpaceToView(imgv, 15)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(priceLabel, 14)
    .centerXEqualToView(imgv)
    .widthIs(180)
    .heightIs(32);
    
    offerLabel.sd_layout
    .topSpaceToView(bgview, 12)
    .leftSpaceToView(bgview, 10)
    .rightSpaceToView(bgview, 10)
    .autoHeightRatio(0);
    
    bgview.sd_layout
    .topSpaceToView(btn, 20)
    .leftSpaceToView(imgv, 16)
    .rightSpaceToView(imgv, 16);
    [bgview setupAutoHeightWithBottomView:offerLabel bottomMargin:12];
    
    imgv.sd_layout
    .topSpaceToView(contentbgview, 20)
    .leftEqualToView(view)
    .rightEqualToView(view);
    [imgv setupAutoHeightWithBottomView:bgview bottomMargin:20];
    
    view.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15);
    [view setupAutoHeightWithBottomView:imgv bottomMargin:0];
    [self setupAutoHeightWithBottomView:view bottomMargin:0];
}

@end
