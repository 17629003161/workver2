//
//  LKRequestCell.m
//  TravelNew
//
//  Created by mac on 2021/2/3.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKRequestCell.h"

@implementation LKRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)btnCancelIsClicked:(id)sender{
    if(self.onSelect){
        self.onSelect(NO);
    }
}

- (void)btnOkIsClicked:(id)sender
{
    if(self.onSelect){
        self.onSelect(YES);
    }
}

- (void)configViews{
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.contentView.backgroundColor = [UIColor colorWithHex:0xf3f3f3];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 95)];
    view.backgroundColor  = [UIColor whiteColor];
    UILabel *tipLabel = [Utils createLabelWithTitle:@"对方发起报价请求，您是否同意"
                                        titleColor:HexColor(0x323232)
                                          fontSize:16];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xEDEDED);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xEDEDED);
    
    UIButton *btnCancle = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancle setTitle:@"拒绝" forState:UIControlStateNormal];
    [btnCancle setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
    btnCancle.titleLabel.font = LKSystemFont(15);
    [btnCancle addTapGestureTarget:self action:@selector(btnCancelIsClicked:)];
    
    UIButton *btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOk setTitle:@"同意" forState:UIControlStateNormal];
    [btnOk setTitleColor:HexColor(0x0076FF) forState:UIControlStateNormal];
    btnOk.titleLabel.font = LKSystemFont(15);
    [btnOk addTapGestureTarget:self action:@selector(btnOkIsClicked:)];
    [view sd_addSubviews:@[ tipLabel,
                            line,
                            line2,
                            btnCancle,
                            btnOk
                             ]];
    [self.contentView addSubview:view];
    
    tipLabel.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 15)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .topSpaceToView(tipLabel, 15)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    line2.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(line, 0)
    .heightIs(40)
    .widthIs(0.5);
    
    btnCancle.sd_layout
    .topSpaceToView(line, 8)
    .rightSpaceToView(line2, 49)
    .widthIs(36)
    .heightIs(24);
    
    btnOk.sd_layout
    .topSpaceToView(line, 8)
    .leftSpaceToView(line2, 49)
    .widthIs(36)
    .heightIs(24);
    [self setupAutoHeightWithBottomView:view bottomMargin:0];

}

@end
