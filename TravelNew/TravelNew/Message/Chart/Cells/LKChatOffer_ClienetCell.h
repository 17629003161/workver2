//
//  LKChatOfferCell.h
//  TravelNew
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "LKChatModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKChatOffer_ClienetCell : BaseCustomTableViewCell
@property(nonatomic,strong) LKChatModel *model;
@property(nonatomic,copy) void(^navigationBlock)(LKChatModel *);

@end

NS_ASSUME_NONNULL_END
