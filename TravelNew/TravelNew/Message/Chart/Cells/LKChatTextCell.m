//
//  LKChatTextCell.m
//  TravelNew
//
//  Created by mac on 2021/2/3.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKChatTextCell.h"
@interface LKChatTextCell()
@property(nonatomic,strong) UILabel *tipLabel;
@end

@implementation LKChatTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configViews{
    UILabel *label = [Utils createLabelWithTitle:@"title" titleColor:HexColor(0x666666) fontSize:15];
    self.tipLabel = label;
    [self.contentView addSubview:label];
    label.sd_layout
    .leftSpaceToView(self.contentView, 25)
    .centerYEqualToView(self.contentView)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

-(void)setTitle:(NSString *)title
{
    _title = title;
    self.tipLabel.text = title;
    [self.tipLabel updateLayout];
}

@end
