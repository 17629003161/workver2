//
//  LKRequestCell.h
//  TravelNew
//
//  Created by mac on 2021/2/3.
//  Copyright © 2021 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKRequestCell : BaseCustomTableViewCell
@property(nonatomic,strong) void (^onSelect)(BOOL isAllow);
@end

NS_ASSUME_NONNULL_END
