//
//  LKOrderPayCtrl.h
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "LKChatModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKOrderPayCtrl : LKBaseViewController

@property(nonatomic,copy) void (^finishPayBlock)(LKChatModel *);
@property(nonatomic,strong) LKChatModel *model;

@end

NS_ASSUME_NONNULL_END
