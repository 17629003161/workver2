//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface OrderPayEntity : NSObject<NSCoding>

@property (nonatomic,assign) BOOL paySuccess;
@property (nonatomic,copy) NSString *body;
 


-(id)initWithJson:(NSDictionary *)json;

@end
