//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "OrderPayEntity.h"

@implementation OrderPayEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.paySuccess = [[json objectForKey:@"paySuccess"]boolValue];
		self.body  = [json objectForKey:@"body"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeBool:self.paySuccess forKey:@"zx_paySuccess"];
	[aCoder encodeObject:self.body forKey:@"zx_body"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.paySuccess = [aDecoder decodeBoolForKey:@"zx_paySuccess"];
	self.body = [aDecoder decodeObjectForKey:@"zx_body"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"paySuccess : %@\n",self.paySuccess?@"yes":@"no"];
	result = [result stringByAppendingFormat:@"body : %@\n",self.body];
	
    return result;
}

@end
