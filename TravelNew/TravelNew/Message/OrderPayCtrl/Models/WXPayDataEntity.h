//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface WXPayDataEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *partnerid;
@property (nonatomic,copy) NSString *paySign;
@property (nonatomic,copy) NSString *wx_package;
@property (nonatomic,copy) NSString *noncestr;
@property (nonatomic,copy) NSString *timestamp;
@property (nonatomic,copy) NSString *appid;
@property (nonatomic,copy) NSString *prepayid;
 


-(id)initWithJson:(NSDictionary *)json;

@end
