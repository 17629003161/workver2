//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "WXPayDataEntity.h"

@implementation WXPayDataEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.partnerid  = [json objectForKey:@"partnerid"];
		self.paySign  = [json objectForKey:@"paySign"];
		self.wx_package  = [json objectForKey:@"wx_package"];
		self.noncestr  = [json objectForKey:@"noncestr"];
		self.timestamp  = [json objectForKey:@"timestamp"];
		self.appid  = [json objectForKey:@"appid"];
		self.prepayid  = [json objectForKey:@"prepayid"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.partnerid forKey:@"zx_partnerid"];
	[aCoder encodeObject:self.paySign forKey:@"zx_paySign"];
	[aCoder encodeObject:self.wx_package forKey:@"zx_wx_package"];
	[aCoder encodeObject:self.noncestr forKey:@"zx_noncestr"];
	[aCoder encodeObject:self.timestamp forKey:@"zx_timestamp"];
	[aCoder encodeObject:self.appid forKey:@"zx_appid"];
	[aCoder encodeObject:self.prepayid forKey:@"zx_prepayid"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.partnerid = [aDecoder decodeObjectForKey:@"zx_partnerid"];
		self.paySign = [aDecoder decodeObjectForKey:@"zx_paySign"];
		self.wx_package = [aDecoder decodeObjectForKey:@"zx_wx_package"];
		self.noncestr = [aDecoder decodeObjectForKey:@"zx_noncestr"];
		self.timestamp = [aDecoder decodeObjectForKey:@"zx_timestamp"];
		self.appid = [aDecoder decodeObjectForKey:@"zx_appid"];
		self.prepayid = [aDecoder decodeObjectForKey:@"zx_prepayid"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"partnerid : %@\n",self.partnerid];
	result = [result stringByAppendingFormat:@"paySign : %@\n",self.paySign];
	result = [result stringByAppendingFormat:@"wx_package : %@\n",self.wx_package];
	result = [result stringByAppendingFormat:@"noncestr : %@\n",self.noncestr];
	result = [result stringByAppendingFormat:@"timestamp : %@\n",self.timestamp];
	result = [result stringByAppendingFormat:@"appid : %@\n",self.appid];
	result = [result stringByAppendingFormat:@"prepayid : %@\n",self.prepayid];
	
    return result;
}

@end
