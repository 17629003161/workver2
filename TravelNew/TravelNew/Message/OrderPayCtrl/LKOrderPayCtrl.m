//
//  LKOrderPayCtrl.m
//  TravelNew
//
//  Created by mac on 2020/11/2.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKOrderPayCtrl.h"
#import "PaySucessCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import <WXApi.h>
#import "OrderPayEntity.h"
#import "WXPayDataEntity.h"
#import "WechatManager.h"

@interface LKOrderPayCtrl ()
@property(nonatomic,strong) UILabel *addrLabel;
@property(nonatomic,strong) UILabel *outTimeLabel;
@property(nonatomic,strong) UILabel *customerNumberLabel;
@property(nonatomic,strong) UILabel *demandLabel;
@property(nonatomic,strong) UILabel *priceLabel;
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) NSMutableArray *selectBtnArray;

@end

@implementation LKOrderPayCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"订单支付" titleColor:nil];
    [self layout];
    [self setSelectIndex:0];
    CGFloat prc = [self.model.offerPrice floatValue]/100.0;
    self.priceLabel.text = [NSString stringWithFormat:@"%.2lf元",prc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(payIsFinished:)
                                                 name:Notification_PayIsFinished
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Notification_PayIsFinished
                                                  object:nil];
}

- (void)payIsFinished:(NSNotification *)notification
{
    if(self.finishPayBlock){
        self.finishPayBlock(self.model);
    }
    [self.navigationController popViewControllerAnimated:YES];


}


- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    for (UIButton *btn in self.selectBtnArray) {
        if(btn.tag==_selectIndex){
            [btn setSelected:YES];
        }else{
            [btn setSelected:NO];
        }
    }
}


-(NSMutableArray *)selectBtnArray
{
    if(!_selectBtnArray){
        _selectBtnArray = [NSMutableArray new];
    }
    return _selectBtnArray;
}




- (void)layout{
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = HexColor(0xF3F3F3);
    self.scrollView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    
    UILabel *label = [Utils createLabelWithTitle:@"出行订单" titleColor:HexColor(0x333333) fontSize:14];
    UIView *topView = [self makeTopView];
    UIView *bottomView = [self makeBottomView];
    NSString *title = [NSString stringWithFormat:@"支付宝支付￥%.2lf",[self.model.offerPrice floatValue]/100.0];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:title font:LKSystemFont(17)
                                        cornerRadius:22.5];
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    [self.scrollView sd_addSubviews:@[label,
                                      topView,
                                      bottomView,
                                      btn]];
    label.sd_layout
    .topSpaceToView(self.scrollView, 15)
    .leftSpaceToView(self.scrollView, 15)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    topView.sd_layout
    .topSpaceToView(label, 10)
    .leftEqualToView(label)
    .rightSpaceToView(self.scrollView, 15);
    
    bottomView.sd_layout
    .topSpaceToView(topView, 20)
    .leftEqualToView(self.scrollView)
    .rightEqualToView(self.scrollView);
    
    btn.sd_layout
    .topSpaceToView(bottomView, 52)
    .centerXEqualToView(self.scrollView)
    .widthIs(kScreenWidth-56)
    .heightIs(45);
    
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, CGRectGetMaxY(btn.frame)+30);

}


- (void)btnPayIsClicked:(id)sender
{
    if(self.selectIndex==0){
        //支付宝支付
        DLog(@"支付宝支付");
        [self doAliPayWithOrderNumber:self.model.offerPriceId];
    }else{
        //微信支付
        DLog(@"微信支付");
        [self doWXPayWithOrderNumber:self.model.offerPriceId];
        
    }
}



#pragma mark - 支付订单
- (void)doWXPayWithOrderNumber:(NSNumber *)orderNumber{
    Api *api = [Api apiPostUrl:@"work/wxpay/payRequirement"
                          para:@{@"billId":self.model.offerPriceId
                                 }
                   description:@"获取微信OrderInfo"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
            WXPayDataEntity *model = [[WXPayDataEntity alloc] initWithJson:dict];
            PayReq *req  = [[PayReq alloc] init];
             req.partnerId = model.partnerid;
             req.prepayId = model.prepayid;
             req.package = model.wx_package;
             req.nonceStr = model.noncestr;
             req.timeStamp = (UInt32)[model.timestamp longLongValue];
             req.sign = model.paySign;
            //调起微信支付
            [WechatManager hangleWechatPayWith:req];
        }else{
           [MBProgressHUD showMessage:@"请求支付数据出错"];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}
#pragma mark - 支付订单
- (void)doAliPayWithOrderNumber:(NSNumber *)orderNumber{
    Api *api = [Api apiPostUrl:@"work/alipay/payRequirement"
                          para:@{@"billId":self.model.offerPriceId
                                 }
                   description:@"获取支付宝OrderInfo"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
            OrderPayEntity *model = [[OrderPayEntity alloc] initWithJson:dict];
            if(model.paySuccess){
                [MBProgressHUD showMessage:@"您已支付过此订单"];
                return;
            }
            DLog(@"orderStr ---- = %@",model.body);
            // NOTE: 调用支付结果开始支付
            NSString *appScheme = @"alisdkdemo";
            [[AlipaySDK defaultService] payOrder:model.body fromScheme:appScheme callback:^(NSDictionary *resultDic) {
                DLog(@"reslut = %@",resultDic);
                if ([resultDic[@"resultStatus"]intValue] == 9000) {
                    DLog(@"成功");
                } else {
                    DLog(@"失败");
                }
            }];

        }else{
           [MBProgressHUD showMessage:@"请求支付数据出错"];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

    #pragma mark 微信支付方法

- (void)WXPayWithAppid:(NSString *)appid partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid package:(NSString *)package noncestr:(NSString *)noncestr timestamp:(NSString *)timestamp sign:(NSString *)sign{
    
    //需要创建这个支付对象
    PayReq *req   = [[PayReq alloc] init];
    //由用户微信号和AppID组成的唯一标识，用于校验微信用户
    req.openID = appid;
    // 商家id，在注册的时候给的
    req.partnerId = partnerid;
    // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
    req.prepayId  = prepayid;
    // 根据财付通文档填写的数据和签名
    req.package  = package;
    // 随机编码，为了防止重复的，在后台生成
    req.nonceStr  = noncestr;
    // 这个是时间戳，也是在后台生成的，为了验证支付的
    NSString * stamp = timestamp;
    req.timeStamp = stamp.intValue;
    // 这个签名也是后台做的
    req.sign = sign;
    [WXApi sendReq:req completion:^(BOOL success) {
        DLog(@"吊起微信成功...");
    }];
}

#pragma mark -支付完成后在后台查询后台支付结果，
- (void)checkOrderPay:(NSNumber *)orderNumber{
    Api *api = [Api apiPostUrl:@"work/alipay/selectIsPaySuccess"
                          para:@{@"out_trade_no":orderNumber
                                 }
                description:@"查询支付宝交易是否成功"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *json = request.responseString;
        DLog(@"json = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"code"] integerValue] == 200){
            if(self.finishPayBlock){
                self.finishPayBlock(self.model);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError: [dict objectForKey:@"message"]];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}



- (UIView *)makeTopView{
    UIView *view = [UIView new];
    view.layer.borderWidth = 0.5;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 4.0;
    
    UILabel *labelL2 = [Utils createLabelWithTitle:@"始发地"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR2 = [Utils createLabelWithTitle:@"利君未来城二期"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.addrLabel = labelR2;
    
    UILabel *labelL3 = [Utils createLabelWithTitle:@"出行时间"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR3 = [Utils createLabelWithTitle:@"7月3日周五9：00-7月5日周日15:00"
                                        titleColor:HexColor(0x181818)
                                          fontSize:13];
    self.outTimeLabel = labelR3;
    
    UILabel *labelL4 = [Utils createLabelWithTitle:@"乘车人数"
                                        titleColor:HexColor(0x818181)
                                          fontSize:13];
    UILabel *labelR4 = [Utils createLabelWithTitle:@"4"
                                        titleColor:HexColor(0x333333)
                                          fontSize:14];
    self.customerNumberLabel = labelR4;
    
    UIView *contentbgview = [UIView new];
    contentbgview.backgroundColor = HexColor(0xf8f9fb);
    UILabel *contentL = [Utils createLabelWithTitle:@"想去西安，游玩两天，包车出行。想去西安，游玩两天，包车出行。"
                                         titleColor:HexColor(0x444444)
                                           fontSize:14];
    contentL.numberOfLines = 0;
    self.demandLabel = contentL;
    [contentbgview addSubview:contentL];
    
    UIImageView *bgimgv2 = ImageViewWithImage(@"bg_Fill2");
    UILabel *tipLabel = [Utils createLabelWithTitle:@"订单金额" titleColor:[UIColor whiteColor] fontSize:13];
    UILabel *priceLabel = [Utils createLabelWithTitle:@"400元" titleColor:HexColor(0xFFA656) fontSize:20];
    self.priceLabel = priceLabel;
    [bgimgv2 sd_addSubviews:@[tipLabel,priceLabel]];
    
    [view sd_addSubviews:@[
                             labelL2,labelR2,
                             labelL3,labelR3,
                             labelL4,labelR4,
                             contentbgview,
                             bgimgv2]];
    
    labelL2.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelL2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR2.sd_layout
    .centerYEqualToView(labelL2)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [labelR2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL3.sd_layout
    .leftEqualToView(labelL2)
    .topSpaceToView(labelR2, 15)
    .autoHeightRatio(0);
    [labelL3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR3.sd_layout
    .rightEqualToView(labelR2)
    .centerYEqualToView(labelL3)
    .autoHeightRatio(0);
    [labelR3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelL4.sd_layout
    .leftEqualToView(labelL3)
    .topSpaceToView(labelL3, 15)
    .autoHeightRatio(0);
    [labelL4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    labelR4.sd_layout
    .rightSpaceToView(view, 16)
    .centerYEqualToView(labelL4)
    .autoHeightRatio(0);
    [labelR4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    contentL.sd_layout
    .topSpaceToView(contentbgview, 12)
    .rightSpaceToView(contentbgview, 10)
    .leftSpaceToView(contentbgview, 10)
    .autoHeightRatio(0);
    
    contentbgview.sd_layout
    .topSpaceToView(labelL4, 11)
    .leftEqualToView(labelL4)
    .rightEqualToView(labelR4);
    [contentbgview setupAutoHeightWithBottomViewsArray:@[contentL] bottomMargin:12];
    
    tipLabel.sd_layout
    .centerYEqualToView(bgimgv2)
    .leftSpaceToView(bgimgv2, 16)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    priceLabel.sd_layout
    .centerYEqualToView(bgimgv2)
    .rightSpaceToView(bgimgv2, 16)
    .autoHeightRatio(0);
    [priceLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgimgv2.sd_layout
    .topSpaceToView(contentbgview, 20)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(50);
    
    [view setupAutoHeightWithBottomViewsArray:@[bgimgv2] bottomMargin:0];
    return view;
}

- (void)selectBtnIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self setSelectIndex:btn.tag];

}

- (UIView *)makeBottomView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *tipLabel = [Utils createBoldLabelWithTitle:@"选择支付方式" titleColor:HexColor(0x333333) fontSize:16];
    UIImageView *imgv1 = ImageViewWithImage(@"zfblogo");
    UILabel *label1 = [Utils createLabelWithTitle:@"支付宝" titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subLabel1 = [Utils createLabelWithTitle:@"10亿人都在用，真安全，更方便" titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.tag = 0;
    [btn1 setImage:[UIImage imageNamed:@"CombinedShapeCopy3"] forState:UIControlStateNormal];
    [btn1 setImage:[UIImage imageNamed:@"CombinedShape"] forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(selectBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectBtnArray addObject:btn1];
    
    UIImageView *imgv2 = ImageViewWithImage(@"weixinzhif");
    UILabel *label2 = [Utils createLabelWithTitle:@"微信支付" titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subLabel2 = [Utils createLabelWithTitle:@"亿万用户的选择，更快更安全" titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 addTarget:self action:@selector(selectBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = 1;
    [btn2 setImage:[UIImage imageNamed:@"CombinedShapeCopy3"] forState:UIControlStateNormal];
    [btn2 setImage:[UIImage imageNamed:@"CombinedShape"] forState:UIControlStateSelected];
    [self.selectBtnArray addObject:btn2];
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xE7E7E7);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xE7E7E7);
    [view sd_addSubviews:@[tipLabel,
                           line1,
                           imgv1,label1,subLabel1,btn1,
                           line2,
                           imgv2,label2,subLabel2,btn2]];
    tipLabel.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [tipLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line1.sd_layout
    .topSpaceToView(tipLabel, 15)
    .leftEqualToView(tipLabel)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv1.sd_layout
    .topSpaceToView(line1, 19)
    .leftEqualToView(line1)
    .widthIs(40)
    .heightIs(40);
    
    label1.sd_layout
    .topEqualToView(imgv1)
    .leftSpaceToView(imgv1, 15)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subLabel1.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 2)
    .autoHeightRatio(0);
    [subLabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn1.sd_layout
    .centerYEqualToView(imgv1)
    .rightSpaceToView(view, 20)
    .widthIs(30)
    .heightIs(30);
    
    line2.sd_layout
    .topSpaceToView(imgv1, 19)
    .leftEqualToView(imgv1)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv2.sd_layout
    .topSpaceToView(line2, 19)
    .leftEqualToView(line2)
    .widthIs(40)
    .heightIs(40);
    
    label2.sd_layout
    .topEqualToView(imgv2)
    .leftSpaceToView(imgv2, 15)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subLabel2.sd_layout
    .leftEqualToView(label2)
    .topSpaceToView(label2, 2)
    .autoHeightRatio(0);
    [subLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn2.sd_layout
    .centerYEqualToView(imgv2)
    .rightSpaceToView(view, 20)
    .widthIs(30)
    .heightIs(30);
    [view setupAutoHeightWithBottomViewsArray:@[imgv2] bottomMargin:25];
    return view;
}


@end
