//
//  WXLoginShare.h
//  Order
//
//  Created by 林海 on 2019/1/12.
//  Copyright © 2019年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

NS_ASSUME_NONNULL_BEGIN

@protocol WXLoginDelegate <NSObject>
/**
 登录成功微信后台返回的数据
 */
- (void)wxLoginShareSuccess:(NSDictionary *)dict;
//微信绑定成功回调
- (void)wxBundingSucess:(NSDictionary *)dict;
/**
 d登录失败微信后台返回的数据
 */
- (void)wxLoginShareFail:(NSDictionary *)dict;

@end



@interface WXLoginShare : NSObject

@property(nonatomic, strong) NSString *KWeiXinRefreshToken;
@property(nonatomic, weak) id<WXLoginDelegate> delegate;

+ (BOOL)handleOpenUrl:(NSURL *)url;

//创建单例
+(WXLoginShare *)shareInstance;
//注册ID
-(void)wxLoginShareRegisterApp;
//微信登录
-(void)WXLogin;
-(void)WXBunding;

NS_ASSUME_NONNULL_END

@end
