//
//  WechatManger.h
//  TravelNew
//
//  Created by mac on 2020/11/11.
//  Copyright © 2020 lester. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXApi.h>
NS_ASSUME_NONNULL_BEGIN

@interface WechatManager : NSObject
+ (id)shareInstance;

+ (BOOL)handleOpenUrl:(NSURL *)url;

+ (void)hangleWechatPayWith:(PayReq *)req;
@end

NS_ASSUME_NONNULL_END
