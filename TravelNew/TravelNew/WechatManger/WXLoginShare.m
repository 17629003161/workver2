//
//  WXLoginShare.m
//  Order
//
//  Created by 林海 on 2019/1/12.
//  Copyright © 2019年 Mac. All rights reserved.
//
//13572273796
//appleId: 1@sxlkcx.com 开发者网站密码 SX_lkcx_123.
//邓氏码：554545143
//13572021231
/*
 旅咖出行出行根据旅客的游玩地理位置为其匹配服务提供者，游客发布旅行需求（需求分为：线上咨询/旅行规划服务、旅行用车服务、旅行住宿服务），服务者提供旅行服务。服务者通过服务游客来赚取金钱，游客在游玩时的拍照与一些游玩心得可以发布分享给其他游客参考。该软件主要解决游客旅行前的线路规划，行程准备、旅行中用车住宿等，为闲置人员提供一些赚钱机会等。
 
 */



#import "WXLoginShare.h"

@interface WXLoginShare()<WXApiDelegate>
@property(nonatomic, assign) BOOL isWXBunding;
@end


@implementation WXLoginShare

+(WXLoginShare *)shareInstance{
    static WXLoginShare *loginShare = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loginShare = [[WXLoginShare alloc]init];
        loginShare.isWXBunding = NO;
    });
    return loginShare;
}

// 注册ID
-(void)wxLoginShareRegisterApp{
    [WXApi registerApp:@"wx51db81742889d0a0" universalLink:@"https://sxlkcx.cn/travel/"];
}

+ (BOOL)handleOpenUrl:(NSURL *)url {
    return [WXApi handleOpenURL:url delegate:[WXLoginShare shareInstance]];
}

// 微信登录
-(void)WXLogin{
    self.isWXBunding = NO;
    [self wxAuthRequest];
}

- (void)WXBunding{
    self.isWXBunding = YES;
    [self wxAuthRequest];
}


-(void)wxAuthRequest{
    SendAuthReq *req = [[SendAuthReq alloc]init];
//    req.scope = @"snsapi_userinfo,snsapi_base";
//    req.state = @"ViewController";
    req.state = @"wx_oauth_authorization_state";//用于保持请求和回调的状态，授权请求或原样带回
    req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
   // req.openID = APP_ID_SHARE;
    
    [WXApi sendReq:req completion:^(BOOL success) {
        if(success){
            DLog(@"调用成功");
        }else{
            DLog(@"调用失败");
        }
    }];
}



-(void)onReq:(BaseReq *)req{
    DLog(@"微信的回调方法---");
}
-(void)onResp:(BaseResp *)resp{
    DLog(@"微信的回调方法222---");
    // 微信登录
    //**resp.errCode  0 允许授权登录  -2用户取消
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        if (resp.errCode == 0) {
            SendAuthResp *aresp = (SendAuthResp *)resp;
            if(self.isWXBunding){
                [self requestBundingApi:aresp.code];
            }else{
                [self requestLoginApi:aresp.code];
               // [self getAccessTokenWithCode:aresp.code];
                DLog(@"微信的回调方法333---%@",aresp);
            }
        }
    }
}

- (void)requestBundingApi:(NSString *)code{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"user/data/bindingWx" para:@{@"code":code} description:@"绑定微信"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([json[@"code"] integerValue]==200){
            if([weakSelf.delegate respondsToSelector:@selector(wxBundingSucess:)]){
                [weakSelf.delegate wxBundingSucess:json];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
    
}


- (void)requestLoginApi:(NSString *)code{
    Api *api = [Api apiPostUrl:@"user/data/wxData"
                          para:@{@"code":code}
                   description:@"微信登陆请求"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            if([self.delegate respondsToSelector:@selector(wxLoginShareSuccess:)]){
                [self.delegate wxLoginShareSuccess:data];
            }
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
        }];
}



// *  获取access_token
- (void)getAccessTokenWithCode:(NSString *)code
{
    NSString *urlString =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",APP_ID_SHARE,APP_SECRET_SHARE,code];
 //   DLog(@"urlString = %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    __weak __typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *dataStr = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                DLog(@"dict = %@",dict);
                /*
                 "access_token" = "yDYlmcI4Td1s1zrqjA6v_a0ptx7wEJaKfgr65OMbBecvO4iBYyLujsfHxu82DSGxYqssbMSyiZbHI3XYC2tAJg";  //
                 "expires_in" = 7200;
                 openid = o9TVB1Co3947nBNB51XfQLB7CLSg;
                 "refresh_token" = "-icY6qNcesIqrRY8CXhtDH9kiXrFxdf59GNusPfpk221X-pSCDFDnIKDnAMPGClwqSERrtuyj_dan4vkPBxTfg";
                 scope = "snsapi_userinfo";
                 unionid = o4E2Q1fAj84mf3ziOqrVzjWbxH7I;
                 */
                if ([dict objectForKey:@"errcode"])
                {// 如果这里有值，说明授权失败
                    // 授权失败(用户取消/拒绝)
                    if ([weakSelf.delegate respondsToSelector:@selector(wxLoginShareFail:)]) {
                        [weakSelf.delegate wxLoginShareFail:dict];
                    }
                }else{
                    [self getUserInfoWithAccessToken:[dict objectForKey:@"access_token"] andOpenId:[dict objectForKey:@"openid"]];
                }
            }
        });
    });
}

//*  获取用户信息
- (void)getUserInfoWithAccessToken:(NSString *)accessToken andOpenId:(NSString *)openId
{
    // 存入本地
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessToken forKey:WX_REFRESH_TOKEN];
    [defaults setObject:openId forKey:OPENID];
    [defaults synchronize];
    
    NSString *urlString =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",accessToken,openId];
 //   DLog(@"urlString = %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    __weak __typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *dataStr = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                DLog(@"dict = %@",dict);
                /**
                 *city = Foshan;
                 country = CN;
                 headimgurl = "http://wx.qlogo.cn/mmopen/vi_32/oXUy91CibcGpekG7sOB7O0icpxq7B3kv4oZyia3AA1mPPjH9qAbA8tG1Xx6WNE03vNwhyibhA7LKSBia0t5qc9dSeow/0";
                 language = "zh_CN";
                 nickname = "\U5929\U9053\U916c\U52e4";
                 openid = o9TVB1Co3947nBNB51XfQLB7CLSg;
                 privilege =     (
                 );
                 province = Guangdong;
                 sex = 1;                 unionid = o4E2Q1fAj84mf3ziOqrVzjWbxH7I;
                 *
                 */
                if ([dict objectForKey:@"errcode"])
                {
                    //AccessToken失效
                    [self getAccessTokenWithRefreshToken:[[NSUserDefaults standardUserDefaults]objectForKey:WX_REFRESH_TOKEN]];
                }else{
                    if ([weakSelf.delegate respondsToSelector:@selector(wxLoginShareSuccess:)]) {
                        [weakSelf.delegate wxLoginShareSuccess:dict];
                    }
                }
            }
        });
    });
}

// *  刷新access_token
- (void)getAccessTokenWithRefreshToken:(NSString *)refreshToken
{
    NSString *urlString =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%@&grant_type=refresh_token&refresh_token=%@",@"wx29c1153063de230b",refreshToken];
    DLog(@"urlString = %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        NSString *dataStr = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                if ([dict objectForKey:@"errcode"])
                {
                    //授权过期
                }else{
                    //重新使用AccessToken获取信息
                    
                    NSString *token = [[NSUserDefaults standardUserDefaults]objectForKey:WX_REFRESH_TOKEN];
                    NSString *openID = [[NSUserDefaults standardUserDefaults]objectForKey:OPENID];
                    
                    if([dict objectForKey:@"access_token"] && [dict objectForKey:@"access_token"]!=nil){
                        token = [dict objectForKey:@"access_token"];
                    }
                    if([dict objectForKey:@"openid"] && [dict objectForKey:@"openid"]!=nil){
                        openID = [dict objectForKey:@"openid"];
                    }
                    [self getUserInfoWithAccessToken:token andOpenId:openID];
                }
            }
        });
    });
    
}
@end
