//
//  ScenicSportDescriptionCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ScenicSportDescriptionCtrl.h"

@interface ScenicSportDescriptionCtrl ()


@end

@implementation ScenicSportDescriptionCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:self.entity.scenicName titleColor:nil];
    self.view.backgroundColor = HexColor(0xF3F3F3);
    
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = HexColor(0x666666);
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentJustified;
    label.alpha = 1.0;
    [self.view addSubview:label];

//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"大雁塔由玄奘法师亲自主持修建，虽经千年沧桑，仍然完好无损。是我国的佛教名塔之一，也是古城西安的独特标志，大雁塔是楼阁式砖塔，塔身用砖砌成呈方形锥体，内有楼梯盘旋而上。每层四面各有一个拱券门洞，凭栏远眺，长安风貌尽收眼底。\n\n塔底层南门两边出自大书法家褚遂良手书的《大唐三藏圣教序》碑和《大唐三藏圣教序记》碑，是唐高宗年间由玄奘亲手竖立于此的，至今保存完好。\n\n值得一提的是，唐代画家吴道子、王维等曾为慈恩寺作过不少壁画，可惜早已湮没在历史中。但大雁塔下四门洞的石门楣、门框上，却还保留着精美的唐代线刻画。"];
//    label.attributedText = string;
    label.text = self.entity.scenicAbstract;
    
    label.sd_layout
    .leftSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 25)
    .rightSpaceToView(self.view, 20)
    .autoHeightRatio(0);

    
}



@end
