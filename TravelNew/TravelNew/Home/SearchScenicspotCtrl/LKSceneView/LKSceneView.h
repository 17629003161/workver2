//
//  LKSceneView.h
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomView.h"
#import "FootprintSearchEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKSceneView : BaseCustomView

@property(nonatomic,strong) FootprintSearchEntity *model;
@end

NS_ASSUME_NONNULL_END
