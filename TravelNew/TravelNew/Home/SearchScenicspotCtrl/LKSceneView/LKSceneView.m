//
//  LKSceneView.m
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKSceneView.h"

@interface LKSceneView()
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIImageView *headerImageView;
@property(nonatomic,strong) UILabel *nameLabel;
@end

@implementation LKSceneView

-(void)setModel:(FootprintSearchEntity *)model
{
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]];
    self.nameLabel.text = model.content;
    [self.nameLabel updateLayout];
}

-(void)configViews{
    self.frame = CGRectMake(0, 0, 90, 122);
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4;
    self.layer.shadowColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:0.5].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 4;
    
    UIImageView *imgv = ImageViewWithImage(@"bg_recommender");
    self.imageView = imgv;
    UIImageView *imgv2 = ImageViewWithImage(@"header_image");
    self.headerImageView = imgv2;
    imgv2.layer.cornerRadius = 10;
    UILabel *label = [Utils createLabelWithTitle:@"蚂蚁之在非" titleColor:HexColor(0x414243) fontSize:10];
    self.nameLabel = label;
    [self sd_addSubviews:@[imgv,imgv2,label]];
    imgv.sd_layout
    .topEqualToView(self)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(90);
    imgv2.sd_layout
    .topSpaceToView(imgv, 6)
    .leftSpaceToView(self, 5)
    .widthIs(20)
    .heightIs(20);
    label.sd_layout
    .leftSpaceToView(imgv2, 5)
    .centerYEqualToView(imgv2)
    .rightSpaceToView(self, 5)
    .autoHeightRatio(0);
    
    [imgv makeTopAngleSize:CGSizeMake(4, 4) bounds:CGRectMake(0, 0, 90, 90)];
}

@end
