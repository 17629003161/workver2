//
//  LKScenicDetailView.m
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKScenicDetailView.h"

@interface LKScenicDetailView()
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *detailLabel;

@end

@implementation LKScenicDetailView

- (void)setModel:(ScenicSearchEntity *)model
{
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.scenicImage]];
    _nameLabel.text = model.scenicName;
    _detailLabel.text = model.scenicAbstract;
    [_nameLabel updateLayout];
    [_detailLabel updateLayout];
}

- (void)doNavigation:(UITapGestureRecognizer *)reg
{
    if(self.model){
        self.navigationBlock(self.model);
    }
}

-(void)configViews{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = YES;
    UIImageView *imgv = ImageViewWithImage(@"bg_recommender");
    self.imageView = imgv;
    UIImageView *imgv2 = ImageViewWithImage(@"daohang");
    [imgv2 addTapGestureTarget:self action:@selector(doNavigation:)];
    UILabel *label = [Utils createLabelWithTitle:@"大雁塔" titleColor:HexColor(0x323232) fontSize:16];
    self.nameLabel = label;
    UILabel *label2 = [Utils createLabelWithTitle:@"大雁塔大雁塔大雁塔大雁塔大雁塔大雁塔大雁塔大雁塔" titleColor:HexColor(0x999999) fontSize:14];
    self.detailLabel = label2;
    label2.numberOfLines = 0;
    [self sd_addSubviews:@[imgv,imgv2,label,label2]];
    imgv.sd_layout
    .topEqualToView(self)
    .leftEqualToView(self)
    .bottomEqualToView(self)
    .widthIs(90);
    label.sd_layout
    .leftSpaceToView(imgv, 20)
    .topSpaceToView(self, 14)
    .rightSpaceToView(self, 20)
    .autoHeightRatio(0);
    
    imgv2.sd_layout
    .centerYEqualToView(label)
    .rightSpaceToView(self, 21)
    .widthIs(16)
    .heightIs(16);
    
    label2.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(label, 6)
    .rightSpaceToView(self, 20)
    .heightIs(40);

}


@end
