//
//  LKScenicDetailView.h
//  TravelNew
//
//  Created by mac on 2020/11/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomView.h"
#import "ScenicSearchEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKScenicDetailView : BaseCustomView
@property(nonatomic,strong) ScenicSearchEntity *model;
@property(nonatomic,copy) void(^navigationBlock)(ScenicSearchEntity *);
@end

NS_ASSUME_NONNULL_END
