//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface FootprintSearchEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,strong) NSNumber *distance;
@property (nonatomic,copy) NSString *imageUrl;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *footprintId;
@property (nonatomic,strong) NSNumber *createTime;
 


-(id)initWithJson:(NSDictionary *)json;

@end
