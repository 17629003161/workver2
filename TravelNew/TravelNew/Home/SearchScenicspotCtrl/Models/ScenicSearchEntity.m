//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "ScenicSearchEntity.h"

@implementation ScenicSearchEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.city  = [json objectForKey:@"city"];
		self.distance  = [json objectForKey:@"distance"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.scenicImage  = [json objectForKey:@"scenicImage"];
		self.scenicId  = [json objectForKey:@"scenicId"];
		self.scenicAbstract  = [json objectForKey:@"scenicAbstract"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.scenicName  = [json objectForKey:@"scenicName"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.city forKey:@"zx_city"];
	[aCoder encodeObject:self.distance forKey:@"zx_distance"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.scenicImage forKey:@"zx_scenicImage"];
	[aCoder encodeObject:self.scenicId forKey:@"zx_scenicId"];
	[aCoder encodeObject:self.scenicAbstract forKey:@"zx_scenicAbstract"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.scenicName forKey:@"zx_scenicName"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.city = [aDecoder decodeObjectForKey:@"zx_city"];
		self.distance = [aDecoder decodeObjectForKey:@"zx_distance"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.scenicImage = [aDecoder decodeObjectForKey:@"zx_scenicImage"];
		self.scenicId = [aDecoder decodeObjectForKey:@"zx_scenicId"];
		self.scenicAbstract = [aDecoder decodeObjectForKey:@"zx_scenicAbstract"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.scenicName = [aDecoder decodeObjectForKey:@"zx_scenicName"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"city : %@\n",self.city];
	result = [result stringByAppendingFormat:@"distance : %@\n",self.distance];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"scenicImage : %@\n",self.scenicImage];
	result = [result stringByAppendingFormat:@"scenicId : %@\n",self.scenicId];
	result = [result stringByAppendingFormat:@"scenicAbstract : %@\n",self.scenicAbstract];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"scenicName : %@\n",self.scenicName];
	
    return result;
}

@end
