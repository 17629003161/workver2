//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ScenicSearchEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *city;
@property (nonatomic,strong) NSNumber *distance;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,copy) NSString *scenicImage;
@property (nonatomic,copy) NSString *scenicId;
@property (nonatomic,copy) NSString *scenicAbstract;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,copy) NSString *scenicName;
 


-(id)initWithJson:(NSDictionary *)json;

@end
