//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "FootprintSearchEntity.h"

@implementation FootprintSearchEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
		self.content  = [json objectForKey:@"content"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.address  = [json objectForKey:@"address"];
		self.distance  = [json objectForKey:@"distance"];
		self.imageUrl  = [json objectForKey:@"imageUrl"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.footprintId  = [json objectForKey:@"footprintId"];
		self.createTime  = [json objectForKey:@"createTime"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.content forKey:@"zx_content"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.distance forKey:@"zx_distance"];
	[aCoder encodeObject:self.imageUrl forKey:@"zx_imageUrl"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.footprintId forKey:@"zx_footprintId"];
	[aCoder encodeObject:self.createTime forKey:@"zx_createTime"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.content = [aDecoder decodeObjectForKey:@"zx_content"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.distance = [aDecoder decodeObjectForKey:@"zx_distance"];
		self.imageUrl = [aDecoder decodeObjectForKey:@"zx_imageUrl"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.footprintId = [aDecoder decodeObjectForKey:@"zx_footprintId"];
		self.createTime = [aDecoder decodeObjectForKey:@"zx_createTime"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"content : %@\n",self.content];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"distance : %@\n",self.distance];
	result = [result stringByAppendingFormat:@"imageUrl : %@\n",self.imageUrl];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"footprintId : %@\n",self.footprintId];
	result = [result stringByAppendingFormat:@"createTime : %@\n",self.createTime];
	
    return result;
}

@end
