//
//  SearchScenicspotCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SearchScenicspotCtrl.h"
#import "SearchCell.h"
#import "SelectCityCtrl.h"
#import "ScenicSearchEntity.h"
#import "FootprintSearchEntity.h"
#import "LKSceneView.h"
#import "LKScenicDetailView.h"
#import "ScenicSpotMain.h"

@interface SearchScenicspotCtrl ()<UITextFieldDelegate,MAMapViewDelegate>{
    UIView *_footerView;
}
@property(nonatomic,strong) UITextField *textField;
@property(nonatomic,strong) MAPointAnnotation *pointAnnotation;
@property(nonatomic,strong) UILabel *cityLabel;

@property (nonatomic,strong) UIView *FootprintListView;
@property (nonatomic,strong) LKScenicDetailView *scenicDetailView;
@property (nonatomic,strong) NSString *scenicId;

@property(nonatomic,strong) ScenicSearchEntity *selectScenicSpot;                   //选择的景点

@property(nonatomic,assign) BOOL isShowList;

@end

@implementation SearchScenicspotCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self layout];
    [self layoutTopView];
    self.selectScenicSpot = nil;
}

- (void)scenicIsClicked:(UITapGestureRecognizer *)reg{
    ScenicSpotMain *vc = [ScenicSpotMain new];
    vc.scenicId = self.selectScenicSpot.scenicId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)layoutTopView{                  //
    self.scenicDetailView = [[LKScenicDetailView alloc] init];
    LKWeakSelf
    self.scenicDetailView.navigationBlock = ^(ScenicSearchEntity * _Nonnull model) {
            NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
            NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
            NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
            [weakSelf doNavigationWithEndLocation:endLocation];
    };
    [self.scenicDetailView addTapGestureTarget:self action:@selector(scenicIsClicked:)];
    [self.view sd_addSubviews:@[self.scenicDetailView]];
    self.scenicDetailView.sd_layout
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .bottomSpaceToView(self.view, 20)
    .heightIs(90);
    
}





-(void)setIsShowList:(BOOL)isShowList
{
    _isShowList = isShowList;
    self.tableView.hidden = !_isShowList;
    self.FootprintListView.hidden = _isShowList;
}

-(void)setSelectScenicSpot:(ScenicSearchEntity *)selectScenicSpot
{
    _selectScenicSpot = selectScenicSpot;
    if(_selectScenicSpot){
        self.scenicDetailView.hidden = NO;
        self.scenicDetailView.model = selectScenicSpot;
    }else{
        self.scenicDetailView.hidden = YES;
    }
}

-(void)transformView:(NSNotification *)aNSNotification
{

}

- (void)itemIsClicked:(UITapGestureRecognizer *)reg
{

}

- (void)changeCityIsClicked:(id)sender
{
    LKWeakSelf
    SelectCityCtrl *vc = [SelectCityCtrl new];
    vc.OnSelectAddress = ^(AMapDistrict * dist) {
        weakSelf.cityLabel.text = dist.name;
        [weakSelf.cityLabel updateLayout];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(dist.center.latitude, dist.center.longitude);
        weakSelf.mapView.centerCoordinate = coord;
//        [weakSelf sendBatchRequest:dist.citycode];
    };
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)viewIsTaped:(id)sender{
    [_textField resignFirstResponder];
}

- (void)layout{
    [self.view addSubview:self.mapView];
    self.mapView.frame = self.view.bounds;
    [self.view sendSubviewToBack:self.mapView];

    self.tableView.frame = CGRectMake(0, 86, kScreenWidth, 243);
    [self.tableView registerClass:[SearchCell class] forCellReuseIdentifier:@"SearchCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _footerView = [Utils createNoDataView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 86)];
    [view addTapGestureTarget:self action:@selector(viewIsTaped:)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    UIImageView *back = ImageViewWithImage(@"left_arrow_black");
    [back setUserInteractionEnabled:YES];
    [back addTapGestureTarget:self action:@selector(backIsClicked:)];
    LKLocationManager *manager = [LKLocationManager sharedManager];
    UILabel *label = [Utils createLabelWithTitle:manager.cityName titleColor:HexColor(0x333333) fontSize:15];
    [label addTapGestureTarget:self action:@selector(changeCityIsClicked:)];
    
    self.cityLabel = label;
    UIImageView *downarrow = ImageViewWithImage(@"Combined Shape");
    
    _textField = [UITextField new];
    _textField.delegate = self;
    _textField.clearButtonMode = UITextFieldViewModeAlways;
    _textField.backgroundColor = HexColor(0xF8F9FB);
    _textField.textColor = HexColor(0x333333);
    _textField.font = [UIFont systemFontOfSize:15];
   // textField.layer.borderColor = HexColor(0xd9d9d9).CGColor;
    //textField.layer.borderWidth = 1.0;
    _textField.layer.cornerRadius = 19.0;
    _textField.placeholder = @"搜索热门景区";
    _textField.returnKeyType = UIReturnKeySearch;
    UIImageView *leftimg = ImageViewWithImage(@"search_home");
    
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 40)];
    [emptyView addSubview:leftimg];
    leftimg.sd_layout
    .centerYEqualToView(emptyView)
    .centerXEqualToView(emptyView)
    .widthIs(14)
    .heightIs(14);
    
    _textField.leftView=emptyView;
    _textField.leftViewMode=UITextFieldViewModeAlways;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = LKSystemFont(14);
    [btn setTitleColor:HexColor(0x2473FF) forState:UIControlStateNormal];
    [btn setTitle:@"搜索" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(searchIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view sd_addSubviews:@[back,label,downarrow,_textField,btn]];
    
    back.sd_layout
    .topSpaceToView(view, 45+10)
    .leftSpaceToView(view, 14)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .centerYEqualToView(back)
    .leftSpaceToView(back, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    downarrow.sd_layout
    .centerYEqualToView(label)
    .leftSpaceToView(label, 3)
    .widthIs(10)
    .heightIs(6);
    
    _textField.sd_layout
    .centerYEqualToView(downarrow)
    .leftSpaceToView(downarrow, 12)
    .heightIs(38)
    .rightSpaceToView(btn, 5);
    _textField.keyboardType = UIKeyboardTypeDefault;
    
    btn.sd_layout
    .centerYEqualToView(downarrow)
    .rightSpaceToView(view, 5);
    [btn setupAutoSizeWithHorizontalPadding:0 buttonHeight:20];
    
}

- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (UIView *)createScenicView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 83, kScreenWidth, 185)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 10;
    
    UILabel *label = [Utils createBoldLabelWithTitle:@"附近精彩动态" titleColor:HexColor(0x323232) fontSize:15];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"查看全部" titleColor:HexColor(0x999999) fontSize:14];
    [label2 addTapGestureTarget:self action:@selector(allBtnClicked:)];
    UIImageView *arrow = ImageViewWithImage(@"return_black1");
    [view sd_addSubviews:@[label,label2,arrow,
                           self.scrollView]];
    label.sd_layout
    .leftSpaceToView(view, 14)
    .topSpaceToView(view, 16)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(label)
    .rightSpaceToView(view, 16)
    .widthIs(6)
    .heightIs(10);
    
    label2.sd_layout
    .centerYEqualToView(label)
    .rightSpaceToView(arrow, 5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.scrollView.sd_layout
    .topSpaceToView(label, 10)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .bottomSpaceToView(view, 5);
    return  view;
    
}

- (void)allBtnClicked:(UITapGestureRecognizer *)reg
{

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate=self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate=nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *strContent = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //DLog(@"当前输入内容：%@", strContent);
    if([strContent isBlankString]){
        self.isShowList = NO;
    }else{
        self.isShowList = YES;
        self.tableView.frame = CGRectMake(0, 86, kScreenWidth, 60*self.dataArray.count);
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchIsClicked:nil];
    return [textField resignFirstResponder];
}

//点击地图底图调用
- (void)mapView:(MAMapView *)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.textField resignFirstResponder];
}


//- (MAMapView *)mapView {
//    if (!_mapView) {
//        _mapView = [[MAMapView alloc] initWithFrame: self.view.bounds];
//        _mapView.delegate = self;
//        // 显示比例尺
//        _mapView.showsScale = NO;
//        // 显示指南针
//        _mapView.showsCompass = NO;
//        // 显示定位蓝点
//        _mapView.showsUserLocation = YES;
//        // 用户定位模式
//        _mapView.userTrackingMode = MAUserTrackingModeFollow;
//        // 设置缩放级别
//        [_mapView setZoomLevel:13];
//        // 设置当前地图的中心点：例如默认地图中心显示坐标为（39.9088230000, 116.3974700000）
//       // _mapView.centerCoordinate = CLLocationCoordinate2DMake(39.9088230000, 116.3974700000);
//        ///如果您需要进入地图就显示定位小蓝点，则需要下面两行代码
//        _mapView.showsUserLocation = YES;
//        _mapView.userTrackingMode = MAUserTrackingModeFollow;
//
//        _pointAnnotation = [[MAPointAnnotation alloc] init];
//        _pointAnnotation.coordinate = CLLocationCoordinate2DMake(39.989631, 116.481018);
//        _pointAnnotation.title = @"方恒国际";
//        _pointAnnotation.subtitle = @"阜通东大街6号";
//        [_mapView addAnnotation:_pointAnnotation];
//        [self setupCustomMapOptions:_mapView];
//    }
//    return _mapView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    ScenicSearchEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    //AMapPOI *poi = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = entity;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.isShowList = NO;
    ScenicSearchEntity *model = [self.dataArray objectAtIndex:indexPath.row];      //景点
    self.selectScenicSpot = model;
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake([model.latitude doubleValue],[model.longitude doubleValue]);
    self.textField.text = model.scenicName;
    [self.textField resignFirstResponder];
    self.scenicId = model.scenicId;
}


- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAUserLocation class]]) {
        MAUserLocationRepresentation *r = [[MAUserLocationRepresentation alloc] init];
        r.showsAccuracyRing = NO;///精度圈是否显示，默认YES
        [self.mapView updateUserLocationRepresentation:r];
        
        static NSString *userLocationStyleReuseIndetifier = @"userLocationStyleReuseIndetifier";
        MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
        if (annotationView == nil) {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"address_icon3"];
        return annotationView;
    }
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:reuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"target_address"];
        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        return annotationView;
    }
    
    return nil;
}


- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    MACoordinateRegion region;
    CLLocationCoordinate2D centerCoordinate = self.mapView.region.center;
    region.center= centerCoordinate;
    _pointAnnotation.coordinate = centerCoordinate;
   // DLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);

}


- (void)searchKeywords:(NSString *)keywords
{
    LKLocationManager *manger = [LKLocationManager sharedManager];
    Api *api = [Api apiPostUrl:@"attractions/search/searchScenic" para:@{@"lon":@(manger.coordinate.longitude),
                                                                         @"lat":@(manger.coordinate.latitude),
                                                                         @"str":keywords}
                   description:@"搜索景点"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            [self.dataArray removeAllObjects];
            NSArray *ary = [json objectForKey:@"data"];
            for (NSDictionary *dict in ary) {
                ScenicSearchEntity *model = [[ScenicSearchEntity alloc] initWithJson:dict];
                [self.dataArray addObject:model];
                [self.tableView reloadData];
                self.isShowList = YES;
            }
            self.tableView.frame = CGRectMake(0, 86, kScreenWidth, 60*ary.count);
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)searchIsClicked:(id)sender{
    [self.textField resignFirstResponder];
    NSString *keywords = [_textField.text trimWhitespace];
    [self searchKeywords:keywords];
    
}


@end

