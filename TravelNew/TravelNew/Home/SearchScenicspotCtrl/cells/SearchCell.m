//
//  SearchCell.m
//  TravelNew
//
//  Created by mac on 2020/10/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "SearchCell.h"

@interface SearchCell()

@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *distanceLabel;

@end


@implementation SearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(ScenicSearchEntity *)model
{
    _model = model;
    self.nameLabel.text = model.scenicName;
    self.distanceLabel.text = [NSString stringWithFormat:@"距我%.1lfkm",[model.distance floatValue]/1000.0];
    [self.nameLabel updateLayout];
    [self.distanceLabel updateLayout];
}


- (void)configViews{
    UIImageView *imgv = ImageViewWithImage(@"city_location");
    UILabel *label1 = [Utils createLabelWithTitle:@"大雁塔" titleColor:HexColor(0x414243) fontSize:14];
    UILabel *label2 = [Utils createLabelWithTitle:@"距我30km" titleColor:HexColor(0x414243) fontSize:10];
    [self.contentView sd_addSubviews:@[imgv,label1,label2]];
    imgv.sd_layout
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 18)
    .widthIs(16)
    .heightIs(23);
    
    label1.sd_layout
    .topSpaceToView(self.contentView, 13)
    .leftSpaceToView(imgv, 5)
    .rightSpaceToView(self.contentView, 18)
    .autoHeightRatio(0);
    
    label2.sd_layout
    .leftEqualToView(label1)
    .rightSpaceToView(self.contentView, 18)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    self.nameLabel = label1;
    self.distanceLabel = label2;
}


@end
