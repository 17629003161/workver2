//
//  SearchCell.h
//  TravelNew
//
//  Created by mac on 2020/10/9.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "ScenicSearchEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchCell : BaseCustomTableViewCell
@property(nonatomic,strong) ScenicSearchEntity *model;
@end

NS_ASSUME_NONNULL_END
