//
//  AllRecommenderCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "AllRecommenderCtrl.h"
#import "HotRecommenderCell.h"
#import "MainRecommenderCtrl.h"
#import "RecommenderEntity.h"

@interface AllRecommenderCtrl ()

@end

@implementation AllRecommenderCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"热门推荐官" titleColor:nil];
    self.tableView.backgroundColor = [UIColor colorWithHex:0xF3F3F3];
    [self.tableView registerClass:[HotRecommenderCell class] forCellReuseIdentifier:@"HotRecommenderCell"];
    self.tableView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomSpaceToView(self.view, SAFEVIEWBOTTOMMARGIN);
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    if(self.cityCode == nil)
        return nil;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getAllLtReferrerUser"
                          para:@{@"cityid":self.cityCode,
                                    @"lon":@(self.coord.longitude),
                                    @"lat":@(self.coord.latitude),
                              @"pageIndex":@(self.pageIndex),
                               @"pageSize":@(self.pageSize)}
                   description:@"user-获取城市全部推荐官"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(RecommenderEntity.class);
    return name;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 168;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotRecommenderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HotRecommenderCell" forIndexPath:indexPath];
    cell.model = [self.dataArray objectAtIndex:indexPath.row];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 11)];
    view.backgroundColor = [UIColor colorWithHex:0xF3F3F3];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 11;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommenderEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    MainRecommenderCtrl *vc = [[MainRecommenderCtrl alloc] init];
    vc.userId = model.userId;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
