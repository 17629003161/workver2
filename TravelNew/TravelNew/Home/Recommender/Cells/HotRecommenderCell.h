//
//  HotRecommenderCell.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommanderMapListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotRecommenderCell : UITableViewCell
@property(nonatomic,strong) RecommanderMapListEntity *model;
@end

NS_ASSUME_NONNULL_END
