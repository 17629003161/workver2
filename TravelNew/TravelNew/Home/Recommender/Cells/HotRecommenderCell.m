//
//  HotRecommenderCell.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "HotRecommenderCell.h"
#import "GRStarsView.h"

@interface HotRecommenderCell()
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *signLabel;
@property(nonatomic,strong) UIImageView *headImgv;
@property(nonatomic,strong) UILabel *seviceLabel;
@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) UILabel *distanceLabel;
@property(nonatomic,strong) UILabel *serviceLabel;
@property(nonatomic,strong) GRStarsView *starView;
@end

@implementation HotRecommenderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(RecommanderMapListEntity *)model
{
    DLog(@"model = %@",model);
    _model = model;
    [self.headImgv sd_setImageWithURL:[NSURL URLWithString:model.headerImage] placeholderImage:[UIImage imageNamed:@"default_header"]];
    NSString *timesstr = [NSString stringWithFormat:@"%@",model.serviceCount];
    NSString *scorestr = [NSString stringWithFormat:@"%@",model.score];
    NSString *commentTimesStr = [NSString stringWithFormat:@"%@",model.appraiseCount];
    self.starView.score = [model.score floatValue];
    self.tipLabel.text = [NSString stringWithFormat:@"服务%@次 | %@分（%@条评论）",timesstr,scorestr,commentTimesStr];
    self.serviceLabel.text = model.services;
    CGFloat dist = [model.distance doubleValue];
    if(dist < 1000){
        self.distanceLabel.text = [NSString stringWithFormat:@"距我%.2lf米",dist];
    }else{
        self.distanceLabel.text = [NSString stringWithFormat:@"距我%.2lf公里",dist/1000.0];
    }
    [self.tipLabel changeStrings:@[timesstr] toColor:HexColor(0x4D86FD)];
    [self.tipLabel changeStrings:@[scorestr,commentTimesStr] toColor:HexColor(0xFFAE3E)];
    self.signLabel.text = model.sign;
    self.nameLabel.text = model.userNickname;
    [self.distanceLabel updateLayout];
    [self.signLabel updateLayout];
    [self.nameLabel updateLayout];
    [self.tipLabel updateLayout];
    [self.serviceLabel updateLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self configViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)configViews{
    self.contentView.backgroundColor = [UIColor colorWithHex:0xF3F3F3];
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 8;
    UIImageView *imgv = ImageViewWithImage(@"person_header");
    imgv.layer.cornerRadius = 30;
    imgv.layer.masksToBounds = YES;
    self.headImgv = imgv;
    UILabel *label1 = [Utils createLabelWithTitle:@"我们在这边"
                                       titleColor:HexColor(0x333333)
                                         fontSize:16];
    self.nameLabel = label1;
    label1.font = [UIFont boldSystemFontOfSize:16];
    
    GRStarsView *starsView = [[GRStarsView alloc] initWithStarSize:CGSizeMake(13, 13) margin:10 numberOfStars:5];
    starsView.allowSelect = NO;
    starsView.score = 4.5;
    self.starView = starsView;
    
    UILabel *label2 = [Utils createLabelWithTitle:@"服务689次 | 4.2分（20条评论）？| 90后"
                                       titleColor:HexColor(0x999999)
                                         fontSize:12];
    self.tipLabel = label2;
    [label2 changeStrings:@[@"689"] toColor:HexColor(0x4D86FD)];
    [label2 changeStrings:@[@"4.2",@"90后"] toColor:HexColor(0xFFAE3E)];
    UILabel *label3 = [Utils createLabelWithTitle:@"西安本地人，热爱旅游喜欢交朋友，西安本地。。。"
                                       titleColor:HexColor(0x323232)
                                         fontSize:14];
    self.signLabel = label3;
    UILabel *label4 = [Utils createLabelWithTitle:@"当地咨询 | 当地用车 | 当地住宿"
                                       titleColor:HexColor(0x999999)
                                         fontSize:12];
    self.serviceLabel = label4;
    UILabel *distanceLabel = [Utils createLabelWithTitle:@"距我100米"
                                              titleColor:HexColor(0x4D86FD)
                                                fontSize:12];
    self.distanceLabel = distanceLabel;
    self.seviceLabel = label4;
    [view sd_addSubviews:@[imgv,label1,starsView,label2,
                             label3,label4,distanceLabel]];
    [self.contentView addSubview:view];
    imgv.sd_layout
    .topSpaceToView(view, 16)
    .leftSpaceToView(view, 18)
    .widthIs(60)
    .heightEqualToWidth();
    
    label1.sd_layout
    .topSpaceToView(view, 25)
    .leftSpaceToView(imgv, 10)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    starsView.sd_layout
    .centerYEqualToView(label1)
    .leftSpaceToView(label1, 10)
    .widthIs(69)
    .heightIs(13);
    
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 2.8)
    .rightSpaceToView(view, 18)
    .autoHeightRatio(0);
    
    label3.sd_layout
    .topSpaceToView(imgv, 14)
    .leftEqualToView(imgv)
    .rightSpaceToView(view, 18)
    .heightIs(20);
    
    label4.sd_layout
    .topSpaceToView(label3, 6)
    .leftEqualToView(label3)
    .rightEqualToView(label3)
    .autoHeightRatio(0);
    
    distanceLabel.sd_layout
    .centerYEqualToView(label4)
    .rightSpaceToView(view, 18)
    .autoHeightRatio(0);
    [distanceLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    view.sd_layout
    .centerXEqualToView(self.contentView)
    .centerYEqualToView(self.contentView)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20)
    .heightIs(150);

}


@end
