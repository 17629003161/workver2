//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface DataEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,copy) NSString *score;
@property (nonatomic,strong) NSNumber *distance;
@property (nonatomic,copy) NSString *services;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,strong) NSNumber *referrerLatitude;
@property (nonatomic,copy) NSString *serviceCount;
@property (nonatomic,copy) NSString *sign;
@property (nonatomic,strong) NSNumber *referrerLongitude;
@property (nonatomic,strong) NSNumber *appraiseCount;
 


-(id)initWithJson:(NSDictionary *)json;

@end
