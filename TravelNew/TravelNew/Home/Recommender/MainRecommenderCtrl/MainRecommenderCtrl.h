//
//  MainRecommenderCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainRecommenderCtrl : LKBaseViewController

@property(nonatomic,strong) NSNumber *userId;

@end

NS_ASSUME_NONNULL_END
