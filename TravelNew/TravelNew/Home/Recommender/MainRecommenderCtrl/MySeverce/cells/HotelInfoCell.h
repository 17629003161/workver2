//
//  HotelInfoCell.h
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "RoomSeupListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotelInfoCell : BaseCustomTableViewCell
@property(nonatomic,copy) void (^showPictureBlock)(NSArray *, UIView *view);
@property(nonatomic,strong) RoomSeupListEntity *model;
@property(nonatomic,copy) void(^navigationBlock)(RoomSeupListEntity *);
@property(nonatomic,assign) BOOL isOpen;
@end

NS_ASSUME_NONNULL_END
