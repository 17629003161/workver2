//
//  CarInfoCell.h
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "CarDataEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarInfoCell : BaseCustomTableViewCell
@property(nonatomic,assign) BOOL isOpen;
@property(nonatomic,strong) CarDataEntity *model;
@end

NS_ASSUME_NONNULL_END
