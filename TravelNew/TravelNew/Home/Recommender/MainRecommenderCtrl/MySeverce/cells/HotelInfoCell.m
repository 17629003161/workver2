//
//  HotelInfoCell.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "HotelInfoCell.h"

@interface HotelInfoCell()
@property(nonatomic,strong) UIImageView *roomImageView;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *roomNumLabel;
@property(nonatomic,strong) UILabel *capicityLabel;
@property(nonatomic,strong) UILabel *priceLabel;
@property(nonatomic,strong) UILabel *addressLabel;

@property(nonatomic,strong) UIImageView *notOpenImageView;

@end

@implementation HotelInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    self.notOpenImageView.hidden = isOpen;
}

-(void)setModel:(RoomSeupListEntity *)model
{
    _model = model;
    LtHouseImageListEntity *entity = nil;
    if(model.ltHouseImageList.count>0){
        entity = model.ltHouseImageList[0];
    }
    [self.roomImageView sd_setImageWithURL:[NSURL URLWithString:entity.imageurl]
                          placeholderImage:[UIImage imageNamed:@"home3"]];
    self.nameLabel.text = model.title;
    self.roomNumLabel.text = [model.bedroomsnum stringValue];
    self.capicityLabel.text = [model.number stringValue];
    self.priceLabel.text = [model.price stringValue];
    self.addressLabel.text = model.address;
    
    [self.nameLabel updateLayout];
    [self.roomNumLabel updateLayout];
    [self.capicityLabel updateLayout];
    [self.priceLabel updateLayout];
    [self.addressLabel updateLayout];
    
}

- (void)pictureIsClicked:(UITapGestureRecognizer *)reg
{
    UIView *picView = reg.view;
    NSMutableArray *ary = [NSMutableArray new];
    for(LtHouseImageListEntity *entity in self.model.ltHouseImageList) {
        [ary addObject:entity.imageurl];
    }
    if(self.showPictureBlock){
        self.showPictureBlock(ary,picView);
    }
}

- (void)arrowIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.navigationBlock){
        self.navigationBlock(self.model);
    }
}

- (void)configViews{
    UIView *bgview = [UIView new];
    bgview.backgroundColor = [UIColor whiteColor];
    bgview.layer.cornerRadius = 8.0;
    bgview.layer.borderColor = HexColor(0xD5D5D5).CGColor;
    bgview.layer.borderWidth = 0.5;
    bgview.layer.masksToBounds = YES;
    
    [self.contentView addSubview:bgview];
    UIImageView *imgv = ImageViewWithImage(@"home3");
    imgv.contentMode = UIViewContentModeScaleAspectFill;
    imgv.layer.masksToBounds = YES;
    [imgv addTapGestureTarget:self action:@selector(pictureIsClicked:)];
    self.roomImageView = imgv;
    UILabel *label1 = [Utils createLabelWithTitle:@"市中心/钟楼地铁三室居" titleColor:HexColor(0x323232) fontSize:16];
    self.nameLabel = label1;
    UIView *boxview1 = [self createBoxView];
    self.roomNumLabel = (UILabel *)[boxview1 viewWithTag:1000];
    UIView *boxview2 = [self createBoxView];
    self.capicityLabel = (UILabel *)[boxview2 viewWithTag:1000];
    UIView *boxview3 = [self createBoxView];
    self.priceLabel = (UILabel *)[boxview3 viewWithTag:1000];
    UIImageView *bgimgview = ImageViewWithImage(@"bg_group_9");
    UILabel *label2 = [Utils createLabelWithTitle:@"西安碑林区南光急街94号" titleColor:HexColor(0x323232) fontSize:14];
    self.addressLabel = label2;
    UILabel *label3 = [Utils createLabelWithTitle:@"导航" titleColor:HexColor(0x999999) fontSize:14];
    [label3 addTapGestureTarget:self action:@selector(arrowIsClicked:)];
    UIImageView *arrow = ImageViewWithImage(@"daohang");
    [arrow addTapGestureTarget:self action:@selector(arrowIsClicked:)];
    [bgview sd_addSubviews:@[imgv,
                             label1,
                             boxview1,boxview2,boxview3,
                             bgimgview,
                             label2,label3,arrow]];
    
    self.notOpenImageView = ImageViewWithImage(@"not_open_photo");
    self.notOpenImageView.hidden = YES;
    [bgview addSubview:self.notOpenImageView];
    
    self.notOpenImageView.sd_layout
    .topEqualToView(bgview)
    .leftEqualToView(bgview)
    .rightEqualToView(bgview)
    .bottomEqualToView(bgview);
    
    
    bgview.sd_layout
    .topSpaceToView(self.contentView, 10)
    .bottomSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20);
    
    imgv.sd_layout
    .topEqualToView(bgview)
    .leftEqualToView(bgview)
    .rightEqualToView(bgview)
    .heightIs(150);
    
    label1.sd_layout
    .topSpaceToView(imgv, 12)
    .leftSpaceToView(bgview, 16)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
        
    CGFloat width = (kScreenWidth-74-26)/3.0;
    boxview1.sd_layout
    .topSpaceToView(label1, 14)
    .leftEqualToView(label1)
    .widthIs(width)
    .heightIs(40);
    
    boxview2.sd_layout
    .centerYEqualToView(boxview1)
    .leftSpaceToView(boxview1, 13)
    .widthIs(width)
    .heightIs(40);
    
    boxview3.sd_layout
    .centerYEqualToView(boxview2)
    .rightSpaceToView(bgview, 17)
    .widthIs(width)
    .heightIs(40);
    
    bgimgview.sd_layout
    .topSpaceToView(boxview1, 15)
    .leftEqualToView(boxview1)
    .rightEqualToView(boxview3)
    .heightIs(50);
    
    label2.sd_layout
    .leftSpaceToView(bgview, 32)
    .topSpaceToView(boxview1, 30)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    arrow.sd_layout
    .centerYEqualToView(label2)
    .rightSpaceToView(bgview, 37)
    .widthIs(14)
    .heightIs(14);
    
    label3.sd_layout
    .centerYEqualToView(arrow)
    .rightSpaceToView(arrow, 5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    UILabel *label11 = [boxview1 viewWithTag:1];
    label11.text = @"卧室(间)";
    UILabel *label12 = [boxview1 viewWithTag:2];
    label12.text = @"3";
    
    UILabel *label21 = [boxview2 viewWithTag:1];
    label21.text = @"入住人数(人)";
    UILabel *label22 = [boxview2 viewWithTag:2];
    label22.text = @"6";
    
    UILabel *label31 = [boxview3 viewWithTag:1];
    label31.text = @"价格(元/天)";
    UILabel *label32 = [boxview3 viewWithTag:2];
    label32.text = @"268";
    [label32 changStrings:@[@"268"] toSize:16 toColor:HexColor(0xFF9739)];
}

- (UIView *)createBoxView{
    UIView *view = [UIView new];
    UILabel *label1 = [Utils createLabelWithTitle:@"title" titleColor:HexColor(0x999999) fontSize:10];
    label1.tag = 1;
    UILabel *label2 = [Utils createLabelWithTitle:@"title2" titleColor:HexColor(0x323232) fontSize:14];
    label2.tag = 1000;
    [view sd_addSubviews:@[label1,
                           label2]];
    label1.sd_layout
    .topSpaceToView(view, 4)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerXEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label2 bottomMargin:3];
    view.layer.borderColor = HexColor(0xE1E1E1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 4;
    
    return view;
}


@end
