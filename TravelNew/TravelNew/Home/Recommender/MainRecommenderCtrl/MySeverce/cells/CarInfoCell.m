//
//  CarInfoCell.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "CarInfoCell.h"
@interface CarInfoCell()
@property(nonatomic,strong) UIImageView *notOpenImageView;
@property(nonatomic,strong) UIImageView *carIcon;

@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *carNumberLabel;
@property(nonatomic,strong) UILabel *carAgeLabel;
@property(nonatomic,strong) UILabel *carSites;

@property(nonatomic,strong) UILabel *titleLabel1;
@property(nonatomic,strong) UILabel *titleLabel2;
@property(nonatomic,strong) UILabel *titleLabel3;

@end

@implementation CarInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    self.notOpenImageView.hidden = isOpen;
}

- (void)setModel:(CarDataEntity *)model
{
    _model = model;
    [self.carIcon sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"add_car"]];
    self.nameLabel.text = [NSString stringWithFormat:@"(%@)%@",model.carColor,model.name];
    self.carNumberLabel.text = [self.model.carNumble stringByReplacingCharactersInRange:NSMakeRange(2, 3) withString:@"***"];
    self.carAgeLabel.text = self.model.carYear;
    self.carSites.text = self.model.numberOfSeats;
    [self.carSites updateLayout];
    [self.nameLabel updateLayout];
    [self.carNumberLabel updateLayout];
    [self.carAgeLabel updateLayout];
}

- (void)configViews{
    UIView *bgview = [UIView new];
    bgview.backgroundColor = HexColor(0xf8f8f8);
    [self.contentView addSubview:bgview];
    UIImageView *carIcon = ImageViewWithImage(@"add_car");
    self.carIcon = carIcon;
    UILabel *label1 = [Utils createLabelWithTitle:@"丰田 凯美瑞" titleColor:HexColor(0x323232) fontSize:18];
    self.nameLabel = label1;
    UIImageView *imgv = ImageViewWithImage(@"car_verify");
    
    UIView *boxview1 = [self createBoxView];
    self.titleLabel1 = (UILabel *)[boxview1 viewWithTag:800];
    self.titleLabel1.text = @"车牌号码";
    [self.titleLabel1 updateLayout];
    self.carNumberLabel = (UILabel *)[boxview1 viewWithTag:1000];
    
    UIView *boxview2 = [self createBoxView];
    self.titleLabel2 = (UILabel *)[boxview2 viewWithTag:800];
    self.titleLabel2.text = @"座位数";
    [self.titleLabel2 updateLayout];
    self.carSites = (UILabel *)[boxview2 viewWithTag:1000];
    
    UIView *boxview3 = [self createBoxView];
    self.titleLabel3 = (UILabel *)[boxview3 viewWithTag:800];
    self.titleLabel3.text = @"车龄";
    [self.titleLabel3 updateLayout];
    self.carAgeLabel = (UILabel *)[boxview3 viewWithTag:1000];
    
    
    [bgview sd_addSubviews:@[carIcon,label1,imgv,
                             boxview1,boxview2,boxview3]];
    self.notOpenImageView = ImageViewWithImage(@"not_open_photo");
    self.notOpenImageView.hidden = YES;
    [bgview addSubview:self.notOpenImageView];
    
    self.notOpenImageView.sd_layout
    .topEqualToView(bgview)
    .leftEqualToView(bgview)
    .rightEqualToView(bgview)
    .bottomEqualToView(bgview);
    
    bgview.sd_layout
    .topSpaceToView(self.contentView, 10)
    .bottomSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 20)
    .rightSpaceToView(self.contentView, 20);
    
    carIcon.sd_layout
    .topSpaceToView(bgview, 17)
    .leftSpaceToView(bgview, 16)
    .widthIs(40)
    .heightIs(40);
    
    label1.sd_layout
    .centerYEqualToView(carIcon)
    .leftSpaceToView(carIcon, 6)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(label1)
    .rightSpaceToView(bgview, 10)
    .widthIs(74)
    .heightIs(22);
    
    CGFloat width = (kScreenWidth-72-40)/3.0;
    boxview1.sd_layout
    .topSpaceToView(label1, 20)
    .leftSpaceToView(bgview, 20)
    .widthIs(width)
    .heightIs(40);
    
    boxview2.sd_layout
    .centerYEqualToView(boxview1)
    .leftSpaceToView(boxview1, 20)
    .widthIs(width)
    .heightIs(40);
    
    boxview3.sd_layout
    .centerYEqualToView(boxview2)
    .leftSpaceToView(boxview2, 20)
    .widthIs(width)
    .heightIs(40);

}

- (UIView *)createBoxView{
    UIView *view = [UIView new];
    UILabel *label1 = [Utils createLabelWithTitle:@"title" titleColor:HexColor(0x999999) fontSize:10];
    label1.tag = 800;
    UILabel *label2 = [Utils createLabelWithTitle:@"title2" titleColor:HexColor(0x323232) fontSize:14];
    label2.tag = 1000;
    [view sd_addSubviews:@[label1,
                           label2]];
    label1.sd_layout
    .topSpaceToView(view, 4)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerXEqualToView(label1)
    .topSpaceToView(label1, 1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label2 bottomMargin:3];
    view.layer.borderColor = HexColor(0xE1E1E1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 4;
    
    return view;
}


@end
