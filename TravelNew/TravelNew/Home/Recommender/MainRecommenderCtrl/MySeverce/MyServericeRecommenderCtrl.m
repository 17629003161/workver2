//
//  MyServericeRecommenderCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MyServericeRecommenderCtrl.h"
#import "CarInfoCell.h"
#import "HotelInfoCell.h"
#import "GetMobileNumberCtrl.h"
#import "EMChatViewController.h"
#import "PaySucessCtrl.h"
#import "YTKBatchRequest.h"
#import "RecommanderBaseInfoEntity.h"
#import "LKTipDialog.h"
#import "FetchTelNumModel.h"
#import "LoginViewController.h"
#import "LKShowPictureView.h"

@interface MyServericeRecommenderCtrl (){
    UIView *_sectionView0;
    UIView *_sectionView1;
}
@property(nonatomic,assign) BOOL isCarServiceOpen;
@property(nonatomic,assign) BOOL isRoomServiceOpen;
@property(nonatomic,strong) NSMutableArray *carDataArray;
@property(nonatomic,strong) NSMutableArray *roomDataArray;
@property(nonatomic,strong) NSString *chartId;

@property(nonatomic,strong) UILabel *phoneLabel;
@property(nonatomic,strong) UILabel *wxLabel;
@property(nonatomic,strong) UIView *tabelHeaderView;
@property(nonatomic,strong) UIView *tableHeaderView2;
@property(nonatomic,strong) NSString *phoneNumber;
@property(nonatomic,strong) LKGadentButton *contactInLineBtn;


@end

@implementation MyServericeRecommenderCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DLog(@"%ld\n",[self.userId integerValue]);
    
    [self.tableView registerClass:[CarInfoCell class] forCellReuseIdentifier:@"CarInfoCell"];
    [self.tableView registerClass:[HotelInfoCell class] forCellReuseIdentifier:@"HotelInfoCell"];
    self.tabelHeaderView = [self createHeaderView];
    self.tableHeaderView2 = [self createHeaderView2];
    self.tableView.tableHeaderView = self.tabelHeaderView;
    self.tableView.tableFooterView = [self createFooterView];
    
    _sectionView0 = [self creatSectionView:@"当地用车"];
    _sectionView1 = [self creatSectionView:@"当地住宿"];
    self.carDataArray = [NSMutableArray new];
    self.roomDataArray = [NSMutableArray new];
    self.isCarServiceOpen = YES;
    self.isRoomServiceOpen = YES;
    [self sendBatchRequest];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.tableView.frame = self.view.bounds;
    UserInfo *user = [UserInfo shareInstance];
    if(![user isLogin]){
        self.contactInLineBtn.hidden = NO;
    }else{
        if([user isCurrentRecomander] && [user isRecomander]){
            self.contactInLineBtn.hidden = YES;
        }else{
            self.contactInLineBtn.hidden = NO;
        }
    }
}

- (void)loadFirstPage{
    [super loadFirstPage];
    [self sendBatchRequest];
}


- (void)sendBatchRequest{
    LKWeakSelf
    Api *api1 = [Api apiPostUrl:@"work/house/getUserHouseInfo"
                           para:@{@"userId":self.userId}
                    description:@"查询推荐官住宿信息列表"];
    Api *api2 = [Api apiPostUrl:@"work/price/list_block1A"
                           para:@{@"userId":self.userId}
                        description:@"查询推荐官车辆报价列表block1A"];
    Api *api3 = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUser_block1A"
                           para:@{@"userId":self.userId}
                    description:@"获取推荐官详情"];
    Api *api4 = [Api apiPostUrl:@"work/requirement/getPhone"
                           para:@{@"referrerUserId":self.userId}
                    description:@"获取号码"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api2,api3,api4]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        DLog(@"api1 jsonstr  = %@",request1.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        NSInteger code = [[json objectForKey:@"code"] integerValue];
        if(code==200){
            self.isRoomServiceOpen = YES;
            NSArray *data = [json objectForKey:@"data"];
            [self.roomDataArray removeAllObjects];
            for(NSDictionary *dict in data){
               RoomSeupListEntity *model = [[RoomSeupListEntity alloc] initWithJson:dict];
                [self.roomDataArray addObject:model];
            }
            [weakSelf.tableView reloadData];
            
        }else if(code==500){
            self.isRoomServiceOpen = NO;
            [self.tableView reloadData];
        }

        YTKRequest *request2 = batchRequest.requestArray[1];
        DLog(@"api2 jsonstr  = %@",request2.responseString);
        NSDictionary *json2 = [NSString jsonStringToDictionary:request2.responseString];
        NSInteger code2 = [[json2 objectForKey:@"code"] integerValue];
        if(code2==200){
            self.isCarServiceOpen = YES;
            NSArray *data = [json2 objectForKey:@"data"];
            [self.carDataArray removeAllObjects];
            for(NSDictionary *dict in data){
                CarDataEntity *model = [[CarDataEntity alloc] initWithJson:dict];
                [self.carDataArray addObject:model];
            }
            [weakSelf.tableView reloadData];
            
        }else if(code2==500){
            self.isCarServiceOpen = NO;
            [self.tableView reloadData];
        }
        
        YTKRequest *request3 = batchRequest.requestArray[2];
        DLog(@"jsonstr = %@",request3.responseString);
        NSDictionary *json3 = [NSString jsonStringToDictionary:request3.responseString];
        if([[json3 objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json3 objectForKey:@"data"];
            RecommanderBaseInfoEntity *model = [[RecommanderBaseInfoEntity alloc] initWithJson:data];
            weakSelf.chartId = model.easemobId;
        }else{
            DLog(@"%@",[json3 objectForKey:@"msg"]);
        }
        
        YTKRequest *request4 = batchRequest.requestArray[3];
        NSString *jsonstr = request4.responseString;
        DLog(@"%@",jsonstr);
        NSDictionary *json4 = [NSString jsonStringToDictionary:jsonstr];
        if([[json4 objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json4 objectForKey:@"data"];
            FetchTelNumModel *model = [[FetchTelNumModel alloc] initWithJson:dict];
            if([model.userId integerValue]==0){
                self.tableView.tableHeaderView = self.tabelHeaderView;
            }else{
                weakSelf.wxLabel.text = model.wxId;
                self.phoneNumber = model.phone;
                weakSelf.phoneLabel.text = model.phone;
                [weakSelf.wxLabel updateLayout];
                [weakSelf.phoneLabel updateLayout];
                self.tableView.tableHeaderView = self.tableHeaderView2;
            }
        }
    } failure:^(YTKBatchRequest *batchRequest) {
        YTKRequest *request1 = batchRequest.requestArray[0];
        if(request1.error){
            //DLog(@"api1 error:%@ responsestr = %@",request1.error,request1.responseString);
            if([request1.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效,请重新登录！"];
            }
        }
        YTKRequest *request2 = batchRequest.requestArray[1];
        if(request2.error){
            //DLog(@"api2 error:%@ responsestr = %@",request2.error,request2.responseString);
            if([request2.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效,请重新登录！"];
            }
        }
        YTKRequest *request3 = batchRequest.requestArray[2];
        if(request3.error){
            DLog(@"api3 error:%@ responsestr = %@",request3.error,request3.responseString);
            if([request3.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效,请重新登录！"];
            }
        }
        YTKRequest *request4 = batchRequest.requestArray[3];
        if(request4.error){
            //DLog(@"api3 error:%@ responsestr = %@",request3.error,request3.responseString);
            if([request4.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效,请重新登录！"];
            }
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        CarInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarInfoCell"];
        cell.isOpen = self.isCarServiceOpen;
        if(self.isCarServiceOpen){
            CarDataEntity *model = [self.carDataArray objectAtIndex:indexPath.row];
            cell.model = model;
        }
        return cell;
    }else{
        HotelInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelInfoCell"];
        cell.isOpen = self.isRoomServiceOpen;
        if(self.isRoomServiceOpen){
            RoomSeupListEntity *model = [self.roomDataArray objectAtIndex:indexPath.row];
            cell.model = model;
        }
        cell.showPictureBlock = ^(NSArray * _Nonnull picAry, UIView * _Nonnull picView) {
            CGRect startFrame = [picView convertRect:picView.bounds toView:self.view];
            LKShowPictureView *view = [[LKShowPictureView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
            view.pictureArray = picAry;
            [view show:startFrame];
        };
        cell.navigationBlock = ^(RoomSeupListEntity * _Nonnull model) {
            LKWeakSelf
            NSString *lat = [NSString stringWithFormat:@"%@",model.latitude];
            NSString *lon = [NSString stringWithFormat:@"%@",model.longitude];
            NSArray *endLocation = [NSArray arrayWithObjects:lat,lon,nil];
            [weakSelf doNavigationWithEndLocation:endLocation];
        };
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        if(!self.isCarServiceOpen) return 1;
        return self.carDataArray.count;
    }else if(section==1){
        if(!self.isRoomServiceOpen) return 1;
        return self.roomDataArray.count;
    }else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        if(!self.isCarServiceOpen) return 134;
        return 130;
    }else{
        if(!self.isRoomServiceOpen) return 134;
        return 339;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 46;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==0){
        return _sectionView0;
    }else{
        return _sectionView1;
    }
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}



- (void)contectInLineIsClicked:(id)sender
{
    UserInfo *user = [UserInfo shareInstance];
    if(![user isLogin]){
        LoginViewController *vc = [LoginViewController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    if(self.chartId){
        UserInfo *user = [UserInfo shareInstance];
        if([user isCurrentRecomander]){
            [user switchCurrentStyle];
        }
        EMChatViewController *chatController = [[EMChatViewController alloc] initWithConversationId:self.chartId type:EMConversationTypeChat createIfNotExist:YES];
            chatController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatController animated:YES];
    }else{
        [MBProgressHUD showMessage:@"用户id为空"];
    }
}


- (UIView *)creatSectionView:(NSString *)title{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createBoldLabelWithTitle:title titleColor:HexColor(0x36364D) fontSize:16];
    [view addSubview:label];
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander]){
        LKTipDialog *dlg = [[LKTipDialog alloc] initWithFrame:CGRectMake(0, 0, 270, 181)];
        dlg.message = @"您现在是推荐官身份，暂不支持获取其他推荐官联系方式，切换身份后再获取吧~";
        [dlg show];
        return;
    }
    GetMobileNumberCtrl *vc = [GetMobileNumberCtrl new];
    vc.userId = self.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)createFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 133)];
    view.backgroundColor = [UIColor whiteColor];
    LKGadentButton *btn = [[LKGadentButton alloc] init];
    btn.titleLabel.font = LKSystemFont(18);
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.titleLabel.text = @"在线联系";
    btn.layer.cornerRadius = 20;
    btn.layer.shadowColor = [UIColor colorWithRed:106/255.0 green:172/255.0 blue:248/255.0 alpha:0.57].CGColor;
    btn.layer.shadowOffset = CGSizeMake(0,3);
    btn.layer.shadowOpacity = 1;
    btn.layer.shadowRadius = 20;
    self.contactInLineBtn = btn;
    [btn addTapGestureTarget:self action:@selector(contectInLineIsClicked:)];
    [view addSubview:btn];
    btn.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 37)
    .rightSpaceToView(view, 37)
    .heightIs(40);
    return view;
}

- (UIView *)createHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 351)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"为保障你的合法权益，请勿线下交易。如推荐管要求线下交易，请及时联系我们" titleColor:HexColor(0x999999) fontSize:12];
    label1.numberOfLines = 0;
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"马上联系" titleColor:HexColor(0x36364D) fontSize:16];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = HexColor(0xf8f8f8);
    UILabel *label3 = [Utils createLabelWithTitle:@"联系推荐官" titleColor:HexColor(0x323232) fontSize:18];
    UIImageView *imgv = ImageViewWithImage(@"name_verify");
    UILabel *label4 = [Utils createLabelWithTitle:@"5 元/次" titleColor:HexColor(0xFF9739) fontSize:16];
    [label4 changeStrings:@[@"元/次"] toSize:10];
    UIView *dotv1 = [self dotWithTitleView:@"旅行线路帮你专属规划"];
    UIView *dotv2 = [self dotWithTitleView:@"当地吃喝玩乐攻略讲解"];
    UIView *dotv3 = [self dotWithTitleView:@"获取推荐官真实电话微信"];
    LKGadentButton *btn = [[LKGadentButton alloc] init];
    btn.titleLabel.text = @"获取联系方式";
    btn.titleLabel.font = LKSystemFont(15);
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.layer.cornerRadius = 20;
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    
    [bgView sd_addSubviews:@[label3,imgv,label4,
                             dotv1,
                             dotv2,
                             dotv3,
                             btn]];
    [view sd_addSubviews:@[label1,
                           label2,
                           bgView]];
    label1.sd_layout
    .topSpaceToView(view,14)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    
    label2.sd_layout
    .topSpaceToView(label1, 24)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgView.sd_layout
    .topSpaceToView(label2, 12)
    .leftEqualToView(label2)
    .rightSpaceToView(view, 20)
    .heightIs(215);
    
    label3.sd_layout
    .topSpaceToView(bgView, 17)
    .leftSpaceToView(bgView, 16)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(label3)
    .leftSpaceToView(label3, 10)
    .widthIs(74)
    .heightIs(22);
    
    label4.sd_layout
    .centerYEqualToView(imgv)
    .rightSpaceToView(bgView, 16)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    dotv1.sd_layout
    .topSpaceToView(label3, 14)
    .leftEqualToView(label3)
    .rightSpaceToView(bgView, 16)
    .heightIs(22);
    
    dotv2.sd_layout
    .topSpaceToView(dotv1, 8)
    .leftEqualToView(dotv1)
    .rightEqualToView(dotv1)
    .heightIs(22);
    
    dotv3.sd_layout
    .topSpaceToView(dotv2, 8)
    .rightEqualToView(dotv2)
    .leftEqualToView(dotv2)
    .heightIs(22);
    
    btn.sd_layout
    .topSpaceToView(dotv3, 18)
    .leftEqualToView(dotv3)
    .rightEqualToView(dotv3)
    .heightIs(40);
    
    return view;
}

- (void)phoneIsClicked:(UITapGestureRecognizer *)reg
{
    [Utils callNumber:self.phoneNumber];
}

- (void)copyIsClicked:(UITapGestureRecognizer *)reg
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.wxLabel.text;
    [MBProgressHUD showMessage:@"复制成功"];
}

- (UIView *)createHeaderView2{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 351)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"为保障你的合法权益，请勿线下交易。如推荐官要求线下交易，请及时联系我们" titleColor:HexColor(0x999999) fontSize:12];
    label1.numberOfLines = 0;
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"马上联系" titleColor:HexColor(0x36364D) fontSize:16];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = HexColor(0xf8f8f8);
    UILabel *label3 = [Utils createLabelWithTitle:@"联系推荐官" titleColor:HexColor(0x323232) fontSize:18];
    UIImageView *imgv = ImageViewWithImage(@"name_verify");
    
    
    UILabel *label4 = [Utils createLabelWithTitle:@"他的手机号" titleColor:HexColor(0x333333) fontSize:14];
    UILabel *label5 = [Utils createBoldLabelWithTitle:@"131****3934" titleColor:HexColor(0x333333) fontSize:16];
    self.phoneLabel = label5;
    UIImageView *phoneImgv = ImageViewWithImage(@"phone_icon");
    [phoneImgv addTapGestureTarget:self action:@selector(phoneIsClicked:)];
    UIView *bgView1 = [UIView new];
    bgView1.backgroundColor = [UIColor whiteColor];
    [bgView1 sd_addSubviews:@[label4,label5,phoneImgv]];
    

    UILabel *label6 = [Utils createLabelWithTitle:@"他的微信号" titleColor:HexColor(0x333333) fontSize:14];
    UILabel *label7 = [Utils createBoldLabelWithTitle:@"XS9230" titleColor:HexColor(0x333333) fontSize:16];
    self.wxLabel = label7;
    UILabel *label8 = [Utils createLabelWithTitle:@"复制" titleColor:HexColor(0x6287FE) fontSize:14];
    [label8 addTapGestureTarget:self action:@selector(copyIsClicked:)];
    UIView *bgView2 = [UIView new];
    bgView2.backgroundColor = [UIColor whiteColor];
    [bgView2 sd_addSubviews:@[label6,label7,label8]];

    
    [bgView sd_addSubviews:@[label3,imgv,bgView1,bgView2]];
    [view sd_addSubviews:@[label1,
                           label2,
                           bgView]];
    label1.sd_layout
    .topSpaceToView(view,14)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    
    label2.sd_layout
    .topSpaceToView(label1, 24)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgView.sd_layout
    .topSpaceToView(label2, 12)
    .leftEqualToView(label2)
    .rightSpaceToView(view, 20)
    .heightIs(215);
    
    label3.sd_layout
    .topSpaceToView(bgView, 17)
    .leftSpaceToView(bgView, 16)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imgv.sd_layout
    .centerYEqualToView(label3)
    .leftSpaceToView(label3, 10)
    .widthIs(74)
    .heightIs(22);
    
    bgView1.sd_layout
    .topSpaceToView(label3, 29)
    .leftSpaceToView(bgView, 16)
    .rightSpaceToView(bgView, 16)
    .heightIs(50);
    
    bgView2.sd_layout
    .topSpaceToView(bgView1, 20)
    .leftSpaceToView(bgView, 16)
    .rightSpaceToView(bgView, 16)
    .heightIs(50);
    
    label4.sd_layout
    .centerYEqualToView(bgView1)
    .leftSpaceToView(bgView1, 13)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label5.sd_layout
    .centerYEqualToView(label4)
    .leftSpaceToView(label4, 50)
    .autoHeightRatio(0);
    [label5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    phoneImgv.sd_layout
    .centerYEqualToView(label4)
    .rightSpaceToView(bgView1, 17)
    .widthIs(12.97)
    .heightIs(16);
    
    label6.sd_layout
    .centerYEqualToView(bgView2)
    .leftSpaceToView(bgView2, 13)
    .autoHeightRatio(0);
    [label6 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label7.sd_layout
    .centerYEqualToView(label6)
    .leftSpaceToView(label6, 50)
    .autoHeightRatio(0);
    [label7 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label8.sd_layout
    .centerYEqualToView(bgView2)
    .rightSpaceToView(bgView2, 16)
    .autoHeightRatio(0);
    [label8 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}




- (UIView *)dotWithTitleView:(NSString *)title{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *dot = ImageViewWithImage(@"visual_dot3");
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x999999) fontSize:14];
    [view sd_addSubviews:@[dot,label]];
    dot.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(view)
    .widthIs(7)
    .heightIs(7);
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(dot, 9)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


@end
