//
//  PaySucessCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/10.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PaySucessCtrl.h"
#import "FetchTelNumModel.h"

@interface PaySucessCtrl ()
@property(nonatomic,strong) UILabel *phoneNumberLabel;
@property(nonatomic,strong) UILabel *wxNUmberLabel;

@end

@implementation PaySucessCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"支付结果" titleColor:nil];
    self.view.backgroundColor = HexColor(0xF3F3F3);
    [self layout];
    [self loadData];
}

- (void)loadData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/requirement/getPhone"
                          para:@{@"referrerUserId":self.userId}
                   description:@"获取号码"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            FetchTelNumModel *model = [[FetchTelNumModel alloc] initWithJson:dict];
            weakSelf.phoneNumber = model.phone;
            NSString *numstr = [self.phoneNumber stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.phoneNumberLabel.text = numstr;
                [weakSelf.phoneNumberLabel updateLayout];
            });
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
    
}

- (void)layout{
    UIImageView *imgv = ImageViewWithImage(@"sucess_right");
    UILabel *label = [Utils createBoldLabelWithTitle:@"支付成功" titleColor:HexColor(0x323232) fontSize:20];
    UIView *bottomView = [self makeBottomView];
    [self.view sd_addSubviews:@[imgv,
                                label,
                                bottomView]];
    imgv.sd_layout
    .topSpaceToView(self.view, 56)
    .centerXEqualToView(self.view)
    .widthIs(80)
    .heightIs(80);
    label.sd_layout
    .centerXEqualToView(imgv)
    .topSpaceToView(imgv, 30)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bottomView.sd_layout
    .topSpaceToView(label, 32)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(217);
}

- (void)phoneIsClicked:(UITapGestureRecognizer *)reg
{
    [Utils callNumber:self.phoneNumber];
}

- (UIView *)makeBottomView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 8;
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"推荐官手机号" titleColor:HexColor(0x333333) fontSize:14];
    UIView *bgview1 = [UIView new];
    bgview1.backgroundColor = HexColor(0xF8F9FB);
    NSString *numstr = [self.phoneNumber stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    UILabel *phoneL = [Utils createBoldLabelWithTitle:numstr titleColor:HexColor(0x333333) fontSize:14];
    self.phoneNumberLabel = phoneL;
    UIImageView *phoneImgv = ImageViewWithImage(@"phone_icon");
    [phoneImgv addTapGestureTarget:self action:@selector(phoneIsClicked:)];
    [bgview1 sd_addSubviews:@[phoneL,phoneImgv]];
    
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"推荐官微信号" titleColor:HexColor(0x333333) fontSize:14];
    UIView *bgview2 = [UIView new];
    bgview2.backgroundColor = HexColor(0xF8F9FB);
    UILabel *wxNumberL = [Utils createBoldLabelWithTitle:@"XS9230" titleColor:HexColor(0x333333) fontSize:14];
    self.wxNUmberLabel = wxNumberL;
    UILabel *copyL = [Utils createBoldLabelWithTitle:@"复制" titleColor:HexColor(0x6287FE) fontSize:14];
    [bgview2 sd_addSubviews:@[wxNumberL,copyL]];
    [view sd_addSubviews:@[label1,
                           bgview1,
                           label2,
                           bgview2]];
    label1.sd_layout
    .topSpaceToView(view, 18)
    .leftSpaceToView(view, 20)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgview1.sd_layout
    .topSpaceToView(label1, 10)
    .leftEqualToView(label1)
    .rightSpaceToView(view, 20)
    .heightIs(50);
    
    phoneL.sd_layout
    .centerYEqualToView(bgview1)
    .leftSpaceToView(bgview1, 10)
    .autoHeightRatio(0);
    [phoneL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneImgv.sd_layout
    .centerYEqualToView(bgview1)
    .rightSpaceToView(bgview1, 18)
    .widthIs(13)
    .heightIs(16);
    
    label2.sd_layout
    .topSpaceToView(bgview1, 20)
    .leftEqualToView(bgview1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    bgview2.sd_layout
    .topSpaceToView(label2, 10)
    .leftEqualToView(label2)
    .rightSpaceToView(view, 20)
    .heightIs(50);
    
    wxNumberL.sd_layout
    .centerYEqualToView(bgview2)
    .leftSpaceToView(bgview2, 10)
    .autoHeightRatio(0);
    [wxNumberL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    copyL.sd_layout
    .centerYEqualToView(bgview2)
    .rightSpaceToView(bgview2,15)
    .autoHeightRatio(0);
    [copyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    return view;
}

@end
