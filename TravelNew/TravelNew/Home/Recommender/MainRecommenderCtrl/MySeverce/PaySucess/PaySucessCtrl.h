//
//  PaySucessCtrl.h
//  TravelNew
//
//  Created by mac on 2020/10/10.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaySucessCtrl : LKBaseViewController
@property(nonatomic,strong) NSNumber *userId;
@property(nonatomic,strong) NSString *phoneNumber;
@end

NS_ASSUME_NONNULL_END
