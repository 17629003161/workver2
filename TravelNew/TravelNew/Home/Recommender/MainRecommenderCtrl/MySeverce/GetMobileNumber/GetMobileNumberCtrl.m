//
//  GetMobileNumberCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/10.
//  Copyright © 2020 lester. All rights reserved.
//

#import "GetMobileNumberCtrl.h"
#import "PaySucessCtrl.h"
#import <AlipaySDK/AlipaySDK.h>
#import <WXApi.h>
#import "WXPayDataEntity.h"
#import "WechatManager.h"

@interface GetMobileNumberCtrl ()
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UIView *wxView;
@property(nonatomic,strong) UIView *zfbView;

@end

@implementation GetMobileNumberCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = HexColor(0xf3f3f3);
    [self setTitle:@"获取手机号码" titleColor:nil];
    [self viewLayout];
    self.selectIndex = 0;
}

- (void)payIsFinished:(NSNotification *)notification
{
    PaySucessCtrl *vc = [PaySucessCtrl new];
    vc.userId = self.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(payIsFinished:)
                                                 name:Notification_PayIsFinished
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Notification_PayIsFinished
                                                  object:nil];
}


- (void)viewLayout{
    UILabel *label = [Utils createLabelWithTitle:@"获取手机号码" titleColor:HexColor(0x333333) fontSize:14];
    UIView *topView = [self makeTopView];
    UIView *bootomView = [self createBottomView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"立即支付" font:LKSystemFont(17)
                                   cornerRadius:12.5];
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    [self.view sd_addSubviews:@[label,
                                topView,
                                bootomView,
                                btn]];
    label.sd_layout
    .topSpaceToView(self.view, 15)
    .leftSpaceToView(self.view, 15)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    topView.sd_layout
    .topSpaceToView(self.view, 49)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(140);
    
    bootomView.sd_layout
    .topSpaceToView(self.view, 215)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view);
    
    btn.sd_layout
    .bottomSpaceToView(self.view, 30)
    .leftSpaceToView(self.view,20)
    .rightSpaceToView(self.view,20)
    .heightIs(45);
}


- (UIView *)makeTopView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 4;
    UILabel *pl1 = [self createPromptLabel:@"服务类型"];
    UILabel *pl2 = [self createPromptLabel:@"订单日期"];
    UILabel *pl3 = [self createPromptLabel:@"订单金额"];
    UILabel *cl1 = [self createContentLabel:@"获取联系方式"];
    UILabel *cl2 = [self createContentLabel:@"2020年7月5日"];
    UILabel *cl3 = [Utils createLabelWithTitle:@"5.00元" titleColor:HexColor(0xFF9739) fontSize:14];
    [view sd_addSubviews:@[pl1,cl1,
                           pl2,cl2,
                           pl3,cl3,
                           ]];
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd"];
    cl2.text = [formatter stringFromDate:date];
    
    pl1.sd_layout
    .topSpaceToView(view, 21)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [pl1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cl1.sd_layout
    .centerYEqualToView(pl1)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [cl1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pl2.sd_layout
    .topSpaceToView(pl1, 12)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [pl2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cl2.sd_layout
    .centerYEqualToView(pl2)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [cl2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    pl3.sd_layout
    .topSpaceToView(pl2, 12)
    .leftSpaceToView(view, 16)
    .autoHeightRatio(0);
    [pl3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    cl3.sd_layout
    .centerYEqualToView(pl3)
    .rightSpaceToView(view, 16)
    .autoHeightRatio(0);
    [cl3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (UILabel *)createPromptLabel:(NSString *)title
{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x818181) fontSize:13];
}
- (UILabel *)createContentLabel:(NSString *)title{
    return [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:14];
}

- (UIView *)createFuncViewImage:(NSString *)img title:(NSString *)title{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0x6295fd).CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 2;
    UIImageView *imgv1 = ImageViewWithImage(@"wd_selecet");
    imgv1.tag = 100;
    imgv1.layer.cornerRadius = 2;
    UIImageView *imgv2 = ImageViewWithImage(img);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:HexColor(0x666666) fontSize:15];
    [view sd_addSubviews:@[imgv1,
                           imgv2,label]];
    imgv1.sd_layout
    .topEqualToView(view)
    .leftEqualToView(view)
    .widthIs(22)
    .heightEqualToWidth();
    
    imgv2.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(view, 35)
    .widthIs(24)
    .heightEqualToWidth();
    
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(imgv2, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}



- (void)btnPayIsClicked:(id)sender
{
    if(self.selectIndex==0){
        //支付宝支付
        DLog(@"支付宝支付");
        [self doPayPhoneNumberOrder:self.selectIndex];
    }else if(self.selectIndex==1){
        //微信支付
        DLog(@"微信支付");
        [self doPayPhoneNumberOrder:self.selectIndex];
    }
}


- (void)doPayPhoneNumberOrder:(NSInteger)index{
    LKWeakSelf
    NSArray *ary = @[@"ali",@"wx"];
    Api *api = [Api apiPostUrl:@"work/requirement/getReferrerUserPhoneById"
                          para:@{@"referrerUserId":self.userId,
                                 @"payChannel":ary[self.selectIndex],
                                 @"amount":@(1)
                                 }
                   description:@"获取支付宝OrderInfo"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            if(weakSelf.selectIndex==0){            // 支付宝支付
                NSString *orderString = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
                DLog(@"orderStr ---- = %@",orderString);
                // NOTE: 调用支付结果开始支付
                NSString *appScheme = @"alisdkdemo";
                [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
    //                DLog(@"reslut = %@",resultDic);
                    if ([resultDic[@"resultStatus"]intValue] == 9000) {
                        DLog(@"成功");
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        DLog(@"失败");
                    }
                }];
            }else if(weakSelf.selectIndex==1){      //微信支付
                NSDictionary *dict = [json objectForKey:@"data"];                //得到从服务器返回到订单信息签名串
                WXPayDataEntity *model = [[WXPayDataEntity alloc] initWithJson:dict];
                PayReq *req  = [[PayReq alloc] init];
                 req.partnerId = model.partnerid;
                 req.prepayId = model.prepayid;
                 req.package = model.wx_package;
                 req.nonceStr = model.noncestr;
                 req.timeStamp = (UInt32)[model.timestamp longLongValue];
                 req.sign = model.paySign;
                //调起微信支付
                [WechatManager hangleWechatPayWith:req];
            }

        }else{
           [MBProgressHUD showMessage:@"请求支付数据出错"];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

- (void)wxPayTest{
    if ([WXApi isWXAppInstalled]) {
           DLog(@"已经安装了微信...");
        
    //这里调用后台接口获取订单的详细信息，然后调用微信支付方法
       }else{

       }
}

    #pragma mark 微信支付方法

- (void)WXPayWithAppid:(NSString *)appid partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid package:(NSString *)package noncestr:(NSString *)noncestr timestamp:(NSString *)timestamp sign:(NSString *)sign{
    
    //需要创建这个支付对象
    PayReq *req   = [[PayReq alloc] init];
    //由用户微信号和AppID组成的唯一标识，用于校验微信用户
    req.openID = appid;
    // 商家id，在注册的时候给的
    req.partnerId = partnerid;
    // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
    req.prepayId  = prepayid;
    // 根据财付通文档填写的数据和签名
    req.package  = package;
    // 随机编码，为了防止重复的，在后台生成
    req.nonceStr  = noncestr;
    // 这个是时间戳，也是在后台生成的，为了验证支付的
    NSString * stamp = timestamp;
    req.timeStamp = stamp.intValue;
    // 这个签名也是后台做的
    req.sign = sign;
    [WXApi sendReq:req completion:^(BOOL success) {
        DLog(@"吊起微信成功...");
    }];
}

#pragma mark -支付完成后在后台查询后台支付结果，
- (void)checkOrderPay:(NSNumber *)orderNumber{
    Api *api = [Api apiPostUrl:@"work/alipay/selectIsPaySuccess"
                          para:@{@"out_trade_no":orderNumber
                                 }
                description:@"查询支付宝交易是否成功"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *json = request.responseString;
        DLog(@"json = %@",json);
        NSDictionary *dict = [NSString jsonStringToDictionary:json];
        if([[dict objectForKey:@"code"] integerValue] == 200){
//            if(self.finishPayBlock){
//                self.finishPayBlock(self.model);
//            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showError: [dict objectForKey:@"message"]];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    UIButton *zfbBtn = [_zfbView viewWithTag:100];
    UIButton *wxBtn = [_wxView viewWithTag:100];
    [wxBtn setSelected:NO];
    [zfbBtn setSelected:NO];
    if(selectIndex==0){
        [zfbBtn setSelected:YES];
    }else if(selectIndex==1){
        [wxBtn setSelected:YES];
    }
}

- (void)selectIsClicked:(UITapGestureRecognizer *)reg{
    UIView *view = reg.view;
    if(view == _wxView){
        self.selectIndex = 1;
    }else if(view == _zfbView){
        self.selectIndex = 0;
    }
    DLog(@"self.selectIndex = %ld",self.selectIndex);
}


- (UIView *)createBottomView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 152)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *labe1 = [Utils createLabelWithTitle:@"提现方式"
                                      titleColor:HexColor(0x272727)
                                        fontSize:16];
    UIView *wxView = [self createSwitchView:@"weixinzhif" title:@"微信支付" subTitle:@"亿万用户的选择，更快更安全"];
    self.wxView = wxView;
    UIView *zfbView = [self createSwitchView:@"zfb_logo" title:@"支付宝" subTitle:@"10亿人都在用，真安全，更方便"];
    self.zfbView = zfbView;
    [view sd_addSubviews:@[labe1,
                           zfbView,
                           wxView]];
    labe1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [labe1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbView.sd_layout
    .topSpaceToView(labe1, 15)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    
    wxView.sd_layout
    .topSpaceToView(zfbView, 0)
    .leftEqualToView(view)
    .rightEqualToView(view)
    .heightIs(80);
    [view setupAutoHeightWithBottomView:wxView bottomMargin:0];

    return view;
}

- (UIView *)createSwitchView:(NSString *)imageName title:(NSString *)title subTitle:(NSString *)subTitle{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [view addTapGestureTarget:self action:@selector(selectIsClicked:)];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xE7E7E7);
    UIImageView *imgv = ImageViewWithImage(imageName);
    UILabel *titleLabel = [Utils createLabelWithTitle:title titleColor:HexColor(0x333333) fontSize:15];
    UILabel *subTitleLabel = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btn = [self createSelectBtn];
    btn.tag = 100;
    [view sd_addSubviews:@[line,
                           imgv,
                           titleLabel,subTitleLabel,
                           btn]];
    line.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 20)
    .rightEqualToView(view)
    .heightIs(0.5);
    
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(line)
    .widthIs(40)
    .heightIs(40);
    
    titleLabel.sd_layout
    .topEqualToView(imgv)
    .leftSpaceToView(imgv, 15)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    subTitleLabel.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel, 2)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 20)
    .widthIs(20)
    .heightIs(20);
    [subTitleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    return view;
}

- (UIButton *)createSelectBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_not_select"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"circle_selected"] forState:UIControlStateSelected];
    return btn;
}


@end
