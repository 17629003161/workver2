//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "CarDataEntity.h"

@implementation CarDataEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.name  = [json objectForKey:@"name"];
        self.logo  = [json objectForKey:@"logo"];
        self.carYear  = [json objectForKey:@"carYear"];
        self.numberOfSeats  = [json objectForKey:@"numberOfSeats"];
        self.vehiclePhoto  = [json objectForKey:@"vehiclePhoto"];
        self.carColor  = [json objectForKey:@"carColor"];
        self.carNumble  = [json objectForKey:@"carNumble"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"zx_name"];
    [aCoder encodeObject:self.logo forKey:@"zx_logo"];
    [aCoder encodeObject:self.carYear forKey:@"zx_carYear"];
    [aCoder encodeObject:self.numberOfSeats forKey:@"zx_numberOfSeats"];
    [aCoder encodeObject:self.vehiclePhoto forKey:@"zx_vehiclePhoto"];
    [aCoder encodeObject:self.carColor forKey:@"zx_carColor"];
    [aCoder encodeObject:self.carNumble forKey:@"zx_carNumble"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.name = [aDecoder decodeObjectForKey:@"zx_name"];
        self.logo = [aDecoder decodeObjectForKey:@"zx_logo"];
        self.carYear = [aDecoder decodeObjectForKey:@"zx_carYear"];
        self.numberOfSeats = [aDecoder decodeObjectForKey:@"zx_numberOfSeats"];
        self.vehiclePhoto = [aDecoder decodeObjectForKey:@"zx_vehiclePhoto"];
        self.carColor = [aDecoder decodeObjectForKey:@"zx_carColor"];
        self.carNumble = [aDecoder decodeObjectForKey:@"zx_carNumble"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"name : %@\n",self.name];
    result = [result stringByAppendingFormat:@"logo : %@\n",self.logo];
    result = [result stringByAppendingFormat:@"carYear : %@\n",self.carYear];
    result = [result stringByAppendingFormat:@"numberOfSeats : %@\n",self.numberOfSeats];
    result = [result stringByAppendingFormat:@"vehiclePhoto : %@\n",self.vehiclePhoto];
    result = [result stringByAppendingFormat:@"carColor : %@\n",self.carColor];
    result = [result stringByAppendingFormat:@"carNumble : %@\n",self.carNumble];
    
    return result;
}

@end
