//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface CarDataEntity : JSONModel<NSCoding>

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *carYear;
@property (nonatomic,copy) NSString *numberOfSeats;
@property (nonatomic,copy) NSString *vehiclePhoto;
@property (nonatomic,copy) NSString *carColor;
@property (nonatomic,copy) NSString *carNumble;

-(id)initWithJson:(NSDictionary *)json;


@end
