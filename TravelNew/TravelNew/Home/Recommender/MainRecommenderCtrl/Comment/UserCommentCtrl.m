//
//  UserCommentCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UserCommentCtrl.h"
#import "UserCommentCell.h"

@interface UserCommentCtrl ()

@end

@implementation UserCommentCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[UserCommentCell class] forCellReuseIdentifier:@"UserCommentCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self loadFirstPage];
    [self checkBlankView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.tableView.frame = self.view.bounds;
}


- (Api *)getListApi{
    if(self.userId==nil)
        return nil;
    Api *api = [Api apiPostUrl:@"work/requirementComment/selectLtRequirementCommentByReferrerUserId"
                          para:@{@"userId":self.userId,
                                 @"pageIndex":@(self.pageIndex),
                                 @"pageSize":@(self.pageSize)}
                   description:@"查询用户评价列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(RecommanderCommentEntity.class);
    return name;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCommentCell"];
    RecommanderCommentEntity *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommanderCommentEntity *model = [self.dataArray objectAtIndex:indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[UserCommentCell class] contentViewWidth:kScreenWidth];
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

@end
