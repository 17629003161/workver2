//
//  UserCommentCell.h
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "RecommanderCommentEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserCommentCell : BaseCustomTableViewCell
@property(nonatomic,strong) RecommanderCommentEntity *model;
@end

NS_ASSUME_NONNULL_END
