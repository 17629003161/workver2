//
//  UserCommentCell.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "UserCommentCell.h"
#import <GRStarsView/GRStarsView.h>

@interface UserCommentCell(){
    UIImageView *_imgv;
    UILabel *_nameLabel;
    GRStarsView *_starsView;
    UILabel *_scoreLabel;
    UILabel *_dateLabel;
    UILabel *_commentLabel;
    UIView *_tagView;
}
@property(nonatomic,strong) NSMutableArray *labelArray;
@end

@implementation UserCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 model = userId : 49
 score : 5
 userNickname : YH-332004.73233937204
 creatime : 1605254400000
 ltRequirementInfoId : 55
 teg : 孔雀东南飞，五里一徘徊。
 headerImage : <null>
 commentId : 1
 saywhat : sdad

 */


-(void)setModel:(RecommanderCommentEntity *)model
{
    _model = model;
    if(!isNull(model.headerImage)){
        [_imgv sd_setImageWithURL:[NSURL URLWithString:model.headerImage] placeholderImage:[UIImage imageNamed:@"header_image"]];
    }
    _nameLabel.text = model.userNickname;
    LKDateUtils *date = [LKDateUtils new];
    date.timeInterval = [model.creatime doubleValue]/1000;
    _dateLabel.text = date.dateString;
    _starsView.score = [model.score floatValue];
    _commentLabel.text = model.saywhat;
    NSArray *ary = [model.teg componentsSeparatedByString:@","];
    UILabel *lastLabel;
    for(int i=0; i<4;i++){
        UILabel *label = (UILabel *)self.labelArray[i];
        if(i<ary.count){
            label.text = ary[i];
            lastLabel = label;
            [label setHidden:NO];
        }else{
            [label setHidden:YES];
        }
    }
    [_nameLabel updateLayout];
    [_dateLabel updateLayout];
    [_commentLabel updateLayout];
    [_tagView setupAutoHeightWithBottomView:lastLabel bottomMargin:0];
    [self updateLayout];
}

- (void)configViews{
    _imgv = ImageViewWithImage(@"header_image");
    _nameLabel = [Utils createBoldLabelWithTitle:@"用户43452"
                               titleColor:HexColor(333333)
                                 fontSize:15];
    _dateLabel = [Utils createLabelWithTitle:@"2020-07-05"
                               titleColor:HexColor(0x999999)
                                 fontSize:11];
    _starsView = [[GRStarsView alloc] initWithStarSize:CGSizeMake(15, 15) margin:3 numberOfStars:5];
    _starsView.allowSelect = NO;
    _starsView.score = 4.5;
    
    _scoreLabel = [Utils createLabelWithTitle:@"5.0" titleColor:HexColor(0xFFAA32) fontSize:16];
    _commentLabel = [Utils createLabelWithTitle:@"大唐不夜城真的太好玩啦"
                               titleColor:HexColor(0x666666)
                                 fontSize:14];
    _commentLabel.numberOfLines = 0;
    
    _tagView = [self creatTagView];
    
    [self.contentView sd_addSubviews:@[_imgv,_nameLabel,_dateLabel,
                                       _starsView,_scoreLabel,_commentLabel,
                                       _tagView]];
    _imgv.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(38)
    .heightEqualToWidth();
    
    _nameLabel.sd_layout
    .topEqualToView(_imgv)
    .leftSpaceToView(_imgv, 10)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _dateLabel.sd_layout
    .centerYEqualToView(_nameLabel)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_dateLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _starsView.sd_layout
    .topSpaceToView(_nameLabel, 4)
    .leftEqualToView(_nameLabel)
    .widthIs(87)
    .heightIs(15);
    
    _scoreLabel.sd_layout
    .centerYEqualToView(_starsView)
    .leftSpaceToView(_starsView, 3)
    .autoHeightRatio(0);
    [_scoreLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _commentLabel.sd_layout
    .topSpaceToView(_imgv, 13)
    .leftEqualToView(_imgv)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    
    _tagView.sd_layout
    .topSpaceToView(_commentLabel, 12)
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .heightIs(56);
    
    [self setupAutoHeightWithBottomView:_tagView bottomMargin:21];
}


- (UIView *)creatTagView{
    self.labelArray = [NSMutableArray new];
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, 0, kScreenWidth, 56);
    CGFloat width = 100;
    CGFloat height = 24;
    for (int i=0; i<4; i++) {
        int x = 15 + (i%3) *(width+10);
        int y = (i/3)*(height+8);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        label.font = LKSystemFont(10);
        label.textColor = HexColor(0xFE8A06);
        label.text = @"体验差，服务态度差";
        label.textAlignment = NSTextAlignmentCenter;
        label.layer.backgroundColor = HexColor(0xFFF9F2).CGColor;
        label.layer.borderColor = HexColor(0xFFAD4A).CGColor;
        label.layer.borderWidth = 0.5;
        label.layer.cornerRadius = 2;
        [self.labelArray addObject:label];
    }
    [view sd_addSubviews:self.labelArray];
    [view setupAutoHeightWithBottomView:[self.labelArray lastObject] bottomMargin:0];
    return view;
}

@end
