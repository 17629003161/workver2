//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RecommanderCommentEntity.h"

@implementation RecommanderCommentEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
        self.score  = [json objectForKey:@"score"];
        self.userNickname  = [json objectForKey:@"userNickname"];
        self.creatime  = [json objectForKey:@"creatime"];
        self.ltRequirementInfoId  = [json objectForKey:@"ltRequirementInfoId"];
        self.teg  = [json objectForKey:@"teg"];
        self.headerImage  = [json objectForKey:@"headerImage"];
        self.commentId  = [json objectForKey:@"commentId"];
        self.saywhat  = [json objectForKey:@"saywhat"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
    [aCoder encodeObject:self.score forKey:@"zx_score"];
    [aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
    [aCoder encodeObject:self.creatime forKey:@"zx_creatime"];
    [aCoder encodeObject:self.ltRequirementInfoId forKey:@"zx_ltRequirementInfoId"];
    [aCoder encodeObject:self.teg forKey:@"zx_teg"];
    [aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
    [aCoder encodeObject:self.commentId forKey:@"zx_commentId"];
    [aCoder encodeObject:self.saywhat forKey:@"zx_saywhat"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
        self.score = [aDecoder decodeObjectForKey:@"zx_score"];
        self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
        self.creatime = [aDecoder decodeObjectForKey:@"zx_creatime"];
        self.ltRequirementInfoId = [aDecoder decodeObjectForKey:@"zx_ltRequirementInfoId"];
        self.teg = [aDecoder decodeObjectForKey:@"zx_teg"];
        self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
        self.commentId = [aDecoder decodeObjectForKey:@"zx_commentId"];
        self.saywhat = [aDecoder decodeObjectForKey:@"zx_saywhat"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    result = [result stringByAppendingFormat:@"score : %@\n",self.score];
    result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
    result = [result stringByAppendingFormat:@"creatime : %@\n",self.creatime];
    result = [result stringByAppendingFormat:@"ltRequirementInfoId : %@\n",self.ltRequirementInfoId];
    result = [result stringByAppendingFormat:@"teg : %@\n",self.teg];
    result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
    result = [result stringByAppendingFormat:@"commentId : %@\n",self.commentId];
    result = [result stringByAppendingFormat:@"saywhat : %@\n",self.saywhat];
    
    return result;
}
@end
