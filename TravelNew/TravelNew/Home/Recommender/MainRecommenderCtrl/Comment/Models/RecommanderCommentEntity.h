//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface RecommanderCommentEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSNumber *score;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,strong) NSNumber *creatime;
@property (nonatomic,strong) NSNumber *ltRequirementInfoId;
@property (nonatomic,copy) NSString *teg;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,strong) NSNumber *commentId;
@property (nonatomic,copy) NSString *saywhat;
 


-(id)initWithJson:(NSDictionary *)json;

@end
