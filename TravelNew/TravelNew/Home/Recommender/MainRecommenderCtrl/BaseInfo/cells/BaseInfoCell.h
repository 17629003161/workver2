//
//  BaseInfoCell.h
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseInfoCell : BaseCustomTableViewCell
@property(nonatomic,strong) UILabel *label1;
@property(nonatomic,strong) UILabel *label2;
@end

NS_ASSUME_NONNULL_END
