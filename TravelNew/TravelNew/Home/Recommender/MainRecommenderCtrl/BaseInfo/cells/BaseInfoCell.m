//
//  BaseInfoCell.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseInfoCell.h"

@implementation BaseInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)configViews{
    _label1 = [Utils createLabelWithTitle:@"性别" titleColor:HexColor(0x999999) fontSize:14];
    _label2 = [Utils createLabelWithTitle:@"男" titleColor:HexColor(0x323232) fontSize:14];
    [self.contentView sd_addSubviews:@[_label1,_label2]];
    _label1.sd_layout
    .leftSpaceToView(self.contentView, 18)
    .centerYEqualToView(self.contentView)
    .autoHeightRatio(0);
    [_label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _label2.sd_layout
    .centerYEqualToView(self.contentView)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);
    [_label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}
@end
