//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LtUserInfoPOEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,strong) NSNumber *creatTime;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,strong) NSNumber *userStyle;
@property (nonatomic,strong) NSNumber *sex;
@property (nonatomic,strong) NSNumber *birthTime;
@property (nonatomic,strong) NSNumber *isRealname;
@property (nonatomic,copy) NSString *unionid;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *password;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *aliUserid;
@property (nonatomic,copy) NSString *job;
@property (nonatomic,copy) NSString *bgImage;
@property (nonatomic,copy) NSString *sign;
 


-(id)initWithJson:(NSDictionary *)json;

@end
