//
//
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LtUserInfoPOEntity.h"

@interface RecommanderBaseInfoEntity : NSObject<NSCoding>

@property (nonatomic,strong) NSNumber *style;
@property (nonatomic,strong) NSNumber *referrerLongitude;
@property (nonatomic,strong) LtUserInfoPOEntity *ltUserInfoPO;
@property (nonatomic,copy) NSString *bgImage;
@property (nonatomic,strong) NSNumber *serviceCount;
@property (nonatomic,strong) NSNumber *score;
@property (nonatomic,strong) NSNumber *isDeposit;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,strong) NSNumber *drivingAge;
@property (nonatomic,assign) BOOL attention;
@property (nonatomic,strong) NSNumber *isCar;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,strong) NSNumber *referrerLatitude;
@property (nonatomic,copy) NSString *provideService;
@property (nonatomic,strong) NSNumber *distance;
@property (nonatomic,strong) NSNumber *isInformation;
@property (nonatomic,strong) NSNumber *isHouse;
@property (nonatomic,copy) NSString *easemobId;
@property (nonatomic,copy) NSString *referrerUserAbstract;
@property (nonatomic,strong) NSNumber *userId;

-(id)initWithJson:(NSDictionary *)json;

@end
