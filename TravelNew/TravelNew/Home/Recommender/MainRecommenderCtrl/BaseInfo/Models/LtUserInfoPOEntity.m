//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "LtUserInfoPOEntity.h"

@implementation LtUserInfoPOEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userNickname  = [json objectForKey:@"userNickname"];
		self.creatTime  = [json objectForKey:@"creatTime"];
		self.phone  = [json objectForKey:@"phone"];
		self.userStyle  = [json objectForKey:@"userStyle"];
		self.sex  = [json objectForKey:@"sex"];
		self.birthTime  = [json objectForKey:@"birthTime"];
		self.isRealname  = [json objectForKey:@"isRealname"];
		self.unionid  = [json objectForKey:@"unionid"];
		self.userId  = [json objectForKey:@"userId"];
		self.easemobId  = [json objectForKey:@"easemobId"];
		self.password  = [json objectForKey:@"password"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.address  = [json objectForKey:@"address"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.city  = [json objectForKey:@"city"];
		self.aliUserid  = [json objectForKey:@"aliUserid"];
		self.job  = [json objectForKey:@"job"];
		self.bgImage  = [json objectForKey:@"bgImage"];
		self.sign  = [json objectForKey:@"sign"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.creatTime forKey:@"zx_creatTime"];
	[aCoder encodeObject:self.phone forKey:@"zx_phone"];
	[aCoder encodeObject:self.userStyle forKey:@"zx_userStyle"];
	[aCoder encodeObject:self.sex forKey:@"zx_sex"];
	[aCoder encodeObject:self.birthTime forKey:@"zx_birthTime"];
	[aCoder encodeObject:self.isRealname forKey:@"zx_isRealname"];
	[aCoder encodeObject:self.unionid forKey:@"zx_unionid"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
	[aCoder encodeObject:self.password forKey:@"zx_password"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.address forKey:@"zx_address"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.city forKey:@"zx_city"];
	[aCoder encodeObject:self.aliUserid forKey:@"zx_aliUserid"];
	[aCoder encodeObject:self.job forKey:@"zx_job"];
	[aCoder encodeObject:self.bgImage forKey:@"zx_bgImage"];
	[aCoder encodeObject:self.sign forKey:@"zx_sign"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.creatTime = [aDecoder decodeObjectForKey:@"zx_creatTime"];
		self.phone = [aDecoder decodeObjectForKey:@"zx_phone"];
		self.userStyle = [aDecoder decodeObjectForKey:@"zx_userStyle"];
		self.sex = [aDecoder decodeObjectForKey:@"zx_sex"];
		self.birthTime = [aDecoder decodeObjectForKey:@"zx_birthTime"];
		self.isRealname = [aDecoder decodeObjectForKey:@"zx_isRealname"];
		self.unionid = [aDecoder decodeObjectForKey:@"zx_unionid"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
		self.password = [aDecoder decodeObjectForKey:@"zx_password"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.address = [aDecoder decodeObjectForKey:@"zx_address"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.city = [aDecoder decodeObjectForKey:@"zx_city"];
		self.aliUserid = [aDecoder decodeObjectForKey:@"zx_aliUserid"];
		self.job = [aDecoder decodeObjectForKey:@"zx_job"];
		self.bgImage = [aDecoder decodeObjectForKey:@"zx_bgImage"];
		self.sign = [aDecoder decodeObjectForKey:@"zx_sign"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"creatTime : %@\n",self.creatTime];
	result = [result stringByAppendingFormat:@"phone : %@\n",self.phone];
	result = [result stringByAppendingFormat:@"userStyle : %@\n",self.userStyle];
	result = [result stringByAppendingFormat:@"sex : %@\n",self.sex];
	result = [result stringByAppendingFormat:@"birthTime : %@\n",self.birthTime];
	result = [result stringByAppendingFormat:@"isRealname : %@\n",self.isRealname];
	result = [result stringByAppendingFormat:@"unionid : %@\n",self.unionid];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
	result = [result stringByAppendingFormat:@"password : %@\n",self.password];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"address : %@\n",self.address];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"city : %@\n",self.city];
	result = [result stringByAppendingFormat:@"aliUserid : %@\n",self.aliUserid];
	result = [result stringByAppendingFormat:@"job : %@\n",self.job];
	result = [result stringByAppendingFormat:@"bgImage : %@\n",self.bgImage];
	result = [result stringByAppendingFormat:@"sign : %@\n",self.sign];
	
    return result;
}

@end
