//
//
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RecommanderBaseInfoEntity.h"

@implementation RecommanderBaseInfoEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.style  = [json objectForKey:@"style"];
        self.referrerLongitude  = [json objectForKey:@"referrerLongitude"];
        self.ltUserInfoPO  = [[LtUserInfoPOEntity alloc] initWithJson:[json objectForKey:@"ltUserInfoPO"]];
        self.bgImage  = [json objectForKey:@"bgImage"];
        self.serviceCount  = [json objectForKey:@"serviceCount"];
        self.score  = [json objectForKey:@"score"];
        self.isDeposit  = [json objectForKey:@"isDeposit"];
        self.city  = [json objectForKey:@"city"];
        self.drivingAge  = [json objectForKey:@"drivingAge"];
        self.attention = [[json objectForKey:@"attention"]boolValue];
        self.isCar  = [json objectForKey:@"isCar"];
        self.cityid  = [json objectForKey:@"cityid"];
        self.referrerLatitude  = [json objectForKey:@"referrerLatitude"];
        self.provideService  = [json objectForKey:@"provideService"];
        self.distance  = [json objectForKey:@"distance"];
        self.isInformation  = [json objectForKey:@"isInformation"];
        self.isHouse  = [json objectForKey:@"isHouse"];
        self.easemobId  = [json objectForKey:@"easemobId"];
        self.referrerUserAbstract  = [json objectForKey:@"referrerUserAbstract"];
        self.userId  = [json objectForKey:@"userId"];
        
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.style forKey:@"zx_style"];
    [aCoder encodeObject:self.referrerLongitude forKey:@"zx_referrerLongitude"];
    [aCoder encodeObject:self.ltUserInfoPO forKey:@"zx_ltUserInfoPO"];
    [aCoder encodeObject:self.bgImage forKey:@"zx_bgImage"];
    [aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
    [aCoder encodeObject:self.score forKey:@"zx_score"];
    [aCoder encodeObject:self.isDeposit forKey:@"zx_isDeposit"];
    [aCoder encodeObject:self.city forKey:@"zx_city"];
    [aCoder encodeObject:self.drivingAge forKey:@"zx_drivingAge"];
    [aCoder encodeBool:self.attention forKey:@"zx_attention"];
    [aCoder encodeObject:self.isCar forKey:@"zx_isCar"];
    [aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
    [aCoder encodeObject:self.referrerLatitude forKey:@"zx_referrerLatitude"];
    [aCoder encodeObject:self.provideService forKey:@"zx_provideService"];
    [aCoder encodeObject:self.distance forKey:@"zx_distance"];
    [aCoder encodeObject:self.isInformation forKey:@"zx_isInformation"];
    [aCoder encodeObject:self.isHouse forKey:@"zx_isHouse"];
    [aCoder encodeObject:self.easemobId forKey:@"zx_easemobId"];
    [aCoder encodeObject:self.referrerUserAbstract forKey:@"zx_referrerUserAbstract"];
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.style = [aDecoder decodeObjectForKey:@"zx_style"];
        self.referrerLongitude = [aDecoder decodeObjectForKey:@"zx_referrerLongitude"];
        self.ltUserInfoPO = [aDecoder decodeObjectForKey:@"zx_ltUserInfoPO"];
        self.bgImage = [aDecoder decodeObjectForKey:@"zx_bgImage"];
        self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
        self.score = [aDecoder decodeObjectForKey:@"zx_score"];
        self.isDeposit = [aDecoder decodeObjectForKey:@"zx_isDeposit"];
        self.city = [aDecoder decodeObjectForKey:@"zx_city"];
        self.drivingAge = [aDecoder decodeObjectForKey:@"zx_drivingAge"];
        self.attention = [aDecoder decodeBoolForKey:@"zx_attention"];
    self.isCar = [aDecoder decodeObjectForKey:@"zx_isCar"];
        self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
        self.referrerLatitude = [aDecoder decodeObjectForKey:@"zx_referrerLatitude"];
        self.provideService = [aDecoder decodeObjectForKey:@"zx_provideService"];
        self.distance = [aDecoder decodeObjectForKey:@"zx_distance"];
        self.isInformation = [aDecoder decodeObjectForKey:@"zx_isInformation"];
        self.isHouse = [aDecoder decodeObjectForKey:@"zx_isHouse"];
        self.easemobId = [aDecoder decodeObjectForKey:@"zx_easemobId"];
        self.referrerUserAbstract = [aDecoder decodeObjectForKey:@"zx_referrerUserAbstract"];
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
        
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"style : %@\n",self.style];
    result = [result stringByAppendingFormat:@"referrerLongitude : %@\n",self.referrerLongitude];
    result = [result stringByAppendingFormat:@"ltUserInfoPO : %@\n",self.ltUserInfoPO];
    result = [result stringByAppendingFormat:@"bgImage : %@\n",self.bgImage];
    result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
    result = [result stringByAppendingFormat:@"score : %@\n",self.score];
    result = [result stringByAppendingFormat:@"isDeposit : %@\n",self.isDeposit];
    result = [result stringByAppendingFormat:@"city : %@\n",self.city];
    result = [result stringByAppendingFormat:@"drivingAge : %@\n",self.drivingAge];
    result = [result stringByAppendingFormat:@"attention : %@\n",self.attention?@"yes":@"no"];
    result = [result stringByAppendingFormat:@"isCar : %@\n",self.isCar];
    result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
    result = [result stringByAppendingFormat:@"referrerLatitude : %@\n",self.referrerLatitude];
    result = [result stringByAppendingFormat:@"provideService : %@\n",self.provideService];
    result = [result stringByAppendingFormat:@"distance : %@\n",self.distance];
    result = [result stringByAppendingFormat:@"isInformation : %@\n",self.isInformation];
    result = [result stringByAppendingFormat:@"isHouse : %@\n",self.isHouse];
    result = [result stringByAppendingFormat:@"easemobId : %@\n",self.easemobId];
    result = [result stringByAppendingFormat:@"referrerUserAbstract : %@\n",self.referrerUserAbstract];
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
    
    return result;
}

@end
