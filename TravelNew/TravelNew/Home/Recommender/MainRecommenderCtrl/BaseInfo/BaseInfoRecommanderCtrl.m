//
//  BaseInfoRecommanderCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/8.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseInfoRecommanderCtrl.h"
#import "BaseInfoCell.h"
#import "RecommanderBaseInfoEntity.h"

@interface BaseInfoRecommanderCtrl (){
    UIView *_footerView;
}
@property(nonatomic,strong) NSArray *promptAry;
@property(nonatomic,strong) UILabel *signLabel;

@end

@implementation BaseInfoRecommanderCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"user id = %ld",[self.userId integerValue]);
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[BaseInfoCell class] forCellReuseIdentifier:@"BaseInfoCell"];
    self.promptAry = @[@"性别",@"出生年月",@"入驻时间",@"驾龄",@"服务城市"];
    self.dataArray = [NSMutableArray arrayWithArray:@[@"男",@"1993年8月",@"2020年8月",@"3年",@"西安"]];
    [self requreData];
}

- (void)loadFirstPage{
    [super loadFirstPage];
    [self requreData];
}

- (void)requreData{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUser_block1A" para:@{@"userId":self.userId} description:@"获取推荐官详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.dataArray removeAllObjects];
        DLog(@"jsonstr = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            RecommanderBaseInfoEntity *model = [[RecommanderBaseInfoEntity alloc] initWithJson:data];
            LtUserInfoPOEntity *user = model.ltUserInfoPO;
            DLog(@"-------> user =   %@",user);
            NSString *sex =[Utils sexToString:user.sex];
            [self.dataArray addObject:sex];
            LKDateUtils *date = [LKDateUtils new];
            date.timeInterval = [user.birthTime doubleValue] /1000;
            [self.dataArray addObject:date.dateString];
            date.timeInterval = [user.creatTime doubleValue] /1000;
            [self.dataArray addObject:date.dateString];
            NSString *str = [NSString  stringWithFormat:@"%ld年",[model.drivingAge integerValue]];
            [self.dataArray addObject:str];
            [self.dataArray addObject:model.city];
            self.signLabel.text = user.sign;
            [self.tableView reloadData];
            [self.signLabel updateLayout];
        }else{
            DLog(@"%@",[json objectForKey:@"msg"]);
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [self requreData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BaseInfoCell"];
    cell.label1.text = self.promptAry[indexPath.row];
    cell.label2.text = self.dataArray[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(!_footerView){
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        UILabel *titleL = [Utils createLabelWithTitle:@"关于我" titleColor:HexColor(0x333333) fontSize:15];
        if(!_signLabel){
            UILabel *label = [Utils createLabelWithTitle:@"西安本地人，热爱旅游喜欢交朋友，西安本地人，热爱旅游喜欢交朋友，西安本地人，热爱旅游喜欢交朋友，西安本地人，热爱旅游喜欢交朋友。"
                                       titleColor:HexColor(0x666666)
                                         fontSize:14] ;
            label.numberOfLines = 0;
            self.signLabel = label;
        }
        [view sd_addSubviews:@[titleL,_signLabel]];
        titleL.sd_layout
        .topSpaceToView(view, 34)
        .leftSpaceToView(view, 20)
        .rightSpaceToView(view, 20)
        .autoHeightRatio(0);
    
        _signLabel.sd_layout
        .topSpaceToView(titleL, 10)
        .leftEqualToView(titleL)
        .rightSpaceToView(view, 20)
        .autoHeightRatio(0);
        _footerView = view;
    }
    return _footerView;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 240;
}



@end
