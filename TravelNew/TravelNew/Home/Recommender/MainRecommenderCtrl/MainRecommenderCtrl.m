//
//  MainRecommenderCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MainRecommenderCtrl.h"
#import "BaseInfoRecommanderCtrl.h"
#import "MyServericeRecommenderCtrl.h"
#import "UserCommentCtrl.h"
#import <GRStarsView/GRStarsView.h>
#import "RecommanderBaseInfoEntity.h"

@interface MainRecommenderCtrl (){
    CGRect _menuBarFrame;
    CGRect _subViewFrame;
    CGFloat _offsety;
}
@property(nonatomic,strong) NSMutableArray *menuArray;
@property(nonatomic,strong) NSMutableArray *subViewArray;
@property(nonatomic,strong) BaseInfoRecommanderCtrl *basevc;
@property(nonatomic,strong) MyServericeRecommenderCtrl *servicevc;
@property(nonatomic,strong) UserCommentCtrl *commentvc;
@property(nonatomic,strong) GRStarsView *starsView;
@property(nonatomic,strong) UIImageView *bgImageView;

@property(nonatomic,strong) UIImageView *headImageview;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *scoreLabel;
@property(nonatomic,strong) UILabel *serviceCountLabel;
@property(nonatomic,strong) UILabel *securityLabel;

@property(nonatomic,assign) BOOL isFollow;
@property(nonatomic,strong) LKGadentButton *btnFollow;

@end

@implementation MainRecommenderCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DLog(@"userId = %ld",[self.userId integerValue]);
    _offsety = 0;
    [self layout];
    [self addBackArrow];
    [self requreData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (UIView *)viewWithTitle:(NSString *)title andIcon:(NSString *)name
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *imgv = ImageViewWithImage(name);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:[UIColor whiteColor] fontSize:11];
    label.tag = 1000;
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .centerYEqualToView(view)
    .leftEqualToView(view)
    .widthIs(12)
    .heightIs(12);
    label.sd_layout
    .centerYEqualToView(view)
    .leftSpaceToView(imgv, 2)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


- (void)requreData{
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUser_block1A"
                          para:@{@"userId":self.userId}
                   description:@"获取推荐官详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"jsonstr = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *data = [json objectForKey:@"data"];
            RecommanderBaseInfoEntity *model = [[RecommanderBaseInfoEntity alloc] initWithJson:data];
            LtUserInfoPOEntity *user = model.ltUserInfoPO;
            DLog(@"-------> user =   %@",user);
            self.isFollow = model.attention;
            [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:model.bgImage] placeholderImage:[UIImage imageNamed:@"bg_recommender"]];
            [self.headImageview sd_setImageWithURL:[NSURL URLWithString:user.headerImage]];
            self.nameLabel.text = user.userNickname;
            [self.starsView setScore:[model.score floatValue]];
            self.scoreLabel.text = [NSString stringWithFormat:@"%.1lf分",[model.score floatValue]];
            self.serviceCountLabel.text = [NSString stringWithFormat:@"服务人次·%ld",[model.serviceCount integerValue]];
            BOOL isSure = [model.isDeposit boolValue];
            if(isSure){
                self.securityLabel.text = @"已缴纳保障金";
            }else{
                self.securityLabel.text = @"未缴纳保障金";
            }
            [self.nameLabel updateLayout];
            [self.scoreLabel updateLayout];
            [self.serviceCountLabel updateLayout];
            [self.securityLabel updateLayout];
        }else{
            DLog(@"%@",[json objectForKey:@"msg"]);
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@,response = %@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)noteIsClicked:(id)sender
{
    [self AddFollow:self.userId];
}

-(void)setIsFollow:(BOOL)isFollow
{
    _isFollow = isFollow;
    if(_isFollow){
        self.btnFollow.titleLabel.text = @"已关注";
    }else{
        self.btnFollow.titleLabel.text = @"关注TA";
    }
}

- (void)AddFollow:(NSNumber *)userId{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"work/attention/attentionUser"
                          para:@{@"userId":userId,
                                 @"isAttention":@(_isFollow)
                          }
                   description:@"推荐官关注"];
    api.withToken = YES;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            BOOL isFollow = [[json objectForKey:@"data"] boolValue];
            weakSelf.isFollow = isFollow;
        }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)reverseFollow{
    self.isFollow = !_isFollow;
}

- (void)layout{
    UIImageView *bgimgv = ImageViewWithImage(@"bg_recommender");
    self.bgImageView = bgimgv;
    [bgimgv setUserInteractionEnabled:YES];
    LKGadentButton *noteBtn = [LKGadentButton blueButtonWhithTitle:@"关注TA"
                                               font:LKSystemFont(13)
                                       cornerRadius:15];
    self.btnFollow = noteBtn;
    [noteBtn addTapGestureTarget:self action:@selector(noteIsClicked:)];
    UIImageView *headImgv = ImageViewWithImage(@"header_image");
    self.headImageview = headImgv;
    headImgv.layer.cornerRadius = 30.0;
    headImgv.layer.masksToBounds = YES;
    GRStarsView *starsView = [[GRStarsView alloc] initWithStarSize:CGSizeMake(13, 13) margin:1 numberOfStars:5];
    self.starsView = starsView;
    starsView.allowSelect = NO;
    starsView.score = 4.5;
    self.starsView = starsView;
    
   // UIImageView *starImgv = ImageViewWithImage(@"star_5");
    UILabel *scoreL = [Utils createLabelWithTitle:@"4.2分" titleColor:HexColor(0xFFAE3E) fontSize:13];
    self.scoreLabel = scoreL;
    UILabel *nameL = [Utils createLabelWithTitle:@"SyrenaCC" titleColor:[UIColor whiteColor] fontSize:17];
    self.nameLabel = nameL;
    UIView *view1 = [self viewWithTitle:@"真实身份验证" andIcon:@"huiyuanguanli"];
    UIView *view2 = [self viewWithTitle:@"服务人次" andIcon:@"-handshake"];
    self.serviceCountLabel = [view2 viewWithTag:1000];
    UIView *view3 = [self viewWithTitle:@"未缴纳保障金" andIcon:@"suifangguanli"];
    self.securityLabel = [view3 viewWithTag:1000];

    UIView *maskView = [UIView new];
    maskView.backgroundColor = [UIColor blackColor];
    maskView.alpha = 0.3;

    [bgimgv sd_addSubviews:@[maskView,headImgv,noteBtn,
                             nameL,starsView,scoreL,
                             view1,view2,view3]];
    [self.view sd_addSubviews:@[bgimgv]];
    
    
    bgimgv.sd_layout
    .leftEqualToView(self.view)
    .topEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(409);
    
    maskView.sd_layout
    .topSpaceToView(bgimgv, 140)
    .leftEqualToView(headImgv)
    .rightEqualToView(bgimgv)
    .bottomEqualToView(bgimgv);
    
    headImgv.sd_layout
    .topSpaceToView(bgimgv, 110)
    .leftSpaceToView(bgimgv, 20)
    .widthIs(60)
    .heightIs(60);
    
    [headImgv makeRectCorner:UIRectCornerTopLeft cornerSize:CGSizeMake(23, 23) bounds:CGRectMake(0, 0, kScreenWidth-20, 144)];
    
    noteBtn.sd_layout
    .centerYEqualToView(headImgv)
    .rightSpaceToView(bgimgv, 18)
    .widthIs(68)
    .heightIs(24);
    
    nameL.sd_layout
    .topSpaceToView(headImgv, 5)
    .leftSpaceToView(bgimgv, 34)
    .autoHeightRatio(0);
    [nameL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
//    starImgv.sd_layout
//    .centerYEqualToView(nameL)
//    .leftSpaceToView(nameL, 12)
//    .widthIs(69)
//    .heightIs(13);
    
    starsView.sd_layout
    .centerYEqualToView(nameL)
    .leftSpaceToView(nameL, 12)
    .widthIs(69)
    .heightIs(13);
    
    scoreL.sd_layout
    .centerYEqualToView(starsView)
    .leftSpaceToView(starsView, 6)
    .autoHeightRatio(0);
    [scoreL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    CGFloat width = (kScreenWidth-68)/3.0;
    view1.sd_layout
    .topSpaceToView(nameL, 11)
    .leftEqualToView(nameL)
    .widthIs(width)
    .heightIs(16);
    
    view2.sd_layout
    .centerYEqualToView(view1)
    .leftSpaceToView(view1, 0)
    .widthIs(width)
    .heightIs(16);
    
    view3.sd_layout
    .centerYEqualToView(view2)
    .leftSpaceToView(view2, 0)
    .widthIs(width)
    .heightIs(16);
    
    
    UIView *menubar = [self createMenuBar];
    [self.view addSubview:menubar];
    menubar.frame = CGRectMake(0, 263, kScreenWidth, 46);
    [menubar makeRectCorner:UIRectCornerTopLeft cornerSize:CGSizeMake(23, 23) bounds:CGRectMake(0, 0, kScreenWidth, 46)];
    _menuBarFrame = menubar.frame;
    
    _subViewArray = [NSMutableArray new];
    _basevc = [BaseInfoRecommanderCtrl new];
    _basevc.userId = self.userId;
    _servicevc = [MyServericeRecommenderCtrl new];
    _servicevc.userId = self.userId;
    _commentvc = [UserCommentCtrl new];
    _commentvc.userId = self.userId;
    [self addChildViewController:_basevc];
    [self addChildViewController:_servicevc];
    [self addChildViewController:_commentvc];
    [self.view sd_addSubviews:@[ _basevc.view,
                                 _servicevc.view,
                                   _commentvc.view]];
    [_subViewArray addObjectsFromArray:@[
                                            _servicevc.view,
                                            _basevc.view,
                                         _commentvc.view]];
    CGFloat maxY = CGRectGetMaxY(menubar.frame);
    CGRect frame = CGRectMake(0, maxY, kScreenWidth, kScreenHeight-maxY);
    _subViewFrame = frame;
    _servicevc.view.frame = frame;
    _basevc.view.frame = frame;
    _commentvc.view.frame = frame;
    
    _basevc.view.hidden = YES;
    _commentvc.view.hidden = YES;
    
}

// 设置一排固定间距自动宽度子view
- (UIView *)createMenuBar
{
    _menuArray = [NSMutableArray new];
    UIView *containerView = [UIView new];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.borderColor = HexColor(0xe6e6e6).CGColor;
    containerView.layer.borderWidth =0.5;
    CGFloat gap = 10;
    CGFloat with = (kScreenWidth-gap*2)/4.0;
    NSArray *titleAry = @[@"我的服务",@"基本信息",@"用户评价"];
    for (int i = 0; i < titleAry.count; i++) {
        UILabel *label = [Utils createBoldLabelWithTitle:titleAry[i]
                                           titleColor:HexColor(0x5e5e71)
                                             fontSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = i;
        [label addTapGestureTarget:self action:@selector(menuIsClicked:)];
        [_menuArray addObject:label];
        [containerView addSubview:label];
        label.sd_layout
        .centerYEqualToView(containerView)
        .leftSpaceToView(containerView, gap + i*with)
        .widthIs(with)
        .heightRatioToView(containerView, 0.8);
    }
    UILabel *label0 = _menuArray[0];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0x1E69FF);
    [containerView addSubview:line];
    line.sd_layout
    .centerXEqualToView(label0)
    .bottomEqualToView(containerView)
    .widthIs(56)
    .heightIs(2);
    [_menuArray addObject:line];
    return containerView;
}

- (void)menuIsClicked:(UITapGestureRecognizer *)reg{
    UIView *line = [_menuArray lastObject];
    for(int i=0;i<_menuArray.count-1;i++){
        UILabel *temp = _menuArray[i];
        temp.textColor = HexColor(0x5e5e71);
        UIView *view = _subViewArray[i];
        view.hidden = YES;
    }
    UILabel *label = (UILabel *)reg.view;
    label.textColor = HexColor(0x36364D);
    [UIView animateWithDuration:0.1 animations:^{
            line.centerX = label.centerX;
        }];
    NSInteger selected = label.tag;
    UIView *view = _subViewArray[selected];
    view.hidden = NO;
    DLog(@"selected = %ld",selected);
}



@end
