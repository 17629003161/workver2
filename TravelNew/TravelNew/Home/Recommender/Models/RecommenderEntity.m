//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RecommenderEntity.h"

@implementation RecommenderEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userId  = [json objectForKey:@"userId"];
		self.userNickname  = [json objectForKey:@"userNickname"];
		self.score  = [json objectForKey:@"score"];
		self.distance  = [json objectForKey:@"distance"];
		self.services  = [json objectForKey:@"services"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.referrerLatitude  = [json objectForKey:@"referrerLatitude"];
		self.serviceCount  = [json objectForKey:@"serviceCount"];
		self.sign  = [json objectForKey:@"sign"];
		self.referrerLongitude  = [json objectForKey:@"referrerLongitude"];
		self.appraiseCount  = [json objectForKey:@"appraiseCount"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.score forKey:@"zx_score"];
	[aCoder encodeObject:self.distance forKey:@"zx_distance"];
	[aCoder encodeObject:self.services forKey:@"zx_services"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.referrerLatitude forKey:@"zx_referrerLatitude"];
	[aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
	[aCoder encodeObject:self.sign forKey:@"zx_sign"];
	[aCoder encodeObject:self.referrerLongitude forKey:@"zx_referrerLongitude"];
	[aCoder encodeObject:self.appraiseCount forKey:@"zx_appraiseCount"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.score = [aDecoder decodeObjectForKey:@"zx_score"];
		self.distance = [aDecoder decodeObjectForKey:@"zx_distance"];
		self.services = [aDecoder decodeObjectForKey:@"zx_services"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.referrerLatitude = [aDecoder decodeObjectForKey:@"zx_referrerLatitude"];
		self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
		self.sign = [aDecoder decodeObjectForKey:@"zx_sign"];
		self.referrerLongitude = [aDecoder decodeObjectForKey:@"zx_referrerLongitude"];
		self.appraiseCount = [aDecoder decodeObjectForKey:@"zx_appraiseCount"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"score : %@\n",self.score];
	result = [result stringByAppendingFormat:@"distance : %@\n",self.distance];
	result = [result stringByAppendingFormat:@"services : %@\n",self.services];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"referrerLatitude : %@\n",self.referrerLatitude];
	result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
	result = [result stringByAppendingFormat:@"sign : %@\n",self.sign];
	result = [result stringByAppendingFormat:@"referrerLongitude : %@\n",self.referrerLongitude];
	result = [result stringByAppendingFormat:@"appraiseCount : %@\n",self.appraiseCount];
	
    return result;
}

@end
