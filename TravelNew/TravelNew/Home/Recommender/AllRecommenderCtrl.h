//
//  AllRecommenderCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AllRecommenderCtrl : LKBaseTableViewController
@property(nonatomic,strong) NSString *cityCode;
@property(nonatomic,assign) CLLocationCoordinate2D coord;
@end

NS_ASSUME_NONNULL_END
