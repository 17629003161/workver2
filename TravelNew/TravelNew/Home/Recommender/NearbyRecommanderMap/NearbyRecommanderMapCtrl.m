//
//  NearbyRecommanderMapCtrl.m
//  TravelNew
//
//  Created by mac on 2020/10/7.
//  Copyright © 2020 lester. All rights reserved.
//

#import "NearbyRecommanderMapCtrl.h"
#import "HotRecommenderCell.h"
#import "MainRecommenderCtrl.h"
#import "RecommanderMapEntity.h"
#import "CustomAnnotationView.h"
#import "MyPointAnnotation.h"
#import "MainRecommenderCtrl.h"
#import "RecommanderMapListEntity.h"

@interface NearbyRecommanderMapCtrl ()<MAMapViewDelegate>
@property(nonatomic,assign) BOOL isMapMode;
@property(nonatomic,strong) NSMutableArray *annonceArry;
@property(nonatomic,strong) UIImageView *btnImageView;

@end

@implementation NearbyRecommanderMapCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.annonceArry = [NSMutableArray new];
    [self layout];
    self.isMapMode = YES;
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    UserInfo *user = [UserInfo shareInstance];
    LKLocationManager *manager = [LKLocationManager sharedManager];
    if(manager.cityCode==nil)
        return nil;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUsersList"
                          para:@{@"cityid":user.cityid,
                                 @"lat":@(manager.coordinate.latitude),
                                 @"lon":@(manager.coordinate.longitude),
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"获取附近的推荐官列表"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(RecommanderMapListEntity.class);
    return name;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate = nil;
}

- (void)layout{
    self.tableView.backgroundColor = [UIColor colorWithWhite:1.0f alpha: 0.8];
    self.tableView.hidden = YES;
    self.isMapMode = YES;
    self.mapView.frame = self.view.bounds;
    [self.view addSubview:self.mapView];
    [self.view sendSubviewToBack:self.mapView];
    [self.tableView registerClass:[HotRecommenderCell class] forCellReuseIdentifier:@"HotRecommenderCell"];
    self.tableView.sd_layout
    .topSpaceToView(self.view, 80);
    
    
    UIImageView *imgv1 = ImageViewWithImage(@"return");
    [imgv1 addTapGestureTarget:self action:@selector(goBackIsClicked:)];
    UIImageView *imgv2 = ImageViewWithImage(@"map");
    self.btnImageView = imgv2;
    [imgv2 addTapGestureTarget:self action:@selector(mapIsClicked:)];
    [self.view sd_addSubviews:@[imgv1,imgv2]];
    imgv1.sd_layout
    .topSpaceToView(self.view, 37)
    .leftSpaceToView(self.view, 15)
    .widthIs(30)
    .heightIs(30);
    imgv2.sd_layout
    .rightSpaceToView(self.view, 19)
    .centerYEqualToView(imgv1)
    .widthIs(60)
    .heightIs(30);
}

- (void)rangeChangeTo:(CLLocationCoordinate2D) centerCoordinate{
    // 当前屏幕中心点的经纬度
    double centerLongitude = self.mapView.region.center.longitude;
    double centerLatitude = self.mapView.region.center.latitude;
    //当前屏幕显示范围的经纬度
    CLLocationDegrees pointssLongitudeDelta = self.mapView.region.span.longitudeDelta;
    CLLocationDegrees pointssLatitudeDelta = self.mapView.region.span.latitudeDelta;
    //左下角
   // double leftBottomLong = centerLongitude - pointssLongitudeDelta/2.0;
  //  double leftBottomLat = centerLatitude - pointssLatitudeDelta/2.0;
    //右下角
    double rightBottomLong = centerLongitude + pointssLongitudeDelta/2.0;
    double rightBottomLat = centerLatitude - pointssLatitudeDelta/2.0;
    //左上角
    double leftTopLong = centerLongitude - pointssLongitudeDelta/2.0;
    double leftToplat = centerLatitude + pointssLatitudeDelta/2.0;
    UserInfo *user = [UserInfo shareInstance];
    if(isNull(user.cityid)) return;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUsers"
                          para:@{@"cityid":user.cityid,
                                 @"ltlat":@(leftToplat),
                                 @"ltlon":@(leftTopLong),
                                 @"rblat":@(rightBottomLat),
                                 @"rblon":@(rightBottomLong)}
                              description:@"user-查询视区内推荐官"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
       // DLog(@"jsonstr = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSArray *ary = [json objectForKey:@"data"];
            [self.mapView removeAnnotations:self.annonceArry];
            [self.annonceArry removeAllObjects];
            for(NSDictionary *dict in ary){
                RecommanderMapEntity *model = [[RecommanderMapEntity alloc] initWithJson:dict];
                MyPointAnnotation *annotation = [[MyPointAnnotation alloc] init];
                annotation.coordinate = CLLocationCoordinate2DMake([model.referrerLatitude doubleValue], [model.referrerLongitude doubleValue]);
                annotation.title = @"";
                annotation.model = model;
                [self.mapView addAnnotation:annotation];
            }
        }

        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
    }];
}

- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    MACoordinateRegion region;
    CLLocationCoordinate2D centerCoordinate = self.mapView.region.center;
    region.center= centerCoordinate;
    [self rangeChangeTo:centerCoordinate];
    //_pointAnnotation.coordinate = centerCoordinate;
    DLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
//    [self setLocationWithLatitude:centerCoordinate.latitude AndLongitude:centerCoordinate.longitude];
   
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAUserLocation class]]) {
        MAUserLocationRepresentation *r = [[MAUserLocationRepresentation alloc] init];
        r.showsAccuracyRing = NO;///精度圈是否显示，默认YES
        [self.mapView updateUserLocationRepresentation:r];
        
        static NSString *userLocationStyleReuseIndetifier = @"userLocationStyleReuseIndetifier";
        MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
        if (annotationView == nil) {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"address_icon3"];
        return annotationView;
    }
    if ([annotation isKindOfClass:[MyPointAnnotation class]])
    {
           MyPointAnnotation *ann = (MyPointAnnotation *)annotation;
            static NSString *reuseIndetifier = @"userAnnotationReuseIndetifier";
                CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
                
                if (annotationView == nil)
                {
                    annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
                }
            // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        if(isNull(ann.model.headerImage))
            ann.model.headerImage = @"";
        [annotationView setImageWithURL:[NSURL URLWithString:ann.model.headerImage] placeholderImage:[UIImage imageWithColor:HexColor(0xE3E3E3)]];
        [annotationView addTapGestureTarget:self action:@selector(markIsClicked:)];
//            [annotationView setImageWithURL:[NSURL URLWithString:ann.model.headerImage] placeholderImage:[UIImage imageWithColor:HexColor(0xE3E3E3)]];
            [annotationView setTitle:@""];
            [annotationView setTitle:nil];
                //没有排队
                // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
                annotationView.centerOffset = CGPointMake(0, -18);
                
                return annotationView;
    }
    
    return nil;
}

- (void)markIsClicked:(UITapGestureRecognizer *)reg
{
    CustomAnnotationView *view = (CustomAnnotationView *)reg.view;
    MyPointAnnotation *ann = (MyPointAnnotation *)view.annotation;
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = ann.model.userId;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)goBackIsClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setIsMapMode:(BOOL)isMapMode
{
    _isMapMode = isMapMode;
    if(self.isMapMode){
        self.tableView.hidden = YES;
        self.btnImageView.image = [UIImage imageNamed:@"maplist"];
    }else{
        self.tableView.hidden = NO;
        self.btnImageView.image = [UIImage imageNamed:@"map"];
    }
}

- (void)mapIsClicked:(UITapGestureRecognizer *)reg{
    self.isMapMode = !self.isMapMode;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 168;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HotRecommenderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HotRecommenderCell" forIndexPath:indexPath];
    RecommanderMapListEntity *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 11)];
   // view.backgroundColor = [UIColor colorWithHex:0xF3F3F3];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 11;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommanderMapListEntity *model = self.dataArray[indexPath.row];
    MainRecommenderCtrl *vc = [[MainRecommenderCtrl alloc] init];
    vc.userId = model.userId;
    [self.navigationController pushViewController:vc animated:YES];
}




@end
