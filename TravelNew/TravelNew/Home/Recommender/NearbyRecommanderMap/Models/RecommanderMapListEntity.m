//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RecommanderMapListEntity.h"

@implementation RecommanderMapListEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.userNickname  = [json objectForKey:@"userNickname"];
		self.isCar  = [json objectForKey:@"isCar"];
		self.isHouse  = [json objectForKey:@"isHouse"];
		self.appraiseCount  = [json objectForKey:@"appraiseCount"];
		self.referrerLatitude  = [json objectForKey:@"referrerLatitude"];
		self.userId  = [json objectForKey:@"userId"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.serviceCount  = [json objectForKey:@"serviceCount"];
		self.isInformation  = [json objectForKey:@"isInformation"];
		self.services  = [json objectForKey:@"services"];
		self.referrerLongitude  = [json objectForKey:@"referrerLongitude"];
		self.score  = [json objectForKey:@"score"];
		self.distance  = [json objectForKey:@"distance"];
		self.sign  = [json objectForKey:@"sign"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.isCar forKey:@"zx_isCar"];
	[aCoder encodeObject:self.isHouse forKey:@"zx_isHouse"];
	[aCoder encodeObject:self.appraiseCount forKey:@"zx_appraiseCount"];
	[aCoder encodeObject:self.referrerLatitude forKey:@"zx_referrerLatitude"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.serviceCount forKey:@"zx_serviceCount"];
	[aCoder encodeObject:self.isInformation forKey:@"zx_isInformation"];
	[aCoder encodeObject:self.services forKey:@"zx_services"];
	[aCoder encodeObject:self.referrerLongitude forKey:@"zx_referrerLongitude"];
	[aCoder encodeObject:self.score forKey:@"zx_score"];
	[aCoder encodeObject:self.distance forKey:@"zx_distance"];
	[aCoder encodeObject:self.sign forKey:@"zx_sign"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.isCar = [aDecoder decodeObjectForKey:@"zx_isCar"];
		self.isHouse = [aDecoder decodeObjectForKey:@"zx_isHouse"];
		self.appraiseCount = [aDecoder decodeObjectForKey:@"zx_appraiseCount"];
		self.referrerLatitude = [aDecoder decodeObjectForKey:@"zx_referrerLatitude"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.serviceCount = [aDecoder decodeObjectForKey:@"zx_serviceCount"];
		self.isInformation = [aDecoder decodeObjectForKey:@"zx_isInformation"];
		self.services = [aDecoder decodeObjectForKey:@"zx_services"];
		self.referrerLongitude = [aDecoder decodeObjectForKey:@"zx_referrerLongitude"];
		self.score = [aDecoder decodeObjectForKey:@"zx_score"];
		self.distance = [aDecoder decodeObjectForKey:@"zx_distance"];
		self.sign = [aDecoder decodeObjectForKey:@"zx_sign"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"isCar : %@\n",self.isCar];
	result = [result stringByAppendingFormat:@"isHouse : %@\n",self.isHouse];
	result = [result stringByAppendingFormat:@"appraiseCount : %@\n",self.appraiseCount];
	result = [result stringByAppendingFormat:@"referrerLatitude : %@\n",self.referrerLatitude];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"serviceCount : %@\n",self.serviceCount];
	result = [result stringByAppendingFormat:@"isInformation : %@\n",self.isInformation];
	result = [result stringByAppendingFormat:@"services : %@\n",self.services];
	result = [result stringByAppendingFormat:@"referrerLongitude : %@\n",self.referrerLongitude];
	result = [result stringByAppendingFormat:@"score : %@\n",self.score];
	result = [result stringByAppendingFormat:@"distance : %@\n",self.distance];
	result = [result stringByAppendingFormat:@"sign : %@\n",self.sign];
	
    return result;
}

@end
