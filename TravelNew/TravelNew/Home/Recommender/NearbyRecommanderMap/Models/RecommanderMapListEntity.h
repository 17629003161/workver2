//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface RecommanderMapListEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,strong) NSNumber *isCar;
@property (nonatomic,strong) NSNumber *isHouse;
@property (nonatomic,strong) NSNumber *appraiseCount;
@property (nonatomic,strong) NSNumber *referrerLatitude;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,strong) NSNumber *serviceCount;
@property (nonatomic,strong) NSNumber *isInformation;
@property (nonatomic,copy) NSString *services;
@property (nonatomic,strong) NSNumber *referrerLongitude;
@property (nonatomic,strong) NSNumber *score;
@property (nonatomic,strong) NSNumber *distance;
@property (nonatomic,copy) NSString *sign;
 


-(id)initWithJson:(NSDictionary *)json;

@end
