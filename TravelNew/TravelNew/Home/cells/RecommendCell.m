//
//  RecommendCell.m
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RecommendCell.h"

@interface RecommendCell(){
    UILabel *_label1;
    UILabel *_label2;
    UIView *_activeView;
    
}
@property (strong, nonatomic) UIImageView *imgView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *signLabel;
@end

@implementation RecommendCell

- (void)configView{
    self.contentView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.masksToBounds = YES;
    UIImageView *bgImgv = ImageViewWithImage(@"gl_rect");
    _label1 = [Utils createBoldLabelWithTitle:@"SyrenaCC" titleColor:HexColor(0xffffff) fontSize:10];
    self.nameLabel = _label1;
    _label2 = [Utils createLabelWithTitle:@"西安一日游，就来预约我" titleColor:HexColor(0xffffff) fontSize:8];
    self.signLabel = _label2;
    _activeView = [UIView new];
    _activeView.backgroundColor = [UIColor whiteColor];
    _activeView.layer.cornerRadius = 4.0;
    UIView *greenView = [UIView new];
    greenView.backgroundColor = HexColor(0x53DC0D);
    greenView.layer.cornerRadius = 2.0;
    [_activeView addSubview:greenView];
    
    [self.contentView addSubview:self.imgView];
    [self.contentView sd_addSubviews:@[bgImgv,_label1,_label2,_activeView]];
    
    _imgView.image = [UIImage imageNamed:@"home2"];
    _imgView.sd_layout
    .leftEqualToView(self.contentView)
    .topEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView);
    
    bgImgv.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .heightIs(30);
    
    _label1.sd_layout
    .leftSpaceToView(self.contentView, 8)
    .bottomSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _label2.sd_layout
    .leftEqualToView(_label1)
    .topSpaceToView(_label1, 1)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(10);
    
    _activeView.sd_layout
    .rightSpaceToView(self.contentView, 8)
    .bottomSpaceToView(self.contentView, 8)
    .widthIs(8)
    .heightIs(8);
    
    greenView.sd_layout
    .centerXEqualToView(_activeView)
    .centerYEqualToView(_activeView)
    .widthIs(4)
    .heightIs(4);
}

- (void)setActive:(BOOL)active{
    if(active){
        [_activeView setHidden:NO];
    }else{
        [_activeView setHidden:YES];
    }
}

- (UIImageView *)imgView{
    if(!_imgView){
        _imgView = [[UIImageView alloc] init];
    }
    return _imgView;
}

- (void)setModel:(RecommenderEntity *)model{
    _model = model;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.headerImage] placeholderImage:[UIImage imageNamed:@"bg_recommender"]];
    self.nameLabel.text = model.userNickname;
    self.signLabel.text = model.sign;
    [self.nameLabel updateLayout];
    [self.signLabel updateLayout];
}

@end
