//
//  BastWayCell.m
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BastWayCell.h"

@interface BastWayCell(){
    UIImageView *_imgv;
    UILabel *_addrL;
    UILabel *_moneyL;
    UILabel *_dayTimeL;
    UILabel *_placeL;
    UIImageView *_headerImgv;
    UILabel *_nameL;
    UIImageView *_locationImgv;
    UILabel *_locationL;
}

@end

@implementation BastWayCell

-(void)setModel:(LineDataEntity *)model
{
    _model = model;
    [_imgv sd_setImageWithURL:[NSURL URLWithString:model.lineImage]];
    _addrL.text = model.travelName;
    _moneyL.text = [NSString stringWithFormat:@"￥@%.0lf元起",[model.price doubleValue]];
    _dayTimeL.text = [NSString stringWithFormat:@"%ld天",[model.travelDay integerValue]];
    [_dayTimeL changeStrings:@[@"天"] toColor:HexColor(0x999999)];
    [_dayTimeL changeStrings:@[@"天"] toSize:10];
    _placeL.text = [NSString stringWithFormat:@"%ld地",[model.ltScenicNum integerValue]];
    [_placeL changeStrings:@[@"地"] toColor:HexColor(0x999999)];
    [_placeL changeStrings:@[@"地"] toSize:10];
    [_headerImgv sd_setImageWithURL:[NSURL URLWithString:model.headerImage]];
    _nameL.text = model.userNickname;
    _locationL.text = [NSString stringWithFormat:@"%@⋅%@",model.city,model.scenicName];
    [_addrL updateLayout];
    [_moneyL updateLayout];
    [_dayTimeL updateLayout];
    [_placeL updateLayout];
    [_nameL updateLayout];
    [_locationL updateLayout];
}

- (void)configView{
    self.contentView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.shadowColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:0.5].CGColor;
    self.contentView.layer.shadowOffset = CGSizeMake(0,2);
    self.contentView.layer.shadowOpacity = 1;
    self.contentView.layer.shadowRadius = 4;
    
    _imgv = ImageViewWithImage(@"home3");
    [_imgv makeTopAngleSize:CGSizeMake(4, 4) bounds:CGRectMake(0, 0, self.contentView.width, 147)];
    _addrL = [Utils createLabelWithTitle:@"前年古城之梦回长安" titleColor:HexColor(0x323232) fontSize:16];
    _moneyL = [Utils createLabelWithTitle:@"￥300元起" titleColor:HexColor(0xFE2E2E) fontSize:14];
    _moneyL.hidden = YES;
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xFFB12C);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xFFB12C);
    _dayTimeL = [Utils createLabelWithTitle:@"2天" titleColor:HexColor(0x323232) fontSize:16];
    [_dayTimeL changeStrings:@[@"天"] toColor:HexColor(0x999999)];
    [_dayTimeL changeStrings:@[@"天"] toSize:10];
    _placeL = [Utils createLabelWithTitle:@"6地" titleColor:HexColor(0x323232) fontSize:16];
    [_placeL changeStrings:@[@"地"] toColor:HexColor(0x999999)];
    [_placeL changeStrings:@[@"地"] toSize:10];
    _headerImgv = ImageViewWithImage(@"default_header");
    _headerImgv.layer.cornerRadius = 10.0;
    _headerImgv.layer.masksToBounds = YES;
    _nameL = [Utils createLabelWithTitle:@"旅咖出行" titleColor:HexColor(0x414243) fontSize:10];
    _locationImgv = ImageViewWithImage(@"");
    _locationL = [Utils createLabelWithTitle:@"西安.大雁塔" titleColor:HexColor(0x959393) fontSize:10];
    
    [self.contentView sd_addSubviews:@[_imgv,
                                       _addrL,_moneyL,
                                       line1,_dayTimeL,line2,_placeL,
                                       _headerImgv,_nameL,_locationImgv,_locationL]];
    _imgv.sd_layout
    .leftSpaceToView(self.contentView, 0)
    .topSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .heightIs(147);
    
    _addrL.sd_layout
    .topSpaceToView(self.contentView, 157)
    .leftSpaceToView(self.contentView, 14)
    .autoHeightRatio(0);
    [_addrL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _moneyL.sd_layout
    .centerYEqualToView(_addrL)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_moneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line1.sd_layout
    .topSpaceToView(self.contentView, 187)
    .leftEqualToView(_addrL)
    .widthIs(2)
    .heightIs(12);
    
    _dayTimeL.sd_layout
    .centerYEqualToView(line1)
    .leftSpaceToView(line1, 6)
    .autoHeightRatio(0);
    [_dayTimeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line2.sd_layout
    .centerYEqualToView(line1)
    .leftSpaceToView(_dayTimeL, 10)
    .widthIs(2)
    .heightIs(12);
    
    _placeL.sd_layout
    .centerYEqualToView(line2)
    .leftSpaceToView(line2, 6)
    .autoHeightRatio(0);
    [_placeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _headerImgv.sd_layout
    .topSpaceToView(_dayTimeL, 6)
    .leftEqualToView(line1)
    .widthIs(20)
    .heightIs(20);
    
    _nameL.sd_layout
    .leftSpaceToView(_headerImgv, 5)
    .centerYEqualToView(_headerImgv)
    .autoHeightRatio(0);
    [_nameL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _locationL.sd_layout
    .centerYEqualToView(_nameL)
    .rightSpaceToView(self.contentView, 14)
    .autoHeightRatio(0);
    [_locationL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _locationImgv.sd_layout
    .centerYEqualToView(_locationL)
    .rightSpaceToView(_locationL, 1)
    .widthIs(10)
    .heightIs(13);
    
}

@end
