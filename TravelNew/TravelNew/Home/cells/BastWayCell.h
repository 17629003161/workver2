//
//  BastWayCell.h
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomCollectionCell.h"
#import "LineDataEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface BastWayCell : BaseCustomCollectionCell
@property(nonatomic,strong) LineDataEntity *model;
@end

NS_ASSUME_NONNULL_END
