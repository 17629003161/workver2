//
//  RecommendCell.h
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomCollectionCell.h"
#import "RecommenderEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecommendCell : BaseCustomCollectionCell
@property(nonatomic,strong) RecommenderEntity *model;
- (void)setActive:(BOOL)active;
@end

NS_ASSUME_NONNULL_END
