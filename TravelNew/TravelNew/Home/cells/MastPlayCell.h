//
//  MastPlayCell.h
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomCollectionCell.h"
#import "ScenicSpotEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MastPlayCell : BaseCustomCollectionCell
@property(nonatomic,strong) ScenicSpotEntity *model;
@end

NS_ASSUME_NONNULL_END
