//
//  MastPlayCell.m
//  TravelNew
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 lester. All rights reserved.
//

#import "MastPlayCell.h"

@interface MastPlayCell(){
    UIImageView *_imgv;
    UIImageView *_addrImgv;
    UILabel *_addrL;
    UILabel *_descL;
    NSMutableArray *_headerImageArray;
    UILabel *_headdescL;
}
@end

@implementation MastPlayCell

- (void)setModel:(ScenicSpotEntity *)model
{
    _model = model;
    [_imgv sd_setImageWithURL:[NSURL URLWithString:model.scenicImage]];
    _addrL.text = [NSString stringWithFormat:@"%@⋅%@",model.city,model.scenicName];
    _descL.text = model.scenicAbstract;
    for(int i=0;i<_headerImageArray.count;i++){
        UIImageView *imgv = _headerImageArray[i];
        imgv.hidden = NO;
        if(i<model.ltRefUserInfos.count){
            LtRefUserInfosEntity *entity = model.ltRefUserInfos[i];
            if(!isNull(entity.headerImage)){
                [imgv sd_setImageWithURL:[NSURL URLWithString:entity.headerImage]];
            }
        }else{
            imgv.hidden = YES;
        }
    }
    if(!isNull(model.ltRefUserNum)){
        _headdescL.text = [NSString stringWithFormat:@"%ld名推荐官服务",[model.ltRefUserNum integerValue]];
    }
    [_addrL updateLayout];
    [_descL updateLayout];
    [_headdescL updateLayout];
}

- (void)configView{
    self.contentView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.shadowColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:0.5].CGColor;
    self.contentView.layer.shadowOffset = CGSizeMake(0,2);
    self.contentView.layer.shadowOpacity = 1;
    self.contentView.layer.shadowRadius = 4;
    
    _headerImageArray = [NSMutableArray new];
    _imgv = ImageViewWithImage(@"home1");
    [_imgv makeTopAngleSize:CGSizeMake(4, 4) bounds:CGRectMake(0, 0, self.contentView.width, 107)];
    _addrImgv = ImageViewWithImage(@"location_home");
    _addrL = [Utils createLabelWithTitle:@"西安.大雁塔"
                              titleColor:HexColor(0x959393)
                                fontSize:10];
    _descL = [Utils createLabelWithTitle:@"玄奘为保存由天蓝经丝绸之路带回长安的经书修。。。"
                              titleColor:HexColor(0x414243)
                                fontSize:13];
    _descL.numberOfLines = 0;
    for(int i=0;i<5;i++){
        [_headerImageArray addObject:ImageViewWithImage(@"person_header")];
    }
    _headdescL = [Utils createLabelWithTitle:@"0名推荐官服务" titleColor:HexColor(0x999999) fontSize:10];
    _headdescL.textAlignment = NSTextAlignmentRight;
    [self.contentView sd_addSubviews:@[
                                       _imgv,
                                       _addrImgv,_addrL,
                                       _descL,
                                       _headdescL]];
    [self.contentView sd_addSubviews:_headerImageArray];
    
    _imgv.sd_layout
    .leftEqualToView(self.contentView)
    .topEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .heightIs(107);
    
    _addrImgv.sd_layout
    .topSpaceToView(_imgv, 8)
    .leftSpaceToView(self.contentView, 6)
    .widthIs(9.82)
    .heightIs(12.55);
    
    _addrL.sd_layout
    .centerYEqualToView(_addrImgv)
    .leftSpaceToView(_addrImgv, 5)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    
    _descL.sd_layout
    .topSpaceToView(_addrImgv, 9)
    .leftEqualToView(_addrImgv)
    .rightSpaceToView(self.contentView, 6)
    .heightIs(36);
    
    for(int i=0;i<_headerImageArray.count;i++){
        UIImageView *imgv =(UIImageView *)[_headerImageArray objectAtIndex:i];
        imgv.layer.cornerRadius = 10.0;
        imgv.layer.masksToBounds = YES;
        imgv.sd_layout
        .leftSpaceToView(self.contentView, 4+i*4)
        .topSpaceToView(_descL, 8)
        .widthIs(20)
        .heightIs(20);
    }
    _headdescL.sd_layout
    .topSpaceToView(_descL, 11)
    .leftSpaceToView(self.contentView, 52)
    .rightSpaceToView(self.contentView, 6)
    .autoHeightRatio(0);
    
    //[self setupAutoHeightWithBottomViewsArray:_headerImageArray bottomMargin:12];
    
}


@end
