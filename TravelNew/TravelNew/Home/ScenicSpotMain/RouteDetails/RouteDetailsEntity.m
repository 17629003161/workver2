//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "RouteDetailsEntity.h"

@implementation RouteDetailsEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"ltScenicUsers":@"LtScenicUsersEntity",};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.lineImage  = [json objectForKey:@"lineImage"];
		self.abstracts  = [json objectForKey:@"abstracts"];
		self.travelName  = [json objectForKey:@"travelName"];
		self.referrerUserId  = [json objectForKey:@"referrerUserId"];
		self.travellineId  = [json objectForKey:@"travellineId"];
		self.ltScenicUsers = [NSMutableArray array];
	for(NSDictionary *item in [json objectForKey:@"ltScenicUsers"])
		{
[self.ltScenicUsers addObject:[[LtScenicUsersEntity alloc] initWithJson:item]];
	}
self.travelDay  = [json objectForKey:@"travelDay"];
		self.carId  = [json objectForKey:@"carId"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.ltRefUserInfo  = [[LtRefUserInfoEntity alloc] initWithJson:[json objectForKey:@"ltRefUserInfo"]];
		self.hotVar  = [json objectForKey:@"hotVar"];
		self.price  = [json objectForKey:@"price"];
		self.ltScenicNum  = [json objectForKey:@"ltScenicNum"];
		self.city  = [json objectForKey:@"city"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.lineImage forKey:@"zx_lineImage"];
	[aCoder encodeObject:self.abstracts forKey:@"zx_abstracts"];
	[aCoder encodeObject:self.travelName forKey:@"zx_travelName"];
	[aCoder encodeObject:self.referrerUserId forKey:@"zx_referrerUserId"];
	[aCoder encodeObject:self.travellineId forKey:@"zx_travellineId"];
	[aCoder encodeObject:self.ltScenicUsers forKey:@"zx_ltScenicUsers"];
	[aCoder encodeObject:self.travelDay forKey:@"zx_travelDay"];
	[aCoder encodeObject:self.carId forKey:@"zx_carId"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.ltRefUserInfo forKey:@"zx_ltRefUserInfo"];
	[aCoder encodeObject:self.hotVar forKey:@"zx_hotVar"];
	[aCoder encodeObject:self.price forKey:@"zx_price"];
	[aCoder encodeObject:self.ltScenicNum forKey:@"zx_ltScenicNum"];
	[aCoder encodeObject:self.city forKey:@"zx_city"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.lineImage = [aDecoder decodeObjectForKey:@"zx_lineImage"];
		self.abstracts = [aDecoder decodeObjectForKey:@"zx_abstracts"];
		self.travelName = [aDecoder decodeObjectForKey:@"zx_travelName"];
		self.referrerUserId = [aDecoder decodeObjectForKey:@"zx_referrerUserId"];
		self.travellineId = [aDecoder decodeObjectForKey:@"zx_travellineId"];
		self.ltScenicUsers = [aDecoder decodeObjectForKey:@"zx_ltScenicUsers"];
		self.travelDay = [aDecoder decodeObjectForKey:@"zx_travelDay"];
		self.carId = [aDecoder decodeObjectForKey:@"zx_carId"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.ltRefUserInfo = [aDecoder decodeObjectForKey:@"zx_ltRefUserInfo"];
		self.hotVar = [aDecoder decodeObjectForKey:@"zx_hotVar"];
		self.price = [aDecoder decodeObjectForKey:@"zx_price"];
		self.ltScenicNum = [aDecoder decodeObjectForKey:@"zx_ltScenicNum"];
		self.city = [aDecoder decodeObjectForKey:@"zx_city"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"lineImage : %@\n",self.lineImage];
	result = [result stringByAppendingFormat:@"abstracts : %@\n",self.abstracts];
	result = [result stringByAppendingFormat:@"travelName : %@\n",self.travelName];
	result = [result stringByAppendingFormat:@"referrerUserId : %@\n",self.referrerUserId];
	result = [result stringByAppendingFormat:@"travellineId : %@\n",self.travellineId];
	result = [result stringByAppendingFormat:@"ltScenicUsers : %@\n",self.ltScenicUsers];
	result = [result stringByAppendingFormat:@"travelDay : %@\n",self.travelDay];
	result = [result stringByAppendingFormat:@"carId : %@\n",self.carId];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"ltRefUserInfo : %@\n",self.ltRefUserInfo];
	result = [result stringByAppendingFormat:@"hotVar : %@\n",self.hotVar];
	result = [result stringByAppendingFormat:@"price : %@\n",self.price];
	result = [result stringByAppendingFormat:@"ltScenicNum : %@\n",self.ltScenicNum];
	result = [result stringByAppendingFormat:@"city : %@\n",self.city];
	
    return result;
}

@end
