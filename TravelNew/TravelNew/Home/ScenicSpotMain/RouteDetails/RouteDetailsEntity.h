//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LtScenicUsersEntity.h"
#import "LtRefUserInfoEntity.h"

@interface RouteDetailsEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *lineImage;
@property (nonatomic,copy) NSString *abstracts;
@property (nonatomic,copy) NSString *travelName;
@property (nonatomic,copy) NSString *referrerUserId;
@property (nonatomic,copy) NSString *travellineId;
@property (nonatomic,strong) NSMutableArray *ltScenicUsers;
@property (nonatomic,strong) NSNumber *travelDay;
@property (nonatomic,copy) NSString *carId;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,strong) LtRefUserInfoEntity *ltRefUserInfo;
@property (nonatomic,strong) NSNumber *hotVar;
@property (nonatomic,strong) NSNumber *price;
@property (nonatomic,strong) NSNumber *ltScenicNum;
@property (nonatomic,copy) NSString *city;
 


-(id)initWithJson:(NSDictionary *)json;

@end
