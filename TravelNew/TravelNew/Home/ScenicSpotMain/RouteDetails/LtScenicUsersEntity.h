//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LtScenicUsersEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *ltRefUserInfos;
@property (nonatomic,copy) NSString *scenicAbstract;
@property (nonatomic,copy) NSString *scenicImage;
@property (nonatomic,strong) NSNumber *longitude;
@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *ltRefUserNum;
@property (nonatomic,strong) NSNumber *price;
@property (nonatomic,strong) NSNumber *hotVar;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *scenicId;
@property (nonatomic,copy) NSString *scenicName;
@property (nonatomic,strong) NSNumber *sequence;
 


-(id)initWithJson:(NSDictionary *)json;

@end
