//
//  RouteDetailsCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "ScenicSpotListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface RouteDetailsCtrl : LKBaseTableViewController
@property (nonatomic,copy) NSString *travellineId;
@property (nonatomic,assign) CLLocationCoordinate2D centerCoordinate;
@end

NS_ASSUME_NONNULL_END
