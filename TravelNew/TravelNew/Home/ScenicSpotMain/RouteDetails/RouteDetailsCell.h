//
//  RouteDetailsCell.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LtScenicUsersEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface RouteDetailsCell : UITableViewCell
@property(nonatomic,strong) LtScenicUsersEntity *model;
- (void)showLine:(BOOL)isShow;
@end

NS_ASSUME_NONNULL_END
