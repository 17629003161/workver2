//
//  RouteDetailsCell.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RouteDetailsCell.h"

@interface RouteDetailsCell()
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UILabel *numberLabel;
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UIView *line;

@end

@implementation RouteDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(LtScenicUsersEntity *)model
{
    _model = model;
    [self.imgv sd_setImageWithURL:[NSURL URLWithString:model.scenicImage]];
    self.nameLabel.text = model.scenicName;
    self.descLabel.text = model.scenicAbstract;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",[model.sequence integerValue]];
    [self.nameLabel updateLayout];
    [self.numberLabel updateLayout];
    [self.descLabel updateLayout];
}

- (void)showLine:(BOOL)isShow{
    self.line.hidden = !isShow;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self configViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)configViews{
    UILabel *numberL = [Utils createLabelWithTitle:@"1" titleColor:HexColor(0xffffff) fontSize:13];
    numberL.backgroundColor = HexColor(0x2F3438);
    numberL.font = [UIFont boldSystemFontOfSize:13];
    numberL.textAlignment = NSTextAlignmentCenter;
    numberL.layer.cornerRadius = 9;
    numberL.layer.masksToBounds = YES;
    self.numberLabel = numberL;
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHex:0xe6e6e6];
    self.line = line;
    UILabel *titleL = [Utils createLabelWithTitle:@"大兴善寺庙" titleColor:HexColor(0x333333) fontSize:15];
    titleL.font = [UIFont boldSystemFontOfSize:15];
    self.nameLabel = titleL;
    UILabel *descL = [Utils createLabelWithTitle:@"大兴善寺，“佛教八宗”之一“密宗”祖庭，是隋唐皇家寺院，帝都长安三大译经场之一，位于长安城东靖善坊内（今西安市小寨兴善寺西街）。《长安志》卷七载：“寺殿崇广，为京城之最。”" titleColor:HexColor(0x666666) fontSize:13];
    descL.numberOfLines = 0;
    self.descLabel = descL;
    UIImageView *imgv = ImageViewWithImage(@"home2");
    imgv.layer.cornerRadius = 4.0;
    imgv.layer.masksToBounds = YES;
    self.imgv = imgv;
    [self.contentView sd_addSubviews:@[line,
                                       numberL,titleL,
                                       descL,
                                       imgv]];
    line.sd_layout
    .leftSpaceToView(self.contentView, 32)
    .topEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .widthIs(2);
    
    numberL.sd_layout
    .centerXEqualToView(line)
    .topEqualToView(self.contentView)
    .widthIs(18)
    .heightIs(18);
    
    titleL.sd_layout
    .centerYEqualToView(numberL)
    .leftSpaceToView(numberL, 2)
    .rightSpaceToView(self.contentView, 20)
    .autoHeightRatio(0);
    
    descL.sd_layout
    .leftEqualToView(titleL)
    .topSpaceToView(titleL, 8)
    .rightSpaceToView(self.contentView, 20)
    .heightIs(72);
    
    imgv.sd_layout
    .leftEqualToView(descL)
    .topSpaceToView(descL, 10)
    .rightEqualToView(descL)
    .heightIs(230);
}

@end
