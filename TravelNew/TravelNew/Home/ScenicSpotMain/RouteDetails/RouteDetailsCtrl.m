//
//  RouteDetailsCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "RouteDetailsCtrl.h"
#import "RouteDetailsCell.h"
#import "ScenicSportDescriptionCtrl.h"
#import "ScenicSpotMain.h"
#import "RouteDetailsEntity.h"

@interface RouteDetailsCtrl ()<MAMapViewDelegate>
@property (nonatomic, strong) MAPolyline *commonPoly;
@property(nonatomic,assign) CGFloat lastOffSetY;
@property(nonatomic,assign) CGRect orgFrame;

@property(nonatomic,strong) UIImageView *scenicSpotImgv;
@property(nonatomic,strong) UILabel *travelName;
@property(nonatomic,strong) UIImageView *headImgv;
@property(nonatomic,strong) UILabel *recommanderName;
@property(nonatomic,strong) UILabel *dayspaceLabel2;
@property(nonatomic,strong) UILabel *dayLabel;
@property(nonatomic,strong) UILabel *placeNumberLabel;

@end

@implementation RouteDetailsCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self layout];
    [self addBackArrow];
    [self requestData];
    self.mapView.centerCoordinate = self.centerCoordinate;
    [self disabelPageRefresh];
}


- (void)requestData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"attractions/line/selectLineInfoByLineId"
                          para:@{@"travellineId":self.travellineId}
                   description:@"根据travellineId 查询路线详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"jsonstr = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            DLog(@"dict = %@",dict);
            RouteDetailsEntity *model = [[RouteDetailsEntity alloc] initWithJson:dict];
            LtRefUserInfoEntity *userInfo = model.ltRefUserInfo;
            NSMutableArray *lines = model.ltScenicUsers;
            [weakSelf.dataArray removeAllObjects];
            [weakSelf.dataArray addObjectsFromArray:lines];
            [weakSelf.tableView reloadData];
            [weakSelf createBrokenLine];                //地图划线
            [weakSelf addPointAnnotation];              //添加标记点
            DLog(@"\nentity = %@",model);
            [weakSelf.scenicSpotImgv sd_setImageWithURL:[NSURL URLWithString:model.lineImage]];
            weakSelf.travelName.text = model.travelName;
            [weakSelf.headImgv sd_setImageWithURL:[NSURL URLWithString:userInfo.headerImage]];
            weakSelf.recommanderName.text = userInfo.userNickname;
            weakSelf.dayLabel.text = [NSString stringWithFormat:@"%ld天",[model.travelDay integerValue]];
            [weakSelf.dayLabel changeStrings:@[@"天"] toSize:10];
            weakSelf.placeNumberLabel.text = [NSString stringWithFormat:@"%ld地",[model.ltScenicNum integerValue]];
            [weakSelf.placeNumberLabel changeStrings:@[@"地"] toSize:10];
            weakSelf.dayspaceLabel2.text = [NSString stringWithFormat:@"共%ld天行程  %ld个景点",[model.travelDay integerValue],[model.ltScenicNum integerValue]];
            [weakSelf.travelName updateLayout];
            [weakSelf.recommanderName updateLayout];
            [weakSelf.dayspaceLabel2 updateLayout];
            [weakSelf.dayLabel updateLayout];
            [weakSelf.placeNumberLabel updateLayout];
            
        }
        DLog(@"aaa");
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}

- (void)layout{
    UIImageView *topImgv = ImageViewWithImage(@"home1");
    self.scenicSpotImgv = topImgv;
    [topImgv setUserInteractionEnabled:YES];
    topImgv.frame = CGRectMake(0, 0, kScreenWidth, 187+20);
    [self.view addSubview:topImgv];
    [self.view sendSubviewToBack:topImgv];
    
    [self.tableView registerClass:[RouteDetailsCell class] forCellReuseIdentifier:@"RouteDetailsCell"];
    CGRect rect = self.view.frame;
    self.tableView.backgroundColor = [UIColor clearColor];
    rect.origin.y = 187;
    rect.size.height -= 187;
    self.tableView.frame = rect;
    self.orgFrame = self.tableView.frame;
    UIView *headerView = [self makeHeaderView];
    self.tableView.tableHeaderView = headerView;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20, 20)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = headerView.bounds;
    maskLayer.path = maskPath.CGPath;
    headerView.layer.mask = maskLayer;
}

- (void)backArrowIsClicked:(UITapGestureRecognizer *)reg{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addPointAnnotation{
    [self.mapView removeAnnotations:self.mapView.annotations];
    for (int i=0; i<self.dataArray.count; i++) {
        LtScenicUsersEntity *model = self.dataArray[i];
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = CLLocationCoordinate2DMake([model.latitude doubleValue], [model.longitude doubleValue]);
        pointAnnotation.title = model.scenicName;
        if(i==0){
            pointAnnotation.subtitle = @"起点";
        }else if(i==self.dataArray.count-1){
            pointAnnotation.subtitle = @"终点";
        }
        [self.mapView addAnnotation:pointAnnotation];
    }
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        MAPinAnnotationView*annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil)
        {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.canShowCallout= YES;       //设置气泡可以弹出，默认为NO
        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
        annotationView.draggable = YES;        //设置标注可以拖动，默认为NO
        annotationView.pinColor = MAPinAnnotationColorPurple;
        return annotationView;
    }
    return nil;
}

- (void)createBrokenLine
{
    [self.mapView removeOverlay:_commonPoly];
    CLLocationCoordinate2D commonPolyLineCoords[self.dataArray.count];
    for (int i =0; i < self.dataArray.count; i ++) {
        LtScenicUsersEntity *model = self.dataArray[i];
        commonPolyLineCoords[i].longitude = [model.longitude doubleValue];
        commonPolyLineCoords[i].latitude = [model.latitude doubleValue];
        if(i==0){
            self.mapView.centerCoordinate = CLLocationCoordinate2DMake([model.latitude doubleValue], [model.longitude doubleValue]);
        }
    }
    //构造折线对象
     _commonPoly = [MAPolyline polylineWithCoordinates:commonPolyLineCoords count:self.dataArray.count];
    //在地图上添加折线对象
    [self.mapView addOverlay:_commonPoly];
}
- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineRenderer *polylineRenderer = [[MAPolylineRenderer alloc] initWithPolyline:overlay];
        
        polylineRenderer.lineWidth    = 3.f;
        polylineRenderer.strokeColor  = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.6];
        polylineRenderer.lineJoinType = kMALineJoinRound;
        polylineRenderer.lineCapType  = kMALineCapRound;
        
        return polylineRenderer;
    }
    return nil;
}

- (UIView *)makeHeaderView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 431)];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *label1 = [Utils createLabelWithTitle:@"西安市文化新知文艺浪漫2日之旅" titleColor:HexColor(0x000001) fontSize:20];
    label1.font = [UIFont boldSystemFontOfSize:20];
    self.travelName = label1;
    UIImageView *imgv = ImageViewWithImage(@"default_header");
    imgv.layer.cornerRadius = 15;
    imgv.layer.masksToBounds = YES;
    self.headImgv = imgv;
    UILabel *label2 = [Utils createLabelWithTitle:@"官方发布" titleColor:HexColor(0x999999) fontSize:12];
    UILabel *label3 = [Utils createLabelWithTitle:@"旅咖出行" titleColor:HexColor(0x333333) fontSize:15];
    self.recommanderName = label3;
    label3.font = [UIFont boldSystemFontOfSize:15];
    UIView *hline1 = [UIView new];
    hline1.backgroundColor = HexColor(0xFFB12C);
    UIView *hline2 = [UIView new];
    hline2.backgroundColor = HexColor(0xFFB12C);
    UILabel *dayLabel = [Utils createLabelWithTitle:@"2天" titleColor:HexColor(0x323232) fontSize:16];
    self.dayLabel = dayLabel;
    UILabel *placeNumLabel =  [Utils createLabelWithTitle:@"6地" titleColor:HexColor(0x323232) fontSize:16];
    self.placeNumberLabel = placeNumLabel;
    
    UIView *line = [[UIView alloc] init];
    line.layer.backgroundColor = HexColor(0x999999).CGColor;
    line.layer.cornerRadius = 12;
    
    UIView *hline = [UIView new];
    hline.backgroundColor = HexColor(0xFFB12C);
    UILabel *label5 = [Utils createBoldLabelWithTitle:@"行程安排" titleColor:HexColor(0x333333) fontSize:15];
    
    UILabel *lable6 = [Utils createLabelWithTitle:@"共两天行程  6个景点" titleColor:HexColor(0x333333) fontSize:15];
    self.dayspaceLabel2 = lable6;
    
    [view sd_addSubviews:@[label1,
                           imgv,label2,label3,
                           hline1,dayLabel,hline2,placeNumLabel,
                           line,
                           hline,label5,
                           self.mapView,
                           lable6]];
    label1.sd_layout
    .topSpaceToView(view, 20)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    
    imgv.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 16)
    .widthIs(30)
    .heightEqualToWidth();
    
    label2.sd_layout
    .leftSpaceToView(imgv, 8)
    .topSpaceToView(label1, 16)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .topSpaceToView(label2, 1)
    .leftEqualToView(label2)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    placeNumLabel.sd_layout
    .centerYEqualToView(label3)
    .rightSpaceToView(view, 20)
    .autoHeightRatio(0);
    [placeNumLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    hline2.sd_layout
    .centerYEqualToView(placeNumLabel)
    .rightSpaceToView(placeNumLabel, 3)
    .widthIs(3)
    .heightIs(15);
    
    dayLabel.sd_layout
    .centerYEqualToView(placeNumLabel)
    .rightSpaceToView(hline2, 3)
    .autoHeightRatio(0);
    [dayLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    hline1.sd_layout
    .centerYEqualToView(placeNumLabel)
    .rightSpaceToView(dayLabel, 3)
    .widthIs(3)
    .heightIs(15);
    
    line.sd_layout
    .topSpaceToView(imgv, 20)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(0.5);
    
    hline.sd_layout
    .leftEqualToView(line)
    .topSpaceToView(line, 33)
    .widthIs(3)
    .heightIs(15);
    
    label5.sd_layout
    .centerYEqualToView(hline)
    .leftSpaceToView(hline, 3)
    .autoHeightRatio(0);
    [label5 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.mapView.sd_layout
    .topSpaceToView(hline,23)
    .leftEqualToView(hline)
    .rightSpaceToView(view, 20)
    .heightIs(170);
    
    lable6.sd_layout
    .topSpaceToView(self.mapView, 30)
    .leftEqualToView(self.mapView)
    .rightEqualToView(self.mapView)
    .autoHeightRatio(0);
    
    return view;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.mapView.showsUserLocation = YES;
   // self.mapView.delegate=self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.mapView.showsUserLocation = NO;
   // self.mapView.delegate=nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 362;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RouteDetailsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RouteDetailsCell" forIndexPath:indexPath];
    LtScenicUsersEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model  = model;
    if(indexPath.row==self.dataArray.count-1){
        [cell showLine:NO];
    }else{
        [cell showLine:YES];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 431;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LtScenicUsersEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    ScenicSpotMain *vc = [[ScenicSpotMain alloc] init];
    vc.scenicId = model.scenicId;
    [self.navigationController pushViewController:vc animated:YES];
//    ScenicSportDescriptionCtrl *vc = [[ScenicSportDescriptionCtrl alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}


//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if(scrollView==self.tableView){
//       // DLog(@"x==%f,y==%f",scrollView.contentOffset.x,scrollView.contentOffset.y);
//        CGFloat offsetY = scrollView.contentOffset.y - self.lastOffSetY;
//        if (offsetY > 0) {
//          //  DLog(@"正在向上滑动");
//            self.tableView.frame = self.view.frame;
//        }
//        else {
//           // DLog(@"正在向下滑动");
//            self.tableView.frame = self.orgFrame;
//        }
//    }
//
//}
//
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    if(scrollView==self.tableView){
//        self.lastOffSetY = scrollView.contentOffset.y;
//        //self.lastSearchFrame = textField.frame;
//    }
//}

@end
