//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LtRefUserInfoEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *referrerLatitude;
@property (nonatomic,copy) NSString *referrerLongitude;
@property (nonatomic,copy) NSString *scenicId;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,copy) NSString *headerImage;
 


-(id)initWithJson:(NSDictionary *)json;

@end
