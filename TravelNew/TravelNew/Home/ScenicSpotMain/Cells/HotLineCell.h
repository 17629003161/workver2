//
//  HotLineCell.h
//  TravelNew
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"
#import "ScenicSpotListEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotLineCell : BaseCustomTableViewCell
@property(nonatomic,strong) ScenicSpotListEntity *model;
@end

NS_ASSUME_NONNULL_END
