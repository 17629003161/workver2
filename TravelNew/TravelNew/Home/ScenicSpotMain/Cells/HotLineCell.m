//
//  HotLineCell.m
//  TravelNew
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 lester. All rights reserved.
//

#import "HotLineCell.h"

@interface HotLineCell(){
    UILabel *_titleL;
    UIImageView *_imgv;
    UILabel *_addrL;
    UILabel *_moneyL;
    UILabel *_dayTimeL;
    UILabel *_placeL;
}
@end


@implementation HotLineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(ScenicSpotListEntity *)model
{
    _model = model;
    [_imgv sd_setImageWithURL:[NSURL URLWithString:model.lineImage]];
    _addrL.text = model.scenicName;
    _moneyL.text = [NSString stringWithFormat:@"￥%.2lf元起",[model.price doubleValue]/100.0];
    _dayTimeL.text = [NSString stringWithFormat:@"%d天",[model.travelDay integerValue]];
    [_dayTimeL changeStrings:@[@"天"] toColor:HexColor(0x999999)];
    [_dayTimeL changeStrings:@[@"天"] toSize:10];
    _placeL.text = [NSString stringWithFormat:@"%d地",[model.ltScenicNum integerValue]];
    [_placeL changeStrings:@[@"地"] toColor:HexColor(0x999999)];
    [_placeL changeStrings:@[@"地"] toSize:10];
    [_addrL updateLayout];
    [_moneyL updateLayout];
    [_dayTimeL updateLayout];
    [_placeL updateLayout];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configViews{
    self.contentView.backgroundColor = HexColor(0xf3f3f3);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 0, kScreenWidth-40, 288)];
    view.backgroundColor = [UIColor whiteColor];
    [view makeTopAngleSize:CGSizeMake(10, 10) bounds:CGRectMake(0, 0, kScreenWidth-40, 288)];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(20, 51, kScreenWidth-80, 203)];
    bgView.backgroundColor = [UIColor whiteColor];
    _titleL = [Utils createBoldLabelWithTitle:@"热门线路" titleColor:HexColor(0x333333) fontSize:15];
    

    bgView.layer.cornerRadius = 4;
    bgView.layer.shadowColor = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:0.5].CGColor;
    bgView.layer.shadowOffset = CGSizeMake(0,3);
    bgView.layer.shadowOpacity = 1;
    bgView.layer.shadowRadius = 8;

    _imgv = ImageViewWithImage(@"home3");
    _addrL = [Utils createLabelWithTitle:@"前年古城之梦回长安" titleColor:HexColor(0x323232) fontSize:16];
    _moneyL = [Utils createLabelWithTitle:@"￥300元起" titleColor:HexColor(0xFE2E2E) fontSize:14];
    _moneyL.hidden = YES;
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xFFB12C);
    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xFFB12C);
    _dayTimeL = [Utils createLabelWithTitle:@"2天" titleColor:HexColor(0x323232) fontSize:16];
    [_dayTimeL changeStrings:@[@"天"] toColor:HexColor(0x999999)];
    [_dayTimeL changeStrings:@[@"天"] toSize:10];
    _placeL = [Utils createLabelWithTitle:@"6地" titleColor:HexColor(0x323232) fontSize:16];
    [_placeL changeStrings:@[@"地"] toColor:HexColor(0x999999)];
    [_placeL changeStrings:@[@"地"] toSize:10];
    [bgView sd_addSubviews:@[_imgv,
                            _addrL,_moneyL,
                            line1,_dayTimeL,line2,_placeL,
                                      ]];
    [view sd_addSubviews:@[_titleL,bgView]];
    [self.contentView addSubview:view];

    [self.contentView sd_addSubviews:@[view]];
    _titleL.sd_layout
    .topSpaceToView(view, 17)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [_titleL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    
    _imgv.sd_layout
    .leftSpaceToView(bgView, 0)
    .topSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .heightIs(137);
    
    _addrL.sd_layout
    .topSpaceToView(bgView, 147)
    .leftSpaceToView(bgView, 12)
    .autoHeightRatio(0);
    [_addrL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _moneyL.sd_layout
    .centerYEqualToView(_addrL)
    .rightSpaceToView(bgView, 12)
    .autoHeightRatio(0);
    [_moneyL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line1.sd_layout
    .topSpaceToView(bgView, 171)
    .leftEqualToView(_addrL)
    .widthIs(2)
    .heightIs(12);
    
    _dayTimeL.sd_layout
    .centerYEqualToView(line1)
    .leftSpaceToView(line1, 6)
    .autoHeightRatio(0);
    [_dayTimeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line2.sd_layout
    .centerYEqualToView(line1)
    .leftSpaceToView(_dayTimeL, 10)
    .widthIs(2)
    .heightIs(12);
    
    _placeL.sd_layout
    .centerYEqualToView(line2)
    .leftSpaceToView(line2, 6)
    .autoHeightRatio(0);
    [_placeL setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
}


@end
