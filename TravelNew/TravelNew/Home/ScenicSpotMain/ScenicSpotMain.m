//
//  ScenicSpotMain.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ScenicSpotMain.h"
#import "ScenicSportDescriptionCtrl.h"
#import "HotLineCell.h"
#import "RouteDetailsCtrl.h"
#import "ScenicSportDescriptionCtrl.h"
#import "MainRecommenderCtrl.h"

@interface ScenicSpotMain (){
    UIView *_topView;
    CGRect orginYframe;
}
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) NSMutableArray *imageViewArray;
@property(nonatomic,strong) UILabel *tipLabel;
@property(nonatomic,strong) UILabel *sceincNameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UILabel *addressLabel;
@property(nonatomic,strong) ScenicSpotEntity *entity;
@property(nonatomic,strong) UILabel *btnNameLabel;
          
@end

@implementation ScenicSpotMain
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //DLog(@" model = %@",self.entity);
    [self disabelPageRefresh];
    self.imageViewArray = [NSMutableArray new];
    [self.tableView registerClass:[HotLineCell class] forCellReuseIdentifier:@"HotLineCell"];
    [self layout];
    [self addBackArrow];
    [self requreScenicSpotDetailData];
    [self loadFirstPage];
    [self checkBlankView];
}

- (Api *)getListApi{
    if(self.scenicId == nil)
        return nil;
    Api *api = [Api apiPostUrl:@"attractions/line/list"
                          para:@{@"scenicId":self.scenicId,
                                 @"pageSize":@(self.pageSize),
                                 @"pageIndex":@(self.pageIndex)
                          }
                   description:@"查询景点旅游路线"];
    return api;
}

- (NSString *)getListEntityName{
    NSString *name =  NSStringFromClass(ScenicSpotListEntity.class);
    return name;
}


-(void)setEntity:(ScenicSpotEntity *)entity{
    _entity = entity;
    for (int i=0; i<self.imageViewArray.count; i++) {
        UIImageView *imgv = [self.imageViewArray objectAtIndex:i];
        imgv.hidden = NO;
        if(i<entity.ltRefUserInfos.count){
            LtRefUserInfosEntity *model = entity.ltRefUserInfos[i];
            [imgv sd_setImageWithURL:[NSURL URLWithString:model.headerImage] placeholderImage:[UIImage imageNamed:@"default_header"]];
        }else{
            imgv.hidden = YES;
        }
    }
    [self.imgv sd_setImageWithURL:[NSURL URLWithString:entity.scenicImage]];
    self.sceincNameLabel.text = entity.scenicName;
    self.descLabel.text = entity.scenicAbstract;
    self.tipLabel.text = [NSString stringWithFormat:@"%ld名推荐官服务",[entity.ltRefUserNum longValue]];
    self.btnNameLabel.text = [NSString stringWithFormat:@"%@ >",entity.scenicName];
    [self.btnNameLabel updateLayout];
    [self.tipLabel updateLayout];
    [self.sceincNameLabel updateLayout];
    [self.descLabel updateLayout];
}

- (void)requreScenicSpotDetailData{
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"attractions/scenic/selectScenicById"
                          para:@{@"scenicId":self.scenicId}
                   description:@"获取景点详情"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSDictionary *dict = [json objectForKey:@"data"];
            ScenicSpotEntity *model = [[ScenicSpotEntity alloc] initWithJson:dict];
            DLog(@"model = %@",model);
            weakSelf.entity = model;
        }
        
        DLog(@"jsonstr = %@",jsonstr);
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@ %@",request.error,request.responseString);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
        }];
}


- (void)layout{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 679)];
    view.backgroundColor = HexColor(0xf3f3f3);
    
    UIImageView *imgv = ImageViewWithImage(@"home3");
    self.imgv = imgv;
    imgv.frame = CGRectMake(0, 0, kScreenWidth, 524);
    UIImageView *maskview = ImageViewWithImage(@"angle_mask");
    [imgv setUserInteractionEnabled:YES];
    
    UILabel *label1 = [Utils createLabelWithTitle:@"大雁塔" titleColor:[UIColor whiteColor] fontSize:30];
    self.sceincNameLabel = label1;
    UIImageView *imageView = ImageViewWithImage(@"fat_addr_icon");
    UILabel *label2 = [Utils createLabelWithTitle:@"陕西省西安市雁塔区大慈恩寺内" titleColor:HexColor(0xfcfcfc) fontSize:12];
    self.addressLabel = label2;
    
    _topView = [self createView1];
    _topView.frame = CGRectMake(0, 0, kScreenWidth-40, 100);
    
    UIView *view2 = [self createView2];
    
    [view sd_addSubviews:@[imgv,maskview,
                           label1,
                           imageView,label2,_topView,view2]];
     [self.view addSubview:view];
    maskview.sd_layout
    .leftEqualToView(imgv)
    .rightEqualToView(imgv)
    .bottomEqualToView(imgv)
    .heightIs(24);
    
    label1.sd_layout
    .topSpaceToView(view, 110)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    imageView.sd_layout
    .topSpaceToView(label1, 14)
    .leftSpaceToView(view, 96)
    .widthIs(10)
    .heightIs(13);
    
    label2.sd_layout
    .centerYEqualToView(imageView)
    .leftSpaceToView(imageView, 5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _topView.sd_layout
    .topSpaceToView(view, 302)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(100);
    [_topView makeTopAngleSize:CGSizeMake(15, 15) bounds:CGRectMake(0, 0, kScreenWidth-40, 100)];
    
    view2.sd_layout
    .topSpaceToView(view, 422)
    .leftSpaceToView(view, 20)
    .rightSpaceToView(view, 20)
    .heightIs(237);
    
    [view2 makeTopAngleSize:CGSizeMake(15, 15) bounds:CGRectMake(0, 0, kScreenWidth-40, 237)];
    

    self.tableView.frame = CGRectMake(0, -50, kScreenWidth, kScreenHeight+50);
    self.tableView.backgroundColor = HexColor(0xf3f3f3);
    self.tableView.tableHeaderView = view;
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 288.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotLineCell" forIndexPath:indexPath];
    ScenicSpotListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScenicSpotListEntity *model = [self.dataArray objectAtIndex:indexPath.row];
    RouteDetailsCtrl *vc = [RouteDetailsCtrl new];
    vc.travellineId = model.travellineId;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)createView1{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"玄奘为保存由天竺经丝绸之路带回长安…"
                                      titleColor:HexColor(0x414243)
                                        fontSize:13];
    self.descLabel = label;
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xa0a0a0);
    UILabel *label1 = [Utils createLabelWithTitle:@"认识大雁塔 >" titleColor:HexColor(0x64C5FC) fontSize:15];
    self.btnNameLabel = label1;
    
    [label1 addTapGestureTarget:self action:@selector(btnScienicSpotIsClicked:)];
    [view sd_addSubviews:@[label,
                           line,
                           label1]];
    label.sd_layout
    .topSpaceToView(view, 24)
    .leftSpaceToView(view, 17)
    .rightSpaceToView(view, 17)
    .heightIs(18);
    
    line.sd_layout
    .topSpaceToView(label, 12)
    .leftSpaceToView(view, 0)
    .rightSpaceToView(view, 0)
    .heightIs(0.5);
    
    label1.sd_layout
    .topSpaceToView(line, 13)
    .centerXEqualToView(line)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
    
}

- (void)btnScienicSpotIsClicked:(id)sender{
    ScenicSportDescriptionCtrl *vc = [ScenicSportDescriptionCtrl new];
    vc.entity = self.entity;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)recomandIsClicked:(UITapGestureRecognizer *)reg
{
    UIImageView *imagv = (UIImageView *)reg.view;
    NSInteger idx = imagv.tag-100;
    LtRefUserInfosEntity *model = self.entity.ltRefUserInfos[idx];
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = model.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)createView2{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth-40, 237)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createLabelWithTitle:@"0名推荐官服务" titleColor:HexColor(0x333333) fontSize:15];
    label.frame = CGRectMake(24, 17, 300, 21);
    self.tipLabel = label;
    UIImageView *image1 =  ImageViewWithImage(@"person_header");
    image1.tag = 100;
    [image1 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image2 =  ImageViewWithImage(@"person_header");
    image2.tag = 101;
    [image2 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image3 =  ImageViewWithImage(@"person_header");
    image3.tag = 102;
    [image3 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image4 =  ImageViewWithImage(@"person_header");
    image4.tag = 103;
    [image4 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image5 =  ImageViewWithImage(@"person_header");
    image5.tag = 104;
    [image5 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image6 =  ImageViewWithImage(@"person_header");
    image6.tag = 105;
    [image6 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image7 =  ImageViewWithImage(@"person_header");
    image7.tag = 106;
    [image7 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    UIImageView *image8 =  ImageViewWithImage(@"white_dot-1");
    image8.tag = 107;
    [image8 addTapGestureTarget:self action:@selector(recomandIsClicked:)];
    
    image1.layer.cornerRadius = 25;
    image1.layer.masksToBounds = YES;
    image2.layer.cornerRadius = 25;
    image2.layer.masksToBounds = YES;
    image3.layer.cornerRadius = 25;
    image3.layer.masksToBounds = YES;
    image4.layer.cornerRadius = 25;
    image4.layer.masksToBounds = YES;
    image5.layer.cornerRadius = 25;
    image5.layer.masksToBounds = YES;
    image6.layer.cornerRadius = 25;
    image6.layer.masksToBounds = YES;
    image7.layer.cornerRadius = 25;
    image7.layer.masksToBounds = YES;
    image8.layer.cornerRadius = 25;
    image8.layer.masksToBounds = YES;
    
    image1.frame = CGRectMake(24, 55, 50, 50);
    image2.frame = CGRectMake(102, 55, 50, 50);
    image3.frame = CGRectMake(180, 55, 50, 50);
    image4.frame = CGRectMake(258, 55, 50, 50);
    image5.frame = CGRectMake(24, 125, 50, 50);
    image6.frame = CGRectMake(102, 125, 50, 50);
    image7.frame = CGRectMake(180, 125, 50, 50);
    image8.frame = CGRectMake(258, 125, 50, 50);
    
    image1.layer.cornerRadius = 25;
    image2.layer.cornerRadius = 25;
    image3.layer.cornerRadius = 25;
    image4.layer.cornerRadius = 25;
    image5.layer.cornerRadius = 25;
    image6.layer.cornerRadius = 25;
    image7.layer.cornerRadius = 25;
    image8.layer.cornerRadius = 25;
    
    [self.imageViewArray addObjectsFromArray:@[image1,image2,image3,image4,image5,image6,image7,image8]];
    
    UILabel *label2 = [Utils createLabelWithTitle:@"我好懒啊！试试一键预约>" titleColor:HexColor(0x999999) fontSize:12];
    [label2 changeStrings:@[@"一键预约>"] toColor:HexColor(0x007BFF)];
    [label2 addTapGestureTarget:self action:@selector(gotoOtherTabbar:)];
    label2.frame = CGRectMake(167, 195, 145, 17);
    label2.hidden = YES;
    [view sd_addSubviews:@[label,image1,image2,image3,image4,
                           image5,image6,image7,image8,label2]];
    return view;
    
}

- (void)gotoOtherTabbar:(id)sender
{
    self.tabBarController.selectedIndex = 1;
}


@end
