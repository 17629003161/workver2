//
//  ScenicSpotMain.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseTableViewController.h"
#import "LKTopImageTableViewController.h"
#import "ScenicSpotEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScenicSpotMain :LKBaseTableViewController
@property(nonatomic,strong) NSString *scenicId;
//@property(nonatomic,strong) ScenicSpotEntity *entity;
@end

NS_ASSUME_NONNULL_END
