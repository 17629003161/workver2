//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "LineDataEntity.h"

@implementation LineDataEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.lineImage  = [json objectForKey:@"lineImage"];
		self.abstracts  = [json objectForKey:@"abstracts"];
		self.userNickname  = [json objectForKey:@"userNickname"];
		self.travelName  = [json objectForKey:@"travelName"];
		self.referrerUserId  = [json objectForKey:@"referrerUserId"];
		self.travellineId  = [json objectForKey:@"travellineId"];
		self.travelDay  = [json objectForKey:@"travelDay"];
		self.price  = [json objectForKey:@"price"];
		self.hotVar  = [json objectForKey:@"hotVar"];
		self.cityid  = [json objectForKey:@"cityid"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		self.city  = [json objectForKey:@"city"];
		self.ltScenicNum  = [json objectForKey:@"ltScenicNum"];
		self.scenicName  = [json objectForKey:@"scenicName"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.lineImage forKey:@"zx_lineImage"];
	[aCoder encodeObject:self.abstracts forKey:@"zx_abstracts"];
	[aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.travelName forKey:@"zx_travelName"];
	[aCoder encodeObject:self.referrerUserId forKey:@"zx_referrerUserId"];
	[aCoder encodeObject:self.travellineId forKey:@"zx_travellineId"];
	[aCoder encodeObject:self.travelDay forKey:@"zx_travelDay"];
	[aCoder encodeObject:self.price forKey:@"zx_price"];
	[aCoder encodeObject:self.hotVar forKey:@"zx_hotVar"];
	[aCoder encodeObject:self.cityid forKey:@"zx_cityid"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	[aCoder encodeObject:self.city forKey:@"zx_city"];
	[aCoder encodeObject:self.ltScenicNum forKey:@"zx_ltScenicNum"];
	[aCoder encodeObject:self.scenicName forKey:@"zx_scenicName"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.lineImage = [aDecoder decodeObjectForKey:@"zx_lineImage"];
		self.abstracts = [aDecoder decodeObjectForKey:@"zx_abstracts"];
		self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.travelName = [aDecoder decodeObjectForKey:@"zx_travelName"];
		self.referrerUserId = [aDecoder decodeObjectForKey:@"zx_referrerUserId"];
		self.travellineId = [aDecoder decodeObjectForKey:@"zx_travellineId"];
		self.travelDay = [aDecoder decodeObjectForKey:@"zx_travelDay"];
		self.price = [aDecoder decodeObjectForKey:@"zx_price"];
		self.hotVar = [aDecoder decodeObjectForKey:@"zx_hotVar"];
		self.cityid = [aDecoder decodeObjectForKey:@"zx_cityid"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		self.city = [aDecoder decodeObjectForKey:@"zx_city"];
		self.ltScenicNum = [aDecoder decodeObjectForKey:@"zx_ltScenicNum"];
		self.scenicName = [aDecoder decodeObjectForKey:@"zx_scenicName"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"lineImage : %@\n",self.lineImage];
	result = [result stringByAppendingFormat:@"abstracts : %@\n",self.abstracts];
	result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"travelName : %@\n",self.travelName];
	result = [result stringByAppendingFormat:@"referrerUserId : %@\n",self.referrerUserId];
	result = [result stringByAppendingFormat:@"travellineId : %@\n",self.travellineId];
	result = [result stringByAppendingFormat:@"travelDay : %@\n",self.travelDay];
	result = [result stringByAppendingFormat:@"price : %@\n",self.price];
	result = [result stringByAppendingFormat:@"hotVar : %@\n",self.hotVar];
	result = [result stringByAppendingFormat:@"cityid : %@\n",self.cityid];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	result = [result stringByAppendingFormat:@"city : %@\n",self.city];
	result = [result stringByAppendingFormat:@"ltScenicNum : %@\n",self.ltScenicNum];
	result = [result stringByAppendingFormat:@"scenicName : %@\n",self.scenicName];
	
    return result;
}

@end
