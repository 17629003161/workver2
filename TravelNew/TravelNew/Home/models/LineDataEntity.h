//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器   http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LineDataEntity : NSObject<NSCoding>

@property (nonatomic,copy) NSString *lineImage;
@property (nonatomic,copy) NSString *abstracts;
@property (nonatomic,copy) NSString *userNickname;
@property (nonatomic,copy) NSString *travelName;
@property (nonatomic,strong) NSNumber *referrerUserId;
@property (nonatomic,copy) NSString *travellineId;
@property (nonatomic,strong) NSNumber *travelDay;
@property (nonatomic,strong) NSNumber *price;
@property (nonatomic,strong) NSNumber *hotVar;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,copy) NSString *headerImage;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,strong) NSNumber *ltScenicNum;
@property (nonatomic,copy) NSString *scenicName;
 


-(id)initWithJson:(NSDictionary *)json;

@end
