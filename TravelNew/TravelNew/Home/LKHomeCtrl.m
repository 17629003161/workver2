//
//  LKHomeCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKHomeCtrl.h"
#import "RecommendCell.h"
#import "MastPlayCell.h"
#import "BastWayCell.h"
#import "Home_SectionHeaderView.h"
#import "Home_SectionFooterView.h"
#import "ScenicSpotMustPlayCtrl.h"
#import "AllRecommenderCtrl.h"
#import "RouteDetailsCtrl.h"
#import "NearbyRecommanderMapCtrl.h"
#import "SelectCityCtrl.h"
#import "SearchScenicspotCtrl.h"
#import "MainRecommenderCtrl.h"
#import "ScenicSpotMain.h"
#import "RouteDetailsCtrl.h"
#import "JSONObject.h"
#import "RecommenderEntity.h"
#import "LineDataEntity.h"
#import "YTKBatchRequest.h"
#import "CustomAnnotationView.h"
#import "MyPointAnnotation.h"
#import "MessageModel.h"
#import "DownwindSetPlacesCtrl.h"
#import "TourSelectView.h"

#define kLineSpacing 12
#define kColSpaceing 9
#define kCollectionViewOffset 972

//新版第二版

@interface LKHomeCtrl ()<UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,MAMapViewDelegate>{
    UIView *_tvHeaderView;
}
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIButton *btnCity;
@property(nonatomic,strong) UILabel *recomandNumberL;


@property(nonatomic,strong) NSMutableArray *scenicSpotArray;
@property(nonatomic,strong) NSMutableArray *annonceArry;

@property(nonatomic,strong) UIView *searchView;
@property(nonatomic,assign) CGRect searchOrignFrame;

@property(nonatomic,strong) UIImageView *animationImgv;
@property(nonatomic,strong) UIImageView *bgAnimationView;
@property(nonatomic,assign) CGPoint animationImagvStartCenter;
@property(nonatomic,assign) CGPoint bgAnimationImagvStartCenter;

@property(nonatomic,strong) NSString *cityId;
@property(nonatomic,assign) CLLocationCoordinate2D coord;
@property(nonatomic,strong) NSString *cityName;

@end

@implementation LKHomeCtrl
- (void)viewDidLoad {
    [super viewDidLoad];
    [self layout];
    self.searchView = [self makeSearchView];
    self.searchView.frame = CGRectMake(18, 190, kScreenWidth-36, 50);
    self.searchOrignFrame = self.searchView.frame;
    [self.view addSubview:_searchView];

    self.scenicSpotArray = [NSMutableArray new];
    self.annonceArry = [NSMutableArray new];
   // WeakSelf
    [self doAnimation];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.mapView.scrollEnabled = NO;
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate=self;
    [self PageRefreshSetup];
    
    __weak LKLocationManager *locationManager = [LKLocationManager sharedManager];
    [locationManager startLocation];
    locationManager.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, CLLocationCoordinate2D location,NSString *cityName) {
        [locationManager stopLocation];
        DLog(@"\n\nLocationManager  = %@",locationManager);
        UserInfo *user = [UserInfo shareInstance];
        if(!self.cityId){
            self.cityId = cityid;
            self.coord = location;
            self.cityName = cityName;
            user.selectCityId = cityid;
            if(isNull(user.cityid) || user.cityid.length==0){
                user.cityid = cityid;
                user.city = cityName;
                user.selectCityId = cityid;
                user.selectCity = cityName;
                [user save];
            }
        }
        [self sendBatchRequest:self.cityId];
    };
    [locationManager startLocation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.mapView.scrollEnabled = YES;
    self.mapView.showsUserLocation = NO;
    self.mapView.delegate=nil;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self doAnimation];
}

- (void)PageRefreshSetup{
    LKWeakSelf
    // 下拉刷新
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        DLog(@"下拉刷新");
        [weakSelf.collectionView.mj_header endRefreshing];
        [self sendBatchRequest:self.cityId];
    }];
}

- (void)sendBatchRequest:(NSString *)cityId {
    LKWeakSelf
    if(cityId==nil) return;
    Api *api1 = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerNum"
                           para:@{@"cityid":cityId}
                    description:@"user - 获取城市推荐官数量"];
    
    Api *api3 = [Api apiPostUrl:@"attractions/scenic/list"
                           para:@{@"cityid":cityId,
                               @"pageIndex":@(1),
                                @"pageSize":@(4)}
                    description:@"attractions - 查询城市景点列表"];
    
    YTKBatchRequest *batchRequest = [[YTKBatchRequest alloc]initWithRequestArray:@[api1,api3]];
    [batchRequest startWithCompletionBlockWithSuccess:^(YTKBatchRequest *batchRequest) {
        DLog(@"succeed");
        YTKRequest *request1 = batchRequest.requestArray[0];
        NSDictionary *json = [NSString jsonStringToDictionary:request1.responseString];
        JSONObject *model = [[JSONObject alloc] initWithJson:json];
        if([model.code integerValue]==200){
            NSInteger num = [model.data integerValue];
            NSString *numstr = [NSString stringWithFormat:@"%ld位",num];
            weakSelf.recomandNumberL.text = [NSString stringWithFormat:@"当地%@推荐官为您服务",numstr];
            [weakSelf.recomandNumberL changeStrings:@[numstr] toColor:HexColor(0x1E69FF)];
        }
        
        YTKRequest *request3 = batchRequest.requestArray[1];
        json = [NSString jsonStringToDictionary:request3.responseString];
        [self.scenicSpotArray removeAllObjects];
        if([[json objectForKey:@"code"] intValue]==200){
            NSArray *dataAry = [json objectForKey:@"data"];
            if(dataAry.count>0){
                for(NSDictionary *dict in dataAry){
                    ScenicSpotEntity *model = [[ScenicSpotEntity alloc] initWithJson:dict];
                 //   DLog(@"model = %@",model);
                    [weakSelf.scenicSpotArray addObject:model];
                }
            }else{

            }
        }
        [weakSelf.collectionView reloadData];
    } failure:^(YTKBatchRequest *batchRequest) {
        DLog(@"failed");
    }];
}

- (void)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 设置item的行间距和列间距
    layout.minimumInteritemSpacing = kColSpaceing;
    layout.minimumLineSpacing = kLineSpacing;
    // 设置item的大小
    layout.itemSize = CGSizeMake(339,237);
    // 设置每个分区的 上左下右 的内边距
    layout.sectionInset = UIEdgeInsetsMake(0, 0 ,0, 0);
    // 设置区头和区尾的大小
    layout.headerReferenceSize = CGSizeMake(kScreenWidth, 65);
    layout.footerReferenceSize = CGSizeMake(kScreenWidth, 65);
    // 设置分区的头视图和尾视图 是否始终固定在屏幕上边和下边
    layout.sectionFootersPinToVisibleBounds = YES;
    // 设置滚动条方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.sectionHeadersPinToVisibleBounds = NO;
    layout.sectionFootersPinToVisibleBounds = NO;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kTabbarHeight-kStatusBarHeight) collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
    
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsVerticalScrollIndicator = NO;   //是否显示滚动条
    _collectionView.scrollEnabled = YES;  //滚动使能
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    //注册cell
    [self.collectionView registerClass:[MastPlayCell class]
            forCellWithReuseIdentifier:@"MastPlayCell"];
    //注册头部视图
     [self.collectionView registerClass:[Home_SectionHeaderView class]
             forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                    withReuseIdentifier:@"ReusableView"];
    //注册尾部
    [self.collectionView registerClass:[Home_SectionFooterView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                   withReuseIdentifier:@"Home_SectionFooterView"];
    [self contentInsetHeaderView];
}

 /**
 * 让状态栏样式为白色
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}




- (void)contentInsetHeaderView {
    _tvHeaderView = [self makeHeaderView];
    _tvHeaderView.frame = CGRectMake(0, -kCollectionViewOffset, kScreenWidth, kCollectionViewOffset);
    [_collectionView addSubview:_tvHeaderView];
    // CGFloat top, left, bottom, right;
    _collectionView.contentInset = UIEdgeInsetsMake(kCollectionViewOffset, 0, 0, 0);
    [_collectionView setContentOffset:CGPointMake(0, -kCollectionViewOffset)];
    
    UIImageView *imgv = ImageViewWithImage(@"home_bg");
    imgv.frame = CGRectMake(-20, -20, kScreenWidth+20, 200);
    [imgv setUserInteractionEnabled:YES];
    self.bgAnimationView = imgv;
    self.bgAnimationImagvStartCenter = imgv.center;
    [_tvHeaderView addSubview:imgv];
    
    UIImageView *animationImgv = ImageViewWithImage(@"home_animation");
    animationImgv.frame = CGRectMake(0, 0, 195, 114);
    animationImgv.center = CGPointMake(imgv.center.x+20, imgv.center.y-10);
    self.animationImgv = animationImgv;
    self.animationImagvStartCenter = animationImgv.center;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"西安" forState:UIControlStateNormal];
    btn.titleLabel.font = LKSystemFont(15);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnCity = btn;
    [btn addTarget:self action:@selector(changeCityIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    

    UIImageView *icon = ImageViewWithImage(@"addr_icon_home_white");
    [_tvHeaderView sd_addSubviews:@[animationImgv,btn,icon]];
    
    btn.sd_layout
    .topSpaceToView(_tvHeaderView, 29)
    .rightSpaceToView(_tvHeaderView, 27);
    // 设置button根据文字size自适应
    [btn setupAutoSizeWithHorizontalPadding:0 buttonHeight:27];
    
    icon.sd_layout
    .centerYEqualToView(btn)
    .leftSpaceToView(btn, 5)
    .widthIs(10)
    .heightIs(13);
}


- (void)doAnimation{
    self.animationImgv.center = self.animationImagvStartCenter;
    self.bgAnimationView.center = self.bgAnimationImagvStartCenter;
    [UIView animateWithDuration:0.8 animations:^{
        self.animationImgv.center = CGPointMake(kScreenWidth/2.0, self.animationImgv.center.y);
        self.bgAnimationView.center = CGPointMake(kScreenWidth/2.0+10, self.bgAnimationImagvStartCenter.y);
        } completion:^(BOOL finished) {
            
        }];
}


- (void)changeCityIsClicked:(id)sender
{
    LKWeakSelf
    SelectCityCtrl *vc = [SelectCityCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    vc.OnSelectAddress = ^(AMapDistrict * dist) {
        [self.btnCity setTitle:dist.name forState:UIControlStateNormal];
        weakSelf.mapView.showsUserLocation = NO;
        weakSelf.mapView.userTrackingMode = MAUserTrackingModeNone;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(dist.center.latitude, dist.center.longitude);
        weakSelf.mapView.centerCoordinate = coord;
        self.cityId = dist.citycode;
        self.coord = coord;
        self.cityName = dist.name;
        UserInfo *user = [UserInfo shareInstance];
        user.selectCityId = dist.citycode;
        [weakSelf sendBatchRequest:self.cityId];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)makeSearchView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderColor = HexColor(0xe1e1e1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 6;
    UIImageView *leftimg = ImageViewWithImage(@"search_home");
    UILabel *label = [Utils createLabelWithTitle:@"搜索热门景区" titleColor:HexColor(0x323232) fontSize:16];
    [view sd_addSubviews:@[leftimg,label]];
    
    [view addTapGestureTarget:self action:@selector(searchIsClicked:)];
    
    leftimg.sd_layout
    .leftSpaceToView(view, 20)
    .centerYEqualToView(view)
    .widthIs(20)
    .heightIs(20);
    
    label.sd_layout
    .centerYEqualToView(leftimg)
    .leftSpaceToView(leftimg, 10)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (UIView *)makeHeaderView{
    LKWeakSelf
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"附近推荐官" titleColor:HexColor(0x333333) fontSize:23];
    label1.font = [UIFont boldSystemFontOfSize:23];
    UILabel *label2 = [Utils createLabelWithTitle:@"当地0位推荐官为您服务" titleColor:HexColor(0x999999) fontSize:14];
    self.recomandNumberL = label2;
    [label2 changeStrings:@[@"0位"] toColor:HexColor(0x1E69FF)];
    [view sd_addSubviews:@[label1,label2]];
    [view addSubview:self.mapView];
    
    TourSelectView *switchView = [TourSelectView new];
    switchView.selectBlock = ^(NSInteger index) {
        if(index==0){
            DLog(@"周边游");
            [MBProgressHUD showMessage:@"周边游暂未开放"];
        }else if(index==1){
            DLog(@"顺风游");
            DownwindSetPlacesCtrl *vc = [[DownwindSetPlacesCtrl alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    };
    [view addSubview:switchView];
    
    label1.sd_layout
    .topSpaceToView(view, 260)
    .leftSpaceToView(view, 18)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 1)
    .leftEqualToView(label1)
    .rightSpaceToView(view, 18)
    .autoHeightRatio(0);
    
    self.mapView.sd_layout
    .leftEqualToView(label2)
    .topSpaceToView(label2, 20)
    .rightSpaceToView(view, 18)
    .heightIs(337);
    
    switchView.sd_layout
    .leftEqualToView(self.mapView)
    .topSpaceToView(self.mapView, 30)
    .rightEqualToView(self.mapView)
    .heightIs(252);
    
    return view;
}

- (void)selectStartAddress:(UITapGestureRecognizer *)reg
{
    DownwindSetPlacesCtrl *vc = [[DownwindSetPlacesCtrl alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchIsClicked:(UITapGestureRecognizer *)reg
{
    SearchScenicspotCtrl *vc = [[SearchScenicspotCtrl alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)rangeChangeTo:(CLLocationCoordinate2D) centerCoordinate{
    // 当前屏幕中心点的经纬度
    double centerLongitude = self.mapView.region.center.longitude;
    double centerLatitude = self.mapView.region.center.latitude;
    //当前屏幕显示范围的经纬度
    CLLocationDegrees pointssLongitudeDelta = self.mapView.region.span.longitudeDelta;
    CLLocationDegrees pointssLatitudeDelta = self.mapView.region.span.latitudeDelta;
    //左下角
   // double leftBottomLong = centerLongitude - pointssLongitudeDelta/2.0;
  //  double leftBottomLat = centerLatitude - pointssLatitudeDelta/2.0;
    //右下角
    double rightBottomLong = centerLongitude + pointssLongitudeDelta/2.0;
    double rightBottomLat = centerLatitude - pointssLatitudeDelta/2.0;
    //左上角
    double leftTopLong = centerLongitude - pointssLongitudeDelta/2.0;
    double leftToplat = centerLatitude + pointssLatitudeDelta/2.0;
    UserInfo *user = [UserInfo shareInstance];
    if(isNull(user.cityid)) return;
    Api *api = [Api apiPostUrl:@"user/LtReferrerUser/getLtReferrerUsers"
                          para:@{@"cityid":user.cityid,
                                 @"ltlat":@(leftToplat),
                                 @"ltlon":@(leftTopLong),
                                 @"rblat":@(rightBottomLat),
                                 @"rblon":@(rightBottomLong)}
                              description:@"user-查询视区内推荐官"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
       // DLog(@"jsonstr = %@",request.responseString);
        NSDictionary *json = [NSString jsonStringToDictionary:request.responseString];
        if([[json objectForKey:@"code"] integerValue]==200){
            NSArray *ary = [json objectForKey:@"data"];
            [self.mapView removeAnnotations:self.annonceArry];
            [self.annonceArry removeAllObjects];
            for(NSDictionary *dict in ary){
                RecommanderMapEntity *model = [[RecommanderMapEntity alloc] initWithJson:dict];
                MyPointAnnotation *annotation = [[MyPointAnnotation alloc] init];
                annotation.coordinate = CLLocationCoordinate2DMake([model.referrerLatitude doubleValue], [model.referrerLongitude doubleValue]);
                annotation.title = @"";
                annotation.model = model;
                [self.mapView addAnnotation:annotation];
            }
        }

        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"%@",request.error);
            if([request.responseString containsString:@"登录失效"]){
                [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
            }
    }];
}


- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    MACoordinateRegion region;
    CLLocationCoordinate2D centerCoordinate = self.mapView.region.center;
    region.center= centerCoordinate;
    [self rangeChangeTo:centerCoordinate];
    //_pointAnnotation.coordinate = centerCoordinate;
    DLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
//    [self setLocationWithLatitude:centerCoordinate.latitude AndLongitude:centerCoordinate.longitude];
   
}

- (void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager*)locationManager
{
    [locationManager requestAlwaysAuthorization];
}


- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAUserLocation class]]) {
        MAUserLocationRepresentation *r = [[MAUserLocationRepresentation alloc] init];
        r.showsAccuracyRing = NO;///精度圈是否显示，默认YES
        [self.mapView updateUserLocationRepresentation:r];
        
        static NSString *userLocationStyleReuseIndetifier = @"userLocationStyleReuseIndetifier";
        MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
        if (annotationView == nil) {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
        }
        annotationView.image = [UIImage imageNamed:@"address_icon3"];
        return annotationView;
    }
    if ([annotation isKindOfClass:[MyPointAnnotation class]])
    {
        MyPointAnnotation *ann = (MyPointAnnotation *)annotation;
        static NSString *reuseIndetifier = @"userAnnotationReuseIndetifier";
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        // 设置为NO，用以调用自定义的calloutView
        annotationView.canShowCallout = NO;
        if(isNull(ann.model.headerImage))
            ann.model.headerImage = @"";
        [annotationView setImageWithURL:[NSURL URLWithString:ann.model.headerImage] placeholderImage:[UIImage imageWithColor:HexColor(0xE3E3E3)]];
        [annotationView setTitle:@""];
        [annotationView setTitle:nil];
        //没有排队
        // 设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        [annotationView addTapGestureTarget:self action:@selector(markIsClicked:)];
                
        return annotationView;
    }
    return nil;
}

- (void)markIsClicked:(UITapGestureRecognizer *)reg
{
    DLog(@"mark---------------%@",[reg.view class]);
    CustomAnnotationView *view = (CustomAnnotationView *)reg.view;
    MyPointAnnotation *ann = (MyPointAnnotation *)view.annotation;
    MainRecommenderCtrl *vc = [MainRecommenderCtrl new];
    vc.userId = ann.model.userId;
    [self.navigationController pushViewController:vc animated:YES];
    
}




//允许多个交互事件
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
        
//点击地图底图调用
- (void)mapView:(MAMapView *)mapView didSingleTappedAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    NearbyRecommanderMapCtrl *vc = [[NearbyRecommanderMapCtrl alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -collectionview 数据源方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;   //返回section数
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
      //每个section的Item数
    return self.scenicSpotArray.count > 4 ? 4 : self.scenicSpotArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    MastPlayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MastPlayCell" forIndexPath:indexPath];
    ScenicSpotEntity *model = [self.scenicSpotArray objectAtIndex:indexPath.item];
    cell.model = model;
    return cell;
 }

 #pragma mark - 头部视图
 - (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader){
         Home_SectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView" forIndexPath:indexPath];
        [headerView.arrowImgv setHidden:NO];
        [headerView.moreLabel setHidden:NO];
        headerView.titleLable.text = @"必玩景点";
        headerView.subTitleLabel.text = @"热门景点推荐";
        if(self.cityName){
            headerView.subTitleLabel.text = [NSString stringWithFormat:@"%@热门景点推荐",self.cityName];
        }
        [headerView.moreLabel addTapGestureTarget:self action:@selector(lookMoreScenicSpot:)];
        reusableview = headerView;
    }else if (kind == UICollectionElementKindSectionFooter){
        Home_SectionFooterView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Home_SectionFooterView" forIndexPath:indexPath];
            reusableview = footerView;
    }
    return reusableview;
 }

- (void)allRecommenderIsClicked:(UITapGestureRecognizer *)reg
{
    AllRecommenderCtrl *vc = [[AllRecommenderCtrl alloc] init];
    vc.cityCode = self.cityId;
    vc.coord = self.coord;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)lookMoreScenicSpot:(UITapGestureRecognizer *)reg{
    ScenicSpotMustPlayCtrl *vc = [[ScenicSpotMustPlayCtrl alloc] init];
    vc.cityCode = self.cityId;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Section 头部视图的尺寸
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(kScreenWidth, 98);
}
#pragma mark - Section尾部视图的尺寸
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(kScreenWidth, 180);
}

- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
   sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((kScreenWidth-9-36)*0.5, 212);
}


#pragma mark - 点击 某个Item时 调用
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];//取消选中
    ScenicSpotEntity *model = [self.scenicSpotArray objectAtIndex:indexPath.item];
    ScenicSpotMain *vc = [ScenicSpotMain new];
    vc.scenicId = model.scenicId;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


   //边距设置:整体边距的优先级，始终高于内部边距的优先级
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout *)collectionViewLayout
insetForSectionAtIndex:(NSInteger)section
{
 return UIEdgeInsetsMake(0, 18, 0, 18);//分别为上、左、下、右
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView==self.collectionView){
       // DLog(@"x==%f,y==%f",scrollView.contentOffset.x,scrollView.contentOffset.y);
        CGFloat offsetY = scrollView.contentOffset.y+kCollectionViewOffset+kStatusBarHeight;
       // DLog(@"offsetY = %.2lf",offsetY);
        CGRect frame = self.searchOrignFrame;
        frame.origin.y = self.searchOrignFrame.origin.y - offsetY;
        if(frame.origin.y < kStatusBarHeight){
            frame.origin.y = kStatusBarHeight;
            self.searchView.alpha = 0.0;
        }else{
            self.searchView.alpha = 1.0;
        }
        if(frame.origin.y >180){
            frame.origin.y = 180;
        }
        self.searchView.frame = frame;
    }
}

@end
