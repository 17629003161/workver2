//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "LtRefUserInfosEntity.h"

@implementation LtRefUserInfosEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.referrerLatitude  = [json objectForKey:@"referrerLatitude"];
		self.referrerLongitude  = [json objectForKey:@"referrerLongitude"];
		self.scenicId  = [json objectForKey:@"scenicId"];
		self.userId  = [json objectForKey:@"userId"];
		self.userNickname  = [json objectForKey:@"userNickname"];
		self.headerImage  = [json objectForKey:@"headerImage"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.referrerLatitude forKey:@"zx_referrerLatitude"];
	[aCoder encodeObject:self.referrerLongitude forKey:@"zx_referrerLongitude"];
	[aCoder encodeObject:self.scenicId forKey:@"zx_scenicId"];
	[aCoder encodeObject:self.userId forKey:@"zx_userId"];
	[aCoder encodeObject:self.userNickname forKey:@"zx_userNickname"];
	[aCoder encodeObject:self.headerImage forKey:@"zx_headerImage"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.referrerLatitude = [aDecoder decodeObjectForKey:@"zx_referrerLatitude"];
		self.referrerLongitude = [aDecoder decodeObjectForKey:@"zx_referrerLongitude"];
		self.scenicId = [aDecoder decodeObjectForKey:@"zx_scenicId"];
		self.userId = [aDecoder decodeObjectForKey:@"zx_userId"];
		self.userNickname = [aDecoder decodeObjectForKey:@"zx_userNickname"];
		self.headerImage = [aDecoder decodeObjectForKey:@"zx_headerImage"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"referrerLatitude : %@\n",self.referrerLatitude];
	result = [result stringByAppendingFormat:@"referrerLongitude : %@\n",self.referrerLongitude];
	result = [result stringByAppendingFormat:@"scenicId : %@\n",self.scenicId];
	result = [result stringByAppendingFormat:@"userId : %@\n",self.userId];
	result = [result stringByAppendingFormat:@"userNickname : %@\n",self.userNickname];
	result = [result stringByAppendingFormat:@"headerImage : %@\n",self.headerImage];
	
    return result;
}

@end
