//
//  
//  AutomaticCoder
//
//  Created by 张玺自动代码生成器  http://zhangxi.me
//  Copyright (c) 2012年 me.zhangxi. All rights reserved.
//
#import "ScenicSpotEntity.h"

@implementation ScenicSpotEntity

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"ltRefUserInfos":@"LtRefUserInfosEntity",};
}

-(id)initWithJson:(NSDictionary *)json;
{
    self = [super init];
    if(self)
    {
    if(json != nil)
    {
       self.ltRefUserInfos = [NSMutableArray array];
	for(NSDictionary *item in [json objectForKey:@"ltRefUserInfos"])
		{
[self.ltRefUserInfos addObject:[[LtRefUserInfosEntity alloc] initWithJson:item]];
	}
self.scenicAbstract  = [json objectForKey:@"scenicAbstract"];
		self.scenicImage  = [json objectForKey:@"scenicImage"];
		self.longitude  = [json objectForKey:@"longitude"];
		self.latitude  = [json objectForKey:@"latitude"];
		self.ltRefUserNum  = [json objectForKey:@"ltRefUserNum"];
		self.price  = [json objectForKey:@"price"];
		self.hotVar  = [json objectForKey:@"hotVar"];
		self.city  = [json objectForKey:@"city"];
		self.scenicId  = [json objectForKey:@"scenicId"];
		self.scenicName  = [json objectForKey:@"scenicName"];
		self.sequence  = [json objectForKey:@"sequence"];
		
    }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.ltRefUserInfos forKey:@"zx_ltRefUserInfos"];
	[aCoder encodeObject:self.scenicAbstract forKey:@"zx_scenicAbstract"];
	[aCoder encodeObject:self.scenicImage forKey:@"zx_scenicImage"];
	[aCoder encodeObject:self.longitude forKey:@"zx_longitude"];
	[aCoder encodeObject:self.latitude forKey:@"zx_latitude"];
	[aCoder encodeObject:self.ltRefUserNum forKey:@"zx_ltRefUserNum"];
	[aCoder encodeObject:self.price forKey:@"zx_price"];
	[aCoder encodeObject:self.hotVar forKey:@"zx_hotVar"];
	[aCoder encodeObject:self.city forKey:@"zx_city"];
	[aCoder encodeObject:self.scenicId forKey:@"zx_scenicId"];
	[aCoder encodeObject:self.scenicName forKey:@"zx_scenicName"];
	[aCoder encodeObject:self.sequence forKey:@"zx_sequence"];
	
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.ltRefUserInfos = [aDecoder decodeObjectForKey:@"zx_ltRefUserInfos"];
		self.scenicAbstract = [aDecoder decodeObjectForKey:@"zx_scenicAbstract"];
		self.scenicImage = [aDecoder decodeObjectForKey:@"zx_scenicImage"];
		self.longitude = [aDecoder decodeObjectForKey:@"zx_longitude"];
		self.latitude = [aDecoder decodeObjectForKey:@"zx_latitude"];
		self.ltRefUserNum = [aDecoder decodeObjectForKey:@"zx_ltRefUserNum"];
		self.price = [aDecoder decodeObjectForKey:@"zx_price"];
		self.hotVar = [aDecoder decodeObjectForKey:@"zx_hotVar"];
		self.city = [aDecoder decodeObjectForKey:@"zx_city"];
		self.scenicId = [aDecoder decodeObjectForKey:@"zx_scenicId"];
		self.scenicName = [aDecoder decodeObjectForKey:@"zx_scenicName"];
		self.sequence = [aDecoder decodeObjectForKey:@"zx_sequence"];
		
    }
    return self;
}

- (NSString *) description
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"ltRefUserInfos : %@\n",self.ltRefUserInfos];
	result = [result stringByAppendingFormat:@"scenicAbstract : %@\n",self.scenicAbstract];
	result = [result stringByAppendingFormat:@"scenicImage : %@\n",self.scenicImage];
	result = [result stringByAppendingFormat:@"longitude : %@\n",self.longitude];
	result = [result stringByAppendingFormat:@"latitude : %@\n",self.latitude];
	result = [result stringByAppendingFormat:@"ltRefUserNum : %@\n",self.ltRefUserNum];
	result = [result stringByAppendingFormat:@"price : %@\n",self.price];
	result = [result stringByAppendingFormat:@"hotVar : %@\n",self.hotVar];
	result = [result stringByAppendingFormat:@"city : %@\n",self.city];
	result = [result stringByAppendingFormat:@"scenicId : %@\n",self.scenicId];
	result = [result stringByAppendingFormat:@"scenicName : %@\n",self.scenicName];
	result = [result stringByAppendingFormat:@"sequence : %@\n",self.sequence];
	
    return result;
}

@end
