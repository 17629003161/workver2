//
//  ColloctionBlankFooterView.m
//  TravelNew
//
//  Created by mac on 2020/12/29.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ColloctionBlankFooterView.h"

@implementation ColloctionBlankFooterView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        UIImageView *imgv = ImageViewWithImage(@"no_data");
        UILabel *label = [Utils createLabelWithTitle:@"当前暂无数据"
                                          titleColor:HexColor(0x999999)
                                            fontSize:16];
        [self sd_addSubviews:@[imgv,label]];
        imgv.sd_layout
        .topSpaceToView(self, 60)
        .centerXEqualToView(self)
        .widthIs(190)
        .heightIs(134);
        
        label.sd_layout
        .topSpaceToView(imgv, 10)
        .centerXEqualToView(self)
        .autoHeightRatio(0);
        [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
        return self;
    }
    return self;
}

@end
