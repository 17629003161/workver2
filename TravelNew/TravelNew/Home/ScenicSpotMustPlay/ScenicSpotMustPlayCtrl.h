//
//  ScenicSpotMustPlayCtrl.h
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "LKBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScenicSpotMustPlayCtrl : LKBaseViewController
@property(nonatomic,strong) NSString *cityCode;
@end

NS_ASSUME_NONNULL_END
