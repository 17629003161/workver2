//
//  ScenicSpotMustPlayCtrl.m
//  TravelNew
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 lester. All rights reserved.
//

#import "ScenicSpotMustPlayCtrl.h"
#import "MastPlayCell.h"
#import "ScenicSpotMain.h"
#import "ColloctionBlankFooterView.h"

@interface ScenicSpotMustPlayCtrl ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,assign) NSInteger pageIndex;
@property(nonatomic,assign) NSInteger pageSize;
@property(nonatomic, strong) ColloctionBlankFooterView *footerView;

@end

@implementation ScenicSpotMustPlayCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"必玩景点" titleColor:nil];
    // Do any additional setup after loading the view.
    self.dataArray = [NSMutableArray new];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    //注册尾视图
    [_collectionView registerClass:[ColloctionBlankFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer"];
    
    [self PageRefreshSetup];
    [self loadFirstPage];
}

- (void)PageRefreshSetup{
    LKWeakSelf
    self.pageSize = 10;
    self.pageIndex = 1;
    [self.dataArray removeAllObjects];
    // 下拉刷新
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        DLog(@"下拉刷新");
        [weakSelf.collectionView.mj_header endRefreshing];
        [self loadFirstPage];
    }];

    // 上拉刷新
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        DLog(@"上拉刷新");
        [weakSelf.collectionView.mj_footer endRefreshing];
        [self loadMorePage];
    }];
}

- (void)loadFirstPage{
    self.pageIndex = 1;
    [self.dataArray removeAllObjects];
    [self.collectionView reloadData];
    [self loadMorePage];
}

- (void)loadMorePage{
    if(self.cityCode==nil)
        return;
    LKWeakSelf
    Api *api = [Api apiPostUrl:@"attractions/scenic/list"
                          para:@{@"cityid":self.cityCode,
                              @"pageIndex":@(self.pageIndex),
                               @"pageSize":@(self.pageSize)}
                   description:@"attractions - 查询城市景点列表"];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        NSLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        NSNumber *code = [json objectForKey:@"code"];
        if([code intValue]==200){
            NSArray *dataAry = [json objectForKey:@"data"];
            if(dataAry.count>0){
                for(NSDictionary *dict in dataAry){
                    ScenicSpotEntity *model = [[ScenicSpotEntity alloc] initWithJson:dict];
                    DLog(@"model = %@",model);
                    [weakSelf.dataArray addObject:model];
                }
                [weakSelf.collectionView reloadData];
            }else{
                
            }
            weakSelf.pageIndex++;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        DLog(@"%@",request.error);
        if([request.responseString containsString:@"登录失效"]){
            [MBProgressHUD showMessage:@"登录失效，请重新登录！"];
        }
    }];
}

- (UICollectionView *)collectionView{
    if(!_collectionView){
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        // 设置item的行间距和列间距
        layout.minimumInteritemSpacing = 9;
        layout.minimumLineSpacing = 13;
        // 设置item的大小
        layout.itemSize = CGSizeMake(107,163);
        // 设置每个分区的 上左下右 的内边距
        layout.sectionInset = UIEdgeInsetsMake(20, 18 ,20, 18);

        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        collectionView.backgroundColor = [UIColor whiteColor];
        collectionView.showsVerticalScrollIndicator = NO;   //是否显示滚动条
        collectionView.scrollEnabled = YES;  //滚动使能
        
        [collectionView registerClass:[MastPlayCell class]
        forCellWithReuseIdentifier:@"MastPlayCell"];
        
        //3、添加到控制器的view
        _collectionView = collectionView;
    }
    return _collectionView;
}

#pragma mark -collectionview 数据源方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;   //返回section数
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
      //每个section的Item数
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MastPlayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MastPlayCell" forIndexPath:indexPath];
    cell.model = [self.dataArray objectAtIndex:indexPath.item];
    return cell;

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ScenicSpotEntity *entity = [self.dataArray objectAtIndex:indexPath.item];
    ScenicSpotMain *vc = [ScenicSpotMain new];
    vc.scenicId = entity.scenicId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionFooter) {    //尾视图
        _footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer" forIndexPath:indexPath];
        reusableView = _footerView;
    }
    return reusableView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if(self.dataArray.count>0){
        return CGSizeZero;
    }else{
        return CGSizeMake(kScreenWidth, kScreenHeight);
    }
}

- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
   sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    return CGSizeMake((kScreenWidth-9-36)*0.5, 212);
}

   //边距设置:整体边距的优先级，始终高于内部边距的优先级
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout *)collectionViewLayout
insetForSectionAtIndex:(NSInteger)section
{
 return UIEdgeInsetsMake(20, 18, 20, 18);//分别为上、左、下、右
}


@end
