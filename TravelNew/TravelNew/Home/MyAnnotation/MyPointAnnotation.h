//
//  MyPointAnnotation.h
//  TravelNew
//
//  Created by mac on 2020/10/24.
//  Copyright © 2020 lester. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "RecommanderMapEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyPointAnnotation : MAPointAnnotation
@property(nonatomic,strong) RecommanderMapEntity *model;
@end

NS_ASSUME_NONNULL_END
