//
//  Home_SectionFooterView.m
//  TravelNew
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "Home_SectionFooterView.h"

@implementation Home_SectionFooterView
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self configView];
    }
    return self;
}

- (void)configView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.shadowColor = [UIColor colorWithRed:207/255.0 green:207/255.0 blue:207/255.0 alpha:0.5].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 6;
    _severView = [self creatViewWithImage:@"server_ok" title:@"服务保障" subTitle:@"7x24小时服务"];
    _verifyView = [self creatViewWithImage:@"verify_truce" title:@"真实认证" subTitle:@"多维度验真机制"];
    _safeView = [self creatViewWithImage:@"safe_out" title:@"安心出行" subTitle:@"多重保障安心玩"];
    [self sd_addSubviews:@[view,_severView,_verifyView,_safeView]];
    
    view.sd_layout
    .topSpaceToView(self, 30)
    .leftSpaceToView(self, 18)
    .rightSpaceToView(self, 18)
    .bottomEqualToView(self);
    
    _verifyView.sd_layout
    .topSpaceToView(self, 64)
    .centerXEqualToView(self)
    .widthIs(61)
    .heightIs(94);
    
    _severView.sd_layout
    .centerYEqualToView(_verifyView)
    .rightSpaceToView(_verifyView, 46)
    .widthIs(61)
    .heightIs(94);
    
    _safeView.sd_layout
    .centerYEqualToView(_verifyView)
    .leftSpaceToView(_verifyView, 46)
    .widthIs(61)
    .heightIs(94);
}

- (UIView *)creatViewWithImage:(NSString *)img
                     title:(NSString *)title
                  subTitle:(NSString *)subTitle{
    UIView *view = [UIView new];
    UIImageView *imgv = ImageViewWithImage(img);
    UILabel *label1 = [Utils createLabelWithTitle:title titleColor:HexColor(0x36364D) fontSize:14];
    UILabel *label2 = [Utils createLabelWithTitle:subTitle titleColor:HexColor(0xB0B0B0) fontSize:10.11];
    [view sd_addSubviews:@[imgv,label1,label2]];
    
    imgv.sd_layout
    .topEqualToView(view)
    .centerXEqualToView(view)
    .widthIs(65)
    .heightIs(40);
    
    label1.sd_layout
    .topSpaceToView(imgv, 5)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 2)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}


@end
