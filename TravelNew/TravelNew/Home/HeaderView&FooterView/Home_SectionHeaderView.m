//
//  Home_SectionHeaderView.m
//  TravelNew
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import "Home_SectionHeaderView.h"

@implementation Home_SectionHeaderView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UILabel *label1 = [Utils createLabelWithTitle:@"title" titleColor:HexColor(0x333333) fontSize:23];
        label1.font = [UIFont boldSystemFontOfSize:23];
        UILabel *label2 = [Utils createLabelWithTitle:@"查看全部" titleColor:HexColor(0x999999) fontSize:14];
        UIImageView *arrow = ImageViewWithImage(@"arrow_right");
        UILabel *label3 = [Utils createLabelWithTitle:@"subTitle" titleColor:HexColor(0x999999) fontSize:14];
        [self sd_addSubviews:@[label1,label2,arrow,
                               label3]];
        self.titleLable = label1;
        self.subTitleLabel = label3;
        self.moreLabel = label2;
        self.arrowImgv = arrow;
        
        label1.sd_layout
        .topSpaceToView(self, 28)
        .leftSpaceToView(self, 18)
        .autoHeightRatio(0);
        [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
        
        label3.sd_layout
        .topSpaceToView(label1, 1)
        .leftEqualToView(label1)
        .rightSpaceToView(self, 18)
        .autoHeightRatio(0);
        
        arrow.sd_layout
        .centerYEqualToView(label1)
        .rightSpaceToView(self, 18)
        .widthIs(6)
        .heightIs(10);
        
        label2.sd_layout
        .centerYEqualToView(arrow)
        .rightSpaceToView(arrow, 5)
        .autoHeightRatio(0);
        [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    }
    return self;
}



@end
