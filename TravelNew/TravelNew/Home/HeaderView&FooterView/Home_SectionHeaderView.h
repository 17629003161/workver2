//
//  Home_SectionHeaderView.h
//  TravelNew
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Home_SectionHeaderView : UICollectionReusableView

@property(strong,nonatomic)UILabel *titleLable;
@property(strong,nonatomic)UILabel *subTitleLabel;
@property(strong,nonatomic)UILabel *moreLabel;
@property(strong,nonatomic)UIImageView *arrowImgv;

@end

NS_ASSUME_NONNULL_END
