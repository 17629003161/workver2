//
//  Home_SectionFooterView.h
//  TravelNew
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Home_SectionFooterView : UICollectionReusableView
@property(strong,nonatomic)UIView *severView;
@property(strong,nonatomic)UIView *verifyView;
@property(strong,nonatomic)UIView *safeView;
@end

NS_ASSUME_NONNULL_END
