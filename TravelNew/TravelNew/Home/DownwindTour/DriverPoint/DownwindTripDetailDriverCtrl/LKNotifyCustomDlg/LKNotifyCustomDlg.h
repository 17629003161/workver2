//
//  LKNotifyCustomDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKNotifyCustomDlg : LKBaseDialogView
@property(nonatomic,strong) void (^toAgreeBlock)(void);
@property(nonatomic,strong) void (^toChatBlock)(void);
@property(nonatomic,strong) void (^toTelBlock)(void);
@end

NS_ASSUME_NONNULL_END
