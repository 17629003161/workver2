//
//  LKNotifyCustomDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKNotifyCustomDlg.h"

@interface LKNotifyCustomDlg()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UILabel *fromLabel;
@property(nonatomic,strong) UILabel *fromDetailLabel;
@property(nonatomic,strong) UILabel *toLabel;
@property(nonatomic,strong) UILabel *toDetailLabel;
@property(nonatomic,strong) UILabel *dateTimeLabel;
@property(nonatomic,strong) UIImageView *leftArrow;
@property(nonatomic,strong) UIImageView *rightArrow;
@property(nonatomic,strong) UILabel *middleAddressLabel;
@property(nonatomic,strong) UILabel *middleAddressLabel2;
@property(nonatomic,strong) UIImageView *singlArrow;
@property(nonatomic,strong) UIImageView *doubleArrow;

@property(nonatomic,assign) NSInteger isSingle;


@end

@implementation LKNotifyCustomDlg

-(void)setIsSingle:(NSInteger )isSingle
{
    _isSingle = isSingle;
    _singlArrow.hidden = YES;
    _doubleArrow.hidden = YES;
    _middleAddressLabel.hidden = YES;
    _middleAddressLabel2.hidden = YES;
    if(isSingle){
        _singlArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
    }else{
        _doubleArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
        _middleAddressLabel2.hidden = NO;
    }
}
- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    //[self hide];
    if(self.toAgreeBlock){
        self.toAgreeBlock();
    }
}

- (void)chatIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
    if(self.toChatBlock){
        self.toChatBlock();
    }
}

- (void)phonIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
    if(self.toTelBlock){
        self.toTelBlock();
    }
}

- (void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    _imgv = ImageViewWithImage(@"header_image");
    _imgv.layer.cornerRadius = 16;
    _imgv.layer.masksToBounds = YES;
    _nameLabel = [Utils createLabelWithTitle:@"尾号7865" titleColor:HexColor(0x333333) fontSize:13];
    _descLabel = [Utils createLabelWithTitle:@"已预付车费" titleColor:HexColor(0xFF9B37) fontSize:11];
    UIImageView *chatImgv = ImageViewWithImage(@"order_btn_chat_big");
    [chatImgv addTapGestureTarget:self action:@selector(chatIsClicked:)];
    UIImageView *phoneCallImgv = ImageViewWithImage(@"order_btn_phone_big_n");
    [phoneCallImgv addTapGestureTarget:self action:@selector(phonIsClicked:)];
    _dateTimeLabel = [Utils createBoldLabelWithTitle:@"3月30日 14:30～15:30出发·2人" titleColor:HexColor(0x333333) fontSize:14];
    _fromLabel = [Utils createBoldLabelWithTitle:@"西安" titleColor:HexColor(0x141418) fontSize:18];
    _fromDetailLabel = [Utils createLabelWithTitle:@"利君未来城2期" titleColor:HexColor(0x333333) fontSize:12];
    _toLabel = [Utils createBoldLabelWithTitle:@"成都" titleColor:HexColor(0x141418) fontSize:18];
    _toDetailLabel = [Utils createLabelWithTitle:@"宽窄巷子地铁" titleColor:HexColor(0x333333) fontSize:12];
    _middleAddressLabel = [Utils createLabelWithTitle:@"汉中" titleColor:HexColor(0x4D84FD) fontSize:10];
    _middleAddressLabel2 = [Utils createLabelWithTitle:@"安康" titleColor:HexColor(0x4D84FD) fontSize:10];
    _singlArrow = ImageViewWithImage(@"trip_icon_single");
    _doubleArrow = ImageViewWithImage(@"trip_icon_return");
    UIView *view = [self createView:CGRectMake(0, 195, kScreenWidth, 40)];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"出发并通知乘客" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];

    
    [self sd_addSubviews:@[_imgv,_nameLabel,_descLabel,_dateTimeLabel,
                              _fromLabel,_fromDetailLabel,
                              _toLabel,_toDetailLabel,
                              _middleAddressLabel,_middleAddressLabel2,
                              _singlArrow,_doubleArrow,
                              view,btn,chatImgv,phoneCallImgv]];

    _imgv.sd_layout
    .topSpaceToView(self, 27)
    .leftSpaceToView(self, 25)
    .widthIs(32)
    .heightIs(32);
    
    _nameLabel.sd_layout
    .topEqualToView(_imgv)
    .leftSpaceToView(_imgv, 9.5)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _descLabel.sd_layout
    .leftEqualToView(_nameLabel)
    .topSpaceToView(_nameLabel, 5)
    .autoHeightRatio(0);
    [_descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneCallImgv.sd_layout
    .topSpaceToView(self, 22)
    .rightSpaceToView(self, 26.5)
    .widthIs(52)
    .heightIs(45);
    
    chatImgv.sd_layout
    .centerYEqualToView(phoneCallImgv)
    .rightSpaceToView(phoneCallImgv, 14)
    .widthIs(52)
    .heightIs(45);
    
    _dateTimeLabel.sd_layout
    .topSpaceToView(_imgv, 27.5)
    .leftSpaceToView(self, 32.5)
    .autoHeightRatio(0);
    [_dateTimeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    
    _fromLabel.sd_layout
    .topSpaceToView(_dateTimeLabel, 20)
    .leftSpaceToView(self, 55)
    .autoHeightRatio(0);
    [_fromLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _fromDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .leftSpaceToView(self, 32)
    .autoHeightRatio(0);
    [_fromDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toLabel.sd_layout
    .topSpaceToView(_dateTimeLabel, 20)
    .rightSpaceToView(self, 55)
    .autoHeightRatio(0);
    [_toLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .rightSpaceToView(self, 32)
    .autoHeightRatio(0);
    [_toDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _singlArrow.sd_layout
    .topSpaceToView(_fromLabel, 7)
    .centerXEqualToView(self)
    .widthIs(55)
    .heightIs(4);
    
    _doubleArrow.sd_layout
    .centerXEqualToView(_singlArrow)
    .centerYEqualToView(_singlArrow)
    .widthIs(55)
    .heightIs(13);
    
    _middleAddressLabel.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _middleAddressLabel2.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    view.sd_layout
    .topSpaceToView(_fromDetailLabel, 17.5)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(40);
    
    btn.sd_layout
    .topSpaceToView(view,25)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
}


-(void)show
{
    [super show];
    self.maskView.hidden = YES;
    self.isSingle = YES;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

-(UIView *)createView:(CGRect)frame
{
    UIView *bgView = [[UIView alloc] initWithFrame:frame];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    NSArray *titleAry = @[@"市区免费接送",
                          @"爱干净",
                          @"会开车"];
    CGFloat pointX = 32.0; //button X坐标
    CGFloat pointY = 10.0; //button Y坐标
    CGFloat padding = 15.0; //button 间距
    CGFloat btnHeight = 30; //button高度
    CGFloat allWidth = frame.size.width - 32;
    UIFont *titleFont = [UIFont systemFontOfSize:15];
    for (int i = 0; i < titleAry.count; i++) {
        CGRect rect = [titleAry[i] boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : titleFont} context:nil];
        CGFloat btnWidth = rect.size.width + 20;
        
        if (pointX + btnWidth > allWidth) {//换行
            pointX = 10;//X从新开始
            pointY += (btnHeight + padding);//换行后Y+
        }
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        [but setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [but setBackgroundImage:[UIImage imageWithColor:HexColor(0xF1F6FF)] forState:UIControlStateSelected];
        but.tag = i;
        but.frame = CGRectMake(pointX, pointY, btnWidth, btnHeight);
        but.tag = i + 1000;
        [but addTarget:self action:@selector(clickButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        but.layer.masksToBounds = YES;
        but.layer.cornerRadius = btnHeight/2;
        but.layer.borderWidth = 1;
        but.layer.borderColor = HexColor(0x4D84FD).CGColor;
        [but setTitleColor:HexColor(0x4D84FD) forState:UIControlStateNormal];
        [but setTitle:titleAry[i] forState:UIControlStateNormal];
        but.titleLabel.font = titleFont;//一定要一样
        pointX += (btnWidth + padding);//每次X都加上button宽和间距5
        [but setSelected:NO];
        [bgView addSubview:but];
    }
    CGRect rect2 = bgView.frame;
    rect2.size.height = pointY + btnHeight + 10;
    bgView.frame = rect2;
    return bgView;
}
 
-(void)clickButtonAction:(UIButton *)sender
{
    UIButton *btn = (UIButton *)sender;
    if (btn.isSelected) {
        [btn setSelected:NO];
        btn.layer.borderColor = HexColor(0xcfcfcf).CGColor;
    }else{
        [btn setSelected:YES];
        btn.layer.borderColor = HexColor(0x4d9bfd).CGColor;
    }
}


@end
