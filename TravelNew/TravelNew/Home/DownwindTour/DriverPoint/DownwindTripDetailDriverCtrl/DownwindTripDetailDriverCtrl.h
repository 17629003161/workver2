//
//  DownwindTripDetailDriverCtrl.h
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseViewController.h"

typedef NS_ENUM(NSInteger, TripStepDriver){
    TripStepDriverInviteAgree=0,         //同意乘客邀请
    TripStepDriverNotify = 1,             //通知乘客出发
    TripStepCustomCall = 2,              //呼叫乘客
    TripStepTripOverDriver = 3,           //旅行结束
    TripStepWaitCustomBesure = 4,            //等待乘客确认
    TripStepCustomIsBesure =5,              //乘客已确认
    TripStepSelectCar = 6                   //选择车辆
};

NS_ASSUME_NONNULL_BEGIN

@interface DownwindTripDetailDriverCtrl : LKBaseViewController
@property(nonatomic,assign) TripStepDriver tripStep;
@end

NS_ASSUME_NONNULL_END
