//
//  LKCustomCallDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKCustomCallDlg.h"

@implementation LKCustomCallDlg

- (void)quitIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

-(void)configViews
{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(@"trip_img_cancel");
    UIImageView *quitImgv = ImageViewWithImage(@"close_x");
    [quitImgv addTapGestureTarget:self action:@selector(quitIsClicked:)];
    UILabel *label1 = [Utils createLabelWithTitle:@"即将用以下号码加密呼出" titleColor:HexColor(0x383232) fontSize:18];
    UILabel *label2 = [Utils createLabelWithTitle:@"为了保证服务质量，接听期间您的通话将被录音。" titleColor:HexColor(0x999999) fontSize:13];
    UILabel *label3 = [Utils createBoldLabelWithTitle:@"132 **** 7190" titleColor:HexColor(0x383232) fontSize:24];
    UILabel *label4 = [Utils createLabelWithTitle:@"若不是本机号码  请修改" titleColor:HexColor(0x999999) fontSize:13];
    [label4 changeStrings:@[@"请修改"] toColor:HexColor(0x4D83FD)];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"加密呼叫" font:[UIFont systemFontOfSize:18] cornerRadius:23];
    [self sd_addSubviews:@[imgv,quitImgv,label1,label2,label3,label4,btn]];
    imgv.sd_layout
    .topSpaceToView(self, 22)
    .centerXEqualToView(self)
    .widthIs(226)
    .heightIs(160);
    quitImgv.sd_layout
    .topSpaceToView(self, 23.5)
    .rightSpaceToView(self, 24.5)
    .widthIs(30)
    .heightIs(30);
    label1.sd_layout
    .topSpaceToView(imgv, 15)
    .leftSpaceToView(self, 25.5)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 11.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label3.sd_layout
    .topSpaceToView(label2, 29.5)
    .leftEqualToView(label2)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label4.sd_layout
    .topSpaceToView(label3, 14.5)
    .leftEqualToView(label3)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(label4, 30)
    .leftSpaceToView(self, 25.5)
    .rightSpaceToView(self, 25.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomViewsArray:@[btn] bottomMargin:43];
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

@end
