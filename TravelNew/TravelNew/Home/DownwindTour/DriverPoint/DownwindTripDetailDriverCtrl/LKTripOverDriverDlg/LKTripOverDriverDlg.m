//
//  LKTripOverDriverDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKTripOverDriverDlg.h"

@interface LKTripOverDriverDlg()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) NSMutableArray *btnArray;

@end

@implementation LKTripOverDriverDlg

- (void)btnsIsClicked:(id)sender
{
    for (UIButton *btn in self.btnArray) {
        btn.layer.borderColor = HexColor(0xF8F9FB).CGColor;
        [btn setSelected:NO];
        if(btn == sender){
            [btn setSelected:YES];
            btn.layer.borderColor = HexColor(0x4D84FD).CGColor;
        }
    }
}

-(NSMutableArray *)btnArray
{
    if(!_btnArray){
        _btnArray = [NSMutableArray new];
        NSArray *titleArray = @[@"值得称赞",@"还可以",@"仍有不足"];
        for (int i=0; i<titleArray.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:titleArray[i] forState:UIControlStateNormal];
            [btn setTitleColor:HexColor(0x484848) forState:UIControlStateNormal];
            btn.titleLabel.font = LKSystemFont(14);
            [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xF8F9FB)] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageWithColor:HexColor(0xF8F9FB)] forState:UIControlStateSelected];
            btn.layer.borderColor = HexColor(0xF1F6FF).CGColor;
            btn.layer.borderWidth = 1.0;
            btn.layer.cornerRadius = 4.0;
            [btn addTarget:self action:@selector(btnsIsClicked:) forControlEvents:UIControlEventTouchUpInside];
            [_btnArray addObject:btn];
        }
    }
    return _btnArray;
}

- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)chatIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)phonIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *tiplabel1 = [Utils createBoldLabelWithTitle:@"此次旅行结束" titleColor:HexColor(0x202020) fontSize:18];
    UILabel *tiplabel2 = [Utils createLabelWithTitle:@"感谢参与绿色出行" titleColor:HexColor(0x808080) fontSize:12];
    UIView *line1 = [UIView new];
    line1.backgroundColor = HexColor(0xF0F0F0);

    UIView *line2 = [UIView new];
    line2.backgroundColor = HexColor(0xF0F0F0);
    UILabel *tipLabel3 = [Utils createBoldLabelWithTitle:@"本次同行的旅游体验如何？" titleColor:HexColor(0x202020) fontSize:15];
    [self sd_addSubviews:self.btnArray];
    
    _imgv = ImageViewWithImage(@"header_image");
    _imgv.layer.cornerRadius = 16;
    _imgv.layer.masksToBounds = YES;
    _nameLabel = [Utils createLabelWithTitle:@"陕A9588" titleColor:HexColor(0x333333) fontSize:13];
    _descLabel = [Utils createLabelWithTitle:@"赵师傅 | 奔驰·白色" titleColor:HexColor(0x999999) fontSize:11];
    UIImageView *chatImgv = ImageViewWithImage(@"order_btn_chat_big");
    [chatImgv addTapGestureTarget:self action:@selector(chatIsClicked:)];
    UIImageView *phoneCallImgv = ImageViewWithImage(@"order_btn_phone_big_n");
    [phoneCallImgv addTapGestureTarget:self action:@selector(phonIsClicked:)];

    [self sd_addSubviews:@[tiplabel1,tiplabel2,
                           line1,
                           line2,
                           tipLabel3,
                           _imgv,_nameLabel,_descLabel,chatImgv,phoneCallImgv
    ]];
    tiplabel1.sd_layout
    .topSpaceToView(self, 25)
    .leftSpaceToView(self, 30)
    .autoHeightRatio(0);
    [tiplabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    tiplabel2.sd_layout
    .topSpaceToView(tiplabel1, 12)
    .leftEqualToView(tiplabel1)
    .autoHeightRatio(0);
    [tiplabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line1.sd_layout
    .topSpaceToView(tiplabel2, 20)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(1);
    
    tipLabel3.sd_layout
    .leftSpaceToView(self, 30)
    .topSpaceToView(line1, 20.5)
    .autoHeightRatio(0);
    [tipLabel3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line2.sd_layout
    .topSpaceToView(line1, 105.5)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(1);
    
    CGFloat w = (kScreenWidth-75)/3.0;
    for (int i=0; i<3; i++) {
        UIButton *btn = self.btnArray[i];
        btn.sd_layout
        .topSpaceToView(tipLabel3, 14.5)
        .leftSpaceToView(self, 30+i*(w+7.5))
        .widthIs(w)
        .heightIs(36);
    }
    
    _imgv.sd_layout
    .topSpaceToView(line2, 17.5)
    .leftSpaceToView(self, 25)
    .widthIs(32)
    .heightIs(32);
    
    _nameLabel.sd_layout
    .topEqualToView(_imgv)
    .leftSpaceToView(_imgv, 9.5)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _descLabel.sd_layout
    .leftEqualToView(_nameLabel)
    .topSpaceToView(_nameLabel, 5)
    .autoHeightRatio(0);
    [_descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneCallImgv.sd_layout
    .centerYEqualToView(_imgv)
    .rightSpaceToView(self, 26.5)
    .widthIs(52)
    .heightIs(45);
    
    chatImgv.sd_layout
    .centerYEqualToView(phoneCallImgv)
    .rightSpaceToView(phoneCallImgv, 14)
    .widthIs(52)
    .heightIs(45);
    [self setupAutoHeightWithBottomView:_imgv bottomMargin:40];
}
-(void)show
{
    [super show];
    self.maskView.hidden = YES;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    
}
@end
