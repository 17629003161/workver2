//
//  DownwindTripDetailDriverCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindTripDetailDriverCtrl.h"
#import "LKInviteAgreeDriverDlg.h"
#import "LKNotifyCustomDlg.h"
#import "LKCustomCallDlg.h"
#import "LKTripOverDriverDlg.h"
#import "LKCustomBesureDlg.h"

@interface DownwindTripDetailDriverCtrl ()
@property(nonatomic,strong) LKInviteAgreeDriverDlg *invitDlg;
@property(nonatomic,strong) LKNotifyCustomDlg *notifyDlg;
@property(nonatomic,strong) LKCustomCallDlg *customCallDlg;
@property(nonatomic,strong) LKTripOverDriverDlg *tripOverDlg;
@property(nonatomic,strong) LKCustomBesureDlg *besureDlg;
@property(nonatomic,strong) UIView *guidView;
@property(nonatomic,assign) NSInteger dlgIndex;
@end

@implementation DownwindTripDetailDriverCtrl
-(LKInviteAgreeDriverDlg *)invitDlg
{
    if(!_invitDlg){
        _invitDlg = [LKInviteAgreeDriverDlg new];
    }
    return _invitDlg;
}

-(LKNotifyCustomDlg *)notifyDlg
{
    if(!_notifyDlg){
        _notifyDlg = [LKNotifyCustomDlg new];
    }
    return _notifyDlg;
}

-(LKCustomCallDlg *)customCallDlg
{
    if(!_customCallDlg){
        _customCallDlg = [LKCustomCallDlg new];
    }
    return _customCallDlg;
}

-(LKTripOverDriverDlg *)tripOverDlg
{
    if(!_tripOverDlg){
        _tripOverDlg = [LKTripOverDriverDlg new];
    }
    return _tripOverDlg;
}

-(LKCustomBesureDlg *)besureDlg
{
    if(!_besureDlg){
        _besureDlg = [LKCustomBesureDlg new];
    }
    return _besureDlg;
}

-(UIView *)guidView
{
    if(!_guidView){
        _guidView = [self createGuidView];
    }
    return _guidView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"行程详情" titleColor:nil];
    [self.view addSubview:self.mapView];
    self.mapView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self refreshStep];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self closeAllDialogs];
}

- (void)closeAllDialogs{
    switch (self.tripStep) {
        case TripStepDriverInviteAgree:
            [self.invitDlg hide];
            break;
        case TripStepDriverNotify:
            [self.notifyDlg hide];
            break;
        case TripStepCustomCall:
            [self.customCallDlg hide];
            break;
        case TripStepTripOverDriver:
            [self.tripOverDlg hide];
            break;
        case TripStepWaitCustomBesure:{
            [self.besureDlg hide];
            [self.guidView removeFromSuperview];
        }
            break;
        default:
            break;
    }
}


- (void)refreshStep{
    switch (self.tripStep) {
        case TripStepDriverInviteAgree:
            [self.invitDlg show];
            break;
        case TripStepDriverNotify:
            [self.notifyDlg show];
            break;
        case TripStepCustomCall:
            [self.customCallDlg show];
            break;
        case TripStepTripOverDriver:
            [self.tripOverDlg show];
            break;
        case TripStepWaitCustomBesure:{
            [self.besureDlg show];
            [self.view addSubview:self.guidView];
        }
            break;
        default:
            break;
    }
}

- (UIView *)createGuidView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 88)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"请提醒乘客确认上车" titleColor:HexColor(0x202020) fontSize:18];
    UILabel *label2 = [Utils createLabelWithTitle:@"1.02公里，5分钟 | 若乘客长时间未确认..." titleColor:HexColor(0x999999) fontSize:12];
    [label2 changeStrings:@[@"1.02",@"5"] toColor:HexColor(0x1E69FF)];
    UIImageView *guidImgv = ImageViewWithImage(@"daohang");
    UILabel *label3 = [Utils createLabelWithTitle:@"导航" titleColor:HexColor(0x202020) fontSize:11];
    [view sd_addSubviews:@[label1,label2,guidImgv,label3]];
    label1.sd_layout
    .topSpaceToView(view, 19)
    .leftSpaceToView(view, 19.5)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .topSpaceToView(label1, 14.5)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    guidImgv.sd_layout
    .topSpaceToView(view, 20.5)
    .rightSpaceToView(view, 25)
    .widthIs(23.5)
    .heightIs(23.5);
    
    label3.sd_layout
    .centerXEqualToView(guidImgv)
    .topSpaceToView(guidImgv, 6.5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}



@end
