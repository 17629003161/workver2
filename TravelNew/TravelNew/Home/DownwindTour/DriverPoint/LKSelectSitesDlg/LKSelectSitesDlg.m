//
//  LKSelectSitesDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKSelectSitesDlg.h"
@interface LKSelectSitesDlg()
@property(nonatomic,strong) NSMutableArray *btnAry;
@property(nonatomic,assign) NSInteger selectIndex;
@end

@implementation LKSelectSitesDlg

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    for (int i=0; i<self.btnAry.count; i++) {
        UIButton *btn = self.btnAry[i];
        btn.layer.borderColor = HexColor(0xF8F9FB).CGColor;
        if(i+1==selectIndex){
            btn.layer.borderColor = UIColor.blueColor.CGColor;
        }
    }
}

- (void)btnFunIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self setSelectIndex:btn.tag];
}

- (void)quitIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)configViews{
    [super configViews];
    CGFloat w = (kScreenWidth-50-15)/4.0;
    UILabel *label = [Utils createBoldLabelWithTitle:@"可提供的座位数" titleColor:HexColor(0x383232) fontSize:18];
    UIImageView *quitImgv = ImageViewWithImage(@"close_x");
    [quitImgv addTapGestureTarget:self action:@selector(quitIsClicked:)];
    _btnAry = [NSMutableArray new];
    for (int i=0; i<4; i++) {
        NSString *title = [NSString stringWithFormat:@"%d座",i+1];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.borderWidth = 1.0;
        btn.layer.cornerRadius = 4.0;
        btn.layer.borderColor = HexColor(0xF8F9FB).CGColor;
        btn.tag = i+1;
        [btn setTitleColor:HexColor(0x333333) forState:UIControlStateNormal];
        btn.titleLabel.font = LKSystemFont(15);
        btn.backgroundColor = HexColor(0xF8F9FB);
        [btn setTitle:title forState:UIControlStateNormal];
        [self.btnAry addObject:btn];
        [self addSubview:btn];
        [btn addTarget:self action:@selector(btnFunIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.sd_layout
        .topSpaceToView(self, 77)
        .leftSpaceToView(self, 25+i*(w+5))
        .widthIs(w)
        .heightIs(50);
    }
    [self addSubview:label];
    [self addSubview:quitImgv];
    LKGadentButton *okbtn = [LKGadentButton blueButtonWhithTitle:@"确定" font:LKSystemFont(18) cornerRadius:23];
    [okbtn addTapGestureTarget:self action:@selector(okbtnIsClicked:)];
    [self addSubview:okbtn];
    
    label.sd_layout
    .topSpaceToView(self, 30)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    quitImgv.sd_layout
    .topSpaceToView(self, 14.5)
    .rightSpaceToView(self, 14.5)
    .widthIs(30)
    .heightIs(30);
    
    okbtn.sd_layout
    .topSpaceToView(label, 112)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:okbtn bottomMargin:41];
    self.selectIndex = 0;
}

- (void)okbtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.selectBlock){
        self.selectBlock(self.selectIndex);
    }
    [self hide];
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

@end
