//
//  LKSelectCarDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/30.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKSelectCarDlg.h"

@interface LKSelectCarDlg()
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UIPageControl *pageCtrl;
@property(nonatomic,strong) NSMutableArray *viewsAry;
@property(nonatomic,assign) NSInteger pageNumber;
@end

@implementation LKSelectCarDlg

-(UIScrollView *)scrollView
{
    if(!_scrollView){
        _scrollView = [UIScrollView new];
    }
    return _scrollView;
}

-(UIPageControl *)pageCtrl
{
    if(!_pageCtrl){
        _pageCtrl = [UIPageControl new];
    }
    return _pageCtrl;
}

-(NSMutableArray *)viewsAry
{
    if(!_viewsAry){
        _viewsAry = [NSMutableArray new];
    }
    return _viewsAry;
}

-(void)setPageNumber:(NSInteger)pageNumber
{
    _pageNumber = pageNumber;
    _pageCtrl.numberOfPages = pageNumber;
    self.scrollView.contentSize = CGSizeMake(kScreenWidth-39, 158.5);
    for(int i=0;i<pageNumber;i++){
        UIView *view = [self createContentView];
        view.frame = CGRectMake(self.scrollView.size.width*i, 0, self.scrollView.frame.size.width, self.frame.size.height);
        [self.scrollView addSubview:view];
    }
}

-(void)configViews{
    [super configViews];
    UILabel *label = [Utils createLabelWithTitle:@"选择您的车辆" titleColor:HexColor(0x383232) fontSize:18];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认" font:[UIFont systemFontOfSize:18] cornerRadius:23];
    [self sd_addSubviews:@[label,self.scrollView,self.pageCtrl,btn]];
    label.sd_layout
    .topSpaceToView(self, 30)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    self.scrollView.sd_layout
    .topSpaceToView(label, 30)
    .leftSpaceToView(self, 19.5)
    .rightSpaceToView(self, 19.5)
    .heightIs(158.5);
    
    self.pageCtrl.sd_layout
    .topSpaceToView(self.scrollView, 8)
    .centerXEqualToView(self)
    .widthIs(40)
    .heightIs(10);
    
    btn.sd_layout
    .topSpaceToView(self.pageCtrl, 26)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
}


- (UIView *)createContentView{
    UIView *bgview = [UIView new];
    bgview.backgroundColor = HexColor(0xbfbfbf);
    UIImageView *icon = ImageViewWithImage(@"default_header");
    UILabel *nameLabel = [Utils createBoldLabelWithTitle:@"银色 宝来" titleColor:HexColor(0x333333) fontSize:18];
    UIImageView *verifyImgv = ImageViewWithImage(@"car_verify");
    [bgview sd_addSubviews:@[icon,nameLabel,verifyImgv]];
    for(int i=0; i<3;i++){
        UIView *view = [self creatButtonView];
        [self.viewsAry addObject:view];
    }
    [bgview sd_addSubviews:self.viewsAry];
    icon.sd_layout
    .topSpaceToView(bgview, 30)
    .leftSpaceToView(bgview, 30)
    .widthIs(48)
    .heightIs(48);
    nameLabel.sd_layout
    .centerYEqualToView(icon)
    .leftSpaceToView(icon, 30)
    .autoHeightRatio(0);
    [nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    verifyImgv.sd_layout
    .centerYEqualToView(icon)
    .rightSpaceToView(bgview, 30)
    .widthIs(60)
    .heightIs(20);
    CGFloat gap = 19;
    CGFloat w = (kScreenWidth-39-4*19)/3.0;
    for (int i=0; i<self.viewsAry.count; i++) {
        UIView *view = self.viewsAry[i];
        view.sd_layout
        .topSpaceToView(icon, 26)
        .leftSpaceToView(bgview, gap+i*(w+gap))
        .widthIs(w)
        .heightIs(23);
    }
    return bgview;
}

- (UIView *)creatButtonView{
    UIView *view = [UIView new];
    view.layer.borderColor = HexColor(0x9f9f9f).CGColor;
    view.layer.cornerRadius = 6;
    view.layer.borderWidth = 1.0;
    view.layer.masksToBounds = YES;
    UILabel *label1 = [Utils createLabelWithTitle:@"车牌号" titleColor:HexColor(0x9f9f9f) fontSize:12];
    UILabel *label2 = [Utils createLabelWithTitle:@"陕A77F1R" titleColor:HexColor(0x333333) fontSize:16];
    [view sd_addSubviews:@[label1,label2]];
    label1.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(view, 10)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .centerXEqualToView(view)
    .topSpaceToView(label1, 8)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

@end
