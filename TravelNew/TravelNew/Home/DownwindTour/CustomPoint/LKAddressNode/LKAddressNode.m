//
//  LKAddressNode.m
//  TravelNew
//
//  Created by mac on 2021/3/31.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKAddressNode.h"

@implementation LKAddressNode
- (instancetype)initWithName:(NSString *)name coordinate:(CLLocationCoordinate2D)coordinate cityId:(NSString *)cityId cityName:(NSString *)cityName
{
    self = [super init];
    if(self){
        self.name = name;
        self.coordinate = coordinate;
        self.cityId = cityId;
        self.cityName = cityName;
    }
    return self;
}

@end
