//
//  LKAddressNode.h
//  TravelNew
//
//  Created by mac on 2021/3/31.
//  Copyright © 2021 lester. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LKAddressNode : NSObject
@property(nonatomic,assign) CLLocationCoordinate2D coordinate;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *cityName;
@property(nonatomic,strong) NSString *cityId;
- (instancetype)initWithName:(NSString *)name coordinate:(CLLocationCoordinate2D)coordinate cityId:(NSString *)cityId cityName:(NSString *)cityName;
@end

NS_ASSUME_NONNULL_END
