//
//  LKCancleDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/25.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKCancleDlg.h"

@implementation LKCancleDlg

- (void)quitIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

-(void)configViews
{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *imgv = ImageViewWithImage(@"trip_img_cancel");
    UIImageView *quitImgv = ImageViewWithImage(@"close_x");
    [quitImgv addTapGestureTarget:self action:@selector(quitIsClicked:)];
    UILabel *label1 = [Utils createLabelWithTitle:@"确认要取消吗？" titleColor:HexColor(0x383232) fontSize:18];
    UILabel *label2 = [Utils createLabelWithTitle:@"车主邀请需要一些时间，建议等待10-15分钟" titleColor:HexColor(0x999999) fontSize:13];
    UIButton *btnWait = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnWait setTitle:@"继续等待" forState:UIControlStateNormal];
    [btnWait setTitleColor:HexColor(0x4D83FD) forState:UIControlStateNormal];
    btnWait.layer.borderColor = HexColor(0x4D83FD).CGColor;
    btnWait.layer.borderWidth = 0.5;
    btnWait.layer.cornerRadius = 23;
    btnWait.layer.masksToBounds = YES;

    LKGadentButton *btnCancle = [LKGadentButton blueButtonWhithTitle:@"取消行程" font:[UIFont systemFontOfSize:18] cornerRadius:23];
    [self sd_addSubviews:@[imgv,quitImgv,label1,label2,btnCancle,btnWait]];
    imgv.sd_layout
    .topSpaceToView(self, 22)
    .centerXEqualToView(self)
    .widthIs(226)
    .heightIs(160);
    quitImgv.sd_layout
    .topSpaceToView(self, 23.5)
    .rightSpaceToView(self, 24.5)
    .widthIs(30)
    .heightIs(30);
    label1.sd_layout
    .topSpaceToView(imgv, 15)
    .leftSpaceToView(self, 25)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .leftEqualToView(label1)
    .topSpaceToView(label1, 11.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    CGFloat w = (kScreenWidth-52-10)/2.0;
    btnWait.sd_layout
    .topSpaceToView(label2, 29.5)
    .leftSpaceToView(self, 26)
    .widthIs(w)
    .heightIs(46);
    btnCancle.sd_layout
    .centerYEqualToView(btnWait)
    .leftSpaceToView(btnWait, 10)
    .widthIs(w)
    .heightIs(46);
    [self setupAutoHeightWithBottomViewsArray:@[btnWait,btnCancle] bottomMargin:43];
    
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

@end
