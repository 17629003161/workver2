//
//  LKPayDialog.m
//  TravelNew
//
//  Created by mac on 2021/3/25.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKPayDialog.h"

@interface LKPayDialog()
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,strong) UIButton *wxBtn;
@property(nonatomic,strong) UIButton *zfbBtn;

@end

@implementation LKPayDialog

- (void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    [self.wxBtn setSelected:NO];
    [self.zfbBtn setSelected:NO];
    if(selectIndex==0){
        [self.zfbBtn setSelected:YES];
    }else if(selectIndex==1){
        [self.wxBtn setSelected:YES];
    }
}

- (void)iconIsClicked:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn==self.zfbBtn){
        self.selectIndex = 0;
    }else{
        self.selectIndex = 1;
    }
}

-(void)configViews
{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"¥" titleColor:HexColor(0x333333) fontSize:11];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"987.6" titleColor:HexColor(0x333333) fontSize:15];
    UILabel *label3 = [Utils createLabelWithTitle:@"顺丰游车费支付" titleColor:HexColor(0x484848) fontSize:11];
    UIImageView *wxIcon = ImageViewWithImage(@"weixinzhif");
    UILabel *wxlabel = [Utils createLabelWithTitle:@"微信支付" titleColor:HexColor(0x303030) fontSize:15];
    UIButton *wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [wxBtn addTarget:self action:@selector(iconIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.wxBtn = wxBtn;
    [wxBtn setImage:[UIImage imageNamed:@"CombinedShapeCopy3"] forState:UIControlStateNormal];
    [wxBtn setImage:[UIImage imageNamed:@"CombinedShape"] forState:UIControlStateSelected];
    
    UIImageView *zfbIcon = ImageViewWithImage(@"zfblogo");
    UILabel *zfblabel = [Utils createLabelWithTitle:@"支付宝" titleColor:HexColor(0x303030) fontSize:15];
    UIButton *zfbBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [zfbBtn addTarget:self action:@selector(iconIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.zfbBtn = zfbBtn;
    [zfbBtn setImage:[UIImage imageNamed:@"CombinedShapeCopy3"] forState:UIControlStateNormal];
    [zfbBtn setImage:[UIImage imageNamed:@"CombinedShape"] forState:UIControlStateSelected];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认支付" font:[UIFont systemFontOfSize:18] cornerRadius:23];
    [self sd_addSubviews:@[label1,label2,
                           label3,
                           zfbIcon,zfblabel,zfbBtn,
                           wxIcon,wxlabel,wxBtn,
                           btn]];
    label2.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self, 35)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label1.sd_layout
    .bottomEqualToView(label2)
    .rightSpaceToView(label2, 6)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(label3, 11.5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbIcon.sd_layout
    .topSpaceToView(self, 105)
    .leftSpaceToView(self, 40)
    .widthIs(20.5)
    .heightIs(20.5);
    
    zfblabel.sd_layout
    .centerYEqualToView(zfbIcon)
    .leftSpaceToView(zfbIcon, 14.5)
    .autoHeightRatio(0);
    [zfblabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    zfbBtn.sd_layout
    .centerYEqualToView(zfblabel)
    .rightSpaceToView(self, 40)
    .widthIs(15)
    .heightIs(15);
    
    wxIcon.sd_layout
    .topSpaceToView(self, 151)
    .leftSpaceToView(self, 40)
    .widthIs(20.5)
    .heightIs(20.5);
    
    wxlabel.sd_layout
    .centerYEqualToView(wxIcon)
    .leftSpaceToView(wxIcon, 14.5)
    .autoHeightRatio(0);
    [wxlabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    wxBtn.sd_layout
    .centerYEqualToView(wxlabel)
    .rightSpaceToView(self, 40)
    .widthIs(15)
    .heightIs(15);
    
    btn.sd_layout
    .topSpaceToView(wxlabel, 41.5)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:btn bottomMargin:50];
}

-(void)show
{
    [super show];
    self.maskView.hidden = YES;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    [self makeTopAngleSize:CGSizeMake(30, 30) bounds:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}


@end
