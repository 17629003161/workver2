//
//  LKWaitDriverOutDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKWaitDriverOutDlg.h"

@interface LKWaitDriverOutDlg()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) UILabel *fromLabel;
@property(nonatomic,strong) UILabel *fromDetailLabel;
@property(nonatomic,strong) UILabel *toLabel;
@property(nonatomic,strong) UILabel *toDetailLabel;
@property(nonatomic,strong) UILabel *dateTimeLabel;
@property(nonatomic,strong) UIImageView *leftArrow;
@property(nonatomic,strong) UIImageView *rightArrow;
@property(nonatomic,strong) UILabel *middleAddressLabel;
@property(nonatomic,strong) UILabel *middleAddressLabel2;
@property(nonatomic,strong) UIImageView *singlArrow;
@property(nonatomic,strong) UIImageView *doubleArrow;
@property(nonatomic,strong) UILabel *aboutDayLabel;
@property(nonatomic,strong) UIImageView *carImgv;
@property(nonatomic,strong) UILabel *carLabel;

@property(nonatomic,assign) NSInteger isSingle;


@end

@implementation LKWaitDriverOutDlg

-(void)setIsSingle:(NSInteger )isSingle
{
    _isSingle = isSingle;
    _singlArrow.hidden = YES;
    _doubleArrow.hidden = YES;
    _middleAddressLabel.hidden = YES;
    _middleAddressLabel2.hidden = YES;
    if(isSingle){
        _singlArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
    }else{
        _doubleArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
        _middleAddressLabel2.hidden = NO;
    }
}
- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)chatIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)phonIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];

}

- (void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *tiplabel1 = [Utils createBoldLabelWithTitle:@"等待车主出发" titleColor:HexColor(0x202020) fontSize:18];
    UILabel *tiplabel2 = [Utils createLabelWithTitle:@"请在3月22日12:00前 到达起点" titleColor:HexColor(0x808080) fontSize:12];
    [tiplabel2 changeStrings:@[@"3月22日12:00"] toColor:HexColor(0x4D84FD)];
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setBackgroundColor:HexColor(0xF2F2F2)];
    [btnCancel setTitle:@"取消订单" forState:UIControlStateNormal];
    btnCancel.titleLabel.font = LKSystemFont(12);
    [btnCancel setTitleColor:HexColor(0x484848) forState:UIControlStateNormal];
    btnCancel.layer.cornerRadius = 14;
    btnCancel.layer.masksToBounds = YES;
    
    _imgv = ImageViewWithImage(@"header_image");
    _imgv.layer.cornerRadius = 16;
    _imgv.layer.masksToBounds = YES;
    _nameLabel = [Utils createLabelWithTitle:@"小王" titleColor:HexColor(0x333333) fontSize:13];
    _descLabel = [Utils createLabelWithTitle:@"出行次数 | 好评率100%" titleColor:HexColor(0x999999) fontSize:11];
    UIImageView *chatImgv = ImageViewWithImage(@"order_btn_chat_big");
    [chatImgv addTapGestureTarget:self action:@selector(chatIsClicked:)];
    UIImageView *phoneCallImgv = ImageViewWithImage(@"order_btn_phone_big_n");
    [phoneCallImgv addTapGestureTarget:self action:@selector(phonIsClicked:)];
    _fromLabel = [Utils createBoldLabelWithTitle:@"西安" titleColor:HexColor(0x141418) fontSize:18];
    _fromDetailLabel = [Utils createLabelWithTitle:@"利君未来城2期" titleColor:HexColor(0x333333) fontSize:12];
    _toLabel = [Utils createBoldLabelWithTitle:@"成都" titleColor:HexColor(0x141418) fontSize:18];
    _toDetailLabel = [Utils createLabelWithTitle:@"宽窄巷子地铁" titleColor:HexColor(0x333333) fontSize:12];
    _middleAddressLabel = [Utils createLabelWithTitle:@"汉中" titleColor:HexColor(0x4D84FD) fontSize:10];
    _middleAddressLabel2 = [Utils createLabelWithTitle:@"安康" titleColor:HexColor(0x4D84FD) fontSize:10];
    _singlArrow = ImageViewWithImage(@"trip_icon_single");
    _doubleArrow = ImageViewWithImage(@"trip_icon_return");
    _dateTimeLabel = [Utils createBoldLabelWithTitle:@"3月22日 12:00～13:00" titleColor:HexColor(0x666666) fontSize:12];
    _aboutDayLabel = [Utils createBoldLabelWithTitle:@"预计自驾7天" titleColor:HexColor(0x666666) fontSize:12];
    _carImgv = ImageViewWithImage(@"header_default");
    _carLabel = [Utils createBoldLabelWithTitle:@"奔驰越野 S6 | 3座 | 单程" titleColor:HexColor(0x333333) fontSize:14];
    UIView *view = [self createView:CGRectMake(0, 195, kScreenWidth, 40)];
    LKGadentButton *btnSure = [LKGadentButton blueButtonWhithTitle:@"确认上车" font:LKSystemFont(18) cornerRadius:23];
    [btnSure addTapGestureTarget:self action:@selector(btnIsClicked:)];
    UIButton *btn110 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn110 setBackgroundColor:HexColor(0xFFDDDD)];
    [btn110 setTitle:@"拨打110" forState:UIControlStateNormal];
    [btn110 setTitleColor:HexColor(0xFF584B) forState:UIControlStateNormal];
    btn110.layer.cornerRadius = 23;

    [self sd_addSubviews:@[tiplabel1,tiplabel2,btnCancel,
                           _imgv,_nameLabel,_descLabel,chatImgv,phoneCallImgv,
                              _fromLabel,_fromDetailLabel,
                              _toLabel,_toDetailLabel,
                              _middleAddressLabel,_middleAddressLabel2,
                              _singlArrow,_doubleArrow,
                              _dateTimeLabel,_aboutDayLabel,_carImgv,_carLabel,view,btn110,btnSure]];
    tiplabel1.sd_layout
    .topSpaceToView(self, 30)
    .leftSpaceToView(self, 30)
    .autoHeightRatio(0);
    [tiplabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    tiplabel2.sd_layout
    .topSpaceToView(tiplabel1, 12)
    .leftEqualToView(tiplabel1)
    .autoHeightRatio(0);
    [tiplabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btnCancel.sd_layout
    .topSpaceToView(self, 36.5)
    .rightSpaceToView(self, 27.5)
    .widthIs(77)
    .heightIs(28);
    
    _imgv.sd_layout
    .topSpaceToView(tiplabel2, 25)
    .leftSpaceToView(self, 25)
    .widthIs(32)
    .heightIs(32);
    
    _nameLabel.sd_layout
    .topEqualToView(_imgv)
    .leftSpaceToView(_imgv, 9.5)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _descLabel.sd_layout
    .leftEqualToView(_nameLabel)
    .topSpaceToView(_nameLabel, 5)
    .autoHeightRatio(0);
    [_descLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    phoneCallImgv.sd_layout
    .centerYEqualToView(_imgv)
    .rightSpaceToView(self, 26.5)
    .widthIs(52)
    .heightIs(45);
    
    chatImgv.sd_layout
    .centerYEqualToView(phoneCallImgv)
    .rightSpaceToView(phoneCallImgv, 14)
    .widthIs(52)
    .heightIs(45);
    
    _fromLabel.sd_layout
    .topSpaceToView(_descLabel, 29)
    .leftSpaceToView(self, 55)
    .autoHeightRatio(0);
    [_fromLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _fromDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .leftSpaceToView(self, 32)
    .autoHeightRatio(0);
    [_fromDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toLabel.sd_layout
    .topSpaceToView(_descLabel, 29)
    .rightSpaceToView(self, 55)
    .autoHeightRatio(0);
    [_toLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .rightSpaceToView(self, 32)
    .autoHeightRatio(0);
    [_toDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _singlArrow.sd_layout
    .topSpaceToView(_fromLabel, 7)
    .centerXEqualToView(self)
    .widthIs(55)
    .heightIs(4);
    
    _doubleArrow.sd_layout
    .centerXEqualToView(_singlArrow)
    .centerYEqualToView(_singlArrow)
    .widthIs(55)
    .heightIs(13);
    
    _middleAddressLabel.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _middleAddressLabel2.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    _dateTimeLabel.sd_layout
    .topSpaceToView(_fromDetailLabel, 11.5)
    .leftEqualToView(_fromDetailLabel)
    .autoHeightRatio(0);
    [_dateTimeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _aboutDayLabel.sd_layout
    .centerYEqualToView(_dateTimeLabel)
    .rightSpaceToView(self, 31.5)
    .autoHeightRatio(0);
    [_aboutDayLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _carImgv.sd_layout
    .leftEqualToView(_dateTimeLabel)
    .topSpaceToView(_dateTimeLabel, 15)
    .widthIs(18)
    .heightIs(18);
    
    _carLabel.sd_layout
    .centerYEqualToView(_carImgv)
    .leftSpaceToView(_carImgv, 12.5)
    .autoHeightRatio(0);
    [_carLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    view.sd_layout
    .topSpaceToView(_carLabel, 17.5)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(40);
    
    btn110.sd_layout
    .topSpaceToView(view, 25)
    .leftSpaceToView(self, 27.5)
    .widthIs(118)
    .heightIs(46);
    
    btnSure.sd_layout
    .topSpaceToView(view,25)
    .leftSpaceToView(btn110, 10)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    
    [self setupAutoHeightWithBottomView:btnSure bottomMargin:40];
}


-(void)show
{
    [super show];
    self.maskView.hidden = YES;
    self.isSingle = YES;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

-(UIView *)createView:(CGRect)frame
{
    UIView *bgView = [[UIView alloc] initWithFrame:frame];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    NSArray *titleAry = @[@"市区免费接送",
                          @"爱干净",
                          @"会开车"];
    CGFloat pointX = 32.0; //button X坐标
    CGFloat pointY = 10.0; //button Y坐标
    CGFloat padding = 15.0; //button 间距
    CGFloat btnHeight = 30; //button高度
    CGFloat allWidth = frame.size.width - 32;
    UIFont *titleFont = [UIFont systemFontOfSize:15];
    for (int i = 0; i < titleAry.count; i++) {
        CGRect rect = [titleAry[i] boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : titleFont} context:nil];
        CGFloat btnWidth = rect.size.width + 20;
        
        if (pointX + btnWidth > allWidth) {//换行
            pointX = 10;//X从新开始
            pointY += (btnHeight + padding);//换行后Y+
        }
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        [but setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [but setBackgroundImage:[UIImage imageWithColor:HexColor(0xF1F6FF)] forState:UIControlStateSelected];
        but.tag = i;
        but.frame = CGRectMake(pointX, pointY, btnWidth, btnHeight);
        but.tag = i + 1000;
        [but addTarget:self action:@selector(clickButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        but.layer.masksToBounds = YES;
        but.layer.cornerRadius = btnHeight/2;
        but.layer.borderWidth = 1;
        but.layer.borderColor = HexColor(0x4D84FD).CGColor;
        [but setTitleColor:HexColor(0x4D84FD) forState:UIControlStateNormal];
        [but setTitle:titleAry[i] forState:UIControlStateNormal];
        but.titleLabel.font = titleFont;//一定要一样
        pointX += (btnWidth + padding);//每次X都加上button宽和间距5
        [but setSelected:NO];
        [bgView addSubview:but];
    }
    CGRect rect2 = bgView.frame;
    rect2.size.height = pointY + btnHeight + 10;
    bgView.frame = rect2;
    return bgView;
}
 
-(void)clickButtonAction:(UIButton *)sender
{
    UIButton *btn = (UIButton *)sender;
    if (btn.isSelected) {
        [btn setSelected:NO];
        btn.layer.borderColor = HexColor(0xcfcfcf).CGColor;
    }else{
        [btn setSelected:YES];
        btn.layer.borderColor = HexColor(0x4d9bfd).CGColor;
    }
}


@end
