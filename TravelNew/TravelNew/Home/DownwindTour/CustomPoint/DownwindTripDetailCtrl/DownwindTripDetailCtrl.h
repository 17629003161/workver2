//
//  DownwindTripDetailCtrl.h
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseViewController.h"

typedef NS_ENUM(NSInteger, TripStep){
    TripStepInvite=0,         //邀请司机
    TripSure=1,             //确认同行
    TripToPay = 2,          //去支付
    TripStepCancle = 3,     //取消订单
    TripStepWaitOut = 4,  //等待车主出发
    TripStepArive = 5,     //到达目的地
    TripStepTripOver = 6,    //旅行结束
    TripStepPay2 = 7        //支付提示框
};

NS_ASSUME_NONNULL_BEGIN

@interface DownwindTripDetailCtrl : LKBaseViewController
@property(nonatomic,assign) TripStep tripStep;
@end

NS_ASSUME_NONNULL_END
