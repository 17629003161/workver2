//
//  LKTripBesureDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/25.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKTripBesureDlg : LKBaseDialogView
@property(nonatomic,strong) void (^toPayBlock)(void);
@property(nonatomic,strong) void (^toChatBlock)(void);
@property(nonatomic,strong) void (^toTelBlock)(void);
@end

NS_ASSUME_NONNULL_END
