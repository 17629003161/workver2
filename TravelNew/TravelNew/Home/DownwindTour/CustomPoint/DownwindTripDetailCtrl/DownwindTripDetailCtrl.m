//
//  DownwindTripDetailCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/29.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindTripDetailCtrl.h"
#import "LKInvitDriverDlg.h"
#import "LKTripBesureDlg.h"
#import "LKPayDialog.h"
#import "LKCancleDlg.h"
#import "LKWaitDriverOutDlg.h"
#import "LKAriveDlg.h"
#import "LKTripOverDlg.h"
#import "LKPayDialog2.h"



@interface DownwindTripDetailCtrl ()

@property(nonatomic,strong) LKInvitDriverDlg *invitDlg;
@property(nonatomic,strong) LKTripBesureDlg *tripSureDlg;
@property(nonatomic,strong) LKPayDialog *payDlg;
@property(nonatomic,strong) LKCancleDlg *cancleDlg;
@property(nonatomic,strong) LKWaitDriverOutDlg *waitOutDlg;
@property(nonatomic,strong) LKAriveDlg *ariveDlg;
@property(nonatomic,strong) LKTripOverDlg *tripOverDlg;
@property(nonatomic,strong) LKPayDialog2 *payDlg2;
@property(nonatomic,assign) NSInteger dlgIndex;

@end

@implementation DownwindTripDetailCtrl

-(LKInvitDriverDlg *)invitDlg
{
    if(!_invitDlg){
        _invitDlg = [LKInvitDriverDlg new];
    }
    return _invitDlg;
}

-(LKTripBesureDlg *)tripSureDlg
{
    if(!_tripSureDlg){
        _tripSureDlg = [LKTripBesureDlg new];
    }
    return _tripSureDlg;
}

-(LKPayDialog *)payDlg
{
    if(!_payDlg){
        _payDlg = [LKPayDialog new];
    }
    return _payDlg;
}

- (LKCancleDlg *)cancleDlg
{
    if(!_cancleDlg){
        _cancleDlg = [LKCancleDlg new];
    }
    return _cancleDlg;
}

- (LKWaitDriverOutDlg *)waitOutDlg
{
    if(!_waitOutDlg){
        _waitOutDlg = [LKWaitDriverOutDlg new];
    }
    return _waitOutDlg;
}

-(LKAriveDlg *)ariveDlg
{
    if(!_ariveDlg){
        _ariveDlg = [LKAriveDlg new];
    }
    return _ariveDlg;
}

-(LKTripOverDlg *)tripOverDlg
{
    if(!_tripOverDlg){
        _tripOverDlg = [LKTripOverDlg new];
    }
    return _tripOverDlg;
}

-(LKPayDialog2 *)payDlg2
{
    if(!_payDlg2){
        _payDlg2 = [LKPayDialog2 new];
    }
    return _payDlg2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"行程详情" titleColor:nil];
    [self.view addSubview:self.mapView];
    self.mapView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [self refreshStep];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self closeAllDialogs];
}

- (void)closeAllDialogs{
    switch (self.tripStep) {
        case TripStepInvite:
            [self.invitDlg hide];
            break;
        case TripSure:
            [self.tripSureDlg hide];
            break;
        case TripToPay:
            [self.payDlg hide];
            break;
        case TripStepCancle:
            [self.cancleDlg hide];
            break;
        case TripStepWaitOut:
            [self.waitOutDlg hide];
            break;
        case TripStepArive:
            [self.ariveDlg hide];
            break;
        case TripStepTripOver:
            [self.tripOverDlg hide];
            break;
        case TripStepPay2:
            [self.payDlg2 hide];
            break;
        default:
            break;
    }
}


- (void)refreshStep{
    switch (self.tripStep) {
        case TripStepInvite:
            [self showInviteDlg];
            break;
        case TripSure:
            [self showTripsureDlg];
            break;
        case TripToPay:
            [self showPayDlg];
            break;
        case TripStepCancle:
            [self showCancleDlg];
            break;
        case TripStepWaitOut:
            [self.waitOutDlg show];
            break;
        case TripStepArive:
            [self.ariveDlg show];
            break;
        case TripStepTripOver:
            [self.tripOverDlg show];
            break;
        case TripStepPay2:
            [self.payDlg2 show];
            break;
        default:
            break;
    }
}


- (void)showInviteDlg{
    [self.invitDlg show];
}

- (void)showPayDlg{
    [self.payDlg show];
}

- (void)showTripsureDlg{
    LKWeakSelf
    LKTripBesureDlg *dlg = self.tripSureDlg;
    dlg.toPayBlock = ^{;
        weakSelf.tripStep = TripToPay;
        [weakSelf refreshStep];
    };
    dlg.toChatBlock = ^{
        
    };
    dlg.toTelBlock = ^{
        
    };
    [dlg show];
}

- (void)showCancleDlg
{
    [self.cancleDlg show];
}

@end
