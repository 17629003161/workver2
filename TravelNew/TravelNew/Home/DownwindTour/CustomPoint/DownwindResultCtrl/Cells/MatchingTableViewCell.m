//
//  MatchingTableViewCell.m
//  TravelNew
//
//  Created by mac on 2021/3/22.
//  Copyright © 2021 lester. All rights reserved.
//

#import "MatchingTableViewCell.h"

@interface MatchingTableViewCell()
@property(nonatomic,strong) UILabel *matchlabel;
@property(nonatomic,strong) UILabel *fromLabel;
@property(nonatomic,strong) UILabel *fromDetailLabel;
@property(nonatomic,strong) UILabel *toLabel;
@property(nonatomic,strong) UILabel *toDetailLabel;
@property(nonatomic,strong) UIImageView *singlArrow;
@property(nonatomic,strong) UIImageView *doubleArrow;
@property(nonatomic,strong) UILabel *dateTimeLabel;
@property(nonatomic,strong) UILabel *middleAddressLabel;
@property(nonatomic,strong) UILabel *middleAddressLabel2;
@property(nonatomic,strong) UILabel *dayNumberLabel;
@property(nonatomic,strong) UIImageView *carIconImgv;
@property(nonatomic,strong) UILabel *carDetailLabel;
@property(nonatomic,strong) NSMutableArray *tagArray;
@property(nonatomic,strong) UIImageView *headerImgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *commentLabel;
@property(nonatomic,assign) BOOL isSingle;

@end

@implementation MatchingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsSingle:(BOOL )isSingle
{
    _isSingle = isSingle;
    _singlArrow.hidden = YES;
    _doubleArrow.hidden = YES;
    _middleAddressLabel.hidden = YES;
    _middleAddressLabel2.hidden = YES;
    if(isSingle){
        _singlArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
    }else{
        _doubleArrow.hidden = NO;
        _middleAddressLabel.hidden = NO;
        _middleAddressLabel2.hidden = NO;
    }
}


- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.funcBlock){
        self.funcBlock();
    }
}

-(void)configViews
{
    [super configViews];
    UIView *shadeview = [UIView new];
    shadeview.layer.borderColor = HexColor(0xe1e1e1).CGColor;
    shadeview.layer.borderWidth = 0.5;
    shadeview.layer.cornerRadius = 13;
    shadeview.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    shadeview.layer.shadowOffset = CGSizeMake(0,3);
    shadeview.layer.shadowOpacity = 1;
    shadeview.layer.cornerRadius = 10;
    shadeview.layer.masksToBounds = YES;
    [self.contentView addSubview:shadeview];
    _matchlabel = [Utils createBoldLabelWithTitle:@"旅行匹配度95%" titleColor:[UIColor whiteColor] fontSize:12];
    _matchlabel.textAlignment = NSTextAlignmentCenter;
    _matchlabel.backgroundColor = HexColor(0x1E69FF);
    _fromLabel = [Utils createBoldLabelWithTitle:@"西安" titleColor:HexColor(0x141418) fontSize:18];
    _fromDetailLabel = [Utils createLabelWithTitle:@"利君未来城2期" titleColor:HexColor(0x333333) fontSize:12];
    _toLabel = [Utils createBoldLabelWithTitle:@"成都" titleColor:HexColor(0x141418) fontSize:18];
    _toDetailLabel = [Utils createLabelWithTitle:@"宽窄巷子地铁" titleColor:HexColor(0x333333) fontSize:12];
    _middleAddressLabel = [Utils createLabelWithTitle:@"汉中" titleColor:HexColor(0x4D84FD) fontSize:10];
    _middleAddressLabel2 = [Utils createLabelWithTitle:@"莫高窟" titleColor:HexColor(0x4D84FD) fontSize:10];
    _singlArrow = ImageViewWithImage(@"trip_icon_single");
    _doubleArrow = ImageViewWithImage(@"trip_icon_return");
    _dateTimeLabel = [Utils createBoldLabelWithTitle:@"3月22日 12:00～13:00" titleColor:HexColor(0x666666) fontSize:12];
    _dayNumberLabel = [Utils createBoldLabelWithTitle:@"预计自驾7天" titleColor:HexColor(0x666666) fontSize:12];
    _carIconImgv = ImageViewWithImage(@"trip_icon_single");
    _carDetailLabel = [Utils createBoldLabelWithTitle:@"奔驰越野 S6 | 3座 | 单程" titleColor:HexColor(0x333333) fontSize:14];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0xBFBFBF);
    _headerImgv = ImageViewWithImage(@"default_header");
    _nameLabel = [Utils createLabelWithTitle:@"小王" titleColor:HexColor(0x333333) fontSize:13];
    _commentLabel = [Utils createLabelWithTitle:@"出行次数 | 好评率100%" titleColor:HexColor(0x999999) fontSize:11];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"邀请接单" font:[UIFont systemFontOfSize:16] cornerRadius:18];
    [btn addTapGestureTarget:self action:@selector(btnIsClicked:)];
    [shadeview sd_addSubviews:@[_matchlabel,
                                _fromLabel,_fromDetailLabel,
                                _toLabel,_toDetailLabel,
                                _singlArrow,_doubleArrow,_middleAddressLabel,_middleAddressLabel2,
                                _dateTimeLabel,_dayNumberLabel,
                                _carIconImgv,_carDetailLabel,
                                line,
                                _headerImgv,
                                _nameLabel,
                                _commentLabel,
                                btn]];
    _matchlabel.sd_layout
    .topEqualToView(shadeview)
    .leftEqualToView(shadeview)
    .widthIs(106.5)
    .heightIs(23.5);
    
    _fromLabel.sd_layout
    .topSpaceToView(_matchlabel, 24.5)
    .leftSpaceToView(shadeview, 43.5)
    .autoHeightRatio(0);
    [_fromLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _fromDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .leftSpaceToView(shadeview, 20)
    .autoHeightRatio(0);
    [_fromDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toLabel.sd_layout
    .centerYEqualToView(_fromLabel)
    .rightSpaceToView(shadeview, 55)
    .autoHeightRatio(0);
    [_toLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .rightSpaceToView(shadeview, 20)
    .autoHeightRatio(0);
    [_toDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    _dateTimeLabel.sd_layout
    .topSpaceToView(_fromDetailLabel, 11.5)
    .leftEqualToView(_fromDetailLabel)
    .autoHeightRatio(0);
    [_dateTimeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _dayNumberLabel.sd_layout
    .centerYEqualToView(_dateTimeLabel)
    .rightSpaceToView(shadeview, 19.5)
    .autoHeightRatio(0);
    [_dayNumberLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _singlArrow.sd_layout
    .topSpaceToView(_fromLabel, 7)
    .centerXEqualToView(self)
    .widthIs(55)
    .heightIs(4);
    
    _doubleArrow.sd_layout
    .centerXEqualToView(_singlArrow)
    .centerYEqualToView(_singlArrow)
    .widthIs(55)
    .heightIs(13);
    
    _middleAddressLabel.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _middleAddressLabel2.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _carIconImgv.sd_layout
    .leftEqualToView(_dateTimeLabel)
    .topSpaceToView(_dateTimeLabel, 15)
    .widthIs(18)
    .heightIs(18);
    
    _carDetailLabel.sd_layout
    .centerYEqualToView(_carIconImgv)
    .leftSpaceToView(_carIconImgv, 12.5)
    .autoHeightRatio(0);
    [_carDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    line.sd_layout
    .topSpaceToView(_carDetailLabel, 44)
    .leftSpaceToView(shadeview, 21)
    .rightSpaceToView(shadeview, 20)
    .heightIs(0.5);
    
    _headerImgv.sd_layout
    .leftSpaceToView(shadeview, 17.5)
    .topSpaceToView(line, 15)
    .widthIs(34)
    .heightIs(34);
    
    _nameLabel.sd_layout
    .leftSpaceToView(_headerImgv, 15)
    .topSpaceToView(line, 19)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _commentLabel.sd_layout
    .leftEqualToView(_nameLabel)
    .topSpaceToView(_nameLabel, 5)
    .autoHeightRatio(0);
    [_commentLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(line, 18)
    .rightSpaceToView(shadeview, 16)
    .widthIs(114)
    .heightIs(36);
    
    shadeview.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(self.contentView, 12)
    .rightSpaceToView(self.contentView, 12)
    .bottomEqualToView(self.contentView);
    [self setupAutoHeightWithBottomView:shadeview bottomMargin:0];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_matchlabel.bounds
                                                   byRoundingCorners: UIRectCornerBottomRight
                                                         cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _matchlabel.bounds;
    maskLayer.path = maskPath.CGPath;
    _matchlabel.layer.mask = maskLayer;
    self.isSingle = YES;
}

-(void)setTagArray:(NSMutableArray *)tagArray
{
    if(!_tagArray){
        _tagArray = [NSMutableArray new];
    }
    [_tagArray addObjectsFromArray:@[@"市区免费接送",@"爱干净",@"会开车"]];
   // [_tagArray addObjectsFromArray:tagArray];
    
}


@end
