//
//  MatchingTableViewCell.h
//  TravelNew
//
//  Created by mac on 2021/3/22.
//  Copyright © 2021 lester. All rights reserved.
//

#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MatchingTableViewCell : BaseCustomTableViewCell
@property(nonatomic,copy) void (^funcBlock)(void);
@end

NS_ASSUME_NONNULL_END
