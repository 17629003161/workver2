//
//  DownwindResultCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/22.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindResultCtrl.h"
#import "MatchingTableViewCell.h"
#import "DownwindTripDetailCtrl.h"

@interface DownwindResultCtrl ()

@end

@implementation DownwindResultCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"行程详情" titleColor:nil];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupUI{
    UIView *topview = [self showResultView];
    [self.view addSubview:topview];
    UIView *payview = [self createPayView];
    [self.view addSubview:payview];
    payview.sd_layout
    .topSpaceToView(topview, 20)
    .leftSpaceToView(self.view, 12)
    .rightSpaceToView(self.view, 12)
    .heightIs(62);
    
    self.tableView.sd_layout
    .topSpaceToView(payview, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self.tableView registerClass:[MatchingTableViewCell class] forCellReuseIdentifier:@"MatchingTableViewCell"];
}
- (void)cancleIsClicked:(id)sender
{
    DownwindTripDetailCtrl *vc = [DownwindTripDetailCtrl new];
    vc.tripStep = TripStepCancle;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)backIsClicked:(UITapGestureRecognizer *)reg
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)showResultView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 192.5)];
    view.backgroundColor = HexColor(0xF9FBFF);
    UIImageView *backarrow = ImageViewWithImage(@"left_arrow_black");
    [backarrow addTapGestureTarget:self action:@selector(backIsClicked:)];
    UILabel *label = [Utils createLabelWithTitle:@"行程详情" titleColor:HexColor(0x333333) fontSize:16];
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    [but setImage:[UIImage imageNamed:@"order_btn_more"] forState:UIControlStateNormal];
    [but addTarget:self action:@selector(cancleIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"等待车主接单" titleColor:HexColor(0x202020) fontSize:18];
    UILabel *label2 = [Utils createLabelWithTitle:@"车主平均邀请时间10-60分钟，请耐心等待···" titleColor:HexColor(0x202020) fontSize:12];
    UILabel *label3 = [Utils createLabelWithTitle:@"利君未来城2期   ->   西安北站" titleColor:HexColor(0x202020) fontSize:13];
    UILabel *label4 = [Utils createLabelWithTitle:@"今天 10:50～11:20 | 9天 | 3人 | 单程 " titleColor:HexColor(0x484848) fontSize:11];
    UIImageView *imgv = ImageViewWithImage(@"order_img_await");
    [view sd_addSubviews:@[backarrow,but,imgv,label,label1,label2,label3,label4,]];
    backarrow.sd_layout
    .topSpaceToView(view, 27)
    .leftSpaceToView(view, 17)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .centerYEqualToView(backarrow)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    but.sd_layout
    .centerYEqualToView(backarrow)
    .rightSpaceToView(view, 21.5)
    .widthIs(25)
    .heightIs(25);
    
    label1.sd_layout
    .topSpaceToView(backarrow, 26.5)
    .leftSpaceToView(view, 19.5)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(label1, 15)
    .leftEqualToView(label1)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .topSpaceToView(label2, 14.5)
    .leftEqualToView(label2)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label4.sd_layout
    .topSpaceToView(label3, 11.5)
    .leftEqualToView(label3)
    .autoHeightRatio(0);
    [label4 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label4 bottomMargin:15];
    
    imgv.sd_layout
    .rightEqualToView(view)
    .bottomEqualToView(view)
    .widthIs(127.5)
    .heightIs(115.5);
    
    return view;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LKWeakSelf
    MatchingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MatchingTableViewCell"];
    cell.funcBlock = ^{
        DownwindTripDetailCtrl *vc = [DownwindTripDetailCtrl new];
        vc.tripStep = TripStepInvite;
        vc.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (UIView *)createPayView
{
    UIView *view = [UIView new];
    view.layer.borderColor = HexColor(0xe1e1e1).CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 13;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,3);
    view.layer.shadowOpacity = 1;
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;
    UILabel *label = [Utils createBoldLabelWithTitle:@"预付车费" titleColor:HexColor(0x141418) fontSize:15];
    UILabel *label2 = [Utils createLabelWithTitle:@"预付后，将自动确认与接单车主同行" titleColor:HexColor(0x333333) fontSize:12];
    UILabel *label3 = [Utils createLabelWithTitle:@"去支付" titleColor:HexColor(0x1E69FF) fontSize:12];
    [label3 addTapGestureTarget:self action:@selector(toPayIsClicked:)];
    [view sd_addSubviews:@[label,label2,label3]];
    label.sd_layout
    .topSpaceToView(view, 13)
    .leftSpaceToView(view, 25)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .leftEqualToView(label)
    .topSpaceToView(label, 9.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .centerYEqualToView(view)
    .rightSpaceToView(view, 19.5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}

- (void)toPayIsClicked:(UITapGestureRecognizer *)reg
{
    DownwindTripDetailCtrl *vc = [DownwindTripDetailCtrl new];
    vc.tripStep = TripToPay;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
