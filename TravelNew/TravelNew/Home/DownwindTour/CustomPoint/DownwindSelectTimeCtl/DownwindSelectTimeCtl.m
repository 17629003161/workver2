//
//  DownwindSelectTimeCtl.m
//  TravelNew
//
//  Created by mac on 2021/3/23.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindSelectTimeCtl.h"
#import "LKDateTimeSelectDlg.h"
#import "LKDateTimeSelectDlg2.h"
#import "LKDownwindPublishDlg.h"
#import "DownwindResultCtrl.h"
#import "DownwindResultDriverCtrl.h"
#import "PriceDetailCtrl.h"
#import "LKNoteDlg.h"

@interface DownwindSelectTimeCtl ()
@property(nonatomic,strong) LKDateUtils *startDate;
@property(nonatomic,strong) LKDateUtils *stopDate;
@property(nonatomic,strong) LKDateTimeSelectDlg *dlg;
@property(nonatomic,strong) LKDateTimeSelectDlg2 *dlg2;
@property(nonatomic,strong) LKDownwindPublishDlg *publishDlg;
@property(nonatomic,assign) NSInteger number;
@property(nonatomic,assign) NSInteger dayNumber;
@property(nonatomic,strong) NSString *note ;
@end

@implementation DownwindSelectTimeCtl
- (LKDateTimeSelectDlg *)dlg
{
    if(!_dlg){
        _dlg = [LKDateTimeSelectDlg new];
        [_dlg setDate];
    }
    return _dlg;
}

- (LKDateTimeSelectDlg2 *)dlg2
{
    if(!_dlg2){
        _dlg2 = [LKDateTimeSelectDlg2 new];
    }
    return _dlg2;
}

- (LKDownwindPublishDlg *)publishDlg
{
    if(!_publishDlg){
        _publishDlg = [LKDownwindPublishDlg new];
    }
    return _publishDlg;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"行程详情" titleColor:nil];
    [self.view addSubview:self.mapView];
    self.mapView.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view);
    [self addAnntion:self.address1];
    [self addAnntion:self.address2];
    [self addAnntion:self.address3];
    [self addAnntion:self.address4];
    
}

- (void)addAnntion:(LKAddressNode *)addr
{
    if(addr==nil) return;
    MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
    annotation.coordinate = addr.coordinate;
    annotation.title = addr.cityName;
    annotation.subtitle = addr.name;
    [self.mapView addAnnotation:annotation];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self refreshStep];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(self.publishDlg.isShowing){
        [self.publishDlg hide];
    }
}

- (void)refreshStep{
    if(self.dlg.isShowing){
        [self.dlg hide];
    }
    if(self.dlg2.isShowing){
        [self.dlg2 hide];
    }
    if(self.publishDlg.isShowing){
        [self.publishDlg hide];
    }
    switch (self.tripStep) {
        case TripStepTime:
            [self showDateSelectDlg];
            break;
        case TripStepPublish:
            [self showPublishDlg];
            break;
        default:
            break;
    }
}

- (void)showDateSelectDlg{
    LKWeakSelf
    self.dlg.selectDateBlock = ^(LKDateUtils * _Nonnull date) {
        weakSelf.startDate = date;
        DLog(@"startDate:%@",date.dateTimeString);
        weakSelf.dlg2.date = date;
        weakSelf.dlg2.selectDateBlock = ^(LKDateUtils * _Nonnull date2) {
            weakSelf.stopDate = date2;
            DLog(@"endDate:%@",date2.dateTimeString);
            weakSelf.tripStep = TripStepPublish;
            [weakSelf refreshStep];
        };
        weakSelf.dlg2.uperStepBlock = ^{
            [weakSelf showDateSelectDlg];
        };
        [weakSelf.dlg2 show];
    };
    [self.dlg show];
}

- (void)showPublishDlg{
    LKDownwindPublishDlg *dlg = self.publishDlg;
    dlg.isDoubleTrip = self.isDoubleTrip;
    dlg.address1 = self.address1;
    dlg.address2 = self.address2;
    dlg.address3 = self.address3;
    dlg.address4 = self.address4;
    NSMutableArray *addrAry = [NSMutableArray new];
    [addrAry addObject:self.address1];
    if(self.address2)
        [addrAry addObject:self.address2];
    if(self.address3)
        [addrAry addObject:self.address3];
    [addrAry addObject:self.address4];
    [self drawPolayLine:addrAry];
    
    dlg.numbers = 1;
    dlg.dayNumber = 3;
    self.number = dlg.numbers;
    self.dayNumber = dlg.dayNumber;
    self.note = @"";
    if([self.startDate.dateString isEqualToString:self.stopDate.dateString]){
        self.publishDlg.timeString = [NSString stringWithFormat:@"%ld月%ld日 %ld:%ld - %ld:%ld",self.startDate.month,self.startDate.day,self.startDate.hour,self.startDate.minute,self.stopDate.hour,self.stopDate.minute];
    }else{
        self.publishDlg.timeString = [NSString stringWithFormat:@"%ld月%ld日 %ld:%ld - %ld月%ld日  %ld:%ld",self.startDate.month,self.startDate.day,self.startDate.hour,self.startDate.minute,self.stopDate.month,self.stopDate.day,self.stopDate.hour,self.stopDate.minute];
    }
    LKWeakSelf
    __block LKDownwindPublishDlg *weakdlg = dlg;
    dlg.publishBlock = ^(NSInteger numbers, NSInteger dayNumber, NSString * _Nonnull note) {
        UserInfo *user = [UserInfo shareInstance];
        if([user isCurrentRecomander]){
            weakSelf.number = numbers;
            weakSelf.dayNumber = dayNumber;
            weakSelf.note = (note==nil)? @"" : note;
            [weakSelf doPublishAction];
            self.tripStep = TripStepNone;
        }else{
            weakSelf.number = numbers;
            weakSelf.dayNumber = dayNumber;
            weakSelf.note = (note==nil)? @"" : note;
            [weakSelf doPublishDemandAction];
            self.tripStep = TripStepNone;
        }
    };
    dlg.btnInfoBlock = ^{
        [weakdlg hide];
        PriceDetailCtrl *vc = [PriceDetailCtrl new];
        [self.navigationController pushViewController:vc animated:YES];
    };
    dlg.btnNoteBlock = ^{
        LKNoteDlg *dlg  = [[LKNoteDlg alloc] init];
        dlg.onSelect = ^(NSString * _Nonnull node) {
            weakdlg.node = node;
        };
        [dlg show];
    };
    [dlg show];
}

- (void)doPublishDemandAction{
    NSInteger index = (self.isDoubleTrip) ? 1: 0;
    NSMutableDictionary *para = [[NSMutableDictionary alloc] initWithDictionary:@{
        @"departurePlaceName":self.address1.name,
        @"departureLon":@(self.address1.coordinate.longitude),
        @"departureLat":@(self.address1.coordinate.latitude),
        @"departureCity":self.address1.cityId,
        @"destinationName":self.address4.name,
        @"destinationLon":@(self.address4.coordinate.longitude),
        @"destinationLat":@(self.address4.coordinate.latitude),
        @"destinationCity":self.address4.cityId,
     
        @"departureTime":[NSNumber numberWithLong:self.startDate.timeInterval*1000],
        @"date":[NSNumber numberWithLong:self.stopDate.timeInterval*1000],
        @"peopleNumber":@(self.number),
        @"whetherToReturn":@(index),
        @"remark":self.note,
        @"doYouAgreeToSitTogether":@(YES)
    }];
    if(self.address2){
        [para addEntriesFromDictionary:@{
        @"viaLon":@(self.address2.coordinate.longitude),
        @"viaLat":@(self.address2.coordinate.latitude),
        @"nameOfPassingPlace":self.address2.name,
        }];
    }
    if(self.address3){
        [para addEntriesFromDictionary:@{
        @"returnMidwayPointLon":@(self.address3.coordinate.longitude),
        @"returnMidwayPointLat":@(self.address3.coordinate.latitude),
        @"waypointOnReturnJourneyName":self.address3.name
        }];
    };
    Api *api = [Api apiPostUrl:@"order/downwindTravel/releaseRequirements"
                          para:para
                   description:@"发布需求"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"error:%@",request.responseString);
        }];
    
    DownwindResultDriverCtrl *vc = [DownwindResultDriverCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)doPublishAction{
    NSInteger index = (self.isDoubleTrip) ? 1: 0;
    NSMutableDictionary *para = [[NSMutableDictionary alloc] initWithDictionary:@{
        @"longitudeOfOrigin":@(self.address1.coordinate.longitude),
        @"latitudeOfOrigin":@(self.address1.coordinate.latitude),
        @"startingPointName":self.address1.name,
        @"departureCityId":self.address1.cityId,
        
        @"endLongitude":@(self.address4.coordinate.longitude),
        @"endLatitude":@(self.address4.coordinate.latitude),
        @"endName":self.address4.name,
        @"destinationCityId":self.address4.cityId,
        @"earliestDepartureTime":[NSNumber numberWithLong:self.startDate.timeInterval*1000],
        @"latestDepartureTime":[NSNumber numberWithLong:self.stopDate.timeInterval*1000],
        @"seatsNumber":@(self.number),
        @"whetherToReturn":@(index)
    }];
    
    if(self.address2){
        [para addEntriesFromDictionary:@{
        @"longitudeOfHalfwayPoint":@(self.address2.coordinate.longitude),
        @"latitudeOfHalfwayPoint":@(self.address2.coordinate.latitude),
        @"halfwayPointName":self.address2.name,
        }];
    }
    if(self.address3){
        [para addEntriesFromDictionary:@{
        @"longitudeOfTheReturnMidwayPoint":@(self.address3.coordinate.longitude),
        @"latitudeOfTheReturnMidwayPoint":@(self.address3.coordinate.latitude),
        @"waypointOnReturnJourneyName":self.address3.name
        }];
    };
    Api *api = [Api apiPostUrl:@"order/downWindRoute/postRoute"
                          para:para
                   description:@"顺风游线路添加"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSString *jsonstr = request.responseString;
        DLog(@"jsonstr = %@",jsonstr);
        NSDictionary *json = [NSString jsonStringToDictionary:jsonstr];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            DLog(@"error:%@",request.responseString);
        }];
    
    DownwindResultDriverCtrl *vc = [DownwindResultDriverCtrl new];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
        }
        if(annotation.subtitle == self.address1.name){
            annotationView.image = [UIImage imageNamed:@"startPoint"];
        }else if(annotation.subtitle==self.address4.name){
            annotationView.image = [UIImage imageNamed:@"endPoint"];
        }else{
            annotationView.image = [UIImage imageNamed:@"car"];
        }
        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -18);
        return annotationView;
    }
    return nil;
}



@end
