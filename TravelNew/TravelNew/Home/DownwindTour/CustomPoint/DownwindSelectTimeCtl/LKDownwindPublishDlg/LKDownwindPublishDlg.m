//
//  LKDownwindTourDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/19.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKDownwindPublishDlg.h"
#import "LKSelectCustomsDlg.h"
#import "LKSelectSitesDlg.h"
#import "LKSelectDayNumberDlg.h"

@interface LKDownwindPublishDlg()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *outTimesLabel;
@property(nonatomic,strong) UILabel *fromLabel;
@property(nonatomic,strong) UILabel *fromDetailLabel;
@property(nonatomic,strong) UILabel *toLabel;
@property(nonatomic,strong) UILabel *toDetailLabel;
@property(nonatomic,strong) UILabel *dateTimeLabel;
@property(nonatomic,strong) UILabel *numberLabel;
@property(nonatomic,strong) UILabel *dayLabel;
@property(nonatomic,strong) UILabel *noteLabel;
@property(nonatomic,strong) UILabel *singleLabel;
@property(nonatomic,strong) UIImageView *leftArrow;
@property(nonatomic,strong) UIImageView *rightArrow;
@property(nonatomic,strong) UILabel *priceLabel;

@property(nonatomic,strong) UILabel *middleAddressLabel;
@property(nonatomic,strong) UILabel *middleAddressLabel2;
@property(nonatomic,strong) UIImageView *singlArrow;
@property(nonatomic,strong) UIImageView *doubleArrow;
@property(nonatomic,strong) UIView *priceView;
@property(nonatomic,strong) LKGadentButton *sendBtn;


@end

@implementation LKDownwindPublishDlg
-(void)setIsDoubleTrip:(BOOL)isDoubleTrip
{
    _isDoubleTrip = isDoubleTrip;
    _singlArrow.hidden = isDoubleTrip;
    _doubleArrow.hidden = !isDoubleTrip;
    if(isDoubleTrip){
        self.singleLabel.text = @"双程";
    }else{
        self.singleLabel.text = @"单程";
    }
    [self.singleLabel updateLayout];
}

-(void)setAddress1:(LKAddressNode *)address1
{
    _address1 = address1;
    self.fromLabel.text = address1.cityName;
    self.fromDetailLabel.text = address1.name;
    [self.fromLabel updateLayout];
    [self.fromDetailLabel updateLayout];
}

-(void)setAddress4:(LKAddressNode *)address4
{
    _address4 = address4;
    self.toLabel.text = address4.cityName;
    self.toDetailLabel.text = address4.name;
    [self.toLabel updateLayout];
    [self.toDetailLabel updateLayout];
}

-(void)setAddress2:(LKAddressNode *)address2
{
    _address2 = address2;
    if(address2 == nil){
        self.middleAddressLabel.hidden = YES;
    }else{
        self.middleAddressLabel.text = address2.name;
        [self.middleAddressLabel updateLayout];
        self.middleAddressLabel.hidden = NO;
    }
}
-(void)setAddress3:(LKAddressNode *)address3
{
    _address3 = address3;
    if(address3 == nil){
        self.middleAddressLabel2.hidden = YES;
    }else{
        self.middleAddressLabel2.text = address3.name;
        [self.middleAddressLabel2 updateLayout];
        self.middleAddressLabel2.hidden = NO;
    }
}

-(void)setNode:(NSString *)node
{
    _node = node;
    self.noteLabel.text = @"已备注";
    [self.noteLabel updateLayout];
}

-(void)setTimeString:(NSString *)timeString
{
    _timeString = timeString;
    self.dateTimeLabel.text = timeString;
    [self.dateTimeLabel updateLayout];
}
-(void)setNumbers:(NSInteger)numbers
{
    _numbers = numbers;
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander]){
        _numberLabel.text = [NSString stringWithFormat:@"%ld座",numbers];
        self.priceView.hidden = YES;
        self.sendBtn.sd_layout
        .topSpaceToView(self.noteLabel, 27);
        [self.sendBtn updateLayout];
    }else{
        _numberLabel.text = [NSString stringWithFormat:@"%ld人",numbers];
        self.priceView.hidden = NO;
    }
    [_numberLabel updateLayout];
}

-(void)setDayNumber:(NSInteger)dayNumber
{
    _dayNumber = dayNumber;
    _dayLabel.text = [NSString stringWithFormat:@"%ld天",dayNumber];
    [_dayLabel updateLayout];
}


- (void)numberLabelIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    UserInfo *user = [UserInfo shareInstance];
    if([user isCurrentRecomander]){
        LKSelectSitesDlg *dlg = [LKSelectSitesDlg new];
        dlg.selectBlock = ^(NSInteger num) {
            weakSelf.numbers = num;
        };
        [dlg show];
    }else{
        LKSelectCustomsDlg *dlg = [LKSelectCustomsDlg new];
        dlg.selectBlock = ^(NSInteger num) {
            weakSelf.numbers = num;
        };
        [dlg show];
    }
}

- (void)dayLabelIsClicked:(UITapGestureRecognizer *)reg
{
    LKWeakSelf
    LKSelectDayNumberDlg *dlg = [LKSelectDayNumberDlg new];
    [dlg setDataArray:@[@"1天",@"2天",@"3天",@"4天",@"5天"]];
    dlg.selectBlock = ^(NSString * _Nonnull numstr) {
        NSString *tmp = [numstr stringByReplacingOccurrencesOfString:@"天" withString:@""];
        weakSelf.dayNumber = [tmp integerValue];
    };
    [dlg show];
}

- (void)noteIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.btnNoteBlock){
        self.btnNoteBlock();
    }
}
- (void)publishIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
    if(self.publishBlock){
        self.publishBlock(self.numbers, self.dayNumber, self.node);
    }
}
- (void)btnInfoIsClicked:(id)sender
{
    if(self.btnInfoBlock){
        self.btnInfoBlock();
    }
    
}

- (void)configViews{
    [super configViews];
    UIView *topview = [UIView new];
    topview.backgroundColor = [UIColor whiteColor];
    _imgv = ImageViewWithImage(@"default_header");
    _imgv.layer.cornerRadius = 16;
    _imgv.layer.masksToBounds = YES;
    _nameLabel = [Utils createLabelWithTitle:@"小王" titleColor:HexColor(0x333333) fontSize:13];
    _outTimesLabel = [Utils createLabelWithTitle:@"32" titleColor:HexColor(0x303030) fontSize:15];
    UILabel *label2 = [Utils createLabelWithTitle:@"出行次数" titleColor:HexColor(0x9C9C9C) fontSize:11];
    _fromLabel = [Utils createBoldLabelWithTitle:@"西安" titleColor:HexColor(0x141418) fontSize:18];
    _fromDetailLabel = [Utils createLabelWithTitle:@"利君未来城2期" titleColor:HexColor(0x333333) fontSize:12];
    _toLabel = [Utils createBoldLabelWithTitle:@"成都" titleColor:HexColor(0x141418) fontSize:18];
    _toDetailLabel = [Utils createLabelWithTitle:@"宽窄巷子地铁" titleColor:HexColor(0x333333) fontSize:12];
    _middleAddressLabel = [Utils createLabelWithTitle:@"汉中" titleColor:HexColor(0x4D84FD) fontSize:10];
    _middleAddressLabel2 = [Utils createLabelWithTitle:@"安康" titleColor:HexColor(0x4D84FD) fontSize:10];
    _singlArrow = ImageViewWithImage(@"trip_icon_single");
    _doubleArrow = ImageViewWithImage(@"trip_icon_return");
    _dateTimeLabel = [Utils createBoldLabelWithTitle:@"3月22日 12:00～13:00" titleColor:HexColor(0x666666) fontSize:12];
    _numberLabel = [Utils createLabelWithTitle:@"3人  >" titleColor:HexColor(0x202020) fontSize:12];
    [_numberLabel addTapGestureTarget:self action:@selector(numberLabelIsClicked:)];
    _singleLabel = [Utils createLabelWithTitle:@"单程" titleColor:HexColor(0x202020) fontSize:12];
    _singleLabel.textAlignment = NSTextAlignmentCenter;
    _dayLabel = [Utils createLabelWithTitle:@"9天  >" titleColor:HexColor(0x202020) fontSize:12];
    _dayLabel.textAlignment = NSTextAlignmentCenter;
    [_dayLabel addTapGestureTarget:self action:@selector(dayLabelIsClicked:)];
    _noteLabel = [Utils createLabelWithTitle:@"备注  >" titleColor:HexColor(0x202020) fontSize:12];
    [_noteLabel addTapGestureTarget:self action:@selector(noteIsClicked:)];
    _noteLabel.textAlignment = NSTextAlignmentRight;
    self.priceView = [self createPriceView];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"发布顺风游" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(publishIsClicked:)];
    self.sendBtn = btn;
    
    [topview sd_addSubviews:@[_imgv,_nameLabel,_outTimesLabel,label2,
                              _fromLabel,_fromDetailLabel,
                              _toLabel,_toDetailLabel,
                              _middleAddressLabel,_middleAddressLabel2,
                              _singlArrow,_doubleArrow,
                              _dateTimeLabel,
                              _numberLabel,_singleLabel,_dayLabel,_noteLabel]];
    [self addSubview:topview];
    [self sd_addSubviews:@[self.priceView,
                            btn]];
    topview.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topEqualToView(self)
    .heightIs(213.5);
    
    _imgv.sd_layout
    .topSpaceToView(topview, 27)
    .leftSpaceToView(topview, 25)
    .widthIs(32)
    .heightIs(32);
    
    _nameLabel.sd_layout
    .centerYEqualToView(_imgv)
    .leftSpaceToView(_imgv, 9.5)
    .autoHeightRatio(0);
    [_nameLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _outTimesLabel.sd_layout
    .topSpaceToView(topview, 28.5)
    .rightSpaceToView(topview, 37)
    .autoHeightRatio(0);
    [_outTimesLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .topSpaceToView(_outTimesLabel, 7)
    .rightSpaceToView(topview, 23.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _fromLabel.sd_layout
    .topSpaceToView(topview, 83)
    .leftSpaceToView(topview, 55)
    .autoHeightRatio(0);
    [_fromLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _fromDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .leftSpaceToView(topview, 32)
    .autoHeightRatio(0);
    [_fromDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toLabel.sd_layout
    .topSpaceToView(topview, 83)
    .rightSpaceToView(topview, 55)
    .autoHeightRatio(0);
    [_toLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _toDetailLabel.sd_layout
    .topSpaceToView(_fromLabel, 11.5)
    .rightSpaceToView(topview, 32)
    .autoHeightRatio(0);
    [_toDetailLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _singlArrow.sd_layout
    .topSpaceToView(topview, 105)
    .centerXEqualToView(topview)
    .widthIs(55)
    .heightIs(4);
    
    _doubleArrow.sd_layout
    .topSpaceToView(topview, 105)
    .centerXEqualToView(topview)
    .widthIs(55)
    .heightIs(13);
    
    _middleAddressLabel.sd_layout
    .centerXEqualToView(topview)
    .bottomSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _middleAddressLabel2.sd_layout
    .centerXEqualToView(topview)
    .topSpaceToView(_singlArrow, 8)
    .autoHeightRatio(0);
    [_middleAddressLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];

    _dateTimeLabel.sd_layout
    .topSpaceToView(_fromDetailLabel, 11.5)
    .leftEqualToView(_fromDetailLabel)
    .autoHeightRatio(0);
    [_dateTimeLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    float w = (kScreenWidth-64)/4.0;
    _numberLabel.sd_layout
    .topSpaceToView(_dateTimeLabel, 29.5)
    .leftSpaceToView(topview, 32)
    .autoHeightRatio(0)
    .widthIs(w);
    
    _singleLabel.sd_layout
    .centerYEqualToView(_numberLabel)
    .leftSpaceToView(_numberLabel, 0)
    .autoHeightRatio(0)
    .widthIs(w);
   
    _dayLabel.sd_layout
    .centerYEqualToView(_singleLabel)
    .leftSpaceToView(_singleLabel, 0)
    .autoHeightRatio(0)
    .widthIs(w);
    
    _noteLabel.sd_layout
    .centerYEqualToView(_dayLabel)
    .leftSpaceToView(_dayLabel, 0)
    .autoHeightRatio(0)
    .widthIs(w);
    
    self.priceView.sd_layout
    .topSpaceToView(topview, 10)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(90);
    
    btn.sd_layout
    .topSpaceToView(self.priceView,0)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
    self.numbers = 3;
}

- (UIView *)createPriceView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *tipLabel1 = [Utils createBoldLabelWithTitle:@"预估价格" titleColor:HexColor(0x303030) fontSize:14];
    UILabel *tipLabel2 = [Utils createBoldLabelWithTitle:@"元" titleColor:HexColor(0x303030) fontSize:14];
    _priceLabel = [Utils createBoldLabelWithTitle:@"9980" titleColor:HexColor(0xFF9710) fontSize:30];
    UIButton *btnInfo = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [btnInfo addTarget:self action:@selector(btnInfoIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view sd_addSubviews:@[tipLabel1,tipLabel2,_priceLabel,btnInfo]];
    _priceLabel.sd_layout
    .centerYEqualToView(view)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [_priceLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    tipLabel1.sd_layout
    .rightSpaceToView(_priceLabel, 2)
    .bottomEqualToView(_priceLabel)
    .autoHeightRatio(0);
    [tipLabel1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    tipLabel2.sd_layout
    .leftSpaceToView(_priceLabel, 2)
    .bottomEqualToView(_priceLabel)
    .autoHeightRatio(0);
    [tipLabel2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btnInfo.sd_layout
    .centerYEqualToView(tipLabel2)
    .leftSpaceToView(tipLabel2, 8)
    .widthIs(10)
    .heightIs(10);
    return view;
}

-(void)show
{
    [super show];
    self.maskView.hidden = YES;
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    [self makeTopAngleSize:CGSizeMake(30, 30) bounds:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}

@end
