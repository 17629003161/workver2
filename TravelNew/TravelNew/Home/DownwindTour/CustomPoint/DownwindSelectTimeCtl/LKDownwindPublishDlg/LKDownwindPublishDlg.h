//
//  LKDownwindTourDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/19.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"
#import "LKAddressNode.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKDownwindPublishDlg : LKBaseDialogView
@property(nonatomic,strong) void (^publishBlock)(NSInteger numbers,NSInteger dayNumber, NSString *note);
@property(nonatomic,strong) void (^btnInfoBlock)(void);
@property(nonatomic,strong) void (^btnNoteBlock)(void);

@property(nonatomic,assign) BOOL isDoubleTrip;
@property(nonatomic,strong) LKAddressNode *address1;
@property(nonatomic,strong) LKAddressNode *address2;
@property(nonatomic,strong) LKAddressNode *address3;
@property(nonatomic,strong) LKAddressNode *address4;
@property(nonatomic,strong) NSString *timeString;
@property(nonatomic,assign) NSInteger numbers;
@property(nonatomic,assign) NSInteger dayNumber;
@property(nonatomic,strong) NSString *node;

@end

NS_ASSUME_NONNULL_END
