//
//  LKSelectDayNumberDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/19.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKSelectDayNumberDlg : LKBaseDialogView
@property(nonatomic,copy) void (^selectBlock)(NSString *);
-(void)setDataArray:(NSArray *)dataArray;
@end

NS_ASSUME_NONNULL_END
