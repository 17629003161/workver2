//
//  LKSelectDayNumberDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/19.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKSelectDayNumberDlg.h"

@interface LKSelectDayNumberDlg ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property(nonatomic,strong) UIPickerView *pickView;
@property(nonatomic,strong) NSArray *dataArray;
@property(nonatomic,strong) NSString *dataString;

@end
    
@implementation LKSelectDayNumberDlg

-(void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
}

- (void)cancleBtnIsClicked:(UITapGestureRecognizer *)reg
{
    [self hide];
}

- (void)okBtnIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.selectBlock && self.dataString)
        self.selectBlock(self.dataString);
    [self hide];
}

-(void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label2 = [Utils createLabelWithTitle:@"请选择预计出行天数" titleColor:HexColor(0x323232) fontSize:17];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确定" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(okBtnIsClicked:)];
    
    [self sd_addSubviews:@[label2,_pickView,btn]];

    label2.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self, 30)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label2, 25)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(107.5);
    
    btn.sd_layout
    .topSpaceToView(_pickView, 19.5)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    
    [self setupAutoHeightWithBottomView:btn bottomMargin:41];
}

-(void)setDataString:(NSString *)dataString
{
    _dataString = dataString;
}

#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _dataArray.count;
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return 150;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@",self.dataArray[row]];
}

//当改变省份时，重新加载第2列的数据，部分加载
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
        self.dataString = self.dataArray[row];
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}
@end
