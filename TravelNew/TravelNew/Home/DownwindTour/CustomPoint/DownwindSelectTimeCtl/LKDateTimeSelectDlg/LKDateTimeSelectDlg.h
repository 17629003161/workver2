//
//  LKDateTimeSelectDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/18.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKDateTimeSelectDlg : LKBaseDialogView
@property(nonatomic,copy) void (^selectDateBlock)(LKDateUtils *);
- (void)setDate;
@end

NS_ASSUME_NONNULL_END
