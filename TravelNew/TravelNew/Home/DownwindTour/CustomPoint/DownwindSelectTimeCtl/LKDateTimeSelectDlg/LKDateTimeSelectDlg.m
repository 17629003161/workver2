//
//  LKDateTimeSelectDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/18.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKDateTimeSelectDlg.h"
#import "LKDateTimeSelectDlg2.h"

@interface LKDateTimeSelectDlg ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIPickerView *_pickView;
    NSMutableArray *_dateArray;
    NSMutableArray *_hoursArray;
    NSMutableArray *_minitesArray;
}
@property(nonatomic,strong) LKDateUtils *selectedDate;
@property(nonatomic,strong) LKDateUtils *date;
@property(nonatomic,strong) NSMutableDictionary *showDict;
@end
    
@implementation LKDateTimeSelectDlg

-(void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"最早出发时间" titleColor:HexColor(0x383232) fontSize:18];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    [self sd_addSubviews:@[label1,
                           _pickView]];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"下一步" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(nextStepIsClicked:)];
    [self addSubview:btn];
    label1.sd_layout
    .topSpaceToView(self, 30)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label1, 40)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(138);
    
    btn.sd_layout
    .topSpaceToView(_pickView, 30)
    .leftSpaceToView(self, 27.5)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
    
    _dateArray = [NSMutableArray new];
    _hoursArray = [NSMutableArray new];
    _minitesArray = [NSMutableArray new];
}


- (void)nextStepIsClicked:(UITapGestureRecognizer *)reg
{
    if(!self.selectedDate){
        [MBProgressHUD showMessage:@"请选择最早出发日期"];
        return;
    }
    if(self.selectDateBlock){
        DLog(@"------ %@",self.selectedDate.dateTimeString);
        self.selectDateBlock(self.selectedDate);
    }
    [self hide];
}

-(NSMutableDictionary *)showDict{
    if(!_showDict){
        _showDict = [NSMutableDictionary new];
    }
    LKDateUtils *date = [LKDateUtils today];
    [_showDict setObject:@"今天" forKey:date.dateString];
    date = [date nextDay];
    [_showDict setObject:@"明天" forKey:date.dateString];
    date = [date nextDay];
    [_showDict setObject:@"后天" forKey:date.dateString];
    return _showDict;
}

-(void)setDate
{
    LKDateUtils *date = [LKDateUtils today];
    date.timeInterval = date.timeInterval + 60*60;
    self.date = date;
    self.selectedDate = date;
    self.selectedDate.showString = [self checkShowText:self.selectedDate];
    [self initComponent0:self.date];
    [_pickView selectRow:0 inComponent:0 animated:YES];
    [_pickView selectRow:0 inComponent:1 animated:YES];
    [_pickView selectRow:0 inComponent:2 animated:YES];
}

- (void)initComponent0:(LKDateUtils *)date{
    LKDateUtils *tmp = date;
    for (int i=0; i<7; i++) {
        if(i==0){
            [_dateArray addObject:tmp];
            [self initComponent1:YES];
            [self initComponent2:YES];
        }else{
            tmp = [tmp nextDay];
            [_dateArray addObject:tmp];
        }
    }
}

- (void)initComponent1:(BOOL)istoday{
    NSInteger start = 0;
    [_hoursArray removeAllObjects];
    if(istoday){
        start = self.date.hour;
    }
    for(NSInteger h=start; h<24;h++){
        [_hoursArray addObject:@(h)];
    }
}

- (void)initComponent2:(BOOL)istoday{
    NSInteger start = 0;
    [_minitesArray removeAllObjects];
    if(istoday){
        start = self.date.minute;
    }
    for(NSInteger m=start; m<60; m+=10){
        [_minitesArray addObject:@(m)];
    }
}



#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _dateArray.count;
    }else if(component==1) {
        return _hoursArray.count;
    }else if(component==2){
        return _minitesArray.count;
    }
    return 0;
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return (kScreenWidth-20)/3.0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        LKDateUtils *date = _dateArray[row];
        date.showString = [self checkShowText:date];
        return date.showString;
    }else if(component==1){
        return [NSString stringWithFormat:@"%@点",_hoursArray[row]];
    }else{
        return [NSString stringWithFormat:@"%@分",_minitesArray[row]];
    }
}

//
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSInteger select0 = [_pickView selectedRowInComponent:0];
    NSInteger select1 = [_pickView selectedRowInComponent:1];
    if (component == 0) {
        BOOL isFirstRow = (row==0) ? YES : NO;
        [self initComponent1:isFirstRow];
        [self initComponent2:isFirstRow];
        [_pickView reloadComponent:1];
        [_pickView reloadComponent:2];
        [_pickView selectRow:0 inComponent:1 animated:YES];
        [_pickView selectRow:0 inComponent:2 animated:YES];
    }else if(component==1){
        if(select0==0 && select1==0){
            [self initComponent2:YES];
        }else{
            [self initComponent2:NO];
        }
        [_pickView reloadComponent:2];
        [_pickView selectRow:0 inComponent:2 animated:YES];
    }
    self.selectedDate = [self calcDate];
    self.selectedDate.showString = [self checkShowText:self.selectedDate];
    DLog(@"%@",self.selectedDate.dateTimeString);
}

- (NSString *)checkShowText:(LKDateUtils *)date{
    NSArray *keyArray = [self.showDict allKeys];
    if([keyArray containsObject:date.dateString]){
        return [_showDict objectForKey:date.dateString];
    }else{
        return [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%ld月%ld日",date.month,date.day]];;
    }
}


- (LKDateUtils *)calcDate{
    NSInteger select0 = [_pickView selectedRowInComponent:0];
    NSInteger select1 = [_pickView selectedRowInComponent:1];
    NSInteger select2 = [_pickView selectedRowInComponent:2];
    LKDateUtils *tmp = _dateArray[select0];
    NSString *str = [NSString stringWithFormat:@"%@ %@:%@:00",
                     tmp.dateString,_hoursArray[select1],_minitesArray[select2]];
    DLog(@"%@",str);
    LKDateUtils *date = [LKDateUtils new];
    date.dateTimeString = str;
    DLog(@"%@",date.dateTimeString);
    return date;
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}

@end

