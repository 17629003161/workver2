//
//  LKDateTimeSelectDlg2.m
//  TravelNew
//
//  Created by mac on 2021/3/18.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKDateTimeSelectDlg2.h"

@interface LKDateTimeSelectDlg2 ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIPickerView *_pickView;
    NSMutableArray *_dateArray;
    NSMutableArray *_hoursArray;
    NSMutableArray *_minitesArray;
    NSMutableArray *_descArray;
}
@property(nonatomic,strong) LKDateUtils *selectedDate;
@property(nonatomic,strong) LKDateUtils *stopDate;

@end
    
@implementation LKDateTimeSelectDlg2

-(void)configViews{
    [super configViews];
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"最晚出发时间" titleColor:HexColor(0x383232) fontSize:18];
    _pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(50, 100, 300, 300)];
    _pickView.dataSource = self;
    _pickView.delegate = self;
    [self sd_addSubviews:@[label1,
                           _pickView]];
    UIButton *upbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [upbtn setTitle:@"上一步" forState:UIControlStateNormal];
    [upbtn setTitleColor:HexColor(0x4D83FD) forState:UIControlStateNormal];
    upbtn.titleLabel.font = LKSystemFont(18);
    upbtn.layer.borderColor = HexColor(0x4D83FD).CGColor;
    upbtn.layer.borderWidth = 1.0;
    upbtn.layer.cornerRadius = 23;
    upbtn.layer.masksToBounds = YES;
    [upbtn addTapGestureTarget:self action:@selector(uperBtnIsClicked:)];
    [self addSubview:upbtn];
    
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确认" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(nextStepIsClicked:)];
    [self addSubview:btn];
    label1.sd_layout
    .topSpaceToView(self, 30)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    _pickView.sd_layout
    .topSpaceToView(label1, 40)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(138);
    
    upbtn.sd_layout
    .topSpaceToView(_pickView, 30)
    .leftSpaceToView(self, 27.5)
    .widthIs(118)
    .heightIs(46);
    
    btn.sd_layout
    .centerYEqualToView(upbtn)
    .leftSpaceToView(upbtn, 10)
    .rightSpaceToView(self, 27.5)
    .heightIs(46);
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
    
    _dateArray = [NSMutableArray new];
    _hoursArray = [NSMutableArray new];
    _minitesArray = [NSMutableArray new];
    _descArray = [NSMutableArray new];
}

- (void)nextStepIsClicked:(UITapGestureRecognizer *)reg
{
    if(!self.selectedDate){
        [MBProgressHUD showMessage:@"请选择最晚出发日期"];
        return;
    }
    if(self.selectDateBlock){
        self.selectDateBlock(self.selectedDate);
    }
    [self hide];
}

- (void)uperBtnIsClicked:(id)sender{
    if(self.uperStepBlock){
        self.uperStepBlock();
        [self hide];
    }
}

- (LKDateUtils *)selectedDate
{
    if(!_selectedDate){
        _selectedDate = [LKDateUtils new];
    }
    return _selectedDate;
}

-(void)setDate:(LKDateUtils *)date
{
    _date = date;
    self.stopDate = [LKDateUtils new];
    self.stopDate.timeInterval = date.timeInterval + 6*60*60;
    DLog(@"传入日期：%@ %@",date.dateString,date.showString);
    [self initComponent0];
}


- (void)initComponent0{
    DLog(@"%@",self.date);
    NSInteger row0 = 0;
    NSInteger row1 = 0;
    [_dateArray removeAllObjects];
    [_dateArray addObject:self.date];
    if(![self.date.dateString isEqualToString:self.stopDate.dateString]){
        self.stopDate.showString = [NSString stringWithFormat:@"%ld月%ld日",self.stopDate.month,self.stopDate.day];
        [_dateArray addObject:self.stopDate];
        if(23-self.date.hour>3){
            row0 = 0;
            row1 = 3;
        }else{
            row0 = 1;
            row1 = 3-(23-self.date.hour);
        }
    }else{
        row0 = 0;
        row1 = 3;
    }
    [self initComponent1:YES];
    [self initComponent2:YES];
    [_pickView reloadComponent:0];
    [_pickView selectRow:row0 inComponent:0 animated:YES];
    if(row0>0){
        [self initComponent1:NO];
    }
    [_pickView selectRow:row1 inComponent:1 animated:YES];
    self.selectedDate = [self calcDate];
    DLog(@"%@",self.selectedDate.dateTimeString);
}

- (void)initComponent1:(Boolean)firstRow{
    [_hoursArray removeAllObjects];
    if(firstRow){
        for(NSInteger i=0; i<=6; i++){
            NSInteger h = self.date.hour+i;
            if(h<24){
                [_hoursArray addObject:@(h)];
            }
        }
    }else{
        for(NSInteger h=0; h<=self.stopDate.hour;h++){
            [_hoursArray addObject:@(h)];
        }
    }
    [_pickView reloadComponent:1];
}

- (void)initComponent2:(Boolean)firstRow{
    [_minitesArray removeAllObjects];
    if(firstRow){
        for(NSInteger s=self.date.minute; s<60; s+=10){
            [_minitesArray addObject:@(s)];
        }
    }else{
        for (NSInteger s=0; s<60; s+=10) {
            [_minitesArray addObject:@(s)];
        }
    }
    [_pickView reloadComponent:2];
}

#pragma mark - dataSource
//注意这里是几列的意思。我刚刚开始学得时候也在这里出错，没理解
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _dateArray.count;
    }else if(component==1) {
        return _hoursArray.count;
    }else if(component==2){
        return _minitesArray.count;
    }
    return 0;
}

#pragma mark - delegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return (kScreenWidth-20)/3.0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

//返回每行显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        LKDateUtils *date = _dateArray[row];
        DLog(@"%@",date.dateString);
        return date.showString;
    }else if(component==1){
        return [NSString stringWithFormat:@"%@点",_hoursArray[row]];
    }else{
        return [NSString stringWithFormat:@"%@分",_minitesArray[row]];
    }
}

//
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSInteger select0 = [_pickView selectedRowInComponent:0];
    NSInteger select1 = [_pickView selectedRowInComponent:1];
    if (component == 0) {
        if(select0==0){
            [self initComponent1:YES];
            [self initComponent2:YES];
        }else{
            [self initComponent1:NO];
            [self initComponent2:NO];
        }
        [_pickView selectRow:0 inComponent:1 animated:YES];
        [_pickView selectRow:0 inComponent:2 animated:YES];
    }else if(component==1){
        if(select0==0 && select1==0){
            [self initComponent2:NO];
        }else{
            [self initComponent2:NO];
        }
        [_pickView reloadComponent:2];
        [_pickView selectRow:0 inComponent:2 animated:YES];
    }
    self.selectedDate = [self calcDate];
    DLog(@"%@",self.selectedDate.dateTimeString);
}

- (LKDateUtils *)calcDate{
    NSInteger select0 = [_pickView selectedRowInComponent:0];
    NSInteger select1 = [_pickView selectedRowInComponent:1];
    NSInteger select2 = [_pickView selectedRowInComponent:2];
    LKDateUtils *tmp = _dateArray[select0];
    NSString *str = [NSString stringWithFormat:@"%@ %@:%@:00",
                     tmp.dateString,_hoursArray[select1],_minitesArray[select2]];
    DLog(@"%@",str);
    LKDateUtils *date = [LKDateUtils new];
    date.dateTimeString = str;
    DLog(@"%@",date.dateTimeString);
    return date;
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
    [self makeTopAngleSize:CGSizeMake(30, 30) bounds:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
}

@end

