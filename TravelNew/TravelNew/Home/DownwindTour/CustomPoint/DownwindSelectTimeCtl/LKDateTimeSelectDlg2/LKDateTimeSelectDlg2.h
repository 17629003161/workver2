//
//  LKDateTimeSelectDlg2.h
//  TravelNew
//
//  Created by mac on 2021/3/18.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKDateTimeSelectDlg2 : LKBaseDialogView
@property(nonatomic,copy) void (^selectDateBlock)(LKDateUtils *);
@property(nonatomic,copy) void (^uperStepBlock)(void);
@property(nonatomic,strong) LKDateUtils *date;
@end

NS_ASSUME_NONNULL_END
