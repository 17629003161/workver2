//
//  LKPayDialog2.m
//  TravelNew
//
//  Created by mac on 2021/3/26.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKPayDialog2.h"

@interface LKPayDialog2()
@property(nonatomic,strong) UIButton *btnClose;
@end

@implementation LKPayDialog2

-(UIButton *)btnClose
{
    if(!_btnClose){
        _btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnClose setImage:[UIImage imageNamed:@"pay_btn_close"] forState:UIControlStateNormal];
    }
    return _btnClose;
}

-(void)btnPayIsClicked:(UITapGestureRecognizer *)reg
{
    DLog(@"pay btn is clicked");
    if(self.payBlock){
        self.payBlock();
    }
}

-(void)configViews{
    self.frame = CGRectMake(0, 0, 258, 350.5);
    UIImageView *imgv = ImageViewWithImage(@"pay_bg_img_lane");
    imgv.frame = self.bounds;
    UILabel *label1 = [Utils createBoldLabelWithTitle:@"真棒！遇到了顺路的车主" titleColor:HexColor(0xffffff) fontSize:16];
    UIImageView *headimgv = ImageViewWithImage(@"default_header");
    UILabel *label2 = [Utils createLabelWithTitle:@"尾号2932" titleColor:HexColor(0xffffff) fontSize:13];
    UILabel *label3 = [Utils createLabelWithTitle:@"出行次数 | 好评率100%" titleColor:[UIColor whiteColor] fontSize:11];
    UIView *iconview1 = [self creatViewIcon:@"pay_icon_real_name" title:@"实名认证"];
    UIView *iconview2 = [self creatViewIcon:@"pay_icon_license" title:@"驾照认证"];
    UIView *iconview3 = [self creatViewIcon:@"pay_icon_vehicle" title:@"车辆认证"];
    UIView *iconview4 = [self creatViewIcon:@"pay_icon_face" title:@"人脸识别"];
    UIView *iconview5 = [self creatViewIcon:@"pay_icon_security" title:@"安全筛查"];
    NSArray *array = @[iconview1,iconview2,iconview3,iconview4,iconview5];
    UIView *btn = [self creatButtonView];
    [btn addTapGestureTarget:self action:@selector(btnPayIsClicked:)];
    btn.layer.cornerRadius = 20;
    btn.layer.masksToBounds = YES;
    [self sd_addSubviews:@[imgv,label1,
                              headimgv,
                              label2,
                              label3,
                              btn]];
    [self sd_addSubviews:array];
    label1.sd_layout
    .topSpaceToView(self, 47)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    headimgv.sd_layout
    .topSpaceToView(label1, 34.5)
    .centerXEqualToView(self)
    .widthIs(43)
    .heightIs(43);
    
    label2.sd_layout
    .topSpaceToView(headimgv, 9.5)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label3.sd_layout
    .topSpaceToView(label2, 10)
    .centerXEqualToView(self)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    CGFloat w = (258-16)/5.0;
    for (int i=0; i<array.count; i++) {
        UIView *view = array[i];
        view.sd_layout
        .topSpaceToView(label3, 35.5)
        .leftSpaceToView(self, i*w+8)
        .widthIs(w);
    }
    btn.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(label3, 98.5)
    .widthIs(139.5)
    .heightIs(40);
}

- (UIView *)creatViewIcon:(NSString *)icon title:(NSString *)title
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *imgv = ImageViewWithImage(icon);
    UILabel *label = [Utils createLabelWithTitle:title titleColor:[UIColor whiteColor] fontSize:9];
    [view sd_addSubviews:@[imgv,label]];
    imgv.sd_layout
    .topSpaceToView(view, 0)
    .centerXEqualToView(view)
    .widthIs(22)
    .heightIs(22);
    label.sd_layout
    .centerXEqualToView(imgv)
    .topSpaceToView(imgv, 6)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomView:label bottomMargin:0];
    return view;
}

- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self.maskView];
    self.maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
    self.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0);
    [keyWindow addSubview:self];
    [keyWindow addSubview:self.btnClose];
    [self.btnClose addTarget:self action:@selector(btnCloseIsClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.isShowing = YES;
    self.btnClose.sd_layout
    .topSpaceToView(self, 34)
    .centerXEqualToView(keyWindow)
    .widthIs(21)
    .heightIs(21);
}

- (void)btnCloseIsClicked:(id)sender
{
    [self hide];
}

- (void)hide{
    [super hide];
    [self.btnClose removeFromSuperview];
}

- (UIView *)creatButtonView{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0,0,139.5,40);
    view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
    view.layer.shadowColor = [UIColor colorWithRed:27/255.0 green:21/255.0 blue:107/255.0 alpha:0.5].CGColor;
    view.layer.shadowOffset = CGSizeMake(1,2);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 4;

    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,139.5,40);
    gl.startPoint = CGPointMake(0, 0);
    gl.endPoint = CGPointMake(1, 1);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:253/255.0 green:251/255.0 blue:222/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:242/255.0 green:157/255.0 blue:1/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:242/255.0 green:154/255.0 blue:1/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:77/255.0 green:131/255.0 blue:253/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:255/255.0 green:189/255.0 blue:84/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:255/255.0 green:152/255.0 blue:31/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0.0),@(0.0),@(0.0),@(0.0),@(0.0),@(1.0)];

    [view.layer addSublayer:gl];
    UILabel *titleLabel = [Utils createLabelWithTitle:@"去支付" titleColor:[UIColor whiteColor] fontSize:18];
    [view addSubview:titleLabel];
    titleLabel.sd_layout
    .centerXEqualToView(view)
    .centerYEqualToView(view)
    .autoHeightRatio(0);
    [titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    return view;
}



@end
