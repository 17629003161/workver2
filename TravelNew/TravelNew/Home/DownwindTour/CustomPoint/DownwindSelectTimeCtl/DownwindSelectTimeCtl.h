//
//  DownwindSelectTimeCtl.h
//  TravelNew
//
//  Created by mac on 2021/3/23.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseViewController.h"
#import "LKAddressNode.h"

typedef NS_ENUM(NSInteger, TripStep){
    TripStepTime=0,           //选择时间
    TripStepPublish=1,         //确认结果
    TripStepNone=2            //无状态
};

NS_ASSUME_NONNULL_BEGIN

@interface DownwindSelectTimeCtl : LKBaseViewController
@property(nonatomic,assign) TripStep tripStep;
@property(nonatomic,assign) BOOL isDoubleTrip;              //NO: 单程    YES:双程
@property(nonatomic,strong) LKAddressNode *address1;            //出发地
@property(nonatomic,strong) LKAddressNode *address2;            //去程途径地(或单程途径地)
@property(nonatomic,strong) LKAddressNode *address3;            //返程途径地
@property(nonatomic,strong) LKAddressNode *address4;            //旅游目的地

@end

NS_ASSUME_NONNULL_END
