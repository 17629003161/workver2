//
//  LKNoteDlg.m
//  TravelNew
//
//  Created by mac on 2021/3/24.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKNoteDlg.h"

@interface LKNoteDlg()
@property(nonatomic,strong) NSMutableArray *btnArray;

@end

@implementation LKNoteDlg

-(void)configViews
{
    self.backgroundColor = [UIColor whiteColor];
    UILabel *label = [Utils createBoldLabelWithTitle:@"给车主留言" titleColor:HexColor(0x383232) fontSize:18];
    UILabel *label1 = [Utils createLabelWithTitle:@"特殊情况入围提前告知，车主可以拒绝出行" titleColor:HexColor(0x808080) fontSize:12];
    UIView *view = [self createView:CGRectMake(0, 94, kScreenWidth, 160)];
    LKGadentButton *btn = [LKGadentButton blueButtonWhithTitle:@"确定" font:LKSystemFont(18) cornerRadius:23];
    [btn addTapGestureTarget:self action:@selector(btnOkIsClicked:)];
    [self sd_addSubviews:@[label,label1,view,btn]];
    label.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self, 27)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label1.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(label, 13)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    btn.sd_layout
    .topSpaceToView(view, 30)
    .leftSpaceToView(self, 28)
    .rightSpaceToView(self, 28)
    .heightIs(46);
    
    [self setupAutoHeightWithBottomView:btn bottomMargin:40];
}

- (void)btnOkIsClicked:(id)sender
{
    NSString *str = [[NSString alloc] init];
    for (UIButton *btn in self.btnArray) {
        if(btn.isSelected){
            NSString *title = [btn titleForState:UIControlStateNormal];
            str = [NSString stringWithFormat:@"%@,%@",str,title];
        }
    }
    DLog(@"%@",str);
    if(self.onSelect){
        self.onSelect(str);
    }
    [self hide];
}

-(NSMutableArray *)btnArray
{
    if(!_btnArray){
        _btnArray = [NSMutableArray new];
    }
    return _btnArray;
}

-(UIView *)createView:(CGRect)frame
{
    UIView *bgView = [[UIView alloc] initWithFrame:frame];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    NSArray *titleAry = @[@"有大件行李",
                          @"有孕妇/老人，请多关照",
                          @"有宠物，我会照看好",
                          @"我会佩戴口罩，请放心",
                          @"希望走高速路线"];
    CGFloat pointX = 10.0; //button X坐标
    CGFloat pointY = 10.0; //button Y坐标
    CGFloat padding = 15.0; //button 间距
    CGFloat btnHeight = 30; //button高度
    CGFloat allWidth = frame.size.width - 10;
    UIFont *titleFont = [UIFont systemFontOfSize:15];
    for (int i = 0; i < titleAry.count; i++) {
        CGRect rect = [titleAry[i] boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : titleFont} context:nil];
        CGFloat btnWidth = rect.size.width + 20;
        
        if (pointX + btnWidth > allWidth) {//换行
            pointX = 10;//X从新开始
            pointY += (btnHeight + padding);//换行后Y+
        }
        UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
        [but setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [but setBackgroundImage:[UIImage imageWithColor:HexColor(0xF1F6FF)] forState:UIControlStateSelected];
        but.tag = i;
        but.frame = CGRectMake(pointX, pointY, btnWidth, btnHeight);
        but.tag = i + 1000;
        [but addTarget:self action:@selector(clickButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        but.layer.masksToBounds = YES;
        but.layer.cornerRadius = btnHeight/2;
        but.layer.borderWidth = 1;
        but.layer.borderColor = HexColor(0xcfcfcf).CGColor;
        [but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [but setTitle:titleAry[i] forState:UIControlStateNormal];
        but.titleLabel.font = titleFont;//一定要一样
        pointX += (btnWidth + padding);//每次X都加上button宽和间距5
        [but setSelected:NO];
        [bgView addSubview:but];
        [self.btnArray addObject:but];
    }
    CGRect rect2 = bgView.frame;
    rect2.size.height = pointY + btnHeight + 10;
    bgView.frame = rect2;
    return bgView;
}
 
-(void)clickButtonAction:(UIButton *)sender
{
    UIButton *btn = (UIButton *)sender;
    if (btn.isSelected) {
        [btn setSelected:NO];
        btn.layer.borderColor = HexColor(0xcfcfcf).CGColor;
    }else{
        [btn setSelected:YES];
        btn.layer.borderColor = HexColor(0x4d9bfd).CGColor;
    }
}

-(void)show
{
    [super show];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    self.sd_layout
    .leftEqualToView(keyWindow)
    .rightEqualToView(keyWindow)
    .bottomEqualToView(keyWindow);
}


@end
