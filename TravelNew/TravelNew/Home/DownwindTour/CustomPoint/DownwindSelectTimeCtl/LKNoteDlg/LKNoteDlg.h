//
//  LKNoteDlg.h
//  TravelNew
//
//  Created by mac on 2021/3/24.
//  Copyright © 2021 lester. All rights reserved.
//

#import "LKBaseDialogView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKNoteDlg : LKBaseDialogView
@property(nonatomic,strong) NSArray *titleArray;
@property(nonatomic,copy) void(^onSelect)(NSString *);

@end

NS_ASSUME_NONNULL_END
