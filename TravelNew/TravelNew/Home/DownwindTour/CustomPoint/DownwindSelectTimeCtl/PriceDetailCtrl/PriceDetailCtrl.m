//
//  PriceDetailCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/24.
//  Copyright © 2021 lester. All rights reserved.
//

#import "PriceDetailCtrl.h"

@interface PriceDetailCtrl ()
@property(nonatomic,strong) UILabel *priceLabel1;
@property(nonatomic,strong) UILabel *priceLabel2;
@property(nonatomic,strong) UILabel *priceLabel3;
@property(nonatomic,strong) UILabel *priceLabel4;

@end

#define WID kScreenWidth-100

@implementation PriceDetailCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"价格详情" titleColor:nil];
    [self setupUI];
}

- (void)setupUI
{
    UILabel *priceLabel = [Utils createBoldLabelWithTitle:@"987" titleColor:HexColor(0x333333) fontSize:44];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"元" titleColor:HexColor(0x656565) fontSize:14];
    UIView *view1 = [self createInfoView];
    UIView *view2 = [self createInfoView];
    UIView *view3 = [self createInfoView];
    UIView *view4 = [self createInfoView];
    UILabel *label = [view1 viewWithTag:100];
    label.text = @"总里程";
    label = [view2 viewWithTag:100];
    label.text = @"起步价";
    label = [view3 viewWithTag:100];
    label.text = @"里程费";
    label = [view4 viewWithTag:100];
    label.text = @"拼成折扣";

    _priceLabel1 = [view1 viewWithTag:200];
    _priceLabel1.text = @"1300公里";
    _priceLabel2 = [view2 viewWithTag:200];
    _priceLabel2.text = @"8元";
    _priceLabel3 = [view3 viewWithTag:200];
    _priceLabel3.text = @"981元";
    _priceLabel4 = [view4 viewWithTag:200];
    _priceLabel4.text = @"-378元";
    
    UILabel *label3 = [Utils createLabelWithTitle:@"计价规则详情>" titleColor:HexColor(0x9C9C9C) fontSize:11];
    [label3 addTapGestureTarget:self action:@selector(btnIsClicked:)];
    
    [self.view sd_addSubviews:@[priceLabel,label2,
                                view1,
                                view2,
                                view3,
                                view4,
                                label3]];
    priceLabel.sd_layout
    .topSpaceToView(self.view, 50)
    .centerXEqualToView(self.view)
    .autoHeightRatio(0);
    [priceLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .leftSpaceToView(priceLabel, 14)
    .topSpaceToView(self.view, 75.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    view1.sd_layout
    .topSpaceToView(priceLabel, 39.5)
    .leftEqualToView(self.view)
    .widthIs(kScreenWidth);
    view2.sd_layout
    .topSpaceToView(view1, 28)
    .leftEqualToView(self.view)
    .widthIs(kScreenWidth);
    view3.sd_layout
    .topSpaceToView(view2, 28)
    .leftEqualToView(self.view)
    .widthIs(kScreenWidth);
    view4.sd_layout
    .topSpaceToView(view3, 28)
    .leftEqualToView(self.view)
    .widthIs(kScreenWidth);
    label3.sd_layout
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.view, 49.5)
    .autoHeightRatio(0);
    [label3 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
}

- (void)btnIsClicked:(UITapGestureRecognizer *)reg
{
    PriceRulesCtrl *vc = [PriceRulesCtrl new];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (UIView *)createInfoView{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label1 = [Utils createLabelWithTitle:@"总里程" titleColor:HexColor(0x656565) fontSize:14];
    label1.tag = 100;
    UILabel *label2 = [Utils createLabelWithTitle:@"1300公里" titleColor:HexColor(0x656565) fontSize:14];
    label2.tag = 200;
    [view sd_addSubviews:@[label1,label2]];
    label1.sd_layout
    .topEqualToView(view)
    .leftSpaceToView(view, 49.5)
    .autoHeightRatio(0);
    [label1 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    label2.sd_layout
    .topEqualToView(view)
    .rightSpaceToView(view, 49.5)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    [view setupAutoHeightWithBottomViewsArray:@[label1,label2] bottomMargin:0];
    return view;
}



@end
