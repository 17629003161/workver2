//
//  PriceRulesTableViewCell.swift
//  TravelNew
//
//  Created by mac on 2021/4/13.
//  Copyright © 2021 lester. All rights reserved.
//

import UIKit

class PriceRulesTableViewCell: UITableViewCell {
    var label1 : UILabel?
    var label2 : UILabel?
    var label3 : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    required init?(coder: NSCoder) {
        super .init(coder: coder)
        setupUI()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    func setupUI() -> Void {
        label1 = UILabel()
        label1?.textAlignment = NSTextAlignment.center
        label1?.text = "lable1"
        label1?.textColor = UIColor(hexString: "#484848")
        label1?.font = UIFont.systemFont(ofSize: 11)
        self.contentView.addSubview(label1!)
        label2 = UILabel()
        label2?.textAlignment = NSTextAlignment.center
        label2?.text = "lable2"
        label2?.textColor = UIColor(hexString: "#484848")
        label2?.font = UIFont.systemFont(ofSize: 11)
        self.contentView.addSubview(label2!)
        label3 = UILabel()
        label3?.textAlignment = NSTextAlignment.center
        label3?.text = "lable3"
        label3?.textColor = UIColor(hexString: "#484848")
        label3?.font = UIFont.systemFont(ofSize: 11)
        self.contentView.addSubview(label3!)
        let line = UIView()
        line.backgroundColor = UIColor(hexString: "#CFCFCF")
        self.contentView.addSubview(line)
        
        label1!.snp.makeConstraints { (make) in
            make.centerX.equalTo(60)
            make.centerY.equalTo(self.contentView)
            make.height.equalTo(11)
        }
        label2?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self.contentView)
            make.centerY.equalTo(self.contentView)
            make.height.equalTo(11)
        })
        label3!.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.contentView.snp.right).offset(-60)
            make.centerY.equalTo(self.contentView)
            make.height.equalTo(11)
        }
        line.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(0.5)
        }
    }
    
}
