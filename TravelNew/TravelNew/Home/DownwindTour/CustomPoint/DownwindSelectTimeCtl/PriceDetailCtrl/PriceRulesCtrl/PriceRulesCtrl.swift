//
//  PriceRulesCtrl.swift
//  TravelNew
//
//  Created by mac on 2021/4/13.
//  Copyright © 2021 lester. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire

class PriceRulesCtrl: BaseSwiftViewController {
    var cityLabel : UILabel?
    var cityLabel2 : UILabel?
    lazy var btnAry : [UIButton] = Array();
    lazy var tableView : UITableView = UITableView()
    var imgv:UIImageView?
    var btnIndex : Int = 0{
        willSet(newValue){
            print(newValue)
            for i in 0..<self.btnAry.count {
                let tmp = self.btnAry[i];
                if(tmp.tag==newValue){
                    tmp.backgroundColor = UIColor(hexString: "#f1f6ff")
                }else{
                    tmp.backgroundColor = UIColor.white
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hexString: "#ffffff")
        self.title = "计价规则"
        // Do any additional setup after loading the view.
        
        setupUI()
        self.btnIndex = 0;
    }
}


extension PriceRulesCtrl{
    @objc func btnIsClicked(btn:UIButton) -> Void {
        self.btnIndex = btn.tag;
        
        let url = BaseUrl + "attractions/scenic/list"
        let para : [String:Any] = ["cityid":"029",
                                   "pageIndex":1,
                                   "pageSize":4]
        AF.request(url, method: .post, parameters: para).responseJSON{(response) in
            switch response.result {
            case .success:
                let jsonstr = String(bytes: response.data!, encoding: .utf8)!
                print("\(jsonstr)")
                if let json = jsonstr.data(using: .utf8) {
                    let model = try? JSONDecoder().decode(Welcome.self, from:json)
                    let array = model?.data
                    for itme in array! {
                        print(itme.scenicName)
                    }
                    print(model!)
                }
                break
            case .failure(let error):
                print("error:\(error)")
                break
            }
        }
        
        
    }
    
    func setupUI() -> Void {
        self.view.addSubview(self.tableView)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView .register(PriceRulesTableViewCell.self, forCellReuseIdentifier: "PriceRulesTableViewCell")
        self.tableView.isScrollEnabled = false
        self.tableView.separatorStyle = .none
        self.tableView.layer.cornerRadius = 4.0
        self.tableView.layer.borderWidth = 0.5
        self.tableView.layer.borderColor = UIColor(hexString: "#CFCFCF")?.cgColor
        self.tableView.layer.masksToBounds = true
        
        cityLabel = UILabel()
        cityLabel?.text = "西安市"
        cityLabel?.textColor = UIColor(hexString: "#333333")
        cityLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.view.addSubview(self.cityLabel!)
        imgv = UIImageView()
        imgv?.image = UIImage(named: "trip_icon_single")
        self.view.addSubview(self.imgv!)
        cityLabel2 = UILabel()
        cityLabel2?.text = "成都市"
        cityLabel2?.textColor = UIColor(hexString: "#333333")
        cityLabel2?.font = UIFont.boldSystemFont(ofSize: 16)
        self.view.addSubview(self.cityLabel2!)
        
        let label1 = UILabel()
        label1.font = UIFont.systemFont(ofSize: 14)
        label1.textColor = UIColor(hexString: "#484848")
        label1.text = "出行人数"
        self.view.addSubview(label1)
        
        let label2 = UILabel()
        label2.font = UIFont.systemFont(ofSize: 14)
        label2.textColor = UIColor(hexString: "#484848")
        label2.text = "预约出行"
        self.view.addSubview(label2)
        
        let label3 = UILabel()
        label3.font = UIFont.systemFont(ofSize: 11)
        label3.textColor = UIColor(hexString: "#484848")
        label3.text = "计费说明：根据乘客行程起终点距离优先计算"
        self.view.addSubview(label3)
        
        let titleAry = ["1人","2人","3人"];
        for i in 0..<titleAry.count {
            let btn = UIButton(type: .custom);
            btn.tag = i
            btn.setTitle(titleAry[i], for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            btn.setTitleColor(UIColor(hexString: "#202020"), for: .normal)
            btn.backgroundColor = UIColor(hexString: "#ffffff")
            btn.layer.cornerRadius = 14.5
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = UIColor(hexString: "#4D9BFD")?.cgColor
            btn.layer.masksToBounds = true
            btn.addTarget(self, action: #selector(btnIsClicked(btn:)), for:.touchUpInside )
            self.view .addSubview(btn)
            self.btnAry.append(btn)
        }
        
        let tipLabel1 = UILabel()
        tipLabel1.text = "其他说明"
        tipLabel1.font = UIFont.systemFont(ofSize: 14)
        tipLabel1.textColor = UIColor(hexString: "#484848")
        self.view .addSubview(tipLabel1)
        
        let tipLabel2 = UILabel()
        tipLabel2.text = "附加费用："
        tipLabel2.font = UIFont.systemFont(ofSize: 12)
        tipLabel2.textColor = UIColor(hexString: "#484848")
        self.view.addSubview(tipLabel2)
        
        let vlabel2 = UILabel()
        vlabel2.numberOfLines = 0;
        vlabel2.text = "路线由车主选择，高速费及过路过桥费等行程中费用由车主支付"
        vlabel2.font = UIFont.systemFont(ofSize: 12)
        vlabel2.textColor = UIColor(hexString: "#989898")
        self.view .addSubview(vlabel2)
        
        let tipLabel3 = UILabel()
        tipLabel3.text = "服务费："
        tipLabel3.font = UIFont.systemFont(ofSize: 12)
        tipLabel3.textColor = UIColor(hexString: "#484848")
        self.view.addSubview(tipLabel3)
        
        let vlabel3 = UILabel()
        vlabel3.numberOfLines = 0;
        vlabel3.text = "平台收取分摊总费用的10%"
        vlabel3.font = UIFont.systemFont(ofSize: 12)
        vlabel3.textColor = UIColor(hexString: "#989898")
        self.view .addSubview(vlabel3)

        imgv?.snp.makeConstraints({ (make) in
            make.top.equalTo(90)
            make.centerX.equalTo(self.view)
            make.width.equalTo(14)
            make.height.equalTo(3)
        })
        cityLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(imgv!)
            make.right.equalTo(imgv!.snp.left).offset(-21)
            make.height.equalTo(15.5)
        })
        cityLabel2?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(imgv!)
            make.left.equalTo(imgv!.snp.right).offset(21)
            make.height.equalTo(15.5)
        })
        label1.snp.makeConstraints { (make) in
            make.top.equalTo(cityLabel!.snp.bottom).offset(29.5)
            make.left.equalTo(24)
            make.height.equalTo(13)
        }
        let w = 76,h=29
        for i in  0..<self.btnAry.count {
            let btn = self.btnAry[i]
            btn.snp.makeConstraints { (make) in
                make.top.equalTo(label1.snp.bottom).offset(15)
                make.left.equalTo(25+i*(w+15))
                make.width.equalTo(w)
                make.height.equalTo(h)
            }
        }
        label2.snp.makeConstraints { (make) in
            make.top.equalTo(label1).offset(74)
            make.left.equalTo(label1)
            make.height.equalTo(13)
        }
        label3.snp.makeConstraints { (make) in
            make.top.equalTo(label2.snp.bottom).offset(13.5)
            make.left.equalTo(label2)
            make.height.equalTo(11)
        }
        
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(label3.snp.bottom).offset(23)
            make.left.equalTo(25)
            make.right.equalTo(-25)
            make.height.equalTo(280)
        }
        tipLabel1.snp.makeConstraints { (make) in
            make.top.equalTo(self.tableView.snp.bottom).offset(29.5)
            make.left.equalTo(self.tableView.snp.left)
            make.height.equalTo(13.5)
        }
        tipLabel2.snp.makeConstraints { (make) in
            make.top.equalTo(tipLabel1.snp.bottom).offset(15.5)
            make.left.equalTo(tipLabel1.snp.left)
            make.height.equalTo(12)
        }
        vlabel2.snp.makeConstraints { (make) in
            make.left.equalTo(tipLabel2.snp.right).offset(4)
            make.top.equalTo(tipLabel2.snp.top)
            make.right.equalTo(-39)
            make.height.equalTo(29)
        }
        tipLabel3.snp.makeConstraints { (make) in
            make.top.equalTo(vlabel2.snp.bottom).offset(10)
            make.left.equalTo(tipLabel1.snp.left)
            make.height.equalTo(12)
        }
        vlabel3.snp.makeConstraints { (make) in
            make.left.equalTo(tipLabel3.snp.right).offset(4)
            make.top.equalTo(tipLabel3.snp.top)
            make.right.equalTo(-39)
            make.height.equalTo(10)
        }
        
    }
}

extension PriceRulesCtrl : UITableViewDataSource, UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row==0 || indexPath.row==2){
            return 45
        }else{
            return 38
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceRulesTableViewCell") as! PriceRulesTableViewCell
        if(indexPath.row==0){
            cell.backgroundColor = UIColor(hexString: "#f1f6ff")
            cell.label1?.text = "起步里程（公里）"
            cell.label2?.text = "拼座（元）"
            cell.label3?.text = "不拼座（元）"
        }else if(indexPath.row==2){
            cell.backgroundColor = UIColor(hexString: "#f1f6ff")
            cell.label1?.text = "超出里程（公里）"
            cell.label2?.text = "拼座（元/公里）"
            cell.label3?.text = "不拼座（元/公里）"
        }else{
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        print("点击了:\((indexPath as NSIndexPath).row)")
    }
    
    
}
