//
//  DownwindTopSelectView.h
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "BaseCustomView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DownwindTopSelectView : BaseCustomView
@property(nonatomic,assign) NSInteger selectIndex;
@property(nonatomic,copy) void(^backBlock)(void);
@property(nonatomic,copy) void(^selectBlock)(NSInteger);

@end

NS_ASSUME_NONNULL_END
