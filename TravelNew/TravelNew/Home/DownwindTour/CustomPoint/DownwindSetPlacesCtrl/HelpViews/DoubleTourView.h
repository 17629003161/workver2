//
//  DoubleTourView.h
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "BaseCustomView.h"
#import "LKAddressNode.h"

NS_ASSUME_NONNULL_BEGIN

@interface DoubleTourView : BaseCustomView
@property(nonatomic,strong) UITextField *field1;
@property(nonatomic,strong) UITextField *field2;
@property(nonatomic,strong) UITextField *field3;
@property(nonatomic,strong) UITextField *field4;
@property(nonatomic,strong) LKAddressNode *address1;
@property(nonatomic,strong) LKAddressNode *address2;
@property(nonatomic,strong) LKAddressNode *address3;
@property(nonatomic,strong) LKAddressNode *address4;

@property(nonatomic,strong) void (^selectBlock)(DoubleTourView *, UITextField *);
- (BOOL)isDataFinished;

@end

NS_ASSUME_NONNULL_END
