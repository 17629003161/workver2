//
//  SingleTourView.m
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "SingleTourView.h"

@interface SingleTourView()

@end

@implementation SingleTourView

- (BOOL)isDataFinished{
    if(self.address1 != nil && self.address3 != nil){
        return YES;
    }else{
        return NO;
    }
}

- (void)viewIsTyped:(UITapGestureRecognizer *)reg
{
    [_field1 resignFirstResponder];
    [_field2 resignFirstResponder];
    [_field3 resignFirstResponder];
}

- (void)fieldIsTaped:(UITapGestureRecognizer *)reg
{
    UITextField *field = (UITextField *)reg.view;
    if(self.selectBlock){
        self.selectBlock(self, field);
    }
}

- (void)configViews{
    [self addTapGestureTarget:self action:@selector(viewIsTyped:)];
    self.backgroundColor = [UIColor whiteColor];
    self.frame = CGRectMake(0, 0, kScreenWidth,205);
    _field1 = [self createTextField:HexColor(0xF8F9FB) dotImage:@"home_icon_origin"];
    _field1.userInteractionEnabled = NO;
    _field1.tag = 1;
    [_field1 addTapGestureTarget:self action:@selector(fieldIsTaped:)];
    _field2 = [self createTextField:HexColor(0xF2FCFF) dotImage:@"home_icon_attractions"];
    _field2.userInteractionEnabled = NO;
    _field2.tag = 2;
    [_field2 addTapGestureTarget:self action:@selector(fieldIsTaped:)];
    _field3 = [self createTextField:HexColor(0xEDF3FF) dotImage:@"home_icon_destination"];
    _field3.userInteractionEnabled = NO;
    _field3.tag = 3;
    [_field3 addTapGestureTarget:self action:@selector(fieldIsTaped:)];
    [self sd_addSubviews:@[_field1,_field2,_field3]];
    _field1.sd_layout
    .topSpaceToView(self, 20)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(45);
    _field2.sd_layout
    .topSpaceToView(_field1, 15)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(45);
    _field3.sd_layout
    .topSpaceToView(_field2, 15)
    .leftSpaceToView(self, 25)
    .rightSpaceToView(self, 25)
    .heightIs(45);
}

- (UITextField *)createTextField:(UIColor *)bgcolor dotImage:(NSString *)imgname{
    UIImageView *dot = ImageViewWithImage(imgname);
    UITextField *field = [UITextField new];
    field.textColor = HexColor(0x333333);
    field.backgroundColor = bgcolor;
    field.font = [UIFont systemFontOfSize:15];
    field.clearButtonMode=UITextFieldViewModeWhileEditing;
    [field textLeftOffset:37.5];
    [field addSubview:dot];
    dot.sd_layout
    .centerYEqualToView(field)
    .leftSpaceToView(field, 20)
    .widthIs(6)
    .heightIs(6);
    return field;
}

- (void)setAddress1:(LKAddressNode *)address1
{
    _address1 = address1;
    _field1.text = [NSString stringWithFormat:@"你将从 %@ 出发",address1.name];
    [_field1 changeStrings:@[address1.name] toColor:HexColor(0x1E69FF)];
}
- (void)setAddress2:(LKAddressNode *)address2
{
    _address2 = address2;
    _field2.text = _address2.name;
}
- (void)setAddress3:(LKAddressNode *)address3
{
    _address3 = address3;
    _field3.text = address3.name;
}


@end
