//
//  DownwindTopSelectView.m
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindTopSelectView.h"

@interface DownwindTopSelectView()
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UILabel *label2;

@end

@implementation DownwindTopSelectView
- (void)configViews{
    self.frame = CGRectMake(0, 0, kScreenWidth, 85);
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *backArrow = ImageViewWithImage(@"left_arrow_black");
    [backArrow addTapGestureTarget:self action:@selector(backarrowIsClicked:)];
    UILabel *label = [Utils createBoldLabelWithTitle:@"单程" titleColor:HexColor(0x141418) fontSize:16];
    label.tag = 0;
    [label addTapGestureTarget:self action:@selector(btnIsTyped:)];
    UILabel *label2 = [Utils createBoldLabelWithTitle:@"往返" titleColor:HexColor(0x141418) fontSize:16];
    label2.tag = 1;
    [label2 addTapGestureTarget:self action:@selector(btnIsTyped:)];
    self.label = label;
    self.label2 = label2;
    [self sd_addSubviews:@[backArrow,label,label2]];
    backArrow.sd_layout
    .bottomSpaceToView(self, 15)
    .leftSpaceToView(self, 17)
    .widthIs(30)
    .heightIs(30);
    
    label.sd_layout
    .centerYEqualToView(backArrow)
    .rightSpaceToView(self, kScreenWidth/2.0+20)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    
    label2.sd_layout
    .centerYEqualToView(backArrow)
    .leftSpaceToView(label, 40)
    .autoHeightRatio(0);
    [label2 setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

-(void)setSelectIndex:(NSInteger)selectIndex
{
    _selectIndex = selectIndex;
    [UIView animateWithDuration:0.2 animations:^{
        if(selectIndex==0){
            self.label.font = [UIFont boldSystemFontOfSize:16];
            self.label.textColor = HexColor(0x141418);
            self.label2.font = [UIFont systemFontOfSize:14];
            self.label2.textColor = HexColor(0x9C9C9C);
        }else if(selectIndex==1){
            self.label2.font = [UIFont boldSystemFontOfSize:16];
            self.label2.textColor = HexColor(0x141418);
            self.label.font = [UIFont systemFontOfSize:14];
            self.label.textColor = HexColor(0x9C9C9C);
        }
        [self.label updateLayout];
        [self.label2 updateLayout];
    }];
}

- (void)btnIsTyped:(UITapGestureRecognizer *)reg
{
    UILabel *label = (UILabel *)reg.view;
    self.selectIndex = label.tag;
    if(self.selectBlock){
        self.selectBlock(self.selectIndex);
    }
}

- (void)backarrowIsClicked:(UITapGestureRecognizer *)reg
{
    if(self.backBlock){
        self.backBlock();
    }
}

@end
