//
//  TourAddressTableViewCell.m
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "TourAddressTableViewCell.h"

@interface TourAddressTableViewCell()
@property(nonatomic,strong) UIImageView *imgv;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *subLabel;
@end

@implementation TourAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPoi:(AMapPOI *)poi
{
    _poi = poi;
    self.titleLabel.text = poi.name;
    self.subLabel.text = poi.address;
    [self.titleLabel updateLayout];
    [self.subLabel updateLayout];
}

- (void)configViews{
    _imgv = ImageViewWithImage(@"address_icon_place");
    _titleLabel = [Utils createLabelWithTitle:@"文苑大酒店" titleColor:HexColor(0x303030) fontSize:14];
    _subLabel = [Utils createLabelWithTitle:@"陕西省西安市莲湖区钟楼广场9号（同盛祥西侧）" titleColor:HexColor(0x484848) fontSize:10];
    [self.contentView sd_addSubviews:@[_imgv,_titleLabel,_subLabel]];
    _imgv.sd_layout
    .topSpaceToView(self.contentView, 12)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(9)
    .heightIs(12.5);
    _titleLabel.sd_layout
    .topEqualToView(_imgv)
    .leftSpaceToView(_imgv, 13)
    .autoHeightRatio(0);
    [_titleLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    _subLabel.sd_layout
    .leftEqualToView(_titleLabel)
    .bottomSpaceToView(self.contentView, 12)
    .autoHeightRatio(0);
    [_subLabel setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
}

@end
