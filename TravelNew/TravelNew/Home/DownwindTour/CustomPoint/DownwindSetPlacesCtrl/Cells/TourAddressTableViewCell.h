//
//  TourAddressTableViewCell.h
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//


#import "BaseCustomTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TourAddressTableViewCell : BaseCustomTableViewCell
@property(nonatomic,strong) AMapPOI *poi;
@end

NS_ASSUME_NONNULL_END
