//
//  DownwindTourPublishCtrl.m
//  TravelNew
//
//  Created by mac on 2021/3/17.
//  Copyright © 2021 lester. All rights reserved.
//

#import "DownwindSetPlacesCtrl.h"
#import "DownwindTopSelectView.h"
#import "SingleTourView.h"
#import "DoubleTourView.h"
#import "TourAddressTableViewCell.h"
#import "DownwindSelectTimeCtl.h"
#import "LKAddressSelectCtrl.h"


@interface DownwindSetPlacesCtrl ()
@property(nonatomic,strong) SingleTourView *singleView;
@property(nonatomic,strong) DoubleTourView *doubleView;
@property(nonatomic,assign) BOOL isDoubleTrip;              //0: 单程    1:双程
@property(nonatomic,strong) LKGadentButton *btn;
@property(nonatomic,strong) UIView *mapSelectView;

@end

@implementation DownwindSetPlacesCtrl

- (void)publishIsClicked:(UITapGestureRecognizer *)reg
{
    DownwindSelectTimeCtl *vc = [DownwindSelectTimeCtl new];
    vc.tripStep = TripStepTime;
    vc.isDoubleTrip = self.isDoubleTrip;
    if(self.isDoubleTrip){
        vc.address1 = self.doubleView.address1;         //出发地
        vc.address2 = self.doubleView.address2;         //去程途径地
        vc.address3 = self.doubleView.address3;         //返程途径地
        vc.address4 = self.doubleView.address4;         //旅游目的地
    }else{
        vc.address1 = self.singleView.address1;         //出发地
        vc.address2 = self.singleView.address2;         //途径地
        vc.address4 = self.singleView.address3;         //旅游目的地
    }
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    self.iskeyBordScrollUp = YES;
    LKWeakSelf
//    self.searchManger.onSearchResultBlock = ^(NSArray * _Nonnull dateArray) {
//        [weakSelf.dataArray removeAllObjects];
//        [weakSelf.dataArray addObjectsFromArray:dateArray];
//        [weakSelf.tableView reloadData];
//    };
//    [self.searchManger doPOIKeywordsSearch:@"大雁塔"];
    self.tableView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setIsDoubleTrip:(BOOL)isDoubleTrip
{
    _isDoubleTrip = isDoubleTrip;
    _singleView.hidden = isDoubleTrip;
    _doubleView.hidden = !isDoubleTrip;
}

- (void)setupUI{
    [self.view addSubview:self.mapView];
    self.mapView.frame = self.view.bounds;
    DownwindTopSelectView *topView = [DownwindTopSelectView new];
    _singleView = [SingleTourView new];
    _singleView.field1.placeholder = @"选择出发地";
    _singleView.field2.placeholder = @"选择途经地";
    _singleView.field3.placeholder = @"选择你的旅行目的地";
    LKWeakSelf
    _singleView.selectBlock = ^(SingleTourView * view, UITextField * field) {
        LKAddressSelectCtrl *vc = [LKAddressSelectCtrl new];
        vc.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, NSString * _Nonnull cityName, CLLocationCoordinate2D location) {
            LKAddressNode *addr = [[LKAddressNode alloc] initWithName:name coordinate:location cityId:cityid cityName:cityName];
            NSInteger tag = field.tag;
            if(tag==1){
                view.address1 = addr;
            }else if(tag==2){
                view.address2 = addr;
            }else if(tag==3){
                view.address3 = addr;
            }
            if([view isDataFinished]){
                weakSelf.btn.hidden = NO;
            }else{
                weakSelf.btn.hidden = YES;
            }
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    _doubleView = [DoubleTourView new];
    _doubleView.field1.placeholder = @"选择出发地";
    _doubleView.field2.placeholder = @"途径点（选填）";
    _doubleView.field3.placeholder = @"选择返程途径地（选填）";
    _doubleView.field4.placeholder = @"选择旅游目的地";
    _doubleView.selectBlock = ^(DoubleTourView * view, UITextField * field) {
        LKAddressSelectCtrl *vc = [LKAddressSelectCtrl new];
        vc.OnSelectAddressBlock = ^(NSString * _Nonnull name, NSString * _Nonnull cityid, NSString * _Nonnull cityName, CLLocationCoordinate2D location) {
            LKAddressNode *addr = [[LKAddressNode alloc] initWithName:name coordinate:location cityId:cityid cityName:cityName];
            NSInteger tag = field.tag;
            if(tag==1){
                view.address1 = addr;
            }else if(tag==2){
                view.address2 = addr;
            }else if(tag==3){
                view.address3 = addr;
            }else if(tag==4){
                view.address4 = addr;
            }
            if([view isDataFinished]){
                weakSelf.btn.hidden = NO;
            }else{
                weakSelf.btn.hidden = YES;
            }
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    LKLocationManager *manager = [LKLocationManager sharedManager];
    LKAddressNode *addr = [[LKAddressNode alloc] initWithName:manager.name coordinate:manager.coordinate cityId:manager.cityCode cityName:manager.cityName];
    _singleView.address1 = addr;
    _doubleView.address1 = addr;
    self.btn = [LKGadentButton blueButtonWhithTitle:@"确认发布" font:LKSystemFont(18) cornerRadius:23];
    [self.btn addTapGestureTarget:self action:@selector(publishIsClicked:)];
    self.btn.hidden = YES;
    _mapSelectView = [self createMapSelectView];
    _mapSelectView.hidden = YES;
    
    [self.view sd_addSubviews:@[topView,_singleView,_doubleView,_btn,_mapSelectView]];
    topView.selectIndex = 0;
    topView.backBlock = ^{
        [self.navigationController popViewControllerAnimated:YES];
    };
    topView.selectBlock = ^(NSInteger selectIndex) {
        NSLog(@"selectIndex = %ld",selectIndex);
        if(selectIndex==0){
            self.isDoubleTrip = NO;
        }else{
            self.isDoubleTrip = YES;
        }
    };
    _singleView.sd_layout
    .topSpaceToView(topView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(205);
    _doubleView.sd_layout
    .topSpaceToView(topView, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(265);
    self.isDoubleTrip = NO;
    [self.view addSubview:self.tableView];
    [self.view bringSubviewToFront:self.tableView];
    [self.tableView registerClass:[TourAddressTableViewCell class] forCellReuseIdentifier:@"TourAddressTableViewCell"];
    self.tableView.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view)
    .heightIs(190);
    _btn.sd_layout
    .bottomSpaceToView(self.view, 39.7)
    .leftSpaceToView(self.view, 27)
    .rightSpaceToView(self.view, 27)
    .heightIs(46);
    _mapSelectView.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view)
    .heightIs(50);
}

- (UIView *)createMapSelectView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *line = [UIView new];
    line.backgroundColor = HexColor(0x9f9f9f);
    UIImageView *icon = ImageViewWithImage(@"address_btn_map");
    UILabel *label = [Utils createBoldLabelWithTitle:@"在地图上选点" titleColor:HexColor(0x303030) fontSize:15];
    [view sd_addSubviews:@[line,icon,label]];
    line.sd_layout
    .leftEqualToView(view)
    .topEqualToView(view)
    .rightEqualToView(view)
    .heightIs(0.5);
    label.sd_layout
    .centerYEqualToView(view)
    .centerXEqualToView(view)
    .autoHeightRatio(0);
    [label setSingleLineAutoResizeWithMaxWidth:kScreenWidth];
    icon.sd_layout
    .centerYEqualToView(label)
    .rightSpaceToView(label, 7)
    .widthIs(19)
    .heightIs(19);
    return view;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TourAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TourAddressTableViewCell"];
    AMapPOI *poi = [self.dataArray objectAtIndex:indexPath.row];
    cell.poi = poi;
    return cell;
}

@end
