//
//  PublishOKDialog.m
//  TravelNew
//
//  Created by mac on 2020/9/22.
//  Copyright © 2020 lester. All rights reserved.
//

#import "PublishOKDialog.h"

@interface PublishOKDialog (){
    UIView *_maskView;
}
@property(nonatomic, strong) UILabel *messageLabel;          //商品数量
@end

@implementation PublishOKDialog

// 步骤3 在initWithFrame:方法中添加子控件
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 注意：该处不要给子控件设置frame与数据，可以在这里初始化子控件的属性
        self.frame = CGRectMake(0, 0, 270, 205);
        self.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0);
        self.backgroundColor = [UIColor clearColor];
        [self configViews];
    }
    return self;
}

- (void)setMessage:(NSString *)message
{
    _message = message;
    self.messageLabel.text = message;
    [self.messageLabel updateLayout];
    [self updateLayout];
}

- (void)configViews{
    _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    _maskView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.6];
    
    UIImageView *imgv = ImageViewWithImage(@"book_right_icon");
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 5;
    
    UILabel *msgL = [Utils createLabelWithTitle:@"您的信息已发布，预计10分钟内推荐官会与您联系..."
                                      titleColor:HexColor(0x4a4a4a)
                                        fontSize:16];
    msgL.numberOfLines = 0;
    
    UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:HexColor(0x4d83fd)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    btn.layer.cornerRadius = 8;
    [btn setTitle:@"我知道啦" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];

    [self sd_addSubviews:@[bgView,imgv,msgL,btn]];
    
    imgv.sd_layout
    .topEqualToView(self)
    .centerXEqualToView(self)
    .widthIs(79)
    .heightIs(63);
    
    bgView.sd_layout
    .topSpaceToView(self, 13)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .bottomEqualToView(self);
    
    msgL.sd_layout
    .topSpaceToView(self, 76)
    .leftSpaceToView(self, 30)
    .rightSpaceToView(self, 30)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .centerXEqualToView(self)
    .bottomSpaceToView(self, 20)
    .widthIs(200)
    .heightIs(38);
}



- (void)btnClicked:(id)sender{
    [self hide];
    if(_okBlock){
        _okBlock(@"ok");
    }
    
}

// 步骤4 在`layoutSubviews`方法中设置子控件的`frame`（在该方法中一定要调用`[super layoutSubviews]`方法）
- (void)layoutSubviews
{
    [super layoutSubviews];
    
}


- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:_maskView];
    [keyWindow addSubview:self];
}

- (void)hide{
    [self removeFromSuperview];
    [_maskView removeFromSuperview];
}

@end
