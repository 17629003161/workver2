//
//  PublishOKDialog.h
//  TravelNew
//
//  Created by mac on 2020/9/22.
//  Copyright © 2020 lester. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ActionBlock)(NSString *);

@interface PublishOKDialog : UIView

@property(nonatomic, strong) ActionBlock okBlock;
@property(nonatomic, strong) NSString *message;
- (void)show;
- (void)hide;

@end

NS_ASSUME_NONNULL_END
